﻿using Moq;
using Vhm.Web.BL;
using Vhm.Web.BL.Interfaces;
using VirginLifecareWeb.Controllers;

namespace Vhm.Tests
{
    public class BaseControllerTests
    {   
        protected Mock<IAccountBl> _accountBlMock;
        protected Mock<IMemberBl> _memberBLMock;
        protected Mock<ISocialBl> _socialBLMock;
        protected Mock<IRewardsBl> _rewardsBLMock;
        protected Mock<IMeasurementsBL> _measurementsBLMock;
        protected Mock<ISponsorBL> _sponsorBLMock;
        protected Mock<IPartialViewsBl> _partialViewsMock;
        protected Mock<IActivityBl> _activityBLMock;

        protected AccountController accountController { get; set; }
        protected ActivityController activityController { get; set; }
        protected SocialController socialController { get; set; }

        public BaseControllerTests()
        {
            _accountBlMock = new Mock<IAccountBl>();
            _memberBLMock = new Mock<IMemberBl>();
            _socialBLMock = new Mock<ISocialBl>();
            _rewardsBLMock = new Mock<IRewardsBl>();
            _measurementsBLMock = new Mock<IMeasurementsBL>();
            _sponsorBLMock = new Mock<ISponsorBL>();
            _partialViewsMock = new Mock<IPartialViewsBl>();
            _activityBLMock = new Mock<IActivityBl>();
        }

        public void GetControllers() {
            //accountController = new AccountController();

            activityController = new ActivityController();

            socialController = new SocialController();
            // code for other controllers as needed
        }
    }
}
