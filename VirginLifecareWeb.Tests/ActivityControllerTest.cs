﻿using System.IO;
using Vhm.Tests;
using Vhm.Web.Models;
using Vhm.Web.Models.ViewModels.Activity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using Vhm.Web.BL;
using System.Web.Mvc;
using System.Web;
namespace Vhm.UnitTests
{
    /// <summary>
    ///This is a test class for ActivityControllerTest and is intended
    ///to contain all ActivityControllerTest Unit Tests
    ///</summary>
    [TestClass]
    public class ActivityControllerTest : BaseControllerTests
    {
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes
        
        #endregion


        /// <summary>
        ///A test for Devices
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod]
        [HostType("ASP.NET")]
        [UrlToTest("http://local.virginhealthmiles.com/v2")]
        public void Devices_ReturnsValidViewModel()
        {
            AutoMapperConfig.Intialize();

            HttpContext.Current = new HttpContext(
                          new HttpRequest("", "http://tempuri.org", ""),
                          new HttpResponse(new StringWriter())
                          );

            var vhmSession = new Session { MemberID = 27, LanguageID = 1033, PrimarySponsorID = 1 };
            GetControllers();
            activityController.VhmSession = vhmSession;
            activityController.ColorThemes = activityController.GetColorThemes();
            var result = (ViewResult)activityController.Devices();

            var model = result.Model as DevicesVM;
            
            Assert.IsNotNull(model, "Model is null");
            Assert.IsTrue(!string.IsNullOrEmpty(model.SyncSoftwareURL), "SyncSoftwareURL is empty");
            Assert.IsTrue(!string.IsNullOrEmpty(model.RunKeeperURL), "RunKeeperURL is empty");
            Assert.IsNotNull(model.RegisterGoZoneURL, "RegisterGoZoneURL is null");
            Assert.IsTrue(!string.IsNullOrEmpty(model.ManageYourAccountURL), "ManageYourAccountURL is empty");
            Assert.IsTrue(!string.IsNullOrEmpty(model.LearnMoreAboutPolar), "LearnMoreAboutPolar is empty");
            Assert.IsTrue(!string.IsNullOrEmpty(model.HowToConnectRK), "HowToConnectRK is empty");
            Assert.IsTrue(!string.IsNullOrEmpty(model.HowToConnectFitBit), "HowToConnectFitBit is empty");
            Assert.IsNotNull(model.GZSupportURL, "GZSupportURL is null");
            Assert.IsNotNull(model.DownloadGZURL, "DownloadGZURL is null");
            Assert.IsNotNull(model.BuyPolarURL, "BuyPolarURL is null");
            Assert.IsNotNull(model.BuyGoZoneURL, "BuyGoZoneURL is null");
            Assert.IsNotNull(model.Content, "Content is null");
            Assert.IsNotNull(model.PageT, "PageT is null");
        }

    }

}
