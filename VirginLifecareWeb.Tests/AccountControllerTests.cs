﻿using Vhm.Web.BL.Interfaces;
using VirginLifecareWeb.Controllers;
using Vhm.Web.Models;
using Xunit;
using Moq;
using Vhm.Web.BL;
using MvcContrib.TestHelper;

namespace Vhm.Tests
{
    public class AccountControllerTests : BaseControllerTests
    {

        public AccountControllerTests()
        {
            _accountBlMock = new Mock<IAccountBl>();
            _memberBLMock = new Mock<IMemberBl>();
            _socialBLMock = new Mock<ISocialBl>();
            _rewardsBLMock = new Mock<IRewardsBl>();
            _measurementsBLMock = new Mock<IMeasurementsBL>();
            _sponsorBLMock = new Mock<ISponsorBL>();
            _partialViewsMock = new Mock<IPartialViewsBl>();
        }

        
        
        [Fact]
        public void Login_WithValidSession_ReturnsView()
        {
            // Arrange
            _accountBlMock.Setup(aa => aa.GetMemberIdBySession(It.IsAny<string>()))
                .Returns(0);

            GetControllers();

            // Act
            var result = accountController.Login();

            // Assert
            result.AssertViewRendered().ForView(string.Empty);
        }

        [Fact]
        public void LogOff_LogsUserOutAndReturnsView()
        {
            // Arrange
            GetControllers();

            accountController.VhmSession = new Session();
            
            // Act
            var result = accountController.LogOff();

            // Assert
            result.AssertActionRedirect().ToAction("Index").ToController("Home");
        }
    }
}
