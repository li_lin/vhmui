﻿using System.Collections.Generic;
using Vhm.Tests;
using Xunit;
using VirginLifecareWeb.Controllers;
using Vhm.Web.BL;
using Moq;
using MvcContrib.TestHelper;
using VirginLifecareWeb.Controllers.ActionResults;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.UnitTests
{
    public class SocialControllerTests : BaseControllerTests
    {

        [Fact]
        public void _AddMemberToGroup_ForNonExistentGroup_ReturnsFalse()
        {
            // Arrange
            var groupId = -1;
            GetControllers();
            _socialBLMock.Setup(sbl => sbl.GetGroupDetails(It.IsAny<string>(), It.IsAny<long>()))
                .Returns(default(Group));


            socialController.VhmSession = new Web.Models.Session();

            // Act
            var result = socialController._AddMemberToGroup(groupId);

            // Assert
            result.AssertResultIs<JsonNetResult>();

            var jsonNetResult = (JsonNetResult)result;

            Assert.Equal(false, jsonNetResult.Data);
        }

        [Fact]
        public void _AddMemberToGroup_ForPrivateGroup_ReturnsFalse()
        {
            // Arrange
            GetControllers();
            var groupId = 1;
            _socialBLMock.Setup(sbl => sbl.GetGroupDetails(It.IsAny<string>(), It.IsAny<long>()))
                .Returns(new Group { IsPrivate = true });

            socialController.VhmSession = new Web.Models.Session();

            // Act
            var result = socialController._AddMemberToGroup(groupId);

            // Assert
            result.AssertResultIs<JsonNetResult>();

            var jsonNetResult = (JsonNetResult)result;

            Assert.Equal(false, jsonNetResult.Data);
        }

        [Fact]
        public void _AddMemberToGroup_ForWrongCommunity_ReturnsFalse()
        {
            // Arrange
            var groupId = 1;
            _socialBLMock.Setup(sbl => sbl.GetGroupDetails(It.IsAny<string>(), It.IsAny<long>()))
                .Returns(new Group { CommunityID = 1 });

            GetControllers();

            socialController.VhmSession = new Web.Models.Session();

            // Act
            var result = socialController._AddMemberToGroup(groupId);

            // Assert
            result.AssertResultIs<JsonNetResult>();

            var jsonNetResult = (JsonNetResult)result;

            Assert.Equal(false, jsonNetResult.Data);
        }

        [Fact]
        public void _AddMemberToGroup_ForPublicSameCommunityGroup_AddsMemberAndReturnsTrue()
        {
            // Arrange
            var groupId = 1;
            
            _socialBLMock.Setup(sbl => sbl.GetGroupDetails(It.IsAny<string>(), It.IsAny<long>()))
                .Returns(new Group { IsPrivate = false, CommunityID = 0 });
            GetControllers();

            socialController.VhmSession = new Web.Models.Session();

            // Act
            var result = socialController._AddMemberToGroup(groupId);

            // Assert
            result.AssertResultIs<JsonNetResult>();

            var jsonNetResult = (JsonNetResult)result;

            Assert.Equal(true, jsonNetResult.Data);

            _socialBLMock.Verify(sbl => sbl.AddMemberToGroup(It.IsAny<string>(), It.IsAny<Group>(), It.IsAny<long>()));
        }

        [Fact]
        public void _AddMemberToGroupByNotificationId_WithBadNotificationId_ReturnsFalse()
        {
            // Arrange
            var notificationId = -1;
            GetControllers();

            // Act
            var result = socialController._AddMemberToGroupByNotificationId(notificationId);

            // Assert
            result.AssertResultIs<JsonNetResult>();

            var jsonNetResult = (JsonNetResult)result;

            Assert.Equal(false, jsonNetResult.Data);
        }

        [Fact]
        public void _AddMemberToGroupByNotificationId_WithMissingNotification_ReturnsFalse()
        {
            // Arrange
            var notificationId = 2;

            _socialBLMock.Setup(sbl => sbl.GetMemberNotifications(It.IsAny<string>(), It.IsAny<long>()))
                .Returns(new List<MemberNotification> { new MemberNotification { MemberNotificationId = 1 } });
 
            GetControllers();

            socialController.VhmSession = new Web.Models.Session();

            // Act
            var result = socialController._AddMemberToGroupByNotificationId(notificationId);

            // Assert
            result.AssertResultIs<JsonNetResult>();

            var jsonNetResult = (JsonNetResult)result;

            Assert.Equal(false, jsonNetResult.Data);
        }

        [Fact]
        public void _AddMemberToGroupByNotificationId_WithValidNotificationId_AddsMemberAndReturnsTrue()
        {
            // Arrange
            var notificationId = 1;

            _socialBLMock.Setup(sbl => sbl.GetMemberNotifications(It.IsAny<string>(), It.IsAny<long>()))
                .Returns(new List<MemberNotification> { new MemberNotification { MemberNotificationId = 1 } });

            GetControllers();

            socialController.VhmSession = new Web.Models.Session();

            // Act
            var result = socialController._AddMemberToGroupByNotificationId(notificationId);

            // Assert
            result.AssertResultIs<JsonNetResult>();

            var jsonNetResult = (JsonNetResult)result;

            Assert.Equal(true, jsonNetResult.Data);

            _socialBLMock.Verify(sbl => sbl.AddMemberToGroup(It.IsAny<string>(), It.IsAny<Group>(), It.IsAny<long>()));
        }
    }
}
