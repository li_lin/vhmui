﻿using System.IO;
using System.Web;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using Vhm.Web.BL;
using Vhm.Web.Models;
using VirginLifecareWeb.Controllers;
using System;

namespace Vhm.Tests
{
    public class MemberControllerTests : BaseControllerTests
    {
        [TestMethod]
        [HostType("ASP.NET")]
        [UrlToTest("http://local.virginhealthmiles.com/v2")]
        public void Devices_ReturnsValidViewModel()
        {
            AutoMapperConfig.Intialize();

            HttpContext.Current = new HttpContext(
                new HttpRequest( "", "http://tempuri.org", "" ),
                new HttpResponse( new StringWriter() )
                );

            var vhmSession = new Session { MemberID = 27, LanguageID = 1033, PrimarySponsorID = 1 };
            var memberController = new MemberController();
            memberController.VhmSession = vhmSession;
            memberController.ColorThemes = memberController.GetColorThemes();


            var result = (ViewResult) activityController.Devices();
        }
    }
}
