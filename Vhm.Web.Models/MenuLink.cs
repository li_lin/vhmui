﻿using System;

namespace Vhm.Web.Models {

    [Serializable]
    public class MenuLink {

        public MenuLink(string text, string url)
        {
            LinkText = text;
            Url = url;
        }

        public string LinkText { get; set; }
        public string Url { get; set; }
    }
}
