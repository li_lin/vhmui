﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vhm.Web.Models.Enums
{
    public enum GroupCategories
    {
        Nutrition = 1,
        Activity = 2,
        Lifestyle = 3,
        HealthConditions = 4,
        WeightManagement = 5,
        TipsGeneralInformation = 6,
        JustForFun = 7,
    }

    public enum  GroupTypes
    {
        COM = 1,
        MEM = 2,
        KAG = 3,
    }

    /// <summary>
    /// Collection of news feed post types
    /// </summary>
    public enum NewsFeedPostType
    {
        ManualPost = 1,
        CommentToPost = 2,
        LikePost = 3,
        MemberFriendWithMember = 4,
        MemberHasJoinedGroup = 5,
        MemberPostedInGroup = 6,
        MemberCreatedGroup = 7, 
        FriendCompletedActivityTask = 8,
        FriendReachedLevel = 9,
        FriendCompletedPromotion = 10,
        FriendReachedNewPersonalBest_N_Steps = 11,
        FriendPassed_N_MillionSteps = 12,
        FriendUploaded_N_ConsectiveDays = 13,
        FriendHasWonPersonalChllenge = 14,
        FriendWasOnWinningTeam = 15,
        FriendEarnedBadge = 16,
        FriendUpdatedProfile = 17,
        MemberAttendingEvent = 18,
        StartingToTrack = 19,
        HalfWayThere = 20,
        AllDone = 21,
        TellFriend = 22,
        AddedEntry = 23,
        ShareMyResults = 24
    }

    public sealed class NotificationType
    {
        public const byte CHALLENGEINVITATION = 1;
        public const byte GROUPINVITATION = 2;
        public const byte FRIENDREQUEST = 3;
        public const byte PROMOINVITE = 4;
        public const byte COMMENTONNEWSFEED = 5;
        public const byte LIKEANEWSFEED = 6;
        public const byte PERSONALMESSAGE = 7;
        public const byte TRACKINGINVITE = 8;
        public const byte NUTRITIONINVITE = 9;
        //public const int COMMENTONCOMMENT = 7;
    }

    public sealed class NotificationStatus
    {
        public const byte WAIT_FOR_ACTION = 1;
        public const byte VIEWED = 2;
        public const byte ACCEPTED = 3;
        public const byte DECLINED = 4;
        public const byte DELETED = 5;
        public const byte EXPIRED = 6;
        public const byte CLEARED = 7;
    }

    public sealed class GroupRelation
    {
        public const string NONE = "None";
        public const string INVITED = "Invited";
        public const string MEMBER = "Member";
        public const string CREATOR = "Creator";
        public const string ADMINISTRATOR = "Administrator";

    }
}
