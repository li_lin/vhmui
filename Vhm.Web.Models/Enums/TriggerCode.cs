﻿namespace Vhm.Web.Models.Enums
{
    public sealed class TriggerCode
    {
        public const string FIRST_ENTRY = "FIRST";
        public const string TELL_A_FRIEND = "FRNDS";
        public const string ALL_DONE = "ALLDN";
        public const string HALFWAY_THERE = "HLFWY";
    }
}
