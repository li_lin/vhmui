﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vhm.Web.Models.Enums
{
    public sealed class CommunityType
    {
        public const string COMMUNITY_TYPE_CLOSED = "CLO";
        public const string COMMUNITY_TYPE_PRIVATE = "PRI";
        public const string COMMUNITY_TYPE_OPEN = "OPN";
    }
    public enum MeasurementType
    {
        CHOLESTEROLHDL = 1,
        CHOLESTEROLLDL = 2,
        CHOLESTEROL = 3,
        CHOLESTEROLTRIGLYCERIDE = 4,
        GLUCOSE = 5,
        DISTANCE = 6,
        WEIGHT = 8,
        HEIGHT = 9,
        BLOODPRESSURESYSTOLIC = 11,
        BLOODPRESSUREDIASTOLIC = 12,
        BODYFAT = 13,
        BMI = 14
    }
}
