﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vhm.Web.Models.Enums
{
    public enum TrackingChallengeFrequencyType
    {
        Once,
        Daily,
        Weekly
    }
}
