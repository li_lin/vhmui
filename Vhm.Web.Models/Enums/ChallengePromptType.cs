﻿namespace Vhm.Web.Models.Enums
{
    public enum ChallengePromptType
    {
        YNO,
        TXT,
        DDM
    }
}
