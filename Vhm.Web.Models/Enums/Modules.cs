﻿namespace Vhm.Web.Models.Enums {
    public enum ModuleEnum {
        COR,
        VAC,
        VBI,
        SEA,
        SEB,
        HRA,
        OSB,
        TFA,
        WEL9,
        LST
    }

    public sealed class ModuleTypeCode
    {
        public const string NUTRITION = "NTR";
    }
}
