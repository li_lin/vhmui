﻿namespace Vhm.Web.Models.Enums
{
    public enum BadgeType
    {
        SYS,
        SPN,
        PRO,
        PER,
        SEC,
        TRK
    }
}
