﻿using System;

namespace Vhm.Web.Models.Enums {

    [Serializable]
    public enum LoginStatus {
        LoginOk,
        BadUserNamePassword,
        LastBadLogin,
        PasswordExpired,
        LockedOut,
        Error
    }
}
