﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vhm.Web.Models.Enums
{
    public sealed class SponsorLogoType
    {
        public const string GIF = "GIF";
        public const string JPEG = "JPEG";
        public const string JPG = "JPG";
        public const string HTML = "HTML";
        public const string PDF = "PDF";
        public const string TXT = "TXT";
        public const string TEXT = "TEXT";

    }
}
