﻿namespace Vhm.Web.Models.Enums
{
    public sealed class ChallengeEnums
    {
    }

    public sealed class ChallengeType
    {
        public const string GoZoneChallenge = "GZS";
        public const string HealthMilesChallenge = "MIL";
        public const string ActivityHealthMilesChallenge = "AMC";
        public const string ActiveMinutesChallenge = "MIN";
        public const string CaloriesBurnedChallenge = "CAL";
        public const string SelfEnteredActivityMinutesChallenge = "SEM";
        public const string PersonalKAG = "TRK";
        public const string BumpSteps = "BST";
        public const string SelfEnteredSteps = "SES";
    }

    public sealed class EReasonCodes
    {
        public const string GoZoneChallengeSecondInvite = "CH2";
        public const string GoZoneChallengeInviteManualTeams = "CIM";
        public const string GoZoneChallengeInviteAutoTeams = "CHI";
        public const string GoZoneChallengeInviteNoTeams = "CHN";

        public const string HealthMilesChallengeInviteManualTeams = "MC0";
        public const string HealthMilesChallengeInviteAutoTeams = "MC1";
        public const string HealthMilesChallengeSecondInvite = "MC2";
        public const string HealthMilesChallengeInviteNoTeams = "MCN";

        public const string ActivityHealthMilesChallengeInviteManualTeams = "AM0";
        public const string ActivityHealthMilesChallengeInviteAutoTeams = "AM1";
        public const string ActivityHealthMilesChallengeSecondInvite = "AM2";
        public const string ActivityHealthMilesChallengeInviteNoTeams = "AMN";

        public const string ActiveMinutesChallengeInviteManualTeams = "AN0";
        public const string ActiveMinutesChallengeInviteAutoTeams = "AN1";
        public const string ActiveMinutesChallengeSecondInvite = "AN2";
        public const string ActiveMinutesChallengeInviteNoTeams = "ANF";

        public const string CaloriesBurnedChallengeInviteManualTeams = "CB0";
        public const string CaloriesBurnedChallengeInviteAutoTeams = "CB1";
        public const string CaloriesBurnedChallengeSecondInvite = "CB2";
        public const string CaloriesBurnedChallengeInviteNoTeams = "CBF";

        public const string SelfEnteredActivityMinutesChallengeInviteManualTeams = "SE0";
        public const string SelfEnteredActivityMinutesChallengeInviteAutoTeams = "SE1";
        public const string SelfEnteredActivityMinutesChallengeSecondInvite = "SE2";
        public const string SelfEnteredActivityMinutesChallengeInviteNoTeams = "SEF";
    }

    public sealed class ChallengeOwnerType
    {
        public const string PersonalChallenge = "PER";
        public const string CorporateChallenge = "CRP";
    }

    public sealed class TeamCreationType
    {
        public const string TeamCreationTypeNone = "NONE";
        public const string TeamCreationTypeManual = "MANUAL";
        public const string TeamCreationTypeManualPlayerDefined = "MANUAL_PLAYER_DEFINED";
        public const string TeamCreationTypeAutoAssignRandom = "AUTO_ASSIGN_RANDOM";
        public const string TeamCreationTypeAutoAssignFilter = "AUTO_ASSIGN_FILTER";
        public const string TeamCreationTypeAutoAssignFilterMultiTeam = "AUTO_ASSIGN_FILTER_MULTI_TEAM";
        public const string TeamCreationTypeUserDefinedMultiTeam = "USER_DEFINED_MULTI_TEAM";
        public const string TeamCreationTypeUserDefinedSingleTeam = "USER_DEFINED_SINGLE_TEAM";
    }

    public sealed class ChallengeDevice
    {
        public const string GoZoneOnly = "GoZone";
        public const string AnyDevice = "Any";
        public const string NoDevice = "";
    }

    public sealed class DeviceType
    {
        public const string GoZone = "SBPED";
        public const string DashMobileApp = "IOSAD";
        public const string RunKeeper = "RNKPR";
        public const string Polar = "POLAR";
        public const string FitBit = "FITBT";
    }

    public sealed class PersonalChallengeType
    {
        public const string StandardTracking = "Standard";
        public const string KnowAnGo = "KAG";
    }
}
