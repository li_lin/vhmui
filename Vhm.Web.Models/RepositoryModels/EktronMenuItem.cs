﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.Models.RepositoryModels
{
    [Serializable]
    public class EktronMenuItem
    {
        public long MenuItemId { get; set; }
        public string MenuLinkTitle { get; set; }
        public string MenuLink { get; set; }
    }
}
