﻿using System;
using System.Collections.Generic;

namespace Vhm.Web.Models.RepositoryModels
{
    [Serializable]
    public class ChallengeQuestion
    {
        public long ChallengeQuestionID { get; set; }
        public long ChallengeId { get; set; }
        public int? IdealMin { get; set; }
        public int? IdealMax { get; set; }
        public string DisplayType { get; set; }
        public string Question { get; set; }
        public string AnswerType { get; set; }
        public byte QuestionFrequency { get; set; }
        public string QuestionFrequencyDatePart { get; set; }
        public int? MinValue { get; set; }
        public int? MaxValue { get; set; }
        public string ScoreType { get; set; }
        public bool LeaderBoardEnabled { get; set; }
        public bool ChatEnabled { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string QuestionPromptType { get; set; }
        public List<ChallengeTask> ChallengeTasks { get; set; }
        public List<ChallengeAnswerKey> ChallengeAnswerKeys { get; set; }
    }
}
