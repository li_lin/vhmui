﻿using System;

namespace Vhm.Web.Models.RepositoryModels {

    [Serializable]
    public class SponsorLanguage
    {
        public int LanguageID { get; set; }
        public string LanguageCode { get; set; }
        public string LanguageName { get; set; }
        public bool? IsSelected { get; set; }
        public bool IsDefault { get; set; }
        public short DisplayOrder { get; set; }
        public short? MsgLangID { get; set; }
    }
}
