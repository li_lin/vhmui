﻿using System;

namespace Vhm.Web.Models.RepositoryModels
{
    [Serializable]
    public class Member : IEquatable<Member>
    {
        public long MemberId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string City { get; set; }
        public string Activity { get; set; }
        public int? Age { get; set; }
        public decimal Average { get; set; }
        public int OccupationId { get; set; }
        public Occupation Occupation { get; set; }
        public int ChallengesParticipatedIn{get; set; }
        public string DisplayName{get; set; }
        public string EmailAddress { get; set; }
        public string Gender{get; set; }
        public string Goals { get; set; }
        public string IdentityNumber{get; set; }
        public string Interests { get;set;}
        public DateTime? LastGZUpload{get; set; }
        public int? Level{get; set; }
        public string Motto { get;set;}
        public int NewsFeedPermissions{get; set; }
        public bool Privacy { get; set; }
        public bool PrivacySettingsSet { get; set; }
        public int ProfileExists { get; set; }
        public byte[] ProfilePicture{ get; set; }
        public DateTime? RewardsAniversary { get; set; }
        public short ShowAge { get; set; }
        public bool ShowAnniversary { get; set; }
        public short ShowBadges { get; set; }
        public short ShowChallengeHistory { get; set; }
        public short ShowCity { get; set; }
        public int ShowFriendNewsFeed { get; set; }
        public short ShowGender { get; set; }
        public short ShowGoals { get; set; }
        public short ShowInterests { get; set; }
        public short ShowLastGZUpload{ get; set; }
        public short ShowNickname { get; set; }
        public short ShowOccupation { get; set; }
        public bool ShowPoints { get; set; }
        public short ShowSponsors { get; set; }
        public bool ShowState { get; set; }
        public short ShowStepsAvg { get; set; }
        public short ShowTotalSteps { get; set; }
        public string Sponsors { get; set; }
        public string State { get; set; }
        public string StateCode { get; set; }
        public string SpouseCode { get; set; }
        public bool? SuppressManageSponsor { get; set; }
        public double Total { get; set; }
	    public bool PersonalInfoComplete    { get; set; }
	    public bool OccupationComplete  { get; set; }
        public bool GoalsComplete  { get; set; }
	    public bool InterestsComplete { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string ImageUrl { get; set; }
        public DateTime LastLoginDate { get; set; }

        public override int GetHashCode()
        {
            return MemberId.GetHashCode();
        }

        public bool Equals(Member other)
        {
            return this.MemberId == other.MemberId;
        }
    }
}
