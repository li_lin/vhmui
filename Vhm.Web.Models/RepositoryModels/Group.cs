﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vhm.Web.Models.RepositoryModels
{
    [Serializable]
    public class Group
    {
        public string Name { get; set; }
        public long Id { get; set; }
        public byte? CategoryId { get; set; }
        public string Category { get; set; }
        public string ProfilePicturePath { get; set; }
        public long AdminId { get; set; }
        public string AdminName { get; set; }
        public string Description { get; set; }
        public bool IsPrivate { get; set; }
        public long CreatorId { get; set; }
        public Member CreatedBy { get; set; }
        public bool Deleted { get; set; }
        //public DateTime CreatedDate { get; set; }
        //display the created date as a string. We don't need the DateTime object on front end
        public string CreatedDateString { get; set; }
        public byte[] Image { get; set; }
        public string GroupImageUrl { get; set; }
        public int TotalMembers { get; set; }
        public long CommunityID { get; set; }
        public byte GroupTypeID { get; set; }
        public bool IsMemberGroupExpert { get; set; }
    }
    public class GroupCategory
    {
        public byte GroupCategoryId { get; set;}
        public string GroupCategoryName { get; set; }
        public string GroupCategoryDescription { get; set; }
    }
}
