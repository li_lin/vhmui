﻿using System;

namespace Vhm.Web.Models.RepositoryModels
{
    [Serializable]
    public class MemberBadge
    {
        public long BadgeID { get; set; }

        public string BadgeName { get; set; }

        public string BadgeCategoryCode { get; set; }

        public DateTime DateEarned { get; set; }

        public decimal? LevelsYearEarned { get; set; }

        public int FriendsWithBadge { get; set; }

        public long? TotalMembersWithBadge { get; set; }

        public long? TotalMembers { get; set; }
        
        public long RewardedTaskTransactionID { get; set; }
        
        public DateTime? BadgeFavoriteDate { get; set; }

        public byte AverageRating { get; set; }

        public int DisplayCelebration { get; set; }
    }
}
