﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vhm.Web.Models.RepositoryModels
{
    public class MemberNotification
    {
        public long MemberNotificationId { get; set; }
        public long MemberId { get; set; }
        public byte NotificationTypeId { get; set; }
        //public string NotificationTypeCode { get; set; }
        public long NotificationTypeEntityId { get; set; }
        public long NotifiedById { get; set; }
        public string NotificationDate { get; set; }
        public byte NotificationStatusID { get; set; }

        //TODO - not sure if or for what the below properties are populated in middle tier
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Sponsors { get; set; }
        public string City { get; set; }
        public string ImageUrl { get; set; }
        public MemberNotification ()
        {
            MemberNotificationId = 0;
            MemberId = 0;
            NotificationTypeId = 0;
            NotificationTypeEntityId = 0;
            NotifiedById = 0;
            NotificationDate = string.Empty;
            NotificationStatusID = 0;
            FirstName = string.Empty;
            LastName = string.Empty;
            Sponsors = string.Empty;
            City = string.Empty;
            ImageUrl = string.Empty;
        }
    }
}
