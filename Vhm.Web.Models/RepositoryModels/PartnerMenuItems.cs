﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vhm.Web.Models.RepositoryModels
{
    [Serializable]
    public class PartnerMenuItems
    {
        public long ItemId { get; set; }

        public string CodeName { get; set; }

        public string Name { get; set; }

        public string Link { get; set; }
    }
}
