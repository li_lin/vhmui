﻿using System;

namespace Vhm.Web.Models.RepositoryModels
{
    [Serializable]
    public class MemberDeviceOrder
    {
        public long SalesOrderID { get; set; }
        public string OrderDate { get; set; }
	    public string ProductCode { get; set; }
	    public string ProductTitle { get; set; }
        public string GoZoneDeviceDescr { get; set; }
        public string GoZoneDeviceVersion { get; set; }
        public Nullable<bool> Shipped { get; set; }
    }
}
