﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vhm.Web.Models.RepositoryModels
{
    public class SocialPagedData
    {
        public bool HasMore { get; set; }
        public List<Member> Friends { get; set; }
        public List<Group> Groups { get; set; }
        public List<Member> SuggetedMembers { get; set; }
        public List<Group> SuggestedGroups { get; set; }
        public List<Member> GroupMembers { get; set; } 
        public int TotalCount { get; set; }
        public int CurrentPage { get; set; }
        public int PerPage { get; set; }
        public int TotalPageCount { get; set; }

        public SocialPagedData()
        {
            Friends = new List<Member>();
            Groups = new List<Group>();
            SuggetedMembers = new List<Member>();
            SuggestedGroups = new List<Group>();
            GroupMembers = new List<Member>();
        }
    }
}
