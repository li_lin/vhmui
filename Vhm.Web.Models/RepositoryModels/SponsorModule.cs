﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Vhm.Web.Models.RepositoryModels {
    [Serializable]
    public class SponsorModule {
        public int SponsorModuleID {
            get;
            set;
        }

        public List<SponsorModuleGZVersion> GZs {
            get;
            set;
        }

        public long SponsorID {
            get;
            set;
        }
        public byte ModuleID {
            get;
            set;
        }
        public string ModuleTypeCode { get; set; }
        public string ModuleName { get; set; }
        public DateTime AvailableFrom {
            get;
            set;
        }
        public DateTime? AvailableTo { get; set; }
        public short? TimeLimitedAccessDays {
            get;
            set;
        }
        public string ModuleOptionList {
            get;
            set;
        }
        public string DeviceOrderTypeCode {
            get;
            set;
        }
        public string GoZoneRedeemAt {
            get;
            set;
        }
        [Range(1, int.MaxValue)]
        public int? FreeGZLimitByNumber {
            get;
            set;
        }
        public DateTime? FreeGZLimitByDate {
            get;
            set;
        }
        public bool? iSyncDownloadPrompt {
            get;
            set;
        }
        public bool? OtherDevices {
            get;
            set;
        }
        public bool? HZLocator {
            get;
            set;
        }
        public bool? AllowFileMeasurements {
            get;
            set;
        }
        public bool? AllowAssessorMeasurements {
            get;
            set;
        }
        public string MaskModuleName { get; set; }
        public byte[] ModuleImage { get; set; }
        public Guid PartnerGUID { get; set; }
        public long? SegmentId { get; set; }
        public string URL { get; set; }
        public bool isPartner { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 3)]
        public string HRAName {
            get;
            set;
        }
        public short? HRASetting {
            get;
            set;
        }
        public bool? AllowDeviceMessaging { get; set; }
        public bool? AllowBumpChallenge { get; set; }

    }
}
