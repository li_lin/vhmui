﻿using System;

namespace Vhm.Web.Models.RepositoryModels
{
    [Serializable]
    public class ChallengeAnswerKey
    {
        public long ChallengeAnswerKeyID { get; set; }
        public long ChallengeQuestionID { get; set; }
        public string Description { get; set; }
        public int EntryMin { get; set; }
        public int EntryMax { get; set; }
        public int Score { get; set; }
    }
}
