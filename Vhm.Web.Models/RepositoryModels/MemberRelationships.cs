﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vhm.Web.Models.RepositoryModels
{
    public class MemberRelationships
    {
        public bool IsFriend { get; set; }
        public MemberNotification ToFriendNotification { get; set; }
        public MemberNotification ToMemberNotification { get; set; }
    }
}
