﻿using System;

namespace Vhm.Web.Models.RepositoryModels {

    [Serializable]
    public class Quest : ChallengeBase {

        public Quest()
        {
            challengeType = CHALLENGE_TYPE_QUEST;
        }
    }
}
