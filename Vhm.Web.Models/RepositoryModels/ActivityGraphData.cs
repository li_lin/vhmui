﻿using System;
using System.Collections.Generic;

namespace Vhm.Web.Models.RepositoryModels {
    public class ActivityGraphData {
        public decimal Target { get; set; }
        public decimal TargetGraphPosition { get; set; }
        public List<ActivityDay> ActivityDays { get; set; }
        public bool HasStepsForPeriod { get; set; }

        public class ActivityDay
        {
            public bool TargetAchieved { get; set; }
            public decimal Steps { get; set; }
            public decimal GraphBarHeight { get; set; }
            public DateTime ActivityDate { get; set; }
        }
    }
}
