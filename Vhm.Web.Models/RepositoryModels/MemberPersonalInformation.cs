﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vhm.Web.Models.RepositoryModels
{
    public class MemberPersonalInformation
    {
        public long MemberID { get; set; }

        public string Title { get; set; }

        public string Forename { get; set; }

        public string Surname { get; set; }

        public string Gender { get; set; }

        public DateTime DateOfBirth { get; set; }

        public int? OccupationID { get; set; }

        public short? CultureID { get; set; }

        public int? LanguageID { get; set; }

        public string Occupation { get; set; }

        public string OccupationOther { get; set; }

        public string IdentityNumber { get; set; }

        public DateTime? AnniversaryDate { get; set; }

        public string Sponsors { get; set; }

        public int? SponsorCount { get; set; }

        public string CultureInfo { get; set; }
 
    }
}
