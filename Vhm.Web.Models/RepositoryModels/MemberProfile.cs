﻿namespace Vhm.Web.Models.RepositoryModels
{
    public class MemberProfile
    {
        public long MemberID { get; set; }
        public string MemberName { get; set; }
        public string EmailAddress { get; set; }
        public byte[] ProfilePicture { get; set; }
       
    }
}
