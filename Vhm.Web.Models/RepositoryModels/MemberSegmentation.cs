﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vhm.Web.Models.RepositoryModels
{
    public class MemberSegmentation
    {
        public long MemberSegmentId
        {
            get;
            set;
        }
        public long SegmentId
        {
            get;
            set;
        }
        public long MemberId
        {
            get;
            set;
        }
    }
}
