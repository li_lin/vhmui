﻿using System;

namespace Vhm.Web.Models.RepositoryModels {

    [Serializable]
    public class NewsfeedPost {
        public long PostId { get; set; }
        public long? ParentId { get; set; }
        public DateTime PostDate { get; set; }
        public string PostText { get; set; }
        public string ImageName { get; set; }
        public int LikeCount { get; set; }
        public bool YouLikeThis { get; set; }
        public long YouLikedId { get; set; }
        public string PostType { get; set; }
        public int PostTypeId { get; set; }

        public long PostById { get; set; }
        public string PostByName { get; set; }
        public string PostByImage { get; set; }

        public long PostToId { get; set; }
        public string PostToName { get; set; }
        
        public long GroupId { get; set; }
        public string GroupName { get; set; }
        public string GroupImage { get; set; }

        public long? BadgeId { get; set; }
        public string BadgeName { get; set; }
        public string BadgeImage { get; set; }
        public int TotalComments { get; set; }
        public int AggregationCount { get; set; }

        public const string POST_TYPE_FRIEND = "MEMFM";
        public const string POST_TYPE_JOIN_GROUP = "MEMJG";
        public const string POST_TYPE_PROMO = "FRIPR";
        public const string POST_TYPE_MANUAL = "MANNP";
        public const string POST_TYPE_BADGE = "FRIEB";

    }
}
