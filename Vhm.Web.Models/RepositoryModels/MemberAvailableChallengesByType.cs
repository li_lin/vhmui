﻿using System;
using System.Collections.Generic;

namespace Vhm.Web.Models.RepositoryModels
{
    public class MemberAvailableChallengesByType
    {
        #region Primitive Properties

        public long ChallengeId { get; set; }

        public long ChallengeMemberID { get; set; }


        public string Name { get; set; }


        public string ChallengeType { get; set; }


        public string ChallengeCategory { get; set; }


        public long OwnerID { get; set; }


        public string OwnerType { get; set; }


        public DateTime? GoLiveDate { get; set; }


        public DateTime SignUpDeadline { get; set; }


        public DateTime StartDate { get; set; }


        public DateTime? EndDate { get; set; }


        public DateTime? UploadDeadline { get; set; }


        public bool IsChatEnabled { get; set; }


        public decimal? ScoreTarget { get; set; }


        public string TeamCreationType { get; set; }


        public string ParticipationType { get; set; }


        public string Segmentation { get; set; }


        public long? DailyLimit { get; set; }


        public bool CanRewardForSignUp { get; set; }


        public bool? Live { get; set; }


        public bool HasSentNoTeamEmail { get; set; }


        public bool HasSentGoEmail { get; set; }


        public bool HasSentWarningEmail { get; set; }


        public bool HasSentWinnerEmail { get; set; }


        public bool HasFinalizedPositions { get; set; }


        public bool HasFinished { get; set; }


        public long? CreatedBy { get; set; }


        public DateTime CreatedDate { get; set; }


        public long? UpdatedByID { get; set; }


        public DateTime? UpdatedDate { get; set; }


        public string RulesCopy { get; set; }

        public DateTime? EndingReminderEmailDate { get; set; }


        public bool? HasSentEndingReminderEmail { get; set; }


        public List<ChallengeQuestion> ChallengeQuestions { get; set; }


        public long? ChallengeTimeZoneIdentifier { get; set; }


        #endregion
    }
}
