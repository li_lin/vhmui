﻿using System;

namespace Vhm.Web.Models.RepositoryModels
{
    [Serializable]
    public class ColorTheme
    {
        public string ColorThemeName { get; set; }
        public string ColorThemeCode { get; set; }
        public bool isDefault { get; set; }

        public byte BrandingThemeId { get; set; }
    }
}
