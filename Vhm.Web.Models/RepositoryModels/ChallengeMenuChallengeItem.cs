﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vhm.Web.Models.RepositoryModels {

    [Serializable]
    public class ChallengeMenuChallengeItem : IComparable {
        private long _id;

        public string ChallengeName { get; set; }

        public ChallengeItemCategory ChallengeType { get; set; }

        public long ChallengeId {
            get { return _id; }
        }

        public string Link { get; set; }

        public ChallengeStatus Status { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public ChallengeMenuChallengeItem(string challengeName, ChallengeStatus challengeStatus,
                long id, DateTime startDate, DateTime endDate, ChallengeItemCategory challengeType, string link, string menuCategory)
        {
            ChallengeName = challengeName;
            Link = link;
            Status = challengeStatus;
            _id = id;
            ChallengeType = challengeType;
            StartDate = startDate;
            EndDate = endDate;
        }

        public int CompareTo(object obj) {
            if (obj is ChallengeMenuChallengeItem) {
                var chmchi = (ChallengeMenuChallengeItem)obj;

                int result = 0;

                result = Status.CompareTo(chmchi.Status);

                if (result == 0
                    && Status == ChallengeStatus.Created
                    && chmchi.Status == ChallengeStatus.Created) {
                    result = chmchi.StartDate.Date.CompareTo(StartDate.Date); // desc order
                }

                if (result == 0) {
                    result = ChallengeName.CompareTo(chmchi.ChallengeName);
                }

                return result;
            } else {
                throw new ArgumentException("Object is not a ChallengeMenuItem");
            }
        }
    }
}
