﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vhm.Web.Models.RepositoryModels
{
    public class PasswordRegEx
    {
        public long PasswordRegExId
        {
            get;
            set;
        }
        public string RegEx
        {
            get;
            set;
        }
        public string Description
        {
            get;
            set;
        }
    }
}
