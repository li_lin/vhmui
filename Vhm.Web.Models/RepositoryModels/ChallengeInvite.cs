﻿using System;

namespace Vhm.Web.Models.RepositoryModels
{
    [Serializable]
    public class ChallengeInvite
    {
        public long ChallengeInviteID { get; set; }
        public long ChallengeID { get; set; }
        public long? MemberID { get; set; }
        public string EmailAddress { get; set; }
        public DateTime? SentDate { get; set; }
        public bool Declined { get; set; }
        public bool DoNotRemind { get; set; }
        public decimal Handicap { get; set; }
        public string Source { get; set; }
        public int? GroupID { get; set; }
        public long? CreatedByID { get; set; }
        public DateTime? CreatedDate { get; set; }
        public bool Sent { get; set; }
    }
}
