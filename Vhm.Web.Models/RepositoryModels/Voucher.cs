﻿using System;

namespace Vhm.Web.Models.RepositoryModels
{
    public class Voucher
    {
        public long SponsorID { get; set; }
        public string SponsorName { get; set; }
        public string Description { get; set; }
        public string CodeType { get; set; }
        public string CodeTypeDescription { get; set; }
        public string CurrencyCode { get; set; }
        public decimal? Value { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime? Expiry { get; set; }
        public bool? Deleted { get; set; }
        public bool? Used { get; set; }
        public bool? Success { get; set; }
        public string VoucherCode { get; set; }
    }
}
