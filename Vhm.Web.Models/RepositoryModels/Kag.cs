﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vhm.Web.Models.RepositoryModels {

    [Serializable]
    public class Kag : Promo {

        public Kag()
        {
            challengeType = CHALLENGE_TYPE_KAG;
        }

        public PromoTarget Target { get; set; }
        public FrequencyTypes FrequencyType { get; set; }
        public string TaskType { get; set; }
        public ResponseTypes ResponseType { get; set; }
        public decimal? CurrentScore { get; set; }
        public DateTime CurrentDate { get; set; }
        public bool IsComplete { get; set; }
        public List<KagTrackerData> Tracker { get; set; }
        public bool ShowPage { get; set; }
        public string TrackerQuestion { get; set; }
        public string RewardImage { get; set; }
        public int RewardAmount { get; set; }

        #region Enumerations
        public enum FrequencyTypes { Once = 1, Daily = 2, Weekly = 3 }
        public enum ResponseTypes { Number = 1, YesNo = 2 }
        #endregion

        #region Constants
        public const string SEC_TYPE_WEIGHT = "WGT";
        #endregion
    }
}
