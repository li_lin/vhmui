﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vhm.Web.Models.RepositoryModels
{
    public class MemberRewardsBalance
    {
        public long? TotalHealthMiles
        {
            get;
            set;
        }

        public long? TotalActivityHealthMiles
        {
            get;
            set;
        }

        public long? TotalMeasurementHealthMiles
        {
            get;
            set;
        }

        public long? TotalParticipationHealthMiles
        {
            get;
            set;
        }
    }
}
