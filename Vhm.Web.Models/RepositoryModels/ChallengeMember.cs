﻿using System;

namespace Vhm.Web.Models.RepositoryModels {
    public class ChallengeMember {
        
        public long ChallengeMemberID { get; set; }
        public long MemberId { get; set; }
        public long ChallengeId { get; set; }
        public decimal Handicap { get; set; }
        public DateTime SignUp { get; set; }
        public decimal Score { get; set; }
        public int? Rank { get; set; }
        public string DeviceType { get; set; }
        public DateTime? LastUpload { get; set; }
        public int? TrophyId { get; set; }
        public long? TeamID { get; set; }
    }
}
