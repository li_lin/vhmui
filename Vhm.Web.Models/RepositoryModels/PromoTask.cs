﻿using System;

namespace Vhm.Web.Models.RepositoryModels {

    [Serializable]
    public class PromoTask
    {
        public long PromoTaskID { get; set; }
        public int PromoTaskOrder { get; set; }
        public long PromoID { get; set; }
        public string PromoTaskTypeID { get; set; }
        public bool RelationAndOr { get; set; }
        public DateTime? TaskStartDate { get; set; }
        public DateTime? TaskEndDate { get; set; }
        public int? StepsThreshold { get; set; }
        public int? InNumberDays { get; set; }
        public int? StepsTotalThreshold { get; set; }
        public int? RewardsAmount { get; set; }
        public string RewardsDescription { get; set; }
        public bool Deleted { get; set; }
        public int? GoalDaysForTask { get; set; }
        public DateTime? BaseLineStartDate { get; set; }
        public DateTime? BaseLineEndDate { get; set; }
        public bool? IsActivityHealthMiles { get; set; }
        public string RewardType { get; set; }
        public string RewardTypeDescription { get; set; }
        public string TriggerCode { get; set; }
        public short? RewardLimit { get; set; }
        public string ImageName { get; set; }
        public bool? Cash { get; set; }
        public string RewardTriggerCategoryCode { get; set; }
   }
}
