﻿using System;

namespace Vhm.Web.Models.RepositoryModels {

    [Serializable]
    public class MeasurementReward {

        public DateTime MeasurementDate { get; set; }
        public decimal TaskRewardAmt { get; set; }
        public string RewardImage { get; set; }
        public bool Cash { get; set; }
        public bool IsBadge { get; set; }

    }
}
