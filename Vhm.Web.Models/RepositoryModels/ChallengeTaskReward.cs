﻿using System;

namespace Vhm.Web.Models.RepositoryModels
{
    [Serializable]
    public class ChallengeTaskReward 
    {

        public long ChallengeTaskRewardID { get; set; }
        public long ChallengeTaskID { get; set; }
        public string RewardType { get; set; }
        public decimal Amount { get; set; }
        public string CertificateName { get; set; }
        public short? ExpirationDays { get; set; }
        public string RewardDetails { get; set; }
        public long? BadgeID { get; set; }

    }
}
