﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vhm.Web.Models.RepositoryModels
{
    public class SmokingInfo
    {
        public string QuestionDesc
        {
            get;
            set;
        }

        public string QuestionAns
        {
            get;
            set;
        }

        public int QuestionaireItemId
        {
            get;
            set;
        }
    }
}
