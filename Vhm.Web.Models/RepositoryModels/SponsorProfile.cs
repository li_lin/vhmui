﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.DataServices.Adapters
{
    [Serializable]
    public class SponsorProfile
    {
        public long SponsorID { get; set; }
        public string SponsorName { get; set; }
        public string AccountManagerName { get; set; }
        public long CommunityID { get; set; }
        public string CommunityTypeCode { get; set; }
        public string SponsorTypeCode { get; set; }
        public string SponsorTypeDescription { get; set; }
        public string DisplayName { get; set; }
        public string VanityUrl { get; set; }
        public bool AllowSearch { get; set; }
        public bool AllowMobileAppAccess { get; set; }
        public bool AllowPersonalChallengeAccess { get; set; }
        public bool AllowChatInPersonalChallenge { get; set; }
        public string CustomSupportEmailAddress { get; set; }
        public string VerificationTypeCode { get; set; }
        public long AccountManagerID { get; set; }        
        public DateTime? LaunchPeriodStartDate { get; set; }
        public DateTime? LaunchPeriodEndDate { get; set; }
        public DateTime? RegistrationDeadline { get; set; }
        public bool International { get; set; }
        public string LogoType { get; set; }
        public byte[] SponsorLogo { get; set; }
        public byte BrandingThemeID { get; set; }
        public bool IncludePoweredByVHM { get; set; }
        public bool UseCustomBrand { get; set; }
        public long? PasswordRegexID { get; set; }
        public int PasswordExpireDays { get; set; }
        public bool AllowPasswordReuse { get; set; }
        public short CultureID { get; set; }
        public string CultureCode { get; set; }
        public List<SponsorLanguage> SponsorLanguages { get; set; }
        public byte IntelliResponseID { get; set; }
        public bool? GenesisUI { get; set; }
        public SponsorProfile()
        {
            
        }
        
    }
}
