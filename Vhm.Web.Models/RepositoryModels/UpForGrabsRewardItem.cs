﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vhm.Web.Models.RepositoryModels
{
    [Serializable]
    public class UpForGrabsRewardItem
    {
        public long Id { get; set; }
        public long RewardId { get; set; }
        public string RewardType { get; set; }
        public string Text { get; set; }
        public string RewardTypeDescription { get; set; }
        public int Amount { get; set; }
        public long? BadgeId { get; set; }
        public short? ExpirationDays { get; set; }
        public string CertificateName { get; set; }
        public bool IsCash { get; set; }
        public string RewardImage { get; set; }
        public string RewardTriggerName { get; set; }
    }
}
