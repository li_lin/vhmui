﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.Models.RepositoryModels
{
    [Serializable]
    public class EktronRichContent
    {
        public string Title { get; set; }
        public string Html { get; set; }
        public long Id { get; set; }
        public DateTime DateCreated { get; set; }

    }
}
