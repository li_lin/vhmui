﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace Vhm.Web.Models.RepositoryModels
{
    [Serializable]
    public class BadgeItem
    {
        public long BadgeID { get; set; }
        public string BadgeName { get; set; }
        public string BadgeDescription { get; set; }
        public string BadgeTypeName { get; set; }
        public string BadgeTypeCode { get; set; }
        public string BadgeCategoryName { get; set; }
        public string BadgeCategoryCode { get; set; }
        public bool ExtraCelebration { get; set; }
        public bool IsLocked { get; set; }
        public bool Live { get; set; }
        public byte[] BadgeImage { get; set; }
        public byte[] BadgeImage_bw { get; set; }

        public BadgeItem()
        {
        }
    }

}

