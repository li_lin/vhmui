﻿using System;

namespace Vhm.Web.Models.RepositoryModels
{
    [Serializable]
    public class MemberDevice
    {
        public long DeviceID { get; set; }
        public string DeviceType { get; set; }
        public long MemberID { get; set; }
        public string SerialNumber { get; set; }
        public string RemoteID { get; set; }
        public DateTime RegistrationDate { get; set; }
        public int? RemoteDeviceVersionID { get; set; }
        public bool? MidnightReset { get; set; }
        public bool? ActiveMinutesMode { get; set; }
        public double? Battery { get; set; }
        public DateTime CreateDate { get; set; }
        public string ThirdPartyId { get; set; }
    }
}
