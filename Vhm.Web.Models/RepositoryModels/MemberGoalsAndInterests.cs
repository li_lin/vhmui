﻿using System;

namespace Vhm.Web.Models.RepositoryModels
{
    [Serializable]
    public class MemberGoalsAndInterests
    {

        public long? MemberID
        {
            get;
            set;
        }

        public bool Selected
        {
            get;
            set;
        }

        public string Type
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public string FilteredDescription
        {
            get;
            set;
        }

        public long? MemberGoalsInterestsID
        {
            get;
            set;
        }

        public short? PredefinedGoalsInterestsID
        {
            get;
            set;
        }

    }
}
