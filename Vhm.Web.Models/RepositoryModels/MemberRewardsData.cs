﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vhm.Web.Models.RepositoryModels
{
    public class MemberRewardsData
    {
        public DateTime Today
        {
            get;
            set;
        }

        public DateTime HmSince
        {
            get;
            set;
        }

        public DateTime? Aniversary
        {
            get;
            set;
        }

        public DateTime Dob
        {
            get;
            set;
        }

        public DateTime ProgramStartDate
        {
            get;
            set;
        }

        public long StructureId
        {
            get;
            set;
        }

        public long SponsorId
        {
            get;
            set;
        }

        public string RewardsAnniversaryType
        {
            get;
            set;
        }

        public DateTime? MemberProgramStart
        {
            get;
            set;
        }

        public int? GameYear
        {
            get;
            set;
        }
    }
}
