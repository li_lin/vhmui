﻿namespace Vhm.Web.Models.RepositoryModels
{
    public class ChallengeMemberScore
    {
        public long MemberChallengeId { get; set; }
        public long MemberId { get; set; }
        public long ChallengeId { get; set; }
        public decimal Score { get; set; }
        public long Rank { get; set; }
        public string MemberName { get; set; }
        public byte[] ProfilePicture { get; set; }
        public string ProfilePicUrl { get; set; }
    }
}
