﻿using System;

namespace Vhm.Web.Models.RepositoryModels {

    [Serializable]
    public class ChallengeBase {

        public const string CHALLENGE_TYPE_PROMO = "Promo";
        public const string CHALLENGE_TYPE_QUEST = "Quest";
        public const string CHALLENGE_TYPE_CORP = "Challenge";
        public const string CHALLENGE_TYPE_CORP_SIGN_UP = "ChallengeSignUp";
        public const string CHALLENGE_TYPE_KAG = "SEC";
        public const string CHALLENGE_TYPE_TRK = "TRK";

        public long ChallengeId { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int DaysLeft { get; set; }
        public int DaysUntilStart { get; set; }
        public int? Rank { get; set; }

        public string BadgeName { get; set; }
        public string Source { get; set; }
        public DateTime? UploadDeadline { get; set; }

        public bool SignedUp { get; set; }
        public bool Started { get; set; }
        public bool Ended { get; set; }
        public bool MenuDisplay { get; set; }
        public bool ShowInBrowseOnly { get; set; }

        protected string challengeType = String.Empty;
        public string ChallengeType { get { return challengeType; } }        
        public bool IsMemberEnrolled { get; set; }
        public int ParticipantCount { get; set; }
    }
}
