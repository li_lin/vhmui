﻿using System;

namespace Vhm.Web.Models.RepositoryModels {
    public class RewardsProgram
    {
        public long ProgramId { get; set; }
        public long SponsorId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string ProgramType { get; set; }
        public string AnniversaryType { get; set; }
    }
}
