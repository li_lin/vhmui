﻿using System;

namespace Vhm.Web.Models.RepositoryModels
{
    [Serializable]
   public class Terms
    {
        public long TermsId { get; set; }
        public string TermsCode{ get; set; }
        
        public int Version{ get; set; }
       
        public bool OnlyValid { get; set; }
        public string Title { get; set; }
        public bool? Accepted { get; set; }
        public int? PreviousVersion { get; set; }
        public string Source { get; set; }
    }
}
