﻿using System;
using System.Collections.Generic;


namespace Vhm.Web.Models.RepositoryModels
{
    [Serializable]
    public class Game
    {
        public long GameId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Name { get; set; }
        public List<Milestone> Milestones { get; set; }
        //public List<Reward> Rewards { get; set; }
        public List<Reward> TriggerRewards { get; set; } 
        public long MemberTotalHealthMiles { get; set; }
        public string ProgramType { get; set; }
        public string GameDisplayType { get; set; }
        public string Currency { get; set; }
        public string CurrencyDescription { get; set; }
        public RewardsBalance AvailableRewards { get; set; }
        public long? MemberTotalActivityHealthMiles { get; set; }
        public long? MemberTotalMeasurementHealthMiles { get; set; }
        public long? MemberTotalParticipationHealthMiles { get; set; }

        public List<GameCap> Caps { get; set; }

        public List<GameTrigger> Triggers { get; set; }

    }
    public enum RewardsType
    {
        ACT,
        MEA=2,
        PAR=3
    }
}
