﻿namespace Vhm.Web.Models.RepositoryModels
{
    public class RewardBadge
    {

        public long BadgeID { get; set; }

        public string BadgeName { get; set; }

        public string BadgeDescription { get; set; }

        public string BadgeTypeName { get; set; }

        public string BadgeTypeCode { get; set; }

        public string BadgeCategoryName { get; set; }

        public string BadgeCategoryCode { get; set; }

        public bool ExtraCelebration { get; set; }

        public bool IsLocked { get; set; }

        public bool Live { get; set; }

        public byte[] BadgeImage { get; set; }

        public byte[] BadgeImage_bw { get; set; }

        public string BadgeImageString { get; set; }

    }
}
