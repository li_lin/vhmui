﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vhm.Web.Models.RepositoryModels
{
    [Serializable]
    public class CultureFormat
    {
        public short CultureID { get; set; }
        public string CultureCode { get; set; }
        public string SpecCultureCode { get; set; }
        public string Name { get; set; }
        public short? CountryID { get; set; }
        public bool Active { get; set; }
        public string MeasureSystem { get; set; }
    }
}
