﻿using System;

namespace Vhm.Web.Models.RepositoryModels {

    [Serializable]
    public class RewardsBalance
    {
        public decimal? Amount { get; set; }
        public string RewardType { get; set; }
        public int Cash { get; set; }
        public string RewardTypeDescription { get; set; }
    }
}
