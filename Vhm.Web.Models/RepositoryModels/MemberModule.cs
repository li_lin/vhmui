﻿
using System;

namespace Vhm.Web.Models.RepositoryModels {

    [Serializable]
    public class MemberModule
    {
        public long ModuleID { get; set; }
        public string ModuleTypeCode { get; set; }
        public string ModuleName { get; set; }
        public bool? OtherDevices { get; set; }
        public bool? HZLocator { get; set; }
        public short? HRASetting { get; set; }
        public string MaskModuleName { get; set; }
        public byte[] ModuleImage { get; set; }
        public string URL { get; set; }
        public bool? IsPartner { get; set; }
        public Guid? PartnerGUID { get; set; }
        public long SponsorModuleId { get; set; }
        public System.DateTime AvailableFrom
        {
            get;
            set;
        }
        public Nullable<System.DateTime> AvailableTo
        {
            get;
            set;
        }
        public Nullable<short> TimeLimitedAccessDays
        {
            get;
            set;
        }
        public string ModuleOptionList
        {
            get;
            set;
        }
        public string DeviceOrderTypeCode
        {
            get;
            set;
        }
        public string GoZoneRedeemAt
        {
            get;
            set;
        }
        public Nullable<int> FreeGZLimitByNumber
        {
            get;
            set;
        }
        public Nullable<System.DateTime> FreeGZLimitByDate
        {
            get;
            set;
        }
        public Nullable<bool> iSyncDownloadPrompt
        {
            get;
            set;
        }
        public Nullable<bool> AllowFileMeasurements
        {
            get;
            set;
        }
        public Nullable<bool> AllowAssessorMeasurements
        {
            get;
            set;
        }
        public string HRAName
        {
            get;
            set;
        }
        public bool? isPartner { get; set; }
        public string SegmentationXML { get; set; }
        public DateTime? SegmentationProcessedDate { get; set; }
        
    }
}
