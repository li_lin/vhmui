﻿using System;

namespace Vhm.Web.Models.RepositoryModels
{
    [Serializable]
    public class PromoTarget
    {
        public long PromoID { get; set; }
        public int TargetRangeMin { get; set; }
        public int TargetRangeMax { get; set; }
        public int TargetThreshold { get; set; }
        public int StretchTargetThreshold { get; set; }
    }
}
