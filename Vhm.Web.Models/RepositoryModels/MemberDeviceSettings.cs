﻿using System;

namespace Vhm.Web.Models.RepositoryModels
{
    public class MemberDeviceSettings
    {

        public bool? MidnightReset { get; set; }

        public long DeviceId { get; set; }

        public bool? EngagementMessagingEnabled { get; set; }

        public bool BumpScreenOn { get; set; }

        public byte? HrTimeFormat { get; set; }

        public long MemberID { get; set; }

        public string Gender { get; set; }

        public DateTime DateOfBirth { get; set; }

        public long? TimeZoneIdentifier { get; set; }

        public string FirstName { get; set; }

        public string NickName { get; set; }

        public bool? SSOMember { get; set; }

        public string DeviceType { get; set; }

        public string SerialNumber { get; set; }

        public DateTime RegistrationDate { get; set; }

        public int? RemoteDeviceVersionID { get; set; }

        public double? Battery { get; set; }

        public int? DailyStepGoal { get; set; }

        public decimal? Weight { get; set; }

        public decimal? Height { get; set; }

        public string MemberTime { get; set; }

        public bool MetricMeasurement { get; set; }

        public int MemberOffsetToGmt { get; set; }
    }
}
