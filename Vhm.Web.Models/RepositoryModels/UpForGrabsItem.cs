﻿using System;
using System.Collections.Generic;

namespace Vhm.Web.Models.RepositoryModels {

    [Serializable]
    public class UpForGrabsItem {

        public List<UpForGrabsRewardItem> Rewards { get; set; }

        public string UFGTaskName { get; set; }
        public decimal? UFGTaskRewardAmt { get; set; }
        public string UFGTaskRewardURL { get; set; }
        public string UFGTaskComment { get; set; } //the task description
        public long? PromoId { get; set; }
    }
}
