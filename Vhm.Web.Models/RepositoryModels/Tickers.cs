﻿using System;

namespace Vhm.Web.Models.RepositoryModels
{
    [Serializable]
    public class Tickers
    {
        public decimal AverageSteps { get; set; }

        public decimal LastWeekSteps { get; set; }

        public decimal LastWeekActMins { get; set; }

        public decimal YesterdaySteps { get; set; }

        public long? SponsorAverageSteps { get; set; }

        public long AgeAverageSteps { get; set; }

        public long GenderAverageSteps { get; set; }

        public decimal TotalHCOffer { get; set; }

        public decimal TotalHCEarn { get; set; }

        public decimal? StepPercentage { get; set; }

        public decimal? MinutePercentage { get; set; }

        public bool ShowDefaultMessage { get; set; }

    }

}
