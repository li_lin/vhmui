﻿using System;
using System.Collections.Generic;

namespace Vhm.Web.Models.RepositoryModels {

    [Serializable]
    public class RecentlyEarnedRewardsItem
    {
        public List<UpForGrabsRewardItem> Rewards { get; set; }

        public int Amount{ get; set; }
        public string RewardImage { get; set; }
        public bool IsCash { get; set; }
        public bool IsBadge { get; set; }
        public string Name { get; set; }
        public long? BadgeId { get; set; }
        public string RewardType { get; set; }
        public long? RewardTaskId { get; set; }
        public DateTime? DateEarned { get; set; }
        public DateTime? EntryDate { get; set; }
        private bool RewardTaskFeedbackPrompt { get; set; }
        private bool MemberRewardPrompt { get; set; }

        public RecentlyEarnedRewardsItem()
        {

        }
    }

    public class MockRewardItem
    {
        //CPLTaskDate CPLTaskName CPLTaskRewardAmt  CPLTaskRewardType RewardImage Cash  BadgeID      
        //RewardTaskID      RewardTaskFeedbackPrompt      MemberRewardPrompt
        public int CPLTaskRewardAmt;
        public string RewardImage;
        public bool Cash;
        public bool IsBadge;
        public string CPLTaskName;
        public long? BadgeID;
        public string CPLTaskRewardType { get; set; }
        public long? RewardTaskId { get; set; }
        public DateTime? CPLTaskDate { get; set; }
        private bool RewardTaskFeedbackPrompt { get; set; }
        private bool MemberRewardPrompt { get; set; }

        public MockRewardItem()
        {

        }

        public MockRewardItem( string name, int amount, string image, bool isCash, bool isBadge = false,
                               long? rewardTaskId = null,
                               long? badgeId = null, DateTime? dateEarned = null, bool feedbackPrompt = false,
                               bool rewardPrompt = false )
        {
            CPLTaskRewardAmt = amount;
            RewardImage = image;
            Cash = isCash;
            CPLTaskName = name;
            IsBadge = BadgeID.HasValue;
            RewardTaskId = rewardTaskId;
            BadgeID = badgeId;
            CPLTaskDate = dateEarned;
            RewardTaskFeedbackPrompt = feedbackPrompt;
            MemberRewardPrompt = rewardPrompt;
        }
    }
}
