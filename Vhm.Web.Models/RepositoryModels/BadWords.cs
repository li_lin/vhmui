﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vhm.Web.Models.RepositoryModels
{
    [Serializable]
    public class BadWords
    {
        public int BadWordID { get; set; }

        public string BadWord { get; set; }
    }
}
