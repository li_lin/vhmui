﻿using System;

namespace Vhm.Web.Models.RepositoryModels {

    [Serializable]
    public class Reward : IComparable<Reward>
    {
        public decimal Amount { get; set; }
        public string RewardType { get; set; }
        public string RewardTypeDescription { get; set; }
        public bool IsCash { get; set; }
        public long MilestoneId { get; set; }
        public long? BadgeId { get; set; }
        public string CertificateName { get; set; }
        public string ImageName { get; set; }
        public string Text { get; set; }
        public string RewardTriggerName { get; set; }
        public int CompareTo( Reward other )
        {
            return this.Amount.CompareTo( other.Amount );
        }
    }
}
