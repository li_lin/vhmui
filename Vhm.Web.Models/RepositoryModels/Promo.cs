﻿using System;
using System.Collections.Generic;

namespace Vhm.Web.Models.RepositoryModels {

    [Serializable]
    public class Promo : ChallengeBase {
        public Promo()
        {
            challengeType = CHALLENGE_TYPE_PROMO;
        }

        public List<PromoTask> Tasks { get; set; } 
    }
}
