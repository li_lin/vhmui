﻿using System;

namespace Vhm.Web.Models.RepositoryModels
{
    [Serializable]
    public class TrackingChallenge : ChallengeBase
    {
        public TrackingChallenge()
        {
            challengeType = CHALLENGE_TYPE_TRK;
        }

        public string ChallengeQuestion { get; set; }
    }
}
