﻿using System;
using System.Collections.Generic;

namespace Vhm.Web.Models.RepositoryModels
{
    [Serializable]
    public class ChallengeTask
    {

        public long ChallengeTaskID { get; set; }
        public string Description { get; set; }
        public string TriggerCode { get; set; }
        public short RewardLimit { get; set; }
        public bool Deleted { get; set; }
        public long ChallengeQuestionID { get; set; }
        public List<ChallengeTaskReward> ChallengeTaskRewards { get; set; }

    }
}
