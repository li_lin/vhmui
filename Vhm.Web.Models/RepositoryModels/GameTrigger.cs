﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vhm.Web.Models.RepositoryModels
{
    [Serializable]
    public class GameTrigger
    {
        public bool AllowTrumping { get; set; }
        public decimal Amount { get; set; }
        public long TriggerId { get; set; }
        public long? GameId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public byte AvailabilityId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool Feedback { get; set; }
        public string Link { get; set; }
        public short? Priority { get; set; }
        public bool Promote { get; set; }
        public string Segmentation { get; set; }
        public bool SendEmailConfirmation { get; set; }
        public string Source { get; set; }
        public long? TriggerCategory { get; set; }
        public string TriggerCode { get; set; }
        public List<Reward> Rewards { get; set; } 
    }
}
