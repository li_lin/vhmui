﻿namespace Vhm.Web.Models.RepositoryModels
{
    public class SponsorSystemBadge 
    {
        public long BadgeID
        {
            get;
            set;
        }
        public string BadgeName
        {
            get;
            set;
        }
        public string BadgeCategoryName
        {
            get;
            set;
        }
        public string BadgeCategoryCode
        {
            get;
            set;
        }
        public string BadgeDescription
        {
            get;
            set;
        }
        public bool? Linked
        {
            get;
            set;
        }
        public byte[] BadgeImage { get; set; }

    }
}
