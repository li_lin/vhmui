﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vhm.Web.Models.RepositoryModels
{
   
        [Serializable]
        public class GameCap
        {
            public long CapId { get; set; }
            public long RewardCategoryId { get; set; }
            public decimal Amount { get; set; }
       
    }
}
