﻿using System;

namespace Vhm.Web.Models.RepositoryModels
{
    [Serializable]
    public class Lookup
    {
        public int? LookupID { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public int? SortOrder { get; set; }
        public string Code { get; set; }
        public string Reference { get; set; }
    }
}
