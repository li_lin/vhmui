﻿using System;
using System.Globalization;

namespace Vhm.Web.Models.RepositoryModels {

    [Serializable]
    public class Challenge : ChallengeBase
    {
        public Challenge()
        {
            challengeType = (SignedUp) ? CHALLENGE_TYPE_CORP : CHALLENGE_TYPE_CORP_SIGN_UP;
        }

        public string OrdinalRank
        {
            get
            {
                if ( !Rank.HasValue )
                {
                    return string.Empty;
                }

                string rankString = Rank.Value.ToString( CultureInfo.InvariantCulture );
                string lastDigit = rankString.Substring( rankString.Length - 1, 1 );
                string suffix;
                switch ( lastDigit )
                {
                    case "1":
                        {
                            suffix = "st";
                            break;
                        }
                    case "2":
                        {
                            suffix = "nd";
                            break;
                        }
                    case "3":
                        {
                            suffix = "rd";
                            break;
                        }
                    default:
                        {
                            suffix = "th";
                            break;
                        }
                }

                return rankString + suffix;
            }
        }
    }
}
