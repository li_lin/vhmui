﻿using System;
using System.Collections.Generic;
using System.Linq;
using Reward = Vhm.Web.Models.RepositoryModels.Reward;
namespace Vhm.Web.Models.RepositoryModels {

    [Serializable]
    public class Milestone : IComparable<Milestone>
    {

        private Dictionary<string, decimal> _milestoneRewards;
 
        public string Name { get; set; }
        public decimal Value { get; set; }
        public List<Reward> Rewards { get; set; }
        public decimal RelativeProgress { get; set; }
        public decimal ActualProgress { get; set; }
        public bool IsCurrentMilestone { get; set; }
        public long MilestoneId { get; set; }

        public Dictionary<string, decimal> RewardsDictionary {
            get
            {
                if (_milestoneRewards == null || _milestoneRewards.Count == 0)
                {
                    _milestoneRewards = ( from rewards in Rewards
                                          group rewards by rewards.RewardType.Trim()
                                          into reward let firstOrDefault = reward.FirstOrDefault() where firstOrDefault != null select
                                              new
                                                  {
                                                      Key = firstOrDefault.RewardType.Trim(),
                                                      Value = reward.Sum( r => r.Amount )
                                                  } ).ToDictionary(r=>r.Key, r=>r.Value);
                }
                return _milestoneRewards;
            }
        }

        public int CompareTo(Milestone otherMilestone)
        {
            return Value.CompareTo(otherMilestone.Value);
        }


    }
}
