﻿using System;

namespace Vhm.Web.Models.RepositoryModels
{
    [Serializable]
    public class GameCurrency
    {
        public string Code { get; set; }
        public string Name { get; set; }

    }
}
