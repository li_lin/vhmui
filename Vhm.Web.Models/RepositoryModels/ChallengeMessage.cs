﻿
using System;

namespace Vhm.Web.Models.RepositoryModels
{
    [Serializable]
    public class ChallengeMessage
    {
        public long ChallengeMessageID { get; set; }
        public long ChallengeID { get; set; }
        public long? TeamID { get; set; }
        public long MemberID { get; set; }
        public string ImgSrc { get; set; }
        public string NickName { get; set; }
        public string FilteredMessageText { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedDateString { get; set; }
        public bool Deleted { get; set; }
    }
}
