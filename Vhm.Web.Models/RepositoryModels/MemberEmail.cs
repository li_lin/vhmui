﻿namespace Vhm.Web.Models.RepositoryModels
{
    public class MemberEmail
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public long? MemberID { get; set; }
        public byte[] ProfilePicture { get; set; }
        public string EmailAddress { get; set; }
    }
}
