﻿using System;

namespace Vhm.Web.Models.RepositoryModels {
    [Serializable]
    public class SponsorModuleGZVersion : IComparable
    {
        public long SponsorModuleGZVersionID { get; set; }
        public long SponsorModuleID { get; set; }
        public int RemoteDeviceVersionID { get; set; }
        public string Description { get; set; }
        public string Version { get; set; }
        public DateTime DateFrom { get; set; }
        public string ProductCode { get; set; }
        public string SupplierCode { get; set; }
        public long SupplierId { get; set; }

        #region IComparable Members

        /// <summary>
        /// return the object with the Max Version
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
 
        public int CompareTo(object obj)
        {
            var gz = (SponsorModuleGZVersion) obj;
            double versionOriginal = 0.0;
            double.TryParse(gz.Version, out versionOriginal);

            double versionThis = 0.0;
            double.TryParse( this.Version, out versionThis );
            return versionThis.CompareTo( versionOriginal );
        }

        #endregion
    }
}
