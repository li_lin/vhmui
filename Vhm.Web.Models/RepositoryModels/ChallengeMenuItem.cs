﻿using System;
using System.Runtime.Serialization;

namespace Vhm.Web.Models.RepositoryModels {

    [Serializable]
    public class ChallengeMenuItem
    {

        private string _codeName;
        private string _link;
        private string _name;
        private ChallengeItemCategory _type;
        private long _Id;
        private DateTime _startDate;
        private DateTime _endDate;
        private bool _displayInWidget;

        public long ItemId {
            get { return _Id; }
        }

        public string CodeName {
            get {
                return _codeName;
            }
        }

        public string Name {
            get {
                return _name;
            }
        }

        public string Link {
            get {
                return _link;
            }
        }

        public ChallengeItemCategory ItemType {
            get { return _type; }
        }

        public DateTime StartDate {
            get { return _startDate; }
        }

        public DateTime EndDate {
            get { return _endDate; }
        }

        public bool DisplayInWidget {
            get {
                return _displayInWidget;
            }
            set {
                _displayInWidget = value;
            }
        }

        public ChallengeMenuItem(string codeName, string name, string link, long id, ChallengeItemCategory itemType, DateTime start, DateTime end) {
            _codeName = codeName;
            _link = link;
            _name = name;
            _Id = id;
            _type = itemType;
            _startDate = start;
            _endDate = end;
        }

        public ChallengeMenuItem(string codeName, string name, string link, long id, ChallengeItemCategory itemType, DateTime start, DateTime end, bool displayInWidget) {
            _codeName = codeName;
            _link = link;
            _name = name;
            _Id = id;
            _type = itemType;
            _startDate = start;
            _endDate = end;
            _displayInWidget = displayInWidget;
        }

        public ChallengeMenuItem(SerializationInfo info, StreamingContext context)
        {
            _Id = info.GetInt64("ItemId");
            _codeName = info.GetString("CodeName");
            _name = info.GetString("Name");
            _link = info.GetString("Link");
            _type = (ChallengeItemCategory)info.GetValue("ItemType", typeof(ChallengeItemCategory));
            _startDate = info.GetDateTime("StartDate");
            _endDate = info.GetDateTime("EndDate");
            _displayInWidget = info.GetBoolean("DisplayInWidget");

        }

        #region ISerializable Members

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("ItemId", ItemId);
            info.AddValue("CodeName", CodeName);
            info.AddValue("Name", Name);
            info.AddValue("Link", Link);
            info.AddValue("ItemType", ItemType);
            info.AddValue("StartDate", StartDate);
            info.AddValue("EndDate", EndDate);
            info.AddValue("DisplayInWidget", DisplayInWidget);
        }

        #endregion
    }

    public enum ChallengeItemCategory {
        QUS = 1,
        PMO = 2,
        CHG = 3,
        DRA = 4,
        LED = 5,
        XXX = 6

    }

    public enum ChallengeStatus {
        None = 0,
        Draft = 1,
        Created = 2,
        InProgress = 4,
        ResultPending = 8,
        Complete = 16,
        Cancelled = 32
    }
}
