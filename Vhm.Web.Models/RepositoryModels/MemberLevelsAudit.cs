﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vhm.Web.Models.RepositoryModels 
{
    [Serializable]
    public class MemberLevelsAudit
    {
        public short LevelId { get; set; }
        public DateTime LevelUpDate { get; set; }
        public long RewardTaskId { get; set; }
    } 
     
}
