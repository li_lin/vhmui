﻿using System;

namespace Vhm.Web.Models.RepositoryModels {

    [Serializable]
    public class KagTrackerData {
        public decimal? TrackerValue { get; set; }
        public string TrackerDateText { get; set; }
        public string TrackerDate { get; set; }
        public bool IsToday { get; set; }
        public KagTrackerData(decimal? val, string dt, string dtText, bool today) {
            TrackerValue = val;
            TrackerDateText = dtText;
            TrackerDate = dt;
            IsToday = today;
        }
    }
}
