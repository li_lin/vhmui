﻿using System;
using System.Collections.Generic;
using Vhm.Web.Models.Enums;

namespace Vhm.Web.Models
{
    [Serializable]
    public class Session : ViewModelBase
    {
        public Session()
        {
            Permission = new SitePermissions();
        }

        public string LastPasswordChange { get; set; }

        public int LastTryWarning { get; set; }

        public int Locked { get; set; }

        public int? NAttemptsLeft { get; set; }

        public int TermsConditions { get; set; }

        public int Membership { get; set; }

        public int Criteria { get; set; }

        public DateTime? CreateDate { get; set; }

        public int SecurityLevel { get; set; }

        public int AdultAge { get; set; }

        public int TimeZone { get; set; }

        public int HSPopupReminder { get; set; }

        public int HealthSnapshot { get; set; }

        public int ItemsInCart { get; set; }

        public int CoBrandImageID { get; set; }

        public string SessionID { get; set; }

        public string MemberStatus { get; set; }

        public string CoBrandImageType { get; set; }

        public string Culture { get; set; }

        public long MemberID { get; set; }

        public long PrimarySponsorID { get; set; }

        public string IWPMemberSince { get; set; }

        public string DateOfBirth { get; set; }

        public string RewardsAniversary { get; set; }

        public DateTime? AnniversaryDate { get; set; }

        public int HMClubMember { get; set; }

        public int SuppressLevelsAnimation { get; set; }

        public int SpendVLCCash { get; set; }

        public int IsInternational { get; set; }

        public int CommunityOn { get; set; }

        public long CommunityID { get; set; }

        public string CommunityType { get; set; }

        public long CommunityGroupID { get; set; }

        public string CPR { get; set; }

        public string Accredited { get; set; }

        public string VoucherReceiver { get; set; }

        public string Member { get; set; }

        public string HealthZoneAdmin { get; set; }

        public string Junior { get; set; }

        public string LZMasterLgn { get; set; }

        public string ChallengeManager { get; set; }

        public string BioAdministrator { get; set; }

        public string Online { get; set; }

        public string ECoach { get; set; }

        public string Parent { get; set; }

        public string BioAssessor { get; set; }

        public string ItemsinCart { get; set; }

        public string Practical { get; set; }

        public string EmployerReport { get; set; }

        public string ElqCustomerGUID { get; set; }

        public string Age { get; set; }

        public int LanguageID { get; set; }

        public string EmployerReportAdmin { get; set; }

        public string Trainer { get; set; }

        public string OnlineReporting { get; set; }

        public string HealthClubFinder { get; set; }

        public string HZMeasurement { get; set; }

        public string BusinessPartnerReport { get; set; }

        public string BusinessPartnerAdmin { get; set; }

        public int TestTaken { get; set; }
     
        public SitePermissions Permission { get; set; }

        public List<SponsorPerks> SponsorPerks { get; set; }
        //extra
        public LoginStatus LoginStatus { get; set; }

        public bool NeedHealthSnapShot { get; set; }
    }

    [Serializable]
    public class SitePermissions
    {
        public bool CrmAdmin { get; set; }
        public bool TrainerAdmin { get; set; }
        public bool Member { get; set; }
        public bool MeasurementAdministrator { get; set; }
        public bool MeasurementAssessor { get; set; }
        public bool VoucherReceiver { get; set; }
        public bool OnlineReporting { get; set; }
        public bool LifeZoneMasterLogin { get; set; }
        public bool ECoach { get; set; }
        public bool ChallengeManager { get; set; }
        public bool HealthZoneAdmin { get; set; }
        public bool Champion { get; set; }
    }

    [Serializable]
    public class SponsorPerks
    {
        public long PerkID { get; set; }
        public long EntityID { get; set; }
        public decimal HealthClubSupplement { get; set; }
        public string DeviceOrderTypeCode { get; set; }
        public bool IsyncDownload { get; set; }
        public long? FreeGoZonesTo { get; set; }
        public DateTime? FreeGoZonesTill { get; set; }
        public bool SpendVLCCash { get; set; }
        public bool CustomPeriod { get; set; }
        public string StartSpendPeriod { get; set; }
        public string EndSpendPeriod { get; set; }
        public int? GoZonesAllocated { get; set; }
        public bool PersonalChallenges { get; set; }
        public short HSCompletion { get; set; }
        public bool? AllowChat { get; set; }
        public string GoZoneRedeemAt { get; set; }
        public bool SuppressHCLocator { get; set; }
        public bool CheckFulfillAllowed { get; set; }
        public bool SuppressFALocator { get; set; }
        public string CurrencyCode { get; set; }
        public bool NoSmokingAgreementOff { get; set; }
        public bool SuppressHZLocator { get; set; }
        public bool SuppressRebateCtr { get; set; }
        public decimal ShippingCost { get; set; }
        public bool SuppressSelfFA { get; set; }
        public bool SuppressLevelsAnimation { get; set; }
        public bool SuppressPartnersSection { get; set; }
        public bool AllowElectronicChecks{ get; set; }
        public bool GZDiscountOn { get; set; }
        public decimal GZDiscount1 { get; set; }
        public decimal GZDiscount2 { get; set; }
        public bool SuppressSelfMsrmts { get; set; }
        public bool? AllowMeasurementAssessors { get; set; }
        public bool? AllowFileMeasurements { get; set; }
        public bool AllowMobileAppLogin { get; set; }
        public string HealthSnapShotName { get; set; }
        public bool AllowPasswordReuse { get; set; }
        public int PasswordExpireDays { get; set; }
        public string PasswordStrength { get; set; }
        public string PasswordRegEx { get; set; }
        public long PasswordRegExID { get; set; }
    }
}
