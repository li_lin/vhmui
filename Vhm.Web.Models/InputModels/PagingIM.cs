﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vhm.Web.Models.InputModels
{
    public class PagingIM
    {

        public long MemberId { get; set; }

        public int? PageNumber { get; set; }

        public int? Count { get; set; }
    }
}
