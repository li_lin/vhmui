﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace Vhm.Web.Models.InputModels
{
        public class GroupIM
        {
                [File(AllowedFileExtensions = new string[] { ".jpg", ".gif", ".png",}, MaxContentLength = 1024 * 1024 * 8, ErrorMessage = "Invalid File")]
                public HttpPostedFileBase Image { get; set; }
                public Stream ImageStream { get; set; }
                public long Id { get; set; }
                public string Name { get; set; }
                public string Description { get; set; }
                public byte? CategoryId { get; set; }
                public List<long> InvitedMembers { get; set; }
                public string Privacy { get; set; }
                public string PageMode { get; set; }
                public GroupIM()
                {
                        InvitedMembers = new List<long>();
                }
        }

        public class GroupIMValidateAttribute : ValidationAttribute
        {
                public override bool IsValid(object value)
                {
                        if (value == null)
                                return true;

                        var groupIM = (GroupIM)value;

                        switch (groupIM.PageMode)
                        {
                                case "Create":
                                        if (string.IsNullOrEmpty(groupIM.Name))
                                                return false;
                                        if (string.IsNullOrEmpty(groupIM.Description))
                                                return false;
                                        if (groupIM.CategoryId == null || groupIM.CategoryId == 0)
                                                return false;
                                        if (string.IsNullOrEmpty(groupIM.Privacy))
                                                return false;
                                        break;
                                case "Edit":
                                        if (groupIM.Id == 0)
                                                return false;
                                        if (string.IsNullOrEmpty(groupIM.Name))
                                                return false;
                                        if (string.IsNullOrEmpty(groupIM.Description))
                                                return false;
                                        if (groupIM.CategoryId == null || groupIM.CategoryId == 0)
                                                return false;
                                        if (string.IsNullOrEmpty(groupIM.Privacy))
                                                return false;
                                        break;
                                case "Create_1":
                                        if (groupIM.Id == 0)
                                                return false;
                                        break;
                                default:
                                        return false;
                                       
                        }
                        return true;
                }

                private bool ValidateGroup(GroupIM groupIM)
                {

                        return true;
                }
        }
}