﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vhm.Web.Models.ContentEnums
{
    /// <summary>
    /// This is reflecting the "Create a Challenge – Individuals or Teams" page in Ektron, ID: 2147483728
    /// </summary>
    public enum AboutAndCreateChallengeContent
    {
        CreateChallenge = 1,
        WhoWillCompete = 2, //This was retired - should we remove??
        IndividualPlayersOnly = 3,
        AllowPlayersHeadToHead = 4,//This was retired - should we remove??
        IndividualPlayersAndTeams = 5,
        AllowPlayersTeamsIndividually = 6,//This was retired - should we remove??
        //Labels 7 to 11 are Not Used - this is coming from CRM on the website and will be retired along with Personal Know and Go release
        Exer_DoUploadSteps = 7, 
        Exer_DoStepActivity = 8,
        Exer_DoTakeItEasy = 9,
        Exer_DoPlayRules = 10,
        Exer_DoEncourage = 11,
        AboutChallsTopMessage = 12,
        AboutChallsActivityChallenge = 13,
        AboutChallsActivityChallengeMessage = 14,
        AboutChallsHealthyChallenges = 15,
        AboutChallsHealthyChallMessage = 16,
        AboutChallsHealthyChallLetsGo = 17,
        AboutChallsIChallengeYouTo = 18, //I challenge you to
        AboutChallsChallengeOne = 19, //SWAP YOUR MORNING DIET COKE FOR GREEN TEA
        AboutChallsChallengeTwo = 20, //BROWN-BAG IT FOR LUNCH
        AboutChallsChallengeThree = 21, //RESIST RECEPTION DESK CANDY
        AboutChallsChallengeFour = 22, //TAKE THE STAIRS (NO ELEVATORS!)
        AboutChallsChallengeFive = 23, //KEEP UP WITH THE LUNCHTIME WALK GROUP
        AboutChallsEveryDayTwoWeeks = 24, //every day for two weeks
        AboutChallsEveryDayOneWeek = 25, //every day for a week
        AboutChallsTwoPiecesPerDayOneWeek = 26, //keep it to two pieces per day for one week 
        AboutChallsThreeTimesWeek = 27, //three times this week
        AboutChallsFewIdeas = 28, //a few ideas...
        WhatIsChallengeQuestion = 29,
        DidYouEatDonutsToday = 30,
        WhatIsTargetAnswer = 31,
        MoreThan = 32,
        LessThan = 33,
        HowLongIsChallenge = 34,
        AnyDetailsYouWantToAdd = 35,
        AnyRulesPrizes = 36,
        ThisIsJustForMe = 37,
        InviteFriends = 38,
        Optional = 39
    }
}
