﻿namespace Vhm.Web.Models.ContentEnums
{
    public enum SelectChallengeContent
    {
        CreateAChallenge = 1,
        Choose = 2,
        Activity = 3,
        Nutrition = 4,
        WellBeing = 5,
        SeeOneYouLike = 6,
        CreateDeviceChallenge = 7,
        CreateQuestionchallenge = 8,
        CreateButtonText = 9
    }
}
