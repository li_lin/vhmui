﻿using System;
using System.Globalization;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.Models.ViewModels.Measurments {
    [Serializable]
    public class MeasurmentSummaryVM {
        public MeasurementReward Reward { get; set; }
        public DateTime? LastMeasurementDate { get; set; }
        public CultureInfo CurrentCulture { get; set; }
        public bool HasHZDesktopAccess { get; set; }
        public string AwardNumberFormat { get; set; }
        public bool RewardEarnedThisMonth { get; set; }
    }
}
