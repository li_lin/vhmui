﻿namespace Vhm.Web.Models.ViewModels.Measurments {
    public enum MeasurementSummaryContent {
        TakeMeasurements = 1,
        LastMeasurement = 2,
        NotTakenMeasurements = 3,
        TakenMeasurements = 4
    }
}
