﻿

namespace Vhm.Web.Models.ViewModels.Measurments
{
    public enum MeasurementLabelsContent
    {
        Ft = 1,
        In = 2,
        Lbs = 3,
        Kg = 4,
        Cm = 5,
        Height = 6,
        Weight = 7,
        Stone = 8
    }
}
