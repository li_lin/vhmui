﻿using System;
using System.Collections.Generic;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.Models.ViewModels.Shared
{
    [Serializable]
    public class HeaderViewModel : ViewModelBase
    {
        public MainMenuItem MainMenu { get; set; }
        //VirginHealth miles brand
        public string VhmLogoUrl { get; set; } //hold the VHM Logo Url for VHM as main brand
        public bool ShowBroughtToYouBy { get; set; } // can be true only if VHM main brand & sponsorLogo not null in database
        public string SponsorBroughtToYouByUrl { get; set; } // the brought to you by Url for "Co-brand logo", if LogoType is an image
        public string SponsorBroughtToYouByText { get; set; } // the brought to you by text for "Co-brand logo", if logoType is text
        public string SponsorDefaultBroughtToYouByText { get; set; } // the brought to you by text for "Co-brand logo", if logoType is text
        public string SponsorBroughtToYouByLogoType { get; set; } // the logo type if ShowBroughtToYouBy is true
        //Custom Brand properties
        public string SponsorImageUrl { get; set; } //if UseCustomBrand - the Sponsor Logo (brought to you by Url), if LogoType is an image
        public string SponsorText { get; set; } //if UseCustomBrand - the Sponsor Logo (brought to you by Url), if logoType is text
        public string SponsorLogoType { get; set; } //the logo type
        //commomn properies for branding
        public bool UseCustomBrand { get; set; }
        
        //other properties
        public long MemberId { get; set; }
        public string MemberFullName { get; set; }
        public int UnreadNotificationCount { get; set; }
        public bool UseSponsorText { get; set; }
        public SitePermissions Permissions { get; set; }
        public string MemberImageUrl { get; set; }
        public bool ShowSiteNavigationMenu { get; set; }
        public Dictionary<HeaderContent, string> Content { get; set; }
        public bool ShowMemberDeviceSettingsLink { get; set; }
        public bool ShowBetaMenuUI { get; set; }

        public List<EktronMenuItem> EktronMenu = new List<EktronMenuItem>();

        public HeaderViewModel()
        {
            MainMenu = new MainMenuItem();
            VhmLogoUrl = string.Empty;
            ShowBroughtToYouBy = false;
            SponsorBroughtToYouByUrl = string.Empty;
            SponsorBroughtToYouByText = string.Empty;
            SponsorBroughtToYouByLogoType = string.Empty;
            SponsorImageUrl = string.Empty;
            SponsorText = string.Empty;
            SponsorLogoType = string.Empty;
            UseCustomBrand = false;
            ShowMemberDeviceSettingsLink = false;
            ShowBetaMenuUI = false;

        }
    }
}
