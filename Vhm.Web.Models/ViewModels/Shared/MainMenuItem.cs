﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.Models.ViewModels.Shared {

    [Serializable]
    public class MainMenuItem {

        public List<MainMenuItem> ChildItems { get; set; }
        public string Url { get; set; }
        public string Text { get; set; }
        public int NavWidth { get; set; }
        public int NavCategoryId { get; set; }
        public bool Root { get; set; }

        public MainMenuItem(string url, string txt, int width, int id)
        {
            ChildItems = new List<MainMenuItem>();
            Url = url;
            Text = txt;
            NavWidth = width;
            NavCategoryId = id;
        }

        public MainMenuItem()
        {
            ChildItems = new List<MainMenuItem>();
        }

        public MainMenuItem(SerializationInfo info, StreamingContext context)
        {
            Url = info.GetString("Url");
            NavWidth = info.GetInt32("NavWidth");
            Text = info.GetString("Text");
            NavCategoryId = info.GetInt32("NavCategoryId");
            Root = info.GetBoolean("Root");
            ChildItems = info.GetValue("ChildItems", typeof(List<MainMenuItem>)) as List<MainMenuItem>;

        }

        #region ISerializable Members

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Url", Url);
            info.AddValue("NavWidth", NavWidth);
            info.AddValue("Text", Text);
            info.AddValue("NavCategoryId", NavCategoryId);
            info.AddValue("ChildItems", ChildItems);
            info.AddValue("Root", Root);
        }
        #endregion
    }
}
