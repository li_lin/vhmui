﻿namespace Vhm.Web.Models.ViewModels.Shared
{
    public enum MiscellaneousLabelsContent
    {
        Package = 1,
        Confirm = 2,
        Step = 3,
        AllDone = 4,
        VHM = 5,
        MembershipAgreement = 6,
        SocialTerms = 7,
        SiteMap = 8,
        Next = 9,
        Format = 10,
        Dates = 11,
        Measurements = 12,
        Running = 13,
        Description = 14,
        Like = 15,
        Completing = 16,
        WhatIsThis = 17,
        All = 18,
        HealthZone = 19,
        IAgree = 20,
        Previous = 21,
        LogOff = 22,
        Done = 23,
        Promotional = 24,
        Back = 25,
        Language = 26,
        SelectLanguage = 27,
        LogOut = 28,
        StartsToday = 29
    }
}