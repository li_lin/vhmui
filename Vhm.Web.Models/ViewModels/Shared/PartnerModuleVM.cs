﻿using System;

namespace Vhm.Web.Models.ViewModels.Shared {

    [Serializable]
    public class PartnerModuleVM : ViewModelBase {
        public PartnerModule Module { get; set; }
    }
}
