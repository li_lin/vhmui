﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vhm.Web.Models.ViewModels.Shared
{
    public class PageTitle
    {
        public PageTitle(bool bShowPageTitle = true, string sPageName = "", bool bShowRightLink = false, string sRghtLnkText = "")
        {
            this.PageTitleName = sPageName;
            ShowPageTitle = bShowPageTitle;
            ShowRightTitleLink = bShowRightLink;
            RightLinkText = sRghtLnkText;
        }

        public bool ShowPageTitle { get; set; }
        public string PageTitleName { get; set; }
        public bool ShowRightTitleLink { get; set; }
        public string RightLinkText { get; set; }
    }
}
