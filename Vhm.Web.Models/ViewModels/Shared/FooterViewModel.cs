﻿using System.Collections.Generic;
namespace Vhm.Web.Models.ViewModels.Shared
{
    public class FooterViewModel
    {
        public string CompanyName { get; set; }
        public MainMenuItem FooterMenu { get; set; }
        public MainMenuItem MainMenu { get; set; }
        public bool ShowPoweredByVhm { get; set; }
        public Dictionary<FooterContent, string> Content { get; set; }

        public FooterViewModel()
        {
            FooterMenu = new MainMenuItem();
            MainMenu = new MainMenuItem();
        }
    }
}
