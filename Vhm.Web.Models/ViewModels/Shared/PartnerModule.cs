﻿using System;

namespace Vhm.Web.Models.ViewModels.Shared
{
    [Serializable]
    public class PartnerModule
    {
        public string MaskModuleName { get; set; }
        public string ModuleImageSrc { get; set; }
        public string URL { get; set; }
        public bool? IsPartner { get; set; }
        public Guid? PartnerGUID { get; set; }
        public PageTitle PageTitle { get; set; }
        public long SponsorModuleId { get; set; }
    }
}
