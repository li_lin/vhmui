﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vhm.Web.Models.ViewModels.Shared
{
    public class PagingDataVM
    {
        public bool HasMoreItems { get; set; }
        public int TotalNumberOfItems { get; set; }
        public int CurentPageNumber { get; set; }
        public int TotalNumberOfPages { get; set; }
        public int PageSize { get; set; }
        public PagingDataVM ()
        {
            HasMoreItems = false;
            TotalNumberOfItems = 0;
            CurentPageNumber = 0;
            TotalNumberOfPages = 1;
            PageSize = 0;
        }
    }
}
