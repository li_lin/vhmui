﻿namespace Vhm.Web.Models.ViewModels.Device
{
    public enum DeviceMemberSettingsContentLabels
    {
        ClockShouldDisplay = 1,
        TwelveHours = 2,
        TwentyFourHours = 3,
        TurnBumpChallenges = 4,
        ResetDisplay = 5,
        AutomaticallyMidnight = 6,
        DoItManually = 7,
        TurnEncouragingMessaging = 8,
        ChangesHaveBeenSaved = 9,
        ChangesWontApear = 10,
        LoadingDeviceSettings = 11
    }
}
