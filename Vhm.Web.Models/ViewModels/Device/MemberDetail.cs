﻿namespace Vhm.Web.Models.ViewModels.Device
{
    public class MemberDetail
    {
        public string SerialNumberWithDashes { get; set; }
        public string NickName { get; set; }
        public long DeviceId { get; set; }
        public bool MetricMeasurement { get; set; }
    }
}
