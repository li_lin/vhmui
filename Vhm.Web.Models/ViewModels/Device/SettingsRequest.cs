﻿namespace Vhm.Web.Models.ViewModels.Device
{
    public class SettingsRequest
    {
        public long MemberId { get; set; }
        public string Nickname { get; set; } //pass here when updating MemberSettings as it can't be reached when at the memberData level
        public bool DistanceDisplayMetric { get; set; } //pass here when updating MemberSettings as it can't be reached when at the memberData level
        public MemberSettings MemberSettings { get; set; }
    }
}
