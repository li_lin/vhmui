﻿namespace Vhm.Web.Models.ViewModels.Device
{
    public class VhmMemberMaxSettings
    {
        public bool EngagementMessagingOn { get; set; }
        public bool MidnightReset { get; set; }
        public byte HrTimeFormat { get; set; }
        public bool DistanceDisplayMetric { get; set; } //DistanceUnitMode - 0 for Metric; 1 for Imperial
        public bool BumpScreenOn { get; set; }
        public bool SyncScreenOn { get; set; } //SyncScreenMode - 1 for On; 0 for Off=disable screen
        public bool CalDistScreenOn { get; set; } //CalDistScreenMode - 1 for On; 0 for Off=disable screen
        public bool SunScreenOn { get; set; } //SunScreenMode - 1 for On; 0 for Off=disable screen
        public bool ClockScreenOn { get; set; } //ClockScreenMode - 1 for On; 0 for Off=disable screen
        public long MemberId { get; set; }
        public string Nickname { get; set; }
        public MemberDetail MemberData { get; set; }
    }
}
