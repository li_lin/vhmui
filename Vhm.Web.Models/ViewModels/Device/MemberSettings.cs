﻿using System;

namespace Vhm.Web.Models.ViewModels.Device
{
    public class MemberSettings
    {
        public long BumpChallangeId { get; set; }
        public int? DailyStepGoal { get; set; }
        public byte HrTimeFormat { get; set; }
        public bool MidnightReset { get; set; }
        public MemberDetail MemberData { get; set; }
        public UInt16 BaseScreensToDisable { get; set; }
        public UInt32 ScenarioScreensToDisable { get; set; }
    }
}
