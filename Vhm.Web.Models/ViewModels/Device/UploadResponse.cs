﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vhm.Web.Models.ViewModels.Device
{

    public class Activity
    {
        public string Date { get; set; }
        public double Steps { get; set; }
        public int ActiveMinutes { get; set; }
        public double Calories { get; set; }
        public int Distance { get; set; }
    }

    public class BumbpChallenge
    {
        public int BumpChallengeID { get; set; }
        public int BumpChallengeCreator { get; set; }
        public int MemberID { get; set; }
    }

    public class UploadResponse
    {
        public string UploadData { get; set; }
        public int ErrorCode { get; set; }
        public long LastUploadTimeStamp { get; set; }
        public string DeviceToken { get; set; }
        public object UploadDataString { get; set; }
        public Activity Activity { get; set; }
        public List<BumbpChallenge> BumbpChallenges { get; set; }
    }
}
