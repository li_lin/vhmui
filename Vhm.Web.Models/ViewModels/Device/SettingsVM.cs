﻿using System.Collections.Generic;
using Vhm.Web.Models.ViewModels.Shared;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.Models.ViewModels.Device
{
    public class SettingsVM : ViewModelBase
    {
        public PageTitle PageT { get; set; }
        public string MemberDeviceSettingsPath { get; set; }
        public Dictionary<DeviceMemberSettingsContentLabels, string> Content;
        public Dictionary<CommonLabelsContent, string> CommonContent;
        public SponsorModule SponsorModule { get; set; }
    }
}
