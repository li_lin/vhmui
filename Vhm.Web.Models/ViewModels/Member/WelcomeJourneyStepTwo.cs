﻿using System.Collections.Generic;
using Vhm.Web.Models.ViewModels.Measurments;
using Vhm.Web.Models.ViewModels.Shared;

namespace Vhm.Web.Models.ViewModels.Member
{
    public class WelcomeJourneyStepTwo
    {
        public int WJStep { get; set; }
        public int WJSeed { get; set; }
        public int WJMaxNumber { get; set; }
        public int WJCurrentStep { get; set; }
        public short CultureId { get; set; }
        public string CultureInfo { get; set; }
        public string MeasurementSystem { get; set; }
        public Dictionary<MeasurementLabelsContent, string> ContentMeasurementLabels;
        public Dictionary<MiscellaneousLabelsContent, string> MiscContent; 

        public WelcomeJourneyStepTwo()
        {
            WJStep = 1;
            WJSeed = 1;
            WJMaxNumber = 5;
            WJCurrentStep = 1;
            CultureId = -1;
            CultureInfo = string.Empty;
            MeasurementSystem = "Imperial";
        }
    }
}
