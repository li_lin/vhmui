﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vhm.Web.Models.ViewModels.Member
{
    public enum WelcomeJourneyContent
    {
        WelcomeToVhm = 1,
        StepTwoFirstMessage = 2,
        StepTwoSubtitle = 3,
        StepTwoSecondMessage = 4,
        StepTwoTip = 5,
        StepFourFirstMessage = 6,
        StepFourSecondMessage = 7,
        StepFiveMessage = 8,
        StepFiveTakeMeThere = 9,
        StepFiveRemindLater = 10,
        StepFiveDontRemind = 11,
        StepFiveTip = 12,
        StepOneIntroMessage = 13,
        StepOneNote = 14, 
        StepTwoEnterHeight = 15,
        StepTwoEnterWeight = 16,
        StepTwoHeightRequirements = 17,
        StepTwoWeightRequirements = 18,
        StepTwoSelectOptionMessage = 19,
        StepOneSubtitle = 20,
        //StepOneInroMessage = 21, //same as 13
        StepFiveSUbtitle = 22,
        //StepFiveSUbtitle = 23,//same as 13
    }

    public enum WelcomeJourneyContentLabels
    {
        EditPrivacySettings = 1,
        ProfileSearchability = 2,
        ShowGender = 3,
        ShowAge = 4,
        ShowTotalSteps = 5,
        ShowAverageStepsPerDay = 6,
        ShowLastGoZoneUploadDate = 7,
        ShowChallenges = 8,
        Public = 9,
        Private = 10,
        DoNotShow = 11,
        ShowBadges = 12,
        ShowOrganization = 13,
        ShowGoals = 14,
        ShowInterests = 15,
        ShowOccupation = 16,
        Step1of3 = 17,
        Step2of3 = 18,
        Step3of3 = 19
    }
}
