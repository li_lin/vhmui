﻿using System.Collections.Generic;
using Vhm.Web.Models.ViewModels.Shared;

namespace Vhm.Web.Models.ViewModels.Member
{
    public class WelcomeJourneyStepThree
    {
        public int WJStep { get; set; }
        public int WJSeed { get; set; }
        public int WJMaxNumber { get; set; }
        public int WJCurrentStep { get; set; }
        public string CommunityType { get; set; }
        public Dictionary<MiscellaneousLabelsContent, string> MiscContent; 

        public WelcomeJourneyStepThree()
        {
            WJStep = 1;
            WJSeed = 1;
            WJMaxNumber = 5;
            WJCurrentStep = 3;
            CommunityType = "OPN";
        }
    }
}
