﻿using System.Collections.Generic;
using Vhm.Web.Models.ViewModels.Shared;

namespace Vhm.Web.Models.ViewModels.Member
{
    public class WelcomeJourneyStepFive
    {
        //public Dictionary<WelcomeJourneyContent, string> Content { get; set; }
        public int WJStep { get; set; }
        public int WJSeed { get; set; }
        public int WJMaxNumber { get; set; }
        public int WJCurrentStep { get; set; }
        public bool IsHraOptional { get; set; }
        public bool IsHraMandatory { get; set; }
        public Dictionary<MiscellaneousLabelsContent, string> MiscContent;
        public WelcomeJourneyStepFive()
        {
            WJStep = 1;
            WJSeed = 1;
            WJMaxNumber = 5;
            WJCurrentStep = 5;
            IsHraOptional = false;
            IsHraMandatory = false;
        }
    }
}
