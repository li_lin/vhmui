﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vhm.Web.Models.ViewModels.Shared;

namespace Vhm.Web.Models.ViewModels.Member
{
    public class NoSmokingViewModel : ViewModelBase
    {
        public PageTitle PageTitle { get; set; }
        public Dictionary<CommonLabelsContent, string> CommonContent { get; set; }
        public Dictionary<NoSmokingContent, string> ContentNoSmoking { get; set; }
        public Dictionary<MiscellaneousLabelsContent, string> ContentMisc { get; set; }
        public bool AlreadyAgreed { get; set; }
        public bool NonSmokerOn { get; set; }
        public bool NoIntentToQuit { get; set; }
        public bool IntentToQuit { get; set; }
        public bool CurrentSmokerOn { get; set; }
        public string HraName { get; set; }
        public bool PageAccessible { get; set; }
        public long MemberId { get; set; }
    }
}
