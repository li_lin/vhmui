﻿using System.Collections.Generic;
using Vhm.Web.Models.RepositoryModels;
using Vhm.Web.Models.ViewModels.Shared;

namespace Vhm.Web.Models.ViewModels.Member
{
    public class WelcomeJourneyStepOne
    {
        public int WJStep { get; set; }
        public int WJSeed { get; set; }
        public int WJMaxNumber { get; set; }
        public int WJCurrentStep { get; set; }
        public short CultureId { get; set; }
        public long LanguageId { get; set; }
        public string CultureInfo { get; set; }
        public List<CultureFormat> CultureFormats { get; set; }
        public List<SponsorLanguage> SponsorLanguages { get; set; }
        public Dictionary<MiscellaneousLabelsContent, string> MiscContent;

        public WelcomeJourneyStepOne()
        {
            WJStep = 1;
            WJSeed = 1;
            WJMaxNumber = 5;
            WJCurrentStep = 1;
            CultureId = 86; //default to "en-US"
            CultureInfo = string.Empty;
        }
    }
}
