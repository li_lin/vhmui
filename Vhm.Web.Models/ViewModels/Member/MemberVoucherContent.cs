﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vhm.Web.Models.ViewModels.Member
{
    public enum MemberVoucherContent
    {
        Title = 1,
        FirstMessage = 2,
        EnterVoucher = 3,
        ErrorMessage = 4,
        ErrorMessageContactUs = 5,
        WhatsVoucher = 6,
        VoucherInfo = 7,
        NoSpecialOffers = 8,
        ErrorMessageNotEnoughCharacters = 9
    }
}
