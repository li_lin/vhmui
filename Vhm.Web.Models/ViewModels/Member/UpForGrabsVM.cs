﻿using System.Collections.Generic;
using System.Globalization;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.Models.ViewModels.Member {
    public class UpForGrabsVM {
        public List<UpForGrabsItem> Rewards { get; set; }
        public string BadgeImageUrl { get; set; }
        public string RewardTypeImageUrl { get; set; }
        public string ImageCDN { get; set; }
        public CultureInfo CurrencyCulture { get; set; }
       
    }
}
