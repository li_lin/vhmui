﻿using System.Collections.Generic;
using System.Globalization;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.Models.ViewModels.Member {
    public class GameDisplayGraphVM {
        public string ImageCDN { get; set; }
        public Game CurrentGame { get; set; }
        public long CurrentProgress { get; set; }
        public int CurrentStep { get; set; }
        public List<Reward> RewardsByType { get; set; }
        public int DaysLeft { get; set; }
        public Dictionary<string, string> RewardsImageLookup { get; set; }
        public Dictionary<string, decimal> RewardsAvailableSummary { get; set; }
        public Dictionary<string, decimal> RewardsEarnedSummary { get; set; }
        public RewardsBalance AvailableRewards { get; set; }
        public List<Reward> OtherRewards { get; set; }
        public bool AllowSpendHealthCash { get; set; }
        //for PRG - to be used to determine the completion percentage of total game
        public decimal RelativeProgressPercentage { get; set; }
        //public decimal MilestonePercentage { get; set; }
        public CultureInfo SponsorCultureFormat { get; set; }

        // for PRG - display the current available reward for the current milestone
        public Reward MilestoneRewardDisplay { get; set; }

        // Game Display Graph messaging flags
        public bool ShowNoTasksInProgressContent { get; set; }
        public bool ShowNoTasksToCompleteContent { get; set; }
        public double OnTrackToEarnAmount { get; set; }
        public double AmountToNextLevel { get; set; }
        public decimal AvialableHealthCash { get; set; }
        public bool IsTickerAvailable { get; set; }
    }

    public class LapsGameDisplayVM
    {
        public string ImageCDN { get; set; }
        public Dictionary<string, string> RewardsImageLookup { get; set; }
        public Dictionary<string, decimal> RewardsAvailableSummary { get; set; }
        public Dictionary<string, decimal> RewardsEarnedSummary { get; set; }
        public List<Reward> RewardsByType { get; set; }
        public List<Reward> OtherRewards { get; set; }
        public bool AllowSpendHealthCash { get; set; }
        public Game CurrentGame { get; set; }
        public long CurrentProgress { get; set; }
        public int CurrentStep { get; set; }
        public int DaysLeft { get; set; }
        public int GameLengthInDays { get; set; }
        public int DaysSpentSinceChallangeStarted { get; set; }
        public CultureInfo SponsorCultureFormat { get; set; }
        public double AmountToNextLevel { get; set; }
        public decimal AvailableCashReward { get; set; }
        public RewardsBalance AvailableRewards { get; set; }
        public decimal AvialableHealthCash { get; set; }
        public bool IsTickerAvailable { get; set; }
        public bool IsSponsorInternational { get; set; }
    }

	public class PersonalChallengesCarouselVM
	{
		public class Leader
		{
			public int Rank;
			public bool IsLoggedInUser;
			public string MemberName;
			public string ScoreSummary;
			public string ProfilePicUrl;
		}

		public class Reward
		{
			public string Name { get; set; }
			public int? Value { get; set; }
			public string Url { get; set; }
		}

		public enum AnswerType
		{
			Integer,
			YesNo
		}

		public class Challenge
		{
			public int ChallengeID { get; set; }
			public string Question { get; set; }
			public int ScoreID { get; set; }
			public AnswerType AnswerType { get; set; }
			public int? MinAnswer { get; set; }
			public int? MaxAnswer { get; set; }
			public int? DefaultAnswer { get; set; }
			public int NumPlayers;
			public IList<Leader> Leaders { get; set; }
			public IList<Reward> Rewards { get; set; }
		}

		public int NumUserEntries { get; set; }
		public IList<Challenge> Challenges { get; set; }
	}
}
