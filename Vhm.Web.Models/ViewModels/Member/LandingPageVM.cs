﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Vhm.Web.Models.ViewModels.Measurments;
using Vhm.Web.Models.ViewModels.Shared;
using Vhm.Web.Models.RepositoryModels;
using Vhm.Web.Models.ViewModels.Social;

namespace Vhm.Web.Models.ViewModels.Member {
    public class LandingPageVM : ViewModelBase {
        public char MemberGender;
        public long MemberId;
       // public Dictionary<LandingPageContent, string> Content { get; set; }
       // public Dictionary<KagContent, string> KnowAndGoContent { get; set; } 
        public PageTitle PageTitle { get; set; }
        public List<RecentlyEarnedRewardsItem> RecentlyEarnedRewards { get; set; }
        public List<RecentlyEarnedRewardsItem> PopupRewards { get; set; }
        public List<RecentlyEarnedRewardsItem> RecentlyEarnedRewardsNoBadges { get; set; }
        public List<ChallengeBase> Challenges { get; set; }
        public Dictionary<string, string> ChallengeTypes { get; set; }
        public byte ChallengeEndedDisplayDays { get; set; }
        public ActivityGraphData ActivityData { get; set; }
        public int ActivityTargetPosition { get; set; }
        public bool MeasurementsEnabled { get; set; }
        public bool ActivityEnabled { get; set; }
        public bool SelfEnteredActivityEnabled { get; set; }
        public bool ShowPickDevicePrompt { get; set; }
        public bool BadgesEnabled { get; set; }
        public bool NutritionEnabled { get; set; }
        public bool SelfTrackingChallengesEnabled { get; set; }
        public int MaxSummaryRewards { get; set; }
        public decimal RecentRewardsAmountTotal { get; set; }
        public NewsFeedVM NewsFeed { get; set; }
        public MeasurmentSummaryVM MeasuermentsSummary { get; set; }
        public CultureInfo CurrentCulture { get; set; }
        public CultureInfo CurrencyCulture { get; set; }
        public Dictionary<LandingPageModelErrors, string> ModelErrors { get; set; }
        public List<MemberModule> Modules { get; set; }
        public List<UpForGrabsItem> UpForGrabsRewards { get; set; } 
        //ShowWelcomeJourney true if new member, false otehrwise. 
        public bool ShowWelcomeJourney { get; set; }
        public int? WelcomeJourneyStep { get; set; }
        public int MaxWelcomeJourneySteps { get; set; }
        public string WelcomeJourneyTitle { get; set; }
        public string ImageCDN { get; set; }
        public string BadgeImageUrl { get; set; }
        public string RewardTypeImageUrl { get; set; }
        /// <summary>
        /// 0 = "Optional, not prompted", 1 = "Optional, but prompted", 2 = "Mandatory, after Welcome Screens", 3 = "Mandatory, before Welcome Screens"
        /// -1 = "Off" - Retired
        /// </summary>
        public short HRASetting { get; set; }
        public bool ConnectionsEnabled { get; set; }
        // Show pops
        public bool ShowPopUpForMemebershipStatusChanged { get; set; }
        public bool ShowPopUpForCreditCardInfo { get; set; }
        public bool ShowPopUpForBillingInfo { get; set; }
        public bool ShowPopUpForTermsPage { get; set; }
        public bool ShowPopUpForTermsToAcknowledge { get; set; }
        public long TermsId { get; set; }
        public string Title { get; set; }
        public string RewardDisplay { get; set; }
        
        public bool HasProgramsModules { get; set; }
        public List<PartnerModule> PartnerModules { get; set; }
        public Tickers TickersData { get; set; }
        public bool PersonalChallengesEnabled { get; set; }

        public bool ShowNewLandingModal { get; set; }
        public bool ShowExpressWelcomeJourney { get; set; }
        public enum LandingPageModelErrors
        {
            Challenges,
            Activity,
            Rewards,
            Connections,
            Measurements,
            Programs,
            Content,
            Badges,
            Modules
        }
        public string ProgramType { get; set; }
        public bool BetaEnabled { get; set; }
        public bool? genesisUI { get; set; }
        public MemberBadge LevelCelebration { get; set; }
    }
}
