﻿using System.Collections.Generic;
using Vhm.Web.Models.ViewModels.Shared;
using Vhm.Web.Models.ViewModels.Social;

namespace Vhm.Web.Models.ViewModels.Member
{
    public class WelcomeJourneyStepFour
    {
        public int WJStep { get; set; }
        public int WJSeed { get; set; }
        public int WJMaxNumber { get; set; }
        public int WJCurrentStep { get; set; }
        public MemberVM MemberProfile { get; set; }
        public Dictionary<MiscellaneousLabelsContent, string> MiscContent;
        public Dictionary<WelcomeJourneyContentLabels, string> WJContent;
        public Dictionary<CommonLabelsContent, string> CommonContent;

        public WelcomeJourneyStepFour()
        {
            WJStep = 1;
            WJSeed = 1;
            WJMaxNumber = 5;
            WJCurrentStep = 4;
            MemberProfile = new MemberVM();
        }
    }
}
