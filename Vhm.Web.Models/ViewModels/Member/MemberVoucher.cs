﻿using System;
using System.Collections.Generic;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.Models.ViewModels.Member
{
    [Serializable]
    public class MemberVoucher
    {
       // public Dictionary<MemberVoucherContent, string> Content { get; set; }
        public Voucher Voucher { get; set; }
    }
}
