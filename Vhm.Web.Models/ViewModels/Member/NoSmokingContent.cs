﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vhm.Web.Models.ViewModels.Member
{
    public enum NoSmokingContent
    {
        TobacoFreeAgreement = 1,
        YouToldUsInYourHRANameThatYo_reementBelow = 2,
        NoMatterWhetherYouReCurrent_hmilesMember = 3,
        YouToldUsInYourHealthSnapsho_hmilesMember = 4,
        TobaccoIsThePrimaryCauseOfMa_rsonalHealth = 5,
        YouVeDeclaredYourselfATobac_hmilesMember = 6,
        YouPromiseToBeTobaccoFreeDur_ewardsPeriod = 7,
        IDontWantToPromiseYet = 8,
        YoureNotReadyToBeTobaccoFree_eMeasureMenu = 9,
        YouMustMakeASelectionBefore_TheAgreement = 10,
        YouToldUsInYourVal0That_Val1Member = 11
    }
}
