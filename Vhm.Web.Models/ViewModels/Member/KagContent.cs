﻿namespace Vhm.Web.Models.ViewModels.Member {
    public enum KagContent
    {
        ShareMyResults = 1,
        TrackingStartsTomorrow = 2,
        TrackingStartsNumberDays = 3,
        Today = 4,
        AveragePerDay = 5,
        AveragePerWeek = 6,
        DaysTracked = 7,
        WeeksTracked = 8,
        GoodDays = 9,
        TopNotchDays = 10,
        Exer_CheckOut = 11,
        Exer_UpToNumber = 12,
        FirstEntry = 13,
        HalfWayHere = 14,
        Exer_ShowRewards = 15,
        TopChallengers = 16,
        Track = 17,
        Earn = 18,
        Achieve = 19,
        NumberLikeThis = 20,
        Exer_YouAndOne = 21,
        Exer_YouAndNumber = 22,
        Exer_ForStarting = 23,
        Exer_ForSpreading = 24,
        Exer_CompleteHalfwayThere = 25,
        Exer_ForCompleting = 26,
        Exer_JustEarned = 27,
        YesDays = 28,
        NoDays = 29,
        Exer_SeeMoreActivity = 30,
        Exer_SeeMoreBioMeasurement = 31,
        Exer_SorryYouCanOnlyTrack = 32,
        CurrentWeek = 33,
        You = 34,
        Others = 35,
        Exer_EarnBadge = 36,
        Exer_TheresReward = 37,
        Exer_RemindYourFriends = 38,
        Exer_WeWantToKnow = 39,
        Done = 40,
        Exer_YouHaveCompleted = 41,
        Start = 42,
        Finish = 43,
        More = 44,
        Less = 45,
        WayToGo = 46,
        ThisWeek = 47,
        Week = 48,
        Day = 49,
        Days = 50,
        TellAFriend = 51,
        Post = 52,
        Good = 53,
        TopNotch = 54,
        KagYes = 55,
        KagNo = 56,
        ThanksForParticpating = 57
    }
}
