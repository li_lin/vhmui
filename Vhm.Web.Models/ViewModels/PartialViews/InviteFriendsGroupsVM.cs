﻿namespace Vhm.Web.Models.ViewModels.PartialViews
{
    public class InviteFriendsGroupsVM
    {
        public string HeaderText { get; set; }

        public bool HasSelectAll { get; set; }

        public int TotalPageCount { get; set; }

        public bool HasViewAll { get; set; }

        public bool HasAlphabet { get; set; }



    }
}
