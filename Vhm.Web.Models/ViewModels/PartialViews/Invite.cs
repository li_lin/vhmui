﻿using System.Collections.Generic;

namespace Vhm.Web.Models.ViewModels.PartialViews
{
   public  class InviteVM
    {
       public InviteFriendsGroupsVM InviteFriends { get; set; }
       public InviteFriendsGroupsVM InviteGroups { get; set; }
       public List<long> SelectedMembers { get; set; }

       public  InviteVM()
       {
           this.InviteFriends=new InviteFriendsGroupsVM();
           this.InviteGroups=new InviteFriendsGroupsVM();
           this.SelectedMembers=new List<long>();
       }
    }
}
