﻿using System.Collections.Generic;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.Models.ViewModels.Social
{
    public class GroupVM
    {
        #region Private properties
        private string _strId;
        private string _sGrpId;
        #endregion
        #region Public Properties
        /// <summary>
        /// isPartOfTheGroup should return true if logged memebr is part of this group or false otherwise - this will help for the redirections/buttons properties and determining if we show public or private profile of the group to the member
        /// </summary>
        public bool IsPartOfTheGroup { get; set; }
        public bool IsLoggedMemberTheAdmin { get; set; }
        //If Name and image are cklickable
        public bool IsClickable { get; set; }
        public string PrivateViewURL { get; set; }
        public Group Group { get; set; }
        public string ImageSrc { get
        {
            if (string.IsNullOrEmpty(this._strId))
                return this.Group.Id.ToString();
            else
                return this._strId;
        }
            set { this._strId = value; }
        }
        public int GroupMembersCount { get; set; }
        public System.Collections.Generic.List<MemberVM> GroupMembers { get; set; }
        public string ShortGroupName { get; set; }
        public bool isInvitedToGroup { get; set; }
        public string sGroupId {
            get
            {
                if (string.IsNullOrEmpty(this._sGrpId))
                    return this.Group.Id.ToString();
                else
                    return this._sGrpId;
            }
            set { this._sGrpId = value; }
        }
        //this will be described by the SocialEnum GroupRelation. We'll set the default to be member. We'll set this property only if needed.
        public string MemberGroupRelation { get; set; }
        #endregion
        public GroupVM ()
        {
            IsPartOfTheGroup = false;
            IsLoggedMemberTheAdmin = false;
            IsClickable = true;
            Group = new Group();
            isInvitedToGroup = false;
            GroupMembersCount = 0;
            GroupMembers = new List<MemberVM>();
            ShortGroupName = string.Empty;
            MemberGroupRelation = Enums.GroupRelation.MEMBER;
        }
    }
}
