﻿namespace Vhm.Web.Models.ViewModels.Social {
    public enum NewsFeedContent {
        HaveJustJoined = 1,
        HaveJustEarned = 2,
        YouAndNPeopleLike = 3,
        NPeopleLike = 4,
        YouLikeThis = 5,
        OnePersonLikes = 6,
        NothingPosted = 7,
        Unlike = 8,
        Like = 9,
        HasJustEarned = 10,
        SureRemoveAsFriend = 11,
        Comment = 12,
        Comments = 13, 
        Yes = 14,
        Cancel = 15,
        NewsFeedMessage = 16,
        Post = 17
    }
}
