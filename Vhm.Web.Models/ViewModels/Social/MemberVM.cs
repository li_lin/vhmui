﻿using System;
using System.Globalization;
using Vhm.Web.Models.RepositoryModels;
using Vhm.Web.Models.Enums;

namespace Vhm.Web.Models.ViewModels.Social
{
    public class MemberVM : IEquatable<MemberVM>
    {
        private string _stringId;

        public RepositoryModels.Member Member { get; set; }

        // TODO: This doesn't seem right... What is the point of this?
        public string ImageSrc
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_stringId))
                {
                    return Member.MemberId.ToString( CultureInfo.InvariantCulture );
                }
                return _stringId;
            }
            set 
            { 
                _stringId = value; 
            }
        }
        public bool ShowAge { get; set; }
        public bool ShowBadges { get; set; }
        public bool ShowChallengeHistory { get; set; }
        public bool ShowCity { get; set; }
        public bool ShowGender { get; set; }
        public bool ShowGoals { get; set; }
        public bool ShowLastGoZoneUpload { get; set; }
        public bool ShowNickname { get; set; }
        public bool ShowInterests { get; set; }
        public bool ShowOccupation { get; set; }
        public bool ShowOrganization { get; set; }
        public bool ShowStepsTotal { get; set; }
        public bool ShowStepsAverage { get; set; }
        public ViewSize ViewSize { get; set; }
        public MemberRelationships RelationShip { get; set; }
        public CultureInfo Culture { get; set; }
        public int ProfileExists { get; set; }
        public bool PrivacySettingsSet { get; set; }
        public bool Privacy { get; set; }
        public MemberVM()
        {
            ViewSize=ViewSize.Small;
        }

        public override int GetHashCode()
        {
            return Member.MemberId.GetHashCode();
        }

        public bool Equals(MemberVM other)
        {
            return Member.MemberId == other.Member.MemberId;
        }
    }
}
