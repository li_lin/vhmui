﻿namespace Vhm.Web.Models.ViewModels.Social {
    public enum CreateGroupContent {
        CreateGroup = 1,
        GroupName = 2,
        Image = 3,
        UploadAnImageToRepresentYourGroup = 4,
        Privacy = 5,
        Public = 6,
        Private = 7,
        TextIsAlreadyCreated = 8,
        TextIsAvailable = 9,
        NameShouldBeMoreThan3Characters = 10,
        NameIsRequired = 11,
        CategoryIsRequired = 12,
        DescriptionIsRequired = 13,
        DeleteThisGroup = 14,
        EditGroup = 15,
        AnyoneCanSearchForAndJoinThisGroup = 16,
        OnlyInvitedMembersCanJoinThisGroup = 17,
        Category = 18,
        Description = 19



    }
}
