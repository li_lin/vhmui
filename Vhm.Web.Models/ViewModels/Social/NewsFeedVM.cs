﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Vhm.Web.Models.RepositoryModels;
using Vhm.Web.Models.ViewModels.Shared;

namespace Vhm.Web.Models.ViewModels.Social {
    [Serializable]
    public class NewsFeedVM : ViewModelBase {
        public List<NewsfeedPost> NewsFeed { get; set; }
        public string NewsfeedMessage { get; set; }
        public CultureInfo CurrentCulture { get; set; }
        public string MemberImage { get; set; }
        public string NewsfeedUrl { get; set; }
        public Dictionary<NewsFeedContent, string> Content { get; set; }
        public Dictionary<NewsFeedModelErrors, string> ModelErrors { get; set; }
        public PageTitle PageTitle { get; set; }
        public enum NewsFeedModelErrors {
            NewsFeed
        }
    }
}
