﻿using System.Collections.Generic;
using Vhm.Web.Models.RepositoryModels;
using Vhm.Web.Models.ViewModels.Shared;

namespace Vhm.Web.Models.ViewModels.Social
{
    public class MyGroupsModel : ViewModelBase
    {
        public PageTitle PageT { get; set; }
        public bool HasGroupInvitations { get; set; }
        public string GroupInvitationsMessage { get; set; }
        public List<GroupVM> SuggestedGroups { get; set; }
        public List<GroupCategory> AllGroupCategory { get; set; }
        public PagingDataVM MyGroupsPagination { get; set; }
        public PagedGroupsVM PagedGroups { get; set; }
        public string JsonMemberGroupsNames { get; set;}
        public string JsonMemberFriendsNames { get; set; }
        public Dictionary<MyGroupsContent,string> Content {get; set;}
        public Dictionary<SocialChallengePromotionLabelsContent, string> ContentSocial { get; set; }
        public MyGroupsModel ()
        {
            PageT = new PageTitle();
            HasGroupInvitations = false;
            GroupInvitationsMessage = string.Empty;
            SuggestedGroups = new List<GroupVM>();
            AllGroupCategory = new List<GroupCategory>();
            MyGroupsPagination = new PagingDataVM();
            PagedGroups = new PagedGroupsVM();
            JsonMemberGroupsNames = string.Empty;
            JsonMemberFriendsNames = string.Empty;
        }
    }
    //use this model class for any time we need to render groups applying pagination
    public class PagedGroupsVM
    {
        public List<GroupVM> Groups { get; set; }
        public PagingDataVM GroupsPaginationInfo { get; set; }
        public string PagingGroupsSource { get; set; }
        public PagedGroupsVM ()
        {
            Groups = new List<GroupVM>();
            GroupsPaginationInfo = new PagingDataVM();
        }
    }

    public class PendingGroupInvitation
    {
        // the GroupId is NotificationTypeEntityId from MemberNotification Table
        public MemberNotification NotificationInfo { get; set; }
        public GroupVM GroupInfo { get; set; }
        public PendingGroupInvitation ()
        {
            NotificationInfo = new MemberNotification();
            GroupInfo = new GroupVM();
        }
    }
}
