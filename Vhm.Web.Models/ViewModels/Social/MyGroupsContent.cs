﻿namespace Vhm.Web.Models.ViewModels.Social {
    public enum MyGroupsContent {

        YouAreNotAMemberOfAnyGroupsS_reateYourOwn = 1,
        YouAreAMemberOfNumberGroups = 2,
        NumberGroupInvitations = 3,
        SearchForGroups = 4,
        ByGroupName = 5,
        ByAdministrator = 6,
        ByCategory = 7,
        CreateANewGroup = 8,
        SuggestedGroups = 9,
        NumberMatchesWereFoundForText = 10,
        BackToMyGroups = 11,
        PendingGroupInvitations = 12,
        Accept = 13,
        Decline = 14,
        InvitationAccepted = 15,
        WereLoadingYourGroups = 16,
        LoadingYourGroups = 17,
        SorryTheGroupForThisInvitat_sBeenRemoved = 18,
        YouAreAMemberOf1Group = 19,
        YouAreAnAdministratorOfThisGroup = 20
    }
}
