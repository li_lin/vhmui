﻿namespace Vhm.Web.Models.ViewModels.Social {
    public enum CGYourGroupHasBeenCreatedContent {

        YourGroupHasBeenCreated = 1,
        EnterTheNames_GroupWhenYoureF_eenCreated = 2,
        NumberTotal = 3,
        SendInvites = 4,
        All = 5,
        None = 6,
        SelectFriendsToInviteToThisGroup = 7,
        SelectOtherGroupsToInviteToThisGroup = 8,
        EnterTheEmailAddressesSepara_lonOrReturns = 9,
        PleaseNoteTheMoreInviteesOn_TakeToUpload = 10,
        ThanksForYourPatience = 11,
        WeCouldNotMatchOneOrMoreOfT_hmilesMember = 12,
        YouAreAdministratorForThisGroup_toDeleteThisGroup = 13
    }
}
