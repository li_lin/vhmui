﻿namespace Vhm.Web.Models.ViewModels.Social {
    public enum FeedbackContent {
        BeTheFirst = 1,
        Title = 2,
        YourRating = 3,
        Comments = 4,
        Like = 5,
        UnLike = 6,
        NumberLikesThis = 7,
        OneLikesThis = 8,
        Done = 9,
        AverageRating = 10
    }
}
