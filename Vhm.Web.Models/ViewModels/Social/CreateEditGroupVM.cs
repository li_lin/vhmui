﻿using System.Collections.Generic;
using Vhm.Web.Models.ViewModels.PartialViews;
using Vhm.Web.Models.ViewModels.Shared;
using Vhm.Web.Models.RepositoryModels;


namespace Vhm.Web.Models.ViewModels.Social {
    public class CreateEditGroupVM : ViewModelBase{
        public GroupVM GroupVM { get; set; }
        public InviteVM Invite { get; set; }
        public List<GroupCategory> Categories { get; set; }
        public string Mode { get; set; }
        public Shared.PageTitle PageTitle { get; set; }
        public Dictionary<CreateGroupContent, string> ContentCreateGroup { get; set; }
        public Dictionary<CGYourGroupHasBeenCreatedContent, string> ContentGroupCreated { get; set; }
        public Dictionary<SocialChallengePromotionLabelsContent, string> ContentSocial { get; set; }
        public Dictionary<ErrorMessagesContent, string> ContentError { get; set; }
        public Dictionary<CommonLabelsContent, string> ContentCommon { get; set; }
        public CreateEditGroupVM()
        {
            GroupVM = new GroupVM();
            Invite = new InviteVM();
            Categories = new List<GroupCategory>();
            PageTitle = new PageTitle();
        }
    }
}
