﻿using System;

namespace Vhm.Web.Models.ViewModels.CmsContent
{
    public class OldLandingTwoByTwo
    {
        public string Title { get; set; }
        public string Summary { get; set; }
        public string BackgroundImage { get; set; }
    }
}
