﻿using System;

namespace Vhm.Web.Models.ViewModels.CmsContent
{
    public class BlogArticle
    {
        public string Title { get; set; }
        public string Summary { get; set; }
        public string FullText { get; set; }
        public string Author { get; set; }
        public string ImgUrl { get; set; }
        public long Id { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
