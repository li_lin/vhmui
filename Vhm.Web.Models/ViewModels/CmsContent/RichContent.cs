﻿using System;

namespace Vhm.Web.Models.ViewModels.CmsContent
{
    public class RichContent
    {
        public string Title { get; set; }
        public string Html { get; set; }
        public long Id { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
