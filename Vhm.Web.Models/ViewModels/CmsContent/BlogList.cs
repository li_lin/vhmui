﻿using System;
using System.Collections.Generic;

namespace Vhm.Web.Models.ViewModels.CmsContent
{
    public class BlogList
    {
        public List<BlogArticle> Articles = new List<BlogArticle>();
    }
}
