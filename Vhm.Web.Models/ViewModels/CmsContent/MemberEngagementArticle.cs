﻿using System;

namespace Vhm.Web.Models.ViewModels.CmsContent
{
    public class MemberEngagementArticle
    {
        public string Title { get; set; }
        public string Summary { get; set; }
        public string FullText { get; set; }
        public string Author { get; set; }
        public string PreviewImageUrl { get; set; }
        public string FullArticleImageUrl { get; set; }
        public string GoogleAnalyticsTrackingCode { get; set; }
        public long Id { get; set; }
        public DateTime DateCreated { get; set; }
        public string ThemeColor { get; set; }
        public string CustomLinkText { get; set; }
        public string CustomLinkUrl { get; set; }
        public string HideReadMoreLink { get; set; }
    }
}
