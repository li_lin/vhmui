﻿using System;

namespace Vhm.Web.Models.ViewModels.CmsContent
{
    public class HomepageBanner
    {
        public string RichHtml { get; set; }
    }
}
