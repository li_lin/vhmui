﻿namespace Vhm.Web.Models.ViewModels.Rewards {
    public class BadgeDetails {
        public long BadgeId { get; set; }
        public string Name { get; set; }
    }
}
