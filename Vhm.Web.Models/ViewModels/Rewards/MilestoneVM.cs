﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.Models.ViewModels.Rewards {
    public class MilestoneVM {

        public MilestoneVM(){}

        private Dictionary<string, decimal> _milestoneRewards;
        private Milestone _milestone;

        public MilestoneVM(Milestone milestone)
        {
            _milestone = milestone;
        }

        public Dictionary<string, decimal> MilestoneRewards
        {
            get { return _milestoneRewards; }
        }
    }
}
