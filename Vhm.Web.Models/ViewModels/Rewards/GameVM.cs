﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.Models.ViewModels.Rewards {
    
    [Serializable]
    public class GameVM
    {
        private Game _game;
        public GameVM()
        {
            
        }

        public GameVM(Game game)
        {
            _game = game;
        }
    }
}
