﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vhm.Web.Models.ContentEnums;

namespace Vhm.Web.Models.ViewModels.Challenges
{
    public class AboutChallengesVM: ViewModelBase
    {
        public Dictionary<AboutAndCreateChallengeContent, string> Content { get; set; }
    }
}
