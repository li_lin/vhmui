﻿using System.Collections.Generic;
using System.Xml.Linq;

namespace Vhm.Web.Models.ViewModels.Challenges
{
    public static class ChallengeSetup
    {
        static ChallengeSetup()
        {
            if (Configurations == null)
            {
                Configurations = new List<SetupConfig>();
                //Load("ChallengeSetup.xml");
            }
        }

        public static List<SetupConfig> Configurations { get; set; }

        public static void Load(string xml)
        {
            XDocument config = XDocument.Parse(xml);

            foreach (XElement node in config.Descendants("ChallengeSetup"))
            {
                foreach (XElement challengeConfig in node.Elements("SetupConfig"))
                {
                    Configurations.Add( new SetupConfig
                        {
                            CategoryKey = challengeConfig.Attribute("CategoryKey").Value,
                            AnswerType = challengeConfig.Attribute( "AnswerType" ).Value,
                            Question = challengeConfig.Attribute( "Question" ).Value,
                            Description = challengeConfig.Attribute( "Description" ).Value,
                            Target = !string.IsNullOrEmpty( challengeConfig.Attribute( "Target" ).Value )
                                         ? int.Parse( challengeConfig.Attribute( "Target" ).Value )
                                         : 0,
                            MoreLess = challengeConfig.Attribute( "MoreLess" ).Value,
                            Duration = challengeConfig.Attribute( "Duration" ).Value
                        } );
                }
            }
        }
    }
}