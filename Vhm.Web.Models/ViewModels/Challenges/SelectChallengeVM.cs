﻿using System.Collections.Generic;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.Models.ViewModels.Challenges
{
    public class SelectChallengeVM
    {
        public Dictionary<string, string> Content { get; set; }
        public List<RewardBadge> Badges { get; set; }
        public string ImageURLBase { get; set; }
    }
}
