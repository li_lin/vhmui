﻿using System.Collections.Generic;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.Models.ViewModels.Challenges
{
    public class CreateChallengeVM
    {
        public Dictionary<string, string> Content { get; set; }
        public List<RewardBadge> Badges { get; set; }
        public SetupConfig ChallengeConfig { get; set; }
        public string CDNPath { get; set; }
    }
}
