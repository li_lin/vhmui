﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vhm.Web.Models.ViewModels.Challenges
{
    public class ChallengeScore
    {
        public long ChallengeScoreID
        {
            get;
            set;
        }

        public long ChallengeMemberID
        {
            get;
            set;
        }

        public long MemberId
        {
            get;
            set;
        }

        public long ChallengeId
        {
            get;
            set;
        }

        public decimal? Value
        {
            get;
            set;
        }

        public decimal Score
        {
            get;
            set;
        }

        public System.DateTime ScoreDate
        {
            get;
            set;
        }

        public System.DateTime UpdatedDate
        {
            get;
            set;
        }

        public bool IsPermanent
        {
            get;
            set;
        }
    }
}
