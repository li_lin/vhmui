﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.Models.ViewModels.Challenges
{
    public class PersonalChallenges : ViewModelBase
    {
        public long ChallengeId { get; set; }
        public string Name { get; set; }
        public string RulesCopy { get; set; }
        public List<ChallengeQuestion> ChallengeQuestions { get; set; }
        public object TrackedDaysInfo { get; set; }
        public bool HasFinished { get; set; }
        public bool HasStarted { get; set; }
        public string BadgeFileName { get; set; }
    }
}
