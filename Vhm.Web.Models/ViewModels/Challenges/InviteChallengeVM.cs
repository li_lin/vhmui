﻿using System.Collections.Generic;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.Models.ViewModels.Challenges
{
    public class InviteChallengeVM
    {
        public Dictionary<string, string> Content { get; set; }

        public SetupConfig ChallengeConfig { get; set; }
        public string CDNPath { get; set; }
        public bool CanInvite { get; set; }
        public string OwnerType { get; set; }
        public string ChallengeName { get; set; }
    }
}
