﻿using System.Collections.Generic;
using Vhm.Web.Models.Enums;
using Vhm.Web.Models.RepositoryModels;
using Vhm.Web.Models.ViewModels.Shared;
using Vhm.Web.Models.ViewModels.Social;

namespace Vhm.Web.Models.ViewModels.Challenges
{
    public class PersonalKnowAndGoVM: ViewModelBase
    {
        
        public Dictionary<SocialChallengePromotionLabelsContent, string> ContentSocial { get; set; }
        public Dictionary<CommonLabelsContent, string> ContentCommon { get; set; }
        public Dictionary<string, string> ContentRankings { get; set; }
        public string MemberImage { get; set; }
        public long ChallengeId { get; set; }
        public PersonalTrackingChallenge Challenge { get; set; }
        public List<ChallengeMessage> Chat { get; set; }
        public List<ChallengeMemberScore> LeaderBoard { get; set; }
        public object TrackedDaysInfo { get; set; }
        public long Rank { get; set; }
        public string ViewerName { get; set; }
        public string BadgeFileName { get; set; }
        public int DaysToDisplay { get; set; }
        public bool IsChallengeCreator { get; set; }
        public string FrequencyType { get; set; }
    }
}
