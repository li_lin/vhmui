﻿namespace Vhm.Web.Models.ViewModels.Challenges {
    public class KAGCompleteVM {
        public bool IsCashReward;
        public int ResponseType;
        public string TaskType;
        public string KAGTitle { get; set; }
        public string RewardImage { get; set; }
        public string BadgeImage { get; set; }
        public decimal RewardAmount { get; set; }
        public bool ShowOthers { get; set; }
        public bool ShowAverage { get; set; }
        public bool ShowDaysTracked { get; set; }
        public bool ShowTargetDays { get; set; }
        public bool ShowStretchDays { get; set; }
        public bool ShowYesDays { get; set; }
        public bool ShowNoDays { get; set; }
        public int TrackingTimeSpan { get; set; }
        public int TotalTrackableDays { get; set; }
        public int PersonalDaysTracked { get; set; }
        public int ChallengeDaysTracked { get; set; }
        public int PersonalTargetDays { get; set; }
        public int ChallengeTargetDays { get; set; }
        public int PersonalStretchDays { get; set; }
        public int ChallengeStretchDays { get; set; }
        public int PersonalYesDays { get; set; }
        public int ChallengeYesDays { get; set; }
        public int PersonalNoDays { get; set; }
        public int ChallengeNoDays { get; set; }
        public decimal PersonalAverage { get; set; }
        public decimal ChallengeAverage { get; set; }
        public string RewardImageUrl { get; set; }
    }
}
