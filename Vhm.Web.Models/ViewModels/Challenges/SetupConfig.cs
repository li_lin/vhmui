﻿namespace Vhm.Web.Models.ViewModels.Challenges
{
    public class SetupConfig
    {
        public string CategoryKey { get; set; }
        public string Question { get; set; }
        public string Description { get; set; }
        public string AnswerType { get; set; }
        public string MoreLess { get; set; }
        public int Target { get; set; }
        public string Duration { get; set; }
        public long BadgeId { get; set; }

    }
}
