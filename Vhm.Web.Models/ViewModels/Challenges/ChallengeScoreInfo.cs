﻿using System.Collections.Generic;

namespace Vhm.Web.Models.ViewModels.Challenges
{
    public class ChallengeScoreInfo
    {
        public List<ChallengeScore> ChallengeScores { get; set; }

        public int ScoreCount { get; set; }
    }
}
