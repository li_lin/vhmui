﻿using System.Collections.Generic;
using Vhm.Web.Models.ViewModels.Shared;

namespace Vhm.Web.Models.ViewModels.Activity
{
    public class DevicesVM: ViewModelBase
    {
        public bool IsMemberRegisterToGZ { get; set; }
        public bool IsMemberLinkedToPolar { get; set; }
        public PageTitle PageT { get; set; }
        public string RunKeeperURL { get; set; }
        public bool IsRunKeeperConnected { get; set; }
        public string FitbitURL { get; set; }
        public bool IsFitbitConnected { get; set; }
        public Dictionary<DeviceContent, string> Content { get; set; }
        public object RegisterGoZoneURL { get; set; }
        public object GZSupportURL { get; set; }
        public object DownloadGZURL { get; set; }
        public string ManageYourAccountURL { get; set; }
        public string LearnMoreAboutPolar { get; set; }
        public object BuyPolarURL { get; set; }
        public object BuyGoZoneURL { get; set; }
        public string SyncSoftwareURL { get; set; }
        public string HowToConnectRK { get; set; }
        public string HowToConnectFitBit { get; set; }
        public bool ShowDevicesFitbitSection { get; set; }
        public string ConnectFitbitThroughRunkeeperPdf { get; set; }
        public bool IsDashAppOn { get; set; }
        public string LearMoreAboutHowThisWork{get;set;}
        public bool ShowDevicesEndomondoSection { get; set; }
        public string EndomondoURL { get; set; }
        public bool IsEndomondoConnected { get; set; }
        public string VendorConnectResponseStatus { get; set; }
        public bool ShowDevicesMisfitSection { get; set; }
        public bool IsMisfitConnected { get; set; }
        public string MisfitConnectURL { get; set; }
        public string HowToConnectMisfit { get; set; }
        public string HowToConnectEndomondo { get; set; }
    }
}
