﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ektron.Cms.Framework.User;
using Ektron.Cms;

public partial class CMSLogin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        UserManager userMngr = new UserManager();
        Response.Write(userMngr.UserId.ToString());
    }

    protected void uxLogin_click(object sender, EventArgs e)
    {
        UserManager userMngr = new UserManager();
        UserData user = userMngr.Login(uxUserName.Text, uxPassword.Text);

        Response.Write("Welcome " + user.DisplayName);
    }
}