﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="CMSLogin" Codebehind="CMSLogin.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
     <h2>
        Log In
    </h2>
    <p>
        Please enter your username and password.
    </p>

    
    <asp:Label ID="lbluser" AssociatedControlID="uxUserName" runat="server">User name</asp:Label>
    <asp:TextBox ID="uxUserName" runat="server"></asp:TextBox>
    <asp:Label ID="Label1" AssociatedControlID="uxUserName" runat="server">User name</asp:Label>
    <asp:TextBox ID="uxPassword" TextMode="Password" runat="server"></asp:TextBox>

    <asp:Button ID="uxLogin" runat="server" OnClick="uxLogin_click" text="Login"/>
    </form>
</body>
</html>
