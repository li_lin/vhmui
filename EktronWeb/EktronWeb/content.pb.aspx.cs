﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Ektron.Cms;
using Ektron.Cms.PageBuilder;
using System.Collections.Generic;

namespace Ektron.Site
{
    public partial class Content : PageBuilder
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            
        }
      

        #region helpers

        public override void Error(string message)
        {
            jsAlert(message);
        }
        public override void Notify(string message)
        {
            jsAlert(message);
        }
        public void jsAlert(string message)
        {
            try
            {
                Literal lit = new Literal();
                lit.Text = "<script type=\"\" language=\"\">{0}</script>";
                lit.Text = string.Format(lit.Text, "alert('" + message + "');");
                Form.Controls.Add(lit);
            }
            catch (Exception)
            {
            }
        }

        #endregion
    }
}