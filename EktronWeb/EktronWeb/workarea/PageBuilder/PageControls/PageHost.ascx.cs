using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Ektron.Cms;
using Ektron.Cms.Common;
using Ektron.Cms.Framework.UI;
using Ektron.Cms.Localization;
using Ektron.Cms.PageBuilder;
using Ektron.Cms.Framework.Content;
using Ektron.Cms.Framework.Organization;


public partial class PageHost : System.Web.UI.UserControl
{
    string appPath; //path to workarea

    protected EkRequestInformation _requestInformation = null;
    Ektron.Cms.Common.EkMessageHelper _messageHelper;

    protected EkRequestInformation RequestInformation
    {
        get
        {
            if (_requestInformation == null)
            {
                _requestInformation = ObjectFactory.GetRequestInfoProvider().GetRequestInformation();
            }
            return _requestInformation;
        }
    }


    protected Ektron.Cms.Common.EkMessageHelper m_refMsg
    {
        get
        {
            if (_messageHelper == null)
            {
                _messageHelper = new EkMessageHelper(RequestInformation);
            }
            return _messageHelper;
        }
    }

    public Double CacheInterval
    {
        get { return (Page as PageBuilder).CacheInterval; }
        set { (Page as PageBuilder).CacheInterval = value; }
    }

    private long _folder = -1;
    public long FolderID
    {
        get
        {
            if (PageID > 0)
            {
                // always verify folder ID unless user doesn't give us a page ID
                // because user can give us a bogus folder ID
                long pagefolderid = GetFolderIdForContentId(PageID);
                if (_folder != pagefolderid)
                {
                    _folder = pagefolderid;
                }
            }
            return _folder;
        }
        set { _folder = value; }
    }

    private long _defaulttaxid = -1;
    public long SelTaxonomyID
    {
        get { return _defaulttaxid; }
        set { _defaulttaxid = value; }
    }

    public long DefaultPageID
    {
        get { return (Page as PageBuilder).DefaultPageID; }
        set { (Page as PageBuilder).DefaultPageID = value; }
    }

    public long PageID
    {
        get { return (Page as PageBuilder).Pagedata.pageID;; }
    }

    public int LangID
    {
        get { return RequestInformation.ContentLanguage; }
    }

    private string _pagePath = "";
    public string PagePath
    {
        get
        {
            if (_pagePath == "")
            {
                _pagePath = GetPathByFolderID(FolderID);
            }
            return _pagePath;
        }
    }

    public string ThemeName
    {
        get { return (Page as PageBuilder).Theme; }
        set { (Page as PageBuilder).Theme = value; }
    }

    #region Init, Load

    protected void Page_Init(object sender, EventArgs e)
    {
        if (ScriptManager.GetCurrent(Page) == null)
        {
            ScriptManager sMgr = new ScriptManager();
            this.Controls.AddAt(0, sMgr);
        }

        appPath = RequestInformation.ApplicationPath.TrimEnd(new char[] { '/' });

        Packages.EktronCoreJS.Register(this);

        this.EnableViewState = false;

    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
      
    }

    #endregion


    protected void RegisterFiles()
    {
        Ektron.Cms.Framework.UI.Packages.jQuery.Plugins.Cookie.RegisterJS(this);
    }

    #region helpers

    ContentManager _contentManager;
    FolderManager _folderManager;

    protected ContentManager ContentManager
    {
        get{
            if (_contentManager == null)
            {
                _contentManager = new ContentManager();
            }

            return _contentManager;
        }
    }

    protected FolderManager FolderManager
    {
        get
        {
            if (_folderManager == null)
            {
                _folderManager = new FolderManager();
            }

            return _folderManager;
        }
    }


    protected long GetFolderIdForContentId(long pageId)
    {
        ContentData content = ContentManager.GetItem(pageId);
        return content.FolderId;
    }

    protected string GetPathByFolderID(long folderID)
    {
        FolderData folder = FolderManager.GetItem(folderID);
        return folder.NameWithPath;
    }

    protected bool IsLoggedIn
    {
        get { return ContentManager.UserId > 0; }
    }

    #endregion
}
