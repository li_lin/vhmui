<%@ Control Language="C#" AutoEventWireup="true" Inherits="PageHost" Codebehind="PageHost.ascx.cs" %>


<asp:Literal ID="sessionKeepalive" runat="server" Visible="false" EnableViewState="false">
    <script type="text/javascript" language="JavaScript">
        function sessionKeepAlive() {
            var wRequest = new Sys.Net.WebRequest();
            wRequest.set_url("<pagepostback>");
            wRequest.set_httpVerb("POST");
            wRequest.add_completed(sessionKeepAlive_Callback);
            wRequest.set_body("Message=keepalive");
            wRequest.get_headers()["Content-Length"] = 0;
            wRequest.invoke();
        }

        function sessionKeepAlive_Callback(executor, eventArgs){}
        window.setInterval( "sessionKeepAlive();", <millis>);
    </script>
</asp:Literal>


