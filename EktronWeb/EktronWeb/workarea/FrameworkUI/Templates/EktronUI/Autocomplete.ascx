﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Ektron.Cms.Framework.UI.Controls.EktronUI.Templates.Autocomplete" Codebehind="Autocomplete.ascx.cs" %>
<span id="<%= this.ControlContainer.ClientID %>" class="ektron-ui-control ektron-ui-autocomplete">
    <ektronUI:TextField ID="uxAutocompleteTextBox" runat="server" CssClass="key" />
</span>