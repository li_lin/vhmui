﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ektron.Cms.Framework.UI.Controls.EktronUI.Templates {
    
    
    public partial class DecimalField {
        
        /// <summary>
        /// uxTextField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl uxTextField;
        
        /// <summary>
        /// aspInput control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox aspInput;
        
        /// <summary>
        /// uxJavaScriptBlock control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Ektron.Cms.Framework.UI.Controls.EktronUI.JavaScriptBlock uxJavaScriptBlock;
    }
}
