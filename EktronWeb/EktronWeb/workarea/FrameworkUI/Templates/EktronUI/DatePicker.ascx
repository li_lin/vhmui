﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Ektron.Cms.Framework.UI.Controls.EktronUI.Templates.Datepicker" Codebehind="Datepicker.ascx.cs" %>

<div ID="uxDatepicker" runat="server">
    <ektronUI:DateField ID="uxDateField" runat="server" CssClass="key" Visible="true" />
    <div id="uxInline" runat="server" visible="false" class="key"></div>    
</div>