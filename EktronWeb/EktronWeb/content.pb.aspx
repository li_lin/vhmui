﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Ektron.Site.Content" Title="Content" Codebehind="content.pb.aspx.cs" %>

<%@ Register Assembly="Ektron.Cms.Widget" Namespace="Ektron.Cms.PageBuilder" TagPrefix="PB" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/PageHost.ascx" TagName="PageHost" TagPrefix="CMS" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/DropZone.ascx" TagName="DropZone" TagPrefix="CMS" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <CMS:PageHost ID="PageHost1" runat="server" />
        <CMS:DropZone ID="dropZoneChannelRight" runat="server" AllowColumnResize="false" AllowAddColumn="false">
            <ColumnDefinitions>
                <PB:ColumnData columnID="0" unit="percent" width="100" />
            </ColumnDefinitions>
        </CMS:DropZone>
    </form>
</html>

