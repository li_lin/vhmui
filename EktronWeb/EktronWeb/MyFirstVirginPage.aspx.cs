﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ektron.Cms.Framework.Content;
using Ektron.Cms.Framework.User;

namespace vhm.ui
{
    public partial class MyFirstVirginPage : Ektron.Cms.PageBuilder.PageBuilder
    {
        protected SponsorSettings SponsorSettings { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            var oCookie = Request.Cookies["SponsorName"];
            string sponsorName = "Virgin HealthMiles";
            if (oCookie != null) {
                sponsorName = oCookie.Value;
            }
            SponsorSettingsHelper sponsorHelper = new SponsorSettingsHelper(sponsorName);
            SponsorSettings = sponsorHelper.GetSettings();
            if (SponsorSettings == null) {
                SponsorSettings = new SponsorSettings();
            }
            
            //TODO: Based on settings, set logo and color theme


            UserManager userMngr = new UserManager();
            //Response.Write(userMngr.UserId.ToString());

            var langCookie = Request.Cookies["SelectedLanguage"];
            int selctedLanguage = 1033;// english
            if (langCookie != null && langCookie.Value != null)
            {
                selctedLanguage = int.Parse(langCookie.Value);
            }
            userMngr.ContentLanguage = selctedLanguage;
            ContentManager contentMngr = new ContentManager();
            contentMngr.ContentLanguage = selctedLanguage;

            HttpCookie ecm = new HttpCookie("ecm");
            if (ecm == null) {
                // cookie was not created
            } else {
                ecm.Value = HttpContext.Current.Request.Cookies["ecm"].Value;
                ecm.Values.Set("DefaultLanguage", selctedLanguage.ToString());
                ecm.Values.Set("NavLanguage", selctedLanguage.ToString());
                ecm.Values.Set("SiteLanguage", selctedLanguage.ToString());
                ecm.Values.Set("LastValidLanguageID", selctedLanguage.ToString());
                ecm.Values.Set("UserCulture", selctedLanguage.ToString());
                HttpContext.Current.Response.SetCookie(ecm);
            }

            this.hdnWp1.Value = Request.Params["SessionID"];

        }

        public override void Error(string message)
        {
            throw new NotImplementedException();
        }

        public override void Notify(string message) {
            throw new NotImplementedException();
        }
    }
}