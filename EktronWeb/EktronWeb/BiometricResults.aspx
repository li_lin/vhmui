﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BiometricResults.aspx.cs" Inherits="Ektron.Cms.BiometricResults" %>

<%@ Register src="Workarea/PageBuilder/PageControls/PageHost.ascx" tagname="PageHost" tagprefix="uc1" %>
<%@ Register src="Workarea/PageBuilder/PageControls/DropZone.ascx" tagname="DropZone" tagprefix="uc2" %>
                                                                                                                  
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
       
<head runat="server">
    <title></title>
    <link href="https://localhost/VirginLifecare/Styles/style_master.css" media="screen,projection,print" rel="stylesheet" type="text/css" />
    <link href="https://localhost/VirginLifecare/Styles/style_Incentives.css" media="screen,projection,print" rel="stylesheet" type="text/css" />
    <link href="https://localhost/VirginLifecare/Styles/style_Incentive_landing.css" media="screen,projection,print" rel="stylesheet" type="text/css" />
    <link href="https://localhost/VirginLifecareWeb/Content/Style/Master.css" rel="stylesheet" type="text/css" />  
    <style type="text/css">
        .pageForm 
        {
        	background: #fff; 
        	height: auto; 
        	margin-left: auto; 
        	margin-right: auto; 
        	margin-top:90px;
        	padding:8px;
        	padding-top: 0px; 
        	width: 933px;
        }
        h1{color: #000;}
        ul, li{ list-style-image: none; list-style-type: none;}
        .VHMcontainer .VHMheader    { background: url('http://localhost/VirginLifecare/images/Interim/header_bkgrd.jpg') #b00000;}
        .VHMcontainer .VHMheader .VHMmenuholder{ margin-right: 0px;}
        .VHMcontainer .VHMheader .VHMheadermenu{ margin-top: 0px;}
        .VHMcontainer .VHMheaderb{ padding-left: 0px; width: 820px;}
        .PageHeaderDiv
        {
        	background: url('http://localhost/VirginLifecare/images/Incentives/content_top_950x35.png') top left no-repeat;
            line-height: 35px;
            margin-left: -8px;
            margin-bottom:10px;
            padding: 0 20px; 
            width: 950px;   
        }
        .PageHeaderDiv h1{color: #fff;}
        .VHMcontainer{ min-height: 0px;}
        .options{ text-align: right;color: #ffffff;}
        .options div{ margin-top: 7px;}
        
        /*blue theme*/
        .VHMcontainer .headerBlue{ background: url('../uploadedimages/header_blue.png');background-repeat: repeat-x;}
        .VHMcontainer .VHMheader .VHMheadermenuc_blue {background: url('../uploadedimages/menu_blue.png');background-repeat: repeat-x;}
        .VHMcontainer .VHMheader .VHMheadermenul_blue {background: url('../uploadedimages/menu_blue_left.png');background-repeat: repeat-x;}
        .VHMcontainer .VHMheader .VHMheadermenur_blue {background: url('../uploadedimages/menu_blue_right.png');background-repeat: repeat-x;}
        .PageHeaderDiv_blue{background: url('../uploadedimages/content_top_950x35_blue.png');background-repeat: no-repeat;}
        .BlueBorder{ border: solid 1pt #0066AC; }
        /*end blue theme*/
    </style>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#selTheme").change(function () {
                if ($(this).val() == "Blue") {
                    $("#header").addClass("headerBlue");
                    $("#menu_center").addClass("VHMheadermenuc_blue");
                    $("#menu_left").addClass("VHMheadermenul_blue");
                    $("#menu_right").addClass("VHMheadermenur_blue");
                    $("#PageHeaderDiv").addClass("PageHeaderDiv_blue");
                    $("div.pageForm").addClass("BlueBorder");
                } else {
                    $("#header").removeClass("headerBlue");
                    $("#menu_center").removeClass("VHMheadermenuc_blue");
                    $("#menu_left").removeClass("VHMheadermenul_blue");
                    $("#menu_right").removeClass("VHMheadermenur_blue");
                    $("#PageHeaderDiv").removeClass("PageHeaderDiv_blue");
                    $("div.pageForm").removeClass("BlueBorder");
                }
            });

            var currentLanguage = ReadCookie("SelectedLanguage");

            if (currentLanguage == "1034") {
                $("#selLanguage").get(0).selectedIndex = 1;
            }

            $("#selLanguage").change(function () {
                var d = 1;
                var lang = $(this).val();
                SetCookie("SelectedLanguage", lang, d);
                window.location.reload();
            });
        });

        function ReadCookie(cookieName) {
            var theCookie = " " + document.cookie;
            var ind = theCookie.indexOf(" " + cookieName + "=");
            if (ind == -1) ind = theCookie.indexOf(";" + cookieName + "=");
            if (ind == -1 || cookieName == "") return "";
            var ind1 = theCookie.indexOf(";", ind + 1);
            if (ind1 == -1) ind1 = theCookie.length;
            return unescape(theCookie.substring(ind + cookieName.length + 2, ind1));
        }

        function SetCookie(cookieName, cookieValue, nDays) {
            var today = new Date();
            var expire = new Date();
            expire.setTime(today.getTime() + 3600000 * 24 * nDays);
            document.cookie = cookieName + "=" + escape(cookieValue)
                 + ";expires=" + expire.toGMTString();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <input type="hidden" runat="server" id="hdnWp1" name="hdnWp1" value=""/>
    <div class="VHMcontainer">
        <div class="VHMheader VHMzoom" id="header">
            <div class="VHMheaderb">
                <div class="VHMmenuholder" id="divMenuHolder" runat="server">
                    <div class="VHMclearfix VHMzoom">
                        <a id="lnkLogo" class="VHMa VHMlogo1" runat="server"><img border="0" width="219" height="43" class="VHMpngfix" src="<%=this.SponsorSettings.Logo %>" alt="Virgin HealthMiles"/></a>
						<ul class="VHMtopmenu" style="vertical-align:top;">
							<li class="VHMli" id="liVHMSupport" runat="server"><a class="VHMa" href="https://localhost/VirginLifecare/secure/Help/Support.aspx">Support</a>&nbsp;&nbsp;|</li>
							<li class="VHMli" id="liVHMPersAcct" runat="server"><a class="VHMa" href="https://localhost/VirginLifecare/secure/Member/MemberDetails.aspx?open=account">My Account</a>&nbsp;&nbsp;|</li>
							<li class="VHMli"><a class="VHMa" href="https://localhost/VirginLifecare/logout.aspx">Log Out</a></li>
						</ul>  
                        <div class="options">
                            <div>
                                <label>Choose your language:</label>
                                <select id="selLanguage">
                                    <option value="1033">English</option>
                                    <option value="1034">Spanish</option>
                                </select>
                            </div>
                            <div>
                                <label>Choose your theme:</label>
                                <select id="selTheme">
                                    <option value="Red">Red</option>
                                    <option value="Blue">Blue</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div id="divMainNav" class="VHMheadermenu noMargin" runat="server">
                        <div id="menu_left" class="VHMheadermenul VHMpngfix"></div>
                        <div id="menu_center" class="VHMheadermenuc">
			                <ul id="mainNav" class="VHMul">
				                <li><a class="VHMa" href="https://localhost/VirginLifecare/secure/Member/landingPage.aspx" >Home</a></li>
				                <asp:repeater id="rptHeaderNav" runat="server">
					                <itemtemplate>
						                <li id="liNavCat" runat="server">
							                <a class="VHMa" href="#" id="lnkCategTitle" runat="server"><asp:label id="lblCategoryTitle" runat="server" /></a>
							                <ul class="VHMclear">
								                <%--<asp:Repeater ID="rptNavItem" Runat="server">--%>
									               <%-- <itemtemplate><li><asp:HyperLink ID="lnkNavItem" class="VHMa" Runat="server"></asp:HyperLink></li></itemtemplate>--%>
								               <%-- </asp:repeater>--%>                                               
							                </ul>
						                </li>
					                </itemtemplate>
				                </asp:repeater>
                                <li><a href="https://localhost/VirginLifecare/secure/Rewards/MonthlyStatement.aspx" class="VHMa">Rewards</a></li>
                                <li><a href="https://localhost/VirginLifecare/secure/Member/ActivityJournal.aspx" class="VHMa">Activity</a></li>
                                <li><a href="https://localhost/VirginLifecare/secure/Member/biomeasurements.aspx" class="VHMa">Measure</a></li>
                                <li><a href="/Ektron.Cms/Partner/?langtype=1033" class="VHMa">Partner</a></li>
			                </ul>			                
                        </div>
                        <div id="menu_right" class="VHMheadermenur VHMpngfix" style="width:0.96%"></div>
                    </div>  
                </div>                                                           
            </div>                         
        </div>        
    </div>
    <uc1:PageHost ID="PageHost1" runat="server" />
    <section>
        <div class="pageForm" style="">
            <div id="PageHeaderDiv" class="PageHeaderDiv">
                <h1>Biometrics</h1>
            </div>
            <div style="width:65%;display :inline-block;">
                <div>
                    <uc2:DropZone ID="dzAbout" runat="server" />
                </div>
                <div>
                    <uc2:DropZone ID="dzREwards" runat="server" />
                </div>  
                <div>
                    <uc2:DropZone ID="dzBioResults" runat="server" />
                </div>  
                <div>
                    <uc2:DropZone ID="dzFAQ" runat="server" />
                </div>   
            </div>    
            <div style="width:25%; margin-left:15px;display :inline-block; vertical-align: top; ">
                <div>
                    <uc2:DropZone ID="dzSchedule" runat="server" />
                </div>
                <div>
                    <uc2:DropZone ID="dzUFG" runat="server" />
                </div>  
                <div>
                    <uc2:DropZone ID="dzHeadlines" runat="server" />
                </div> 
                <div>
                    <uc2:DropZone ID="dzFeedback" runat="server" />
                </div>   
            </div>
        </div>
    </section>
    
    </form>
</body>
</html>

