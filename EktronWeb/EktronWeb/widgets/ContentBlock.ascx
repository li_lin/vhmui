<%@ Control Language="C#" AutoEventWireup="true" Debug="true" Inherits="Widgets_ContentBlock" Codebehind="ContentBlock.ascx.cs" %>
<%@ Register Src="~/widgets/ContentBlock/Content.ascx" TagPrefix="UC" TagName="ContentControl" %>

<asp:MultiView ID="ViewSet" runat="server">
    <asp:View ID="View" runat="server">
        <UC:ContentControl ID="CB" runat="server" Visible="true" DynamicParameter="id" />
        <asp:Label ID="errorLb" runat="server" />
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>" class="CBWidget">
            <div class="CBEdit">
                <asp:Label ID="editError" runat="server" />
 
           
                <div class="BySearch CBTabPanel">
                    <asp:Label ID="Label1" runat="server" Text="Content Id:" AssociatedControlID="ucContentId" />
                    <asp:TextBox ID="ucContentId" runat="server"></asp:TextBox>
                </div>

                <div class="CBEditControls">
                    <asp:Button ID="CancelButton" CssClass="CBCancel" runat="server" Text="Cancel" OnClick="CancelButton_Click" />
                    <asp:Button ID="SaveButton" CssClass="CBSave" runat="server" Text="Save" OnClick="SaveButton_Click" OnClientClick="return Ektron.PFWidgets.ContentBlock.Save();" />
                </div>
             
        </div>
    </asp:View>
</asp:MultiView>