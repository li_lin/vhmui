using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Ektron.Cms.Widget;
using Ektron.Cms;
using Ektron.Cms.Common;
using Ektron.Cms.PageBuilder;


public partial class Widgets_ContentBlock : System.Web.UI.UserControl, IWidget
{

    #region properties

    private long _ContentBlockId;

    
    [WidgetDataMember(0)]
    public long ContentBlockId { get { return _ContentBlockId; } set { _ContentBlockId = value; } }


    #endregion

    EkRequestInformation _requestInformation;

    private EkRequestInformation RequestInformation
    {
        get
        {
            if (_requestInformation == null)
            {
                _requestInformation = ObjectFactory.GetRequestInfoProvider().GetRequestInformation();
            }
            return _requestInformation;
        }
    }

    Ektron.Cms.PageBuilder.WidgetHost _host;

    protected string appPath;
    protected int langType;
    protected string uniqueId;


    protected void Page_Init(object sender, EventArgs e)
    {
        _host = Ektron.Cms.Widget.WidgetHost.GetHost(this) as Ektron.Cms.PageBuilder.WidgetHost;
        _host.Title = "Content Block Widget";
        _host.Edit += new EditDelegate(EditEvent);
        _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        _host.Create += new CreateDelegate(delegate() { EditEvent(""); });
        _host.ExpandOptions = Expandable.ExpandOnEdit;

        this.EnableViewState = false;


        Page.ClientScript.GetPostBackEventReference(SaveButton, "");
        appPath = RequestInformation.ApplicationPath;
        langType = RequestInformation.ContentLanguage;
        MainView();
        ViewSet.SetActiveView(View);
    }

   
    protected void MainView()
    {
        if (ContentBlockId > -1)
        {
            PageBuilder p = Page as PageBuilder;
            CB.ContentId = ContentBlockId;
            //CB.DataBind();

        }
		else if (Page.Request["id"] != null && long.TryParse(Page.Request["id"], out _ContentBlockId))
		{
			CB.Visible = true;
        }
    }

    void EditEvent(string settings)
    {
        try
        {
            ViewSet.SetActiveView(Edit);

            if (ContentBlockId > 0)
            {
                ucContentId.Text = ContentBlockId.ToString();
              
            }
        }
        catch (Exception e)
        {
            errorLb.Text = e.Message + e.Data + e.StackTrace + e.Source + e.ToString();
            _host.Title = _host.Title + " error";
            ViewSet.SetActiveView(View);
        }
    }

   

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        Int64 cid = 0;
        if (Int64.TryParse(ucContentId.Text, out cid))
        {
            ContentBlockId = cid;
            _host.SaveWidgetDataMembers();
            MainView();
        }
        else
        {
            ucContentId.Text = "";
            editError.Text = "Invalid Content Block Id";

        }
        ViewSet.SetActiveView(View);
    }


    protected void CancelButton_Click(object sender, EventArgs e)
    {
        MainView();
        ViewSet.SetActiveView(View);
    }

    
}
