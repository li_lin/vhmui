﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ektron.Cms;
using Ektron.Cms.Framework.Content;

public partial class widgets_ContentBlock_Content : System.Web.UI.UserControl
{

    protected ContentData ContentSource { get; set; }
    public long ContentId { get; set; }
    public string DynamicParameter { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (ContentId == 0 && string.IsNullOrWhiteSpace(DynamicParameter))
        {
            long cid;
            if (long.TryParse(Request[DynamicParameter], out cid))
            {
                ContentId = cid;
            }
        }

        if (ContentId > 0)
        {
            ContentManager contentManager = new ContentManager();
            ContentSource = contentManager.GetItem(ContentId);

            if (ContentSource == null)
            {
                ContentSource = new ContentData();
            }

            this.DataBind();
        }
    }

}