﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OHDEarnedRewards.ascx.cs" Inherits="Vhm.Widgets.POC.OHDEarnedRewards" %>
 
<style type="text/css">
    .rewardItem{ display: inline-block;height: 80px;width: 80px;}
    div.rewardAmt{ position: absolute;font-weight: bold;color: #CB0100;font-size: 12pt;text-align: center;width: 80px; margin-top: 10px;}	
    div.cash{ margin-top: 5px;}
</style>
<div class="widget" style="background: #FFFFE0; width: 550px; height: 110px; text-align: center; padding:5px;">
    <div>
        <h3>You can earn rewards by participating in the <%= this.Title %></h3>
    </div>        
    <div>
    
        <% foreach (var reward in this.Rewards)
           {%>
                 <div class="rewardItem">
                     <div class="rewardAmt <%=reward.IsCash ? "" : "cash" %>"><%=reward.Amount %></div> 
                     <img src="/VirginLifeCare/images/rewards/RewardTypes/<%=reward.RewardImage %>"/>
                 </div>
         <%   }%> 
    </div>
</div>