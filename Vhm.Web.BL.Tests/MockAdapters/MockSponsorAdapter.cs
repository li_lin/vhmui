﻿using System;
using System.Collections.Generic;
using Vhm.Web.DataServices.Adapters;
using Vhm.Web.DataServices.Interfaces;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.BL.Tests.MockAdapters {
    public class MockSponsorAdapter : BaseAdapter, ISponsorAdapter {
        #region ISponsorAdapter Members

        public DataServices.Adapters.SponsorProfile GetSponsorProfile(long sponsorId) {
            throw new NotImplementedException();
        }

        public List<ColorTheme> GetAvailableSponsorColorThemes(long sponsorId) {
            throw new NotImplementedException();
        }

        public List<SponsorLanguage> GetSponsorLanguages(long sponsorId) {
            throw new NotImplementedException();
        }

        public List<PasswordRegEx> GetPasswordRegExValues() {
            throw new NotImplementedException();
        }

        public List<SponsorModule> GetSponsorModules(long sponsorId) {
            throw new NotImplementedException();
        }

        public List<RewardsProgram> GetSponsorPrograms(long sponsorId)
        {
            throw new NotImplementedException();
        }

        public List<SponsorModuleGZVersion> GetSponsorModuleGZVersions(long sponsorId)
        {
            return new List<SponsorModuleGZVersion>
                       {
                           new SponsorModuleGZVersion
                               {
                                   DateFrom = DateTime.Now.AddDays( -30 ),
                                   Description = string.Empty,
                                   ProductCode = string.Empty,
                                   RemoteDeviceVersionID = 0,
                                   SponsorModuleGZVersionID = 0,
                                   SponsorModuleID = 0,
                                   Version = "1.0"
                               },
                           new SponsorModuleGZVersion
                               {
                                   DateFrom = DateTime.Now.AddDays( -20 ),
                                   Description = string.Empty,
                                   ProductCode = string.Empty,
                                   RemoteDeviceVersionID = 0,
                                   SponsorModuleGZVersionID = 0,
                                   SponsorModuleID = 0,
                                   Version = "2.0"
                               },
                           new SponsorModuleGZVersion
                               {
                                   DateFrom = DateTime.Now.AddDays( -10 ),
                                   Description = string.Empty,
                                   ProductCode = string.Empty,
                                   RemoteDeviceVersionID = 0,
                                   SponsorModuleGZVersionID = 0,
                                   SponsorModuleID = 0,
                                   Version = "2.1"
                               }
                       };
        }

        #endregion
    }
}
