﻿using System;
using System.Collections.Generic;
using Vhm.Web.DataServices.Interfaces;
using Vhm.Web.Models.RepositoryModels;
using System.Web.Script.Serialization;

namespace Vhm.Web.BL.Tests.MockAdapters
{
    public class MockChallengeAdapter : IChallengeAdapter
    {
        #region IChallengeAdapter Members

        private const string TEST_CHAT_MESSAGE_FILE = "TestChatMessage.txt";
        private JavaScriptSerializer serializer = new JavaScriptSerializer();

        public List<ChallengeMessage> GetChallengeMessages(long challengeId, bool deleted)
        {
            string text = System.IO.File.ReadAllText(TEST_CHAT_MESSAGE_FILE);
            var message = serializer.Deserialize<ChallengeMessage>( text );
            
            return new List<ChallengeMessage>
                {
                    message
                };
        }

        public bool AddChallengeInvite(long challengeId, string toIds, string emailAddresses, bool sent,
                                       DateTime? dateSent, bool isDeclined, bool doNotRemind, decimal handicap,
                                       string source, long? groupId, long memberId)
        {
            throw new NotImplementedException();
        }
      
        public long AddChallengeMember( ChallengeMember member )
        {
            throw new NotImplementedException();
        }
        public bool RemoveCacheByKey(string key)
        {
            throw new NotImplementedException();
        }

        public List<SponsorSystemBadge> GetChallengeBadges( long sponsorId, string category )
        {
            throw new NotImplementedException();
        }
        public bool SendChallengeInvite(long challengeId, string personalMessage)
        {
            throw new NotImplementedException();
        }
        public List<ChallengeMessage> GetChallengeMessagesForTeam(long challengeId, long teamId, bool deleted)
        {
            string text = System.IO.File.ReadAllText(TEST_CHAT_MESSAGE_FILE);
            var message = serializer.Deserialize<ChallengeMessage>(text);

            return new List<ChallengeMessage>
                {
                    message
                };
        }

        /// <summary>
        /// This mock method will take a message and save it in a text file. This file will contain one and
        /// only one message at any time. Each time the method is called, it will overwrite any previous message.
        /// Parameters are actually irrelevant.
        /// </summary>
        /// <param name="challengeId"></param>
        /// <param name="teamId"></param>
        /// <param name="memberId"></param>
        /// <param name="nickName"></param>
        /// <param name="messageText"></param>
        public void SaveChallengeMessage(long challengeId, long? teamId, long memberId, string nickName, string messageText)
        {
            var newMessage = new ChallengeMessage
                {
                    ChallengeID = challengeId,
                    ChallengeMessageID = 0,
                    MemberID = memberId,
                    NickName = nickName,
                    CreatedDate = DateTime.Now,
                    Deleted = false,
                    FilteredMessageText = messageText,
                    TeamID = teamId
                };

            System.IO.File.WriteAllText(TEST_CHAT_MESSAGE_FILE, serializer.Serialize(newMessage));
        }

        public PersonalTrackingChallenge GetChallenge(long challengeId)
        {
            throw new NotImplementedException();
        }

        public List<Models.ViewModels.Challenges.ChallengeScore> GetTrackedDays(long challengeId, long memberId)
        {
            throw new NotImplementedException();
        }

        public long AddChallengeScore(Models.ViewModels.Challenges.ChallengeScore challengeScore)
        {
            throw new NotImplementedException();
        }

        public bool UpdateChallengeScore(Models.ViewModels.Challenges.ChallengeScore challengeScore)
        {
            throw new NotImplementedException();
        }

        public List<ChallengeMemberScore> GetChallengeLeaders(long challengeId, int pageNumber, int pageSize)
        {
            throw new NotImplementedException();
        }

        public ChallengeMemberScore GetChallengeMember(long memberId, long challengeId)
        {
            throw new NotImplementedException();
        }

        public long SaveChallenge( PersonalTrackingChallenge newChallenge, long memberId )
        {
            throw new NotImplementedException();
        }

        public void AddChallengeInvitesForAllFriends(long challengeId, long challengeCreator)
        {
            throw new NotImplementedException();
        }
        public void AddChallengeInvitesForAllFriends(long challengeId, long challengeCreator, string source)
        {
            throw new NotImplementedException();
        }

        public List<MemberAvailableChallengesByType> GetMemberAvailableChallengesByType( long memberId,
            string challengeTypes, DateTime systemTimeNow )
        {
            throw new NotImplementedException();
        }

        public List<PersonalTrackingChallenge> GetChallenges( long[] challengeIds )
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
