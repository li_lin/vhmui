﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Vhm.Web.Models.RepositoryModels;
using Assert = Xunit.Assert;

namespace Vhm.Web.BL.Tests {

    [TestClass]
    public class SponsorBLTests : BaseTest {

        [TestMethod]
        public void GetSponsorCurrentGZVersion_SponsorIDEqualsOne_ReturnMaxGZVersionFromListOfAllGZVersions()
        {
            // Arrange
            Initialize();

            const int SPONSOR_ID = 1;

            // Act
            var allVersions = SponsorBL.GetSponsorGZVersions( SPONSOR_ID );
            var maxVersion = (from version in allVersions
                             select Convert.ToDouble( version.Version )).Max();

            SponsorModuleGZVersion maxGZVersion = SponsorBL.GetSponsorCurrentGZVersion(SPONSOR_ID);

            // Assert
            Assert.True(Convert.ToDouble(maxGZVersion.Version).Equals(maxVersion));
        }

        [TestMethod]
        public void GetSponsorCurrentGZVersion_SponsorIDEqualsOne_SupplierCodeNotEmpty() {
            
            // Arrange
            Initialize();
            const int SPONSOR_ID = 1;

            // Act
            SponsorModuleGZVersion maxGZVersion = SponsorBL.GetSponsorCurrentGZVersion(SPONSOR_ID);

            // Assert
            Assert.True(!string.IsNullOrEmpty(maxGZVersion.SupplierCode));
        }

        [TestMethod]
        public void GetSponsorCurrentGZVersion_SponsorIDEqualsOne_ProductCodeNotEmpty() {

            // Arrange
            Initialize();
            const int SPONSOR_ID = 1;

            // Act
            SponsorModuleGZVersion maxGZVersion = SponsorBL.GetSponsorCurrentGZVersion(SPONSOR_ID);

            // Assert
            Assert.True(!string.IsNullOrEmpty(maxGZVersion.ProductCode));
        }

        [TestMethod]
        public void GetSponsorCurrentGZVersion_SponsorIDEqualsOne_VersionNotEmpty() {

            // Arrange
            Initialize();
            const int SPONSOR_ID = 1;

            // Act
            SponsorModuleGZVersion maxGZVersion = SponsorBL.GetSponsorCurrentGZVersion(SPONSOR_ID);

            // Assert
            Assert.True(!string.IsNullOrEmpty(maxGZVersion.Version));
        }
    }
}
