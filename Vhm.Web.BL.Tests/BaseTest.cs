﻿using System.IO;
using System.Web;

namespace Vhm.Web.BL.Tests {
    public class BaseTest {

        protected void Initialize()
        {
            DataServices.Repositories.AutoMapperConfig.Intialize();

            HttpContext.Current = new HttpContext(
                          new HttpRequest("", "http://tempuri.org", ""),
                          new HttpResponse(new StringWriter())
                          );
        }
    }
}
