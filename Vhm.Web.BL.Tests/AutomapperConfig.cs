﻿using AutoMapper;
using Vhm.Web.DataServices.Adapters;
using Vhm.Web.DataServices.MiddleTierActivityService;
using Vhm.Web.DataServices.MiddleTierService;
using Vhm.Web.Models;
using Vhm.Web.Models.RepositoryModels;
using Vhm.Web.Models.ViewModels.Challenges;
using VhmMemberChallengeMenu = Vhm.Web.DataServices.MiddleTierService.VhmMemberChallengeMenu;

namespace Vhm.Web.BL.Tests {
    public static class AutoMapperConfig {

        private static bool IsInitiallized { get; set; }

        public static void Intialize() {

            if (IsInitiallized)
            {
                return;
            }
            Mapper.CreateMap<VhmSponsorProfile, SponsorProfile>();
            Mapper.CreateMap<VhmSponsorLanguage, SponsorLanguage>();
            Mapper.CreateMap<VhmMemberSegmentation, MemberSegmentation>();
            Mapper.CreateMap<VhmNoSmokingInfo, SmokingInfo>()
                  .ForMember(dst => dst.QuestionaireItemId, opt => opt.MapFrom(src => src.pkQuestionaireItemId));
           Mapper.CreateMap<VhmMemberRewardsData, MemberRewardsData>()
                  .ForMember( dst => dst.Dob, opt => opt.MapFrom( src => src.DOB ) )
                  .ForMember( dst => dst.StructureId, opt => opt.MapFrom( src => src.StructureID ) )
                  .ForMember( dst => dst.SponsorId, opt => opt.MapFrom( src => src.SponsorID ) )
                  .ForMember( dst => dst.Today, opt => opt.MapFrom( src => src.dToday ) )
                  .ForMember(dst => dst.HmSince, opt => opt.MapFrom(src => src.HMSince));
            Mapper.CreateMap<VhmBrandingTheme, ColorTheme>();
            Mapper.CreateMap<VhmCultureFormat, CultureFormat>();
            Mapper.CreateMap<VhmPasswordRegEx, PasswordRegEx>();
            Mapper.CreateMap<VhmMemberPersonalInfo, MemberPersonalInformation>();
            Mapper.CreateMap<VhmMemberRecentReward, RecentlyEarnedRewardsItem>()
                .ForMember(destination => destination.Rewards, options => options.MapFrom(source => source.Rewards))
                .ForMember(destination => destination.Amount, options => options.MapFrom(source => source.CPLTaskRewardAmt))
                .ForMember(destination => destination.BadgeId, options => options.MapFrom(source => source.BadgeID))
                .ForMember(destination => destination.DateEarned, options => options.MapFrom(source => source.CPLTaskDate))
                .ForMember(destination => destination.EntryDate, options => options.MapFrom(source => source.CPLEntryDate))
                .ForMember(destination => destination.IsBadge, options => options.MapFrom(source => source.BadgeID.HasValue))
                .ForMember(destination => destination.IsCash, options => options.MapFrom(source => source.Cash))
                .ForMember(destination => destination.Name, options => options.MapFrom(source => source.CPLTaskName))
                .ForMember(destination => destination.RewardImage, options => options.MapFrom(source => source.RewardImage))
                .ForMember(destination => destination.RewardTaskId, options => options.MapFrom(source => source.RewardTaskID))
                .ForMember(destination => destination.RewardType, options => options.MapFrom(source => source.CPLTaskRewardType));

            Mapper.CreateMap<VhmMemberChallengeMenu, ChallengeBase>()
                .ForMember(destination => destination.ChallengeId, options => options.MapFrom(source => source.PKID))
                .ForMember(destination => destination.ChallengeType, options => options.MapFrom(source => source.DataSource))
                .ForMember(destination => destination.EndDate, options => options.MapFrom(source => source.EndDate))
                .ForMember(destination => destination.Name, options => options.MapFrom(source => source.Name))
                .ForMember(destination => destination.Rank, options => options.MapFrom(source => source.IndividualRank))
                .ForMember(destination => destination.SignedUp, options => options.MapFrom(source => source.IsMemberEnrolled))
                .ForMember(destination => destination.StartDate, options => options.MapFrom(source => source.StartDate))
                .ForMember(destination => destination.Source, options => options.MapFrom(source => source.DataSource))
                .ForMember(destination => destination.MenuDisplay, options => options.MapFrom(source => source.MenuDisplay))
                .ForMember(destination => destination.UploadDeadline, options => options.MapFrom(source => source.UploadDeadline))
                .ForMember(destination => destination.BadgeName, options => options.MapFrom(source => source.BadgeImage.HasValue ? source.BadgeImage.ToString() : string.Empty));

            Mapper.CreateMap<VhmMemberChallengeMenu, Challenge>()
                .ForMember(destination => destination.ChallengeId, options => options.MapFrom(source => source.PKID))
                .ForMember(destination => destination.ChallengeType, options => options.MapFrom(source => source.DataSource))
                .ForMember(destination => destination.EndDate, options => options.MapFrom(source => source.EndDate))
                .ForMember(destination => destination.Name, options => options.MapFrom(source => source.Name))
                .ForMember(destination => destination.Rank, options => options.MapFrom(source => source.IndividualRank))
                .ForMember(destination => destination.SignedUp, options => options.MapFrom(source => source.IsMemberEnrolled))
                .ForMember(destination => destination.StartDate, options => options.MapFrom(source => source.StartDate))
                .ForMember(destination => destination.Source, options => options.MapFrom(source => source.DataSource))
                .ForMember(destination => destination.MenuDisplay, options => options.MapFrom(source => source.MenuDisplay))
                .ForMember(destination => destination.UploadDeadline, options => options.MapFrom(source => source.UploadDeadline))
                .ForMember(destination => destination.BadgeName, options => options.MapFrom(source => source.BadgeImage.HasValue ? source.BadgeImage.ToString() : string.Empty));

            Mapper.CreateMap<VhmMemberChallengeMenu, Quest>()
                .ForMember(destination => destination.ChallengeId, options => options.MapFrom(source => source.PKID))
                .ForMember(destination => destination.ChallengeType, options => options.MapFrom(source => source.DataSource))
                .ForMember(destination => destination.EndDate, options => options.MapFrom(source => source.EndDate))
                .ForMember(destination => destination.Name, options => options.MapFrom(source => source.Name))
                .ForMember(destination => destination.Rank, options => options.MapFrom(source => source.IndividualRank))
                .ForMember(destination => destination.SignedUp, options => options.MapFrom(source => source.IsMemberEnrolled))
                .ForMember(destination => destination.StartDate, options => options.MapFrom(source => source.StartDate))
                .ForMember(destination => destination.Source, options => options.MapFrom(source => source.DataSource))
                .ForMember(destination => destination.MenuDisplay, options => options.MapFrom(source => source.MenuDisplay))
                .ForMember(destination => destination.UploadDeadline, options => options.MapFrom(source => source.UploadDeadline))
                .ForMember(destination => destination.BadgeName, options => options.MapFrom(source => source.BadgeImage.HasValue ? source.BadgeImage.ToString() : string.Empty));

            Mapper.CreateMap<VhmPromoTask, PromoTask>();
            Mapper.CreateMap<VhmPromoTarget, PromoTarget>();

            Mapper.CreateMap<VhmMemberChallengeMenu, Promo>()
                .ForMember(destination => destination.ChallengeId, options => options.MapFrom(source => source.PKID))
                .ForMember(destination => destination.ChallengeType, options => options.MapFrom(source => source.DataSource))
                .ForMember(destination => destination.EndDate, options => options.MapFrom(source => source.EndDate))
                .ForMember(destination => destination.Name, options => options.MapFrom(source => source.Name))
                .ForMember(destination => destination.Rank, options => options.MapFrom(source => source.IndividualRank))
                .ForMember(destination => destination.SignedUp, options => options.MapFrom(source => source.IsMemberEnrolled))
                .ForMember(destination => destination.StartDate, options => options.MapFrom(source => source.StartDate))
                .ForMember(destination => destination.Source, options => options.MapFrom(source => source.DataSource))
                .ForMember(destination => destination.UploadDeadline, options => options.MapFrom(source => source.UploadDeadline))
                .ForMember(destination => destination.MenuDisplay, options => options.MapFrom(source => source.MenuDisplay))
                .ForMember(destination => destination.BadgeName, options => options.MapFrom(source => source.BadgeImage.HasValue ? source.BadgeImage.ToString() : string.Empty));

            Mapper.CreateMap<VhmMemberChallengeMenu, Kag>()
                .ForMember(destination => destination.ChallengeId, options => options.MapFrom(source => source.PKID))
                .ForMember(destination => destination.ChallengeType, options => options.MapFrom(source => source.Type))
                .ForMember(destination => destination.EndDate, options => options.MapFrom(source => source.EndDate))
                .ForMember(destination => destination.Name, options => options.MapFrom(source => source.Name))
                .ForMember(destination => destination.Rank, options => options.MapFrom(source => source.IndividualRank))
                .ForMember(destination => destination.SignedUp, options => options.MapFrom(source => source.IsMemberEnrolled))
                .ForMember(destination => destination.StartDate, options => options.MapFrom(source => source.StartDate))
                .ForMember(destination => destination.Source, options => options.MapFrom(source => source.DataSource))
                .ForMember(destination => destination.MenuDisplay, options => options.MapFrom(source => source.MenuDisplay))
                .ForMember(destination => destination.UploadDeadline, options => options.MapFrom(source => source.UploadDeadline))
                .ForMember(destination => destination.BadgeName, options => options.MapFrom(source => source.BadgeImage.HasValue ? source.BadgeImage.ToString() : string.Empty));

            Mapper.CreateMap<VhmChallengeMessage, ChallengeMessage>();

            Mapper.CreateMap<VhmStep, ActivityGraphData.ActivityDay>()
                .ForMember(destination => destination.Steps, options => options.MapFrom(source => source.Steps))
                .ForMember(destination => destination.ActivityDate, options => options.MapFrom(source => source.ActivityDate));

            Mapper.CreateMap<SocMemberProfile, Member>();
            Mapper.CreateMap<Member, SocMemberProfile>();
            Mapper.CreateMap<GoalsAndInterests, MemberGoalsAndInterests>();
            Mapper.CreateMap<SocMemberFriend, Member>();
            Mapper.CreateMap<MemberOccupation, Occupation>();
            Mapper.CreateMap<MemberPersonalInformation, VhmMemberPersonalInfo>();

            Mapper.CreateMap<VhmMember, Member>()
                .ForMember(destination => destination.Age, options => options.MapFrom(source => source.Age))
                .ForMember(destination => destination.Average, options => options.MapFrom(source => source.Average))
                .ForMember(destination => destination.City, options => options.MapFrom(source => source.HomeCity))
                .ForMember(destination => destination.DisplayName, options => options.MapFrom(source => source.Forename + " " + source.Surname))
                .ForMember(destination => destination.EmailAddress, options => options.MapFrom(source => source.EmailAddress))
                .ForMember(destination => destination.FirstName, options => options.MapFrom(source => source.Forename))
                .ForMember(destination => destination.Gender, options => options.MapFrom(source => source.Gender))
                .ForMember(destination => destination.IdentityNumber, options => options.MapFrom(source => source.IdentityNumber))
                .ForMember(destination => destination.LastName, options => options.MapFrom(source => source.Surname))
                .ForMember(destination => destination.MemberId, options => options.MapFrom(source => source.MemberID))
                .ForMember(destination => destination.UpdatedDate, options => options.MapFrom(source => source.UpdatedDate))
                .ForMember(destination => destination.LastLoginDate, options => options.MapFrom(source => source.UpdatedDate));

            Mapper.CreateMap<SocNewsFeedPost, NewsfeedPost>()
                .ForMember(destination => destination.ImageName, options => options.MapFrom(source => source.BadgeImageURL))
                .ForMember(destination => destination.LikeCount, options => options.MapFrom(source => source.Likes.Count))
                .ForMember(destination => destination.ParentId, options => options.MapFrom(source => source.NewsFeedParentPostID))
                .ForMember(destination => destination.PostDate, options => options.MapFrom(source => source.CreatedDate))
                .ForMember(destination => destination.PostId, options => options.MapFrom(source => source.NewsFeedPostID))
                .ForMember(destination => destination.PostText, options => options.MapFrom(source => source.NewsFeedPost))
                .ForMember(destination => destination.PostType, options => options.MapFrom(source => source.PostTypeCode))
                .ForMember(destination => destination.PostTypeId, options => options.MapFrom(source => source.NewsFeedPostTypeID))
                .ForMember(destination => destination.PostById, options => options.MapFrom(source => source.FromMemberID))
                .ForMember(destination => destination.PostByName, options => options.MapFrom(source => source.FromProfile.FirstName + " " + source.FromProfile.LastName))
                .ForMember(destination => destination.PostByImage, options => options.MapFrom(source => source.FromProfile.MemberImageURL))
                .ForMember(destination => destination.PostToId, options => options.MapFrom(source => source.ToMemberID))
                .ForMember(destination => destination.PostToName, options => options.MapFrom(source => source.ToProfile.FirstName + " " + source.ToProfile.LastName))
                .ForMember(destination => destination.GroupId, options => options.MapFrom(source => source.GroupID))
                .ForMember(destination => destination.GroupName, options => options.MapFrom(source => source.GroupName))
                .ForMember(destination => destination.GroupImage, options => options.MapFrom(source => source.GroupImageUrl))
                .ForMember(destination => destination.BadgeId, options => options.MapFrom(source => source.BadgeID))
                .ForMember(destination => destination.BadgeName, options => options.MapFrom(source => source.BadgeName))
                .ForMember(destination => destination.BadgeImage, options => options.MapFrom(source => source.BadgeImageURL))
                .ForMember(destination => destination.YouLikeThis, options => options.MapFrom(source => source.YouLikedIt))
                .ForMember(destination => destination.YouLikedId, options => options.MapFrom(source => source.YouLikedID))
                .ForMember(destination => destination.TotalComments, options => options.MapFrom(source => source.TotalComments));

            Mapper.CreateMap<SocMemberRelationship, MemberRelationships>()
                .ForMember(destination => destination.IsFriend, options => options.MapFrom(source => source.IsFriend))
                .ForMember(destination => destination.ToFriendNotification, options => options.MapFrom(source => source.ToFriendNotification))
                .ForMember(destination => destination.ToMemberNotification, options => options.MapFrom(source => source.ToMemberNotification));

            Mapper.CreateMap<SocMemberNotification, MemberNotification>()
                .ForMember(destination => destination.City, options => options.MapFrom(source => source.City))
                .ForMember(destination => destination.FirstName, options => options.MapFrom(source => source.FirstName))
                .ForMember(destination => destination.ImageUrl, options => options.MapFrom(source => source.ImageUrl))
                .ForMember(destination => destination.LastName, options => options.MapFrom(source => source.LastName))
                .ForMember(destination => destination.MemberId, options => options.MapFrom(source => source.MemberID))
                .ForMember(destination => destination.MemberNotificationId, options => options.MapFrom(source => source.MemberNotificationID))
                .ForMember(destination => destination.NotificationDate, options => options.MapFrom(source => source.NotificationDate))
                .ForMember(destination => destination.NotificationStatusID, options => options.MapFrom(source => source.NotificationStatusID))
                .ForMember(destination => destination.NotificationTypeEntityId, options => options.MapFrom(source => source.NotificationTypeEntityID))
                .ForMember(destination => destination.NotificationTypeId, options => options.MapFrom(source => source.NotificationTypeID))
                .ForMember(destination => destination.NotifiedById, options => options.MapFrom(source => source.NotifiedByID))
                .ForMember(destination => destination.Sponsors, options => options.MapFrom(source => source.Sponsors));

            Mapper.CreateMap<MemberNotification, SocMemberNotification>()
                .ForMember(destination => destination.City, options => options.MapFrom(source => source.City))
                .ForMember(destination => destination.FirstName, options => options.MapFrom(source => source.FirstName))
                .ForMember(destination => destination.ImageUrl, options => options.MapFrom(source => source.ImageUrl))
                .ForMember(destination => destination.LastName, options => options.MapFrom(source => source.LastName))
                .ForMember(destination => destination.MemberID, options => options.MapFrom(source => source.MemberId))
                .ForMember(destination => destination.MemberNotificationID, options => options.MapFrom(source => source.MemberNotificationId))
                .ForMember(destination => destination.NotificationDate, options => options.MapFrom(source => source.NotificationDate))
                .ForMember(destination => destination.NotificationStatusID, options => options.MapFrom(source => source.NotificationStatusID))
                .ForMember(destination => destination.NotificationTypeEntityID, options => options.MapFrom(source => source.NotificationTypeEntityId))
                .ForMember(destination => destination.NotificationTypeID, options => options.MapFrom(source => source.NotificationTypeId))
                .ForMember(destination => destination.NotifiedByID, options => options.MapFrom(source => source.NotifiedById))
                .ForMember(destination => destination.Sponsors, options => options.MapFrom(source => source.Sponsors));

            Mapper.CreateMap<VhmMemberModule, MemberModule>()
                .ForMember(destination => destination.ModuleID, options => options.MapFrom(source => source.ModuleID))
                .ForMember(destination => destination.ModuleName, options => options.MapFrom(source => source.ModuleName))
                .ForMember(destination => destination.ModuleTypeCode, options => options.MapFrom(source => source.ModuleTypeCode))
                .ForMember(destination => destination.OtherDevices, options => options.MapFrom(source => source.OtherDevices))
                .ForMember(destination => destination.HZLocator, options => options.MapFrom(source => source.HZLocator))
                .ForMember(destination => destination.MaskModuleName, options => options.MapFrom(source => source.MaskModuleName))
                .ForMember(destination => destination.ModuleImage, options => options.MapFrom(source => source.ModuleImage))
                .ForMember(destination => destination.URL, options => options.MapFrom(source => source.URL))
                .ForMember(destination => destination.IsPartner, options => options.MapFrom(source => source.isPartner))
                .ForMember(destination => destination.PartnerGUID, options => options.MapFrom(source => source.PartnerGUID))
                .ForMember(destination => destination.SponsorModuleId, options => options.MapFrom(source => source.SponsorModuleID));

            Mapper.CreateMap<VhmMemberLatestMeasureReward, MeasurementReward>()
                .ForMember(destination => destination.MeasurementDate, options => options.MapFrom(source => source.MeasurementDate))
                .ForMember(destination => destination.Cash, options => options.MapFrom(source => source.Cash.GetValueOrDefault()))
                .ForMember(destination => destination.RewardImage, options => options.MapFrom(source => source.RewardImage))
                .ForMember(destination => destination.IsBadge, options => options.MapFrom(source => source.BadgeID.HasValue))
                .ForMember(destination => destination.TaskRewardAmt, options => options.MapFrom(source => source.CPLTaskRewardAmt));

            Mapper.CreateMap<VhmPromoTask, PromoTask>();

            Mapper.CreateMap<VhmSponsorPerk, SponsorPerks>()
                .ForMember(destination => destination.EntityID, options => options.MapFrom(source => source.EntityID))
                .ForMember(destination => destination.HSCompletion, options => options.MapFrom(source => source.HSCompletion))
                .ForMember(destination => destination.SuppressFALocator, options => options.MapFrom(source => source.SuppressFALocator))
                .ForMember(destination => destination.SuppressSelfFA, options => options.MapFrom(source => source.SuppressSelfFA))
                .ForMember(destination => destination.AllowFileMeasurements, options => options.MapFrom(source => source.AllowFileMeasurements))
                .ForMember(destination => destination.SuppressSelfMsrmts, options => options.MapFrom(source => source.SuppressSelfMsrmts))
                .ForMember(destination => destination.AllowMeasurementAssessors, options => options.MapFrom(source => source.AllowMeasurementAssessors))
                .ForMember(destination => destination.SuppressRebateCtr, options => options.MapFrom(source => source.SuppressRebateCtr));

            Mapper.CreateMap<VhmMemberRedeemVoucher, Voucher>();

            Mapper.CreateMap<Voucher, VhmMemberRedeemVoucher>();

            Mapper.CreateMap<VhmTerms, Terms>();

            Mapper.CreateMap<VhmTermsAndConditions, Terms>();

            Mapper.CreateMap<VhmTermsToAcknowledge, Terms>();

            Mapper.CreateMap<VhmTicker, Tickers>();

            Mapper.CreateMap<VhmGameInfo, Game>()
                .ForMember(destination => destination.GameId, options => options.MapFrom(source => source.Game.GameId))
                .ForMember(destination => destination.EndDate, options => options.MapFrom(source => source.Game.EndDate))
                .ForMember(destination => destination.Name, options => options.MapFrom(source => source.Game.Name))
                .ForMember(destination => destination.MemberTotalHealthMiles, options => options.MapFrom(source => source.MileBalance.TotalHealthMiles))
                .ForMember(destination => destination.Milestones, options => options.MapFrom(source => source.Game.Milestones))
                .ForMember(destination => destination.TriggerRewards, options => options.MapFrom(source => source.Game.TriggerRewards))
                .ForMember(destination => destination.ProgramType, options => options.MapFrom(source => source.ProgramType))
                .ForMember(destination => destination.Currency, options => options.MapFrom(source => source.Game.GameCurrency))
                .ForMember(destination => destination.GameDisplayType, options => options.MapFrom(source => source.Game.GameDisplayType))
                .ForMember(destination => destination.AvailableRewards, options => options.MapFrom(source => source.RewardsBalance))
                .ForMember(destination => destination.StartDate, options => options.MapFrom(source => source.Game.StartDate))
                .ForMember(destination => destination.MemberTotalActivityHealthMiles, options => options.MapFrom(source => source.MileBalance.TotalActivityHealthMiles))
                .ForMember(destination => destination.MemberTotalMeasurementHealthMiles, options => options.MapFrom(source => source.MileBalance.TotalMeasurementHealthMiles))
                .ForMember(destination => destination.MemberTotalParticipationHealthMiles, options => options.MapFrom(source => source.MileBalance.TotalParticipationHealthMiles));

            Mapper.CreateMap<VhmGameTrigger, GameTrigger>();
            Mapper.CreateMap<VhmGameCap, GameCap>();

            Mapper.CreateMap<VhmMemberRewardsBalance, RewardsBalance>();

            Mapper.CreateMap<VhmGameMilestone, Milestone>()
                .ForMember(destination => destination.Name, options => options.MapFrom(source => source.Name))
                .ForMember(destination => destination.MilestoneId, options => options.MapFrom(source => source.MilestoneId))
                .ForMember(destination => destination.Rewards, options => options.MapFrom(source => source.Rewards))
                .ForMember(destination => destination.Value, options => options.MapFrom(source => source.Value));

            Mapper.CreateMap<VhmGameReward, Reward>()
                .ForMember(destination => destination.Amount, options => options.MapFrom(source => source.Amount))
                .ForMember(destination => destination.RewardType, options => options.MapFrom(source => source.RewardType))
                .ForMember(destination => destination.MilestoneId, options => options.MapFrom(source => source.MilestoneId))
                .ForMember(destination => destination.RewardTypeDescription, options => options.MapFrom(source => source.RewardTypeDescription))
                .ForMember(destination => destination.IsCash, options => options.MapFrom(source => source.Cash))
                .ForMember(destination => destination.ImageName, options => options.MapFrom(source => source.ImageName));

            Mapper.CreateMap<VhmProgramGame, Game>()
                    .ForMember(destination => destination.Caps, options => options.MapFrom(source => source.Caps))
                    .ForMember(destination => destination.Triggers, options => options.MapFrom(source => source.Triggers));

            Mapper.CreateMap<VhmReward, Reward>()
                .ForMember(destination => destination.Amount, options => options.MapFrom(source => source.Amount))
                .ForMember(destination => destination.IsCash, options => options.MapFrom(source => source.Cash))
                .ForMember(destination => destination.RewardType, options => options.MapFrom(source => source.RewardType))
                .ForMember(destination => destination.RewardTypeDescription, options => options.MapFrom(source => source.RewardTypeDescription))
                .ForMember(destination => destination.ImageName, options => options.MapFrom(source => source.ImageName))
                .ForMember(destination => destination.Text, options => options.MapFrom(source => source.Text))
                .ForMember(destination => destination.RewardTriggerName, options => options.MapFrom(source => source.RewardTriggerName));

            Mapper.CreateMap<VhmReward, UpForGrabsRewardItem>()
                .ForMember(destination => destination.Id, options => options.MapFrom(source => source.Id))
                .ForMember(destination => destination.RewardType, options => options.MapFrom(source => source.RewardType))
                .ForMember(destination => destination.RewardId, options => options.MapFrom(source => source.RewardId))
                .ForMember(destination => destination.Text, options => options.MapFrom(source => source.Text))
                .ForMember(destination => destination.Amount, options => options.MapFrom(source => source.Amount))
                .ForMember(destination => destination.BadgeId, options => options.MapFrom(source => source.BadgeId))
                .ForMember(destination => destination.ExpirationDays, options => options.MapFrom(source => source.ExpirationDays))
                .ForMember(destination => destination.CertificateName, options => options.MapFrom(source => source.CertificateName))
                .ForMember(destination => destination.IsCash, options => options.MapFrom(source => source.Cash))
                .ForMember(destination => destination.RewardImage, options => options.MapFrom(source => source.ImageName))
                .ForMember(destination => destination.RewardTriggerName, options => options.MapFrom(source => source.RewardTriggerName));

            Mapper.CreateMap<VhmUpForGrabsReward, UpForGrabsItem>()
                .ForMember(destination => destination.Rewards, options => options.MapFrom(source => source.Rewards))
                .ForMember(destination => destination.UFGTaskName, options => options.MapFrom(source => source.UFGTaskName))
                .ForMember(destination => destination.UFGTaskRewardAmt, options => options.MapFrom(source => source.UFGTaskRewardAmt))
                .ForMember(destination => destination.UFGTaskRewardURL, options => options.MapFrom(source => source.UFGTaskRewardURL))
                .ForMember(destination => destination.UFGTaskComment, options => options.MapFrom(source => string.IsNullOrEmpty(source.UFGTaskComment) ? string.Empty : source.UFGTaskComment));

            Mapper.CreateMap<VhmUpForGrabsReward, UpForGrabsRewardItem>();

            Mapper.CreateMap<MemberPermission, SitePermissions>();
            Mapper.CreateMap<VhmSponsorPerk, SponsorPerks>();
            Mapper.CreateMap<VhmSession, Session>();

            Mapper.CreateMap<SitePermissions, MemberPermission>();
            Mapper.CreateMap<SponsorPerks, VhmSponsorPerk>();
            Mapper.CreateMap<Session, VhmSession>();

            Mapper.CreateMap<VhmMemberLevelsAudit, MemberLevelsAudit>();

            Mapper.CreateMap<VhmBadWord, BadWords>();
            Mapper.CreateMap<DataServices.MiddleTierService.RewardBadge, Models.RepositoryModels.RewardBadge>();

            Mapper.CreateMap<VhmSponsorModule, SponsorModule>();
            Mapper.CreateMap<VhmSponsorModuleGZVersion, SponsorModuleGZVersion>();

            Mapper.CreateMap<VhmChallengeScore, ChallengeScore>();
            Mapper.CreateMap<ChallengeScore, VhmChallengeScore>();
            Mapper.CreateMap<VhmChallengeMemberScore, ChallengeMemberScore>();
            Mapper.CreateMap<VhmChallengeMember, ChallengeMember>();
            Mapper.CreateMap<VhmChallenge, PersonalTrackingChallenge>();
            Mapper.CreateMap<VhmChallengeQuestion, ChallengeQuestion>();

            Mapper.CreateMap<VhmMemberDevice, MemberDevice>();
            //Mapper.CreateMap<MemberDevice, MiddleTierActivityService.VhmMemberDevice>();

            IsInitiallized = true;
        }
    }
}
