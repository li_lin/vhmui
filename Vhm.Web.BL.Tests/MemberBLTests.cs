﻿using System;
using System.Linq;
using Vhm.Web.BL;
using System.Collections.Generic;
using Xunit;
using Vhm.Web.Models.RepositoryModels;
using Vhm.Web.DataServices.Interfaces;
using Moq;

namespace Vhm.UnitTests
{
    public class MemberBLTests
    {
        private Mock<IMemberAdapter> _memberAdapterMock;
        private Mock<IAccountAdapter> _accountAdapterMock;
       

        public MemberBLTests()
        {
            _memberAdapterMock = new Mock<IMemberAdapter>();
            _accountAdapterMock = new Mock<IAccountAdapter>();
           
        }

        [Fact]
        public void SaveMemberPrivateSettings_CallsSaveMethod()
        {
            // Arrange
            var sessionId = string.Empty;
            Member member = null;
            var deleteProfilePicture = false;

            // Act
            MemberBL.SaveMemberPrivateSettings(sessionId, member, deleteProfilePicture);

            // Assert
            _memberAdapterMock.Verify(ma => ma.SaveMemberPrivateSettings(sessionId, member, deleteProfilePicture));
        }

        ///<summary>
        /// Test for Days Left
        ///</summary>
        [Fact]
        public void GetMemberChallenges_EndDateIsTomorrow_DaysLeftIsOne()
        {
            // Arrange
            long memberId = 1;
            long sponsorId = 1;
            var startDate = DateTime.Now;
            byte displayDays = 7;
            int languageId = 1033;
            _memberAdapterMock.Setup(c => c.GetMemberChallenges(It.IsAny<long>(), It.IsAny<long>(), displayDays, languageId)).
                Returns(
                    new List<ChallengeBase>
                        {
                            new Challenge
                                {
                                    StartDate = startDate.AddDays( -25 ),
                                    EndDate = startDate.AddDays( 1 )
                                }
                        } );

            // Act
            var result = MemberBL.GetMemberChallenges(memberId, sponsorId, displayDays,languageId);

            // Assert
            Assert.Equal(1, result.First().DaysLeft);
        }

        ///<summary>
        /// Test for Days Until Start
        ///</summary>
        [Fact]
        public void GetMemberChallenges_StartDateIsTomorrow_DaysUntilStartIsOne() {
            // Arrange
            long memberId = 1;
            long sponsorId = 1;
            var startDate = DateTime.Now;
            byte displayDays = 7;
            int languageId = 1033;
            _memberAdapterMock.Setup(c => c.GetMemberChallenges(It.IsAny<long>(), It.IsAny<long>(), displayDays, languageId)).
                Returns(
                    new List<ChallengeBase>
                        {
                            new Challenge
                                {
                                    StartDate = startDate.AddDays( 1 ),
                                    EndDate = startDate.AddDays( 1 )
                                }
                        });

            // Act
            var result = MemberBL.GetMemberChallenges(memberId, sponsorId, displayDays, languageId);

            // Assert
            Assert.Equal(1, result.First().DaysUntilStart);
        }

        ///<summary>
        /// Test for Challenge Ended
        ///</summary>
        [Fact]
        public void GetMemberChallenges_EndDateIsYesterday_EndedIsTrue() {
            // Arrange
            long memberId = 1;
            long sponsorId = 1;
            var startDate = DateTime.Now;
            byte displayDays = 7;
            int languageId = 1033;
            _memberAdapterMock.Setup(c => c.GetMemberChallenges(It.IsAny<long>(), It.IsAny<long>(), displayDays, languageId)).
                Returns(
                    new List<ChallengeBase>
                        {
                            new Challenge
                                {
                                    StartDate = startDate.AddDays( -31 ),
                                    EndDate = startDate.AddDays( -1 )
                                }
                        });

            // Act
            var result = MemberBL.GetMemberChallenges(memberId, sponsorId, displayDays,languageId);

            // Assert
            Assert.True(result.First().Ended);
        }

        ///<summary>
        /// Test for Challenge Started
        ///</summary>
        [Fact]
        public void GetMemberChallenges_StartDateIsYesterday_StartedIsTrue() {
            // Arrange
            long memberId = 1;
            long sponsorId = 1;
            var startDate = DateTime.Now;
            byte displayDays = 7;
            int languageId = 1033;
            _memberAdapterMock.Setup(c => c.GetMemberChallenges(It.IsAny<long>(), It.IsAny<long>(), displayDays, languageId)).
                Returns(
                    new List<ChallengeBase>
                        {
                            new Challenge
                                {
                                    StartDate = startDate.AddDays( -1 ),
                                    EndDate = startDate.AddDays( 30 )
                                }
                        });

            // Act
            var result = MemberBL.GetMemberChallenges(memberId, sponsorId, displayDays, languageId);

            // Assert
            Assert.True(result.First().Started);
        }

        [Fact]
        public void GetMemberActivityGraphData()
        {
            // Arrane
            long memberId=27;
            int graphHeight=17;
            int activityDaysToShow=2;
            int stepsTargetDefault=3;
            DateTime date=DateTime.Now.AddDays(-25);
            //int daysToShow=4;
            var result = MemberBL.GetMemberActivityGraphData(memberId, graphHeight, activityDaysToShow, stepsTargetDefault);
            Assert.InRange(result.Target,0,int.MaxValue);
         

        }
    }
}
