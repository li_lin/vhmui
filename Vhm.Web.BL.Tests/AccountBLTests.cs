﻿using Moq;
using Vhm.Web.BL;
using Vhm.Web.DataServices.Interfaces;
using Xunit;

namespace Vhm.UnitTests
{
    public class AccountBLTests
    {
        [Fact]
        public void Login_EmptyUserName_ReturnsNull()
        {
            // Arrange
            var accountAdapter = new Mock<IAccountAdapter>();

            // Act
            var session = AccountBL.Login(null, null, null, 0);

            // Assert
            Assert.NotNull(session);
        }
    }
}
