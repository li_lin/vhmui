﻿using System;
using Vhm.Web.BL;
using System.Collections.Generic;
using Xunit;
using Vhm.Web.Models.RepositoryModels;
using Vhm.Web.DataServices.Interfaces;
using Moq;
using Vhm.Web.Models.InputModels;
using Vhm.Web.Models.Enums;

namespace Vhm.UnitTests
{
    public class SocialBLTests
    {
        private Mock<ISocialAdapter> _socialAdapterMock;
        private Mock<IMemberAdapter> _memberAdapterMock;

        public SocialBLTests()
        {
            _socialAdapterMock = new Mock<ISocialAdapter>();
            _memberAdapterMock = new Mock<IMemberAdapter>();
        }

        [Fact]
        public void GetMemberFriends_MemberHasNoFriends_ReturnsEmptyList()
        {
            // Arrange
            var pagingIm = new PagingIM
            {
                MemberId = 1,
                Count = 1,
                PageNumber = 0
            };

            _socialAdapterMock.Setup(sa => sa.GetMemberFriends(It.IsAny<string>(), It.IsAny<long>(), It.IsAny<int?>(), It.IsAny<int?>()))
                .Returns(new SocialPagedData
                {
                    TotalCount = 1
                });


            // Act
            var members = SocialBL.GetMemberFriends(null, pagingIm);

            // Assert
            _socialAdapterMock.Verify(sa => sa.GetMemberFriends(null, pagingIm.MemberId, pagingIm.PageNumber, pagingIm.Count));
            Assert.NotNull(members);
            Assert.Equal(1, members.TotalCount);
        }

        [Fact]
        public void GetMemberGroups_MemberIsNotPartOfAnyGroup_ReturnsEmptyList()
        {
            // Arrange
            var pagingIm = new PagingIM
            {
                MemberId = 1,
                Count = 1,
                PageNumber = 0
            };

            _socialAdapterMock.Setup(sa => sa.GetMemberGroups(It.IsAny<string>(), It.IsAny<long>(), It.IsAny<List<GroupTypes>>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>()))
                .Returns(new SocialPagedData
                {
                    Groups = new List<Group>()
                });


            // Act
            var socialPagedData = SocialBL.GetMemberGroups(null, new List<GroupTypes>() { GroupTypes.MEM }, pagingIm, false);

            // Assert
            Assert.NotNull(socialPagedData);
            Assert.Equal(0, socialPagedData.Groups.Count);
        }

        [Fact]
        public void GetGroupMembers_GroupShouldHaveOneMemberAtleast_ReturnsMemberIdsFromGroup()
        {
            // Arrange
            _socialAdapterMock.Setup(sa => sa.GetGroupMembers(It.IsAny<string>(), It.IsAny<long>(), 0, 24, false))
                .Returns(new SocialPagedData
                {
                    GroupMembers = new List<Member>
                    {
                        new Member()
                    }
                });


            // Act
            var members = SocialBL.GetGroupMembers(null, 14, 0, 24, false);

            // Assert
            Assert.NotNull(members);
            Assert.Equal(1, members.GroupMembers.Count);
        }

        [Fact]
        public void GetAllGroupCategories_ReturnsGroupCategories()
        {
            // Arrange
            var group = new Group() { };

            _socialAdapterMock.Setup(sa => sa.GetAllGroupCategories(It.IsAny<string>()))
                .Returns(new List<GroupCategory>
                {
                    new GroupCategory()
                });


            // Act
            var membersIds = SocialBL.GetAllGroupCategories("");

            // Assert
            Assert.Equal(1, membersIds.Count);
        }

        #region Get Suggested Groups

        [Fact]
        public void GetListOfSuggestedGroups_ValidSession_ReturnsSocialPagedDataWithSuggestedGroups()
        {
            // Arrange
            var sessionId = Guid.NewGuid().ToString();

            var paging = new PagingIM
            {
                Count = 20,
                PageNumber = 0
            };

            const bool includePic = true;

            var group = new Group
            {
                Name = "Test Group"
            };

            var groups = new List<Group>();

            groups.Add(group);

            _socialAdapterMock.Setup(adpt => adpt.GetMemberSuggestedGroups(It.IsAny<string>(), It.IsAny<int?>(), It.IsAny<int?>(), It.IsAny<bool>()))
                .Returns(new SocialPagedData
                             {
                                 SuggestedGroups = groups,
                                 TotalCount = 0,
                                 Friends = new List<Member>(),
                                 CurrentPage = paging.PageNumber.GetValueOrDefault(),
                                 GroupMembers = new List<Member>(),
                                 Groups = new List<Group>(),
                                 PerPage = paging.Count.GetValueOrDefault(),
                                 HasMore = false,
                                 SuggetedMembers = new List<Member>(),
                                 TotalPageCount = 1
                             });


            // Act
            var socPagData = SocialBL.GetListOfSuggestedGroups(sessionId, paging, includePic);

            // Assert
            Assert.True(socPagData.Groups.Count == 1);

            _socialAdapterMock.Verify(adpt => adpt.GetMemberSuggestedGroups(It.IsAny<string>(), It.IsAny<int?>(), It.IsAny<int?>(), It.IsAny<bool>()));
        }

        [Fact]
        public void GetListOfSuggestedGroups_ValidSession_ReturnsPagedGroupsVMWithSuggestedGroups()
        {
            // Arrange
            var sessionId = Guid.NewGuid().ToString();
            var paging = new PagingIM();
            paging.Count = 20;
            paging.PageNumber = 0;
            const bool includePic = true;
            Group g = new Group();
            g.Name = "Test Group";
            var groups = new List<Group>();
            Member m = new Member();
            m.FirstName = "Test";
            m.LastName = "user";
            var members = new List<Member>();
            members.Add(m);
            groups.Add(g);
            _socialAdapterMock.Setup(
                adpt =>
                adpt.GetMemberSuggestedGroups(It.IsAny<string>(), It.IsAny<int?>(), It.IsAny<int?>(), It.IsAny<bool>()))
                .Returns(new SocialPagedData
                {
                    SuggestedGroups = groups,
                    TotalCount = 0,
                    Friends = members,
                    CurrentPage = paging.PageNumber.GetValueOrDefault(),
                    GroupMembers = members,
                    Groups = groups,
                    PerPage = paging.Count.GetValueOrDefault(),
                    HasMore = false,
                    SuggetedMembers = members,
                    TotalPageCount = 1
                });
            _socialAdapterMock.Setup(
                adpt =>
                adpt.GetGroupMembers(It.IsAny<string>(), It.IsAny<long>(), It.IsAny<int?>(), It.IsAny<int?>(), It.IsAny<bool>()))
                .Returns(new SocialPagedData
                {
                    SuggestedGroups = groups,
                    TotalCount = 0,
                    Friends = members,
                    CurrentPage = paging.PageNumber.GetValueOrDefault(),
                    GroupMembers = members,
                    Groups = groups,
                    PerPage = paging.Count.GetValueOrDefault(),
                    HasMore = false,
                    SuggetedMembers = members,
                    TotalPageCount = 1
                });
            var adapter = _socialAdapterMock.Object;

            // Act
            var pagedGroupsVM = SocialBL.GetListOfSuggestedGroups(sessionId, paging, includePic);

            // Assert
            Assert.True(pagedGroupsVM.Groups.Count == 1);

            _socialAdapterMock.Verify(adpt => adpt.GetMemberSuggestedGroups(It.IsAny<string>(), It.IsAny<int?>(), It.IsAny<int?>(), It.IsAny<bool>()));

        }
        #endregion

    }
}
