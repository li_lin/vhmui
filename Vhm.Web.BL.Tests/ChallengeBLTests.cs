﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Vhm.Web.BL;
using System.Collections.Generic;
using Vhm.Web.BL.Tests.MockAdapters;
using Vhm.Web.DataServices.Adapters;
using Vhm.Web.Models.RepositoryModels;
using Vhm.Web.DataServices.Interfaces;
using Moq;
using Assert = Xunit.Assert;

namespace Vhm.Web.BL.Tests
{
     [TestClass]
    public class ChallengeBLTests
    {
        private IChallengeAdapter _challengeAdapterMock;

        public ChallengeBLTests()
        {
            _challengeAdapterMock = new MockChallengeAdapter();
            AutoMapperConfig.Intialize();
        }

        ///<summary>
        /// Test for saving a message with no bad words in it.
        ///</summary>
        [TestMethod]
        public void SaveChallengeMessage_MessageHasNoBadWords_MessageIsUnchanged()
        {
            // Arrange
            long challengeId = 1;
            long memberId = 26694;
            ChallengeBL.SetChallengeAdapter(_challengeAdapterMock);
            var nickName = "Nick Name";

            // Act
            string newMessage = "The quick brown fox jumped over the lazy dog";
            ChallengeBL.SaveChallengeMessage(challengeId, null, memberId,nickName, newMessage);

            var message = ChallengeBL.GetChallengeMessages( challengeId ).FirstOrDefault();

            // Assert
            Assert.NotNull(message);
            Assert.Equal(newMessage, message.FilteredMessageText);
        }

        ///<summary>
        /// Test for saving a message with bad words in it.
        ///</summary>
        [TestMethod]
        public void SaveChallengeMessage_MessageHasBadWords_BadWordsReplacedWithAsterisks()
        {
            // Arrange
            long challengeId = 1;
            long memberId = 26694;
            ChallengeBL.SetChallengeAdapter(_challengeAdapterMock);
            var nickName = "Nick Name";
            // Act
            string newMessage = "The quick brown fox got his lazy ass off the fucking couch and called me a dick.";
            ChallengeBL.SaveChallengeMessage(challengeId, null, memberId, nickName, newMessage);

            var message = ChallengeBL.GetChallengeMessages(challengeId).FirstOrDefault();

            // Assert
            Assert.NotNull(message);
            Assert.True(!message.FilteredMessageText.Contains("ass"));
            Assert.True(!message.FilteredMessageText.Contains("fucking"));
            Assert.True(!message.FilteredMessageText.Contains("dick"));

            Assert.True(message.FilteredMessageText.Contains("***"));
            Assert.True(message.FilteredMessageText.Contains("*******"));
            Assert.True(message.FilteredMessageText.Contains("****"));
        }
    }
}
