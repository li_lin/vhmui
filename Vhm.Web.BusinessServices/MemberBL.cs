﻿using System;
using System.Linq;
using Vhm.Web.Common;
using Vhm.Web.DataServices.Adapters;
using Vhm.Web.DataServices.Interfaces;
using Vhm.Web.Models;
using Vhm.Web.Models.Enums;
using Vhm.Web.Models.RepositoryModels;
using System.Collections.Generic;
using Vhm.Web.Models.ViewModels.Member;
using VirginLifecare.Exception;

namespace Vhm.Web.BL
{
    public static class MemberBL 
    {

        private static IMemberAdapter _memberAdapter;
        private static IAccountAdapter _accountAdapter;

        static MemberBL()
        {
            Initialize();
        }

        private static void Initialize()
        {
            _memberAdapter = GetMemberAdapter();
            _accountAdapter = GetAccountAdapter();
        }

        static IMemberAdapter GetMemberAdapter()
        {
            return _memberAdapter ?? new MemberAdapter();
        }

        static IAccountAdapter GetAccountAdapter() {
            return _accountAdapter ?? new AccountAdapter();
        }

        public static void PostCookieToEloqua(string eloquaCookieGuid, long memberId)
        {
            // TODO: PostCookieToEloqua
        }

        public static bool SaveMemberPrivateSettings(string sessionID, Member member, bool deletePictureProfile)
        {
            return _memberAdapter.SaveMemberPrivateSettings(sessionID, member, deletePictureProfile);
        }

        public static List<MemberGoalsAndInterests> GetMemberGoalsAndInterests(string sessionId, long memberId)
        {
            return _memberAdapter.GetMemberGoalsAndInterests(sessionId, memberId);
        }

        public static Member GetMemberById(string sessionId, long memberId)
        {
            return _memberAdapter.GetMember(sessionId, memberId);
        }

        public static List<MemberEmail> GetMembersByEmailList(List<string> emailList)
        {
            return _memberAdapter.GetMembersByEmailList(emailList);
        }
        
        public static List<RecentlyEarnedRewardsItem> GetRecentlyEarnedRewards(long memberId, int count, bool? measurmentRewardsOnly, bool? badgesOnly)
        {
            return _memberAdapter.GetRecentlyEarnedRewards(memberId, count, measurmentRewardsOnly, badgesOnly);
        }

        public static bool BadgesEnabledForSponsor(long sponsorId)
        {
            return _memberAdapter.BadgesEnabledForSponsor(sponsorId);
        }

        public static Dictionary<T, string> GetContentLabels<T>(PageNames pageId, int languageId)
        {
            Dictionary<T, string> ContentLabels = new Dictionary<T, string>();
            //TODO: check cache for content
            try
            {
                if (pageId < 0)
                {
                    throw new ContentConfigurationException(pageId.ToString());
                }
                ContentLabels = _memberAdapter.GetContentLabels<T>((long)pageId, languageId);
            }
            catch(Exception ex){
               
            }

            return ContentLabels;

        }

        public static List<ChallengeBase> GetCorporateTrackingChallenges( long memberId, long sponsorId,
                                                                   byte displayDays, int languageId )
        {
            return _memberAdapter.GetCorporateTrackingChallenges( memberId, sponsorId, displayDays, languageId );
        }

        public static List<ChallengeBase> GetMemberChallenges(long memberId, long sponsorId, byte displayDays, int languageId)
        {
            var challenges = _memberAdapter.GetMemberChallenges(memberId, sponsorId, displayDays, languageId);
            var sorted = new List<ChallengeBase>();

            var memberNow = _memberAdapter.GetMemberNow(memberId, DateTime.Now);

            foreach (var challenge in challenges)
            {
                challenge.DaysLeft = Math.Max((int)Math.Ceiling(challenge.EndDate.GetValueOrDefault().Subtract(memberNow).TotalDays), 0);
                challenge.DaysUntilStart = (int) Math.Ceiling( challenge.StartDate.Subtract( memberNow ).TotalDays );
                challenge.Ended = challenge.EndDate < memberNow;
                challenge.Started = challenge.StartDate <= memberNow;
            }

            // Challenges should appear in the following order:
            // KAG
            // Quests - only show quests for which the member has not signed up
            // Promos
            // Corporate
            sorted.AddRange(
                challenges.Where(c => c is Quest).
                    OrderBy(c => c.StartDate).ToList());

            sorted.AddRange(
                challenges.Where(c => c is TrackingChallenge).OrderBy(
                    c => c.StartDate).ToList());

            sorted.AddRange(
                challenges.Where(c => c is Challenge).OrderBy(
                    c => c.StartDate).ToList());

            sorted.AddRange(
                challenges.Where(c => c is Promo).OrderBy(    // This will include KAG promos
                    c => c.StartDate).ToList());

            return sorted;
        }

        public static List<Lookup> GetLookup( string lookupType )
        {
            return _memberAdapter.GetLookup( lookupType );
        } 

        public static ActivityGraphData GetMemberActivityGraphData(long memberId, int graphHeight, int activityDaysToShow, int stepsTargetDefault)
        {

            var data = _memberAdapter.GetMemberActivityGraphData(memberId, DateTime.Now, activityDaysToShow);
            var stepsList = (from days in data.ActivityDays
                             select days.Steps).ToList();

            if (data.Target == 0)
            {
                data.Target = stepsTargetDefault;
            }

            decimal maxSteps = stepsList.Any() ? stepsList.Max() : 1;

            maxSteps = Math.Max(maxSteps, data.Target);

            decimal topBuffer = Convert.ToInt32(Math.Round((double)maxSteps * .10));
            maxSteps += topBuffer;
            const decimal MIN_HEIGHT = 0M;
            const decimal MIN_HEIGHT_WITH_STEPS = 5m;
            const decimal MIN_TARGET_HEIGHT = 10M;

            // We need to fill in any missing days of steps, so that we get a list of activity days
            // with no gaps. Start with a mock list of empty days, then union that with the actual list
            IEnumerable<ActivityGraphData.ActivityDay> mockList = GetMockListOfActivityDays(activityDaysToShow);

            List<ActivityGraphData.ActivityDay> actualList = data.ActivityDays;

            var joinedList = (from items in actualList
                              select items)
                .Union(from items in mockList
                       where actualList.All(d => d.ActivityDate.Day != items.ActivityDate.Day)
                       select items).Take(activityDaysToShow).ToList();

            // calculate the bar height based on the configured height of the graph
            const decimal MIN_HEIGHT_TO_SHOW_CHECKMARK = 24;
            foreach (var activityDay in joinedList)
            {
                var pct = activityDay.Steps == 0
                              ? maxSteps
                              : Math.Round((activityDay.Steps / maxSteps), 2);
                activityDay.GraphBarHeight = activityDay.Steps == 0 
                                                 ? MIN_HEIGHT
                                                 : Math.Max(graphHeight * pct, MIN_HEIGHT_WITH_STEPS);

                // determine if the member has reached their activity target
                activityDay.TargetAchieved = activityDay.Steps >= stepsTargetDefault;

                // make sure the checkmark can fit in the bar
                if (activityDay.TargetAchieved)
                {
                    activityDay.GraphBarHeight = Math.Max(activityDay.GraphBarHeight, MIN_HEIGHT_TO_SHOW_CHECKMARK);
                }
            }

            data.HasStepsForPeriod = joinedList.Any(item => item.Steps > 0);

            data.ActivityDays = joinedList.OrderBy(day => day.ActivityDate).ToList();

            var targetPct = Math.Round((data.Target / maxSteps), 2);

            data.TargetGraphPosition = data.Target == 0
                                           ? MIN_TARGET_HEIGHT
                                           : Math.Max(graphHeight * targetPct, MIN_TARGET_HEIGHT);

            // don't allow the target bar to go off the graph
            data.TargetGraphPosition = Math.Min(data.TargetGraphPosition, graphHeight);

            if (!data.HasStepsForPeriod)
            {
                data.TargetGraphPosition = new decimal(graphHeight * .70);
            }
            return data;
        }

        public static void SaveKagEntry(long promoId, long memberId, decimal score, DateTime date)
        {
            _memberAdapter.SaveKagEntry(promoId, memberId, score, date);
        }

        public static List<MemberModule> GetMemberModules(long memberId, long sponsorId)
        {
            return _memberAdapter.GetMemberModules(memberId, sponsorId);
        }

        #region Welcome Journey

        /// <summary>
        /// Used in the welcome Journey on Landing page
        /// </summary>
        /// <param name="memberId"></param>
        /// <returns>will return the current step a new member should see in WJ </returns>
        public static byte? GetMemberWelcomeJourneyCurrentStep(long memberId)
        {
            return _memberAdapter.GetMemberWelcomeJourneyCurrentStep(memberId);
        }

        public static IEnumerable<byte[]> GetMemberPasswordHistory(long memberId, int recentPasswordsCount)
        {
            return _memberAdapter.GetMemberPasswordHistory(memberId, recentPasswordsCount);
        }

        /// <summary>
        /// will be used in Welcome Journey for saving the step a member
        /// </summary>
        /// <param name="memberId">not null</param>
        /// <param name="currentStep">not null</param>
        /// <returns>if save is successfull, we'll return currentStep + 1, otherwise 0</returns>
        public static byte SaveMemberWelcomeJourneyCurrentStep(long memberId, byte currentStep)
        {
            byte nextStep = 0;
            if (memberId > 0 && currentStep > 0)
            {
                var saveThisStep = _memberAdapter.SaveMemberWelcomeJourneyCurrentStep(memberId, currentStep);
                if (saveThisStep)
                {
                    nextStep = Convert.ToByte(currentStep + 1);
                }
            }
            return nextStep;
        }
        /// <summary>
        /// To be used in Welcome Journey, step one; Manage My account - display settings in the future
        /// </summary>
        /// <returns> a list of all available cultures we have for members to pick from</returns>
        public static List<CultureFormat> GetCultureFormats()
        {
            return _memberAdapter.GetCultureFormats();
        }

        public static MemberPersonalInformation GetMemberPersonalInformation(long memberId)
        {
            return _memberAdapter.GetMemberPersonalInformation(memberId);
        }
        public static bool SaveMemberPersonalInformation(MemberPersonalInformation member)
        {
            return _memberAdapter.SaveMemberPersonalInformation(member);
        }
        public static decimal InsertMeasurementAssessmentGrouping(Int16 assessmentGroupingTypeID)
        {
            return _memberAdapter.InsertMeasurementAssessmentGrouping(assessmentGroupingTypeID);
        }
        public static long MeasurementInsert(long? assessmentGroupingID, Int16 measurementTypeID, long performedByEntityID,
                                 long memberEntityID, Int16 statusID, Int32 sourceID, double? unSystemValue,
                                 double? unUserValue, long? groupID, DateTime udTimeZoneDate, bool? validated,
                                 byte? dataCollectionMethodID, long? sourceDataReferenceID)
        {
            return _memberAdapter.InsertMeasurement(assessmentGroupingID, measurementTypeID, performedByEntityID,
                                                     memberEntityID, statusID, sourceID, unSystemValue, unUserValue,
                                                     groupID,
                                                     udTimeZoneDate, validated, dataCollectionMethodID,
                                                     sourceDataReferenceID);
        }
        public static bool SaveEmailPromptFlag(long entityID, DateTime? hzAccess, DateTime? hcAccess, DateTime? faDate,
                                  DateTime? questStart, DateTime? logExer, DateTime? exeProg, DateTime? completedReg,
                                  DateTime? stepsLastUploaded, DateTime? noSmokingAgreement, DateTime? measurementsTaken)
        {
            return _memberAdapter.SaveEmailPromptFlag(entityID, hzAccess, hcAccess, faDate, questStart, logExer, exeProg,
                                               completedReg, stepsLastUploaded, noSmokingAgreement, measurementsTaken);
        }
        /// <summary>
        /// saves the option member selects during Welcome Journey's last step. We need to update the session to reflect this. We need to update the curent session only if option -1:remind me later, the member should be prompted next time he/she logs in. If the option selected is 1:Don't remind we need to update the current session and database so after session expires, member doesn't get the pop up again. If they choose 2:take me there, then after HRA is completed this info will be updated so no need for any saves of this kind
        /// </summary>
        /// <param name="session"></param>
        /// <param name="timeout">timeout value</param>
        /// <returns>true if successful</returns>
        public static bool SaveHealthSnapshotOption(Session session, int timeout)
        {
            var updateDoNotRemind = true;
            //we need to update the session--pass in the session
            var updateSession = _accountAdapter.UpdateSession(session, timeout);
            if (session.HSPopupReminder == 1)
            {
                //we need to save the "do not remind" option for member
                updateDoNotRemind = UpdateDoNotRemindHRAOption(session.MemberID);
            }
            return (updateSession && updateDoNotRemind);
            
        }
        /// <summary>
        /// use this to update the VhmSession middle tier object. make sure you invalidate the cached session objects after calling this method. I am calling _accountAddapter as that is where all logic for Session lives. here as 
        /// </summary>
        /// <param name="session"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public static bool UpdateVhmSession(Session session, int timeout)
        {
            return _accountAdapter.UpdateSession(session, timeout);
        }
        #endregion

        #region Redeem a Voucher
        public static List<Voucher> CheckVoucherByCode(string voucherCode)
        {
            return _memberAdapter.CheckVoucherByCode(voucherCode);
        }
        public static bool RedeemRewardsVoucher(Voucher voucher, long memberId)
        {
            return _memberAdapter.RedeemRewardsVoucher(voucher, memberId);
        }

        #endregion

        #region Private Methods

        private static IEnumerable<ActivityGraphData.ActivityDay> GetMockListOfActivityDays(int activityDaysToShow)
        {
            var list = new List<ActivityGraphData.ActivityDay>();

            for (var i = 0; i < activityDaysToShow; i++)
            {
                list.Add(new ActivityGraphData.ActivityDay
                              {
                                  Steps = 0,
                                  ActivityDate = DateTime.Now.AddDays(-i)
                              });
            }
            return list;
        }

        private static bool UpdateDoNotRemindHRAOption(long memberId)
        {
            return _memberAdapter.UpdateDoNotRemindHRAOption( memberId );
        }


        #endregion

        #region Terms and Conditions

        public static List<Terms> GetTerms(long memberId, string termsCode, int version)
        {
            return _memberAdapter.GetTerms(memberId, termsCode, version);
        }

        public static List<Terms> GetTermsAndConditions(long memberId)
        {
            return _memberAdapter.GetTermsAndConditions(memberId);
        }

        public static List<Terms> GetTermsToAcknowledge(long memberId)
        {
            return _memberAdapter.GetTermsToAcknowledge(memberId);
        }

        public static bool UpdateAcknowledgeMemberTerms(string sessionID, int timeOut, string termsCode, bool accepted, string source, long memberID)
        {
            return _memberAdapter.UpdateAcknowledgeMemberTerms(sessionID, timeOut, termsCode, accepted, source, memberID);
        }
        #endregion

        public static Tickers GetTickers(long memberId, long sponsorId)
        {
            return _memberAdapter.GetTickers( memberId, sponsorId ); 
        }

        public static DateTime GetMemberNow(long memberId, DateTime systemDate)
        {
            return _memberAdapter.GetMemberNow(memberId, systemDate);
        }
        public static DateTime GetMemberTimeByUtcTime(long memberId, DateTime utcTime)
        {
            return _memberAdapter.GetMemberTimeByUtcTime(memberId, utcTime);
        }
        public static bool UpdateLastHraDate(long memberId)
        {
            return _memberAdapter.UpdateLastHraDate( memberId );
        }
        /// <summary>
        /// Get a list of all bad words
        /// </summary>
        /// <param name="badWordId">if null, returns all bad words</param>
        /// <returns>a list of bad words</returns>
        public static List<BadWords> GetBadWords(int? badWordId)
        {
            return _memberAdapter.GetBadWords(badWordId);
        }

        public static NoSmokingViewModel GetNoSmokingViewModel(long memberId, long sponsorId, DateTime anniversaryDate)
        {

            var modules = GetMemberModules( memberId, sponsorId );
            var hra = modules.FirstOrDefault( m => m.ModuleTypeCode == ModuleEnum.HRA.ToString() );
            var model = new NoSmokingViewModel();
            model.PageAccessible = true;
            if (hra != null)
            {
                model.HraName = hra.HRAName;
            }
            
            //model.PageAccessible = true;
            var smokingInfo = GetNonSmokingInfo( memberId , anniversaryDate );
            foreach ( var info in smokingInfo )
            {
                switch ( info.QuestionDesc.ToLower().Trim() )
                {
                    case "current smoker":
                        model.CurrentSmokerOn = !string.IsNullOrEmpty(info.QuestionAns);
                        break;
                    case "i am thinking aboutà":
                        model.IntentToQuit = !string.IsNullOrEmpty(info.QuestionAns);
                        break;
                    case "i have no intention..":
                        model.NoIntentToQuit = !string.IsNullOrEmpty(info.QuestionAns);
                        break;
                    case "non-smoker":
                        model.NonSmokerOn = !string.IsNullOrEmpty(info.QuestionAns);
                        break;
                    case "i agree":
                        model.AlreadyAgreed =  !string.IsNullOrEmpty(info.QuestionAns);
                        break;
                }
            }
            return model;
        }

        public static List<SmokingInfo> GetNonSmokingInfo(long memberId, DateTime anniversaryDate)
        {
            return _memberAdapter.GetSmokingInfo( memberId, anniversaryDate );
        }

        public static int SaveNoSmoking(long memberId, bool isAgreed)
        {
            var  oMemData= RewardsBL.GetMemberRewardsBalnace( memberId );
            throw new NotImplementedException();
        }

       public static List<MemberSegmentation> GetMemberSegmentations (long memberId)
        {
            return _memberAdapter.GetMemberSegmentation(memberId);
        }       

       public static long GetHmToNextLevel(Game game)
        {
           var hmToNextLevel = 0;
           try
           {
               if (game != null)
               {
                   var currentMilestone = game.Milestones.SingleOrDefault(m => m.IsCurrentMilestone);
                   if (currentMilestone != null)
                   {
                       var nextIdx = game.Milestones.IndexOf(currentMilestone) + 1;
                       nextIdx = nextIdx == game.Milestones.Count ? game.Milestones.Count - 1 : nextIdx;
                       var nextMilestone = game.Milestones[nextIdx];
                       hmToNextLevel = Convert.ToInt32(nextMilestone.Value) - Convert.ToInt32(game.MemberTotalHealthMiles);
                   }
               }
           }
           catch ( Exception ex)
           {
               VirginLifecare.Exception.ExceptionHandler.ProcessException( new Exception("Error getting HM to next level", ex));
           }
            
           return hmToNextLevel;
        }

       public static List<EktronMenuItem> GetEktronMenuById(long MenuId, int lang)
       {
           return _memberAdapter.GetEktronMenu(MenuId,lang);
       }

        public static List<MemberBadge> GetMemberBadges( long memberId, long? communityId, bool? celebrationOnly )
        {
            return _memberAdapter.GetMemberBadges( memberId, communityId, celebrationOnly );
        }

        public static bool SaveRewardedTaskCelebration( long memberId, long? communityId, bool? celebrationOnly )
        {
            return _memberAdapter.SaveRewardedTaskCelebration(memberId, communityId, celebrationOnly);
        }

        public static List<MemberDeviceOrder> GetMemberDeviceOrders( long memberId )
        {
            return _memberAdapter.GetMemberDeviceOrders(memberId);
        }

        public static bool CheckForRegisteredDevice( long memberId )
        {
            return _memberAdapter.CheckForRegisteredDevice( memberId );
        }
    }

}
