﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Vhm.Web.Common;
using Vhm.Web.DataServices.Adapters;
using Vhm.Web.DataServices.Interfaces;

using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.BL
{
    public static class RewardsBL
    {
        private static readonly IRewardsAdapter _rewardsAdapter;
        private static readonly ISocialAdapter _socialAdapter;
        private static readonly IMemberAdapter _memberAdapter;

        static RewardsBL()
        {
            _rewardsAdapter = GetRewardsAdapter();
            _socialAdapter = GetSocialAdapter();
            _memberAdapter = GetMemberAdapter();
        }

        private static ISocialAdapter GetSocialAdapter()
        {
            return _socialAdapter ?? new SocialAdapter();
        }

        private static IMemberAdapter GetMemberAdapter() {
            return _memberAdapter ?? new MemberAdapter();
        }

        private static IRewardsAdapter GetRewardsAdapter() {
            return _rewardsAdapter ?? new RewardsAdapter();
        }         

        public static bool MemberEarnedReward(Member member, string triggerCode)
        {
            return false;
        }

        public static bool TriggerReward( long memberId, string rewardCode, bool processNow, bool supressResult,
            DateTime triggerDate, long challengeTaskId )
        {
            return _rewardsAdapter.TriggerReward( memberId, rewardCode, processNow, supressResult, triggerDate,
                challengeTaskId );
        }

        public static List<UpForGrabsItem> GetMemberAvailableUpForGrabs(long memberId, long sponsorId)
        {
            //var ufgList = CacheUI.Get(CacheProvider.CacheItems.UpForGrabsItem) as List<UpForGrabsItem>;

            //if (ufgList == null)
            //{

            var ufgList = _rewardsAdapter.GetMemberAvailableUpForGrabs(memberId);
            var challenges = _memberAdapter.GetMemberChallenges(memberId, sponsorId, ApplicationSettings.ChallengeEndedDisplayDays, ApplicationSettings.DefaultLanguage);
            // Showing One challenge in UPFG
            var kag = challenges.FirstOrDefault(c => c.ChallengeType == ChallengeBase.CHALLENGE_TYPE_KAG);
            var challenge = challenges.FirstOrDefault(c => (c.ChallengeType == ChallengeBase.CHALLENGE_TYPE_CORP || c.ChallengeType == ChallengeBase.CHALLENGE_TYPE_CORP_SIGN_UP)
                && !c.SignedUp); 
            ChallengeBase quest = null;
            if (challenges != null)
            {
                // Showing One Quest in UPFG
                quest = challenges.FirstOrDefault(c => c.ChallengeType == ChallengeBase.CHALLENGE_TYPE_QUEST);
            }

            const string QUEST_PLACEHOLDER = "«questid»";
            const string QUEST_NAME_PLACEHOLDER = "«questname»";
            const String CHALLENGE_PLACEHOLDER = "«challengeid»";
            const String CHALLENGE_NAME_PLACEHOLDER = "«challengename»";
            // .... merge challenge with UFG
            if (kag != null)
            {
                //Replacing challengeId and ChallengeName with Member's challenge
                if (kag.MenuDisplay)// / If MenuDisplay == false it means don't show the SEC pag
                {
                    // challenge sign up bonus doesn't have a specific challenge id. assign the first challenge
                    // if the promoId is null
                    ufgList.Where(ug => ug.UFGTaskRewardURL != null
                                    && ug.UFGTaskRewardURL.ToLower().IndexOf(CHALLENGE_PLACEHOLDER, StringComparison.Ordinal) >= 0
                                    && (ug.PromoId == kag.ChallengeId))
                        .ToList()
                        .ForEach(ufg => 
                            ufg.UFGTaskRewardURL = ufg.UFGTaskRewardURL.ToLower()
                            .Replace(CHALLENGE_PLACEHOLDER, kag.ChallengeId.ToString(CultureInfo.InvariantCulture))
                            .Replace(CHALLENGE_NAME_PLACEHOLDER, kag.Name));
                }
                else
                {
                    ufgList.Where(ug => ug.UFGTaskRewardURL != null 
                                    && ug.UFGTaskRewardURL.ToLower().IndexOf(CHALLENGE_PLACEHOLDER, StringComparison.Ordinal) > 0)
                                    .ToList()
                                    .ForEach(ufg => ufg.UFGTaskRewardURL = string.Empty);
                }
            }

            // corporate challenges, probably challenge sign up bonus
            if (challenge != null)
            {
                ufgList.Where(ug => ug.UFGTaskRewardURL != null
                                     && ug.UFGTaskRewardURL.ToLower().IndexOf(CHALLENGE_PLACEHOLDER, StringComparison.Ordinal) >= 0)
                         .ToList()
                         .ForEach(ufg =>
                             ufg.UFGTaskRewardURL = ufg.UFGTaskRewardURL.ToLower()
                             .Replace(CHALLENGE_PLACEHOLDER, challenge.ChallengeId.ToString(CultureInfo.InvariantCulture))
                             .Replace(CHALLENGE_NAME_PLACEHOLDER, challenge.Name)); 
            }

            if (quest != null)
            {
                if (quest.MenuDisplay)
                {
                    //Replacing questId and questName with Member's Quest
                    ufgList.Where(ug => ug.UFGTaskRewardURL != null 
                                && ug.UFGTaskRewardURL.IndexOf(QUEST_PLACEHOLDER, StringComparison.Ordinal) > 0 
                                && ug.PromoId == quest.ChallengeId)
                            .ToList()
                            .ForEach(ufg => ufg.UFGTaskRewardURL = ufg.UFGTaskRewardURL.ToLower()
                                .Replace(QUEST_PLACEHOLDER, quest.ChallengeId.ToString(CultureInfo.InvariantCulture))
                                .Replace(QUEST_NAME_PLACEHOLDER, quest.Name));
                }
                else
                {
                    ufgList.Where(ug => ug.UFGTaskRewardURL != null 
                                && ug.UFGTaskRewardURL.ToLower().IndexOf(QUEST_PLACEHOLDER, StringComparison.Ordinal) > 0)
                            .ToList()
                            .ForEach(ufg => ufg.UFGTaskRewardURL = string.Empty);
                }
            }

            ufgList.RemoveAll(ug => ug.UFGTaskRewardURL != null && (ug.UFGTaskRewardURL.ToLower().IndexOf(CHALLENGE_PLACEHOLDER, StringComparison.Ordinal) > 0 || ug.UFGTaskRewardURL.IndexOf(QUEST_PLACEHOLDER, StringComparison.Ordinal) > 0));

            //Adding Challenge ID at the end of the URL. The URL comes in Like this "/secure/Challenge/SEChallenge.aspx?secId="
            var secURL = ApplicationSettings.SECPageUrl;
            List<ChallengeBase> promoChallenge = null;
            if (challenges != null)
            {
                promoChallenge = challenges.Where(c => c.MenuDisplay).ToList();
            }
            if (promoChallenge != null)
            {

                ufgList.Where(ufg => ufg.PromoId != null && promoChallenge.Any(p => p.ChallengeId == ufg.PromoId)).ToList()
                    .ForEach(ug => ug.UFGTaskRewardURL = string.Format(secURL, ug.PromoId));
            }
            //    CacheUI.Update(CacheProvider.CacheItems.UpForGrabsItem, ufgList);
            //}
            return ufgList;

        }
       
        public static List<Reward> GetGameRewardTypes(List<Milestone> milestones )
        {

            var rewardTypes = new List<Reward>();
            foreach (var milestone in milestones)
            {
                var rewards = ( from reward in milestone.Rewards
                                where !rewardTypes.Any( r => r.RewardType == reward.RewardType )
                                select reward ).ToList();

                // ensure that we don't have duplicate types by checking rewardTypes.All() 
                //do not dispaly  Certificates or Text only as rows in the rewards table. see bug 7782
                rewardTypes.AddRange(
                    rewards.Where(
                        r =>
                        rewardTypes.All( rt => rt.RewardType != r.RewardType ) &&
                        r.RewardType != "BADGE" && r.RewardType.Trim() != "TEXT" &&
                        r.RewardType.Trim() != "CERT" ) );

            }
            rewardTypes.ForEach(r => r.RewardType = r.RewardType.Trim());

            return rewardTypes;
        }

        public static List<GameCurrency> GetGameCurrencies()
        {
            return _rewardsAdapter.GetGameCurrencies();
        }

        public static Game GetSponsorCurrentGame(string sessionId, Member member, long sponsorId)
        {
            var currentGame = _rewardsAdapter.GetSponsorCurrentGame(member.MemberId, sponsorId);
            
            if (currentGame != null)
            {
                if (currentGame.Milestones == null)
                {
                    currentGame.Milestones = new List<Milestone>();
                }
                
                if (currentGame.Milestones.Any())
                {
                    currentGame = currentGame.GameDisplayType == "PRG"
                                      ? CalculateTaskProgressDisplay( currentGame )
                                      : CalculateLevelsProgressDisplay( currentGame );

                    // If the program is a rolling member anniversary, display the date to be one day less than 
                    // a year from the Member Anniversary Date 
                    currentGame.EndDate = currentGame.EndDate.HasValue
                                              ? currentGame.EndDate.Value
                                              : member.RewardsAniversary.GetValueOrDefault().AddYears( 1 ).AddDays( -1 );

                    // double check to make sure we have a valid date
                    if ( currentGame.EndDate.GetValueOrDefault().Year == 1 )
                    {
                        currentGame.EndDate = null;
                    }
                }
            }

            return currentGame;
        }

        private static Game CalculateLevelsProgressDisplay(Game currentGame)
        {
            var maxProgress = currentGame.Milestones.Max().Value;
            if (maxProgress > 0)
            {
                var currentHealthMiles = currentGame.MemberTotalHealthMiles;
                decimal totalRewardThreshold = 0;

                for (int idx = 0; idx < currentGame.Milestones.Count; idx++)
                {
                    var milestone = currentGame.Milestones[idx];
                    totalRewardThreshold = milestone.Value;
                    if (idx + 1 < currentGame.Milestones.Count)
                    {
                        var nextMilestone = currentGame.Milestones[idx + 1];

                        // for Task types (game display type = 'PRG'), compare currentHealthMiles to 
                        // the first milestone on iteration 0. Levels start with the first milestone
                        // having 0 for the threshold. Tasks do not. 
                        if (currentHealthMiles >= nextMilestone.Value)
                        {
                            milestone.RelativeProgress = 100;
                        }
                        else
                        {
                            decimal cumulativeProgress = (nextMilestone.Value - totalRewardThreshold);
                            //var value = nextMilestone.Value - cumulativeProgress;
                            var value = nextMilestone.Value - milestone.Value;
                            if (idx > 0 && value > 0)
                            {
                                currentHealthMiles = currentHealthMiles - Convert.ToInt64(milestone.Value);
                                milestone.RelativeProgress = (currentHealthMiles / value) * 100;
                            }
                            else
                            {
                                milestone.RelativeProgress = (currentHealthMiles / cumulativeProgress) * 100;
                            }
                            milestone.ActualProgress = milestone.RelativeProgress;
                            milestone.RelativeProgress = Math.Max(milestone.RelativeProgress, 15);
                            milestone.RelativeProgress = Math.Round(milestone.RelativeProgress, 0);
                            milestone.IsCurrentMilestone = true;
                            break;
                        }
                    }
                    else
                    {
                        decimal cumulativeProgress = milestone.Value;
                        currentHealthMiles -= Convert.ToInt64(cumulativeProgress);
                        currentHealthMiles = Math.Max( currentHealthMiles, 1 );
                        milestone.RelativeProgress = (currentHealthMiles / cumulativeProgress) * 100;

                        // never show more than 90% complete for the last milestone
                        milestone.RelativeProgress = Math.Min(milestone.RelativeProgress, 95);
                        
                        milestone.IsCurrentMilestone = true;
                    }

                }
            }

            return currentGame;
        }

        private static Game CalculateTaskProgressDisplay(Game currentGame)
        {
           
            var maxProgress = currentGame.Milestones.Max().Value;
            if (maxProgress > 0)
            {
                var currentHealthMiles = currentGame.MemberTotalHealthMiles;
                decimal currentMilestoneMax = 0;
                decimal currentMilestoneMin = 0;
                decimal currentMilestoneRange = 1;
                for (int idx = 0; idx < currentGame.Milestones.Count; idx++)
                {
                    var milestone = currentGame.Milestones[idx];
                    if (idx < currentGame.Milestones.Count)
                    {
                        currentMilestoneMin = currentMilestoneMax;
                        currentMilestoneMax = milestone.Value;
                        currentMilestoneRange = currentMilestoneMax - currentMilestoneMin;

                        // for Task types (game display type = 'PRG'), compare currentHealthMiles to 
                        // the first milestone on iteration 0. Levels start with the first milestone
                        // having 0 for the threshold. Tasks do not. 
                        if (currentHealthMiles >= currentMilestoneMax)
                        {
                            milestone.RelativeProgress = 100;
                        }
                        else
                        {
                            milestone.RelativeProgress = ((currentHealthMiles - currentMilestoneMin) / currentMilestoneRange) * 100;
                            milestone.RelativeProgress = (currentGame.Milestones.Count == 1) ? milestone.RelativeProgress : Math.Max(milestone.RelativeProgress, 15);
                            milestone.RelativeProgress = Math.Round(milestone.RelativeProgress, 0);
                            milestone.IsCurrentMilestone = true;
                            break;
                        }
                    }
                    else
                    {
                        decimal cumulativeProgress = milestone.Value;
                        milestone.RelativeProgress = (currentHealthMiles / cumulativeProgress) * 100;
                        milestone.RelativeProgress = Math.Min(milestone.RelativeProgress, 100);
                        // never show more than 90% complete for the last milestone
                        milestone.IsCurrentMilestone = true;
                    }
                }
            }
            return currentGame;
        }
        public static List<MemberLevelsAudit> GetMemberLevelsAudit(long memberId, long sponsorId)
        {
            return _rewardsAdapter.GetMemberLevelsAudit(memberId, sponsorId);
        }

        public static BadgeItem GetBadge(long badgeId)
        {
            return _rewardsAdapter.GetBadges(badgeId);
        }

        public static List<GameTrigger> GetSystemTriggers(long memberId, long sponsorId)
        {
            return _rewardsAdapter.GetSystemTriggers(memberId, sponsorId);
        }

        public static  MemberRewardsData GetMemberRewardsData( long memberId )
        {
            return _rewardsAdapter.GetMemberRewardsData( memberId );
        }

        public static MemberRewardsBalance GetMemberRewardsBalnace( long memberId )
        {
            return _rewardsAdapter.GetMemberRewardsBalnace(memberId);
        }
    }
}
