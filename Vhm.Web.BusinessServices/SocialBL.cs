﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Vhm.Web.BL.SocialWebService;
using Vhm.Web.Common;
using Vhm.Web.DataServices.Adapters;
using Vhm.Web.DataServices.Interfaces;
using Vhm.Web.DataServices.MiddleTierService;
using Vhm.Web.Models.Enums;
using Vhm.Web.Models.InputModels;
using Vhm.Web.Models.RepositoryModels;
using Vhm.Web.Models.ViewModels.Shared;
using Vhm.Web.Models.ViewModels.Social;
using System;
using System.Net;
using Vhm.Web.Common.Utils;


namespace Vhm.Web.BL
{
    public static class SocialBL
    {
        private static readonly ISocialAdapter _socialAdapter;
        private static readonly IMemberAdapter _memberAdapter;

        static SocialBL()
        {
            _socialAdapter = GetSocialAdapter();
            _memberAdapter = GetMemberAdapter();
        }

        private static IMemberAdapter GetMemberAdapter()
        {
            return _memberAdapter ?? new MemberAdapter();
        }

        private static ISocialAdapter GetSocialAdapter()
        {
            return _socialAdapter ?? new SocialAdapter();
        }

        public static int GetMemberFriendCount(long memberId)
        {
            return _socialAdapter.GetMemberFriendCount(memberId);
        }

        public static int GetMemberGroupCount(long memberId)
        {
            return _socialAdapter.GetMemberGroupCount(memberId);
        }

        public static SocialPagedData GetMemberFriends(string sessionId, PagingIM paging)
        {
            return _socialAdapter.GetMemberFriends(sessionId, paging.MemberId, paging.PageNumber, paging.Count);
        }
        
        /// <summary> used to get paged groups that member is part of. </summary>
        /// <param name="sessionId">valid session id</param>
        /// <param name="types"> type of Groups we need to be returned, in this case it's going to be Member Groups - type= MEM</param>
        /// <param name="paging">paging object that needs to have MemberId, PageNumber, Count populated with the needed inputs</param>
        /// <param name="includeImages">boolean. Pass in true if we need group images to be included in result object as a propeerty of the Group object. Pass in false otherwise.</param>
        /// <returns>Returns a SocialPagedData type of object - containing pagination info-SocialPagedData = defined in Vhm.Web.Models.RepositoryModels.SocialPagedData </returns>
        public static SocialPagedData GetMemberGroups(string sessionId, List<GroupTypes> types, PagingIM paging, bool includeImages)
        {
            return _socialAdapter.GetMemberGroups(sessionId, paging.MemberId, types, paging.PageNumber, paging.Count, includeImages);
        }

        public static SocialPagedData GetSuggestedGroups(string sessionId, PagingIM paging, bool includePic)
        {
            return _socialAdapter.GetMemberSuggestedGroups(sessionId, paging.PageNumber, paging.Count, false);
        }

        /// <summary>
        /// used to get all the suggested groups = public static groups from the same comunity as member, groups that member is not a part of yet.
        /// </summary>
        /// <param name="sessionId">valid session id</param>
        /// <param name="paging">paging object that needs to have MemberId - must have, PageNumber - can be null, Count - can be null Populate with the page number needed to display and number of items per page to show</param>
        /// <param name="includePic">true if we need group images to be included in result object as a propeerty of the Group object. Pass in false otherwise.</param>
        /// <returns>PagedGroupsVM type of object - containing pagination info - of Member's groups. Find this object declaration at Vhm.Web.Models.ViewModels.Social.PagedGroupsVM</returns>
        public static PagedGroupsVM GetListOfSuggestedGroups(string sessionId, PagingIM paging, bool includePic)
        {
            var memberSuggestedGroups = _socialAdapter.GetMemberSuggestedGroups(sessionId, paging.PageNumber, paging.Count, false);
            var suggestedGroups = GetSimpleListOfGroupDetails(memberSuggestedGroups.SuggestedGroups, paging.MemberId, false);

            var suggGrpPagination = new PagingDataVM
                                        {
                                            HasMoreItems = memberSuggestedGroups.HasMore,
                                            TotalNumberOfItems = memberSuggestedGroups.TotalCount,
                                            CurentPageNumber =
                                                (paging.PageNumber != null)
                                                    ? Convert.ToInt32(paging.PageNumber)
                                                    : 0,
                                            TotalNumberOfPages = memberSuggestedGroups.TotalPageCount,
                                            PageSize =
                                                (paging.Count != null) ? Convert.ToInt32(paging.Count) : 0
                                        };
            var returnSuggestedGrps = new PagedGroupsVM
                                          {
                                              Groups = suggestedGroups,
                                              GroupsPaginationInfo = suggGrpPagination,
                                              PagingGroupsSource = "suggestedGroups"
                                          };
            return returnSuggestedGrps;
        }

        public static MyGroupsModel GetMyGroupsObjects(string sessionId, long memberId, PagingIM pagingMyGroups, PagingIM pagSuggestedGrps, bool includeImages)
        {
            var myGrps = new MyGroupsModel();
            var types = new List<GroupTypes> { GroupTypes.MEM };
            var pagedGroups = GetMemberGroups(sessionId, types, pagingMyGroups, includeImages);
            #region Set up pagination

            var myGrpsPagination = new PagingDataVM
                                       {
                                           HasMoreItems = pagedGroups.HasMore,
                                           TotalNumberOfItems = pagedGroups.TotalCount,
                                           CurentPageNumber =
                                               (pagingMyGroups.PageNumber != null)
                                                   ? Convert.ToInt32(pagingMyGroups.PageNumber)
                                                   : 0,
                                           TotalNumberOfPages = pagedGroups.TotalPageCount,
                                           PageSize =
                                               (pagingMyGroups.Count != null)
                                                   ? Convert.ToInt32(pagingMyGroups.Count)
                                                   : 0
                                       };
            myGrps.MyGroupsPagination = myGrpsPagination;
            #endregion

            #region My Groups section
            var myGroupsList = GetSimpleListOfGroupDetails(pagedGroups.Groups, pagingMyGroups.MemberId, true);
            myGrps.PagedGroups = new PagedGroupsVM
                                     {
                                         Groups = myGroupsList,
                                         GroupsPaginationInfo = myGrpsPagination,
                                         PagingGroupsSource = "myGroups"
                                     };
            #endregion
            #region Group Invitations section
            string invitMessage = "";
            //we need member notifications and we count out how many pending group invitations we have
            var memberNotific = _socialAdapter.GetMemberNotifications(sessionId, memberId);
            if (memberNotific != null && memberNotific.Any())
            {
                var grpInvitNotific = (from MemberNotification mn in memberNotific
                                       where mn.NotificationTypeId == NotificationType.GROUPINVITATION
                                       select mn).ToList();
                if (grpInvitNotific.Any())
                {
                    int numberOfInvitations = grpInvitNotific.Count();
                    myGrps.HasGroupInvitations = true;
                    invitMessage = numberOfInvitations == 1 ? "1 Group Invitation" : string.Format("{0} Group Invitations", numberOfInvitations);
                }
                else
                {
                    myGrps.HasGroupInvitations = false;
                }
            }
            else
            {
                myGrps.HasGroupInvitations = false;
            }
            myGrps.GroupInvitationsMessage = invitMessage;
            #endregion
            #region Group Categories
            var listOfCateg = _socialAdapter.GetAllGroupCategories(sessionId);
            var listOfCategIds = new List<long>();
            if (listOfCateg != null)
            {
                myGrps.AllGroupCategory = listOfCateg;
                listOfCategIds.AddRange(listOfCateg.Select(gc => gc.GroupCategoryId).Select(dummy => (long)dummy));
            }
            #endregion
            #region Suggested Groups
            var listOfSuggGrpsPaged = _socialAdapter.GetMemberSuggestedGroups(sessionId, pagSuggestedGrps.PageNumber, pagSuggestedGrps.Count, false);
            var suggestedGroups = GetSimpleListOfGroupDetails(listOfSuggGrpsPaged.SuggestedGroups, pagSuggestedGrps.MemberId, false);
            myGrps.SuggestedGroups = suggestedGroups;
            #endregion

            return myGrps;
        }

        public static PagedGroupsVM GetMyGroupsPaged(string sessionId, PagingIM pagingMyGroups, bool includeImages)
        {
            var types = new List<GroupTypes> { GroupTypes.MEM };
            var pagedGroups = GetMemberGroups(sessionId, types, pagingMyGroups, includeImages);
            #region Set up pagination

            var myGrpsPagination = new PagingDataVM
                                       {
                                           HasMoreItems = pagedGroups.HasMore,
                                           TotalNumberOfItems = pagedGroups.TotalCount,
                                           CurentPageNumber =
                                               (pagingMyGroups.PageNumber != null)
                                                   ? Convert.ToInt32(pagingMyGroups.PageNumber)
                                                   : 0,
                                           TotalNumberOfPages = pagedGroups.TotalPageCount,
                                           PageSize =
                                               (pagingMyGroups.Count != null)
                                                   ? Convert.ToInt32(pagingMyGroups.Count)
                                                   : 0
                                       };
            #endregion
            #region My Groups section
            var myGroupsList = GetSimpleListOfGroupDetails(pagedGroups.Groups, pagingMyGroups.MemberId, true);
            var pdGroups = new PagedGroupsVM
                               {
                                   Groups = myGroupsList,
                                   GroupsPaginationInfo = myGrpsPagination,
                                   PagingGroupsSource = "myGroups"
                               };
            #endregion
            return pdGroups;
        }

        public static bool AddMemberToGroup(string sessionId, Group group, long memberId)
        {
            bool hasBeenAddedSuccessfully = false;
            long membGrpId = _socialAdapter.AddMemberToGroup(sessionId, group, memberId);
            if (membGrpId > 0)
            {
                hasBeenAddedSuccessfully = true;
            }
            return hasBeenAddedSuccessfully;
        }

        public static SocialPagedData GetGroupMembers(string sessionId, long groupId, int? pageNumber, int? count, bool includeImage)
        {
            return _socialAdapter.GetGroupMembers(sessionId, groupId, pageNumber, count, includeImage);
        }
        public static List<MemberVM> GetListOfGroupMembers(string sessionId, long groupId, int? pageNumber, int? count, bool includeImage)
        {
            var membList = GetGroupMembers(sessionId, groupId, pageNumber, count, includeImage);
            return (from mG in membList.GroupMembers select new MemberVM { Member = mG }).ToList();
        }
        public static CreateEditGroupVM GetCreateEditGroupVM(string sessionId, PagingIM paging, long? id, string mode, long memberId)
        {

            var createEditGroupVm = new CreateEditGroupVM { Mode = mode };

            if (id == null)
            {
                createEditGroupVm.Mode = "Create";
            }
            else if (mode != "Create_1")
            {
                createEditGroupVm.Mode = "Edit";
                createEditGroupVm.GroupVM.Group = GetGroupDetails(sessionId, id.Value);
                if (createEditGroupVm.GroupVM.Group.AdminId != memberId)
                    return null;
            }
            if (id != null)
            {
                createEditGroupVm.GroupVM.Group.Id = id.Value;
            }
            createEditGroupVm.Categories = GetAllGroupCategories(sessionId);
            createEditGroupVm.Invite.InviteFriends.HeaderText = "Select Friends to invite to this group";
            createEditGroupVm.Invite.InviteFriends.HasSelectAll = true;
            createEditGroupVm.Invite.InviteFriends.HasAlphabet = true;
            createEditGroupVm.Invite.InviteFriends.HasViewAll = true;

            var memberFriends = GetMemberFriends(sessionId, paging);
            createEditGroupVm.Invite.InviteFriends.TotalPageCount = memberFriends.TotalPageCount;

            createEditGroupVm.Invite.InviteGroups.HeaderText = "Select other Groups to invite to this group";
            createEditGroupVm.Invite.InviteGroups.HasSelectAll = false;
            createEditGroupVm.Invite.InviteGroups.HasAlphabet = false;
            createEditGroupVm.Invite.InviteGroups.HasViewAll = true;
            createEditGroupVm.Invite.InviteGroups.TotalPageCount = GetMemberGroupCount(memberId);

            return createEditGroupVm;
        }
        public static List<MemberVM> GetMembersByEmailsOrIdentityNumbers(string sessionId, IEnumerable<string> emailsAndidentityNumbers)
        {
            var emails = new List<string>();
            var identityNumbers = new List<string>();
            const int VHMIDENITYNUMBERLENGTH = 10;
            foreach (string item in emailsAndidentityNumbers)
            {
                if (item.IndexOf("@", StringComparison.Ordinal) > 0)
                {
                    var regex = new System.Text.RegularExpressions.Regex(@"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$");
                    System.Text.RegularExpressions.Match match = regex.Match(item);
                    if (match.Success)
                        emails.Add(item);
                }
                else if (item.Length == VHMIDENITYNUMBERLENGTH)
                {
                    identityNumbers.Add(item);
                }
            }
            return GetMembersByEmailsOrIdentityNumbers(sessionId, emails, identityNumbers);
        }

        public static List<MemberVM> GetMembersByEmailsOrIdentityNumbers(string sessionId, IEnumerable<string> emails, IEnumerable<string> identityNumbers)
        {
            var members = _socialAdapter.GetMembersByEmailsOrIdentityNumbers(sessionId, emails, identityNumbers);

            return members.Select(m => new MemberVM { Member = m }).ToList();
        }

        public static List<MemberVM> GetMembersByIds(string sessionId, List<long> mIds)
        {
            var members = _socialAdapter.GetMembersById(sessionId, mIds);

            return members.Select(m=> new MemberVM{ Member = m }).ToList();
        }

        public static List<GroupCategory> GetAllGroupCategories(string sessionId)
        {
            List<GroupCategory> lst = _socialAdapter.GetAllGroupCategories(sessionId);
            var q = from s in lst
                    select new GroupCategory
                               {
                                   GroupCategoryDescription =
                                       WebUtility.HtmlDecode(s.GroupCategoryDescription),
                                   GroupCategoryId = s.GroupCategoryId,
                                   GroupCategoryName = WebUtility.HtmlDecode(s.GroupCategoryName)
                               };
            return q.ToList();
        }

        public static Group GetGroupDetails(string sessionId, long groupId)
        {
            Group gGroup = _socialAdapter.GetGroupDetails(sessionId, groupId);
            gGroup.Name = WebUtility.HtmlDecode(gGroup.Name);
            return gGroup;
        }

        public static GroupVM GetGroupVMDetails(string sessionId, long groupId, long memberId, int? noOfGroupMembers, bool? isPartOfGroup)
        {
            Group mtGroup = _socialAdapter.GetGroupDetails(sessionId, groupId);
            var detailedGroup = new GroupVM();
            if (mtGroup != null)
            {
                mtGroup.Description = WebUtility.HtmlDecode(mtGroup.Description);
                mtGroup.Name = WebUtility.HtmlDecode(mtGroup.Name);
                //determine if member is part of the group. If not, get a list of members that are part of the group - we need 10 random members
                var isMembPartOfGroup = isPartOfGroup ?? IsMemberPartOfGroup(memberId, mtGroup.Id);
                var listOfGroupMemebrs = new List<MemberVM>();
                int countOfGrpMembers = 0;
                var noOfGroupMembersToGet = noOfGroupMembers ?? 0;
                if (!isMembPartOfGroup && noOfGroupMembersToGet > 0)
                {
                    var lstMemb = GetGroupMembers(sessionId, mtGroup.Id, 0, noOfGroupMembers, false);
                    var amount = Math.Min(lstMemb.TotalCount, Convert.ToInt32(noOfGroupMembers));
                    countOfGrpMembers = lstMemb.TotalCount;
                    var members = (from mG in lstMemb.GroupMembers select new MemberVM { Member = mG }).ToList();
                    listOfGroupMemebrs = members.Take(amount).ToList();
                }
                //we need to add "..." if group name is greater than 55 charachters
                string shortGroupName = WebUtility.HtmlDecode(mtGroup.Name);
                if (shortGroupName.Length > 50)
                {
                    shortGroupName = shortGroupName.Remove(47);
                    shortGroupName += "...";
                }

                #region determine relation between member and Group
                string strGroupRelation = GroupRelation.NONE;
                if (isMembPartOfGroup)
                {
                    strGroupRelation = GroupRelation.MEMBER;
                    if (mtGroup.AdminId == memberId)
                    {
                        strGroupRelation = GroupRelation.ADMINISTRATOR;
                    }
                    else if (mtGroup.CreatedBy != null && mtGroup.CreatedBy.MemberId == memberId)
                    {
                        strGroupRelation = GroupRelation.CREATOR;
                    }
                }
                else
                {
                    //get notifications and check if any pending invitations
                    var memberNotific = GetMemberNotifications(sessionId, memberId);
                    if (memberNotific != null && memberNotific.Count > 0)
                    {
                        var grpNotif = (from notif in memberNotific
                                        where notif.NotificationTypeEntityId == groupId &&
                                              notif.NotificationTypeId == NotificationType.GROUPINVITATION &&
                                              (notif.NotificationStatusID == NotificationStatus.WAIT_FOR_ACTION ||
                                               notif.NotificationStatusID == NotificationStatus.CLEARED)
                                        select notif).ToList();
                        if (grpNotif.Count > 0)
                        {
                            strGroupRelation = GroupRelation.INVITED;
                        }
                    }
                }
                detailedGroup.MemberGroupRelation = strGroupRelation;
                #endregion
                detailedGroup.Group = mtGroup;
                detailedGroup.IsLoggedMemberTheAdmin = mtGroup.AdminId == memberId;
                detailedGroup.IsPartOfTheGroup = isMembPartOfGroup;
                detailedGroup.PrivateViewURL = ApplicationSettings.ShowGroupURL;
                detailedGroup.GroupMembersCount = countOfGrpMembers;
                detailedGroup.GroupMembers = listOfGroupMemebrs;
                detailedGroup.ShortGroupName = shortGroupName;
            }
            return detailedGroup;
        }

        public static SocialPagedData SearchGroupByName(string sessionId, PagingIM pagingSearchGrp, string groupName)
        {
            const GroupTypes GROUP_TYPE = GroupTypes.MEM;
            var matchedGroups = _socialAdapter.SearchGroupByName(sessionId, groupName, GROUP_TYPE, pagingSearchGrp.PageNumber, pagingSearchGrp.Count);
            return matchedGroups;
        }

        public static PagedGroupsVM SearchGroupByGroupName(string sessionId, PagingIM pagingSearchGrp, string groupName)
        {
            var listOfSrcdGroups = SearchGroupByName(sessionId, pagingSearchGrp, groupName);
            var getVMOfSrcdGroups = GetSimpleListOfGroupDetails(listOfSrcdGroups.Groups, pagingSearchGrp.MemberId, null);
            var pagSrchdGrps = new PagingDataVM
                                   {
                                       HasMoreItems = listOfSrcdGroups.HasMore,
                                       TotalNumberOfItems = listOfSrcdGroups.TotalCount,
                                       CurentPageNumber =
                                           (pagingSearchGrp.PageNumber != null)
                                               ? Convert.ToInt32(pagingSearchGrp.PageNumber)
                                               : 0,
                                       TotalNumberOfPages = listOfSrcdGroups.TotalPageCount,
                                       PageSize =
                                           (pagingSearchGrp.Count != null)
                                               ? Convert.ToInt32(pagingSearchGrp.Count)
                                               : 0
                                   };
            return new PagedGroupsVM
                       {
                           Groups = getVMOfSrcdGroups,
                           GroupsPaginationInfo = pagSrchdGrps,
                           PagingGroupsSource = "search"
                       };
        }
       
        public static List<MemberProfile> SearchFriendsByName( long memberId, string name )
        {
            return _socialAdapter.SearchFriendsByName( memberId, name );
        }

        public static SocialPagedData SearchGroupByAdministrator(string sessionId, PagingIM pagingSearchGrp, string adminName)
        {
            var matchedGroups = _socialAdapter.SearchGroupByAdminName(sessionId, adminName, GroupTypes.MEM, pagingSearchGrp.PageNumber, pagingSearchGrp.Count);
            return matchedGroups;
        }
       
        public static PagedGroupsVM SearchGroupByAdminName(string sessionId, PagingIM pagingSearchGrp, string adminName)
        {
            var listOfSrcdGroups = SearchGroupByAdministrator(sessionId, pagingSearchGrp, adminName);
            var getVMOfSrcdGroups = GetSimpleListOfGroupDetails(listOfSrcdGroups.Groups, pagingSearchGrp.MemberId, null);
            var pagSrchdGrps = new PagingDataVM
                                   {
                                       HasMoreItems = listOfSrcdGroups.HasMore,
                                       TotalNumberOfItems = listOfSrcdGroups.TotalCount,
                                       CurentPageNumber =
                                           (pagingSearchGrp.PageNumber != null)
                                               ? Convert.ToInt32(pagingSearchGrp.PageNumber)
                                               : 0,
                                       TotalNumberOfPages = listOfSrcdGroups.TotalPageCount,
                                       PageSize =
                                           (pagingSearchGrp.Count != null)
                                               ? Convert.ToInt32(pagingSearchGrp.Count)
                                               : 0
                                   };
            return new PagedGroupsVM
                       {
                           Groups = getVMOfSrcdGroups,
                           GroupsPaginationInfo = pagSrchdGrps,
                           PagingGroupsSource = "search"
                       };
        }

        public static SocialPagedData SearchGrpByCategory(string sessionId, PagingIM pagingSearchGrp, GroupCategories categoryId)
        {
            var matchedGroups = _socialAdapter.SearchGroupsByCategory(sessionId, categoryId, GroupTypes.MEM, pagingSearchGrp.PageNumber, pagingSearchGrp.Count);
            return matchedGroups;
        }

        public static PagedGroupsVM SearchGroupByCategory(string sessionId, PagingIM pagingSearchGrp, GroupCategories categoryId)
        {
            var listOfSrcdGroups = SearchGrpByCategory(sessionId, pagingSearchGrp, categoryId);
            var getVMOfSrcdGroups = GetSimpleListOfGroupDetails(listOfSrcdGroups.Groups, pagingSearchGrp.MemberId, null);
            var pagSrchdGrps = new PagingDataVM
                                   {
                                       HasMoreItems = listOfSrcdGroups.HasMore,
                                       TotalNumberOfItems = listOfSrcdGroups.TotalCount,
                                       CurentPageNumber =
                                           (pagingSearchGrp.PageNumber != null)
                                               ? Convert.ToInt32(pagingSearchGrp.PageNumber)
                                               : 0,
                                       TotalNumberOfPages = listOfSrcdGroups.TotalPageCount,
                                       PageSize =
                                           (pagingSearchGrp.Count != null)
                                               ? Convert.ToInt32(pagingSearchGrp.Count)
                                               : 0
                                   };
            return new PagedGroupsVM
                       {
                           Groups = getVMOfSrcdGroups,
                           GroupsPaginationInfo = pagSrchdGrps,
                           PagingGroupsSource = "search"
                       };
        }

        public static List<PendingGroupInvitation> GetAllGroupInvitations(string sessionId, long memberId)
        {
            var returnList = new List<PendingGroupInvitation>();
            //get all notifications and filter out the group invites ones
            var memberNotific = _socialAdapter.GetMemberNotifications(sessionId, memberId);
            if (memberNotific != null && memberNotific.Any())
            {
                var grpInvitNotific = (from MemberNotification mn in memberNotific
                                       where mn.NotificationTypeId == NotificationType.GROUPINVITATION
                                       select mn).ToList();
                if (grpInvitNotific.Any())
                {
                    foreach (var memberNotif in grpInvitNotific)
                    {
                        //get the group
                        var notif = memberNotif;
                        var grp = GetGroupDetails(sessionId, notif.NotificationTypeEntityId);
                        var detailedGroup = new GroupVM
                                                {
                                                    Group = grp,
                                                    IsLoggedMemberTheAdmin = false,
                                                    //this is basically an invitation

                                                    IsPartOfTheGroup = false,
                                                    PrivateViewURL = ApplicationSettings.ShowGroupURL,
                                                    isInvitedToGroup = true
                                                };
                        var pendinGroupinv = new PendingGroupInvitation
                                                 {
                                                     NotificationInfo = notif,
                                                     GroupInfo = detailedGroup
                                                 };
                        returnList.Add(pendinGroupinv);
                    }
                }
            }
            return returnList;
        }
        public static bool DeclineNotification(string sessionId, MemberNotification notific)
        {
            return _socialAdapter.DeclineNotification(sessionId, notific);
        }
        public static List<string> GetMemberGroupNames(string sessionId, long memberId)
        {
            var listOfNames = _socialAdapter.GetMemberGroupNames(sessionId, memberId, GroupTypes.MEM);
            return listOfNames;
        }
        public static List<string> GetMemberFriendNames(string sessionId, long memberId)
        {
            var listOfNames = _socialAdapter.GetMemberFriendNames(sessionId, memberId);
            return listOfNames;
        }

        public static byte[] GetMemberImage(string sessionId, long id, string defaultImagePath)
        {
            if (id == 0)
            {
                return null;
            }
            byte[] image = _socialAdapter.GetMemberImage(id);
            if (image == null || image.Length < 1)
            {
                image = DefaultImage.GetDefaultImage(defaultImagePath);
            }
            return image;
        }

        public static bool IsMemberFriend(string sessionId, long memberId, long currentMemberId)
        {
            var friends = _socialAdapter.GetMemberFriends(sessionId, memberId, null, null);

            return friends.Friends.Any(f => f.MemberId == currentMemberId);
        }
        public static byte[] GetGroupImage(long id, string defaultImagePath)
        {
            if (id == 0)
            {
                return DefaultImage.GetDefaultImage(defaultImagePath);
            }
            byte[] image = _socialAdapter.GetGroupImage(id);
            if (image == null || image.Length < 1)
            {
                image = DefaultImage.GetDefaultImage(defaultImagePath);
            }
            return image;
        }
        public static byte[] GetGroupImageGuid(long id)
        {
            byte[] image = _socialAdapter.GetGroupImage(id);
            return image;
        }
        public static string GetGroupImagePath(long id, string memberPicturePath, string defaultGroupGuid)
        {
            string strGroupImagePath;
            if (id < 1)
            {
                strGroupImagePath = string.Format("{0}{1}.png", memberPicturePath, new Guid(defaultGroupGuid));
            }
            else
            {
                var image = _socialAdapter.GetGroupImage(id);
                if (image != null && image.Length > 1)
                {
                    strGroupImagePath = string.Format("{0}{1}.jpg", memberPicturePath, new Guid(image));
                }
                else
                {
                    strGroupImagePath = string.Format("{0}{1}.png", memberPicturePath, new Guid(defaultGroupGuid));
                }
            }
            return strGroupImagePath;
        }
        public static string GetMemberImagePath(long id, string memberPicturePath, string defaultMemberGuid)
        {
            string strMemberImagePath;
            if (id < 1)
            {
                strMemberImagePath = string.Format("{0}{1}.png", memberPicturePath, defaultMemberGuid);
            }
            else
            {
                var image = _socialAdapter.GetMemberImage(id);
                if (image != null && image.Length > 1)
                {
                    strMemberImagePath = string.Format("{0}{1}.jpg", memberPicturePath, new Guid(image));
                }
                else
                {
                    strMemberImagePath = string.Format("{0}{1}.png", memberPicturePath, defaultMemberGuid);
                }
            }
            return strMemberImagePath;
        }
        public static MemberVM GetMemberProfile(string sessionId, long memberId, long currentMemberId, bool includeImage)
        {
            var memberVM = new MemberVM
                               {
                                   Member = _socialAdapter.GetMemberProfile(sessionId, memberId, includeImage, currentMemberId),
                                   RelationShip = _socialAdapter.GetMemberRelationship(sessionId, memberId)
                               };
            if (memberId == currentMemberId)
            {
                if (memberVM.ProfileExists == 1) //do not make any overwrite for the default values for any of the new members
                {
                    memberVM.ShowAge = true;
                    memberVM.ShowBadges = true;
                    memberVM.ShowChallengeHistory = true;
                    memberVM.ShowCity = true;
                    memberVM.ShowGender = true;
                    memberVM.ShowGoals = true;
                    memberVM.ShowInterests = true;
                    memberVM.ShowLastGoZoneUpload = true;
                    memberVM.ShowNickname = true;
                    memberVM.ShowOrganization = true;
                    memberVM.ShowStepsTotal = true;
                    memberVM.ViewSize = ViewSize.Large;
                }
            }
            else
            {
                var isFriend = IsMemberFriend(sessionId, memberId, currentMemberId);
                memberVM.ShowAge = IsProfileInfoVisible(memberVM.Member.ShowAge, isFriend);
                memberVM.ShowBadges = IsProfileInfoVisible(memberVM.Member.ShowBadges, isFriend);
                memberVM.ShowChallengeHistory = IsProfileInfoVisible(memberVM.Member.ShowChallengeHistory, isFriend);
                memberVM.ShowCity = IsProfileInfoVisible(memberVM.Member.ShowCity, isFriend);
                memberVM.ShowGender = IsProfileInfoVisible(memberVM.Member.ShowGender, isFriend);
                memberVM.ShowGoals = IsProfileInfoVisible(memberVM.Member.ShowGoals, isFriend);
                memberVM.ShowInterests = IsProfileInfoVisible(memberVM.Member.ShowInterests, isFriend);
                memberVM.ShowLastGoZoneUpload = IsProfileInfoVisible(memberVM.Member.ShowLastGZUpload, isFriend);
                memberVM.ShowNickname = IsProfileInfoVisible(memberVM.Member.ShowNickname, isFriend);
                memberVM.ShowOccupation = IsProfileInfoVisible(memberVM.Member.ShowOccupation, isFriend);
                memberVM.ShowOrganization = IsProfileInfoVisible(memberVM.Member.ShowSponsors, isFriend);
                memberVM.ShowStepsAverage = IsProfileInfoVisible(memberVM.Member.ShowStepsAvg, isFriend);
                memberVM.ShowStepsTotal = IsProfileInfoVisible(memberVM.Member.ShowTotalSteps, isFriend);
                memberVM.ViewSize = ViewSize.Medium;
            }

            if (memberVM.ShowInterests || memberVM.ShowGoals)
            {
                List<MemberGoalsAndInterests> lst = _memberAdapter.GetMemberGoalsAndInterests(sessionId, memberId);
                var goals = (from s in lst
                             where s.Type == "goal" && s.Selected
                             select s.Description).ToList();

                if (goals.Any())
                {
                    memberVM.Member.Goals = goals.Aggregate((a, x) => a + ", " + x);
                }

                var interests = (from s in lst
                                 where s.Type == "interest" && s.Selected
                                 select s.Description).ToList();

                if (interests.Any())
                {
                    memberVM.Member.Interests = interests.Aggregate((a, x) => a + ", " + x);
                }

                memberVM.Member.Goals = WebUtility.HtmlDecode(memberVM.Member.Goals);
                memberVM.Member.Interests = WebUtility.HtmlDecode(memberVM.Member.Interests);
            }
            if (memberVM.ShowOccupation)
            {
                MemberVM m = GetMembersByIds(sessionId, new List<long> { memberId }).FirstOrDefault();
                List<Occupation> lst = _socialAdapter.GetOccupations();
                memberVM.Member.Occupation =
                    (from s in lst where s.LookupID == m.Member.OccupationId select s).FirstOrDefault();
            }
            return memberVM;
        }

        public static bool DeleteGroup(string sessionId, long memberId, long id)
        {

            Group group = GetGroupDetails(sessionId, id);
            if (group.AdminId == memberId)
            {
                return _socialAdapter.DeleteGroup(sessionId, group);
            }
            return false;
        }

        public static long AddGroup(string sessionId, long memberId, GroupIM groupIM, long communityId)
        {
            long newId = 0;
            if (ValidateGroupIM(groupIM) && groupIM.PageMode != "Create_1")
            {
                var group = new Group();
                group.Name = WebUtility.HtmlEncode(groupIM.Name);
                group.AdminId = memberId;
                group.CreatorId = memberId;
                group.CommunityID = communityId;
                //we need to save the date as en-us format.
                var enUsCulture = new CultureInfo("en-US");
                group.CreatedDateString = _memberAdapter.GetMemberNow(memberId, DateTime.Now).ToString(enUsCulture);
                group.Description = groupIM.Description;
                group.CategoryId = groupIM.CategoryId;
                if (groupIM.ImageStream != null)
                {
                    //TODO need to change the GetByteArrFromStream to call into Angel's service and upload image to CDC and return a guid for the group name 
                    group.Image = SaveGroupImageFromStream(groupIM.ImageStream);
                    //group.Image = ImageResizer.CreateThumbnail(group.Image);
                }
                if (groupIM.Privacy == "Private")
                    group.IsPrivate = true;
                else
                    group.IsPrivate = false;
                group.GroupTypeID = (byte)GroupTypes.MEM;
                if (!IsGroupNameAvalibale(sessionId, group.Name))
                {
                    return 0;
                }
                newId = _socialAdapter.AddGroup(sessionId, group);
            }
            if (groupIM.PageMode == "Create_1" && groupIM.InvitedMembers.Count > 0)
            {
                SendGroupInvitation(sessionId, memberId, groupIM.Id, groupIM.InvitedMembers);
            }
            return newId;
        }

        private static byte[] SaveGroupImageFromStream(Stream stream)
        {
            var memoryStream = new MemoryStream();
            stream.CopyTo(memoryStream);
            var resizedImage = ImageResizer.CreateThumbnail(memoryStream.ToArray());//this will be sent as input to service returning GUID
            //we'll use this webservice to save the image as the save of the images is using com+
            var wsSocial = new WSVHMSocialSoapClient();
            var imgSaveResponse = wsSocial.SaveImageToCdn(resizedImage, ".jpg");
            if (Guid.Empty != imgSaveResponse)
            {
                return imgSaveResponse.ToByteArray();
            }
            return new byte[0];
        }

        public static bool UpdateGroup(string sessionId, long memberId, GroupIM groupIM)
        {
            bool res = false;
            if (ValidateGroupIM(groupIM))
            {
                Group group = GetGroupDetails(sessionId, groupIM.Id);
                group.Name = WebUtility.HtmlEncode(groupIM.Name);
                group.Description = WebUtility.HtmlEncode(groupIM.Description);
                group.CategoryId = groupIM.CategoryId;
                if (groupIM.ImageStream != null)
                {
                    group.Image = SaveGroupImageFromStream(groupIM.ImageStream);
                }
                @group.IsPrivate = groupIM.Privacy == "Private";

                group.GroupTypeID = (byte)GroupTypes.MEM;
                if (group.AdminId == memberId)
                {
                    res = _socialAdapter.UpdateGroup(sessionId, group);
                }

                if (groupIM.InvitedMembers.Count > 0 && res)
                {
                    SendGroupInvitation(sessionId, memberId, groupIM.Id, groupIM.InvitedMembers);
                }
            }
            return res;
        }

        public static bool SendGroupInvitation(string sessionId, long notifiedById, long groupId, List<long> mIds)
        {
            var notification = new MemberNotification
                                   {
                                       NotifiedById = notifiedById,
                                       NotificationTypeEntityId = groupId,
                                       NotificationTypeId = NotificationType.GROUPINVITATION,
                                       NotificationStatusID = NotificationStatus.WAIT_FOR_ACTION
                                   };
            return _socialAdapter.AddMemberNotificationList(sessionId, mIds, notification);
        }

        #region Private methods
        //this will return only short info about the group. More details can be obtained by opening group profile
        private static List<GroupVM> GetSimpleListOfGroupDetails(List<Group> simpleGroupList, long memberId, bool? isPartOfGroup)
        {
            var detailedGroupList = new List<GroupVM>();

            if (simpleGroupList != null && simpleGroupList.Count > 0)
            {
                foreach (var simpleGroup in simpleGroupList)
                {
                    bool isMembPartOfGroup = isPartOfGroup ?? IsMemberPartOfGroup(memberId, simpleGroup.Id);
                    //we need to add "..." if group name is greater than 55 charachters
                    string shortGroupName = WebUtility.HtmlDecode(simpleGroup.Name);
                    if (shortGroupName.Length > 50)
                    {
                        shortGroupName = shortGroupName.Remove(47);
                        shortGroupName += "...";
                    }
                    var detailedGroup = new GroupVM
                                            {
                                                Group = simpleGroup,
                                                IsLoggedMemberTheAdmin = simpleGroup.AdminId == memberId,
                                                IsPartOfTheGroup = isMembPartOfGroup,
                                                PrivateViewURL = ApplicationSettings.ShowGroupURL,
                                                ShortGroupName = shortGroupName
                                            };
                    detailedGroupList.Add(detailedGroup);
                }
            }

            return detailedGroupList;
        }

        #endregion
        private static bool ValidateGroupIM(GroupIM groupIM)
        {
            switch (groupIM.PageMode)
            {
                case "Create":
                    if (string.IsNullOrEmpty(groupIM.Name))
                        return false;
                    if (string.IsNullOrEmpty(groupIM.Description))
                        return false;
                    if (groupIM.CategoryId == null || groupIM.CategoryId == 0)
                        return false;
                    if (string.IsNullOrEmpty(groupIM.Privacy))
                        return false;
                    break;
                case "Edit":
                    if (groupIM.Id == 0)
                        return false;
                    if (string.IsNullOrEmpty(groupIM.Name))
                        return false;
                    if (string.IsNullOrEmpty(groupIM.Description))
                        return false;
                    if (groupIM.CategoryId == null || groupIM.CategoryId == 0)
                        return false;
                    if (string.IsNullOrEmpty(groupIM.Privacy))
                        return false;
                    break;
                case "Create_1":
                    if (groupIM.Id == 0)
                        return false;
                    break;
                default:
                    return false;
            }
            return true;
        }

        public static bool IsGroupNameAvalibale(string sessionId, string groupName)
        {
            groupName = WebUtility.HtmlEncode(groupName);
            return !_socialAdapter.IsGroupNameAvalibale(groupName.Trim());
        }
        private static bool IsProfileInfoVisible(short setting, bool isFriend)
        {
            if (setting == 1 || (setting == 0 && isFriend))
            {
                return true;
            }

            return false;
        }

        #region Notifications
        public static int GetMemberNotificationCount(long memberId)
        {
            return _socialAdapter.GetMemberWaitingForActionNotificationCount(memberId);
        }

        public static bool UpdateNotification(string sessionId, MemberNotification notification)
        {
            return _socialAdapter.UpdateNotification(sessionId, notification);
        }

        public static List<MemberNotification> GetMemberNotifications(string sessionId, long memberId)
        {
            return _socialAdapter.GetMemberNotifications(sessionId, memberId);
        }

        #endregion

        public static string GetVhmContent(long sponsorId, long memberId, string contentAreaId, int languageId)
        {
            return _socialAdapter.GetVhmContent(sponsorId, memberId, contentAreaId, languageId);
        }

        public static string GetSponsorContentForMember(long memberId)
        {
            return _socialAdapter.GetSponsorContentForMember(memberId);
        }
        #region NewsFeed
        public static IEnumerable<NewsfeedPost> GetMemberNewsfeed(long memberId, long communityId, int commentCount)
        {
            var allNewsFeed = _socialAdapter.GetMemberNewsfeed(memberId, communityId, commentCount)
                .Where(p => !string.IsNullOrEmpty(p.PostText)).ToList();

            // manual posts by friends or by the current user. These do not get aggregated
            var manualPosts = (from posts in allNewsFeed
                               where posts.PostType == NewsfeedPost.POST_TYPE_MANUAL
                               select posts).ToList();

            var friendPostsGrouped = (from posts in allNewsFeed
                                      where posts.PostType == NewsfeedPost.POST_TYPE_FRIEND
                                      group posts by posts.PostById
                                          into p
                                          select p).ToList();

            var friendPosts = friendPostsGrouped.Select(posts => GetAggregationCount(posts.ToList())).ToList();

            // joined a group 
            var friendGroupPostsGrouped = (from posts in allNewsFeed
                                           where posts.PostType == NewsfeedPost.POST_TYPE_JOIN_GROUP
                                           group posts by posts.GroupId
                                               into p
                                               select p).ToList();

            var friendGroupPosts = friendGroupPostsGrouped.Select(groupPosts => GetAggregationCount(groupPosts.ToList())).ToList();

            // get the grouping of badge posts
            var friendsBadgePostsGrouped = (from posts in allNewsFeed
                                            where posts.PostType == NewsfeedPost.POST_TYPE_BADGE
                                            group posts by posts.BadgeId
                                                into p
                                                select p).ToList();

            var friendsBadgePosts = friendsBadgePostsGrouped.Select(badgePosts => GetAggregationCount(badgePosts.ToList())).ToList();

            // promo posts
            var friendPromoPostsGrouped = (from posts in allNewsFeed
                                           where posts.PostType == NewsfeedPost.POST_TYPE_PROMO
                                           group posts by posts.PostText
                                               into p
                                               select p).ToList();

            var friendPromoPosts = friendPromoPostsGrouped.Select(promoPosts => GetAggregationCount(promoPosts.ToList())).ToList();


            var newsFeed = new List<NewsfeedPost>();
            newsFeed.AddRange(manualPosts);
            newsFeed.AddRange(friendPosts.OrderByDescending(p => p.PostDate));
            newsFeed.AddRange(friendsBadgePosts.OrderByDescending(p => p.PostDate));
            newsFeed.AddRange(friendGroupPosts.OrderByDescending(p => p.PostDate));
            newsFeed.AddRange(friendPromoPosts.OrderByDescending(p => p.PostDate));

            // Post Date is saved in UTC time by the MT, so display it in local time.
            newsFeed.ForEach(nf => nf.PostDate = nf.PostDate.ToLocalTime());


            return newsFeed.ToList();
        }

        public static string GetWeeklyStatusUpdatePrompt(string sessionId, string cultureCode)
        {
            // For new users, before WJ starts the cultureCode is null,and this method is being called by Ajax (async) so that it throws an error.
            cultureCode = cultureCode ?? "en-us";
            var ci = new CultureInfo(cultureCode);
            DateTimeFormatInfo dateTimeFormat = ci.DateTimeFormat;
            int weekNumber = ci.Calendar.GetWeekOfYear(DateTime.Now, dateTimeFormat.CalendarWeekRule,
                                                       dateTimeFormat.FirstDayOfWeek);
            return _socialAdapter.GetWeeklyStatusUpdatePrompt(sessionId, weekNumber);
        }

        public static void LikePost(string sessionId, long memberId, long postById, long postId, long groupId)
        {
            _socialAdapter.LikeNewsfeedPost(sessionId, memberId, postById, postId, groupId);
        }

        /// <summary>
        /// submit a post from wall (news feed) or group post/ comment...
        /// </summary>
        /// <param name="sessionId">this should be removed as it is not checked in MT anymore</param>
        /// <param name="memberId">memberId of user making the post = logged in user = can be retrieved from session</param>
        /// <param name="postText">text that will fill in the post/news feed/ comment</param>
        /// <param name="groupId">0 if this is a wall news feed/ group id if this is a group post</param>
        /// <param name="communityId"></param>
        /// <param name="typeCode">Newsfeed Post Type</param>
        /// <returns>true if insert + trigger the reward for first post was successful, false otherwise</returns>
        public static bool SumbitMemberPost(string sessionId, long memberId, string postText, long groupId, long communityId, NewsFeedPostType typeCode)
        {
            bool newsfeedPostAdded = _socialAdapter.SumbitMemberPost(sessionId, memberId, postText, groupId, communityId, typeCode);
            
            return newsfeedPostAdded;
        }

        /// <summary>
        /// submit a post from wall (news feed) or group post/ comment...
        /// </summary>
        /// <param name="sessionId">this should be removed as it is not checked in MT anymore</param>
        /// <param name="memberId">memberId of user making the post = logged in user = can be retrieved from session</param>
        /// <param name="postText">text that will fill in the post/news feed/ comment</param>
        /// <param name="groupId">0 if this is a wall news feed/ group id if this is a group post</param>
        /// <param name="communityId"></param>
        /// <param name="newsFeedTypeId">type of the feed - MemberPostedInGroup/ManualPost/Comment and so on </param>
        /// <returns>true if insert + trigger the reward for first post was successful, false otherwise</returns>
        public static bool SumbitMemberPost(string sessionId, long memberId, string postText, long groupId, long communityId, int newsFeedTypeId)
        {
            return _socialAdapter.SumbitMemberPost(sessionId, memberId, postText, groupId, communityId, newsFeedTypeId);
        }
        private static NewsfeedPost GetAggregationCount(List<NewsfeedPost> posts)
        {
            if (posts.Count == 0)
            {
                return null;
            }

            var firstPost = posts.First();
            var post = CloneNewsfeedPost(firstPost);
            post.AggregationCount = Math.Max(posts.Count - 1, 1);

            return post;
        }

        private static NewsfeedPost CloneNewsfeedPost(NewsfeedPost original)
        {
            return new NewsfeedPost
                       {
                           BadgeId = original.BadgeId,
                           BadgeName = original.BadgeName,
                           BadgeImage = original.BadgeImage,
                           GroupId = original.GroupId,
                           GroupName = original.GroupName,
                           GroupImage = original.GroupImage,
                           ImageName = original.ImageName,
                           LikeCount = original.LikeCount,
                           ParentId = original.ParentId,
                           PostById = original.PostById,
                           PostText = original.PostText,
                           PostByImage = original.PostByImage,
                           PostByName = original.PostByName,
                           PostDate = original.PostDate,
                           PostId = original.PostId,
                           PostToId = original.PostToId,
                           PostToName = original.PostToName,
                           PostType = original.PostType
                       };

        }
        #endregion



        public static bool IsMemberPartOfGroup(long memberId, long groupId)
        {
            return _socialAdapter.IsMemberPartOfGroup(memberId, groupId);
        }
    }
}
