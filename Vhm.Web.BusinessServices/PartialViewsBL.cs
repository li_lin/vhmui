﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vhm.Web.Common;
using Vhm.Web.Models.ViewModels.PartialViews;
using Vhm.Web.Models.Enums;
using Vhm.Web.Models.InputModels;
using Vhm.Web.Models.RepositoryModels;
using Vhm.Web.Models.ViewModels.Social;
using System.IO;

namespace Vhm.Web.BL
{
    public static class PartialViewsBL
    {

        public static InviteFriendsGroupsVM GetInviteFriendsGroupsVM(string sessionId, PagingIM paging)
        {
            var model = new InviteFriendsGroupsVM();
            return model;
        }

        public static List<MemberVM> AddToInvitationList(string sessionId, List<long> inviteList, List<long> newMembersIds)
        {
            inviteList.AddRange(newMembersIds);
            inviteList = inviteList.Distinct().ToList();
            List<MemberVM> mList = SocialBL.GetMembersByIds(sessionId, inviteList);
            return mList;

        }

        public static IEnumerable<MemberVM> FilterInvitedMembersIds(long inviterId, List<MemberVM> members, List<long> existingList)
        {
            List<MemberVM> uinqIds = members.Distinct().ToList();
            List<MemberVM> mList = (from s in uinqIds where !existingList.Contains(s.Member.MemberId) && s.Member.MemberId != inviterId select s).ToList();
            return mList;
        }

        public static IEnumerable<MemberVM> GetNewInviteList( string sessionId, long inviterId, List<long> memberIds,
                                                              List<long> groupIds, List<long> existingList )
        {
            if ( memberIds == null )
            {
                memberIds = new List<long>();
            }
            if ( existingList == null )
            {
                existingList = new List<long>();
            }
            if ( groupIds == null )
            {
                groupIds = new List<long>();
            }
            var newList = new List<MemberVM>();
            if ( groupIds.Count > 0 )
            {
                foreach ( long groupId in groupIds )
                {
                    // for this case we need to actually get all groups member
                    var grpMembersList = SocialBL.GetGroupMembers( sessionId, groupId, 0, int.MaxValue, false );
                    List<Member> gMembers = grpMembersList.GroupMembers;
                    var q = from s in gMembers
                            select new MemberVM { Member = s };
                    newList.AddRange( q.ToList() );
                }

            }
            List<MemberVM> members = SocialBL.GetMembersByIds( sessionId, memberIds );
            newList.AddRange( members );
            IEnumerable<MemberVM> mList = FilterInvitedMembersIds( inviterId, newList, existingList );
            return mList;
        }

        public static IEnumerable<MemberVM> GetMoreFriends(string sessionId, PagingIM paging)
        {
            SocialPagedData pd=SocialBL.GetMemberFriends(sessionId, paging);
            return (from s in pd.Friends select new MemberVM { Member = s }).ToList();
        }

        public static IEnumerable<GroupVM> GetMoreGroups(string sessionId, PagingIM paging, bool includeImage)
        {
            //TODO 
            var types = new List<GroupTypes> { GroupTypes.MEM };
            bool includeImages = false;
            SocialPagedData pd = SocialBL.GetMemberGroups(sessionId,types, paging, includeImages);
            return (from s in pd.Groups select new GroupVM() { Group = s }).ToList();
        }

        //TODO need to remove the below 2 methods as they're no longer used
        public static byte[] GetMemberImage(string sessionId, long id, string defaultImagePath)
        {
            return SocialBL.GetMemberImage(sessionId, id, defaultImagePath);
        }

        public static byte[] GetGroupImage(long id, string defaultImagePath)
        {
            return SocialBL.GetGroupImage(id, defaultImagePath);
        }

        public static List<long> GetMemeberIdFromCSV(string sessionId, StreamReader csvStreamReader)
        {
            string sLine;
            var stringArray = new string[] { };
            var charArray = new[] { ',' };
            var emails = new List<string>();
            
            sLine = csvStreamReader.ReadLine();
            if ( sLine != null )
            {
                stringArray = sLine.Split(charArray);
            }
            for (int i = 0; i <= stringArray.GetUpperBound(0); i++)
            {
                //make sure no white spaces are alowed in the column name or the "usp_ImportChallengeInvitations"
                //will return no results.
                string str = Convert.ToString(stringArray[i]);
                str = str; //this is not removing a white space inside a string...so it's not enough
                str = str.Replace(" ", "").Replace(" ", "");
            }
            sLine = csvStreamReader.ReadLine();


            while (sLine != null)
            {
                stringArray = sLine.Split(charArray);
                emails.Add(sLine);
                sLine = csvStreamReader.ReadLine();

            }
            csvStreamReader.Close();
             var members= SocialBL.GetMembersByEmailsOrIdentityNumbers(sessionId, emails);
            return members.Select(m=>m.Member.MemberId).ToList();
        }

        public static string GetGroupImagePath(long id)
        {
            return SocialBL.GetGroupImagePath(id, ApplicationSettings.GroupImageUrl,ApplicationSettings.DefaultGroupPicGuid);
        }
        public static string GetMemberImagePath(long mId)
        {
            return SocialBL.GetMemberImagePath(mId, ApplicationSettings.MemberImageUrl, ApplicationSettings.DefaultMemberPicGuid);
        }
    }
}
