﻿using Vhm.Web.DataServices.Adapters;
using Vhm.Web.DataServices.Interfaces;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.BL
{
    public static class MeasurementsBL
    {

        private static readonly IMeasurementsAdapter _measurementsAdapter;

        static MeasurementsBL( )
        {
            _measurementsAdapter = GetMeasurementsAdapter();
        }

        private static IMeasurementsAdapter GetMeasurementsAdapter()
        {
            return _measurementsAdapter ?? new MeasurementsAdapter();
        }

        public static MeasurementReward GetMemberLatestMeasurementSummary(long memberId, int? maxResults)
        {
            return _measurementsAdapter.GetMemberLatestMeasurementSummary( memberId, maxResults );
        }

        public static bool UpdateMemberLatestMeasurementSummary(long memberId, int? maxResults)
        {
            return _measurementsAdapter.UpdateMemberLatestMeasurementSummary( memberId, maxResults );
        }
    }
}
