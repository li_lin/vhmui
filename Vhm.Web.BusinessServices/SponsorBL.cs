﻿using System.Collections.Generic;
using System.Linq;
using Vhm.Providers;
using Vhm.Web.DataServices.Adapters;
using Vhm.Web.DataServices.Interfaces;
using Vhm.Web.Models.Enums;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.BL
{
    public static class SponsorBL
    {

        public static ISponsorAdapter _sponsorAdapter;

        static SponsorBL()
        {
            _sponsorAdapter = GetSponsorAdapter();
        }

        private static ISponsorAdapter GetSponsorAdapter()
        {
            return _sponsorAdapter ?? new SponsorAdapter();
        }

        public static SponsorProfile GetSponsorProfile(long sponsorId)
        {
            var profile = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.SponsorProfile) as SponsorProfile;
            if ((profile == null || profile.SponsorID != sponsorId) && sponsorId > 0)
            {
                profile = _sponsorAdapter.GetSponsorProfile(sponsorId);
                profile.SponsorLanguages = GetSponsorLanguages(sponsorId);
                CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.SponsorProfile, profile);
            }
            return profile;
        }

        public static List<SponsorLanguage> GetSponsorLanguages(long sponsorId)
        {
            var sponsorAllLanguages = _sponsorAdapter.GetSponsorLanguages(sponsorId);

            if (sponsorAllLanguages != null)
            {
                sponsorAllLanguages = (from l in sponsorAllLanguages
                                       where l.IsSelected == true
                                       select l).ToList();

                if (sponsorAllLanguages.Count == 0)
                {
                    sponsorAllLanguages = (from l in sponsorAllLanguages
                                           where l.IsDefault
                                           select l).ToList();
                }
            }
            return sponsorAllLanguages;
        }

        public static List<ColorTheme> GetAvailableSponsorColorThemes(long sponsorId)
        {
            return _sponsorAdapter.GetAvailableSponsorColorThemes(sponsorId);
        }

        public static List<SponsorModule> GetSponsorModules(long sponsorId)
        {
            return _sponsorAdapter.GetSponsorModules(sponsorId);
        }

        public static IEnumerable<SponsorModuleGZVersion> GetSponsorGZVersions(long sponsorId)
        {

            var sponorModules = _sponsorAdapter.GetSponsorModules(sponsorId);
            var gzModule = sponorModules.SingleOrDefault(m => m.ModuleTypeCode == ModuleEnum.VAC.ToString());
            return gzModule != null ? gzModule.GZs : null;
        }

        public static SponsorModuleGZVersion GetSponsorCurrentGZVersion(long sponsorId)
        {
            var sponorModules = _sponsorAdapter.GetSponsorModules(sponsorId);
            var gzModule = sponorModules.SingleOrDefault(m => m.ModuleTypeCode == ModuleEnum.VAC.ToString());

            return gzModule != null ? gzModule.GZs.Max() : null;
        }

        public static List<RewardsProgram> GetSponsorPrograms(long sponsorId)
        {
            return _sponsorAdapter.GetSponsorPrograms(sponsorId);
        }

        public static List<PasswordRegEx> GetPasswordRegExValues()
        {
            return _sponsorAdapter.GetPasswordRegExValues();
        }

        public static PasswordRegEx GetSponsorPasswordRegex(long sponsorId)
        {
            var sponsorProfile = GetSponsorProfile(sponsorId);
            var sponsorRegEx =
                GetPasswordRegExValues().FirstOrDefault(t => t.PasswordRegExId == sponsorProfile.PasswordRegexID);
            return sponsorRegEx;
        }
    }
}
