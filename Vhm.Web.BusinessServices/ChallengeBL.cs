﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Web;
using Vhm.Web.Common;
using Vhm.Web.DataServices.Adapters;
using Vhm.Web.DataServices.Interfaces;
using Vhm.Web.Models.Enums;
using Vhm.Web.Models.InputModels;
using Vhm.Web.Models.RepositoryModels;
using Vhm.Web.Models.ViewModels.Challenges;
using System.Linq;
namespace Vhm.Web.BL
{
    public static class ChallengeBL
    {
        public static IChallengeAdapter challengeAdapter; // adapter is public for testing purposes. This allows the use of a Mock adapter.
        public static ISocialAdapter socialAdapter;
        public static  IRewardsAdapter rewardsAdapter;

        const string CHALLENGE_SOURCE_PERSONAL = "PERSONAL";

        static ChallengeBL()
        {
            Initialize();
        }

        private static void Initialize()
        {
            challengeAdapter = GetMemberAdapter();
            rewardsAdapter = GetRewardsAdapter();
            socialAdapter = GetSocialAdapter();
        }

        public static void SetChallengeAdapter(IChallengeAdapter adapter)
        {
            challengeAdapter = adapter;
        }
        public static void SetRewardAdapter(IRewardsAdapter adapter)
        {
            rewardsAdapter = adapter;
        }

        private static IChallengeAdapter GetMemberAdapter()
        {
            return challengeAdapter ?? new ChallengeAdapter();
        }

        private static IRewardsAdapter GetRewardsAdapter()
        {
            return rewardsAdapter ?? new RewardsAdapter();
        }

        private static ISocialAdapter GetSocialAdapter()
        {
            return socialAdapter ?? new SocialAdapter();
        }

        public static bool TriggerReward(long memberId, string rewardCode, bool processNow, bool surpressResult, DateTime triggerDate, long? challengeTaskId)
        {
            return rewardsAdapter.TriggerReward(memberId, rewardCode, processNow, surpressResult, triggerDate, challengeTaskId);
        }

        public static PersonalTrackingChallenge GetChallenge(long challengeId)
        {
            var challenge = challengeAdapter.GetChallenge(challengeId);
            if (challenge != null && challenge.RulesCopy != null )
            {
                challenge.RulesCopy = HttpUtility.HtmlDecode( challenge.RulesCopy );
            }
            if (challenge != null && challenge.Name != null)
            {
                challenge.Name = HttpUtility.HtmlDecode( challenge.Name );
            }
            return challenge;
        }

        public static List<PersonalTrackingChallenge> GetChallenges( long[] challangeIds )
        {
            return challengeAdapter.GetChallenges( challangeIds );
        }

        public static List<MemberAvailableChallengesByType> GetMemberAvailableChallengesByType( long memberId,
            string challengeType )
        {
            return challengeAdapter.GetMemberAvailableChallengesByType( memberId, challengeType, DateTime.Now );
        }

        public static void SaveChallengeMessage(long challengeId, long? teamId, long memberId, string nickName,string messageText)
        {
            string filteredMessage = FilterChatMessage(messageText);
            challengeAdapter.SaveChallengeMessage(challengeId, teamId, memberId,nickName, filteredMessage);
        }

        public static List<ChallengeMessage> GetChallengeMessages(long challengeId)
        {
            // The business rule for challenges is to get all messages for a particular challenge, therefore
            // the only thing we need to pass in is challengeId. The adapter method signature allows for the 
            // other paramter being set to true to show deleted message.
            const bool SHOW_DELETED = false;
            var chat =  challengeAdapter.GetChallengeMessages(challengeId, SHOW_DELETED);

            // get the distinct list of memberIds and their profile images into a dictionary.
            var memberImages = chat.Select(m => new { MemberId = m.MemberID })
                                      .Distinct() // need to do distinct because we need a dictionary. no duplicates allowed.                                    
                                      .ToDictionary(m => m.MemberId,
                                                     m => SocialBL.GetMemberImagePath(m.MemberId,
                                                                                       ApplicationSettings.MemberImageUrl,
                                                                                       ApplicationSettings.DefaultMemberPicGuid));

            chat.ForEach(m => m.ImgSrc = memberImages[m.MemberID]);
            chat.ForEach(m => m.CreatedDateString = m.CreatedDate.ToShortDateString());

            return chat;
        }

        public static List<ChallengeMessage> GetChallengeMessagesForTeam(long challengeId, long teamId, bool? deleted = false)
        {
            return challengeAdapter.GetChallengeMessagesForTeam(challengeId, teamId, deleted.GetValueOrDefault()); 
        }

        private static string FilterChatMessage(string message)
        {
            var badWords = MemberBL.GetBadWords(null).Select(w => w.BadWord).ToList();
            var words = message.Split(' ');
            var cnt = words.Count();
            for (var i = 0; i < cnt; i++)
            {
                var test = Regex.Replace(words[i], @"[^\w\s]", ""); // remove any punctuation from the string
                test = test.Replace("-", "").Replace("_", "");
                if (badWords.Any(bw => bw.ToLower().Equals(test.ToLower())))
                {
                    words[i] = new string('*', words[i].Length);
                }
            }
            return string.Join(" ", words);
        }

        public static IEnumerable<Member> GetFriendSelectListForChallengeInvite(string sessionId, PagingIM paging)
        {
            var data = SocialBL.GetMemberFriends(sessionId, paging);

            var profileList = data.Friends.Select(friend => new Member
                {
                    MemberId = friend.MemberId, 
                    EmailAddress = friend.EmailAddress, 
                    DisplayName = friend.DisplayName, 
                    ImageUrl = friend.ImageUrl
                }).ToList();

            return profileList;
        }

        /// <summary>
        /// Gets the tracked days.
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="challengeId">The challenge id.</param>
        /// <param name="memberId">The member id.</param>
        /// <returns></returns>
        public static ChallengeScoreInfo GetTrackedDays(DateTime startDate, DateTime endDate, long challengeId, long memberId)
        {
            
            var originalchallengeScores = challengeAdapter.GetTrackedDays(challengeId, memberId);
            var daysOfYes = originalchallengeScores.FindAll(cs => cs.Score > 0).Count; //find out how many yes member has answer since the challenge started

            var challengeScores = originalchallengeScores.FindAll(cs => cs.ScoreDate.Date >= startDate.Date).ToList();

            var padDays = Math.Ceiling(endDate.Subtract( startDate ).TotalDays);
            if (challengeScores.Count < padDays)// pad the list with empty ChallengeScore for those unanswer days
            {
                var scoreDate = startDate.Date;
                for (int i = 0; i < padDays; i++)
                {
                    if (challengeScores.FindAll(x => x.ScoreDate == scoreDate).Count == 0)
                    {
                        challengeScores.Add(new ChallengeScore {
                            ScoreDate = scoreDate,
                            MemberId = memberId,
                            ChallengeId = challengeId,
                            Score = 0
                        });
                    }
                    scoreDate = scoreDate.AddDays(1);
                    if ( scoreDate > endDate )
                    {
                        break;
                    }
                }
            }

            return new ChallengeScoreInfo {
                ChallengeScores = challengeScores.OrderBy(cs => cs.ScoreDate).ToList(),
                ScoreCount = daysOfYes
            };
        }

        public static bool UpdateChallengeScore(ChallengeScore challengeScore)
        {
            return challengeAdapter.UpdateChallengeScore(challengeScore);
        }
        public static bool SendChallengeInvite(long challengeId, string personalMessage)
        {
            return challengeAdapter.SendChallengeInvite(challengeId, personalMessage);
        }
        public static long AddChallengeScore(ChallengeScore challengeScore)
        {
            return challengeAdapter.AddChallengeScore(challengeScore);
        }

        public static ChallengeMemberScore GetChallengeMember(long memberId, long challengeId)
        {
            return challengeAdapter.GetChallengeMember(memberId, challengeId);
        }

        public static List<ChallengeMemberScore> GetChallengeLeaders(long challengeId, int pageNumber, int pageSize)
        {

            var leaders = challengeAdapter.GetChallengeLeaders(challengeId, pageNumber, pageSize);
            const string DEF_IMAGE_PATH = "/images/Social/img-no-profile.gif";
            string imagePath = ConfigurationManager.AppSettings["CDNBaseURL"];
            imagePath += ConfigurationManager.AppSettings["MemberImageUrl"];

            leaders.ForEach(
                l => l.ProfilePicUrl = l.ProfilePicture != null && l.ProfilePicture.Length > 0
                                           ? imagePath + new Guid(l.ProfilePicture).ToString() + ".jpg"
                                           : DEF_IMAGE_PATH
                );
            return leaders;
        }

        public static long SaveChallenge( string sessionId, long memberId, string name, string description, string question,
                                          string displayType, byte frequency, string frequencyDatePart,
                                          string answType, string scoreType, string promptType, int? target, string targetType, long badgeId, 
                                          string category, string type, string createType, string ownerType, string partType )
        {

            var today = DateTime.Now.Date; // remove time values  
            const string REWARD_TYPE_BADGE = "BADGE";
            const string TRIGGER_CODE_FIRST_ENTRY = "FIRST";
            const string TRIGGER_CODE_ALL_DONE = "ALLDN";
            const string TRIGGER_CODE_FIRST_ENTRY_DESCRIPTION = "First Entry";
            const string TRIGGER_CODE_ALL_DONE_DESCRIPTION = "All Done";

            var startDate = today;
            var endDate = GetPersonalTrackingEndDate( startDate, frequencyDatePart );
            const string SAVED_FREQUENCY = "dd"; // save these as one per day for now

            // If this is a target challenge (aka not yes/no), then the rule is:
            var answerKey = GetChallengeAnswerKey( promptType, targetType, target );
            int? idealMax = null;
            int? idealMin = null;

            var answerKeys = new List<ChallengeAnswerKey>();  
            if ( answerKey != null )
            {
                answerKeys.Add( answerKey );
            } else
            {
                idealMin = 0;
                idealMax = 1;
            } 

            var newQuestion = new ChallengeQuestion
                {
                    Question = question,
                    DisplayType = displayType,
                    AnswerType = answType,
                    IdealMin = idealMin,
                    IdealMax = idealMax,
                    QuestionFrequency = frequency,
                    QuestionFrequencyDatePart = SAVED_FREQUENCY,
                    MinValue = idealMin,
                    MaxValue = idealMax,
                    ScoreType = scoreType,
                    ChatEnabled = true,
                    LeaderBoardEnabled = true,
                    QuestionPromptType = promptType,
                    ChallengeAnswerKeys = answerKeys,
                    ChallengeTasks = new List<ChallengeTask>
                        {
                            new ChallengeTask
                                {
                                    Description = TRIGGER_CODE_ALL_DONE_DESCRIPTION,
                                    TriggerCode = TRIGGER_CODE_ALL_DONE,
                                    RewardLimit = 1,
                                    ChallengeTaskRewards = new List<ChallengeTaskReward>
                                        {
                                            new ChallengeTaskReward
                                                {
                                                    BadgeID = badgeId,
                                                    RewardType = REWARD_TYPE_BADGE,
                                                    CertificateName = null,
                                                    ExpirationDays = null,
                                                    RewardDetails = null,
                                                    Amount = 1
                                                }
                                        }
                                }
                        }
                };

            var questions = new List<ChallengeQuestion> { newQuestion };

            var uploadDeadlineDays = int.Parse( ConfigurationManager.AppSettings["UploadDeadlineDaysAfterChallenge"] );

            var newChallenge = new PersonalTrackingChallenge
                {
                    ChallengeCategory = category,
                    ChallengeType = type,
                    ChallengeQuestions = questions,
                    TeamCreationType = createType,                    
                    OwnerType = ownerType,
                    ParticipationType = partType,
                    Live = true,
                    RulesCopy = description,
                    Name = name,
                    OwnerID = memberId,
                    GoLiveDate = today,
                    SignUpDeadline = endDate,
                    UploadDeadline = endDate.AddDays(uploadDeadlineDays),
                    IsChatEnabled = true,
                    StartDate = startDate,
                    EndDate = endDate,
                    EndingReminderEmailDate = endDate,
                    HasSentEndingReminderEmail = false
                };

            // Save the challenge
            var challengeId = challengeAdapter.SaveChallenge( newChallenge, memberId );

            // Add the member to the challenge
            AddPersonalTrackingChallengeMember( memberId, challengeId );

            // Send the invitations
            SendPersonalTrackingInvite( memberId, challengeId );

            return challengeId;
        }

        private static ChallengeAnswerKey GetChallengeAnswerKey( string promptType, string targetType, int? target )
        {
            ChallengeAnswerKey answerKey = null;

            int maxVal;
            int minVal = maxVal = 0;

            if ( promptType == ChallengePromptType.TXT.ToString() )
            {
                if ( targetType == ChallengeTargetType.MORE.ToString().ToLowerInvariant() )
                {
                    minVal = target.GetValueOrDefault();
                    maxVal = int.MaxValue;
                }
                if (targetType == ChallengeTargetType.LESS.ToString().ToLowerInvariant())
                {
                    minVal = 0;
                    maxVal = target.GetValueOrDefault();
                }

                answerKey = new ChallengeAnswerKey
                    {
                        Description = string.Empty,
                        EntryMin = minVal,
                        EntryMax = maxVal,
                        Score = 1
                    };
            }

            return answerKey;
        }

        public static long AddPersonalTrackingChallengeMember(long memberId, long challengeId)
        {
            var challengeMember = new ChallengeMember {
                ChallengeId = challengeId,
                MemberId = memberId,
                SignUp = DateTime.Now
            };

            return challengeAdapter.AddChallengeMember(challengeMember); 
        }

        /// <summary>
        /// Adds a challenge invite record for the creator of a personal tracking challenge. In this case,
        /// we only send memberId. Email address has to be blank or else it will add an extra record. 
        /// </summary>
        /// <param name="memberId"></param>
        /// <param name="challengeId"></param>
        private static void SendPersonalTrackingInvite(long memberId, long challengeId)
        {
            const bool INVITE_SENT = true;
            const bool IS_DECLINED = false;
            const bool DO_NOT_REMIND = false;
            const decimal HANDICAP = 0;

            challengeAdapter.AddChallengeInvite(challengeId, memberId.ToString(CultureInfo.InvariantCulture), string.Empty, INVITE_SENT,
                                                DateTime.Now, IS_DECLINED, DO_NOT_REMIND, HANDICAP, CHALLENGE_SOURCE_PERSONAL, null, memberId);
        }

        private static DateTime GetPersonalTrackingEndDate(DateTime startDate, string frequencyDatePart)
        {
            var endDate = startDate;

            switch (frequencyDatePart)
            {
                case "oneDay":
                    {
                        endDate = startDate.AddDays(1).AddHours(23).AddMinutes(59);
                        break;
                    }
                case "oneWeek":
                    {
                        endDate = startDate.AddDays(6).AddHours(23).AddMinutes(59);
                        break;
                    }
                case "twoWeek":
                    {
                        endDate = startDate.AddDays(13).AddHours(23).AddMinutes(59);
                        break;
                    }
                case "oneMonth":
                    {
                        endDate = startDate.AddMonths(1).AddDays(-1).AddHours(23).AddMinutes(59);
                        break;
                    }
            }
            return endDate;
        }

        public static List<SponsorSystemBadge> GetChallengeBadges(long sponsorId, string category)
        {
            return challengeAdapter.GetChallengeBadges( sponsorId, category );
        } 

        public static List<MemberProfile> SearchFriendsByName( long memberId, string name )
        {
            return socialAdapter.SearchFriendsByName( memberId, name );
        }

        public static List<RewardBadge> GetBadgesByType(BadgeType type)
        {
            var badges = rewardsAdapter.GetBadgesByType( type );
            return badges.OrderBy( b => b.BadgeCategoryCode ).ToList();
        } 

        public static void AddChallengeInvitesForAllFriends(long challengeId, long challengeCreator, string source)
        {
           challengeAdapter.AddChallengeInvitesForAllFriends(challengeId, challengeCreator, source); 
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="challengeId"></param>
        /// <param name="challengeCreator"></param>
        /// <param name="memberIds">comma delimitted list of memberIds</param>
        /// <param name="emails">comma delimitted list of emails. this will be in sync with the member id list </param>
        public static void AddChallengeInvites( string sessionId, long challengeId, long challengeCreator,
                                                string memberIds, string emails )
        {
            var today = DateTime.Now;

            challengeAdapter.AddChallengeInvite( challengeId, memberIds, emails, false, today, false, false, 0,
                                                 CHALLENGE_SOURCE_PERSONAL, null, challengeCreator );

            var memberIdList = GetMemberIdsFromList( memberIds );
            //need to do this carefully to ensure that all valid memberIds get into the list

            var notification = new MemberNotification
                {
                    NotifiedById = challengeCreator,
                    NotificationTypeEntityId = challengeId,
                    NotificationTypeId = NotificationType.CHALLENGEINVITATION,
                    NotificationStatusID = NotificationStatus.WAIT_FOR_ACTION
                };
            socialAdapter.AddMemberNotificationList( sessionId, memberIdList.ToList(), notification );

        }

        private static IEnumerable<long> GetMemberIdsFromList( string memberIds )
        {
            var memberIdList = new List<long>();
            var aMemberIds = memberIds.Split(',');
            foreach (string id in aMemberIds)
            {
                long tmpId = -1;
                long.TryParse(id, out tmpId);
                if (tmpId > 0)
                {
                    memberIdList.Add(tmpId);
                }
            }
            return memberIdList;
        }

        public static void SendTrackingNotification(string sessionId, string memberIds, long challengeId, long memberId )
        {
            #region send the notifications
            var notification = new MemberNotification
            {
                NotifiedById = memberId,
                NotificationTypeEntityId = challengeId,
                NotificationTypeId = NotificationType.TRACKINGINVITE,
                NotificationStatusID = NotificationStatus.WAIT_FOR_ACTION
            };
            var memberIdList = GetMemberIdsFromList(memberIds).ToList();
            socialAdapter.AddMemberNotificationList(sessionId, memberIdList, notification);
            #endregion

        }

        public static bool RemoveCacheByKey(string key)
        {
            return challengeAdapter.RemoveCacheByKey(key);
        }
    }
}
