﻿using System;
using System.Linq;
using Vhm.Web.BL;
using Vhm.Web.Common;
using Vhm.Web.DataServices.Adapters;
using Vhm.Web.DataServices.Interfaces;
using Vhm.Web.Models;
using Vhm.Web.Models.Enums;
using Vhm.Web.Models.RepositoryModels;
using System.Collections.Generic;
using Vhm.Web.Models.ViewModels.Member;
using VirginLifecare.Exception;

namespace Vhm.Web.ContentBL
{
    public static class ContentBL 
    {

        private static IContentAdapter _contentAdapter;

        static ContentBL()
        {
            Initialize();
        }

        private static void Initialize()
        {
            _contentAdapter = GetContentAdapter();
        }

        static IContentAdapter GetContentAdapter()
        {
            return _contentAdapter ?? new ContentAdapter();
        }

        public static EktronRichContent GetEktronRichContent(string contentAreaType, long sponsorId, int langId)
        {
            return _contentAdapter.GetEktronRichContent(contentAreaType, sponsorId, langId);
        }

        public static List<EktronRichContent> GetSponsorLatestBlogPosts(string contentAreaType, long sponsorId, int languageId, int count)
        {
            return _contentAdapter.GetSponsorLatestBlogPosts(contentAreaType, sponsorId, languageId, count);
        }

        public static EktronRichContent GetBlogPostById(long contentId, int languageId)
        {
            return _contentAdapter.GetBlogPostById(contentId, languageId);
        }
    }

}
