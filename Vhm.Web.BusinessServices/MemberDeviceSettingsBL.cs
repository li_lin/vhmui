﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;
using Vhm.Web.Models.RepositoryModels;
using Vhm.Web.Models.ViewModels.Device;

namespace Vhm.Web.BL
{
    public class MemberDeviceSettingsBL
    {
        /// <summary>
        /// A static method for executing requests towards VHM.API services
        /// </summary>
        /// <param name="method">The method.</param>
        /// <param name="uri">The URI.</param>
        /// <param name="jsonData">The json data.</param>
        /// <returns></returns>
        public static string ExecuteRequest(string method, string uri, string jsonData = "")
        {
            string html = string.Empty;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.Method = method;

            if (method == "POST" || method == "PUT")
            {
                byte[] data = Encoding.UTF8.GetBytes(jsonData);

                request.ContentType = "application/json";
                request.ContentLength = data.Length;
                request.Headers.Add("DeviceToken", CreateGUID());

                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(data, 0, data.Length);
                }
            }

            using (WebResponse response = request.GetResponse())
            {
                Stream stream = response.GetResponseStream();
                if (stream != null)
                {
                    StreamReader reader = new StreamReader(stream);
                    html = reader.ReadToEnd();
                }
                response.Close();
            }

            return html;
        }

        public static string ExecuteGetRequest(string url)
        {
            string html = string.Empty;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = "application/json";
            request.Headers.Add("DeviceToken", CreateGUID());
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream stream = response.GetResponseStream();
            if (stream != null)
            {

                StreamReader reader = new StreamReader(stream);
                html = reader.ReadToEnd();
            }
            response.Close();
            return html;
        }
        public static VhmMemberMaxSettings GetMemberDeviceSettingsBL(string url)
        {
            try
            {
                var memberDeviceSettings = new VhmMemberMaxSettings();
                var webApiRequest = ExecuteGetRequest( url );
                var settingsRequest = new JavaScriptSerializer().Deserialize<SettingsRequest>(webApiRequest);
                if ( settingsRequest != null )
                {
                    memberDeviceSettings = BuildVhmMemberMaxSettings( settingsRequest );
                }
                return memberDeviceSettings;
            }
            catch ( Exception ex )
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(new Exception("Error trying to retrieve member's device settings using Device web api. " , ex));
                return new VhmMemberMaxSettings();
            }
        }
        public static VhmMemberMaxSettings SaveMemberDeviceSettigsBL(VhmMemberMaxSettings memberMaxSettings, string method, string uri)
        {
            try
            {
                var savedValues = new VhmMemberMaxSettings();
                #region Save Values
                //create the SettingsRequest object
                string nickName =  !string.IsNullOrEmpty(memberMaxSettings.Nickname) ? memberMaxSettings.Nickname : string.Empty;

                MemberSettings memberSettings = new MemberSettings()
                    {
                        MidnightReset = true,//memberMaxSettings.MidnightReset,
                        HrTimeFormat = memberMaxSettings.HrTimeFormat,
                        BaseScreensToDisable = GetBaseScreensToDisable(memberMaxSettings.SyncScreenOn, memberMaxSettings.CalDistScreenOn, memberMaxSettings.SunScreenOn, memberMaxSettings.ClockScreenOn, memberMaxSettings.BumpScreenOn),
                        ScenarioScreensToDisable = GetScenarioScreensToDisable(memberMaxSettings.EngagementMessagingOn)
                    };
                var settingsRequestObj = new SettingsRequest() { MemberId = memberMaxSettings.MemberId, Nickname = nickName, DistanceDisplayMetric = memberMaxSettings.DistanceDisplayMetric, MemberSettings = memberSettings };
                var serializedJson = new JavaScriptSerializer().Serialize(settingsRequestObj);
                string settingsRequest = serializedJson;

                string settignsResponse = ExecuteRequest(method, uri, settingsRequest);
                if ( !string.IsNullOrEmpty( settignsResponse ) )
                {
                    var settingsResponseObject = new JavaScriptSerializer().Deserialize<SettingsRequest>(settignsResponse);
                    savedValues = BuildVhmMemberMaxSettings(settingsResponseObject);
                }
                #endregion
                return savedValues;
            }
            catch(Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(new Exception("Error trying to save member's device settings using Device web api. ", ex));
                throw;
            }
        }
        public SponsorModule GetSponsorValidatedActivityModule( long sponsorId )
        {
            var sponsorModule = SponsorBL.GetSponsorModules(sponsorId);
            var vacModule = sponsorModule.Find(item => item.ModuleTypeCode == "VAC");
            if ( vacModule != null && vacModule.AllowBumpChallenge == null )
            {
                vacModule.AllowBumpChallenge = true; //if null always default to true 
            }
            if(vacModule != null && vacModule.AllowDeviceMessaging == null)
            {
                vacModule.AllowDeviceMessaging = true; //if null always default to true
            }
            return vacModule;
        }
       
        #region Private Methods
        private static string CreateGUID()
        {
            return Guid.NewGuid().ToString();
        }
        private static VhmMemberMaxSettings BuildVhmMemberMaxSettings(SettingsRequest settingsRequest)
        {
            try
            {
                var memberDeviceSettings = new VhmMemberMaxSettings();
                
                var settWithBaseScreen = GetBaseScreenDisabled(settingsRequest.MemberSettings.BaseScreensToDisable,
                                                                memberDeviceSettings);
                if(settWithBaseScreen != null)
                {
                    memberDeviceSettings = settWithBaseScreen;
                }
                else
                {
                    memberDeviceSettings.CalDistScreenOn = true;
                    memberDeviceSettings.ClockScreenOn = true;
                    memberDeviceSettings.BumpScreenOn = true;
                    memberDeviceSettings.SunScreenOn = true;
                    memberDeviceSettings.SyncScreenOn = true;
                }
                memberDeviceSettings.Nickname = settingsRequest.MemberSettings.MemberData.NickName;
                memberDeviceSettings.HrTimeFormat = settingsRequest.MemberSettings.HrTimeFormat;
                memberDeviceSettings.MidnightReset = true;// settingsRequest.MemberSettings.MidnightReset;
                memberDeviceSettings.DistanceDisplayMetric = settingsRequest.MemberSettings.MemberData.MetricMeasurement;//settingsRequest.DistanceDisplayMetric;
                memberDeviceSettings.MemberData = settingsRequest.MemberSettings.MemberData;
                memberDeviceSettings.MemberId = settingsRequest.MemberId;
                memberDeviceSettings.EngagementMessagingOn =
                    settingsRequest.MemberSettings.ScenarioScreensToDisable == 0;

                return memberDeviceSettings;
            }
            catch(Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(new Exception("Error trying to retrieve member's device settings using Device web api. ", ex));
                return new VhmMemberMaxSettings();
            }
        }
        #region Convert bool values to unsigned int/short corespondents for base/scenario screens params       
        /// <summary>
        /// Unsigned int representation of 32 bit control to select which scenario screen IDs (up to 32) to be disabled
        /// We curently have 11 scenarios. Engagement scenarios are: 3, 4, 6, 7, 8, 11. 
        /// Scenario 7 has been removed from displaying on device completely.
        /// Scenario 6 - bussiness wants to ALWAYS display, no matter the user's selections
        /// This means, if user sets Engagement messages off, we need to turn off scenarios: 3,4,8,11
        /// the boolArray below starts count from position 0 (position 0 - scenario 0, position 1 - scenario 1, ...)
        /// </summary>
        /// <param name="engagementOn"></param>
        /// <returns>ie. 0 = no scenariosreens disabled, ???? = all scenariosreens disabled.</returns>
        private static UInt32 GetScenarioScreensToDisable(bool engagementOn)
        {
            UInt32 result = 0;
            if(!engagementOn)
            {
                //result = 2048; - only for scenario 11 disabled
                // hex- 0x918 == dec-2328 --same thing. the screens repres starts from screen 0. 
                Boolean[] boolArray = new Boolean[32] 
                { 
                    false, false, false, true, true, false, false, false, //scenario0 scenario1 scenario2 scenario3 ...
                    true, false, false, true, false, false, false, false, 
                    false, false, false, false, false, false, false, false, 
                    false, false, false, false, false, false, false, false 
                };
                result = ba2uint(boolArray);
            }
            return result;
        }

        private static ushort GetBaseScreensToDisable(bool syncScreenOn, bool calDistScreenOn, bool sunScreenOn, bool clockScreenOn, bool bumpScreenOn)
        {
            ushort result = 0;
            bool allConfigurableBaseScreensOn = syncScreenOn && calDistScreenOn && sunScreenOn && clockScreenOn && bumpScreenOn; //this means all screens are on, none is disabled - return 0
            if(!(allConfigurableBaseScreensOn))
            {
                #region Explain screens
                //the screens repres starts from screen 1 - at first position.
                // true = means screen is not displaying to user; false means - screen displays on Max
                //for base screens, the scr 1 is position 0 in the array:
                //Boolean[] boolArray = new Boolean[16] 
                //scr1 - Step screen = boolArray[0]
                //scr2, scr3 - active minutes screens = (scr2 = boolArray[1]; scr3 = boolArray[2])
                //scr4 - calories and distance screen = boolArray[3]
                //scr5 - clock screen = 12hr format = = boolArray[4]; scr 6 - clockscreen 24hr format = = boolArray[5]
                //scr7, scr8, scr9 - bump screen. We only need to disable scr7. the others only show during bump
                //scr 10, scr11, scr12 - sync screens. We only need to disable scr10. the others only show during an actual sync
                //scr13 - some bluetooth message
                //scr14 - Pace screen - sun screen
                #endregion

                Boolean[] boolArray = new Boolean[16] 
                { 
                    //scr1  scr2   scr3   scr4 ...
                    false, false, false, !calDistScreenOn, !clockScreenOn, !clockScreenOn, !bumpScreenOn, 
                    false, false, !syncScreenOn, false, false, false, !sunScreenOn, false, false
                };
                result = ba2ushort(boolArray);
            }
            return result;
        }

        /// <summary>
        /// convert a boolean array to an unsingend int to be passed down to Max. To be used to get ScenarioScreensToDisable
        /// </summary>
        /// <param name="b"></param>
        /// <returns>Unsigned int representation of 32 bit control to select which scenario(up to 32) to be disabled, ie. 0 = no scenariosreens disabled, 4294967295 = all scenariosreens disabled.</returns>
        private static uint ba2uint(bool[] b)
        {
            uint ret = 0;
            if(b.Length < 32)
            {
                return 0;
            }
            uint one = 1;
            for(int i = 31; i >= 0; i--)
            {
                if(b[i])
                {
                    ret += (one << i);
                }
            }
            return ret;
        }
        /// <summary>
        /// convert a boolean array to an unsingend int to be passed down to Max - to be used to get the BaseScreensToDisable
        /// </summary>
        /// <param name="b"></param>
        /// <returns>Unsigned short representation of 16 bit control to select which base screen IDs (up to 16) to be disabled, i.e. 0 = no screens disabled, 65535 = all screens disabled.  </returns>
        private static ushort ba2ushort(bool[] b)
        {
            ushort ret = 0;
            if(b.Length < 16)
            {
                return 0;
            }
            ushort one = 1;
            for(short i = 15; i >= 0; i--)
            {
                if(b[i])
                {
                    ret += Convert.ToUInt16((one << i));
                }
            }
            return ret;
        }
        #endregion

        #region Convert ushort/uint to bool array of screens/scenario and translate
        // boolArray[3], boolArray[3], boolArray[3], boolArray[3] - true means Engagement Messages are off
        //OR we can simply check if val > 0 - then disable engagement screens
        private static bool GetEngagementValue(UInt32 val)
        {
            bool returnVal = false;
            if ( val == 0 )
            {
                returnVal = false;
            }
            Boolean[] boolArray = new Boolean[32];
            boolArray = uInt2ba( val );
            if(Convert.ToBoolean(boolArray[3]) && Convert.ToBoolean(boolArray[4]) && Convert.ToBoolean(boolArray[3]) && Convert.ToBoolean(boolArray[3]))
            {
                returnVal = true;
            }
            return returnVal;
        }

        private static VhmMemberMaxSettings GetBaseScreenDisabled( ushort val, VhmMemberMaxSettings memberMaxSettings )
        {
            try
            {
                if ( val == 0 ) // no screens disabled
                {
                    memberMaxSettings.CalDistScreenOn = true;
                    memberMaxSettings.ClockScreenOn = true;
                    memberMaxSettings.BumpScreenOn = true;
                    memberMaxSettings.SunScreenOn = true;
                    memberMaxSettings.SyncScreenOn = true;
                } 
                else
                {
                    #region Explain screens
                    //the screens repres starts from screen 0.
                    // true = means screen is not displaying to user; false means - screen displays on Max
                    //for base screens, the scr 1 is position 0 in the array:
                    //Boolean[] boolArray = new Boolean[16] 
                    //scr1 - Step screen = boolArray[0]
                    //scr2, scr3 - active minutes screens = (scr2 = boolArray[1]; scr3 = boolArray[2])
                    //scr4 - calories and distance screen = boolArray[3]
                    //scr5 - clock screen = 12hr format = = boolArray[4]; scr 6 - clockscreen 24hr format = boolArray[5]
                    //scr7, scr8, scr9 - bump screen. We only need to disable scr7. the others only show during bump  (scr 7 = boolArray[6])
                    //scr 10, scr11, scr12 - sync screens. We only need to disable scr10. the others only show during an actual sync (scr10 = = boolArray[9])
                    //scr13 - some bluetooth message = boolArray[12]
                    //scr14 - Pace screen - sun screen = boolArray[13]
                    #endregion
                    Boolean[] boolArray = new Boolean[16];
                    boolArray = uShort2ba(val);
                    memberMaxSettings.CalDistScreenOn = !Convert.ToBoolean(boolArray[3]);
                    memberMaxSettings.ClockScreenOn = !Convert.ToBoolean(boolArray[4]);
                    memberMaxSettings.BumpScreenOn = !Convert.ToBoolean(boolArray[6]);
                    memberMaxSettings.SunScreenOn = !Convert.ToBoolean(boolArray[13]);
                    memberMaxSettings.SyncScreenOn = !Convert.ToBoolean(boolArray[9]);
                }
                return memberMaxSettings;
            }
            catch ( Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(new Exception("Error trying to convert ushort to a bool array to interpret the screens that are disabled", ex));
                return null;
            }
        }
        /// <summary>
        /// convert an unsingend int to a boolean array to be passed down to Max. To be used to get ScenarioScreensToDisable
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        private static bool[] uShort2ba(ushort val)
        {
            Boolean[] boolArray = new Boolean[16];
            if(val == 0)
            {
                return boolArray;
            }
            ushort one = 1;
            for(short i = 15; i >= 0; i--)
            {
                var test = val >> i & one;
                boolArray[i] = Convert.ToBoolean(test);
            }
            return boolArray;
        }
        /// <summary>
        /// convert an unsingend int to a boolean array to be passed down to Max - to be used to get the BaseScreensToDisable
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        private static bool[] uInt2ba(uint val)
        {
            Boolean[] boolArray = new Boolean[32];
            if(val == 0)
            {
                return boolArray;
            }
            uint one = 1;
            for(short i = 31; i >= 0; i--)
            {
                var test = val >> i & one;
                boolArray[i] = Convert.ToBoolean(test);
            }
            return boolArray;
        }
        #endregion
        #endregion

    }
}
