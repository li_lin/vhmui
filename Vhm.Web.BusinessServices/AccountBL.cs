﻿using System;
using System.Web.Security;
using Vhm.Web.DataServices.Adapters;
using Vhm.Web.Models;
using Vhm.Web.DataServices.Interfaces;
using Vhm.Web.Models.Enums;

namespace Vhm.Web.BL
{
    public static class AccountBL
    {
        private static readonly IAccountAdapter _accountAdapter;

        static AccountBL()
        {
            _accountAdapter = GetAccountAdapter();
        }

        private static IAccountAdapter GetAccountAdapter()
        {
            return _accountAdapter ?? new AccountAdapter();
        }

        public static void KillSession(string sessionId)
        {
            _accountAdapter.KillSession( sessionId );
        }

        public static Session Login(string userName, string password, string system, int timeout)
        {
            var newSession = _accountAdapter.GetNewSession(userName, password, system, timeout);

            if (newSession != null)
            {
                newSession.LoginStatus = CheckLoginStatus(newSession);
            }
            else
            {
                newSession = new Session();
                newSession.LoginStatus = LoginStatus.Error;
            }

            return newSession;
        }

        private static LoginStatus CheckLoginStatus(Session session)
        {
            if (session == null || String.IsNullOrEmpty(session.SessionID))
            {
                return LoginStatus.BadUserNamePassword;
            }

            if (session.Locked == 1)
            {
                return LoginStatus.LockedOut;
            }

            if (session.LastTryWarning == 1)
            {
                return LoginStatus.LastBadLogin;
            }

            return LoginStatus.LoginOk;
        }

        public static long GetMemberIdBySession(string sessionId)
        {
            long memberId = 0;

            var session = _accountAdapter.GetMemberIdBySession(sessionId);

            if (session != null)
            {
                memberId = session.MemberID;
            }

            return memberId;
        }

        public static Session GetSession(string sessionId)
        {
            return _accountAdapter.GetMemberIdBySession(sessionId);
        }

        public static void Logout()
        {
            FormsAuthentication.SignOut();
        }

        public static bool UpdateSession(Session session, int timeOut)
        {
            return _accountAdapter.UpdateSession(session, timeOut);
        }
    }
}
