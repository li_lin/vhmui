﻿using System;
using Vhm.Providers;
using Vhm.Web.BL.SocialWebService;
using Vhm.Web.DataServices.Adapters;
using Vhm.Web.DataServices.Interfaces;
using Vhm.Web.DataServices.MiddleTierActivityService;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.BL
{
    public static class ActivityBL
    {
        private static readonly IActivityAdapter _activityAdapter;

        static ActivityBL()
        {
            _activityAdapter = GetActivityAdapter();
        }

        private static IActivityAdapter GetActivityAdapter()
        {
            return _activityAdapter ?? new ActivityAdapter();
        }

        public static bool IsMemberConnectedToRunKeeper(long memberID)
        {
            //check Cache to see if "IsConnectedToRunKeeper" is populated; if so, get the value from there
            var isConnectedToRunKeeperObj = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.IsConnectedToRunKeeper);
            var isConnectedToRunKeeper = false;
            if(isConnectedToRunKeeperObj != null)
            {
                isConnectedToRunKeeper = bool.Parse(isConnectedToRunKeeperObj.ToString());
            }
            else
            {
                isConnectedToRunKeeper = _activityAdapter.IsMemberConnectedToRunKeeper(memberID);
                CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.IsConnectedToRunKeeper, isConnectedToRunKeeper);
            }
            return isConnectedToRunKeeper;
        }

        public static bool AddMemberRunkeeperToken(long memberID, string code)
        {
            return _activityAdapter.AddMemberRunkeeperToken(memberID, code);
        }

        public static bool DeleteMemberRunkeeperToken(long memberID)
        {
            return _activityAdapter.DeleteMemberRunkeeperToken(memberID);
        }

        public static bool SyncRunkeeperData(long memberID, bool forceSyncByPassCache)
        {
            var isRunKeeperDataSyncObj = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.IsRunKeeperDataSync);
            var isRunKeeperDataSync = isRunKeeperDataSyncObj != null && bool.Parse(isRunKeeperDataSyncObj.ToString());
            //if forceSyncByPassCache = true - we will sync data or if cache key IsRunKeeperDataSync is null - force a sync of the data; ignore, otherwise
            if(forceSyncByPassCache || !isRunKeeperDataSync)
            {
                isRunKeeperDataSync = _activityAdapter.SyncRunkeeperData(memberID);
                CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.IsRunKeeperDataSync, true);
                //hard coded true value so that if there is an issue on RunKeeper side, do not keep on trying to sync each time we load landing page. this data can be forced to be synced from aj page anyway.
            }
            return isRunKeeperDataSync;
        }

        public static bool IsMemberConnectedToFitbit(long memberID)
        {
            //check Cache to see if "IsConnectedToFitbit" is populated; if so, get the value from there
            var isConnectedToFitbitObj = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.IsConnectedToFitbit);
            var isConnectedToFitbit = false;
            if(isConnectedToFitbitObj != null)
            {
                isConnectedToFitbit = bool.Parse(isConnectedToFitbitObj.ToString());
            }
            else
            {
                isConnectedToFitbit = _activityAdapter.IsMemberConnectedToFitbit( memberID );
                CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.IsConnectedToFitbit, isConnectedToFitbit);
            }
            return isConnectedToFitbit;
        }

        public static bool AddMemberFitbitTokens(long memberID, FitbitAuthInfo code)
        {
            return _activityAdapter.AddMemberFitbitTokens(memberID, code);
        }

        public static bool DeleteMemberFitbitTokens(long memberID)
        {
            return _activityAdapter.DeleteMemberFitbitTokens(memberID);
        }

        public static bool IsMemberRegisterToGZ(long memberID)
        {
            return _activityAdapter.IsMemberRegisterToGZ(memberID);
        }

        public static bool IsMemberLinkedToPolar(long memberID)
        {
            return _activityAdapter.IsMemberLinkedToPolar(memberID);
        }

        /// <summary>
        /// Use this method to get member's Major Version. If member doesn't have a registered GoZone, this method will return the major version of the Sponsor's selected GoZone device.
        /// </summary>
        /// <param name="memberId"></param>
        /// <param name="sponsorId"></param>
        /// <returns></returns>
        public static string GetMemberOrSponsorMajorGoZoneVersion(long memberId, long sponsorId)
        {
            var wsSocial = new WSVHMSocialSoapClient( );
            var majorGoZoneVersion = "0";
            majorGoZoneVersion = wsSocial.GetMemberGoZoneVersionMajor( memberId, sponsorId );
            return majorGoZoneVersion;
        }

        /// <summary>
        /// Use this method to get member's Major Version of their registered GoZone
        /// If member doesn't have a goZone registered, this will return an Empty string. The diference between this method and GetMemberOrSponsorMajorGoZoneVersion is that GetMemberMajorGoZoneVersion will return SPonsor's available gozone version for a member that doesn't have a device registered. This is not what we might need if we need to specifically know if a member has a gozone registered to them and what is its version.
        /// </summary>
        /// <param name="memberId"></param>
        /// <returns>Member's major version of it's registered gozone.</returns>
        public static string GetMemberGoZoneVersionMajor( long memberId )
        {
            try
            {
                string deviceVersion = "";
                //since this will be used in the header, we need to cache it so we don't go grab it from MT each time we load a page
                var memberGzDeviceMajorVersion = CacheProvider.Get( System.Web.HttpContext.Current, CacheProvider.CacheItems.MemberGoZoneDeviceMajorVersion );
                //var checkIfFinishedWJ = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.FinishedWelcomeJourney);
                if ( memberGzDeviceMajorVersion == null && memberId > 0 )
                {
                    MemberDevice memberGzDevice = _activityAdapter.GetMemberExistingGoZoneDevice( memberId );
                    if ( memberGzDevice != null && !string.IsNullOrEmpty( memberGzDevice.SerialNumber ) && memberGzDevice.SerialNumber.Length > 2 )
                    {
                        //the first 2 characters in the serial number represent the PID = the product id or major version
                        deviceVersion = memberGzDevice.SerialNumber.Substring( 0, 2 );
                    } 
                    CacheProvider.Add( System.Web.HttpContext.Current, CacheProvider.CacheItems.MemberGoZoneDeviceMajorVersion, deviceVersion );
                    //no need to check if this is being populated. we need to store whatever we get back from MT
                } else
                {
                    deviceVersion = memberGzDeviceMajorVersion != null ? memberGzDeviceMajorVersion.ToString() : "";
                }
                return deviceVersion;
            }
            catch(Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(new Exception("Error trying to determine member's registered gozone major version for user with memberId: " +memberId, ex));
                return string.Empty;
            }
        }
    }
}
