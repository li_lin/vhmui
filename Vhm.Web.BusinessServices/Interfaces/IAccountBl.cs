﻿using System.Collections.Generic;
using Vhm.Web.Models;

namespace Vhm.Web.BL
{
    public interface IAccountBl
    {
        void KillSession( string sessionId );
        long GetMemberIdBySession(string sessionId);
        Session GetSession(string sessionId);
        Session Login(string userName, string password, string system, int timeout);
        void Logout();
        bool UpdateSession( Session session, int timeOut );
    }
}
