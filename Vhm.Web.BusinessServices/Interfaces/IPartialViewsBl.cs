﻿using System;
using System.Collections.Generic;
using Vhm.Web.Models.ViewModels.Social;
using Vhm.Web.Models.ViewModels.PartialViews;
using Vhm.Web.Models.ViewModels;
using Vhm.Web.Models.InputModels;
using System.IO;

namespace Vhm.Web.BL
{
    public interface IPartialViewsBl
    {
        List<MemberVM> AddToInvitationList(string sessionId, List<long> inviteList, List<long> newMembersIds);
        List<MemberVM> FilterInvitedMembersIds(long inviterId, List<MemberVM> memberIds, List<long> existingList);
        //TODO - remove this as it is not needed anymore
        byte[] GetGroupImage(long id, string defaultImagePath);
        string GetGroupImagePath( long id );
        InviteFriendsGroupsVM GetInviteFriendsGroupsVM(string sessionId, PagingIM paging);
        //TODO - remove this as it is not used anymore. We're not storing images in database
        byte[] GetMemberImage(string sessionId, long id, string defaultImagePath);
        string GetMemberImagePath( long mId );
        List<long> GetMemeberIdFromCSV(string sessionId, StreamReader csvStreamReader);
        List<MemberVM> GetMoreFriends(string sessionId, PagingIM paging);
        List<GroupVM> GetMoreGroups(string sessionId, PagingIM paging, bool includeImage);
        List<MemberVM> GetNewInviteList(string sessionId, long inviterId, List<long> memberIds, List<long> groupIds, List<long> existingList);
    }
}
