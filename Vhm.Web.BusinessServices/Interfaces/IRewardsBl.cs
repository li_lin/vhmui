﻿using System.Collections.Generic;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.BL.Interfaces
{
    public interface IRewardsBl
    {
        bool MemberEarnedReward(Member member, string triggerCode);
        List<UpForGrabsItem> GetMemberAvailableUpForGrabs(long memberId, long sponsorId);
        Game GetSponsorCurrentGame(string sessionId, Member member, long sponsorId, int? hm );
        List<MemberLevelsAudit> GetMemberLevelsAudit(long memberId, long sponsorId);
        BadgeItem GetBadge(long badgeId);
        List<GameTrigger> GetSystemTriggers(long memberId, long sponsorId);
    }
}
