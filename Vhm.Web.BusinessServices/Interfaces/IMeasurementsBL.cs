﻿using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.BL.Interfaces {
    public interface IMeasurementsBL
    {
        MeasurementReward GetMemberLatestMeasurementSummary(long memberId, int? maxResults);
        bool UpdateMemberLatestMeasurementSummary(long memberId, int? maxResults);
    }
}
