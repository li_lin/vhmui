﻿using System;
using Vhm.Web.Models;
using Vhm.Web.Models.RepositoryModels;
using System.Collections.Generic;

namespace Vhm.Web.BL
{
    public interface IMemberBl
    {
        bool CheckForRegisteredDevice( long memberId );
        void PostCookieToEloqua(string eloquaCookieGuid, long memberId);
        bool SaveMemberPrivateSettings(string sessionID, Member member, bool deletePictureProfile);
        List<MemberGoalsAndInterests> GetMemberGoalsAndInterests(string sessionId, long memberId);
        List<RecentlyEarnedRewardsItem> GetRecentlyEarnedRewards(long memberId, int count, bool? measurmentRewardsOnly);
        Dictionary<T, string> GetContentLabels<T>( string pageName, int languageId ) ;
        List<ChallengeBase> GetMemberChallenges(long memberId, long sponsorId, byte displayDays, int languageId);
        ActivityGraphData GetMemberActivityGraphData(long memberId, int graphHeight, int activityDaysToShow, int stepsTargetDefault);
        bool BadgesEnabledForSponsor( long sponsorId );
        Member GetMemberById( string sessionId, long memberId );
        List<MemberModule> GetMemberModules( long memberId, long sponsorId );
        byte? GetMemberWelcomeJourneyCurrentStep( long memberId );
        byte SaveMemberWelcomeJourneyCurrentStep( long memberId, byte currentStep );
        List<CultureFormat> GetCultureFormats();
        List<Terms> GetTerms(long memberId, string termsCode, int version);
        List<Terms> GetTermsAndConditions(long memberId);
        List<Terms> GetTermsToAcknowledge(long memberId);
        bool UpdateAcknowledgeMemberTerms(string sessionID, int timeOut, string termsCode, bool accepted, string source, long memberID);
        MemberPersonalInformation GetMemberPersonalInformation( long memberId );
        bool SaveMemberPersonalInformation( MemberPersonalInformation member );
        decimal InsertMeasurementAssessmentGrouping( Int16 assessmentGroupingTypeID );
        Int64 MeasurementInsert(Int64? assessmentGroupingID, Int16 measurementTypeID, Int64 performedByEntityID,
                                 Int64 memberEntityID, Int16 statusID, Int32 sourceID, double? unSystemValue,
                                 double? unUserValue, Int64? groupID, DateTime udTimeZoneDate, bool? validated,
                                 byte? dataCollectionMethodID, Int64? sourceDataReferenceID);

        bool SaveEmailPromptFlag( Int64 entityID, DateTime? hzAccess, DateTime? hcAccess, DateTime? faDate,
                                  DateTime? questStart, DateTime? logExer, DateTime? exeProg, DateTime? completedReg,
                                  DateTime? stepsLastUploaded, DateTime? noSmokingAgreement, DateTime? measurementsTaken );

        bool SaveHealthSnapshotOption( Session session, int option );
        bool UpdateVhmSession( Session session, int timeout );

        void SaveKagEntry(long promoId, long memberId, decimal score, DateTime date );
        List<Voucher> CheckVoucherByCode( string voucherCode );
        bool RedeemRewardsVoucher( Voucher voucher, long memberId );
        Tickers GetTickers(long memberId, long sponsorId);
        DateTime GetMemberNow( long memberId, DateTime systemDate );
        bool UpdateLastHraDate( long memberId );
        List<BadWords> GetBadWords(int? badWordId);
    }
}
