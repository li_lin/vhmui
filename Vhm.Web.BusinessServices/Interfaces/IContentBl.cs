﻿using System;
using Vhm.Web.Models;
using Vhm.Web.Models.RepositoryModels;
using System.Collections.Generic;

namespace Vhm.Web.BL
{
    public interface IContentBl
    {
        EktronRichContent GetEktronRichContent(string contentAreaType, long sponsorId, int languageId);

        List<EktronRichContent> GetSponsorLatestBlogPosts( string contentAreaType, long sponsorId, int languageId,
                                                           int count );
        EktronRichContent GetBlogPostById( long contentId, int languageId );
    }
}
