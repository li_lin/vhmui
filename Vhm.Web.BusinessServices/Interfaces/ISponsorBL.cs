﻿using System.Collections.Generic;
using Vhm.Web.DataServices.Adapters;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.BL.Interfaces {
    public interface ISponsorBL
    {
        SponsorProfile GetSponsorProfile( long sponsorId );
        List<ColorTheme> GetAvailableSponsorColorThemes(long sponsorId);
    }
}
