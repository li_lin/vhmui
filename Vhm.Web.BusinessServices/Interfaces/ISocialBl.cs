﻿using System.Collections.Generic;
using Vhm.Web.Models.Enums;
using Vhm.Web.Models.InputModels;
using Vhm.Web.Models.RepositoryModels;
using Vhm.Web.Models.ViewModels.Social;

namespace Vhm.Web.BL.Interfaces
{
    public interface ISocialBl
    {
        long AddGroup(string sessionId, long memberId, GroupIM groupIM, long communityId);
        bool AddMemberToGroup(string sessionId, Group group, long memberId);
        bool DeclineNotification(string sessionId, MemberNotification notific);
        bool DeleteGroup(string sessionId, long memberId, long id);
        List<GroupCategory> GetAllGroupCategories(string sessionId);
        List<PendingGroupInvitation> GetAllGroupInvitations(string sessionId, long memberId);
        CreateEditGroupVM GetCreateEditGroupVM(string sessionId, PagingIM paging, long? id, string mode, long memberId);
        Group GetGroupDetails(string sessionId, long groupId);
        GroupVM GetGroupVMDetails(string sessionId, long groupId, long memberId, int? noOfGroupMembers, bool? isPartOfGroup);
        byte[] GetGroupImage(long id, string defaultImagePath);
        byte[] GetGroupImageGuid(long id);
        SocialPagedData GetGroupMembers(string sessionId, long groupId, int? pageNumber, int? count, bool includeImage);
        PagedGroupsVM GetListOfSuggestedGroups(string sessionId, PagingIM paging, bool includePic);
        List<string> GetMemberFriendNames(string sessionId, long memberId);
        SocialPagedData GetMemberFriends(string sessionId, PagingIM paging);
        List<string> GetMemberGroupNames(string sessionId, long memberId);
        SocialPagedData GetMemberGroups(string sessionId, List<GroupTypes> types, PagingIM paging, bool includeImages);
        byte[] GetMemberImage(string sessionId, long id, string defaultImagePath);
        string GetMemberImagePath( long id, string memberPicturePath, string defaultMemberGuid );
        MemberVM GetMemberProfile(string sessionId, long memberId, long currentMemberId, bool includeImage);
        List<MemberVM> GetMembersByEmailsOrIdentityNumbers(string sessionId, IEnumerable<string> emails, IEnumerable<string> identityNumbers);
        List<MemberVM> GetMembersByEmailsOrIdentityNumbers(string sessionId, IEnumerable<string> emailsAndidentityNumbers);
        List<MemberVM> GetMembersByIds(string sessionId, List<long> mIds);
        MyGroupsModel GetMyGroupsObjects(string sessionId, long memberId, PagingIM pagingMyGroups, PagingIM pagSuggestedGrps, bool includeImages);
        PagedGroupsVM GetMyGroupsPaged(string sessionId, PagingIM pagingMyGroups, bool includeImages);
        SocialPagedData GetSuggestedGroups(string sessionId, PagingIM paging, bool includePic);
        bool IsGroupNameAvalibale(string sessionId, string groupName);
        bool IsMemberFriend(string sessionId, long memberId, long currentMemberId);
        SocialPagedData SearchGroupByAdministrator(string sessionId, PagingIM pagingSearchGrp, string adminName);
        PagedGroupsVM SearchGroupByAdminName(string sessionId, PagingIM pagingSearchGrp, string adminName);
        PagedGroupsVM SearchGroupByCategory(string sessionId, PagingIM pagingSearchGrp, GroupCategories categoryId);
        PagedGroupsVM SearchGroupByGroupName(string sessionId, PagingIM pagingSearchGrp, string groupName);
        SocialPagedData SearchGroupByName(string sessionId, PagingIM pagingSearchGrp, string groupName);
        SocialPagedData SearchGrpByCategory(string sessionId, PagingIM pagingSearchGrp, GroupCategories categoryId);
        bool SendGroupInvitation(string sessionId, long notifiedById, long groupId, List<long> mIds);
        bool UpdateGroup(string sessionId, long memberId, GroupIM groupIM);
        bool UpdateNotification(string sessionId, MemberNotification notification);
        List<MemberNotification> GetMemberNotifications(string sessionId, long memberId);
        int GetMemberNotificationCount(string sessionId, long memberId);
        List<NewsfeedPost> GetMemberNewsfeed(long memberId, long communityId, int commentCount);
        void LikePost(string sessionId, long memberId, long postById, long postId, long groupId);

        bool SumbitMemberPost( string sessionId, long memberId, string postText, long groupId, long communityId,
                               int newsFeedTypeId );
        string GetVhmContent(long sponsorId, long memberId, string contentAreaId, int languageId );
        string GetSponsorContentForMember(long memberId);
        bool IsMemberPartOfGroup(long memberId,long groupId);
        
    }
}
