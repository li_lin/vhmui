﻿using System;
using System.Collections.Generic;


namespace Vhm.Web.BL
{
    public interface IActivityBl
    {
        bool AddMemberRunkeeperToken(long memberID, string code);

        bool IsMemberConnectedToRunKeeper(long memberID);

        bool DeleteMemberRunkeeperToken(long memberID);

        bool SyncRunkeeperData(long memberID, bool forceSyncByPassCache);

        bool AddMemberFitbitTokens(long memberID, string code);

        bool IsMemberConnectedToFitbit(long memberID);

        bool DeleteMemberFitbitTokens(long memberID);

        bool IsMemberRegisterToGZ(long memberID);

        bool IsMemberLinkedToPolar(long memberID);
    }
}
