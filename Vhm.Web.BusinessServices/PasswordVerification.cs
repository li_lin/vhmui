﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Text.RegularExpressions;
using Vhm.Web.Common;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.BL
{
    public static class PasswordVerification
    {
        public static bool IsMatchToRegEx(string password, string regEx)
        {
            var regularExpression = new Regex(regEx);
            var matchResult = regularExpression.Match(password);

            return matchResult.Success;
        }

        public static bool IsExpire(int expireInDays, DateTime latestChangedDate)
        {
            return DateTime.Now.Subtract(latestChangedDate).Days + 1 > expireInDays;
        }

        public static bool IsDuplicatePassword(IEnumerable<byte[]> oldPins, byte[] encryptPin)
        {
            var newEncryptPinArray = new byte[50];
            encryptPin.CopyTo(newEncryptPinArray, 0);

            return oldPins.Any(oldPin => oldPin.SequenceEqual(newEncryptPinArray));
        }

        public static int GetCountRecentPasswords()
        {
            int recentPasswordsCount;
            if (Int32.TryParse(ApplicationSettings.RecentPasswordsCount, out recentPasswordsCount))
            {
                if (recentPasswordsCount > 0) return recentPasswordsCount;
            }
            return 2;
        }
    }
}
