﻿using System.Xml;
using log4net.Core;
using log4net.Layout;

namespace Vhm.Web.Common.Logging
{
    public class Log4NetXmlLayout : XmlLayoutBase
    {
        protected override void FormatXml(XmlWriter writer, LoggingEvent loggingEvent)
        {
            #region Start Entry

            writer.WriteStartElement("LogEntry");

            #endregion

            #region Level

            writer.WriteStartElement("Level");
            writer.WriteString(loggingEvent.Level.DisplayName);
            writer.WriteEndElement();

            #endregion

            #region Message

            writer.WriteStartElement("Message");
            writer.WriteString(loggingEvent.RenderedMessage);
            writer.WriteEndElement();

            #endregion

            #region Details

            writer.WriteStartElement("Details");

            if (loggingEvent.ExceptionObject != null)
            {
                writer.WriteString(loggingEvent.ExceptionObject.ToString());
            }

            writer.WriteEndElement();

            #endregion

            #region Stack Trace

            writer.WriteStartElement("StackTrace");

            if (loggingEvent.ExceptionObject != null)

                writer.WriteString(string.IsNullOrEmpty(loggingEvent.ExceptionObject.StackTrace)
                                       ? string.Empty
                                       : loggingEvent.ExceptionObject.StackTrace);

            writer.WriteEndElement();

            #endregion

            #region Timestamp

            writer.WriteStartElement("TimeStamp");
            writer.WriteString(loggingEvent.TimeStamp.ToString("dd/MM/yyyy HH:mm:ss"));
            writer.WriteEndElement();

            #endregion

            #region End Entry

            writer.WriteEndElement();

            #endregion
        }
    }
}
