﻿using System;
using System.Web;
using System.Web.SessionState;
using System.Collections;
using System.Threading;
using System.Web.Configuration;
using System.Configuration;

namespace Vhm.Web.Common
{
    public class CustomSessionStateModule : IHttpModule, IDisposable
    {

        private readonly Hashtable _pSessionItems = new Hashtable();
        private Timer _pTimer;
        private const int P_TIMER_SECONDS = 10;
        private bool _pInitialized;
        private int _pTimeout;
        private HttpCookieMode _pCookieMode = HttpCookieMode.UseCookies;
        private readonly ReaderWriterLock _pHashtableLock = new ReaderWriterLock();
        private ISessionIDManager _pSessionIDManager;
        private SessionStateSection _pConfig;


        // The SessionItem class is used to store data for a particular session along with 
        // an expiration date and time. SessionItem objects are added to the local Hashtable 
        // in the OnReleaseRequestState event handler and retrieved from the local Hashtable 
        // in the OnAcquireRequestState event handler. The ExpireCallback method is called 
        // periodically by the local Timer to check for all expired SessionItem objects in the 
        // local Hashtable and remove them. 

        private class SessionItem
        {
            internal SessionStateItemCollection items;
            internal HttpStaticObjectsCollection staticObjects;
            internal DateTime expires;
        }


        // 
        // IHttpModule.Init 
        // 

        public void Init(HttpApplication app)
        {
            // Add event handlers.
            app.AcquireRequestState += OnAcquireRequestState;
            app.ReleaseRequestState += OnReleaseRequestState;

            // Create a SessionIDManager.
            _pSessionIDManager = new SessionIDManager();
            _pSessionIDManager.Initialize();

            // If not already initialized, initialize timer and configuration. 
            if (!_pInitialized)
            {
                lock (typeof (CustomSessionStateModule))
                {
                    if (!_pInitialized)
                    {
                        // Create a Timer to invoke the ExpireCallback method based on 
                        // the pTimerSeconds value (e.g. every 10 seconds).

                        _pTimer = new Timer(ExpireCallback,
                                            null,
                                            0,
                                            P_TIMER_SECONDS*1000);

                        // Get the configuration section and set timeout and CookieMode values.
                        Configuration cfg =
                            WebConfigurationManager.OpenWebConfiguration(
                                System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
                        _pConfig = (SessionStateSection) cfg.GetSection("system.web/sessionState");

                        _pTimeout = (int) _pConfig.Timeout.TotalMinutes;
                        _pCookieMode = _pConfig.Cookieless;

                        _pInitialized = true;
                    }
                }
            }
        }



        // 
        // IHttpModule.Dispose 
        // 

        public void Dispose()
        {
            if (_pTimer != null)
            {
                _pTimer.Dispose();
                _pTimer.Dispose();
            }
        }


        // 
        // Called periodically by the Timer created in the Init method to check for  
        // expired sessions and remove expired data. 
        // 

        private void ExpireCallback(object state)
        {
            try
            {
                _pHashtableLock.AcquireWriterLock(Int32.MaxValue);

                RemoveExpiredSessionData();

            }
            finally
            {
                _pHashtableLock.ReleaseWriterLock();
            }
        }


        // 
        // Recursivly remove expired session data from session collection. 
        // 
        private void RemoveExpiredSessionData()
        {
            foreach (DictionaryEntry entry in _pSessionItems)
            {
                var item = (SessionItem) entry.Value;

                if (DateTime.Compare(item.expires, DateTime.Now) <= 0)
                {
                    string sessionID = entry.Key.ToString();
                    _pSessionItems.Remove(entry.Key);

                    var stateProvider =
                        new HttpSessionStateContainer(sessionID,
                                                      item.items,
                                                      item.staticObjects,
                                                      _pTimeout,
                                                      false,
                                                      _pCookieMode,
                                                      SessionStateMode.Custom,
                                                      false);

                    SessionStateUtility.RaiseSessionEnd(stateProvider, this, EventArgs.Empty);
                    RemoveExpiredSessionData();
                    break;
                }
            }

        }


        // 
        // Event handler for HttpApplication.AcquireRequestState 
        // 

        private void OnAcquireRequestState(object source, EventArgs args)
        {
            var app = (HttpApplication) source;
            HttpContext context = app.Context;
            bool isNew = false;
            SessionItem sessionData = null;
            bool supportSessionIDReissue;

            _pSessionIDManager.InitializeRequest(context, false, out supportSessionIDReissue);
            string sessionID = _pSessionIDManager.GetSessionID(context);


            if (sessionID != null)
            {
                try
                {
                    _pHashtableLock.AcquireReaderLock(Int32.MaxValue);
                    sessionData = (SessionItem) _pSessionItems[sessionID];

                    if (sessionData != null)
                        sessionData.expires = DateTime.Now.AddMinutes(_pTimeout);
                }
                finally
                {
                    _pHashtableLock.ReleaseReaderLock();
                }
            }
            else
            {
                bool redirected, cookieAdded;

                sessionID = _pSessionIDManager.CreateSessionID(context);
                _pSessionIDManager.SaveSessionID(context, sessionID, out redirected, out cookieAdded);

                if (redirected)
                    return;
            }

            if (sessionData == null)
            {
                // Identify the session as a new session state instance. Create a new SessionItem 
                // and add it to the local Hashtable.

                isNew = true;

                sessionData = new SessionItem();

                sessionData.items = new SessionStateItemCollection();
                sessionData.staticObjects = SessionStateUtility.GetSessionStaticObjects(context);
                sessionData.expires = DateTime.Now.AddMinutes(_pTimeout);

                try
                {
                    _pHashtableLock.AcquireWriterLock(Int32.MaxValue);
                    _pSessionItems[sessionID] = sessionData;
                }
                finally
                {
                    _pHashtableLock.ReleaseWriterLock();
                }
            }

            // Add the session data to the current HttpContext.
            SessionStateUtility.AddHttpSessionStateToContext(context,
                                                             new HttpSessionStateContainer(sessionID,
                                                                                           sessionData.items,
                                                                                           sessionData.staticObjects,
                                                                                           _pTimeout,
                                                                                           isNew,
                                                                                           _pCookieMode,
                                                                                           SessionStateMode.Custom,
                                                                                           false));

            // Execute the Session_OnStart event for a new session. 
            if (isNew && Start != null)
            {
                Start(this, EventArgs.Empty);
            }
        }

        // 
        // Event for Session_OnStart event in the Global.asax file. 
        // 

        public event EventHandler Start;


        // 
        // Event handler for HttpApplication.ReleaseRequestState 
        // 

        private void OnReleaseRequestState(object source, EventArgs args)
        {
            HttpApplication app = (HttpApplication) source;
            HttpContext context = app.Context;
            string sessionID;

            // Read the session state from the context
            HttpSessionStateContainer stateProvider =
                (HttpSessionStateContainer) (SessionStateUtility.GetHttpSessionStateFromContext(context));

            // If Session.Abandon() was called, remove the session data from the local Hashtable 
            // and execute the Session_OnEnd event from the Global.asax file. 
            if (stateProvider.IsAbandoned)
            {
                try
                {
                    _pHashtableLock.AcquireWriterLock(Int32.MaxValue);

                    sessionID = _pSessionIDManager.GetSessionID(context);
                    _pSessionItems.Remove(sessionID);
                }
                finally
                {
                    _pHashtableLock.ReleaseWriterLock();
                }

                SessionStateUtility.RaiseSessionEnd(stateProvider, this, EventArgs.Empty);
            }

            SessionStateUtility.RemoveHttpSessionStateFromContext(context);
        }


    }
}
