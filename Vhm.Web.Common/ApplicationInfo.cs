﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;

namespace Vhm.Web.Common
{
    [Serializable]
    public static class ApplicationSettings
    {
        #region Public Properties
        
        public static string BaseURL
        {
            get
            {

                return Convert.ToString(GetValue(configKey.BaseURL));
            }
        }
        public static string CDNBaseURL
        {
            get
            {
                return Convert.ToString(GetValue(configKey.CDNBaseURL));
            }
        }
        public static string GZSupportURL
        {
            get
            {
                return GetURLValue(configKey.GZSupportURL);
            }
        }
        public static string RegisterYourGoZone
        {
            get
            {
                return GetURLValue(configKey.RegisterYourGoZone);
            }
        }
        public static string RegisterYourMax
        {
            get
            {
                return GetURLValue(configKey.RegisterYourMax);
            }
        }
        public static string ManageYourAccountURL
        {
            get
            {
                return GetURLValue(configKey.ManageYourAccountURL);
            }
        }
        public static string DownloadGZURL
        {
            get
            {
                return GetURLValue(configKey.DownloadGZURL);
            }
        }
        public static string BuyPolarURL
        {
            get
            {
                return GetURLValue(configKey.BuyPolarURL);
            }
        }
        public static string BuyGoZoneURL
        {
            get
            {
                return GetURLValue(configKey.BuyGoZoneURL);
            }
        }
        public static string LearnMoreAboutPolar
        {
            get
            {
                return Convert.ToString(GetValue(configKey.LearnMoreAboutPolar));
            }
        }
        public static string SyncSoftwareURL
        {
            get
            {
                return GetURLValue(configKey.SyncSoftwareURL);
            }
        }
        public static string HowToConnectRK
        {
            get
            {
                return Convert.ToString(GetValue(configKey.HowToConnectRK));
            }
        }
        public static string HowToConnectFitBit
        {
            get
            {
                return Convert.ToString(GetValue(configKey.HowToConnectFitBit));
            }
        }
        public static string ConnectThroughRunkeeperPairingPdf
        {
            get
            {
                return Convert.ToString(GetValue(configKey.ConnectThroughRunkeeperPairingPdf));
            }
        }
        public static string ModuleImageUrl
        {
            get
            {
                return GetCDNImageURLValue(configKey.ModuleImageUrl);
            }
        }
        public static int DefaultLanguage
        {
            get
            {
                return Convert.ToInt32(GetValue(configKey.DefaultLanguage));
            }
        }
        public static string NewsfeedUrl
        {
            get
            {
                return GetURLValue(configKey.NewsfeedUrl);
            }
        }
        public static byte MiniNewsfeedDaysToInclude
        {
            get { return byte.Parse(GetValue(configKey.MiniNewsfeedDaysToInclude).ToString()); }
        }
        public static int StepsTargetDefault
        {
            get
            {
                return Convert.ToInt32(GetValue(configKey.StepsTargetDefault));
            }
        }
        public static int ActivityDaysToShow
        {
            get
            {
                return Convert.ToInt32(GetValue(configKey.ActivityDaysToShow));
            }
        }
        public static int ActivityGraphHeight
        {
            get
            {
                return Convert.ToInt32(GetValue(configKey.ActivityGraphHeight));
            }
        }
        public static int MaxRecentlyEarnedRewardsSummaryToShow
        {
            get
            {
                return Convert.ToInt32(GetValue(configKey.MaxRecentlyEarnedRewardsSummaryToShow));
            }
        }
        public static int MaxRecentlyEarnedRewardsToShow
        {
            get
            {
                return Convert.ToInt32(GetValue(configKey.MaxRecentlyEarnedRewardsToShow));
            }
        }
        public static string DeleteURL
        {
            get
            {
                return Convert.ToString(GetValue(configKey.DeleteURL));
            }
        }
        public static string ReturnURL
        {
            get
            {
                return GetURLValue(configKey.ReturnURL);
            }
        }
        public static string APIURL
        {
            get
            {
                return Convert.ToString(GetValue(configKey.APIURL));
            }
        }
        public static string TokenURL
        {
            get
            {
                return Convert.ToString(GetValue(configKey.TokenURL));
            }
        }
        public static string AuthURL
        {
            get
            {
                return Convert.ToString(GetValue(configKey.AuthURL));
            }
        }
        public static string ClientSecret
        {
            get
            {
                return Convert.ToString(GetValue(configKey.ClientSecret));
            }
        }
        public static string ClientID
        {
            get
            {
                return Convert.ToString(GetValue(configKey.ClientID));
            }
        }
        public static int TimeoutInterval
        {
            get
            {
                return Convert.ToInt32(GetValue(configKey.TimeoutInterval));
            }
        }
        public static string GroupImageUrl
        {
            get
            {
                return GetCDNImageURLValue(configKey.GroupImageUrl);
            }
        }
        public static string DefaultGroupPicGuid
        {
            get
            {
                return GetURLValue(configKey.DefaultGroupPicGuid);
            }
        }
        public static string DefaultMemberPicGuid
        {
            get
            {
                return Convert.ToString(GetValue(configKey.DefaultMemberPicGuid));
            }
        }
        public static byte ChallengeEndedDisplayDays
        {
            get
            {
                return byte.Parse(GetValue(configKey.ChallengeEndedDisplayDays).ToString());
            }
        }
        public static string MTEndpointName
        {
            get
            {
                return Convert.ToString(GetValue(configKey.MTEndpointName));
            }
        }
        public static string MTActivityEndpointName
        {
            get
            {
                return Convert.ToString(GetValue(configKey.MTActivityEndpointName));
            }
        }
        public static string CompanyName
        {
            get
            {
                return Convert.ToString(GetValue(configKey.CompanyName));
            }
        }
        public static int DBSessionTimeCheckInMinutes
        {
            get
            {
                return int.Parse(GetValue(configKey.DBSessionTimeCheckInMinutes).ToString());
            }
        }
        public static string PartnersPageSponsorModulURL
        {
            get
            {
                var url = Convert.ToString(GetValue(configKey.PartnersPageSponsorModulURL));
                var m = url.StartsWith("http");
                var ret = m ? url : BaseURL + url;
                return ret;
            }
        }
        public static string PartnersPageURL
        {
            get
            {
                return GetURLValue(configKey.PartnersPageURL);
            }
        }
        public static string MemberImageUrl
        {
            get
            {
                return GetCDNImageURLValue(configKey.MemberImageUrl);
            }
        }
        public static string SECPageUrl
        {
            get
            {

                return GetURLValue(configKey.SECPageUrl);

            }
        }
        public static string ProgramName
        {
            get
            {
                return Convert.ToString(GetValue(configKey.ProgramName));
            }
        }
        public static string ExpressProgramMenuDescription
        {
            get
            {
                return Convert.ToString(GetValue(configKey.ExpressProgramMenuDescription));
            }
        }
        public static string SiteImageUrl
        {
            get
            {
                return GetCDNImageURLValue(configKey.SiteImageUrl);
            }
        }
        public static string BadgeImageUrl
        {
            get
            {
                return GetCDNImageURLValue(configKey.BadgeImageUrl);
            }
        }
        public static string RewardTypeImageUrl
        {
            get
            {
                return GetCDNImageURLValue(configKey.RewardTypeImageUrl);
            }
        }
        public static byte MiniNewsfeedCount
        {
            get
            {
                return byte.Parse(GetValue(configKey.MiniNewsfeedCount).ToString());
            }
        }
        public static int ChallengeLeaderBoardPageSize
        {
            get { return int.Parse(GetValue(configKey.ChallengeLeaderBoardPageSize).ToString()); }
        }
        public static int ChallengeChatMessagesToDisplay
        {
            get { return int.Parse(GetValue(configKey.ChallengeChatMessagesToDisplay).ToString()); }

        }
        public static string UploadFileErrorURL
        {
            get
            {
                return GetURLValue(configKey.UploadFileErrorURL);
            }
        }
        public static string CachedResourceVersion
        {
            get
            {
                return Convert.ToString(GetValue(configKey.CachedResourceVersion));
            }
        }
        public static bool ShowException
        {
            get
            {
                return bool.Parse(GetValue(configKey.ShowException).ToString());
            }
        }
        public static string ShowGroupURL
        {
            get
            {
                return GetURLValue(configKey.ShowGroupURL);
            }
        }
        public static string WebSiteCDNImages
        {
            get
            {
                return GetCDNImageURLValue(configKey.WebSiteCDNImages);
            }
        }
        public static string VhmLogoImageURL
        {
            get
            {
                return GetCDNImageURLValue(configKey.VhmLogoImageURL);
            }
        }
        public static string SiteBackgroundURL
        {
            get
            {
                return GetValue(configKey.SiteBackground).ToString();// GetCDNImageURLValue(configKey.SiteBackgroundURL);
            }
        }
        public static string SSOintLoginFalureUrl
        {
            get
            {
                return GetURLValue(configKey.SSOintLoginFalureUrl);
            }
        }
        public static string SSOintEmployyeNotFoundUrl
        {
            get
            {
                return GetURLValue(configKey.SSOintEmployyeNotFoundUrl);
            }
        }
        public static string SSOintSessionlogOutUrl
        {
            get
            {
                return GetURLValue(configKey.SSOintSessionlogOutUrl);
            }
        }
        public static string SSOintLandingUrl
        {
            get
            {
                return GetURLValue(configKey.SSOintLandingUrl);
            }
        }
        public static string SSOintEnrollemntStartUrl
        {
            get
            {
                return GetURLValue(configKey.SSOintEnrollemntStartUrl);
            }
        }
        public static string SSOintSponsorID
        {
            get
            {
                return Convert.ToString(GetValue(configKey.SSOintSponsorID));
            }
        }
        public static string SSOintLoginRedirectUrl
        {
            get
            {
                return Convert.ToString(GetValue(configKey.SSOintLoginRedirectUrl));
            }
        }
        public static string SSOintisyncStarturl
        {
            get
            {
                return GetURLValue(configKey.SSOintisyncStarturl);
            }
        }
        public static string SSOintCanLoginFalureUrl
        {
            get
            {
                return GetURLValue(configKey.SSOintCanLoginFalureUrl);
            }
        }
        public static string SSOintCanEmployyeNotFoundUrl
        {
            get
            {
                return GetURLValue(configKey.SSOintCanEmployyeNotFoundUrl);
            }
        }
        public static string SSOintCanSessionlogOutUrl
        {
            get
            {
                return GetURLValue(configKey.SSOintCanSessionlogOutUrl);
            }
        }
        public static string SSOintCanLandingUrl
        {
            get
            {
                return GetURLValue(configKey.SSOintCanLandingUrl);
            }
        }
        public static string SSOintCanEnrollemntStartUrl
        {
            get
            {
                return GetURLValue(configKey.SSOintCanEnrollemntStartUrl);
            }
        }
        public static string SSOintCanSponsorID
        {
            get
            {
                return Convert.ToString(GetValue(configKey.SSOintCanSponsorID));
            }
        }
        public static string SSOintCanLoginRedirectUrl
        {
            get
            {
                return Convert.ToString(GetValue(configKey.SSOintCanLoginRedirectUrl));
            }
        }
        public static string SSOintCanisyncStarturl
        {
            get
            {
                return GetURLValue(configKey.SSOintCanisyncStarturl);
            }
        }
        public static string SSOlmcLoginFalureUrl
        {
            get
            {
                return GetURLValue(configKey.SSOlmcLoginFalureUrl);
            }
        }
        public static string SSOlmcEmployyeNotFoundUrl
        {
            get
            {
                return GetURLValue(configKey.SSOlmcEmployyeNotFoundUrl);
            }
        }
        public static string SSOlmcSessionlogOutUrl
        {
            get
            {
                return GetURLValue(configKey.SSOlmcSessionlogOutUrl);
            }
        }
        public static string SSOlmcLandingUrl
        {
            get
            {
                return GetURLValue(configKey.SSOlmcLandingUrl);
            }
        }
        public static string SSOlmcEnrollemntStartUrl
        {
            get
            {
                return GetURLValue(configKey.SSOlmcEnrollemntStartUrl);
            }
        }
        public static string SSOlmcSponsorID
        {
            get
            {
                return Convert.ToString(GetValue(configKey.SSOlmcSponsorID));
            }
        }
        public static string SSOlmcLoginRedirectUrl
        {
            get
            {
                return Convert.ToString(GetValue(configKey.SSOlmcLoginRedirectUrl));
            }
        }
        public static string SSOlmcisyncStarturl
        {
            get
            {
                return GetURLValue(configKey.SSOlmcisyncStarturl);
            }
        }
		//-> CGW New method for LMC Direct SSO
		public static string SSOLMCDirectLoginFalureUrl
		{
			get
			{
				return GetURLValue(configKey.SSOLMCDirectLoginFalureUrl);
			}
		}
		public static string SSOLMCDirectEmployyeNotFoundUrl
		{
			get
			{
				return GetURLValue(configKey.SSOLMCDirectEmployyeNotFoundUrl);
			}
		}
		public static string SSOLMCDirectSessionlogOutUrl
		{
			get
			{
				return GetURLValue(configKey.SSOLMCDirectSessionlogOutUrl);
			}
		}
		public static string SSOLMCDirectLandingUrl
		{
			get
			{
				return GetURLValue(configKey.SSOLMCDirectLandingUrl);
			}
		}
		public static string SSOLMCDirectEnrollemntStartUrl
		{
			get
			{
				return GetURLValue(configKey.SSOLMCDirectEnrollemntStartUrl);
			}
		}
		public static string SSOLMCDirectSponsorID
		{
			get
			{
				return Convert.ToString(GetValue(configKey.SSOLMCDirectSponsorID));
			}
		}
		public static string SSOLMCDirectLoginRedirectUrl
		{
			get
			{
				return Convert.ToString(GetValue(configKey.SSOLMCDirectLoginRedirectUrl));
			}
		}
		public static string SSOLMCDirectisyncStarturl
		{
			get
			{
				return GetURLValue(configKey.SSOLMCDirectisyncStarturl);
			}
		}
		// End LMC Direct Method
        public static string VlifeSponsorIDs
        {
            get
            {
                return Convert.ToString(GetValue(configKey.VlifeSponsorIDs));
            }
        }
        public static string vlifeHWCSponsorIDs
        {
            get
            {
                return Convert.ToString(GetValue(configKey.vlifeHWCSponsorIDs));
            }
        }
        public static string vlifeSAPPponsorIDs
        {
            get
            {
                return Convert.ToString(GetValue(configKey.vlifeSAPPponsorIDs));
            }
        }
        public static string VlifeServiceHWCKeyword
        {
            get
            {
                return Convert.ToString(GetValue(configKey.VlifeServiceHWCKeyword));
            }
        }
        public static string VlifeServiceoHWCrgunitid
        {
            get
            {
                return Convert.ToString(GetValue(configKey.VlifeServiceoHWCrgunitid));
            }
        }
        public static string VlifeServiceAPPKeyword
        {
            get
            {
                return Convert.ToString(GetValue(configKey.VlifeServiceAPPKeyword));
            }
        }
        public static string VlifeServiceAPPgunitid
        {
            get
            {
                return Convert.ToString(GetValue(configKey.VlifeServiceAPPgunitid));
            }
        }
        public static string VlifeServiceVHMclientid
        {
            get
            {
                return Convert.ToString(GetValue(configKey.VlifeServiceVHMclientid));
            }
        }
        public static string VlifeServiceProviderID
        {
            get
            {
                return Convert.ToString(GetValue(configKey.VlifeServiceProviderID));
            }
        }
        public static string VlifeServiceURL
        {
            get
            {
                return Convert.ToString(GetValue(configKey.VlifeServiceURL));
            }
        }
        public static string KAGEditThreshold_Weekly
        {
            get
            {
                return Convert.ToString(GetValue(configKey.KAGEditThreshold_Weekly));
            }
        }
        public static bool KAGShareInNewsfeedDefault
        {
            get { return Convert.ToBoolean(GetValue(configKey.KAGShareInNewsfeedDefault)); } 
        }
        public static string KAGEditThreshold_Daily
        {
            get
            {
                return Convert.ToString(GetValue(configKey.KAGEditThreshold_Daily));
            }
        }
        public static int PersonalTrackingDaysToDisplay
        {
            get
            {
                return Convert.ToInt32(GetValue(configKey.PersonalTrackingDaysToDisplay));
            }  
        }
        public static int PersonalTrackingFriendsPerPage
        {
            get
            {
                return Convert.ToInt32(GetValue(configKey.PersonalTrackingFriendsPerPage));
            }  
        }       
        public static string ShowDevicesFitbitSection
        {
            get
            {
                return Convert.ToString(GetValue(configKey.ShowDevicesFitbitSection));
            }
        }
        public static string SponsorIdsWithFitBitConnection
        {
            get
            {
                return Convert.ToString(GetValue(configKey.SponsorIdsWithFitBitConnection));
            }
        }
        public static int FriendCountThreshold
        {
            get
            {
                return Convert.ToInt32(GetValue(configKey.FriendCountThreshold));
            }
        }
        public static string IntelliResponseURL
        {
            get
            {
                return GetURLValue(configKey.IntelliResponseURL);
            }
        }
        public static string OnlineReportingURL
        {
            get
            {
                return GetURLValue(configKey.OnlineReportingURL);
            }
        }
        public static string MarketingSharepoint
        {
            get
            {
                return Convert.ToString(GetValue(configKey.MarketingSharepoint));
            }
        }
        public static string MarketingURL
        {
            get
            {
                return GetURLValue(configKey.MarketingURL);
            }
        }
        public static string BuildBadgeImageUrl
        {
            get
            {
                return GetURLValue(configKey.BuildBadgeImageUrl);
            }
        }
        public static string ConsumerKey
        {
            get
            {
                return Convert.ToString(GetValue(configKey.ConsumerKey));
            }
        }
        public static string ConsumerSecret
        {
            get
            {
                return Convert.ToString(GetValue(configKey.ConsumerSecret));
            }
        }
        public static string IntelliresponseSpanishID
        {
            get
            {
                return Convert.ToString(GetValue(configKey.IntelliresponseSpanishID));
            }
        }
        public static string MemberDeviceSettingsPath
        {
            get
            {
                return Convert.ToString(GetValue(configKey.MemberDeviceSettingsPath));
            }
        }
        public static string SettingsApiUri
        {
            get
            {
                return Convert.ToString(GetValue(configKey.SettingsApiUri));
            }
        }
        public static string VlifeServiceAeraGunitId
        {
            get
            {
                return Convert.ToString(GetValue(configKey.VlifeServiceAeraGunitid));
            }
        }
        public static string VlifeServiceAeraKeyword
        {
            get
            {
                return Convert.ToString(GetValue(configKey.VlifeServiceAeraKeyword));
            }
        }
        public static string VlifeAeraSponsorIDs
        {
            get
            {
                return Convert.ToString(GetValue(configKey.vlifeAeraSponsorIDs));
            }
        }
        public static string MTHomeDeviceEndpointName
        {
            get
            {
                return Convert.ToString(GetValue(configKey.MTHomeDeviceEndpointName));
            }
        }
        public static string LearnMoreHowThisWork
        {
            get
            {
                return Convert.ToString(GetValue(configKey.LearnMoreHowThisWork));
            }
        }
        public static string ShowDevicesEndomondoSection
        {
            get
            {
                return Convert.ToString(GetValue(configKey.ShowDevicesEndomondoSection));
            }
        }
        public static string EndomondoConsumerKey
        {
            get
            {
                return Convert.ToString(GetValue(configKey.EndomondoConsumerKey));
            }
        }
        public static string EndomondoConsumerSecret
        {
            get
            {
                return Convert.ToString(GetValue(configKey.EndomondoConsumerSecret));
            }
        }
        public static string SponsorIdsWithEndomondoConnection
        {
            get
            {
                return Convert.ToString(GetValue(configKey.SponsorIdsWithEndomondoConnection));
            }
        }
        public static string ApiActivityUrl
        {
            get
            {
                return Convert.ToString(GetValue(configKey.ApiActivityUrl));
            }
        }
        public static string ApiIsConnectedToVendor
        {
            get
            {
                return Convert.ToString(GetValue(configKey.ApiIsConnectedToVendor));
            }
        }
        public static string ShowDevicesMisfitSection
        {
            get
            {
                return Convert.ToString(GetValue(configKey.ShowDevicesMisfitSection));
            }
        }
        public static string SponsorIdsWithMisfitConnection
        {
            get
            {
                return Convert.ToString(GetValue(configKey.SponsorIdsWithMisfitConnection));
            }
        }
        public static string HowToConnectEndomondo
        {
            get
            {
                return Convert.ToString(GetValue(configKey.HowToConnectEndomondo));
            }
        }
        public static string HowToConnectMisfit
        {
            get
            {
                return Convert.ToString(GetValue(configKey.HowToConnectMisfit));
            }
        }
        public static string MisfitConsumerKey
        {
            get
            {
                return Convert.ToString(GetValue(configKey.MisfitConsumerKey));
            }
        }
        public static string MisfitConsumerSecret
        {
            get
            {
                return Convert.ToString(GetValue(configKey.MisfitConsumerSecret));
            }
        }

        public static string RecentPasswordsCount
        {
            get
            {
                return Convert.ToString(GetValue(configKey.RecentPasswordsCount));
            }
        }
        #endregion

        #region Private Methods

        private static object GetValue(configKey key)
        {
            var keyString = Convert.ToString(key);
            var curr = HttpContext.Current;
            if (curr != null && curr.Application["ApplicationSetting"] != null && curr.Application["ApplicationSetting"] is Dictionary<string, object> && ((Dictionary<string, object>)curr.Application["ApplicationSetting"]).ContainsKey(keyString))
            {
                var appSettings = HttpContext.Current.Application["ApplicationSetting"];
                return ((Dictionary<string, object>)appSettings)[keyString];
            }

            return ConfigurationManager.AppSettings[keyString];
        }
        private static string GetURLValue(configKey key)
        {
            var keyString = Convert.ToString(key);
            string url;


            var curr = HttpContext.Current;
            if (curr != null && curr.Application["ApplicationSetting"] != null && curr.Application["ApplicationSetting"] is Dictionary<string, object> && ((Dictionary<string, object>)curr.Application["ApplicationSetting"]).ContainsKey(keyString))
            {
                var appSettings = HttpContext.Current.Application["ApplicationSetting"];
                url = ((Dictionary<string, object>)appSettings)[keyString].ToString();
            }
            else
            {
                url = ConfigurationManager.AppSettings[keyString];
            }
            return url.StartsWith("http") ? url : BaseURL + url;


        }

        private static string GetCDNImageURLValue(configKey key)
        {
            var appSettings = System.Web.HttpContext.Current.Application["ApplicationSetting"] as Dictionary<string, object>;
            var keyString = Convert.ToString(key);
            string url;
            if (appSettings != null && appSettings.ContainsKey(keyString))
            {
                url = appSettings[keyString].ToString();
            }
            else
            {
                url = ConfigurationManager.AppSettings[keyString];
            }
            return url.StartsWith("http") ? url : CDNBaseURL + url;
        }
        #endregion

        #region Config Keys enumeration

        enum configKey
        {
            ActivityDaysToShow,
            ActivityGraphHeight,
            APIURL,
            AuthURL,
            BadgeImageUrl,
            BaseURL,
            BuildBadgeImageUrl,
            BuyGoZoneURL,
            BuyPolarURL,
            CachedResourceVersion,
            CDNBaseURL,
            ChallengeEndedDisplayDays,
            CheckMemberPage,
            ClientID,
            ClientSecret,
            ClientValidationEnabled,
            CompanyName,
            DBSessionTimeCheckInMinutes,
            DefaultGroupPicGuid,
            DefaultLanguage,
            DefaultMemberPicGuid,
            DeleteURL,
            DownloadGZURL,
            GroupImageUrl,
            GZSupportURL,
            HowToConnectFitBit,
            HowToConnectRK,
            IntelliResponseURL,
            LearnMoreAboutPolar,
            ManageYourAccountURL,
            MarketingSharepoint,
            MarketingURL,
            MaxRecentlyEarnedRewardsSummaryToShow,
            MaxRecentlyEarnedRewardsToShow,
            MemberImageUrl,
            MiniNewsfeedCount,
            MiniNewsfeedDaysToInclude,
            ModuleImageUrl,
            MTEndpointName,
            MTActivityEndpointName,
            NewsfeedUrl,
            OnlineReportingURL,
            PartnersPageSponsorModulURL,
            PartnersPageURL,
            ProgramName,
            PreserveLoginUrl,
            RegisterYourGoZone,
            RegisterYourMax,
            ReturnURL,
            RewardTypeImageUrl,
            SECPageUrl,
            ShowException,
            ShowGroupURL,
            SiteBackground,
            SiteBackgroundURL,
            SiteImageUrl,
            SiteUrlBase,
            StepsTargetDefault,
            SSOintSponsorID,
            SSOintEnrollemntStartUrl,
            SSOintLandingUrl,
            SSOintSessionlogOutUrl,
            SSOintEmployyeNotFoundUrl,
            SSOintLoginFalureUrl,
            SSOintLoginRedirectUrl,
            SSOintisyncStarturl,
            SSOintCanSponsorID,
            SSOintCanEnrollemntStartUrl,
            SSOintCanLandingUrl,
            SSOintCanSessionlogOutUrl,
            SSOintCanEmployyeNotFoundUrl,
            SSOintCanLoginFalureUrl,
            SSOintCanLoginRedirectUrl,
            SSOintCanisyncStarturl,
            SSOlmcSponsorID,
            SSOlmcEnrollemntStartUrl,
            SSOlmcLandingUrl,
            SSOlmcSessionlogOutUrl,
            SSOlmcEmployyeNotFoundUrl,
            SSOlmcLoginFalureUrl,
            SSOlmcLoginRedirectUrl,
            SSOlmcisyncStarturl,
            SSOLMCDirectSponsorID, //-Used for LMC Direct ->
            SSOLMCDirectEnrollemntStartUrl,
            SSOLMCDirectLandingUrl,
            SSOLMCDirectSessionlogOutUrl,
            SSOLMCDirectEmployyeNotFoundUrl,
            SSOLMCDirectLoginFalureUrl,
            SSOLMCDirectLoginRedirectUrl,
            SSOLMCDirectisyncStarturl,  //-Used for LMC Direct *//
            SyncSoftwareURL,
            TimeoutInterval,
            TokenURL,
            UnobtrusiveJavaScriptEnabled,
            UploadFileErrorURL,
            VhmLogoImageURL,
            WebSiteCDNImages,
            ConsumerKey,
            ConsumerSecret,
            VlifeSponsorIDs,
            VlifeServiceProviderID,
            VlifeServiceURL,
            vlifeHWCSponsorIDs,
            vlifeAeraSponsorIDs,
            vlifeSAPPponsorIDs,
            VlifeServiceHWCKeyword,
            VlifeServiceoHWCrgunitid,
            VlifeServiceAPPKeyword,
            VlifeServiceAPPgunitid,
            VlifeServiceVHMclientid,
            KAGEditThreshold_Weekly,
            KAGEditThreshold_Daily,
            KAGShareInNewsfeedDefault,
            ShowDevicesFitbitSection,
            SponsorIdsWithFitBitConnection,
            ChallengeChatMessagesToDisplay,
            IntelliresponseSpanishID,
            ChallengeLeaderBoardPageSize,
            FriendCountThreshold,
            MemberDeviceSettingsPath,
            SettingsApiUri,
            PersonalTrackingDaysToDisplay,
            PersonalTrackingFriendsPerPage,
            ConnectThroughRunkeeperPairingPdf,
            VlifeServiceAeraGunitid,
            VlifeServiceAeraKeyword,
            MTHomeDeviceEndpointName,
            LearnMoreHowThisWork,
            ExpressProgramMenuDescription,
            ShowDevicesEndomondoSection,
            EndomondoConsumerKey,
            EndomondoConsumerSecret,
            SponsorIdsWithEndomondoConnection,
            ApiActivityUrl,
            ApiIsConnectedToVendor,
            ShowDevicesMisfitSection,
            SponsorIdsWithMisfitConnection,
            HowToConnectMisfit,
            HowToConnectEndomondo,
            MisfitConsumerSecret,
            MisfitConsumerKey,
            RecentPasswordsCount
        }

        #endregion        
    }
}
