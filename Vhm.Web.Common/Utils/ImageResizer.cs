﻿using System;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using System.Web;

namespace Vhm.Web.Common.Utils
{
        public static class ImageResizer
        {
                private const int MaxImageLength = 150;

                #region Helper Functions

                /// <summary>
                /// Creates the thumbnail.
                /// </summary>
                /// <param name="passedImage">The passed image.</param>
                /// <param name="imageSize">Size of the image.</param>
                /// <returns></returns>
                public static byte[] CreateThumbnail(byte[] passedImage, int imageSize = MaxImageLength)
                {
                        byte[] returnedThumbnail = new byte[] { };

                        if (passedImage != null && passedImage.Length > 0)
                        {
                                using (MemoryStream startMemoryStream = new MemoryStream(),
                                                    newMemoryStream = new MemoryStream())
                                {
                                        // write the string to the stream  
                                        startMemoryStream.Write(passedImage, 0, passedImage.Length);

                                        // create the start Bitmap from the MemoryStream that contains the image  
                                        Bitmap startBitmap = new Bitmap(startMemoryStream);

                                        // set thumbnail height and width proportional to the original image.  
                                        int newHeight = 0;
                                        int newWidth = 0;
                                        double hwRatio;

                                        if (startBitmap.Height > startBitmap.Width)
                                        {
                                                hwRatio = (double)startBitmap.Height / imageSize;
                                        }
                                        else
                                        {
                                                hwRatio = (double)startBitmap.Width / imageSize;
                                        }

                                        newHeight = (int)(startBitmap.Height / hwRatio);
                                        newWidth = (int)(startBitmap.Width / hwRatio);

                                        // create a new Bitmap with dimensions for the thumbnail.  

                                        // Copy the image from the START Bitmap into the NEW Bitmap.  
                                        // This will create a thumnail size of the same image.
                                        //so far we only save png for badges and jpg otherwise.  
                                        Bitmap newBitmap = ResizeImage(startBitmap, newWidth, newHeight);
                                        if (startBitmap.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Png))
                                        {
                                            // Save this image to the specified stream in the specified format.  
                                            newBitmap.Save(newMemoryStream, System.Drawing.Imaging.ImageFormat.Png);
                                        }
                                        //else if (startBitmap.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Gif))
                                        //{
                                        //    // Save this image to the specified stream in the specified format.  
                                        //    newBitmap.Save(newMemoryStream, System.Drawing.Imaging.ImageFormat.Gif);
                                        //}
                                        else
                                        {
                                            // Save this image to the specified stream in the specified format.  
                                            newBitmap.Save(newMemoryStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                                        }
                                        // Save this image to the specified stream in the specified format.  
                                        //newBitmap.Save(newMemoryStream, System.Drawing.Imaging.ImageFormat.Jpeg);

                                        // Fill the byte[] for the thumbnail from the new MemoryStream.  
                                        returnedThumbnail = newMemoryStream.ToArray();
                                }
                        }

                        // return the resized image as a string of bytes.  
                        return returnedThumbnail;
                }

                /// <summary>
                /// Creates the thumbnail. - custom for Sponsor logo in the CRM Admin console
                /// </summary>
                /// <param name="passedImage">The passed image.</param>
                /// <param name="maxImageSizeWidth">Max Width size of the image.</param>
                /// /// <param name="maxImageSizeHeight">Max Height size of the image.</param>
                /// <returns></returns>
                public static byte[] CreateCustomThumbnail(byte[] passedImage, int maxImageSizeWidth, int maxImageSizeHeight)
                {
                    byte[] returnedThumbnail = new byte[] { };

                    if (passedImage != null && passedImage.Length > 0)
                    {
                        using (MemoryStream startMemoryStream = new MemoryStream(),
                                            newMemoryStream = new MemoryStream())
                        {
                            // write the string to the stream  
                            startMemoryStream.Write(passedImage, 0, passedImage.Length);

                            // create the start Bitmap from the MemoryStream that contains the image  
                            Bitmap startBitmap = new Bitmap(startMemoryStream);

                            // set thumbnail height and width proportional to the original image.  
                            int newHeight = maxImageSizeHeight;
                            int newWidth = maxImageSizeWidth;
                            var imageWidth = startBitmap.Width;
                            var imageHeight = startBitmap.Height;
                            //resize ONLY if imageWidth > maxImageSizeWidth and/or imageHeight > maxImageSizeHeight
                            if (imageWidth > maxImageSizeWidth || imageHeight > maxImageSizeHeight)
                            {
                                var dWidth = (double) imageWidth;
                                var dHeight = (double) imageHeight;
                                double hwRatio = dWidth/dHeight;

                                if (imageWidth > maxImageSizeWidth && imageHeight > maxImageSizeHeight)
                                {
                                    newWidth = maxImageSizeWidth;
                                    newHeight = (int)(newWidth / hwRatio);
                                    //for images with dimensions > 250x80 we need to make sure both newWidth and newHeight are less than the maximum . can happen that the ratio is small so the above newHeight = (int)(imageWidth / hwRatio) can be greater still than the maxImageSizeHeight
                                    if (newHeight > maxImageSizeHeight)
                                    {
                                        newHeight = maxImageSizeHeight;
                                        newWidth = (int) (newHeight*hwRatio);
                                    }
                                }
                                else
                                {
                                    if (imageWidth > maxImageSizeWidth)
                                    {
                                        newWidth = maxImageSizeWidth;
                                        newHeight = (int)(imageWidth / hwRatio);
                                    }
                                    if (imageHeight > maxImageSizeHeight)
                                    {
                                        //hwRatio = imageWidth/imageHeight;
                                        newHeight = maxImageSizeHeight;
                                        newWidth = (int)(newHeight * hwRatio);
                                    }
                                }
                                
                            }
                            else
                            {
                                newHeight = imageHeight;
                                newWidth = imageWidth;
                            }
                            // create a new Bitmap with dimensions for the thumbnail.  
                           
                            startBitmap.MakeTransparent(Color.Transparent);
                            
                            // Change a color to be transparent 
                            
                            Image newImage = (Image)startBitmap;

                            newImage = CustomResize(newImage, newWidth, newHeight);

                            newImage.Save(newMemoryStream, ImageFormat.Png);

                            
                            // Fill the byte[] for the thumbnail from the new MemoryStream.  
                            returnedThumbnail = newMemoryStream.ToArray();
                        }
                    }

                    // return the resized image as a string of bytes.  
                    return returnedThumbnail;
                }


            /// <summary>
            /// Resizes the image.
            /// </summary>
            /// <param name="image">The image.</param>
            /// <param name="width">The width.</param>
            /// <param name="height">The height.</param>
            /// <returns></returns>
            private static Bitmap ResizeImage(Bitmap image, int width, int height)
            {
                Bitmap resizedImage = new Bitmap(width, height);
                using (Graphics gfx = Graphics.FromImage(resizedImage))
                {

                    //gfx.Clear(Color.Transparent);
                    gfx.DrawImage(image, new Rectangle(0, 0, width, height),
                        new Rectangle(0, 0, image.Width, image.Height), GraphicsUnit.Pixel);
                }

                Color color = image.GetPixel(0, 0);
                resizedImage = MakeTransparent(image, color) ?? image;

                return resizedImage;
            }

            /// <summary>  
                /// Returns a transparent background GIF image from the specified Bitmap.  
                /// </summary>  
                /// <param name="bitmap">The Bitmap to make transparent.</param>  
                /// <param name="color">The Color to make transparent.</param>  
                /// <returns>New Bitmap containing a transparent background gif.</returns>  
                public static Bitmap MakeTransparent(Bitmap bitmap, Color color)
                {
                    byte R = color.R;
                    byte G = color.G;
                    byte B = color.B;
                    MemoryStream fin = new MemoryStream();
                    bitmap.Save(fin, System.Drawing.Imaging.ImageFormat.Png);
                    MemoryStream fout = new MemoryStream((int)fin.Length);
                    int count = 0;
                    byte[] buf = new byte[256];
                    byte transparentIdx = 0;
                    fin.Seek(0, SeekOrigin.Begin);
                    //header  
                    count = fin.Read(buf, 0, 13);
                    if ((buf[0] != 71) || (buf[1] != 73) || (buf[2] != 70)) return null; //GIF  
                    fout.Write(buf, 0, 13);
                    int i = 0;
                    if ((buf[10] & 0x80) > 0)
                    {
                        i = 1 << ((buf[10] & 7) + 1) == 256 ? 256 : 0;
                    }
                    for (; i != 0; i--)
                    {
                        fin.Read(buf, 0, 3);
                        if ((buf[0] == R) && (buf[1] == G) && (buf[2] == B))
                        {
                            transparentIdx = (byte)(256 - i);
                        }
                        fout.Write(buf, 0, 3);
                    }
                    bool gcePresent = false;
                    while (true)
                    {
                        fin.Read(buf, 0, 1);
                        fout.Write(buf, 0, 1);
                        if (buf[0] != 0x21) break;
                        fin.Read(buf, 0, 1);
                        fout.Write(buf, 0, 1);
                        gcePresent = (buf[0] == 0xf9);
                        while (true)
                        {
                            fin.Read(buf, 0, 1);
                            fout.Write(buf, 0, 1);
                            if (buf[0] == 0) break;
                            count = buf[0];
                            if (fin.Read(buf, 0, count) != count) return null;
                            if (gcePresent)
                            {
                                if (count == 4)
                                {
                                    buf[0] |= 0x01;
                                    buf[3] = transparentIdx;
                                }
                            }
                            fout.Write(buf, 0, count);
                        }
                    }
                    while (count > 0)
                    {
                        count = fin.Read(buf, 0, 1);
                        fout.Write(buf, 0, 1);
                    }
                    fin.Close();
                    fout.Flush();
                    return new Bitmap(fout);
                }

                #endregion

                private static Image CustomResize(Image imgToResize, int width, int height)
                {
                    int sourceWidth = imgToResize.Width;
                    int sourceHeight = imgToResize.Height;

                    float nPercent = 0;
                    float nPercentW = 0;
                    float nPercentH = 0;

                    nPercentW = ((float)width / (float)sourceWidth);
                    nPercentH = ((float)height / (float)sourceHeight);

                    if (nPercentH < nPercentW)
                        nPercent = nPercentH;
                    else
                        nPercent = nPercentW;

                    int destWidth = (int)(sourceWidth * nPercent);
                    int destHeight = (int)(sourceHeight * nPercent);

                    Bitmap b = new Bitmap(destWidth, destHeight);
                    Graphics g = Graphics.FromImage((Image)b);
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;

                    g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
                    g.Dispose();

                    return (Image)b;
                }
        }

   
}
