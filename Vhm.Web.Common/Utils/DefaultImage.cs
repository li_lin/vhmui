﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Vhm.Web.Common.Utils
{
    public static class DefaultImage
    {
        #region Helper functions
        /// <summary>
        /// Creates the default image to be returned for a profile (member's or group's).
        /// </summary>
        /// <param name="defaultImagePath">The path to the default image to be shown.</param>
        /// <returns></returns>
        public static byte[] GetDefaultImage(string defaultImagePath)
        {
            var fs = new FileStream(defaultImagePath, FileMode.Open, FileAccess.Read);
            var br = new BinaryReader(fs);
            byte[] image = br.ReadBytes((int)fs.Length);
            br.Close();
            fs.Close();
            return image;
        }

        /// <summary>
        /// Builds the member profile picture path.
        /// </summary>
        /// <param name="dbGuid">The database unique identifier.</param>
        /// <returns></returns>
        public static string BuildMemberProfilePicturePath(byte[] dbGuid)
        {
            string result;
            if (dbGuid != null && dbGuid.Length > 1)
            {
                result = string.Format("{0}{1}.jpg",
                                       ApplicationSettings.MemberImageUrl,
                                        new Guid(dbGuid));
            }
            else
            {
                result = string.Format("{0}{1}.gif",
                                        ApplicationSettings.MemberImageUrl,
                                        ApplicationSettings.DefaultMemberPicGuid);
            }
            return result;
        }
        #endregion
    }
}
