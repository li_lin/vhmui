﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by coded UI test builder.
//      Version: 10.0.0.0
//
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------

namespace CodedUITests
{
    using System;
    using System.CodeDom.Compiler;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text.RegularExpressions;
    using System.Windows.Input;
    using Microsoft.VisualStudio.TestTools.UITest.Extension;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
    using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
    using Mouse = Microsoft.VisualStudio.TestTools.UITesting.Mouse;
    using MouseButtons = System.Windows.Forms.MouseButtons;
    
    
    [GeneratedCode("Coded UITest Builder", "10.0.40219.457")]
    public partial class UIMap
    {
        
        /// <summary>
        /// ChallengeManagerSelectSponsor - Use 'ChallengeManagerSelectSponsorParams' to pass parameters into this method.
        /// </summary>
        public void ChallengeManagerSelectSponsor()
        {
            #region Variable Declarations
            WinButton uINewtabCtrlTButton = this.UITransactionsProductiWindow.UITabRowTabList.UINewtabCtrlTButton;
            WinEdit uIAddressandsearchusinEdit = this.UITransactionsProductiWindow.UIAddressBarClient.UIAddressandsearchusinEdit;
            WinEdit uIAddressandsearchusinEdit1 = this.UITransactionsProductiWindow.UIAddressBarClient.UIAddressandsearchusinEdit1;
            HtmlDocument uIAbouttabsDocument = this.UITransactionsProductiWindow.UIAbouttabsDocument;
            BrowserWindow uITransactionsProductiWindow = this.UITransactionsProductiWindow;
            HtmlEdit uIMemberIDorEmailAddreEdit = this.UITransactionsProductiWindow.UIVirginPulseDocument.UIMemberIDorEmailAddreEdit;
            HtmlEdit uIPasswordEdit = this.UITransactionsProductiWindow.UIVirginPulseDocument.UIPasswordEdit;
            HtmlInputButton uILoginButton = this.UITransactionsProductiWindow.UIVirginPulseDocument.UILoginButton;
            HtmlHyperlink uIContinuetothiswebsitHyperlink = this.UITransactionsProductiWindow.UICertificateErrorNaviDocument.UIContinuetothiswebsitHyperlink;
            HtmlHyperlink uIChallengeConsoleHyperlink = this.UITransactionsProductiWindow.UILandingPageVirginPulDocument.UIChallengeConsoleHyperlink;
            HtmlEdit uISponsorIDEdit = this.UITransactionsProductiWindow.UIChallengeManagerDocument.UISponsorIDEdit;
            HtmlButton uISearchButton = this.UITransactionsProductiWindow.UIChallengeManagerDocument.UIWrapperPane.UISearchButton;
            #endregion

            // Go to web page 'https://rpm.newrelic.com/accounts/310548/applications/2087720/transactions#id=252602847&app_trace_id=2105839538&type=browser' using new browser instance
            BrowserWindow rpmnewreliccomBrowser = BrowserWindow.Launch(new System.Uri(this.ChallengeManagerSelectSponsorParams.Url));

            // Click 'New tab (Ctrl+T)' button
            Mouse.Click(uINewtabCtrlTButton, new Point(15, 14));

            // Type 'local.virginhealthmiles.com/login.aspx' in 'Address and search using Bing' text box
            uIAddressandsearchusinEdit.Text = this.ChallengeManagerSelectSponsorParams.UIAddressandsearchusinEditText;

            // Click 'Address and search using Bing' text box
            Mouse.Click(uIAddressandsearchusinEdit1, new Point(224, 4));

            // Type '{Enter}' in 'about:tabs' document
            Keyboard.SendKeys(uIAbouttabsDocument, this.ChallengeManagerSelectSponsorParams.UIAbouttabsDocumentSendKeys, ModifierKeys.None);

            // Go to web page 'http://local.virginhealthmiles.com/login.aspx'
            uITransactionsProductiWindow.NavigateToUrl(new System.Uri(this.ChallengeManagerSelectSponsorParams.UITransactionsProductiWindowUrl));

            // Type '0612690001' in 'Member ID or Email Address' text box
            uIMemberIDorEmailAddreEdit.Text = this.ChallengeManagerSelectSponsorParams.UIMemberIDorEmailAddreEditText;

            // Type '********' in 'Password' text box
            uIPasswordEdit.Password = this.ChallengeManagerSelectSponsorParams.UIPasswordEditPassword;

            // Click 'Login' button
            Mouse.Click(uILoginButton, new Point(42, 7));

            // Click 'Continue to this website (not recommended).' link
            Mouse.Click(uIContinuetothiswebsitHyperlink, new Point(165, 8));

            // Click 'Ok' button in the browser dialog window
            uITransactionsProductiWindow.PerformDialogAction(BrowserDialogAction.Ok);

            // Click 'Challenge Console' link
            Mouse.Click(uIChallengeConsoleHyperlink, new Point(84, 15));

            // Type '1' in 'Sponsor ID' text box
            uISponsorIDEdit.Text = this.ChallengeManagerSelectSponsorParams.UISponsorIDEditText;

            // Click 'Search' button
            Mouse.Click(uISearchButton, new Point(57, 17));
        }
        
        #region Properties
        public virtual ChallengeManagerSelectSponsorParams ChallengeManagerSelectSponsorParams
        {
            get
            {
                if ((this.mChallengeManagerSelectSponsorParams == null))
                {
                    this.mChallengeManagerSelectSponsorParams = new ChallengeManagerSelectSponsorParams();
                }
                return this.mChallengeManagerSelectSponsorParams;
            }
        }
        
        public UITransactionsProductiWindow UITransactionsProductiWindow
        {
            get
            {
                if ((this.mUITransactionsProductiWindow == null))
                {
                    this.mUITransactionsProductiWindow = new UITransactionsProductiWindow();
                }
                return this.mUITransactionsProductiWindow;
            }
        }
        #endregion
        
        #region Fields
        private ChallengeManagerSelectSponsorParams mChallengeManagerSelectSponsorParams;
        
        private UITransactionsProductiWindow mUITransactionsProductiWindow;
        #endregion
    }
    
    /// <summary>
    /// Parameters to be passed into 'ChallengeManagerSelectSponsor'
    /// </summary>
    [GeneratedCode("Coded UITest Builder", "10.0.40219.457")]
    public class ChallengeManagerSelectSponsorParams
    {
        
        #region Fields
        /// <summary>
        /// Go to web page 'https://rpm.newrelic.com/accounts/310548/applications/2087720/transactions#id=252602847&app_trace_id=2105839538&type=browser' using new browser instance
        /// </summary>
        public string Url = "https://rpm.newrelic.com/accounts/310548/applications/2087720/transactions#id=252" +
            "602847&app_trace_id=2105839538&type=browser";
        
        /// <summary>
        /// Type 'local.virginhealthmiles.com/login.aspx' in 'Address and search using Bing' text box
        /// </summary>
        public string UIAddressandsearchusinEditText = "local.virginhealthmiles.com/login.aspx";
        
        /// <summary>
        /// Type '{Enter}' in 'about:tabs' document
        /// </summary>
        public string UIAbouttabsDocumentSendKeys = "{Enter}";
        
        /// <summary>
        /// Go to web page 'http://local.virginhealthmiles.com/login.aspx'
        /// </summary>
        public string UITransactionsProductiWindowUrl = "http://local.virginhealthmiles.com/login.aspx";
        
        /// <summary>
        /// Type '0612690001' in 'Member ID or Email Address' text box
        /// </summary>
        public string UIMemberIDorEmailAddreEditText = "0612690001";
        
        /// <summary>
        /// Type '********' in 'Password' text box
        /// </summary>
        public string UIPasswordEditPassword = "xV9ygbGPYhRuzJS6Ph1FUZq5aw6DS4TT";
        
        /// <summary>
        /// Type '1' in 'Sponsor ID' text box
        /// </summary>
        public string UISponsorIDEditText = "1";
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "10.0.40219.457")]
    public class UITransactionsProductiWindow : BrowserWindow
    {
        
        public UITransactionsProductiWindow()
        {
            #region Search Criteria
            this.SearchProperties[UITestControl.PropertyNames.Name] = "Transactions - Production-web - New Relic - Internet Explorer";
            this.SearchProperties[UITestControl.PropertyNames.ClassName] = "IEFrame";
            this.WindowTitles.Add("Transactions - Production-web - New Relic - Internet Explorer");
            this.WindowTitles.Add("New tab - Internet Explorer");
            this.WindowTitles.Add("Virgin Pulse - Internet Explorer");
            this.WindowTitles.Add("Certificate Error: Navigation Blocked - Internet Explorer");
            this.WindowTitles.Add("Landing Page - Virgin Pulse - Internet Explorer");
            this.WindowTitles.Add("Challenge Manager - Internet Explorer");
            #endregion
        }
        
        public void LaunchUrl(System.Uri url)
        {
            this.CopyFrom(BrowserWindow.Launch(url));
        }
        
        #region Properties
        public UITabRowTabList UITabRowTabList
        {
            get
            {
                if ((this.mUITabRowTabList == null))
                {
                    this.mUITabRowTabList = new UITabRowTabList(this);
                }
                return this.mUITabRowTabList;
            }
        }
        
        public UIAddressBarClient UIAddressBarClient
        {
            get
            {
                if ((this.mUIAddressBarClient == null))
                {
                    this.mUIAddressBarClient = new UIAddressBarClient(this);
                }
                return this.mUIAddressBarClient;
            }
        }
        
        public HtmlDocument UIAbouttabsDocument
        {
            get
            {
                if ((this.mUIAbouttabsDocument == null))
                {
                    this.mUIAbouttabsDocument = new HtmlDocument(this);
                    #region Search Criteria
                    this.mUIAbouttabsDocument.SearchProperties[HtmlDocument.PropertyNames.Id] = null;
                    this.mUIAbouttabsDocument.SearchProperties[HtmlDocument.PropertyNames.RedirectingPage] = "False";
                    this.mUIAbouttabsDocument.SearchProperties[HtmlDocument.PropertyNames.FrameDocument] = "False";
                    this.mUIAbouttabsDocument.FilterProperties[HtmlDocument.PropertyNames.Title] = null;
                    this.mUIAbouttabsDocument.FilterProperties[HtmlDocument.PropertyNames.AbsolutePath] = "tabs";
                    this.mUIAbouttabsDocument.FilterProperties[HtmlDocument.PropertyNames.PageUrl] = "about:tabs";
                    this.mUIAbouttabsDocument.WindowTitles.Add("New tab - Internet Explorer");
                    #endregion
                }
                return this.mUIAbouttabsDocument;
            }
        }
        
        public UIVirginPulseDocument UIVirginPulseDocument
        {
            get
            {
                if ((this.mUIVirginPulseDocument == null))
                {
                    this.mUIVirginPulseDocument = new UIVirginPulseDocument(this);
                }
                return this.mUIVirginPulseDocument;
            }
        }
        
        public UICertificateErrorNaviDocument UICertificateErrorNaviDocument
        {
            get
            {
                if ((this.mUICertificateErrorNaviDocument == null))
                {
                    this.mUICertificateErrorNaviDocument = new UICertificateErrorNaviDocument(this);
                }
                return this.mUICertificateErrorNaviDocument;
            }
        }
        
        public UILandingPageVirginPulDocument UILandingPageVirginPulDocument
        {
            get
            {
                if ((this.mUILandingPageVirginPulDocument == null))
                {
                    this.mUILandingPageVirginPulDocument = new UILandingPageVirginPulDocument(this);
                }
                return this.mUILandingPageVirginPulDocument;
            }
        }
        
        public UIChallengeManagerDocument UIChallengeManagerDocument
        {
            get
            {
                if ((this.mUIChallengeManagerDocument == null))
                {
                    this.mUIChallengeManagerDocument = new UIChallengeManagerDocument(this);
                }
                return this.mUIChallengeManagerDocument;
            }
        }
        #endregion
        
        #region Fields
        private UITabRowTabList mUITabRowTabList;
        
        private UIAddressBarClient mUIAddressBarClient;
        
        private HtmlDocument mUIAbouttabsDocument;
        
        private UIVirginPulseDocument mUIVirginPulseDocument;
        
        private UICertificateErrorNaviDocument mUICertificateErrorNaviDocument;
        
        private UILandingPageVirginPulDocument mUILandingPageVirginPulDocument;
        
        private UIChallengeManagerDocument mUIChallengeManagerDocument;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "10.0.40219.457")]
    public class UITabRowTabList : WinTabList
    {
        
        public UITabRowTabList(UITestControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[WinTabList.PropertyNames.Name] = "Tab Row";
            this.WindowTitles.Add("Transactions - Production-web - New Relic - Internet Explorer");
            #endregion
        }
        
        #region Properties
        public WinButton UINewtabCtrlTButton
        {
            get
            {
                if ((this.mUINewtabCtrlTButton == null))
                {
                    this.mUINewtabCtrlTButton = new WinButton(this);
                    #region Search Criteria
                    this.mUINewtabCtrlTButton.SearchProperties[WinButton.PropertyNames.Name] = "New tab (Ctrl+T)";
                    this.mUINewtabCtrlTButton.WindowTitles.Add("Transactions - Production-web - New Relic - Internet Explorer");
                    #endregion
                }
                return this.mUINewtabCtrlTButton;
            }
        }
        #endregion
        
        #region Fields
        private WinButton mUINewtabCtrlTButton;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "10.0.40219.457")]
    public class UIAddressBarClient : WinClient
    {
        
        public UIAddressBarClient(UITestControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[WinControl.PropertyNames.Name] = "Address Bar";
            this.WindowTitles.Add("New tab - Internet Explorer");
            #endregion
        }
        
        #region Properties
        public WinEdit UIAddressandsearchusinEdit
        {
            get
            {
                if ((this.mUIAddressandsearchusinEdit == null))
                {
                    this.mUIAddressandsearchusinEdit = new WinEdit(this);
                    #region Search Criteria
                    this.mUIAddressandsearchusinEdit.SearchProperties[WinEdit.PropertyNames.Name] = "Address and search using Bing";
                    this.mUIAddressandsearchusinEdit.WindowTitles.Add("New tab - Internet Explorer");
                    #endregion
                }
                return this.mUIAddressandsearchusinEdit;
            }
        }
        
        public WinEdit UIAddressandsearchusinEdit1
        {
            get
            {
                if ((this.mUIAddressandsearchusinEdit1 == null))
                {
                    this.mUIAddressandsearchusinEdit1 = new WinEdit(this);
                    #region Search Criteria
                    this.mUIAddressandsearchusinEdit1.SearchProperties[WinEdit.PropertyNames.Name] = "Address and search using Bing";
                    this.mUIAddressandsearchusinEdit1.WindowTitles.Add("New tab - Internet Explorer");
                    #endregion
                }
                return this.mUIAddressandsearchusinEdit1;
            }
        }
        #endregion
        
        #region Fields
        private WinEdit mUIAddressandsearchusinEdit;
        
        private WinEdit mUIAddressandsearchusinEdit1;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "10.0.40219.457")]
    public class UIVirginPulseDocument : HtmlDocument
    {
        
        public UIVirginPulseDocument(UITestControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[HtmlDocument.PropertyNames.Id] = null;
            this.SearchProperties[HtmlDocument.PropertyNames.RedirectingPage] = "False";
            this.SearchProperties[HtmlDocument.PropertyNames.FrameDocument] = "False";
            this.FilterProperties[HtmlDocument.PropertyNames.Title] = "Virgin Pulse";
            this.FilterProperties[HtmlDocument.PropertyNames.AbsolutePath] = "/login.aspx";
            this.FilterProperties[HtmlDocument.PropertyNames.PageUrl] = "http://local.virginhealthmiles.com/login.aspx";
            this.WindowTitles.Add("Virgin Pulse - Internet Explorer");
            #endregion
        }
        
        #region Properties
        public HtmlEdit UIMemberIDorEmailAddreEdit
        {
            get
            {
                if ((this.mUIMemberIDorEmailAddreEdit == null))
                {
                    this.mUIMemberIDorEmailAddreEdit = new HtmlEdit(this);
                    #region Search Criteria
                    this.mUIMemberIDorEmailAddreEdit.SearchProperties[HtmlEdit.PropertyNames.Id] = "oUserID";
                    this.mUIMemberIDorEmailAddreEdit.SearchProperties[HtmlEdit.PropertyNames.Name] = "_ctl0$MainContentPlaceHolder$oSession$oUserID";
                    this.mUIMemberIDorEmailAddreEdit.FilterProperties[HtmlEdit.PropertyNames.LabeledBy] = null;
                    this.mUIMemberIDorEmailAddreEdit.FilterProperties[HtmlEdit.PropertyNames.Type] = "SINGLELINE";
                    this.mUIMemberIDorEmailAddreEdit.FilterProperties[HtmlEdit.PropertyNames.Title] = "Member ID or Email Address";
                    this.mUIMemberIDorEmailAddreEdit.FilterProperties[HtmlEdit.PropertyNames.Class] = "watermark signin login-input-rounded";
                    this.mUIMemberIDorEmailAddreEdit.FilterProperties[HtmlEdit.PropertyNames.ControlDefinition] = "name=\"_ctl0$MainContentPlaceHolder$oSess";
                    this.mUIMemberIDorEmailAddreEdit.FilterProperties[HtmlEdit.PropertyNames.TagInstance] = "2";
                    this.mUIMemberIDorEmailAddreEdit.WindowTitles.Add("Virgin Pulse - Internet Explorer");
                    #endregion
                }
                return this.mUIMemberIDorEmailAddreEdit;
            }
        }
        
        public HtmlEdit UIPasswordEdit
        {
            get
            {
                if ((this.mUIPasswordEdit == null))
                {
                    this.mUIPasswordEdit = new HtmlEdit(this);
                    #region Search Criteria
                    this.mUIPasswordEdit.SearchProperties[HtmlEdit.PropertyNames.Id] = "oPwdID";
                    this.mUIPasswordEdit.SearchProperties[HtmlEdit.PropertyNames.Name] = "_ctl0$MainContentPlaceHolder$oSession$oPwdID";
                    this.mUIPasswordEdit.FilterProperties[HtmlEdit.PropertyNames.LabeledBy] = null;
                    this.mUIPasswordEdit.FilterProperties[HtmlEdit.PropertyNames.Type] = "PASSWORD";
                    this.mUIPasswordEdit.FilterProperties[HtmlEdit.PropertyNames.Title] = "Password";
                    this.mUIPasswordEdit.FilterProperties[HtmlEdit.PropertyNames.Class] = "watermark signin login-input-rounded";
                    this.mUIPasswordEdit.FilterProperties[HtmlEdit.PropertyNames.ControlDefinition] = "name=\"_ctl0$MainContentPlaceHolder$oSess";
                    this.mUIPasswordEdit.FilterProperties[HtmlEdit.PropertyNames.TagInstance] = "3";
                    this.mUIPasswordEdit.WindowTitles.Add("Virgin Pulse - Internet Explorer");
                    #endregion
                }
                return this.mUIPasswordEdit;
            }
        }
        
        public HtmlInputButton UILoginButton
        {
            get
            {
                if ((this.mUILoginButton == null))
                {
                    this.mUILoginButton = new HtmlInputButton(this);
                    #region Search Criteria
                    this.mUILoginButton.SearchProperties[HtmlButton.PropertyNames.Id] = "oLogon";
                    this.mUILoginButton.SearchProperties[HtmlButton.PropertyNames.Name] = "_ctl0$MainContentPlaceHolder$oSession$oLogon";
                    this.mUILoginButton.FilterProperties[HtmlButton.PropertyNames.DisplayText] = "Login";
                    this.mUILoginButton.FilterProperties[HtmlButton.PropertyNames.Type] = "submit";
                    this.mUILoginButton.FilterProperties[HtmlButton.PropertyNames.Title] = null;
                    this.mUILoginButton.FilterProperties[HtmlButton.PropertyNames.Class] = "btn yellow-custom";
                    this.mUILoginButton.FilterProperties[HtmlButton.PropertyNames.ControlDefinition] = "name=\"_ctl0$MainContentPlaceHolder$oSess";
                    this.mUILoginButton.FilterProperties[HtmlButton.PropertyNames.TagInstance] = "7";
                    this.mUILoginButton.WindowTitles.Add("Virgin Pulse - Internet Explorer");
                    #endregion
                }
                return this.mUILoginButton;
            }
        }
        #endregion
        
        #region Fields
        private HtmlEdit mUIMemberIDorEmailAddreEdit;
        
        private HtmlEdit mUIPasswordEdit;
        
        private HtmlInputButton mUILoginButton;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "10.0.40219.457")]
    public class UICertificateErrorNaviDocument : HtmlDocument
    {
        
        public UICertificateErrorNaviDocument(UITestControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[HtmlDocument.PropertyNames.Id] = null;
            this.SearchProperties[HtmlDocument.PropertyNames.RedirectingPage] = "False";
            this.SearchProperties[HtmlDocument.PropertyNames.FrameDocument] = "False";
            this.FilterProperties[HtmlDocument.PropertyNames.Title] = "Certificate Error: Navigation Blocked";
            this.FilterProperties[HtmlDocument.PropertyNames.AbsolutePath] = "/invalidcert.htm";
            this.FilterProperties[HtmlDocument.PropertyNames.PageUrl] = "res://ieframe.dll/invalidcert.htm?SSLError=33554432#https://local.virginhealthmil" +
                "es.com/login.aspx";
            this.WindowTitles.Add("Certificate Error: Navigation Blocked - Internet Explorer");
            #endregion
        }
        
        #region Properties
        public HtmlHyperlink UIContinuetothiswebsitHyperlink
        {
            get
            {
                if ((this.mUIContinuetothiswebsitHyperlink == null))
                {
                    this.mUIContinuetothiswebsitHyperlink = new HtmlHyperlink(this);
                    #region Search Criteria
                    this.mUIContinuetothiswebsitHyperlink.SearchProperties[HtmlHyperlink.PropertyNames.Id] = "overridelink";
                    this.mUIContinuetothiswebsitHyperlink.SearchProperties[HtmlHyperlink.PropertyNames.Name] = "overridelink";
                    this.mUIContinuetothiswebsitHyperlink.FilterProperties[HtmlHyperlink.PropertyNames.Target] = null;
                    this.mUIContinuetothiswebsitHyperlink.FilterProperties[HtmlHyperlink.PropertyNames.InnerText] = "Continue to this website (not recommended).";
                    this.mUIContinuetothiswebsitHyperlink.FilterProperties[HtmlHyperlink.PropertyNames.AbsolutePath] = "/login.aspx";
                    this.mUIContinuetothiswebsitHyperlink.FilterProperties[HtmlHyperlink.PropertyNames.Title] = null;
                    this.mUIContinuetothiswebsitHyperlink.FilterProperties[HtmlHyperlink.PropertyNames.Href] = "https://local.virginhealthmiles.com/login.aspx";
                    this.mUIContinuetothiswebsitHyperlink.FilterProperties[HtmlHyperlink.PropertyNames.Class] = null;
                    this.mUIContinuetothiswebsitHyperlink.FilterProperties[HtmlHyperlink.PropertyNames.ControlDefinition] = "name=\"overridelink\" id=\"overridelink\" hr";
                    this.mUIContinuetothiswebsitHyperlink.FilterProperties[HtmlHyperlink.PropertyNames.TagInstance] = "2";
                    this.mUIContinuetothiswebsitHyperlink.WindowTitles.Add("Certificate Error: Navigation Blocked - Internet Explorer");
                    #endregion
                }
                return this.mUIContinuetothiswebsitHyperlink;
            }
        }
        #endregion
        
        #region Fields
        private HtmlHyperlink mUIContinuetothiswebsitHyperlink;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "10.0.40219.457")]
    public class UILandingPageVirginPulDocument : HtmlDocument
    {
        
        public UILandingPageVirginPulDocument(UITestControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[HtmlDocument.PropertyNames.Id] = null;
            this.SearchProperties[HtmlDocument.PropertyNames.RedirectingPage] = "False";
            this.SearchProperties[HtmlDocument.PropertyNames.FrameDocument] = "False";
            this.FilterProperties[HtmlDocument.PropertyNames.Title] = "Landing Page - Virgin Pulse";
            this.FilterProperties[HtmlDocument.PropertyNames.AbsolutePath] = "/v2/member/landingpage";
            this.FilterProperties[HtmlDocument.PropertyNames.PageUrl] = "https://local.virginhealthmiles.com/v2/member/landingpage";
            this.WindowTitles.Add("Landing Page - Virgin Pulse - Internet Explorer");
            #endregion
        }
        
        #region Properties
        public HtmlHyperlink UIChallengeConsoleHyperlink
        {
            get
            {
                if ((this.mUIChallengeConsoleHyperlink == null))
                {
                    this.mUIChallengeConsoleHyperlink = new HtmlHyperlink(this);
                    #region Search Criteria
                    this.mUIChallengeConsoleHyperlink.SearchProperties[HtmlHyperlink.PropertyNames.Id] = null;
                    this.mUIChallengeConsoleHyperlink.SearchProperties[HtmlHyperlink.PropertyNames.Name] = null;
                    this.mUIChallengeConsoleHyperlink.SearchProperties[HtmlHyperlink.PropertyNames.Target] = null;
                    this.mUIChallengeConsoleHyperlink.SearchProperties[HtmlHyperlink.PropertyNames.InnerText] = "Challenge Console";
                    this.mUIChallengeConsoleHyperlink.FilterProperties[HtmlHyperlink.PropertyNames.AbsolutePath] = "/challengemanager";
                    this.mUIChallengeConsoleHyperlink.FilterProperties[HtmlHyperlink.PropertyNames.Title] = null;
                    this.mUIChallengeConsoleHyperlink.FilterProperties[HtmlHyperlink.PropertyNames.Href] = "https://local.virginhealthmiles.com/challengemanager";
                    this.mUIChallengeConsoleHyperlink.FilterProperties[HtmlHyperlink.PropertyNames.Class] = null;
                    this.mUIChallengeConsoleHyperlink.FilterProperties[HtmlHyperlink.PropertyNames.ControlDefinition] = "href=\"/challengemanager\"";
                    this.mUIChallengeConsoleHyperlink.FilterProperties[HtmlHyperlink.PropertyNames.TagInstance] = "14";
                    this.mUIChallengeConsoleHyperlink.WindowTitles.Add("Landing Page - Virgin Pulse - Internet Explorer");
                    #endregion
                }
                return this.mUIChallengeConsoleHyperlink;
            }
        }
        #endregion
        
        #region Fields
        private HtmlHyperlink mUIChallengeConsoleHyperlink;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "10.0.40219.457")]
    public class UIChallengeManagerDocument : HtmlDocument
    {
        
        public UIChallengeManagerDocument(UITestControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[HtmlDocument.PropertyNames.Id] = null;
            this.SearchProperties[HtmlDocument.PropertyNames.RedirectingPage] = "False";
            this.SearchProperties[HtmlDocument.PropertyNames.FrameDocument] = "False";
            this.FilterProperties[HtmlDocument.PropertyNames.Title] = "Challenge Manager";
            this.FilterProperties[HtmlDocument.PropertyNames.AbsolutePath] = "/challengemanager/";
            this.FilterProperties[HtmlDocument.PropertyNames.PageUrl] = "https://local.virginhealthmiles.com/challengemanager/";
            this.WindowTitles.Add("Challenge Manager - Internet Explorer");
            #endregion
        }
        
        #region Properties
        public HtmlEdit UISponsorIDEdit
        {
            get
            {
                if ((this.mUISponsorIDEdit == null))
                {
                    this.mUISponsorIDEdit = new HtmlEdit(this);
                    #region Search Criteria
                    this.mUISponsorIDEdit.SearchProperties[HtmlEdit.PropertyNames.Id] = "SponsorID";
                    this.mUISponsorIDEdit.SearchProperties[HtmlEdit.PropertyNames.Name] = null;
                    this.mUISponsorIDEdit.SearchProperties[HtmlEdit.PropertyNames.LabeledBy] = "Sponsor ID";
                    this.mUISponsorIDEdit.SearchProperties[HtmlEdit.PropertyNames.Type] = "SINGLELINE";
                    this.mUISponsorIDEdit.FilterProperties[HtmlEdit.PropertyNames.Title] = null;
                    this.mUISponsorIDEdit.FilterProperties[HtmlEdit.PropertyNames.Class] = "ng-pristine ng-valid";
                    this.mUISponsorIDEdit.FilterProperties[HtmlEdit.PropertyNames.ControlDefinition] = "class=\"ng-pristine ng-valid\" id=\"Sponsor";
                    this.mUISponsorIDEdit.FilterProperties[HtmlEdit.PropertyNames.TagInstance] = "1";
                    this.mUISponsorIDEdit.WindowTitles.Add("Challenge Manager - Internet Explorer");
                    #endregion
                }
                return this.mUISponsorIDEdit;
            }
        }
        
        public UIWrapperPane UIWrapperPane
        {
            get
            {
                if ((this.mUIWrapperPane == null))
                {
                    this.mUIWrapperPane = new UIWrapperPane(this);
                }
                return this.mUIWrapperPane;
            }
        }
        #endregion
        
        #region Fields
        private HtmlEdit mUISponsorIDEdit;
        
        private UIWrapperPane mUIWrapperPane;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "10.0.40219.457")]
    public class UIWrapperPane : HtmlDiv
    {
        
        public UIWrapperPane(UITestControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[HtmlDiv.PropertyNames.Id] = "wrapper";
            this.SearchProperties[HtmlDiv.PropertyNames.Name] = null;
            this.FilterProperties[HtmlDiv.PropertyNames.InnerText] = " \r\n\r\n\r\n \r\nSELECT A SPONSOR\r\n \r\n\r\n\r\nSpons";
            this.FilterProperties[HtmlDiv.PropertyNames.Title] = null;
            this.FilterProperties[HtmlDiv.PropertyNames.Class] = "container ng-scope";
            this.FilterProperties[HtmlDiv.PropertyNames.ControlDefinition] = "class=\"container ng-scope\" id=\"wrapper\" ";
            this.FilterProperties[HtmlDiv.PropertyNames.TagInstance] = "5";
            this.WindowTitles.Add("Challenge Manager - Internet Explorer");
            #endregion
        }
        
        #region Properties
        public HtmlButton UISearchButton
        {
            get
            {
                if ((this.mUISearchButton == null))
                {
                    this.mUISearchButton = new HtmlButton(this);
                    #region Search Criteria
                    this.mUISearchButton.SearchProperties[HtmlButton.PropertyNames.Id] = null;
                    this.mUISearchButton.SearchProperties[HtmlButton.PropertyNames.Name] = null;
                    this.mUISearchButton.SearchProperties[HtmlButton.PropertyNames.DisplayText] = "Search";
                    this.mUISearchButton.FilterProperties[HtmlButton.PropertyNames.Type] = "submit";
                    this.mUISearchButton.FilterProperties[HtmlButton.PropertyNames.Title] = null;
                    this.mUISearchButton.FilterProperties[HtmlButton.PropertyNames.Class] = "btn yellow-button pull-left";
                    this.mUISearchButton.FilterProperties[HtmlButton.PropertyNames.ControlDefinition] = "class=\"btn yellow-button pull-left\" ng-c";
                    this.mUISearchButton.FilterProperties[HtmlButton.PropertyNames.TagInstance] = "1";
                    this.mUISearchButton.WindowTitles.Add("Challenge Manager - Internet Explorer");
                    #endregion
                }
                return this.mUISearchButton;
            }
        }
        #endregion
        
        #region Fields
        private HtmlButton mUISearchButton;
        #endregion
    }
}
