'use strict';

function setTheme(rootScope, scope) {
    rootScope.Theme = scope.Theme = {
        Class: 'Red'
    };
}

function WelcomeCtrl($scope, $rootScope, $http, $route, $routeParams, $location, Enrollment) {
    
    setTheme($rootScope, $scope);
    $scope.Token = $routeParams.token;
    $scope.Model = {Error: false};
    $scope.Waiting = true;

    Enrollment.query({ token: $routeParams.token }, function(response) {
        $scope.Waiting = false;
        if (!response.IsValid) {
            $scope.Model.Error = true;
        }
    }, function(response) {
        if (response.status == 401 || response.status == 404) {
            $scope.Waiting = false;
            $scope.Model.Error = true;
        } else {
            $location.path("/home");
        }
    });
}
WelcomeCtrl.$inject = ['$scope', '$rootScope', '$http', '$route', '$routeParams', '$location', 'Enrollment'];

function EnrollCtrl($scope, $rootScope, $http, $route, $routeParams, $location, Enrollment) {
    setTheme($rootScope, $scope);
    $scope.Token = $routeParams.token;
    $scope.Titles = ['Mr','Mrs','Ms','Miss','Dr'];
    $scope.Months = [
        { Month: 1, Abbr: 'Jan' },
        { Month: 2, Abbr: 'Feb' },
        { Month: 3, Abbr: 'Mar' },
        { Month: 4, Abbr: 'Apr' },
        { Month: 5, Abbr: 'May' },
        { Month: 6, Abbr: 'Jun' },
        { Month: 7, Abbr: 'Jul' },
        { Month: 8, Abbr: 'Aug' },
        { Month: 9, Abbr: 'Sep' },
        { Month: 10, Abbr: 'Oct' },
        { Month: 11, Abbr: 'Nov' },
        { Month: 12, Abbr: 'Dec' }
    ];
    
    $scope.Days = [];

    var Term = function () {
        var self = this;
        self.TermsCode = '';
        self.Accept = false;
        self.Description = '';
        self.EntityID = 0;
        self.version = 0;
     };
    $scope.Model = {
        Token: $scope.Token,
        Title: '',
        FirstName: '',
        LastName: '',
        Gender: '',
        DOB_Month: '',
        DOB_Day: '',
        DOB_Year: '',
        Email: '',
        ConfirmEmail: '',
        Phone: '',
        Password: '',
        ConfirmPassword: '',
        Accept: '',
        Agree: '',
        Errors: [],
        IsValid: true,
        Terms: [],
        SponsorFilters: []
    };
    for (var d = 1; d < 32; d++) {
        $scope.Days.push(d);
    }
    $scope.Years = [];
    var today = new Date();
    
    for (var y = today.getFullYear() ; y > today.getFullYear() - 115; y--) {
        $scope.Years.push(y);
    }

    $scope.ValidateEnrollment = function(model, field) {
        
        model.IsValid = true;
        model.Errors = [];
        
        if (model.FirstName.length == 0 || model.LastName.length == 0) {
            model.IsValid = false;
            model.Errors.push('First and Last name are required');
        }
        if (model.Gender.length == 0) {
            model.IsValid = false;
            model.Errors.push('Gender required');
        }
        
        ////Check Filter values
        for (var index = 0; index < model.SponsorFilters.length; ++index) {
            if (model.SponsorFilters[index].FilterValueID == null || model.SponsorFilters[index].FilterValueID == 0) {
                model.IsValid = false;
                model.Errors.push(model.SponsorFilters[index].FilterName + ' is  required');
            }
        }
       
        if (model.DOB_Month == null || model.DOB_Day == null || model.DOB_Year == null) {
            model.IsValid = false;
        }
        var minAge = 18;
        if (model.DOB_Year > today.getFullYear() - minAge) {
            model.IsValid = false;
            model.Errors.push('You must be at least ' + minAge + ' years old to join');
        }
        
        if (model.Email.length == 0) {
            model.IsValid = false;
            model.Errors.push('Email is required');
        } else {
            
            if (model.Email != model.ConfirmEmail) {
                model.IsValid = false;
                model.Errors.push('Emails do not match');
            }
            if (model.Email.indexOf('@') < 0 || model.Email.indexOf('.') < 0) { // not too picky about this
                model.Errors.push('Email is invalid');
            }
        }
        if (model.Phone.length == 0) {
            model.IsValid = false;
            model.Errors.push('Phone number is required');
        }
        
        var passwordRegEx = new RegExp('(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,50})$');
        if (model.Password.length < 8) {
            model.IsValid = false;
            model.Errors.push('Password must be at least 8 characters');
        } else if (passwordRegEx.test(model.Password) == false) {
            model.IsValid = false;
            model.Errors.push('Password must contain at least one letter and at least one number');
        } else {
            if (model.Password != model.ConfirmPassword) {
                model.IsValid = false;
            }
        }
        if (model.IsValid) {
            $scope.SaveEnrollment();
        } 
    };

    Enrollment.get({ token: $routeParams.token },
        function(data) {

            for (var i = 0; i < data.Terms.length; i++) {
                var t = new Term();
                t.TermsCode = data.Terms[i].TermsCode;
                t.Description = data.Terms[i].Description;
                t.EntityID = data.Terms[i].EntityID;
                t.Version = data.Terms[i].Version;
           
                $scope.Model.Terms.push(t);
            }

            $scope.Model.SponsorFilters = data.SponsorFilters;
        },
        function (response) {
            if (response.status == 401 || response.status == 404) {
                $scope.Waiting = false;
                $scope.Model.Error = true;
            } else {
                $location.path("/home");
            }
        });
    
    $scope.SaveEnrollment = function () {
        
        $scope.Waiting = true;
        $scope.Model.Errors = [];

        Enrollment.post(JSON.stringify($scope.Model),
            function(mId) {
                $rootScope.Email = $scope.Model.Email;

                document.cookie = "LoginID=" + $scope.Email + ";path=/";

                // if enrollment was successful, login and go directly to the landing page
                $.ajax({
                    url: '/v2/account/LoginExpress',
                    data: { userName: $scope.Model.Email, pwd: $scope.Model.Password },
                    cache: false,
                    success: function(loggedIn) {
                        if (loggedIn == true) {
                            window.location = '/v2/member/landingpage';
                        } else {
                            $scope.Model.IsValid = false;
                            $scope.Model.Errors.push("Your account has been created, but there was a problem logging you in. Please click <a href='/login.aspx'>here</a> to log in.");
                        }
                        return true;
                    },
                    error: function () {
                        $scope.$apply(function() {
                            $scope.Waiting = false;
                            $scope.Model.IsValid = false;
                            $scope.Model.Errors.push("Your account has been created, but there was a problem logging you in. Please click <a href='/login.aspx'>here</a> to log in.");
                            return false;
                        });
                    }
                });
            }, function(response) {
                $scope.Waiting = false;
                if (response.status == 400) {
                    $scope.Model.IsValid = false;
                    if (response.data.indexOf('domain') > 0) {
                        $scope.Model.Errors.push('You cannot register for the program with this email address (' + $scope.Model.Email + '). Please contact your plan administrator for more details.');
                    } else if (response.data == "\"Validation Failed\"") {
                        $scope.Model.Errors.push("Validation Failed. Please include all required fields.");
                    } else {
                        $scope.Model.Errors.push("There was a problem with registration. It could be that the invitation is already used or the email you entered already exists in our system.");
                    }
                } else {
                    $scope.Model.IsValid = false;
                    $scope.Model.Errors.push("There was a problem saving your information.");
                }
            });
    };
}
EnrollCtrl.$inject = ['$scope', '$rootScope', '$http', '$route', '$routeParams', '$location', 'Enrollment'];


