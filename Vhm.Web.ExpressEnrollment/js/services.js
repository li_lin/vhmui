'use strict';

var services = angular.module('FaF.services', ['ngResource']).
    factory('Enrollment', function($resource) {
        return $resource('/FFApi/api/Enrollment/:token', {}, {
            get: { method: 'GET', isArray: false },
            query: { method: 'GET', isArray: false},
            post: { method: 'POST' },
            update: { method: 'PUT' },
            remove: { method: 'DELETE' }
        });
    });





