'use strict';

// Declare app level module which depends on filters, and services
angular.module('FaF', ['FaF.filters', 'FaF.services', 'FaF.directives', 'ngCookies']).
    config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/welcome/:token', { templateUrl: 'partials/welcome.html', controller: WelcomeCtrl });
        $routeProvider.when('/enroll/:token', { templateUrl: 'partials/enroll.html', controller: EnrollCtrl });
        $routeProvider.otherwise({ redirectTo: '/home' });
    }]);
