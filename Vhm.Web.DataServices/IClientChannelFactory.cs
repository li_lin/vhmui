﻿using System.ServiceModel;

namespace Vhm.Web.DataServices
{
    public interface IClientChannelFactory<TChannel> where TChannel : IClientChannel
    {
        void Open();
        void Close();
        void Abort();

        TChannel CreateChannel();
    }
}
