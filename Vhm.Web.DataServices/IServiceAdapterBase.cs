﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vhm.Web.DataServices.MiddleTierService;

namespace Vhm.Web.DataServices
{
    public interface  IServiceAdapterBase
    {

        VhmMember GetMemberById(long? memberId);
        VhmMember[] GetMembersById(long[] memberIds);
        VhmSession CheckForValidSession(string sessionId);
        VhmMember[] SearchMemberByEmail(string email);
        bool UpdateMember(VhmMember member);
        VhmSession GetNewSession(string userName, string password, int timeout, bool updateLastAction, bool updateVisit,
                                 string system, bool isMobile, string aPnToken);
        VhmMember GetMemberBySession(VhmSession memberSession);
        SocMemberProfile GetSocialMemberProfile(long memberId);
        bool SaveMemberProfile(SocMemberProfile memberProfile, bool deletePictureProfile);
        Sponsor[] GetMemberActiveSponsors(long memberId);
        bool InvalidateSession(string sessionId);
        SocGroupCategory[] GetGroupCategories();
    }
}
