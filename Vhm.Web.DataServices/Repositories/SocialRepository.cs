﻿using System;
using System.Configuration;
using Vhm.Web.Common;
using Vhm.Web.Common.Utils;
using Vhm.Web.DataServices.MiddleTierService;
using Vhm.Web.Models.RepositoryModels;
using System.Globalization;

namespace Vhm.Web.DataServices.Repositories
{
    public class SocialRepository
    {
        #region Front End Objects
        public static Group GetGroup(SocGroup group)
        {
            if (group == null)
            {
                return new Group();
            }
            string adminName = string.Format("{0} {1}", group.GroupAdminFirstName, group.GroupAdminLastName);
            var g = new Group
            {
                CommunityID = group.CommunityID,
                AdminId = group.GroupAdminID,
                GroupTypeID = group.GroupTypeID,
                AdminName = System.Net.WebUtility.HtmlDecode(adminName),
                Category = System.Net.WebUtility.HtmlDecode(group.GroupCategoryName),
                Description = System.Net.WebUtility.HtmlDecode(group.GroupDescription),
                Id = group.GroupID,
                Name = group.GroupName,
                CategoryId = group.GroupCategoryID,
                IsPrivate = group.Privacy,
                CreatorId = group.GroupCreatorID,
                CreatedDateString = group.CreatedDate.ToShortDateString(),
                Image = group.GroupImage,
                GroupImageUrl = group.GroupImageUrl,
                Deleted = group.Deleted,
                ProfilePicturePath = group.GroupImageUrl
            };
            return g;
        }

        public static Group GetGroup(SocMemberGroup group)
        {
            string adminName = string.Format("{0} {1}", group.FirstName, group.LastName);
            var g = new Group
                        {
                            Name = group.GroupName,
                            Id = group.GroupID,
                            CategoryId = group.GroupCategoryID,
                            Category = System.Net.WebUtility.HtmlDecode(group.GroupCategoryName),
                            ProfilePicturePath = group.GroupImageUrl,
                            AdminName = System.Net.WebUtility.HtmlDecode(adminName),
                            AdminId = (group.GroupAdminID != null) ? Convert.ToInt64(group.GroupAdminID) : 0,
                            Description = System.Net.WebUtility.HtmlDecode(group.GroupDescription),
                            Deleted = group.Deleted,
                            CreatedDateString = group.JoinedDate.Year > 1
                                                    ? group.JoinedDate.ToShortDateString()
                                                    : string.Empty,
                            Image = group.GroupImage,
                            GroupImageUrl = group.GroupImageUrl,
                            IsMemberGroupExpert = group.GroupExpert
                        };
            return g;
        }

        public static GroupCategory GetGroupCategory(SocGroupCategory mtCategoryObject)
        {
            var categoryGrp = new GroupCategory
            {
                GroupCategoryId = mtCategoryObject.GroupCategoryID,
                GroupCategoryName = System.Net.WebUtility.HtmlDecode(mtCategoryObject.GroupCategoryName),
                GroupCategoryDescription = System.Net.WebUtility.HtmlDecode(mtCategoryObject.GroupCategoryDescription)

            };
            return categoryGrp;
        }

        public static MemberNotification GetMemberNotification(SocMemberNotification socMemberNotif)
        {
            var returnNotific = new MemberNotification
                                    {

                                        MemberNotificationId = socMemberNotif.MemberNotificationID,
                                        MemberId = socMemberNotif.MemberID,
                                        NotificationTypeId = socMemberNotif.NotificationTypeID,
                                        NotificationTypeEntityId = socMemberNotif.NotificationTypeEntityID,
                                        NotifiedById = socMemberNotif.NotifiedByID,
                                        NotificationDate = socMemberNotif.NotificationDate.ToShortDateString(),
                                        NotificationStatusID = socMemberNotif.NotificationStatusID
                                    };
            return returnNotific;
        }

        public static SocMemberNotification SocMemberNotification(MemberNotification notification)
        {
            return AutoMapper.Mapper.Map<SocMemberNotification>(notification);
        }

        public static Member GetMember(VhmMember member)
        {
            //VhmMember doesn't return picture info
            var m = new Member
            {
                MemberId = member.MemberID,
                EmailAddress = member.EmailAddress,
                FirstName = member.Forename,
                Age = member.Age,
                Average = member.Average,
                LastName = member.Surname,
                OccupationId = member.OccupationID,
                Total = member.Total,
                UpdatedDate = member.UpdatedDate,
                Gender = member.Gender,
                IdentityNumber = member.IdentityNumber
            };
            //build image path for the profile pic. Some middle tier methods retrun it, some don't

            return m;
        }

        public static Member GetMember(SocMemberFriend mFrined)
        {
            var member = AutoMapper.Mapper.Map<Member>(mFrined);
            //var member = DataMapper.CopyIntoNewType<Member, SocMemberFriend>(mFrined);
            //we need to build the ImageUrl as it seems it is not returned by SocMemberFriend
            member.ImageUrl = BuildMemberProfilePicturePath(mFrined.ProfilePicture);
            return member;
        }

        public static Member GetMember(SocMemberProfile mProfile)
        {
            var m = new Member
            {
                MemberId = mProfile.MemberID,
                FirstName = mProfile.FirstName,
                City = mProfile.City,
                LastName = mProfile.LastName,
                Sponsors = mProfile.Sponsors,
                Activity = mProfile.Activity,
                Age = mProfile.Age,
                Average = mProfile.Average,
                ChallengesParticipatedIn = mProfile.ChallengesParticipatedIn,
                DisplayName = mProfile.DisplayName,
                Gender = mProfile.Gender,
                IdentityNumber = mProfile.IdentityNumber,
                Goals = mProfile.Goals,
                Interests = mProfile.Interests,
                LastGZUpload = mProfile.LastGZUpload,
                Level = mProfile.Level,
                Motto = mProfile.Motto,
                NewsFeedPermissions = mProfile.NewsFeedPermissions,
                PrivacySettingsSet = mProfile.PrivacySettingsSet,
                ProfileExists = mProfile.ProfileExists,
                ProfilePicture = mProfile.ProfilePicture,
                RewardsAniversary = mProfile.RewardsAniversary,
                ShowAge = mProfile.ShowAge,
                ShowAnniversary = mProfile.ShowAnniversary,
                ShowBadges = mProfile.ShowBadges,
                ShowChallengeHistory = mProfile.ShowChallengeHistory,
                ShowCity = mProfile.ShowCity,
                ShowFriendNewsFeed = mProfile.ShowFriendNewsFeed,
                ShowGender = mProfile.ShowGender,
                ShowGoals = mProfile.ShowGoals,
                ShowInterests = mProfile.ShowInterests,
                ShowLastGZUpload = mProfile.ShowLastGZUpload,
                ShowNickname = mProfile.ShowNickname,
                ShowOccupation = mProfile.ShowOccupation,
                ShowPoints = mProfile.ShowPoints,
                ShowSponsors = mProfile.ShowSponsors,
                ShowState = mProfile.ShowState,
                ShowStepsAvg = mProfile.ShowStepsAvg,
                ShowTotalSteps = mProfile.ShowTotalSteps,
                State = mProfile.State,
                StateCode = mProfile.StateCode,
                SuppressManageSponsor = mProfile.SuppressManageSponsor,
                Total = mProfile.Total,
                UpdatedDate = mProfile.UpdatedDate,
                ImageUrl = BuildMemberProfilePicturePath(mProfile.ProfilePicture),

                PersonalInfoComplete = mProfile.PersonalInfoComplete,

                OccupationComplete = mProfile.OccupationComplete,
                GoalsComplete = mProfile.GoalsComplete,
                InterestsComplete = mProfile.InterestsComplete,

            };
            return m;
        }

        public static Occupation Occupation(MemberOccupation item)
        {
            return AutoMapper.Mapper.Map<Occupation>( item );
        }

        public static Member GetMember(SocSuggestedFriend memberSuggested)
        {
            var m = new Member
            {
                MemberId = memberSuggested.SuggestedFriendID,
                FirstName = memberSuggested.FirstName,
                ImageUrl = BuildMemberProfilePicturePath(memberSuggested.ProfilePicture)
            };
            return m;
        }
        public static Member GetMember(SocMemberInvite member)
        {
            var m = new Member
                        {
                            DisplayName = member.DisplayName,
                            FirstName = member.FirstName,
                            LastName = member.LastName,
                            EmailAddress = member.EmailAddress,
                            IdentityNumber = member.IdentityNumber,
                            Motto = member.Motto,
                            ShowNickname = member.ShowNickname
                        };

            if (member.MemberID != null)
                m.MemberId = member.MemberID.Value;

            return m;
        }

        public static SocialPagedData GetPagedData(PagedData pagedData)
        {
            var pData = new SocialPagedData();
            int nCurrentPage = 0;
            if (pagedData.PageNumber != null)
            {
                pData.CurrentPage = (int)pagedData.PageNumber;
                nCurrentPage = (int)pagedData.PageNumber;
            }

            pData.TotalCount = pagedData.TotalCount;
            if (pagedData.TotalPageCount != null)
            {
                pData.TotalPageCount = (int)pagedData.TotalPageCount;
                //set up the "Has More Value
                nCurrentPage = nCurrentPage + 1;
                pData.HasMore = (nCurrentPage < pData.TotalPageCount);
            }
            if (pagedData.MemberFriends != null)
            {
                foreach (SocMemberFriend i in pagedData.MemberFriends)
                {
                    pData.Friends.Add(GetMember(i));
                }
            }
            if (pagedData.SuggestedGroups != null)
            {

                foreach (SocMemberGroup i in pagedData.SuggestedGroups)
                {
                    pData.SuggestedGroups.Add(GetGroup(i));
                }
            }
            if (pagedData.MemberGroups != null)
            {
                foreach (SocMemberGroup g in pagedData.MemberGroups)
                {
                    pData.Groups.Add(GetGroup(g));
                }
            }
            if (pagedData.Groups != null)
            {
                foreach (SocGroup socGrp in pagedData.Groups)
                {
                    pData.Groups.Add(GetGroup(socGrp));
                }
            }
            if (pagedData.GroupMembers != null)
            {
                foreach (SocMemberProfile i in pagedData.GroupMembers)
                {
                    pData.GroupMembers.Add(GetMember(i));
                }
            }
            return pData;
        }
        #endregion

        #region Convert to middle tier objects
        public static SocMemberGroup GetSocMemberGroup(Group feGroup, long nMemberId)
        {
            //should be able to use one generic method that converts front end obj to middle tier and vice -versa like:
            // SocMemberGroup memberGroup = DataMapper.CopyIntoNewType<SocMemberGroup, Group>(group.Group);
            return new SocMemberGroup
                       {
                           MemberID = nMemberId,
                           GroupName = feGroup.Name,
                           GroupID = feGroup.Id,
                           GroupCategoryID = feGroup.CategoryId,
                           GroupTypeID = Convert.ToByte(EnumGroupType.MEM),
                           GroupCategoryName = feGroup.Category,
                           GroupCreatorID = feGroup.AdminId,
                           GroupCreatorName = feGroup.AdminName,
                           GroupDescription = feGroup.Description,
                           GroupExpert = feGroup.IsMemberGroupExpert,
                           Deleted = feGroup.Deleted
                       };
        }
        #endregion


        public static SocGroup GetSocGroup(Group group)
        {
            return new SocGroup
                       {
                           CommunityID = group.CommunityID,
                           GroupImage = group.Image,
                           //use this as looks like there are issues with parsing the datetime when users have set up different cultures
                           CreatedDate = DateTime.Parse(group.CreatedDateString, new CultureInfo("en-US")),
                           GroupAdminID = group.AdminId,
                           GroupCategoryID = group.CategoryId,
                           GroupDescription = group.Description,
                           GroupCreatorID = group.CreatorId,
                           GroupTypeID = group.GroupTypeID,
                           GroupID = group.Id,
                           GroupName = group.Name,
                           Privacy = group.IsPrivate,
                       };
        }

        public static SocMemberNotification GetSocMemberNotification(MemberNotification item)
        {
            return AutoMapper.Mapper.Map<SocMemberNotification>(item);
        }

        public static NewsfeedPost NewsFeedPost(SocNewsFeedPost item)
        {
            return AutoMapper.Mapper.Map<NewsfeedPost>(item);
        }

        public static MemberRelationships MemberRelationships(SocMemberRelationship item)
        {
            return AutoMapper.Mapper.Map<MemberRelationships>(item);
        }

        private static string BuildMemberProfilePicturePath(byte[] dbGuid)
        {
            string result;
            if (dbGuid != null && dbGuid.Length > 1)
            {
                result = string.Format("{0}{1}.jpg",
                                       ApplicationSettings.MemberImageUrl,
                                        new Guid(dbGuid));
            }
            else
            {
                result = string.Format("{0}{1}.gif",
                                        ApplicationSettings.MemberImageUrl,
                                        ApplicationSettings.DefaultMemberPicGuid);
            }
            return result;
        }

        public static MemberProfile MemberProfile( VhmMemberProfile item )
        {
            return AutoMapper.Mapper.Map<MemberProfile>(item);
        }


    }
}
