﻿using System.Collections.Generic;
using AutoMapper;
using Vhm.Web.DataServices.Adapters;
using Vhm.Web.DataServices.Interfaces;
using Vhm.Web.DataServices.MiddleTierService;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.DataServices.Repositories
{
    class SponsorRepository
    {
        public static SponsorProfile SponsorProfile(VhmSponsorProfile item)
        {
            return Mapper.Map<VhmSponsorProfile, SponsorProfile>(item);
        }

        public static ColorTheme AvailableSponsorColorThemes(VhmBrandingTheme item)
        {
            return Mapper.Map<VhmBrandingTheme, ColorTheme>(item);
        }

        public static SponsorLanguage SponsorLanguage(VhmSponsorLanguage item)
        {
            return Mapper.Map<VhmSponsorLanguage, SponsorLanguage>(item);
        }

        public static SponsorModule SponsorModules(VhmSponsorModule item)
        {
            return Mapper.Map<VhmSponsorModule, SponsorModule>(item);
        }

        public static SponsorModuleGZVersion SponsorModuleGZVersion(VhmSponsorModuleGZVersion item)
        {
            return Mapper.Map<VhmSponsorModuleGZVersion, SponsorModuleGZVersion>( item );
        }

        public static RewardsProgram RewardsProgram(VhmProgram item)
        {
            return Mapper.Map<VhmProgram, RewardsProgram>( item );
        }
    }
}
