﻿using Vhm.Web.DataServices.MiddleTierActivityService;
using Vhm.Web.DataServices.MiddleTierService;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.DataServices.Repositories
{
    class MemberRepository
    {
        public static SocMemberProfile GetSocMemberProfile(Member item)
        {
            return AutoMapper.Mapper.Map<Member, SocMemberProfile>(item);
        }

        public static VhmMemberPersonalInfo VhmMemberPersonalInfo(MemberPersonalInformation item)
        {
            return AutoMapper.Mapper.Map<VhmMemberPersonalInfo>(item);
        }

        public static MemberGoalsAndInterests MemberGoalsAndInterests(GoalsAndInterests item)
        {
            return AutoMapper.Mapper.Map<MemberGoalsAndInterests>(item);
        }

        public static RecentlyEarnedRewardsItem RewardItem( VhmMemberRecentReward item )
        {
            var newItem = AutoMapper.Mapper.Map<RecentlyEarnedRewardsItem>(item);
            return newItem;
        }

        public static Quest Quest(VhmMemberChallengeMenu item) {
            var quest = AutoMapper.Mapper.Map<Quest>(item);
            return quest;
        }        

        public static Promo Promo(VhmMemberChallengeMenu item) {
            var promo = AutoMapper.Mapper.Map<Promo>(item);
            return promo;
        }

        public static Kag Kag(VhmMemberChallengeMenu item) {
            var kag = AutoMapper.Mapper.Map<Kag>(item);
            return kag;
        }

        public static TrackingChallenge TrackingChallenge(VhmMemberChallengeMenu item)
        {
            var trk = AutoMapper.Mapper.Map<TrackingChallenge>(item);
            return trk;
        }

        public static Challenge Challenge(VhmMemberChallengeMenu item)
        {
            var challenge = AutoMapper.Mapper.Map<Challenge>( item );
            return challenge;
        }

        public static ActivityGraphData.ActivityDay ActivityDay(VhmStep item)
        {
            var activity = AutoMapper.Mapper.Map<ActivityGraphData.ActivityDay>( item );
            return activity;
        }
    
        public static Member Member(VhmMember item)
        {
            return AutoMapper.Mapper.Map<Member>( item );
        }

        public static MemberEmail MemberEmail(VhmMemberEmail item)
        {
            return AutoMapper.Mapper.Map<MemberEmail>(item);
        }

        public static MemberModule MemberModule(VhmMemberModule item)
        {
            return AutoMapper.Mapper.Map<MemberModule>( item );
        }

        public static PromoTask PromoTask(VhmPromoTask item)
        {
            return AutoMapper.Mapper.Map<PromoTask>( item );
        }

        public static PromoTarget PromoTarget(VhmPromoTarget item) {
            var target = AutoMapper.Mapper.Map<PromoTarget>(item);
            return target;
        }

        public static CultureFormat CultureFormat(VhmCultureFormat item)
        {
            var culture = AutoMapper.Mapper.Map<CultureFormat>(item);
            return culture;
        }

        public static MemberPersonalInformation MemberPersInfo(VhmMemberPersonalInfo item)
        {
            return AutoMapper.Mapper.Map<MemberPersonalInformation>( item );
        }
        public static Terms Terms(VhmTerms item)
        {
            return AutoMapper.Mapper.Map<Terms>(item);
        }
        public static Terms TermsAndConditions(VhmTermsAndConditions item)
        {
            return AutoMapper.Mapper.Map<Terms>(item);
        }
        public static Terms TermsToAcknowledge(VhmTermsToAcknowledge item)
        {
            return AutoMapper.Mapper.Map<Terms>(item);
        }

        public static Voucher MemberVoucher (VhmMemberRedeemVoucher item)
        {
            return AutoMapper.Mapper.Map<Voucher>(item);
        }

        public static VhmMemberRedeemVoucher MemberMiddleTierVoucher(Voucher item)
        {
            return AutoMapper.Mapper.Map<VhmMemberRedeemVoucher>(item);
        }

        public static Tickers Ticker(VhmTicker item)
        {
            var tickers= AutoMapper.Mapper.Map<Tickers>(item);
            return tickers;
        }

        public static BadWords BadWords(VhmBadWord item)
        {
            var badWord = AutoMapper.Mapper.Map<BadWords>(item);
            return badWord;
        }

        public static MemberDevice MemberDevice( VhmMemberDevice item )
        {
            var device = AutoMapper.Mapper.Map<MemberDevice>( item );
            return device;
        }

        public static Lookup Lookup( VhmLookup item )
        {
            var lookup = AutoMapper.Mapper.Map<Lookup>( item );
            return lookup;
        }
    }
}
