﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Vhm.Web.Models;
using Vhm.Web.DataServices.MiddleTierService;

namespace Vhm.Web.DataServices.Repositories
{
    public static class AccountRepository
    {
        private static DateTime? StringToDate(string dtString)
        {
            if (string.IsNullOrEmpty(dtString))
            {
                return null;
            }

            //Try to parse string to datetime type
            DateTime convertedDate;

            if (DateTime.TryParse(dtString, out convertedDate))
            {
                return convertedDate;
            }

            //For other instances try to parse string and get values that way
            var year = int.Parse(dtString.Substring(0, 4));
            var month = int.Parse(dtString.Substring(4, 2));
            var day = int.Parse(dtString.Substring(6, 2));
            var dt = new DateTime(year, month, day);

            return dt;
        }

        public static SponsorPerks SponsorPerks(VhmSponsorPerk item) {
            return AutoMapper.Mapper.Map<VhmSponsorPerk, SponsorPerks>(item);
        }

        public static Session GetMemberSession(VhmSession session)
        {
            return AutoMapper.Mapper.Map<Session>(session);
        }
        public static VhmSession GetMiddleTierSession (Session session)
        {
            return AutoMapper.Mapper.Map<VhmSession>(session);
        }
        //public static Session GetMemberSession(VhmSession session)
        //{
        //    if (session == null)
        //    {
        //        return new Session();
        //    }
            
        //    var s = new Session
        //    {
        //        Locked = session.Locked, // == 1,
        //        SessionID = session.SessionID,
        //        MemberID = session.MemberID,
        //        LastTryWarning = session.LastTryWarning, // == 1,
        //        LastPasswordChange = session.LastPasswordChange,
        //        NAttemptsLeft = session.NAttemptsLeft.GetValueOrDefault(),
        //        BusinessPartnerAdmin = session.BusinessPartnerAdmin,
        //        BusinessPartnerReport = session.BusinessPartnerReport,
        //        EmployerReport = session.EmployerReport,
        //        EmployerReportAdmin = false,
        //        CommunityId = session.CommunityID,
        //        CommunitiyOn = (session.CommunityOn == 1),
        //        CommunitityType = session.CommunityType,
        //        Culture = session.Culture,
        //        // TODO: Add isConnectedToRunKeeper value
        //        // (session.isConnectedToRunKeeper !=null) ? session.IsConnectedToRunKeeper : false
        //        LanguageID = session.LanguageID == string.Empty ? "1033" : session.LanguageID, //int.Parse((() ??  "1033")),
        //        SitePermissions = new SitePermissions
        //        {
        //            Member = (session.Permission != null) && session.Permission.Member,
        //            MeasurementAssessor = (session.Permission != null) && session.Permission.MeasurementAssessor,
        //            HealthZoneAdmin = (session.Permission != null) && session.Permission.HealthZoneAdmin,
        //            LifeZoneMasterLogin = (session.Permission != null) && session.Permission.LifeZoneMasterLogin,
        //            OnlineReporting = (session.Permission != null) && session.Permission.OnlineReporting,
        //            VoucherReceiver = (session.Permission != null) && session.Permission.VoucherReceiver,
        //            ChallengeManager = (session.Permission != null) && session.Permission.ChallengeManager,
        //            ECoach = (session.Permission != null) && session.Permission.ECoach,
        //            CrmAdmin = (session.Permission != null) && session.Permission.CrmAdmin
        //        },
        //        PrimarySponsorID = session.PrimarySponsorID,
        //        AdultAge = session.AdultAge,
        //        Membership = session.Membership,
        //        SponsorPerks = session.SponsorPerks, // SponsorPerks(session.SponsorPerks.FirstOrDefault()),
        //        //MemberStatus = session.MemberStatus,
        //        IWPMemberSince = session.IWPMemberSince,
        //        //HealthSnapshot = session.HealthSnapshot,
        //        HSPopupReminder = session.HSPopupReminder
        //    };


        //    return s;
        //}
    }
}
