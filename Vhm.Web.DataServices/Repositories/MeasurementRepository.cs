﻿using Vhm.Web.DataServices.MiddleTierService;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.DataServices.Repositories {
    public class MeasurementRepository {

        public static MeasurementReward MeasurementReward(VhmMemberLatestMeasureReward item) {
            var newItem = AutoMapper.Mapper.Map<MeasurementReward>(item);
            return newItem;
        }
    }
}
