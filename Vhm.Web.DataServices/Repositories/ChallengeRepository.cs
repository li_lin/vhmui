﻿using Vhm.Web.DataServices.MiddleTierActivityService;
using Vhm.Web.Models.RepositoryModels;
using Vhm.Web.Models.ViewModels.Challenges;

namespace Vhm.Web.DataServices.Repositories
{
    public class ChallengeRepository
    {
        public static ChallengeMessage ChallengeMessage( VhmChallengeMessage item )
        {
            return AutoMapper.Mapper.Map<ChallengeMessage>(item);
        }
        public static ChallengeScore ChallengeScore(VhmChallengeScore item)
        {
            return AutoMapper.Mapper.Map<ChallengeScore>(item);
        }
        public static VhmChallengeScore ChallengeScore(ChallengeScore item)
        {
            return AutoMapper.Mapper.Map<VhmChallengeScore>(item);
        }
        public static ChallengeMember ChallengeMember(VhmChallengeMember item)
        {
            return AutoMapper.Mapper.Map<ChallengeMember>(item);
        }
        public static VhmChallengeMember VhmChallengeMember(ChallengeMember item)
        {
            return AutoMapper.Mapper.Map<VhmChallengeMember>(item);
        }
        public static VhmChallengeInviteDetail VhmChallengeInviteDetail(ChallengeInvite item)
        {
            return AutoMapper.Mapper.Map<VhmChallengeInviteDetail>(item);
        }
        public static ChallengeMemberScore ChallengeMemberScore(VhmChallengeMemberScore item)
        {
            return AutoMapper.Mapper.Map<ChallengeMemberScore>(item);
        }
        
        public static PersonalTrackingChallenge PersonalTrackingChallenge(VhmChallenge item)
        {
            return AutoMapper.Mapper.Map<PersonalTrackingChallenge>(item);
        }

        public static VhmChallenge VhmChallenge(PersonalTrackingChallenge item)
        {
            return AutoMapper.Mapper.Map<VhmChallenge>(item);
        }

        public static VhmMemberAvailableChallengesByType VhmMemberAvailableChallengesByType(MemberAvailableChallengesByType item )
        {
            return AutoMapper.Mapper.Map<VhmMemberAvailableChallengesByType>( item );
        }

        public static MemberAvailableChallengesByType MemberAvailableChallengesByType(VhmMemberAvailableChallengesByType item )
        {
            return AutoMapper.Mapper.Map<MemberAvailableChallengesByType>( item );
        }

        public static ChallengeQuestion ChallengeQuestion( VhmChallengeQuestion item )
        {
            return AutoMapper.Mapper.Map<ChallengeQuestion>(item); 
        }

        public static SponsorSystemBadge SponsorSystemBadge( VhmSponsorSystemBadge item )
        {
            return AutoMapper.Mapper.Map<SponsorSystemBadge>(item); 
        }
    }
}
