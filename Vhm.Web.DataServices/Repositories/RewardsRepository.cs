﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Vhm.Web.Models;
using Vhm.Web.DataServices.MiddleTierService;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.DataServices.Repositories
{
    class RewardsRepository
    {

        public static Game Game(VhmGameInfo item) {
            var newItem = AutoMapper.Mapper.Map<Game>(item);
            return newItem;
        }

        public static Reward Reward(VhmGameReward item)
        {
            var newItem = AutoMapper.Mapper.Map<Reward>( item );
            return newItem;
        }
        public static UpForGrabsItem UpForGrabsItem (VhmUpForGrabsReward item)
        {
            return AutoMapper.Mapper.Map<UpForGrabsItem>( item );
        }

        public static GameCurrency GameCurrency( VhmGameCurrency item )
        {
            return AutoMapper.Mapper.Map<GameCurrency>(item);
        }

        public static GameTrigger GameTrigger(VhmGameTrigger item)
        {
            return AutoMapper.Mapper.Map<GameTrigger>( item );
        } 

        public static MemberLevelsAudit MemberLevelsAudit(VhmMemberLevelsAudit levelsAudits)
        {
            return AutoMapper.Mapper.Map<MemberLevelsAudit>(levelsAudits);
        }

        public static BadgeItem Badge(MiddleTierService.RewardBadge item)
        {
            return AutoMapper.Mapper.Map<BadgeItem>(item);
        }

        public static Models.RepositoryModels.RewardBadge RewardBadge(MiddleTierService.RewardBadge item)
        {
            return AutoMapper.Mapper.Map<Models.RepositoryModels.RewardBadge>(item);
        }
    }
}
