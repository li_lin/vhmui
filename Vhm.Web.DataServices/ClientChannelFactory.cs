﻿using System.ServiceModel;

namespace Vhm.Web.DataServices {
    
    internal class ClientChannelFactory<TChannel> : IClientChannelFactory<TChannel>
        where TChannel : IClientChannel {
        private ChannelFactory<TChannel> factory;

        public ClientChannelFactory(string endpointConfigurationName)
        {
            this.factory = new ChannelFactory<TChannel>(endpointConfigurationName);
        }

        
        public void Open()
        {
            this.factory.Open();
        }
        public void Close()
        {
            this.factory.Close();
        }
        public void Abort()
        {
            this.factory.Abort();
        }
        public TChannel CreateChannel()
        {
            return this.factory.CreateChannel();
        }
    }
}
