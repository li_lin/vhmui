﻿using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.DataServices.Interfaces {
    public interface IMeasurementsAdapter
    {
        MeasurementReward GetMemberLatestMeasurementSummary(long memberID, int? maxResults);
        bool UpdateMemberLatestMeasurementSummary( long memberId, int? maxResults );
    }
}
