﻿using System;
using System.Collections.Generic;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.DataServices.Interfaces
{
    public interface IMemberAdapter
    {
        bool CheckForRegisteredDevice( long memberId );
        bool SaveMemberPrivateSettings(string sessionId, Member member, bool deletePictureProfile);
        List<MemberGoalsAndInterests> GetMemberGoalsAndInterests(string sessionId, long memberId);
        List<RecentlyEarnedRewardsItem> GetRecentlyEarnedRewards(long memberId, int maxRewards, bool? measurmentRewardsOnly, bool ? badgesOnly);
        Dictionary<T, string> GetContentLabels<T>(long pageId, int languageId);
        List<ChallengeBase> GetMemberChallenges(long memberId, long sponsorId, byte displayDays, int languageId);
        List<ChallengeBase> GetCorporateTrackingChallenges(long memberId, long sponsorId, byte displayDays, int languageId);
        ActivityGraphData GetMemberActivityGraphData(long memberId, DateTime startDate, int daysToShow);
        bool BadgesEnabledForSponsor(long sponsorId);
        Member GetMember(string sessionId, long memberId);
        List<MemberEmail> GetMembersByEmailList( List<string> emailList );
        List<MemberModule> GetMemberModules(long memberId, long sponsorId);
        byte? GetMemberWelcomeJourneyCurrentStep(long memberId);
        bool SaveMemberWelcomeJourneyCurrentStep(long memberId, byte currentStep);
        List<CultureFormat> GetCultureFormats();
        List<Terms> GetTerms(long memberId, string termsCode, int version);
        List<Terms> GetTermsAndConditions(long memberId);
        List<Terms> GetTermsToAcknowledge(long memberId);
        bool UpdateAcknowledgeMemberTerms(string sessionID, int timeOut, string termsCode, bool accepted, string source, long memberID);
        MemberPersonalInformation GetMemberPersonalInformation(long memberId);
        bool SaveMemberPersonalInformation(MemberPersonalInformation member);
        decimal InsertMeasurementAssessmentGrouping(Int16 assessmentGroupingTypeID);

        Int64 InsertMeasurement(Int64? assessmentGroupingID, Int16 measurementTypeID, Int64 performedByEntityID,
                                 Int64 memberEntityID, Int16 statusID, Int32 sourceID, double? unSystemValue,
                                 double? unUserValue, Int64? groupID, DateTime udTimeZoneDate, bool? validated,
                                 byte? dataCollectionMethodID, Int64? sourceDataReferenceID);
        bool SaveEmailPromptFlag(Int64 entityID, DateTime? hzAccess, DateTime? hcAccess, DateTime? faDate,
                                  DateTime? questStart, DateTime? logExer, DateTime? exeProg, DateTime? completedReg,
                                  DateTime? stepsLastUploaded, DateTime? noSmokingAgreement, DateTime? measurementsTaken);

        void SaveKagEntry(long promoId, long memberId, decimal score, DateTime date);
        List<Voucher> CheckVoucherByCode(string voucherCode);
        bool RedeemRewardsVoucher(Voucher voucher, long memberId);
        Tickers GetTickers(long memberId, long sponsorId);
        DateTime GetMemberNow(long memberId, DateTime systemDate);
        DateTime GetMemberTimeByUtcTime(long memberId, DateTime utcTime);
        bool UpdateDoNotRemindHRAOption( long memberId );
        bool UpdateLastHraDate( long memberId );
        List<BadWords> GetBadWords(int? badWordId);
        List<SmokingInfo> GetSmokingInfo(long memberId, DateTime anniversaryDate);
        List<MemberSegmentation> GetMemberSegmentation(long memberId);
        List<EktronMenuItem> GetEktronMenu( long menuId, int languageId );
        List<Lookup> GetLookup( string lookupType );
        List<MemberBadge> GetMemberBadges( long memberId, long? communityId, bool? celebrationOnly );
        bool SaveRewardedTaskCelebration( long memberId, long? communityId, bool? celebrationOnly );
        List<MemberDeviceOrder> GetMemberDeviceOrders(long memberId);
        List<byte[]> GetMemberPasswordHistory(long memberId, int recentPasswordsCount);
    }
}
