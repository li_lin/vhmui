﻿using System;
using System.Collections.Generic;
using Vhm.Web.Models.Enums;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.DataServices.Interfaces
{
    public interface IRewardsAdapter
    {
        List<UpForGrabsItem> GetMemberAvailableUpForGrabs(long memberId);
        Game GetSponsorCurrentGame(long memberId, long sponsorId );
        List<MemberLevelsAudit> GetMemberLevelsAudit(long memberId, long sponsorId);
        BadgeItem GetBadges(long badgeId);
        List<GameTrigger> GetGameTriggers( long gameId );
        List<GameTrigger> GetSystemTriggers(long memberId, long sponsorId);
        MemberRewardsData GetMemberRewardsData( long memberId );
        MemberRewardsBalance GetMemberRewardsBalnace( long memberId );
        bool TriggerReward( long memberId, string rewardCode, bool processNow, bool surpressResult, DateTime triggerDate, long? challengeTaskId);
        List<RewardBadge> GetBadgesByType( BadgeType type );
        List<GameCurrency> GetGameCurrencies();
    }
}
