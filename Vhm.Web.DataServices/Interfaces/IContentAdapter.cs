﻿using System;
using System.Collections.Generic;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.DataServices.Interfaces
{
    public interface IContentAdapter
    {
        EktronRichContent GetEktronRichContent(string contentAreaType, long sponsorId, int languageId);

        List<EktronRichContent> GetSponsorLatestBlogPosts(string contentAreaType, long sponsorId, int languageId, int count);

        EktronRichContent GetBlogPostById(long contentId, int languageId);
    }
}
