﻿
using System.Collections.Generic;
using Vhm.Web.DataServices.Adapters;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.DataServices.Interfaces {
    public interface ISponsorAdapter
    {
        SponsorProfile GetSponsorProfile( long sponsorId );
        List<ColorTheme> GetAvailableSponsorColorThemes( long sponsorId );
        List<SponsorLanguage> GetSponsorLanguages( long sponsorId );
        List<PasswordRegEx> GetPasswordRegExValues();
        List<SponsorModule> GetSponsorModules(long sponsorId);
        List<SponsorModuleGZVersion> GetSponsorModuleGZVersions(long sponsorModuleId);
        List<RewardsProgram> GetSponsorPrograms( long sponsorId );
    }
}
