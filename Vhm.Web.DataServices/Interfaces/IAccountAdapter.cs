﻿using System.Collections.Generic;
using Vhm.Web.Models;

namespace Vhm.Web.DataServices.Interfaces
{
    public interface IAccountAdapter
    {
        void KillSession( string sessionId );

        Session GetNewSession(string userName, string password, string system  ,int timeout);

        Session GetMemberIdBySession(string sessionId);

        Session GetSession(string sessionId);

        bool UpdateSession( Session session, int timeOut );
    }
}
