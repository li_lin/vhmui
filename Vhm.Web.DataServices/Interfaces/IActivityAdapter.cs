﻿using Vhm.Web.DataServices.MiddleTierActivityService;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.DataServices.Interfaces
{
    public interface IActivityAdapter
    {
        bool AddMemberRunkeeperToken(long memberID, string code);
        bool IsMemberConnectedToRunKeeper(long memberID);
        bool DeleteMemberRunkeeperToken(long memberID);
        bool SyncRunkeeperData(long memberID);
        bool AddMemberFitbitTokens(long memberID, FitbitAuthInfo code);
        bool IsMemberConnectedToFitbit(long memberID);
        bool DeleteMemberFitbitTokens(long memberID);
        bool IsMemberRegisterToGZ(long memberID);
        bool IsMemberLinkedToPolar( long memberID );
        MemberDevice GetMemberExistingGoZoneDevice( long memberId );
    }
}
