﻿using System.Collections.Generic;
using Vhm.Web.Models.Enums;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.DataServices.Interfaces
{
    public interface ISocialAdapter
    {
        SocialPagedData GetGroupMembers(string sessionId, long groupId, int? pageNumber, int? count, bool includeImage);

        SocialPagedData GetMemberFriends(string sessionId, long memberId, int? pageNumber, int? count);
        #region Groups

        /// <summary></summary>
        /// <param name="sessionId"></param>
        /// <param name="memberId"> </param>
        /// <param name="pageNumber"> </param>
        /// <param name="count"> </param>
        /// <returns> Returns a list of all active member's groups (groups member is part of, administrating or have created)</returns>
        SocialPagedData GetMemberGroups(string sessionId, long memberId, List<GroupTypes> types, int? pageNumber, int? count, bool includeImages);
        List<Group> GetMemberGroupInvitations (long nMemberId);

        SocialPagedData GetMemberSuggestedGroups(string sessionId, int? pageNumber, int? count, bool includePic);

        List<GroupCategory> GetAllGroupCategories (string sessionId);

        List<MemberNotification> GetMemberNotifications(string sessionId, long memberId);
        int GetMemberWaitingForActionNotificationCount(long memberId);

        Member GetMemberProfile(string sessionId, long memberId, bool includeImage, long? currentProfileId);

        Group GetGroupDetails ( string sessionId, long groupId );
        long AddMemberToGroup (string sessionId, Group groupToAdd, long nMemberId);
        SocialPagedData SearchGroupByName(string sessionId, string groupName, GroupTypes groupType, int? pageNumber, int? count);
        SocialPagedData SearchGroupByAdminName(string sessionId, string adminName, GroupTypes groupType, int? pageNumber, int? count);
        SocialPagedData SearchGroupsByCategory(string sessionId, GroupCategories categoryId, GroupTypes groupType, int? pageNumber, int? count);
        bool IsMemberPartOfGroup(long memberId, long groupId);
        bool DeclineNotification(string sessionId, MemberNotification notification);
        List<string> GetMemberGroupNames(string sessionId, long memberId, GroupTypes grpType);
        List<string> GetMemberFriendNames (string sessionId, long memberId);
        #endregion

        List<Member> GetMembersById(string sessionId, List<long> memberIds);

        List<Member> GetMembersByEmailsOrIdentityNumbers(string sessionId, IEnumerable<string> emails, IEnumerable<string> identiyNumbers);

        byte[] GetMemberImage(long id);

        byte[] GetGroupImage(long id);

        bool DeleteGroup(string sessionId,Group  group);

        long AddGroup(string sessionId, Group group);

        bool UpdateGroup(string sessionId, Group group);

        bool AddMemberNotificationList(string sessionId, List<long> mIds, MemberNotification notif);

        bool IsGroupNameAvalibale(string groupName);

        List<Occupation> GetOccupations();

        List<NewsfeedPost> GetMemberNewsfeed(long memberId, long communityId, int commentCount);

        void LikeNewsfeedPost(string sessionId, long memberId, long postById, long postId, long groupId);

        MemberRelationships GetMemberRelationship( string sessionId, long friendId );

        string GetVhmContent(long sponsorId, long memberId, string contentAreaId, int languageId );

        string GetSponsorContentForMember( long memberId );

        bool UpdateNotification( string sessionId, MemberNotification notification );

        bool SumbitMemberPost(string sessionId, long memberId, string postText, long groupId, long communityId, int newsFeedTypeId);

        bool SumbitMemberPost( string sessionId, long memberId, string postText, long groupId, long communityId, NewsFeedPostType typeCode );

        string GetWeeklyStatusUpdatePrompt(string sessionId, int weekNumber);

        int GetMemberFriendCount( long memberId );

        int GetMemberGroupCount( long memberId );

        List<MemberProfile> SearchFriendsByName( long memberId, string name );
    }
}
