﻿using System;
using System.Collections.Generic;
using Vhm.Web.Models.RepositoryModels;
using Vhm.Web.Models.ViewModels.Challenges;

namespace Vhm.Web.DataServices.Interfaces
{
    public interface IChallengeAdapter
    {
        List<ChallengeMessage> GetChallengeMessages( long challengeId,  bool deleted);
        List<ChallengeMessage> GetChallengeMessagesForTeam( long challengeId, long teamId, bool deleted );
        void SaveChallengeMessage( long challengeId, long? teamId, long memberId, string nickName, string messageText );
        PersonalTrackingChallenge GetChallenge(long challengeId);
        List<ChallengeScore> GetTrackedDays( long challengeId, long memberId );
        long AddChallengeScore(ChallengeScore challengeScore);
        bool UpdateChallengeScore(ChallengeScore challengeScore);
        List<ChallengeMemberScore> GetChallengeLeaders( long challengeId, int pageNumber, int pageSize );
        ChallengeMemberScore GetChallengeMember(long memberId, long challengeId);
        long SaveChallenge(PersonalTrackingChallenge newChallenge, long memberId);
        List<SponsorSystemBadge> GetChallengeBadges( long sponsorId, string category );
        long AddChallengeMember( ChallengeMember member );
        bool AddChallengeInvite(long challengeId, string toIds, string emailAddresses, bool sent,
                                       DateTime? dateSent, bool isDeclined, bool doNotRemind, decimal handicap,
                                       string source, long? groupId, long memberId);
        void AddChallengeInvitesForAllFriends(long challengeId, long challengeCreator, string source);
        bool SendChallengeInvite( long challengeId, string personalMessage );
        bool RemoveCacheByKey(string key);

        List<MemberAvailableChallengesByType> GetMemberAvailableChallengesByType( long memberId, string challengeTypes,
            DateTime systemTimeNow );

        List<PersonalTrackingChallenge> GetChallenges( long[] challengeIds );
    }
}
