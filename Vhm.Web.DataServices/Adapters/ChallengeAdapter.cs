﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vhm.Web.DataServices.Interfaces;
using Vhm.Web.DataServices.MiddleTierActivityService;
using Vhm.Web.DataServices.Repositories;
using Vhm.Web.Models.RepositoryModels;
using Vhm.Web.Models.ViewModels.Challenges;

namespace Vhm.Web.DataServices.Adapters
{
    public class ChallengeAdapter : BaseActivityAdapter, IChallengeAdapter
    {
        public List<ChallengeMessage> GetChallengeMessages(  long challengeId, bool deleted )
        {
            var messages = _activityProxy.GetChallengeMessages(  challengeId, deleted ).ToList();
            return messages.Select( ChallengeRepository.ChallengeMessage ).ToList();
        }

        public List<ChallengeMessage> GetChallengeMessagesForTeam( long challengeId, long teamId, bool deleted )
        {
            var messages = _activityProxy.GetChallengeMessagesForTeam(challengeId, teamId, deleted).ToList();
            return messages.Select(ChallengeRepository.ChallengeMessage).ToList();
        }

        public void SaveChallengeMessage( long challengeId, long? teamId, long memberId, string nickName, string messageText )
        {
            var newMessage = new VhmChallengeMessage
                {
                    ChallengeID = challengeId,
                    NickName = nickName,
                    MemberID = memberId,
                    TeamID = teamId,
                    FilteredMessageText = messageText
                };
            _activityProxy.SaveChallengeMessage(newMessage);
        }

        public PersonalTrackingChallenge GetChallenge(long challengeId)
        {
            var challenge = _activityProxy.GetChallenge( new[] {challengeId} ).SingleOrDefault();
            return ChallengeRepository.PersonalTrackingChallenge( challenge );
        }

        public List<ChallengeScore> GetTrackedDays(long challengeId, long memberId)
        {
            var messages = _activityProxy.GetChallengeScores( memberId, challengeId).ToList();
            return messages.Select( ChallengeRepository.ChallengeScore ).ToList();
        }

        public ChallengeMemberScore GetChallengeMember(long memberId, long challengeId)
        {
            var score = _activityProxy.GetChallengeMemberRank(memberId, challengeId);
            return ChallengeRepository.ChallengeMemberScore(score);
        }
        public List<ChallengeMemberScore> GetChallengeLeaders(long challengeId, int pageNumber, int pageSize)
        {
            var leaders = _activityProxy.GetChallengeMemberScore( challengeId, pageNumber, pageSize ).ToList();
            return leaders.Select(ChallengeRepository.ChallengeMemberScore).ToList();
        } 

        public long AddChallengeScore(ChallengeScore challengeScore)
        {
            var messages = _activityProxy.AddChallengeScore(ChallengeRepository.ChallengeScore( challengeScore ));
            return messages;
        }
        public bool UpdateChallengeScore(ChallengeScore challengeScore)
        {
            return _activityProxy.UpdateChallengeScore( ChallengeRepository.ChallengeScore( challengeScore ) );
        }
        public bool RemoveCacheByKey(string key)
        {
            return _proxy.RemoveCacheByKey(key);
        }
        public bool SendChallengeInvite(long challengeId, string personalMessage)
        {
            return _activityProxy.SendChallengeInvite(challengeId, personalMessage);
        }
        public long SaveChallenge(PersonalTrackingChallenge newChallenge, long memberId)
        {

            var challenge = ChallengeRepository.VhmChallenge( newChallenge );

            var id = _activityProxy.AddChallenge(challenge, memberId);
            return id ;
        }

        public long AddChallengeMember( ChallengeMember member )
        {
            var challengeMember = ChallengeRepository.VhmChallengeMember( member );
            var id = _activityProxy.AddChallengeMember(challengeMember);
            return id;
        }

        public bool AddChallengeInvite(long challengeId, string toIds, string emailAddresses, bool sent,
                                       DateTime? dateSent, bool isDeclined, bool doNotRemind, decimal handicap,
                                       string source, long? groupId, long memberId)
        {
            var ok = _activityProxy.AddChallengeInvite(challengeId, toIds, emailAddresses, sent, dateSent, isDeclined,
                                                       doNotRemind, handicap, source, groupId, memberId);
            return ok;
        }

        public void AddChallengeInvitesForAllFriends(long challengeId, long challengeCreator, string source)
        {
            _activityProxy.AddChallengeInviteForAllFriends(challengeId, challengeCreator, challengeCreator, source);
        }

        public List<SponsorSystemBadge> GetChallengeBadges(long sponsorId, string category )
        {
            var badges = _activityProxy.GetSponsorSystemBadgesByCategory( sponsorId, category );

            return badges.Select( ChallengeRepository.SponsorSystemBadge ).ToList();
        }

        public List<MemberAvailableChallengesByType> GetMemberAvailableChallengesByType(long memberId, string challengeTypes, DateTime systemTimeNow )
        {
            var challenges = _activityProxy.GetMemberAvailableChallengesByType( memberId, challengeTypes, systemTimeNow );
            return challenges.Select( ChallengeRepository.MemberAvailableChallengesByType ).ToList();
        }

        public List<PersonalTrackingChallenge> GetChallenges(long[] challengeIds)
        {
            if ( challengeIds.Length == 0 )
            {
                return null;
            }

            var challenges = _activityProxy.GetChallenge(challengeIds);
            if (challenges == null)
            {
                return null;
            }
            return challenges.Select(ChallengeRepository.PersonalTrackingChallenge).ToList();
        }

    }
}
