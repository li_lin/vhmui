﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.Web;
using AutoMapper;
using Vhm.Web.DataServices.ContentService;
using Vhm.Web.DataServices.Interfaces;
using Vhm.Web.DataServices.MiddleTierService;
using Vhm.Web.DataServices.Repositories;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.DataServices.Adapters
{
    public class ContentAdapter : BaseAdapter, IContentAdapter
    {
        public EktronRichContent GetEktronRichContent(string contentAreaType, long sponsorId, int languageId)
        {
            EktronRichContent returnContent = new EktronRichContent();
            VhmContent vhmContent = new VhmContent();

            var ektronService = new VhmContentServiceClient("WSHttpBinding_IVhmContentService");
            vhmContent = ektronService.GetRichContent(contentAreaType, sponsorId, languageId);

            returnContent.Title = vhmContent.Title;
            returnContent.Html = vhmContent.Html;

            return returnContent;
        }

        public List<EktronRichContent> GetSponsorLatestBlogPosts(string contentAreaType, long sponsorId, int languageId, int count)
        {
            List<EktronRichContent> returnContentList = new List<EktronRichContent>();
            List<VhmContent> vhmContentList = new List<VhmContent>();

            var ektronService = new VhmContentServiceClient("WSHttpBinding_IVhmContentService");
            vhmContentList = ektronService.GetSponsorLatestBlogPosts(contentAreaType, sponsorId, languageId, count).ToList();

            if(vhmContentList != null && vhmContentList.Any())
            {
                foreach (VhmContent item in vhmContentList)
                {
                    EktronRichContent listContent = new EktronRichContent();
                    listContent.Title = item.Title;
                    listContent.Html = item.Html;
                    listContent.Id = item.Id;
                    listContent.DateCreated = item.DateCreated;

                    returnContentList.Add(listContent);
                }
                
            }
            return returnContentList;
        }

        public EktronRichContent GetBlogPostById(long contentId, int languageId)
        {
            EktronRichContent returnContent = new EktronRichContent();
            VhmContent vhmContent = new VhmContent();

            var ektronService = new VhmContentServiceClient("WSHttpBinding_IVhmContentService");
            vhmContent = ektronService.GetBlogPostById(contentId, languageId);

            returnContent.Title = vhmContent.Title;
            returnContent.Html = vhmContent.Html;
            returnContent.Id = vhmContent.Id;
            returnContent.DateCreated = vhmContent.DateCreated;

            return returnContent;
        }
    }
}
