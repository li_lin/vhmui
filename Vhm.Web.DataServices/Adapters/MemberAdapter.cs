﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.Web;
using AutoMapper;
using Vhm.Web.DataServices.ContentService;
using Vhm.Web.DataServices.Interfaces;
using Vhm.Web.DataServices.MiddleTierService;
using Vhm.Web.DataServices.Repositories;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.DataServices.Adapters
{
    public class MemberAdapter : BaseActivityAdapter, IMemberAdapter
    {
        public bool SaveMemberPrivateSettings(string sessionId, Member member, bool deletePictureProfile)
        {
            var socMember = MemberRepository.GetSocMemberProfile( member );
            return _proxy.UpdateProfile(sessionId, socMember, deletePictureProfile);
        }

        public List<MemberGoalsAndInterests> GetMemberGoalsAndInterests(string sessionId, long memberId)
        {
            List<GoalsAndInterests> lst = _proxy.GetMemberGoalsAndInterests(sessionId, memberId);
            var newLst = new List<MemberGoalsAndInterests>();
            foreach (var goalsAndInterestse in lst)
            {
                newLst.Add(MemberRepository.MemberGoalsAndInterests(goalsAndInterestse));
            }
            return newLst;
        }

        public Member GetMember(string sessionId, long memberId)
        {
            var member = _proxy.GetMemberById(sessionId, memberId);
            return MemberRepository.Member(member);
        }

        public List<MemberEmail> GetMembersByEmailList( List<string> emailList )
        {
            var memberEmails = _proxy.GetMembersByEmailList( emailList );
            return memberEmails.Select(MemberRepository.MemberEmail).ToList();
        }

        public List<RecentlyEarnedRewardsItem> GetRecentlyEarnedRewards(long memberId, int maxRewards, bool? measurmentRewardsOnly, bool ? badgesOnly)
        {
            var rewards = _proxy.GetMemberRecentRewards(memberId, maxRewards, measurmentRewardsOnly, badgesOnly);
            return rewards.Select(MemberRepository.RewardItem).ToList();
        }

        public List<ChallengeBase> GetCorporateTrackingChallenges(long memberId, long sponsorId,
                                                                               byte displayDays, int languageId )
        {
            var vhmChallenges = _proxy.GetMemberChallengeMenu(memberId, sponsorId, displayDays);
            var challenges = vhmChallenges.Where(c => c.DataSource == ChallengeBase.CHALLENGE_TYPE_CORP && c.ShowInBrowseOnly.GetValueOrDefault()).ToList();

            var allChallenges = new List<ChallengeBase>();
            allChallenges.AddRange(challenges.Select(MemberRepository.Challenge));

            return allChallenges;
        }

        public List<ChallengeBase> GetMemberChallenges(long memberId, long sponsorId, byte displayDays, int languageId)
        {

            var vhmChallenges = _proxy.GetMemberChallengeMenu(memberId, sponsorId, displayDays);

            var challenges = vhmChallenges.Where(c => c.DataSource == ChallengeBase.CHALLENGE_TYPE_CORP && c.Type != ChallengeBase.CHALLENGE_TYPE_TRK).ToList();
            var quests = vhmChallenges.Where(c => c.DataSource == ChallengeBase.CHALLENGE_TYPE_QUEST).ToList();
            var vhmpromos = vhmChallenges.Where(c => c.DataSource == ChallengeBase.CHALLENGE_TYPE_PROMO && c.Type != ChallengeBase.CHALLENGE_TYPE_KAG).ToList();
            var vhmkags = vhmChallenges.Where(c => c.Type == ChallengeBase.CHALLENGE_TYPE_KAG).ToList();
            var trackingChallenges = vhmChallenges.Where(c => c.Type == ChallengeBase.CHALLENGE_TYPE_TRK).ToList();

            if (challenges.Count > 0)
            {
                var notEnrolledChallenges = challenges.Where(vhmMemberChallenge => !vhmMemberChallenge.IsMemberEnrolled);
                foreach (var vhmMemberChallenge in notEnrolledChallenges)
                {
                    vhmMemberChallenge.DataSource = ChallengeBase.CHALLENGE_TYPE_CORP_SIGN_UP; //we need to make sure to redirect member to invite page first
                }
            }
            
            #region switch corporate challenge name
            using (var ektronService = new VhmContentServiceClient("WSHttpBinding_IVhmContentService"))
            {
                foreach (var challenge in challenges)
                {
                    CorporateChallengeContent vhmContent = ektronService.GetCorporateChallenge(challenge.PKID, languageId);
                    if (vhmContent != null)
                    {
                        challenge.Name = vhmContent.ChallengeName;
                    }
                }

                foreach (var quest in quests)
                {
                    QuestContent vhmContent = ektronService.GetQuest(quest.PKID, languageId);
                    if(vhmContent != null)
                    {
                        quest.Name = vhmContent.Name;
                    }
                }

                foreach (var promo in vhmpromos)
                {
                    PromoContent vhmContent = ektronService.GetPromo(promo.PKID, languageId);
                    if(vhmContent != null)
                    {
                        promo.Name = vhmContent.PromoName;
                    }
                }

                foreach (var vhmkag in vhmkags)
                {
                    KnowAndGoContent vhmContent = ektronService.GetKnowAndGo(vhmkag.PKID, languageId);
                    if(vhmContent != null)
                    {
                        vhmkag.Name = vhmContent.Name;
                    }
                }
            }
            #endregion

            List<TrackingChallenge> trks = trackingChallenges.Select( MemberRepository.TrackingChallenge ).ToList();
            foreach ( var trk in trks )
            {
                var trackingChallenge = _activityProxy.GetChallenge( new[] { trk.ChallengeId } ).FirstOrDefault();
                if ( trackingChallenge != null )
                {
                    trk.Name = trackingChallenge.Name;
                    trk.ChallengeQuestion = trackingChallenge.ChallengeQuestions[0].Question;
                }
            }

            var kags = vhmkags.Select(MemberRepository.Kag).ToList();
            // KAG promos have more information to load
            if (kags.Count > 0)
            {
                foreach (Kag kag in kags)
                {
                    // Get this KnowAndGo from the MT
                    var promo = _proxy.GetPromo(kag.ChallengeId, null, null);

                    if (promo != null && promo.PromoID.HasValue && promo.FrequencyTypeID.HasValue && promo.ResponseTypeID.HasValue)
                    {
                        kag.FrequencyType = (Kag.FrequencyTypes)promo.FrequencyTypeID;
                        kag.ResponseType = (Kag.ResponseTypes)promo.ResponseTypeID;
                        kag.ShowPage = !promo.ShowUpForGrabs.GetValueOrDefault();
                        kag.TrackerQuestion = promo.TrackerQuestion;
                        kag.CurrentDate = DateTime.UtcNow;
                        var entries = _proxy.GetMemberPromoEntries(promo.PromoID.Value, memberId);
                        if ( kag.FrequencyType == Kag.FrequencyTypes.Once && entries.Count > 0 )
                        {
                            kag.IsComplete = true;
                        }
                        var thisEntry = (from entry in entries
                                         where entry.EntryDate.Date == DateTime.UtcNow.Date
                                         select entry).OrderBy(e => e.EntryDate).FirstOrDefault();

                        if (thisEntry != null)
                        {
                            kag.CurrentScore = thisEntry.EntryValue;
                        }
                      

                        // Get the promo tasks associated with this KAG                
                        var taskList = _proxy.GetPromoTasks(kag.ChallengeId, null, true);
                        kag.Tasks = taskList.Select(MemberRepository.PromoTask)
                                            .OrderBy(t => t.PromoTaskOrder)
                                            .ToList();


                        var firstKagTask = kag.Tasks.FirstOrDefault();
                        if (firstKagTask == null)
                        {
                            continue;
                        }

                        // get reward image based on reward type
                        kag.RewardImage = firstKagTask.ImageName;

                        // get total rewards. Multiply reward amount x rewards limit. Tasks like 
                        // 'tell a friend' are rewarded multiple times.
                        var totalRewards = (from task in taskList
                                            select task.RewardsAmount * task.RewardLimit).Sum();
                        kag.RewardAmount = totalRewards.GetValueOrDefault();

                        // Get the KAG type from the first task (they must all be the same)
                        kag.TaskType = firstKagTask.PromoTaskTypeID;

                        var lastKagTask = kag.Tasks.LastOrDefault();
                        var startDate = firstKagTask.TaskStartDate.GetValueOrDefault();
                        if (lastKagTask != null)
                        {
                            var endDate = lastKagTask.TaskEndDate.GetValueOrDefault();
                            if (startDate > DateTime.MinValue)
                            {
                                kag.StartDate = startDate;
                            }
                            if (endDate > DateTime.MinValue)
                            {
                                kag.EndDate = endDate;
                            }
                        }
                        var vhmPromoTarget = _proxy.GetPromoTarget(kag.ChallengeId);
                        var promoTarget = MemberRepository.PromoTarget(vhmPromoTarget);
                        kag.Target = promoTarget;
                    }
                    else
                    {
                        kag.Target = new PromoTarget();
                        kag.Tasks = new List<PromoTask>();
                    }
                }
            }

            var allChallenges = new List<ChallengeBase>();
            allChallenges.AddRange(challenges.Select(MemberRepository.Challenge));
            allChallenges.AddRange(quests.Select(MemberRepository.Quest));
            allChallenges.AddRange(vhmpromos.Select(MemberRepository.Promo));
            allChallenges.AddRange(kags);
            allChallenges.AddRange(trks);
            return allChallenges;
        }

        public void SaveKagEntry(long promoId, long memberId, decimal score, DateTime date)
        {
            var newEntry = new VhmMemberPromoEntry
                               {
                                   PromoID = promoId,
                                   MemberID = memberId,
                                   EntryDate = date,
                                   EntryValue = score
                               };
            _proxy.AddMemberPromoEntries(newEntry);

        }

        public bool BadgesEnabledForSponsor(long sponsorId)
        {
            //TODO: Cache the results from this so they can be used elsewhere in the application
            List<VhmSponsorSystemBadge> badges = _proxy.GetSponsorSystemBadges(sponsorId);
            return badges.Any(b => b.Linked.GetValueOrDefault());
        }

        public ActivityGraphData GetMemberActivityGraphData(long memberId, DateTime startDate, int daysToShow)
        {
            var activities = _proxy.GetMemberActivities(memberId, DateTime.Now.AddDays(-daysToShow), startDate);

            var list = activities.lstStep.Select(MemberRepository.ActivityDay).ToList();
            var data = new ActivityGraphData
                           {
                               ActivityDays = list,
                               Target = new decimal(activities.Steps.GetValueOrDefault())
                           };

            return data;
        }

        public Dictionary<T, string> GetContentLabels<T>(long pageId, int languageId)
        { 
            var content = new Dictionary<T, string>();
            try
            {

                var key = (languageId.ToString(CultureInfo.InvariantCulture) + pageId.ToString(CultureInfo.InvariantCulture));
                if (HttpContext.Current.Session != null)
                {
                    if (HttpContext.Current.Session[key] != null)
                    {
                        content = (Dictionary<T, string>)HttpContext.Current.Session[key];
                    }
                }
                if (content == null || content.Count == 0)
                {
                    content = new Dictionary<T, string>();
                    var ektronService = new VhmContentServiceClient("WSHttpBinding_IVhmContentService");
                    var labels = ektronService.GetPageLabels(pageId, languageId);
                    // Get page labels from Ektron
                    // Map the config constants to enum values and assign the content to the matching enum value
                    // in the content dictionary
                    var typeOfT = typeof(T);
                    const string MISSING_CONTENT = "LabelID {0}";
                    foreach (var value in Enum.GetValues(typeOfT))
                    {
                        // get the enum item name
                        var id = (int)Enum.Parse(typeOfT, Enum.GetName(typeOfT, value));

                        string labelText;
                        if (labels != null)
                        {
                            // get the content and add it to the dictionary
                            var label = labels.Labels.SingleOrDefault(l => l.ID == id);

                            // If content is missing or has not been configured, show a static message
                            labelText = label != null ? label.Text : string.Format(MISSING_CONTENT, id);
                        }
                        else
                        {
                            labelText = string.Format(MISSING_CONTENT, id);
                        }
                        content.Add((T)value, labelText);
                    }
                    //Adding content to cach
                    if (HttpContext.Current.Session != null)
                    {
                        HttpContext.Current.Session.Add(key, content);
                    }
                    try
                    {
                        if (ektronService.State != CommunicationState.Faulted)
                        {
                            ektronService.Close();
                        }
                    }
                    finally
                    {
                        if (ektronService.State != CommunicationState.Closed)
                        {
                            ektronService.Abort();
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                log.Error("Error getting Labels for enumeration:" + pageId);
                
            }
            return content;
        }

        public List<MemberModule> GetMemberModules(long memberId, long sponsorId)
        {
            var modules = _proxy.GetMemberModules(memberId, sponsorId);
            return modules.Select(MemberRepository.MemberModule).ToList();
        }

        #region Welcome Journey

        /// <summary>
        /// Used in the welcome Journey on Landing page
        /// </summary>
        /// <param name="memberId"></param>
        /// <returns>will return the current step a new member should see in WJ </returns>
        public byte? GetMemberWelcomeJourneyCurrentStep(long memberId)
        {
            return _proxy.GetMemberWelcomeJourneyStep(memberId);
        }

        /// <summary>
        /// will be used in Welcome Journey for saving the step a member
        /// </summary>
        /// <param name="memberId">not null</param>
        /// <param name="currentStep">not null</param>
        /// <returns>if save is successfull, we'll return currentStep + 1, otherwise -1</returns>
        public bool SaveMemberWelcomeJourneyCurrentStep(long memberId, byte currentStep)
        {
            return _proxy.SaveMemberWelcomeJourneyStep(memberId, currentStep);
        }

        /// <summary>
        /// To be used in Welcome Journey, step one; Manage My account - display settings in the future
        /// </summary>
        /// <returns> a list of all available cultures we have for members to pick from</returns>
        public List<CultureFormat> GetCultureFormats()
        {
            var mtCultures = _proxy.GetCultureFormats();
            return mtCultures.Select(MemberRepository.CultureFormat).ToList();
        }

        public MemberPersonalInformation GetMemberPersonalInformation(long memberId)
        {
            var membPersInfo = _proxy.GetMemberPersonalInformation(memberId);
            var feMembPersInfo = membPersInfo.Select(MemberRepository.MemberPersInfo).ToList();
            return feMembPersInfo.Find(x => x.MemberID == memberId);
        }
        public bool SaveMemberPersonalInformation(MemberPersonalInformation member)
        {
            //var socMember = DataMapper.CopyIntoNewType<VhmMemberPersonalInfo, MemberPersonalInformation>(member);
            var socMember = AutoMapper.Mapper.Map<VhmMemberPersonalInfo>( member );
            return _proxy.SaveMemberPersonalInformation(socMember);
        }
        public decimal InsertMeasurementAssessmentGrouping(Int16 assessmentGroupingTypeID)
        {
            return _proxy.InsertMeasurementAssessmentGrouping(assessmentGroupingTypeID);
        }
        public long InsertMeasurement(long? assessmentGroupingID, Int16 measurementTypeID, long performedByEntityID, long memberEntityID, Int16 statusID, Int32 sourceID, double? unSystemValue, double? unUserValue, long? groupID, DateTime udTimeZoneDate, bool? validated, byte? dataCollectionMethodID, long? sourceDataReferenceID)
        {
            return _proxy.InsertMeasurement(assessmentGroupingID, measurementTypeID, performedByEntityID, memberEntityID, statusID, sourceID, unSystemValue, unUserValue, groupID,
                                             udTimeZoneDate, validated, dataCollectionMethodID, sourceDataReferenceID);
        }
        public bool SaveEmailPromptFlag(long entityID, DateTime? hzAccess, DateTime? hcAccess, DateTime? faDate,
                                  DateTime? questStart, DateTime? logExer, DateTime? exeProg, DateTime? completedReg,
                                  DateTime? stepsLastUploaded, DateTime? noSmokingAgreement, DateTime? measurementsTaken)
        {
            return _proxy.SaveEmailPromptFlag(entityID, hzAccess, hcAccess, faDate, questStart, logExer, exeProg,
                                               completedReg, stepsLastUploaded, noSmokingAgreement, measurementsTaken);
        }
        /// <summary>
        /// use to save the "Do not remind" option selected for HRA options. This will update the Session in database.
        /// </summary>
        /// <param name="memberId"></param>
        /// <returns></returns>
        public bool UpdateDoNotRemindHRAOption(long memberId)
        {
            return _proxy.UpdateMemberDoNotRemindHS(memberId);
        }
        #endregion

        #region Terms and Conditions
        public List<Terms> GetTerms(long memberId, string termsCode, int version)
        {
            var terms = _proxy.GetTerms(termsCode, version, memberId, false);
            return terms.Select(MemberRepository.Terms).ToList();
        }

        public List<Terms> GetTermsAndConditions(long memberId)
        {
            var terms = _proxy.GetTermsAndConditions(memberId);
            return terms.Select(MemberRepository.TermsAndConditions).ToList();
        }
        public List<Terms> GetTermsToAcknowledge(long memberId)
        {
            var terms = _proxy.GetTermsToAcknowledge(memberId);
            return terms.Select(MemberRepository.TermsToAcknowledge).ToList();
        }

        public bool UpdateAcknowledgeMemberTerms(string sessionID, int timeOut, string termsCode, bool accepted, string source, long memberID)
        {
            return _proxy.UpdateAcknowledgeMemberTerms(sessionID, timeOut, termsCode, accepted, source, memberID);
        }

        #endregion
        public DateTime GetMemberNow(long memberId, DateTime systemDate)
        {
            return _proxy.GetMemberTimeBySystemTime(memberId, systemDate);
        }

        public DateTime GetMemberTimeByUtcTime(long memberId, DateTime utcTime)
        {
            return _proxy.GetMemberTimeByUtcTime(memberId, utcTime);
        }
        #region
        public List<Voucher> CheckVoucherByCode(string voucherCode)
        {
            var voucher = _proxy.CheckVoucherByCode(voucherCode);
            return voucher.Select(MemberRepository.MemberVoucher).ToList();
        }
        public bool RedeemRewardsVoucher(Voucher voucher, long memberId)
        {
            VhmMemberRedeemVoucher mtVoucher = MemberRepository.MemberMiddleTierVoucher(voucher);
            return _proxy.RedeemRewardsVoucher(mtVoucher, memberId);
        }

        public Tickers GetTickers(long memberId, long sponsorId)
        {
            var ticker = _proxy.GetTickers(memberId, sponsorId);

            return MemberRepository.Ticker(ticker);
        }

        #endregion

        public bool UpdateLastHraDate(long memberId)
        {
            return _proxy.UpdateLastHraDate(memberId);
        }

        /// <summary>
        /// Get a list of all bad words
        /// </summary>
        /// <param name="badWordId">if null, returns all bad words</param>
        /// <returns>a list of bad words</returns>
        public List<BadWords> GetBadWords(int? badWordId)
        {
            var mtBadWords = _proxy.GetBadWords(badWordId);
            return mtBadWords.Select(MemberRepository.BadWords).ToList();
        }

        public List<SmokingInfo> GetSmokingInfo(long memberId, DateTime anniversaryDate)
        {
            var result = _proxy.GetSmokingInfo(memberId, anniversaryDate);
            return Mapper.Map<List<SmokingInfo>>(result);
        }

        public List<MemberSegmentation> GetMemberSegmentation(long memberId)
        {
            var result = _proxy.GetMemberSegmentations(memberId);
            return Mapper.Map<List<MemberSegmentation>>(result);
        }

        public List<EktronMenuItem> GetEktronMenu(long menuId, int languageId)
        {
            List<EktronMenuItem> returnMenuList = new List<EktronMenuItem>();
            List<VhmEktronMenuItemData> EktronMenuList = new List<VhmEktronMenuItemData>();
            var ektronService = new VhmContentServiceClient("WSHttpBinding_IVhmContentService");
            EktronMenuList = ektronService.GetEktronMenuById(menuId, languageId).ToList();

            foreach (var item in EktronMenuList)
            {
                EktronMenuItem EktmenuItem = new EktronMenuItem();
                EktmenuItem.MenuItemId = item._pageId;
                EktmenuItem.MenuLink = item._pageLink;
                EktmenuItem.MenuLinkTitle = item._pageTitle;
                returnMenuList.Add(EktmenuItem);
            }

            return returnMenuList;
        }

        public List<Models.RepositoryModels.Lookup> GetLookup(string lookupType)
        {
            var items = _proxy.GetLookup(lookupType);
            return items.Select(MemberRepository.Lookup).ToList();
        }

        public List<MemberBadge> GetMemberBadges( long memberId, long? communityId, bool? celebrationOnly )
        {
            var result = _proxy.GetMemberBadges( memberId, communityId, celebrationOnly );
            return Mapper.Map<List<MemberBadge>>( result );
        }

        public bool SaveRewardedTaskCelebration( long memberId, long? communityId, bool? celebrationOnly )
        {
            return _proxy.SaveRewardedTaskCelebration( memberId, communityId, celebrationOnly );
        }

        public List<MemberDeviceOrder> GetMemberDeviceOrders(long memberId)
        {
            var result = _proxy.GetMemberDeviceOrders(memberId);
            return Mapper.Map<List<MemberDeviceOrder>>(result);
        }

        public List<byte[]> GetMemberPasswordHistory(long memberId, int recentPasswordsCount)
        {
            var result = _proxy.GetMemberPasswordHistory(memberId, recentPasswordsCount);
            return result;
        }

        public bool CheckForRegisteredDevice( long memberId )
        {
            return _activityProxy.CheckForRegisteredDevice( memberId );
        }
    }
}
