﻿using Vhm.Web.DataServices.Interfaces;
using Vhm.Web.DataServices.Repositories;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.DataServices.Adapters {
    public class MeasurementsAdapter : BaseAdapter, IMeasurementsAdapter {

        public MeasurementReward GetMemberLatestMeasurementSummary(long memberId, int? maxResults)
        {
            var rewards = _proxy.GetMemberLatestMeasurementSummary( memberId, maxResults );
            return MeasurementRepository.MeasurementReward(rewards);
        }

        public bool UpdateMemberLatestMeasurementSummary(long memberId, int? maxResults)
        {
            return _proxy.UpdateMemberLatestMeasurementSummary( memberId, maxResults );
        }
    }
}
