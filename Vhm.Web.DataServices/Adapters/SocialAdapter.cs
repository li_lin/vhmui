﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vhm.Providers;
using Vhm.Web.Common;
using Vhm.Web.DataServices.Interfaces;
using Vhm.Web.DataServices.MiddleTierService;
using Vhm.Web.DataServices.Repositories;
using Vhm.Web.DataServices.SocialService;
using Vhm.Web.Models.Enums;
using Vhm.Web.Models.InputModels;
using Vhm.Web.Models.RepositoryModels;
using GroupCategory = Vhm.Web.Models.RepositoryModels.GroupCategory;

namespace Vhm.Web.DataServices.Adapters
{
    public class SocialAdapter : BaseAdapter, ISocialAdapter
    {

        public int GetMemberFriendCount(long memberId)
        {
            int count = _proxy.GetMemberFriendsCount(memberId);

            return count;
        }

        public SocialPagedData GetGroupMembers(string sessionId, long groupId, int? pageNumber, int? count, bool includeImage)
        {
            var pagedMembers = _proxy.GetGroupMembers(sessionId, groupId, pageNumber, count, includeImage);
            var members = SocialRepository.GetPagedData(pagedMembers);
            return members;
        }

        public int GetMemberGroupCount(long memberId)
        {
            var groupTypes = new List<EnumGroupType> { EnumGroupType.MEM };
            int count = _proxy.GetMemberGroupsCount(memberId, groupTypes);

            return count;
        }

        public SocialPagedData GetMemberFriends(string sessionId, long memberId, int? pageNumber, int? count)
        {
            var pagedData = _proxy.GetMemberFriends(sessionId, memberId, pageNumber, count, false);

            return SocialRepository.GetPagedData(pagedData);
        }

        public List<GroupCategory> GetAllGroupCategories(string sessionId)
        {
            try
            {
                var mtObject = _proxy.GetGroupCategories(sessionId);
                var groupCategList = new List<GroupCategory>();
                foreach (var socGroupCategory in mtObject)
                {
                    var grpCat = SocialRepository.GetGroupCategory(socGroupCategory);
                    groupCategList.Add(grpCat);

                }
                return groupCategList;
            }
            catch
            {
                return null;
            }
        }

        public int GetMemberWaitingForActionNotificationCount(long memberId)
        {
            return _proxy.GetMemberWaitingForActionNotificationCount( memberId );
        }

        public List<MemberNotification> GetMemberNotifications(string sessionId, long memberId)
        {
            try
            {
                var mtObject = _proxy.GetMemberNotifications(sessionId, memberId);

                return mtObject.Select(SocialRepository.GetMemberNotification).ToList();
            }
            catch
            {
                return null;
            }
        }

        public List<Member> GetMembersByEmailsOrIdentityNumbers(string sessionId, IEnumerable<string> emails, IEnumerable<string> identiyNumbers)
        {
            var lst = new List<Member>();
            List<SocMemberInvite> members = _proxy.GetMembersByEmail(sessionId, emails.ToList(), identiyNumbers.ToList());
            foreach (SocMemberInvite member in members)
            {
                var m = SocialRepository.GetMember(member);
                lst.Add(m);
            }

            return lst;
        }

        public List<Member> GetMembersById(string sessionId, List<long> memberIds)
        {
            var lst = new List<Member>();
            List<VhmMember> members = _proxy.GetMembersById(sessionId, memberIds);
            foreach (VhmMember vhmMember in members)
            {
                var m = SocialRepository.GetMember(vhmMember);
                lst.Add(m);
            }

            return lst;
        }

        public List<Member> GetMemberFriends(long memberId)
        {
            throw new NotImplementedException();
        }

        public SocialPagedData GetMemberGroups(string sessionId, long memberId, List<GroupTypes> types, int? pageNumber, int? count, bool includeImages)
        {
            var dtotypes = from s in types select (EnumGroupType)((int)s);
            PagedData pagedData = _proxy.GetMemberGroups(sessionId, memberId, dtotypes.ToList(), pageNumber, count, includeImages);
            return SocialRepository.GetPagedData(pagedData);
        }

        public List<Group> GetMemberGroupInvitations(long nMemberId)
        {
            throw new NotImplementedException();
        }

        public SocialPagedData GetMemberSuggestedGroups(string sessionId, int? pageNumber, int? count, bool includePic)
        {
            PagedData pagedDataMT = _proxy.GetSuggestedGroups(sessionId, pageNumber, count, includePic);
            return SocialRepository.GetPagedData(pagedDataMT);
        }
        public Group GetGroupDetails(string sessionId, long groupId)
        {
            var socialGroup = _proxy.GetGroupDetails(sessionId, groupId);

            return SocialRepository.GetGroup(socialGroup);
        }
        public byte[] GetMemberImage(long id)
        {
            return _proxy.GetMemberImage(id);
        }

        public byte[] GetGroupImage(long id)
        {
            return _proxy.GetGroupImage(id);
        }
        public Member GetMemberProfile(string sessionId, long memberId, bool includeImage, long? currentMemberId = 0)
        {
            var memberProfileObj = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.MemberProfile) as SocMemberProfile;
            Member member;

            if (memberProfileObj != null)
            {
                if ( memberProfileObj.MemberID == currentMemberId )
                {
                    memberProfileObj = _proxy.GetProfile(sessionId, memberId, includeImage);
                }
                member = SocialRepository.GetMember(memberProfileObj);
            }
            else
            {
                SocMemberProfile mProfile = _proxy.GetProfile(sessionId, memberId, includeImage);
                member = SocialRepository.GetMember(mProfile);
                CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.MemberProfile, mProfile);
            }

            return member;
        }

        public long AddMemberToGroup(string sessionId, Group groupToAdd, long nMemberId)
        {
            var mtSocMemb = SocialRepository.GetSocMemberGroup(groupToAdd, nMemberId);
            long nMemberGroupId = _proxy.AddMemberToGroup(sessionId, mtSocMemb);
            return nMemberGroupId;
        }

        public SocialPagedData SearchGroupByName(string sessionId, string groupName, GroupTypes groupType, int? pageNumber, int? count)
        {
            var dtotypes = (EnumGroupType)((int)groupType);
            var mtSocGroups = _proxy.SearchGroupsByName(sessionId, groupName, dtotypes, pageNumber, count);
            return SocialRepository.GetPagedData(mtSocGroups);
        }                

        public SocialPagedData SearchGroupByAdminName(string sessionId, string adminName, GroupTypes grpTypes, int? pageNumber, int? count)
        {
            var dtotypes = (EnumGroupType)((int)grpTypes);
            var mtSocGroups = _proxy.GetGroupsByAdminName(sessionId, adminName, dtotypes, pageNumber, count);
            return SocialRepository.GetPagedData(mtSocGroups);
        }
        public SocialPagedData SearchGroupsByCategory(string sessionId, GroupCategories categoryId, GroupTypes groupType, int? pageNumber, int? count)
        {
            var dtotypes = (EnumGroupType)((int)groupType);
            var categMT = (EnumGroupCategories)((int)categoryId);
            var mtSocGroups = _proxy.GetGroupsByCategory(sessionId, categMT, dtotypes, pageNumber, count);
            return SocialRepository.GetPagedData(mtSocGroups);
        }
        public bool IsMemberPartOfGroup( long memberId, long groupId)
        {
            return _proxy.IsMemberInGroup( groupId, memberId);
        }
        public bool DeleteGroup(string sessionId, Group group)
        {

            SocGroup socGroup = SocialRepository.GetSocGroup(group);
            return _proxy.DeleteGroup(sessionId, socGroup);
        }
        public long AddGroup(string sessionId, GroupIM groupIM)
        {
            return _proxy.AddGroup( sessionId, new SocGroup
                                                   {
                                                       GroupCategoryID = groupIM.CategoryId,
                                                       GroupName = groupIM.Name
                                                   } );
        }
        public bool Update(string sessionId, Group group)
        {
            return _proxy.UpdateGroup(sessionId, SocialRepository.GetSocGroup(group));
        }
        public bool UpdateGroup(string sessionId, Group group)
        {
            SocGroup gr = SocialRepository.GetSocGroup(group);
            return _proxy.UpdateGroup(sessionId, gr);
        }
        public long AddGroup(string sessionId, Group group)
        {
            SocGroup gr = SocialRepository.GetSocGroup(group);
            return _proxy.AddGroup(sessionId, gr);
        }
        public bool AddMemberNotificationList(string sessionId,List<long> mIds, MemberNotification notif )
        {
            SocMemberNotification notification = SocialRepository.GetSocMemberNotification(notif);
            return _proxy.AddMemberNotificationList(sessionId, mIds, notification);
        }
        public bool DeclineNotification (string sessionId, MemberNotification notification)
        {
            SocMemberNotification socNotification = SocialRepository.GetSocMemberNotification(notification);
            return _proxy.DeclineNotification(sessionId, socNotification);
        }
        public bool UpdateNotification(string sessionId, MemberNotification notification)
        {
            var socMemberNotification = SocialRepository.SocMemberNotification( notification );
            return _proxy.UpdateMemberNotification( sessionId, socMemberNotification );
        }
        public List<string> GetMemberGroupNames (string sessionId, long memberId, GroupTypes grpType)
        {
            var groupType = (EnumGroupType)((int)grpType);
            var listOfNames = _proxy.GetMemberGroupNames(sessionId, memberId, groupType);
            return listOfNames;
        }
        public List<string> GetMemberFriendNames (string sessionId, long memberId)
        {
            return _proxy.GetMemberFriendNames(sessionId, memberId);
        }

        public bool IsGroupNameAvalibale(string groupName)
        {
           return _proxy.GroupNameExists(groupName);
        }

        public List<Occupation> GetOccupations()
        {
            List<MemberOccupation> lst= _proxy.GetOccupations();
            var newLst = new List<Occupation>();
            newLst.AddRange(lst.Select(SocialRepository.Occupation).ToList());

            //foreach (MemberOccupation memberOccupation in lst)
            //{
            //    newLst.Add(DataMapper.CopyIntoNewType<Occupation, MemberOccupation>(memberOccupation));
            //}
            return newLst;
        }

        public void LikeNewsfeedPost(string sessionId, long memberId, long postById, long postId, long groupId)
        {
            var likePost = new SocNewsFeedPost
                               {
                                   NewsFeedPostTypeID = (int) NewsFeedPostType.LikePost,
                                   FromMemberID = memberId,
                                   ToMemberID = postById,
                                   GroupID = groupId,
                                   NewsFeedParentPostID = postId
                               };

            _proxy.AddNewsFeedPost( sessionId, likePost );

            //var notification = new MemberNotification {
            //    MemberId = postById,
            //    NotifiedById = memberId,
            //    NotificationTypeEntityId = postId,
            //    NotificationTypeId = NotificationType.LIKEANEWSFEED,
            //    NotificationStatusID = NotificationStatus.WAIT_FOR_ACTION
            //};

            //var socNotification = SocialRepository.GetSocMemberNotification(notification);
            //_proxy.AddMemberNotification( sessionId, socNotification );

        }

        /// <summary>
        /// submit a post from wall (news feed) or group post/ comment...
        /// </summary>
        /// <param name="sessionId">this should be removed as it is not checked in MT anymore</param>
        /// <param name="memberId">memberId of user making the post = logged in user = can be retrieved from session</param>
        /// <param name="postText">text that will fill in the post/news feed/ comment</param>
        /// <param name="groupId">0 if this is a wall news feed/ group id if this is a group post</param>
        /// <param name="communityId"></param>
        /// <param name="newsFeedTypeId">type of the feed - MemberPostedInGroup/ManualPost/Comment and so on </param>
        /// <param name="typeCode">Newsfeed Type Code</param>
        /// <returns>true if insert + trigger the reward for first post was successful, false otherwise</returns>
        public bool SumbitMemberPost(string sessionId, long memberId, string postText, long groupId, long communityId, NewsFeedPostType typeCode)
        {
            List<SocNewsFeedPostType> newsfeedPostTypes = _proxy.GetNewsFeedPostTypes(sessionId);
            var postType = newsfeedPostTypes.SingleOrDefault(t => t.PostTypeCode == typeCode.ToString());
            if ( postType != null )
            {
                var post = new SocNewsFeedPost
                {
                    FromMemberID = memberId,
                    NewsFeedPostTypeID = postType.NewsFeedPostTypeID,
                    NewsFeedPost = postText,
                    GroupID = groupId,
                    CommunityID = communityId
                };
                var nfp = _proxy.AddNewsFeedPost( sessionId, post );

                bool success = nfp.NewsFeedPostID > 0;
                return success;
            } else
            {
                return false;
            }
        }
        /// <summary>
        /// submit a post from wall (news feed) or group post/ comment...
        /// </summary>
        /// <param name="sessionId">this should be removed as it is not checked in MT anymore</param>
        /// <param name="memberId">memberId of user making the post = logged in user = can be retrieved from session</param>
        /// <param name="postText">text that will fill in the post/news feed/ comment</param>
        /// <param name="groupId">0 if this is a wall news feed/ group id if this is a group post</param>
        /// <param name="communityId"></param>
        /// <param name="newsFeedTypeId">type of the feed - MemberPostedInGroup/ManualPost/Comment and so on </param>
        /// <returns>true if insert + trigger the reward for first post was successful, false otherwise</returns>
        public bool SumbitMemberPost(string sessionId, long memberId, string postText, long groupId, long communityId, int newsFeedTypeId)
        {
            var post = new SocNewsFeedPost
            {
                FromMemberID = memberId,
                NewsFeedPostTypeID = newsFeedTypeId, //isGroupPost ? (int)NewsFeedPostType.MemberPostedInGroup : (int)NewsFeedPostType.ManualPost,
                NewsFeedPost = postText,
                GroupID = groupId,
                CommunityID = communityId
            };
            var nfp = _proxy.AddNewsFeedPost( sessionId, post );
            
            bool success = nfp.NewsFeedPostID > 0;
            return success;
        }

        public List<NewsfeedPost> GetMemberNewsfeed( long memberId, long communityId, int commentCount )
        {
            var data = _proxy.GetMemberRecentNewsFeedPosts(memberId, communityId, ApplicationSettings.MiniNewsfeedDaysToInclude, ApplicationSettings.MiniNewsfeedCount, commentCount );
            
            return data.Select( SocialRepository.NewsFeedPost ).ToList();
        }

        public MemberRelationships GetMemberRelationship( string sessionId, long friendId )
        {
            var relationship = _proxy.IsFriend( sessionId, friendId );
            return SocialRepository.MemberRelationships( relationship );
        }

        public string GetVhmContent(long sponsorId, long memberId, string contentAreaId, int languageId)
        {
            var socialService = new WSVHMSocial();
            //return socialService.GetContent(contentAreaId,  memberId, languageId);
            return socialService.GetContent( contentAreaId,sponsorId, memberId, languageId );
        }

        public string GetSponsorContentForMember( long memberId )
        {
            var socialService = new WSVHMSocial();
            return socialService.GetSponsorContentForMember(memberId);
        }

        public string GetWeeklyStatusUpdatePrompt(string sessionId, int weekNumber)
        {
            return _proxy.GetWeeklyStatusUpdatePrompt(sessionId, weekNumber);
        }

        public List<MemberProfile> SearchFriendsByName( long memberId, string name )
        {
            var friends = _proxy.SearchFriendsByName( memberId, name );
            return friends.Select( SocialRepository.MemberProfile ).ToList();
        }
    }

}
