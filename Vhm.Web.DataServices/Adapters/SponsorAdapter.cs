﻿using System.Collections.Generic;
using System.Linq;
using Vhm.Web.DataServices.Interfaces;
using Vhm.Web.DataServices.Repositories;
using Vhm.Web.Models.RepositoryModels;
using Vhm.Web.DataServices.MiddleTierService;
using AutoMapper;

namespace Vhm.Web.DataServices.Adapters {
    public class SponsorAdapter : BaseAdapter, ISponsorAdapter {

        public SponsorProfile GetSponsorProfile( long sponsorId )
        {
            var profile = _proxy.GetSponsorProfile( sponsorId );
            return SponsorRepository.SponsorProfile( profile );
        }

        public List<SponsorModuleGZVersion> GetSponsorModuleGZVersions(long sponsorModuleId)
        {
            var gzVersions = _proxy.GetSponsorModuleGZVersions(sponsorModuleId);
            return gzVersions.Select( SponsorRepository.SponsorModuleGZVersion ).ToList();
        } 

        public List<ColorTheme> GetAvailableSponsorColorThemes( long sponsorId )
        {
            var brandThemes = _proxy.GetBrandingThemes();
            return brandThemes.Select(SponsorRepository.AvailableSponsorColorThemes).ToList();
        }

        public List<SponsorLanguage> GetSponsorLanguages( long sponsorId )
        {
            var languages = _proxy.GetSponsorLanguages( sponsorId );
            return languages.Select( SponsorRepository.SponsorLanguage ).ToList();
        }

        public List<PasswordRegEx> GetPasswordRegExValues()
        {
            var passwordRegExValues = _proxy.GetPasswordRegExValues();
            return Mapper.Map<List<VhmPasswordRegEx>, List<PasswordRegEx>>(passwordRegExValues);
        }

        public List<SponsorModule> GetSponsorModules(long sponsorId)
        {
            var modules = _proxy.GetSponsorModules( sponsorId );
            return modules.Select(SponsorRepository.SponsorModules).ToList();
        }

        public string GetWeeklyStatusUpdatePrompt(string sessionId, int weekNumber)
        {
            return _proxy.GetWeeklyStatusUpdatePrompt(sessionId, weekNumber);
        }

        public List<RewardsProgram> GetSponsorPrograms(long sponsorId)
        {
            var programs = _proxy.GetPrograms( sponsorId );
            return programs.Select( SponsorRepository.RewardsProgram ).ToList();
        }
    }
}
