﻿using Vhm.Web.DataServices.Interfaces;
using Vhm.Web.DataServices.MiddleTierActivityService;
using Vhm.Web.DataServices.Repositories;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.DataServices.Adapters
{
    public class ActivityAdapter : BaseActivityAdapter, IActivityAdapter
    {
        
        public bool AddMemberRunkeeperToken(long memberID, string code)
        {
            if (string.IsNullOrEmpty(code))
            {
                return false;
            }

            return _activityProxy.AddMemberRunkeeperToken(memberID, code);
        }

        public bool IsMemberConnectedToRunKeeper(long memberID)
        {
            return _activityProxy.IsMemberConnectedToRunKeeper(memberID);
        }

        public bool DeleteMemberRunkeeperToken(long memberID)
        {
            return _activityProxy.DeleteMemberRunkeeperToken(memberID);
        }

        public string GetMemeberAccessToken(long memberID)
        {
            var device = _activityProxy.GetMemberExistingRkDevice(memberID);

            return device != null ? device.RemoteID : string.Empty;
        }

        public bool SyncRunkeeperData(long memberID)
        {
            return _activityProxy.SyncRunkeeperData(memberID);
        }

        public bool AddMemberFitbitTokens(long memberID, FitbitAuthInfo code)
        {
            return _activityProxy.AddMemberFitbitTokens(memberID, code);
        }

        public bool IsMemberConnectedToFitbit(long memberID)
        {
            return _activityProxy.IsMemberConnectedToFitbit(memberID);
        }

        public bool DeleteMemberFitbitTokens(long memberID)
        {
            return _activityProxy.DeleteMemberFitbitTokens(memberID);
        }

        public bool IsMemberRegisterToGZ(long memberID)
        {
            return _activityProxy.GetMemberExistingGoZoneDevice(memberID) != null;
        }

        public bool IsMemberLinkedToPolar(long memberID)
        {
            return _activityProxy.IsMemberLinkedToPolar(memberID);
        }

        /// <summary>
        /// use this to return member's GoZone device(device with the type=SBPED), if one is registered under user's account
        /// </summary>
        /// <param name="memberId">user's vhm(crm) memberId</param>
        /// <returns>member's GoZone device, if one is registered under user's account</returns>
        public MemberDevice GetMemberExistingGoZoneDevice(long memberId)
        {
            VhmMemberDevice mtDevice = _activityProxy.GetMemberExistingGoZoneDevice(memberId);
            var memberExistingDevice = MemberRepository.MemberDevice(mtDevice);//AutoMapper.Mapper.Map<VhmMemberDevice, MemberDevice>( mtDevice );
            return memberExistingDevice;
        }
    }
}
