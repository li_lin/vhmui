﻿using System.Collections.Generic;
using Vhm.Web.DataServices.Interfaces;
using Vhm.Web.DataServices.Repositories;
using Vhm.Web.Models;

namespace Vhm.Web.DataServices.Adapters
{
    public class AccountAdapter : BaseAdapter, IAccountAdapter
    {
        public void KillSession(string sessionId)
        {
            _proxy.KillSession( sessionId );
        }

        public Session GetNewSession(string userName, string password, string system, int timeout)
        {
            var session = _proxy.GetNewSession(userName, password, system, timeout);
            return AccountRepository.GetMemberSession(session);
        }

        public Session GetMemberIdBySession(string sessionId)
        {
            var session = _proxy.CheckSession(sessionId);
            return AccountRepository.GetMemberSession(session);
        }

        public Session GetSession(string sessionId)
        {
            var session = _proxy.CheckSession(sessionId);
            return AccountRepository.GetMemberSession(session);
        }

        public bool UpdateSession (Session session, int timeOut)
        {
            var mtSession = AccountRepository.GetMiddleTierSession( session );
            _proxy.UpdateSessionData(mtSession, timeOut);
            return true;
        }
    }
}
