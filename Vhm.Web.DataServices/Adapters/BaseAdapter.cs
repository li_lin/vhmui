﻿
using System.ServiceModel;
using Vhm.Web.Common;
using Vhm.Web.DataServices.MiddleTierService;
using log4net;

namespace Vhm.Web.DataServices.Adapters
{
    public class BaseAdapter
    {
        //protected readonly IClientChannelFactory<IMiddleTierServiceChannel> _proxyFactory;
        protected readonly MiddleTierServiceClient _proxy;
        public static readonly ILog log = LogManager.GetLogger(typeof(BaseAdapter).Name);

        public BaseAdapter()
        {
            string endpointName = ApplicationSettings.MTEndpointName;
            _proxy = new MiddleTierServiceClient(endpointName);
        }

        ~BaseAdapter()
        {
            try {
                if (_proxy.State != CommunicationState.Faulted) {
                    _proxy.Close();
                }
            } finally
            {
                if (_proxy.State != CommunicationState.Closed)
                {
                    _proxy.Abort();
                }
            }
        }
    }
}
