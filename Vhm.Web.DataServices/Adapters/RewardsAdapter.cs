﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vhm.Web.DataServices.MiddleTierService;
using Vhm.Web.DataServices.Repositories;
using Vhm.Web.DataServices.Interfaces;
using Vhm.Web.Models.Enums;
using Vhm.Web.Models.RepositoryModels;
using RewardBadge = Vhm.Web.Models.RepositoryModels.RewardBadge;

namespace Vhm.Web.DataServices.Adapters {
    public class RewardsAdapter :BaseAdapter, IRewardsAdapter {
        public List<UpForGrabsItem> GetMemberAvailableUpForGrabs(long memberId)
        {
            var listOfUpForGrabs = new List<UpForGrabsItem>();
            var upForGrabs = _proxy.GetUpForGrabsRewards(memberId);
            if (upForGrabs != null && upForGrabs.Count > 0 )
            {
                listOfUpForGrabs = upForGrabs.Select(RewardsRepository.UpForGrabsItem).ToList();
            }
            return listOfUpForGrabs;
        }

        /// <summary>
        /// Triggers the reward.
        /// </summary>
        /// <param name="memberId">The member id.</param>
        /// <param name="rewardCode">The reward code.</param>
        /// <param name="processNow">if set to <c>true</c> [process now].</param>
        /// <param name="surpressResult">if set to <c>true</c> [surpress result].</param>
        /// <param name="triggerDate">The trigger date.</param>
        /// <param name="challengeTaskId">The challenge task id.</param>
        /// <returns></returns>
        public bool TriggerReward(long memberId, string rewardCode, bool processNow, bool surpressResult, DateTime triggerDate, long? challengeTaskId)
        {
            return _proxy.TriggerReward( memberId, rewardCode, processNow, surpressResult, triggerDate, challengeTaskId );
        }

        public List<GameTrigger> GetGameTriggers(long gameId)
        {
            var triggers = _proxy.GetGameTriggers( gameId );
            return triggers.Select( RewardsRepository.GameTrigger ).ToList();
        }

        public List<GameTrigger> GetSystemTriggers( long memberId, long sponsorId )
        {
            var triggers = _proxy.GetSystemTrigers(memberId, sponsorId);
            return triggers.Select(RewardsRepository.GameTrigger).ToList();
        }

        public List<GameCurrency> GetGameCurrencies()
        {
            var currencies = _proxy.GetGameCurrencies();
            return currencies.Select( RewardsRepository.GameCurrency ).ToList();
        } 

        public Game GetSponsorCurrentGame(long memberId, long sponsorId)
        {
            var currentGame = _proxy.GetGameInfo( memberId, sponsorId );

            Game game = null;
            if (currentGame != null && currentGame.Game != null)
            {
                game = RewardsRepository.Game(currentGame);
                //game.Rewards = game.Rewards ?? new List<Reward>();
                game.AvailableRewards = game.AvailableRewards ?? new RewardsBalance();
            }
          
            return game;
        }

        public List<MemberLevelsAudit> GetMemberLevelsAudit(long memberId, long sponsorId)
        {
            var levelsAudit = _proxy.GetMemberLevelsAudit(memberId, sponsorId);

           var levelsAuditList = new List<MemberLevelsAudit>();
            if (levelsAudit != null && levelsAudit.Count>0)
            {
               
                levelsAuditList = levelsAudit.Select(RewardsRepository.MemberLevelsAudit).ToList();
            }

            return levelsAuditList;
        }

        public BadgeItem GetBadges(long badgeId)
        {
           var rewardsBadge= _proxy.GetBadge(badgeId);
           BadgeItem badge = null;
           if (rewardsBadge != null)
           {
               badge = RewardsRepository.Badge(rewardsBadge);
           }
           return badge;

        }

        public MemberRewardsData GetMemberRewardsData( long memberId )
        {
            var result = _proxy.GetMemberRewardsData( memberId );
            return AutoMapper.Mapper.Map<VhmMemberRewardsData, MemberRewardsData>( result );
        }

        public MemberRewardsBalance GetMemberRewardsBalnace(long memberId)
        {
            var result = _proxy.GetMemberRewardsBalance(memberId);
            return AutoMapper.Mapper.Map<VhmMemberRewardsBalance, MemberRewardsBalance>(result);
        }

        public List<RewardBadge> GetBadgesByType( BadgeType type )
        {
            var list = _proxy.GetBadgesByType( type.ToString() );
            return list != null ? list.Select(RewardsRepository.RewardBadge).ToList() : new List<RewardBadge>();
        }
    }
}
