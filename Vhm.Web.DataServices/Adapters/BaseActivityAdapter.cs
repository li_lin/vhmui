﻿using System.ServiceModel;
using Vhm.Web.Common;
using Vhm.Web.DataServices.MiddleTierActivityService;

namespace Vhm.Web.DataServices.Adapters
{
    public class BaseActivityAdapter : BaseAdapter
    {
        protected readonly MiddleTierActivityServiceClient _activityProxy;

        public BaseActivityAdapter()
        {
            string endpointName = ApplicationSettings.MTActivityEndpointName;
            _activityProxy = new MiddleTierActivityServiceClient(endpointName);
        }

        ~BaseActivityAdapter()
        {
            try {
                if (_activityProxy.State != CommunicationState.Faulted)
                {
                    _activityProxy.Close();
                }
            } finally
            {
                if (_activityProxy.State != CommunicationState.Closed)
                {
                    _activityProxy.Abort();
                }
            }
        }
    }
}
