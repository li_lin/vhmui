﻿using System;
using System.Net;
using System.Text;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Vhm.Web.Api.Common
{
    /// <summary>
    /// Base class for all API controllers.
    /// Provides support for authentication and a catch-all exception handler for consistent JSON error responses.
    /// </summary>
    public abstract class ControllerBase : Controller
    {
        protected ControllerBase()
        {
        }

        protected virtual string ToJson(object o)
        {
            var settings = new JsonSerializerSettings();
            settings.Converters.Add(new StringEnumConverter());
            settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            return JsonConvert.SerializeObject(o, settings);
        }

        protected virtual ActionResult ToJsonResult(object o)
        {
            return new ContentResult
                       {
                           ContentType = "application/json",
                           Content = ToJson(o),
                           ContentEncoding = Encoding.UTF8
                       };
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            Exception e = filterContext.Exception;
            ApiException oe = e as ApiException;
            HttpStatusCode statusCode = (oe != null) ? oe.StatusCode : HttpStatusCode.InternalServerError;

            filterContext.ExceptionHandled = true;
            filterContext.RequestContext.HttpContext.Response.StatusCode = (int)(statusCode);
            filterContext.Result = ToJsonResult(new ErrorMessage
                                                     {
                                                         Message = e.Message,
                                                         ErrorCode = (int)statusCode
                                                     });
        }

        protected virtual long GetMemberId()
        {
            return MemberUtility.GetSession(Request).MemberID;
        }
    }
}