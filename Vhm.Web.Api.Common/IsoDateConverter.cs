﻿using System;
using Newtonsoft.Json.Converters;

namespace Vhm.Web.Api.Common
{
    public class IsoDateConverter : IsoDateTimeConverter
    {
        public IsoDateConverter()
        {
            DateTimeFormat = "yyyy-MM-dd";
        }
    }
}
