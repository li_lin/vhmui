﻿using System;
using System.Net;
using System.Web;
using System.Web.Security;
using Vhm.Web.BL;
using Vhm.Web.Models;

namespace Vhm.Web.Api.Common
{
    public static class MemberUtility
    {
        public static readonly string SESSION_COOKIE_NAME = "SessionID";

        /// <summary>
        /// Gets the Session model associated with this request.
        /// Without a valid session cookie, an auth exception is thrown.
        /// </summary>
        public static Session GetSession(HttpRequestBase request)
        {
            return GetSession(request.Cookies[SESSION_COOKIE_NAME]);
        }

        /// <summary>
        /// Gets the Session model associated with this cookie.
        /// Without a valid session cookie, an auth exception is thrown.
        /// </summary>
        /// <returns></returns>
        public static Session GetSession(HttpCookie authCookie)
        {
            // Grab session cookie
            if (authCookie == null)
            {
                throw new ApiException(HttpStatusCode.Forbidden, "Session cookie missing");
            }

            return GetSessionByEncryptedTicket(authCookie.Value);
        }

        /// Gets the Session model associated with this encrypted ticket.
        /// Without a valid session cookie, an auth exception is thrown.
        /// We use 403 (Forbidden) instead of 401 (Unauthorized) since we want to avoid FormsAuthentication's auto-redirect to the login page -- that's not appropriate for API requests.
        public static Session GetSessionByEncryptedTicket(string encryptedTicket)
        {
            // Attempt to decrypt and validate cookie
            string sessionId;
            try
            {
                sessionId = FormsAuthentication.Decrypt(encryptedTicket).UserData;
            }
            catch (Exception e)
            {
                throw new ApiException(HttpStatusCode.Forbidden, "Session cookie invalid: " + e.Message);
            }

            // Lookup Member ID from session
            Session session = AccountBL.GetSession(sessionId);
            if (session == null)
            {
                throw new ApiException(HttpStatusCode.Forbidden, "Session expired");
            }

            return session;
        }
    }
}
