﻿using System;
using System.Collections;
using System.Globalization;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Vhm.Web.Models.ViewModels.Shared;

namespace VirginLifecareWeb.Helpers
{
    public static class GroupPaginationHtmlHelper
    {
        //the pagingSouce will represent - 'myGroups' if we're applying paging for my groups or "searchResults" - if we apply paging for search
        public static HtmlString Paging (this HtmlHelper html, PagingDataVM pagedList, string pagingSource, string arrowLeftUrl, string arrowRightUrl, int displayNoOfPages)
        {
            StringBuilder sb = new StringBuilder();

            // only show paging if we have more items than the page size
            if(pagedList.TotalNumberOfItems > pagedList.PageSize)
            {
                #region Drop Down Option
                sb.Append("<span class=\"poLeft\"><span style=\"margin-right: 0px;\">Show </span>");
                //<option selected=\"selected\" value=\"24\">24</option>
                sb.Append("<select name=\"ddItemsPerPage\" id=\"ddItemsPerPage\" class=\"ddlCategory\" onchange = '");
                string jsonChangeEvent = string.Format("javascript:VHM.SocialGroups.changeItemsPerPage(\"{0}\", this);", pagingSource);
                sb.Append(jsonChangeEvent);
                if(pagedList.PageSize == 24)
                {
                    sb.Append("'> <option value=\"24\" selected=\"selected\" >24</option>");
                }
                else
                {
                    sb.Append("'> <option value=\"24\" >24</option>");
                }
                if(pagedList.TotalNumberOfItems > 24)
                {
                    if(pagedList.PageSize == 48)
                    {
                        sb.Append("<option value=\"48\" selected=\"selected\">48</option>");
                    }
                    else
                    {
                        sb.Append("<option value=\"48\">48</option>");
                    }
                }
                if(pagedList.TotalNumberOfItems > 48)
                {
                    if(pagedList.PageSize == 96)
                    {
                        sb.Append("<option value=\"96\" selected=\"selected\">96</option>");
                    }
                    else
                    {
                        sb.Append("<option value=\"96\">96</option>");
                    }
                }
                sb.Append("</select><span> per page</span> ");
                sb.Append("</span>");
                #endregion
                #region Paging

                sb.Append("<span class=\"poRight\"><ul class=\"pagingList\">");

                if(pagedList.CurentPageNumber > 0)
                { // previous link
                    sb.Append("<li class=\"pagingItem\"><a href=\"");
                    string jsFunction = string.Format("javascript:VHM.SocialGroups.getGroupPage('{0}','{1}', '{2}');", pagingSource, pagedList.CurentPageNumber, pagedList.PageSize);
                    sb.Append(jsFunction);
                    //sb.Append(url.Replace(pagePlaceHolder, pagedList.CurentPageNumber.ToString()));
                    sb.Append("\" title=\"Go to Previous Page\"><img src=\"");
                    sb.Append(arrowLeftUrl);
                    sb.Append("\" alt=\"Go to previous page\" class=\"navigationIcon\" /></a></li>");
                }
                //calculate pages numbers to be shown
                //we only show 5 number of pages out of the total
                ArrayList pages = new ArrayList();
                if(pagedList.TotalNumberOfPages <= displayNoOfPages)
                {
                    for(int i = 0; i < pagedList.TotalNumberOfPages; i++)
                        pages.Add((i + 1).ToString(CultureInfo.InvariantCulture));
                }
                else
                {
                    Int32 start; Int32 end;
                    if(pagedList.CurentPageNumber > 4)
                    {
                        start = pagedList.CurentPageNumber - 4;
                    }
                    else
                    {
                        start = 1;
                    } 
                    end = start + displayNoOfPages - 1;
                    start = (end >= pagedList.TotalNumberOfPages) ? (pagedList.TotalNumberOfPages - 5 + 1) : start;
                    end = start + 5 -1;
                    for(int k = 0; start <= end; k++)
                    {
                        pages.Add(start);
                        start++;
                    }
                }
                foreach (var item in pages)
                {
                    sb.Append("<li class=\"pagingItem\">");
                    if(item.ToString() == (pagedList.CurentPageNumber + 1).ToString())
                    {
                        sb.Append("<span class=\"currentPageItem\">").Append((item).ToString()).Append("</span>");
                    }
                    else
                    {
                        sb.Append("<a class=\"blueLink\" href=\"");
                        string jsFunction = string.Format("javascript:VHM.SocialGroups.getGroupPage('{0}','{1}', '{2}');", pagingSource, item, pagedList.PageSize);
                        sb.Append(jsFunction);
                        sb.Append("\" title=\"Go to Page ").Append((item).ToString());
                        sb.Append("\">").Append((item).ToString()).Append("</a>");
                    }
                    sb.Append("</li>");
                }

                if(pagedList.TotalNumberOfPages > 4)
                {
                    //<li class=\"pagingItem\" id=\"liOfTotal\">of&nbsp;pagedList.TotalNumberOfPages&nbsp;</li>
                    sb.AppendFormat("<li class=\"pagingItem\" id=\"liOfTotal\">of&nbsp;{0}&nbsp;</li>",
                                    pagedList.TotalNumberOfPages);
                }
                int curentPageNo = pagedList.CurentPageNumber + 1; //page no begin at 0
                if(curentPageNo < pagedList.TotalNumberOfPages)
                { // next link
                    sb.Append("<li class=\"pagingItem\"><a href=\"");
                    string jsFunction = string.Format("javascript:VHM.SocialGroups.getGroupPage('{0}','{1}', '{2}');", pagingSource, pagedList.CurentPageNumber + 2, pagedList.PageSize);
                    sb.Append(jsFunction);
                    sb.Append("\" title=\"Go to Next Page\"><img src=\"");
                    sb.Append(arrowRightUrl);
                    sb.Append("\" alt=\"Go to next page\" class=\"navigationIcon\" />");
                    sb.Append("</a></li>");
                }
                sb.Append("</ul></span>");
                #endregion
            }
            var ret = new MvcHtmlString(sb.ToString());
            return ret;
        }
    }
}