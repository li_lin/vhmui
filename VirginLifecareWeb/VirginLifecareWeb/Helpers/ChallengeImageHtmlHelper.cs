﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vhm.Web.Models.RepositoryModels;
using Vhm.Web.Models.ViewModels.Member;

namespace VirginLifecareWeb.Helpers
{
    public static class ChallengeImageHtmlHelper
    {


        public static HtmlString ChallengeImage(LandingPageVM model, ChallengeBase challenge)
        {
            string imagePath = string.Empty;
            string kagClass = "";
            if ( challenge is Promo )
            {
                if ( !string.IsNullOrEmpty( challenge.BadgeName ) )
                {
                    if ( challenge is Kag )
                    {
                        kagClass = "kagImage";
                    }
                    imagePath = model.BadgeImageUrl + challenge.BadgeName + ".png";
                } else
                {
                    if ( challenge is Kag )
                    {
                        var kag = challenge as Kag;
                        kagClass = "kagImage";
                        imagePath = string.Concat( model.RewardTypeImageUrl, kag.RewardImage, "_on.gif" );
                    } else
                    {
                        imagePath = model.ImageCDN + challenge.ChallengeType + ".png";
                    }
                }
            } else
            {
                imagePath = model.ImageCDN + challenge.ChallengeType + ".png";
            }
            
            return

                new HtmlString(
                    "<img class=\"challengeBadge " + kagClass + "\" src =\"" + imagePath + "\" alt = \"badge\" / >" );
        }

    }
}
