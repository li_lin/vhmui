﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vhm.Web.DataServices.MiddleTierActivityService;

namespace VirginLifecareWeb.Helpers
{
    interface IOAuth1AuthenticationHelper
    {
        OAuthUrlToken GetAuthUrlToken(string callbackUrl);

        OAuthAuthorizedInfo ProcessApprovedAuthCallback(string vendorCode, string tempAuthToken, string verifier, string tokenSecret);
    }
}
