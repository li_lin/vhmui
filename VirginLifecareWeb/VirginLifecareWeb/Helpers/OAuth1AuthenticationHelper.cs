﻿using log4net;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace VirginLifecareWeb.Helpers
{
    public class OAuthUrlToken
    {
        public string Url { get; set; }
        public string Secret { get; set; }
    }


    public class OAuthAuthorizedInfo
    {
        public string VendorCode { get; set; }
        public string AuthToken { get; set; }
        public string AuthTokenSecret { get; set; }
        public string UserId { get; set; }
    }


    public class OAuth1AuthenticationHelper : IOAuth1AuthenticationHelper
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(OAuth1AuthenticationHelper));

        private string _consumerKey;
        private string _consumerSecret;

        private string _requestTokenUrl;
        private string _accessTokenUrl;
        private string _authorizeUrl;


        /// <summary>                                                                         
        /// Initializes a new instance of the <see cref="OAuth1AuthenticationHelper"/> class. 
        /// </summary>                                                                        
        /// <param name="ConsumerKey">The consumer key.</param>                               
        /// <param name="ConsumerSecret">The consumer secret.</param>                         
        /// <param name="requestTokenUrl">The request token URL.</param>                      
        /// <param name="accessTokenUrl">The access token URL.</param>                        
        /// <param name="authorizeUrl">The authorize URL.</param>                             
        public OAuth1AuthenticationHelper(string ConsumerKey, string ConsumerSecret, string requestTokenUrl, string authorizeUrl, string accessTokenUrl)
        {
            _consumerKey = ConsumerKey;
            _consumerSecret = ConsumerSecret;
            _requestTokenUrl = requestTokenUrl;
            _authorizeUrl = authorizeUrl;
            _accessTokenUrl = accessTokenUrl;
        }


        /// <summary>                                                                         
        /// Gets the authentication URL token.                                                
        /// </summary>                                                                        
        /// <param name="callbackUrl">The callback URL.</param>                               
        public OAuthUrlToken GetAuthUrlToken(string callbackUrl)
        {
            // Obtain Unauthorized Request Token
            //-------------------------------------------------------
            // Request Token Url = {_requestTokenUrl}/oauth/request_token
            RestClient client = new RestClient(_requestTokenUrl);
            client.Authenticator = OAuth1Authenticator.ForRequestToken(_consumerKey, _consumerSecret, callbackUrl);

            RestRequest request = new RestRequest("oauth/request_token", Method.POST);
            IRestResponse response = client.Execute(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                // The request failed, we are done here
                _log.ErrorFormat("Failure oauth/request_token - {0}. Url={1}", response.StatusDescription, client.BaseUrl);

                throw new Exception(string.Format("Failure oauth/request_token - {0}", response.StatusDescription));
            }

            // Get the oAuth token and secret from the response...
            NameValueCollection queryStringValues = HttpUtility.ParseQueryString(response.Content);  //Parsing the content not the query string.
            string oAuthToken = queryStringValues["oauth_token"];
            string oAuthTokenSecret = queryStringValues["oauth_token_secret"];

            // Assemble the URL to redirect the user to Authorize request token
            //---------------------------------------------------------------------
            // {_authorizeUrl}/oauth/authorize
            client = new RestClient(_authorizeUrl);

            request = new RestRequest("oauth/authorize");
            request.AddParameter("oauth_token", oAuthToken);
            string url = client.BuildUri(request).ToString();

            // Return the Url and the oAuthSecret
            OAuthUrlToken urlToken =  new OAuthUrlToken
            {
                Url = url,
                Secret = oAuthTokenSecret
            };

            return urlToken;
        }



        /// <summary>                                                                         
        /// Processes the approved authentication callback.                                   
        /// </summary>                                                                        
        /// <param name="vendorCode">The vendor code.</param>                                 
        /// <param name="tempAuthToken">The temporary authentication token.</param>           
        /// <param name="verifier">The verifier.</param>                                      
        /// <param name="tokenSecret">The token secret.</param>                               
        public OAuthAuthorizedInfo ProcessApprovedAuthCallback(string vendorCode, string tempAuthToken, string verifier, string tokenSecret)
        {
            // Assemble the URL to request the Access Token...
            //-------------------------------------------------------

            // Access Token Url = {_accessTokenUrl}/oauth/access_token
            RestClient client = new RestClient(_accessTokenUrl);
            RestRequest request = new RestRequest("oauth/access_token", Method.POST);

            client.Authenticator = OAuth1Authenticator.ForAccessToken(
                _consumerKey, _consumerSecret, tempAuthToken, tokenSecret, verifier);

            // Issue the request and parse the response...
            //---------------------------------------------------
            IRestResponse response = client.Execute(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                // The request failed, we are done here
                _log.ErrorFormat("Failure oauth/access_token - {0}. Url={1}", response.StatusDescription, client.BaseUrl);

                throw new Exception(string.Format("Failure oauth/access_token - {0}", response.StatusDescription));
            }

            NameValueCollection queryStringValues = HttpUtility.ParseQueryString(response.Content); //Parsing the content not the query string.
            string oauth_token = queryStringValues["oauth_token"];
            string oauth_token_secret = queryStringValues["oauth_token_secret"];
            
            // Attempt to get the user_id value, or null if the OAuth implementation does not provide it.
            string encoded_user_id = queryStringValues.Get("encoded_user_id");

            OAuthAuthorizedInfo oAuthInfo = new OAuthAuthorizedInfo()
            {
                VendorCode = vendorCode,
                AuthToken = oauth_token,
                AuthTokenSecret = oauth_token_secret,
                UserId = encoded_user_id
            };

            return oAuthInfo;
        }
    }
}