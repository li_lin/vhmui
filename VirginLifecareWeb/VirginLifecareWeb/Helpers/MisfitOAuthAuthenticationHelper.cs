﻿using System;
using log4net;
using RestSharp;

namespace VirginLifecareWeb.Helpers
{
    public class MisfitOAuthAuthenticationHelper : IOAuth1AuthenticationHelper
    {

        private readonly ILog _log = LogManager.GetLogger(typeof(MisfitOAuthAuthenticationHelper));

        protected string _consumerKey;
        protected string _consumerSecret;

        protected string _requestTokenUrl;
        protected string _accessTokenUrl;
        protected string _authorizeUrl;
        /// <summary>                                                                         
        /// class ExchangeTokenResponse                                                       
        /// </summary>                                                                        
        private class ExchangeTokenResponse
        {
            public string access_token { get; set; }
            public string token_type { get; set; }
        }


        /// <summary>                                                                         
        /// Initializes a new instance of the <see cref="OAuthMisfit"/> class.                
        /// </summary>                                                                        
        /// <param name="consumerKey">The consumer key.</param>                               
        /// <param name="consumerSecret">The consumer secret.</param>                         
        /// <param name="requestTokenUrl">The request token URL.</param>                      
        /// <param name="accessTokenUrl">The access token URL.</param>                        
        /// <param name="authorizeUrl">The authorize URL.</param>                             
        public MisfitOAuthAuthenticationHelper(string consumerKey, string consumerSecret, string requestTokenUrl, string authorizeUrl, string accessTokenUrl)
        {
            _consumerKey = consumerKey;
            _consumerSecret = consumerSecret;
            _requestTokenUrl = requestTokenUrl;
            _authorizeUrl = authorizeUrl;
            _accessTokenUrl = accessTokenUrl;
        }


        //------------------------------------------------------
        #region IOAuth1AuthenticationHelper

        /// <summary>                                                                         
        /// Gets the authentication URL token.                                                
        /// POST https://api.misfitwearables.com/auth/dialog/authorize                        
        /// </summary>                                                                        
        /// <param name="callbackUrl">The callback URL.</param>                               
        public OAuthUrlToken GetAuthUrlToken(string callbackUrl)
        {
            LogInfo("Start - GetAuthUrlToken");

            // Obtain Unauthorized Request Token
            //-------------------------------------------------------
            // Request Token Url = {_requestTokenUrl}/auth/dialog/authorize
            RestClient client = new RestClient(_requestTokenUrl);
            RestRequest request = new RestRequest("auth/dialog/authorize", Method.POST);

            request.AddParameter("response_type", "code", ParameterType.QueryString);
            request.AddParameter("client_id", _consumerKey, ParameterType.QueryString);
            request.AddParameter("redirect_uri", callbackUrl, ParameterType.QueryString);
            request.AddParameter("scope", "public", ParameterType.QueryString);

            string url = client.BuildUri(request).ToString();
            _log.DebugFormat("AuthUrl={0}", url);

            // Return the Url. For this provider OAuth implementation there is not secret, 
            // but they require the callback url to exchange the code for a token...
            OAuthUrlToken urlToken = new OAuthUrlToken
            {
                Url = url,
                Secret = callbackUrl,  // We will use the secret field to hold on to the callback url.
            };

            LogInfo("End - GetAuthUrlToken");
            return urlToken;
        }



        /// <summary>                                                                         
        /// Processes the approved authentication callback.                                   
        ///                                                                                   
        /// POST https://api.misfitwearables.com/auth/tokens/exchange                         
        /// {                                                                                 
        ///   grant_type=authorization_code&code=CODE&redirect_uri=URL&client_id=CLIENTID&client_secret=CLIENTSECRET
        /// }                                                                                 
        /// </summary>                                                                        
        /// <param name="vendorCode">The vendor code.</param>                                 
        /// <param name="tempAuthToken">The temporary authentication token.</param>           
        /// <param name="verifier">The verifier.</param>                                      
        /// <param name="tokenSecret">The token secret.</param>                               
        public OAuthAuthorizedInfo ProcessApprovedAuthCallback(string vendorCode, string tempAuthToken, string verifier, string tokenSecret)
        {
            LogInfo("Start - ProcessApprovedAuthCallback");

            // Assemble the URL to request the Access Token...
            //-------------------------------------------------------
            // Request Token Url = {_requestTokenUrl}/auth/tokens/exchange
            RestClient client = new RestClient(_accessTokenUrl);
            RestRequest request = new RestRequest("auth/tokens/exchange", Method.POST);

            // Parameters need to be posted in the body of the request, using ParameterType.GetOrPost accomplishes that.
            request.AddParameter("grant_type", "authorization_code", ParameterType.GetOrPost);
            request.AddParameter("code", tempAuthToken, ParameterType.GetOrPost);
            request.AddParameter("redirect_uri", tokenSecret, ParameterType.GetOrPost);  // tokenSecret is expected to contain the callback url.
            request.AddParameter("client_id", _consumerKey, ParameterType.GetOrPost);
            request.AddParameter("client_secret", _consumerSecret, ParameterType.GetOrPost);

            // Issue the request and parse the response...
            //---------------------------------------------------
            _log.DebugFormat("Issuing request: {0}", client.BaseUrl);
            var response = client.Execute<ExchangeTokenResponse>(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                // The request failed, we are done here
                _log.ErrorFormat("Failure auth/tokens/exchange - {0}. Url={1}", response.StatusDescription, client.BaseUrl);

                throw new Exception(string.Format("Failure auth/tokens/exchange - {0}", response.StatusDescription));
            }

            string oauth_token = response.Data.access_token;
            string token_type = response.Data.token_type;


            OAuthAuthorizedInfo oAuthInfo = new OAuthAuthorizedInfo()
            {
                VendorCode = vendorCode,
                AuthToken = oauth_token,
                AuthTokenSecret = null,
                UserId = null,
            };

            LogInfo("End - ProcessApprovedAuthCallback");
            return oAuthInfo;
        }

        #endregion IOAuth1AuthenticationHelper
        //------------------------------------------------------


        /// <summary>                                                                         
        /// Logs the information.
        /// </summary>                                                                        
        /// <param name="msg">The MSG.</param>                                                
        private void LogInfo(string msg)
        {
            if (_log.IsInfoEnabled)
            {
                _log.Info(msg);
            }
        }

    }
}
