﻿using System.Collections.Generic;

namespace VirginLifecareWeb.Theming
{
    public interface ICustomThemeProvider
    {
        IEnumerable<CssStyle> GetCss();
    }
}
