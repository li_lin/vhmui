﻿using System;
using System.Collections.Generic;

namespace VirginLifecareWeb.Theming
{
    public class StaticCustomThemeProvider : ICustomThemeProvider
    {
        public IEnumerable<CssStyle> GetCss()
        {
            return new List<CssStyle>
            {
                new CssStyle
                {
                    ClassName = "#mainNav li a",
                    Properties = new List<Tuple<string, string>>
                    {
                        Tuple("color", "#900")
                    }
                },
                new CssStyle
                {
                    ClassName = "#mainNav li a",
                    Properties = new List<Tuple<string, string>>
                    {
                        Tuple("background-image", "url('red/images/arrow-nav.png')")
                    }
                },
                new CssStyle
                {
                    ClassName = "#extendableHeader_sub",
                    Properties = new List<Tuple<string, string>>
                    {
                        Tuple("position", "relative"),

                        Tuple("border-radius", "6px 6px 0 0"),
                        Tuple("-webkit-border-top-left-radius", "6px"),
                        Tuple("-webkit-border-top-right-radius", "6px"),
                        Tuple("-moz-border-top-left-radius", "6px"),
                        Tuple("-moz-border-top-right-radius", "6px"),

                        Tuple("background", "-webkit-gradient(linear, 0 0, 0 100%, from(#cc0000) to(#990000))"), // old webkit
                        Tuple("background", "-webkit-linear-gradient(#cc0000, #990000)"), // new webkit
                        Tuple("background", "-moz-linear-gradient(#cc0000, #990000)"), // gecko
                        Tuple("background", "-ms-linear-gradient(#cc0000, #990000)"), // IE10
                        Tuple("background", "-o-linear-gradient(#cc0000, #990000)"), // opera 11.10+
                        Tuple("-pie-background", "linear-gradient(#cc0000, #990000)"), // future CSS3 browsers

                        Tuple("behavior", "url(/PIE.htc)")
                    }
                },
                new CssStyle
                {
                    ClassName = "#contentHeader",
                    Properties = new List<Tuple<string, string>>
                    {
                        Tuple("width", "900px"),
                        Tuple("padding", "9px 20px 0 20px")
                    }
                },
                new CssStyle
                {
                    ClassName = "#mainNav li span.arrow span",
                    Properties = new List<Tuple<string, string>>
                    {
                        Tuple("border-top", "5px solid #900")
                    }
                },
                new CssStyle
                {
                    ClassName = ".VHMfooter",
                    Properties = new List<Tuple<string, string>>
                    {
                        Tuple("background", "-webkit-gradient(linear, 0 0, 0 100%, from(#990000) to(#cc0000))"), // old webkit
                        Tuple("background", "-webkit-linear-gradient(#990000, #cc0000)"), // new webkit
                        Tuple("background", "-moz-linear-gradient(#990000, #cc0000)"), // gecko
                        Tuple("background", "-ms-linear-gradient(#990000, #cc0000)"), // IE10
                        Tuple("background", "-o-linear-gradient(#990000, #cc0000)"), // opera 11.10+
                        Tuple("-pie-background", "linear-gradient(#990000, #cc0000)"), // future CSS3 browsers

                        Tuple("behavior", "url(/PIE.htc)")
                    }
                }
            };
        }


        public Tuple<string, string> Tuple(string item1, string item2)
        {
            return new Tuple<string, string>(item1, item2);
        }
    }
}
