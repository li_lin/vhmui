﻿using System;
using System.Collections.Generic;

namespace VirginLifecareWeb.Theming
{
    public class CssStyle
    {
        public string ClassName { get; set; }
        public List<Tuple<string, string>> Properties { get; set; }
    }
}