using System;
using System.Globalization;

namespace VirginLifecareWeb.Extensions
{
    public static class DateTimeExtension
    {
        public static string ToDayMonthString(this DateTime dt, CultureInfo culture)
        {
            var dateString = dt.Date.ToString("d", culture);
            string pattern = culture.DateTimeFormat.ShortDatePattern;
            pattern = pattern.Replace( "y", string.Empty );
            
            //var year = dt.Year.ToString( culture );
            //var dateSeparator = culture.DateTimeFormat.DateSeparator;
            //var dayMonth = dateString.IndexOf(year, StringComparison.Ordinal) == 0
            //               ? dateString.Replace(year + dateSeparator, string.Empty)
            //               : dateString.Replace(dateSeparator + year, string.Empty);

            return dt.Date.ToString(pattern);
        }
    }
}