﻿using System.Web;
using System.Web.Optimization;

namespace VirginLifecareWeb.App_Start
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles( BundleCollection bundles )
        {

            bundles.Add( new ScriptBundle( "~/bundles/jquery" ).Include(
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/jQuery/jquery-migrate-1.1.1.js",
                "~/Scripts/bootstrap/bootstrap.js",
                "~/Scripts/jquery.validate.js",
                "~/Scripts/jquery.validate.unobtrusive.js",
                "~/Scripts/jquery-ui-{version}.js",
                "~/Scripts/TouchPunch.js",
                "~/Scripts/jQuery/AjaxFileUpload.js",
                "~/Scripts/jQuery/jquery.vhm.plugins.js",
                "~/Scripts/jQuery/tablesorter.js",
                "~/Scripts/jQuery/jquery.vhm.plugins.js"
                ) );

            bundles.Add( new ScriptBundle( "~/bundles/vhm" ).Include(
                "~/Scripts/respond.src.js",
                "~/Scripts/elqNow/elqCfg.js",
                "~/Scripts/elqNow/elqImg.js",
                "~/Scripts/lib/stringformat.js",
                "~/Scripts/lib/ConversionUtil.js",
                "~/Scripts/lib/json2.js",
                "~/Scripts/jQuery/ajaxForm.js",
                "~/Scripts/lib/core.js",
                "~/Scripts/VHM/social/social_notifications.js",
                "~/Scripts/VHM/social/socialGroups.js",
                "~/Scripts/VHM/social/social_newsFeed.js",
                "~/Scripts/VHM/VHMNotifications.js",
                "~/Scripts/VHM/main.js",
                "~/Scripts/VHM/utils.js",
                "~/Scripts/activity-devices.js",
                "~/Scripts/LandingPageV3/Plugins/i18next-1.7.1.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryplugins").Include(
            //    "~/Scripts/jQuery/AjaxFileUpload.js",
            //    "~/Scripts/jQuery/jquery.webservice.js"
            //    ));

            //bundles.Add(new ScriptBundle("~/bundles/sponsors").Include(
            //    "~/Scripts/Sponsors/*.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.

            bundles.Add( new ScriptBundle( "~/bundles/LandingPage" ).Include(
                "~/Scripts/JQuery/jquery.ActivityTracker.js",
                "~/Scripts/JQuery/jquery.ActivityTrackerShortcuts.js",
                "~/Scripts/VHM/WelcomeJourney.js",
                "~/Scripts/VHM/LandingPage.js",
                "~/Scripts/VHM/self_entry_challenge.js",
                "~/Scripts/jqModal.js"
                ) );
            bundles.Add( new ScriptBundle( "~/bundles/activity" ).Include(
                "~/Scripts/VHM/Activity/ActivityJournal.js"
                ) );
            bundles.Add( new ScriptBundle( "~/bundles/Challenges" ).Include(
                "~/Scripts/knockout-2.2.1.js",
                "~/Scripts/VHM/Challenges/AboutCreateChallenges.js",
                "~/Scripts/VHM/Challenges/challenges.js"
                ) );
            bundles.Add( new ScriptBundle( "~/bundles/nutrition" ).Include(
                "~/Scripts/knockout-2.2.1.js",
                "~/Scripts/VHM/self_entry_challenge.js", // for date formatting
                "~/Scripts/VHM/main.js", // for cookies
                "~/Scripts/VHM/Nutrition/Nutrition.js",
                "~/Scripts/LandingPageV3/Plugins/raphael-min.js",
                "~/Scripts/LandingPageV3/Plugins/i18next-1.7.1.js"
                ) );

            bundles.Add( new StyleBundle( "~/Content/css" ).Include(
                "~/Content/Style/reset.css",
                "~/Content/Style/LandingPageV3/Plugins/jquery-ui-1.10.3.custom.css",
                "~/Content/Style/Bootstrap/bootstrap.css",
                "~/Content/Style/font-awesome.css",
                "~/Content/Style/Bootstrap/bootstrap-responsive.css",
                "~/Content/Style/Shared/Buttons.css",
                "~/Content/Style/master.css",
                "~/Content/Style/PersonalChallenge.css",
                "~/Content/Style/Shared/BubbleInfoPopUp.css",
                "~/Content/Style/SocialStyles/style_Social.css",
                "~/Content/Style/SocialStyles/style_Notifications.css",
                "~/Content/Style/responsive.css",
                "~/Content/Style/styleV3.css"
                ) );



        bundles.Add(new StyleBundle("~/Content/LandingPage").Include(
                "~/Content/Style/LandingPage.css",
                "~/Content/Style/ActivityTracker.css",
                "~/Content/Style/jqModal.css",
                "~/Content/Style/SocialStyles/NewsFeed.css",
                "~/Content/Style/style_feedback.css"
                ));

            bundles.Add(new StyleBundle("~/Content/sec").Include(
                "~/Content/Style/SEC.css"
                             ));
            bundles.Add(new StyleBundle("~/Content/activity").Include(
                "~/Content/bootstrap.css",
                "~/Content/bootstrap-responsive.css"
                             ));
            bundles.Add(new StyleBundle("~/Content/nutrition").Include(
                "~/Content/Style/Nutrition.css"
                             ));

            //Wolverine Landing page 
            bundles.Add(new StyleBundle("~/Content/Style/LandingPageV3/landingpagev3CSS").Include(
                "~/Content/Style/LandingPageV3/Plugins/bootstrap.css",
                "~/Content/Style/LandingPageV3/Plugins/bootstrap-responsive.css",
                "~/Content/Style/LandingPageV3/Plugins/jquery-ui-1.10.3.custom.min.css",
                "~/Content/Style/LandingPageV3/style_notifications.css",
                "~/Content/Style/LandingPageV3/main.css",
                "~/Content/Style/styleV3.css",
                "~/Content/Style/font-awesome.css",
                "~/Content/Style/Plugins/messenger.css",
                "~/Content/Style/Plugins/messenger-theme-block.css",
                "~/Content/Style/Plugins/messenger-spinner.css"
            ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.

            bundles.Add(new ScriptBundle("~/Scripts/LandingPageV3/landingpageV3JS").Include(
                "~/Scripts/LandingPageV3/Plugins/jquery-1.8.3.js",
                "~/Scripts/LandingPageV3/Plugins/i18next-1.7.1.js",
                "~/Scripts/LandingPageV3/Plugins/bootstrap.js",
                "~/Scripts/LandingPageV3/Plugins/raphael.js",
                "~/Scripts/LandingPageV3/Plugins/g.raphael.js",
                "~/Scripts/LandingPageV3/Plugins/knockout-2.3.0-nonminified.js",
                "~/Scripts/LandingPageV3/Plugins/jquery-ui-1.10.3.custom.js",
                "~/Scripts/lib/core.js",
                "~/Scripts/LandingPageV3/plugins/jquery.placeholder.js",
                "~/Scripts/VHM/WelcomeJourney.js",
                "~/Scripts/lib/moment.js",
                "~/Scripts/lib/messenger.js",
                "~/Scripts/LandingPageV3/jqModalV3.js",
                "~/Scripts/VHM/Social/social_notifications.js",
                "~/Scripts/LandingPageV3/social_newsfeed.js",
                "~/Scripts/LandingPageV3/VHMNotificationsV3.js",
                "~/Scripts/LandingPageV3/LapsGameDisplay.js",
                "~/Scripts/LandingPageV3/LevelsGraphDisplay.js",
                "~/Scripts/LandingPageV3/ActivityTab.js",
                "~/Scripts/LandingPageV3/ActivityChallengesAccordion.js",
                "~/Scripts/LandingPageV3/PersonalTrackingTab.js",
                "~/Scripts/LandingPageV3/GraphPlugin.js",
                "~/Scripts/LandingPageV3/main.js",
                "~/Scripts/lib/ConversionUtil.js",
                "~/Scripts/VHM/utils.js",
                "~/Scripts/lib/inflection.js"
                ));

            //bundles.Add(new StyleBundle("~/Content/Sponsor").Include(
            //    "~/Content/Styles/*.css"));
        }
        
    }
}
