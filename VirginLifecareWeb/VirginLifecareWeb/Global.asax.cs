﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using Vhm.Web.Common;
using VirginLifecareWeb.App_Start;


namespace VirginLifecareWeb
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
           
            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );
            routes.MapRoute(
                "ShortDefault", // Route name
                "{controller}/Index/{id}", // URL with parameters
                new { controller = "Challenge", action = UrlParameter.Optional, id = UrlParameter.Optional } // Parameter defaults
            );
            routes.MapRoute(
                "BlogRoute", // Route name
                "{controller}/BlogArticle/{id}", // URL with parameters
                new { controller = "Content", action = UrlParameter.Optional, id = UrlParameter.Optional } // Parameter defaults
            );
            routes.MapRoute(
                "EngagementArticle", // Route name
                "{controller}/Engagement/{id}", // URL with parameters
                new { controller = "Content", action = UrlParameter.Optional, id = UrlParameter.Optional } // Parameter defaults
            );
            

        }
        protected void Session_End(Object sender, EventArgs e)
        {
            Response.Cookies.Add(new HttpCookie("SessionId", null));
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            ViewEngines.Engines.Add(new RazorViewEngine() { PartialViewLocationFormats = new string[] { "~/Views/Shared/Member/{0}.cshtml" } });
            ViewEngines.Engines.Add(new RazorViewEngine() { PartialViewLocationFormats = new string[] { "~/Views/Shared/PartialViews/{0}.cshtml" } });

            //BundleTable.EnableOptimizations = false;
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Vhm.Web.BL.AutoMapperConfig.Intialize();

            MvcHandler.DisableMvcResponseHeader = true;

            //string configFilePath = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "App_Data", "CacheConfig.xml");
            //XmlDocument configDoc = new XmlDocument();
            //configDoc.Load(configFilePath);
           // MemberAdapter ma = new MemberAdapter();
           // ma.LoadKeyManager(configDoc.InnerXml);
            var dict = new Dictionary<string, object>();
            foreach (var prop in typeof(ApplicationSettings).GetProperties()) {

                object value = prop.GetValue(prop.Name, null);
                dict.Add(prop.Name, value);
            }
           System.Web.HttpContext.Current.Application["ApplicationSetting"]=dict;
          
        }

        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            const int TWO_MINUTES = 120;
            string refreshInterval =
                ( FormsAuthentication.Timeout.TotalSeconds + TWO_MINUTES ).ToString( CultureInfo.InvariantCulture );
            Response.AddHeader("Refresh", refreshInterval);

        }

        protected void Application_EndRequest()
        {
            if (Context.Response.StatusCode == 404)
            {
                Response.Clear();

                Response.Redirect("/Error/Pages/notfound.aspx");
            }

            if (Context.Response.StatusCode == 500)
            {
                Response.Clear();

                Response.Redirect("/Error/Pages/error.aspx");
            }
        }

        protected void Application_Error(Object sender, EventArgs e)
        {
            
            
            if (IsMaxRequestExceededEexception(Server.GetLastError()))
            {
                string url = string.Format(ApplicationSettings.UploadFileErrorURL);
                Response.Redirect(url);
            }
           
        }

        const int TimedOutExceptionCode = -2147467259;
        public static bool IsMaxRequestExceededEexception(Exception e)
        {
            // unhandeled errors = caught at global.ascx level
            // http exception = caught at page level

            Exception main;
            var unhandeled = e as HttpUnhandledException;

            if (unhandeled != null && unhandeled.ErrorCode == TimedOutExceptionCode)
            {
                main = unhandeled.InnerException;
            }
            else
            {
                main = e;
            }
            var http = main as HttpException;

            if (http != null && http.ErrorCode == TimedOutExceptionCode)
            {
                // hack: no real method of identifing if the error is max request exceeded as 
                // it is treated as a timeout exception
                if (http.StackTrace.Contains("GetEntireRawContent"))
                {
                    // MAX REQUEST HAS BEEN EXCEEDED
                    return true;
                }
            }
            return false;
        }
    }
}
