﻿$(document).ready(function () {
    if ($('#personal-tracking').html() != undefined) {
        ko.applyBindings(new ChallengeModel(), document.getElementById("personal-tracking"));
    }

});

var ChallengeModel = function () {
    var self = this;

    self.Challenges = ko.observableArray([]);
    self.ChallengeQuestion = ko.observable('');
    self.HaveChallenges = ko.observable(false);
    self.ActiveChallengesExist = ko.observable(false); //challenge that started
    self.DisplayNoChallengesMessage = ko.observable(true);

    self.SaveAnswer = function (data, event) {
        $(".question-text-field").removeAttr("style");
        var domElement = $(event.target).parent();
        var value = $.trim($(event.target).data("value"));
        var me = data.TrackedDaysInfo.TrackedDays[0];
        

        /* Check If this is one time only event*/
        if (data.ChallengeQuestions[0].QuestionFrequencyDatePart == 'yy' && data.ChallengeQuestions[0].QuestionFrequency == 255) {
            var me = data.TrackedDaysInfo.TrackedDays[0];
        } else if (data.ChallengeQuestions[0].QuestionFrequencyDatePart == 'wk' && data.ChallengeQuestions[0].QuestionFrequency == 1) {
           
            var today = new Date();
            var i = 0;
            var isFirstIteration = true;
            var incrementValue = 6;
           
            for (var i = 0; i < this.TrackedDaysInfo.TrackedDays.length; i = i + incrementValue) {
                if (isFirstIteration) {

                    if (this.TrackedDaysInfo.TrackedDays[i + 6] == undefined) {
                        this.lastTrackedDayData = this.TrackedDaysInfo.TrackedDays[0];
                        this.lastTrackedDayData.answer = parseInt(this.TrackedDaysInfo.TrackedDays[0].answer);
                        isFirstIteration = false;
                        break;
                    } 
                    
                    var sameDayNextWeek = data.TrackedDaysInfo.TrackedDays[i + 6].Day;
                    var trackedDayParsedToDate = new Date( sameDayNextWeek );
                    if ( trackedDayParsedToDate > today ) {
                        me = data.TrackedDaysInfo.TrackedDays[0];
                        isFirstIteration = false;
                        break;
                    }
                    isFirstIteration = false;
                } else {
                    
                    if (this.TrackedDaysInfo.TrackedDays[i + 7] == undefined) {
                        this.lastTrackedDayData = this.TrackedDaysInfo.TrackedDays[i];
                        this.lastTrackedDayData.answer = parseInt(this.TrackedDaysInfo.TrackedDays[i].answer);
                        isFirstIteration = false;
                        break; 
                    } 
                    var sameDayNextWeek = data.TrackedDaysInfo.TrackedDays[i + 7].Day;
                    var trackedDayParsedToDate = new Date( sameDayNextWeek );
                    if ( trackedDayParsedToDate > today ) {
                        me = data.TrackedDaysInfo.TrackedDays[i];
                        break; 
                    }
                    incrementValue = 7;
                }
               
            }
            
        } else {
            var me = data.TrackedDaysInfo.TrackedDays[data.TrackedDaysInfo.TodaysTrackedDayIndex];

        }

        if (value == 'track') {
            var min = data.ChallengeQuestions[0].ChallengeAnswerKeys[0].EntryMin;
            var max = data.ChallengeQuestions[0].ChallengeAnswerKeys[0].EntryMax;
        }

        switch (value) {
            case 'yes':
                me.Answer = 0;
                break;
            case 'no':
                me.Answer = 1;
                break;
            default:
                var number = domElement.find('input').val();
                if (number == "" || number < 0) {
                    number = 0;
                }
                me.Answer = parseInt(number);
        }

        me.HitTarget = ko.computed(function () {
            var nAnswer = parseInt(me.Answer);

            if (data.ChallengeQuestions[0].AnswerType == 'YNO') {
                return me.Answer == '0';
            } else {
                return (nAnswer >= min && nAnswer <= max);
            }

        }, this);



        var updatedData = {
            challengeScoreId: me.ChallengeScoreId,
            challengeId: me.ChallengeId,
            answer: me.Answer,
            score: me.HitTarget() ? 1 : 0,
            scoreDate: me.Day,
            isMemberAnswer: me.IsMemberAnswer
        };

        $.ajax({
            url: '../Challenges/SaveChallengeScore',
            data: updatedData,
            cache: false,
            success: function (score) {
                me.IsMemberAnswer = true;
                me.ChallengeScoreId = score;

                if (data.IsYesNoAnswer) {
                    if (me.Answer == 0)
                        $(event.target).closest("div.question-ph").find(".change-answer span").text("Yes");
                    else {
                        $(event.target).closest("div.question-ph").find(".change-answer span").text("No");
                    }
                } else {
                    $(event.target).closest("div.question-ph").find(".change-answer span").text(me.Answer);
                }
                ToggleAnswerOptions(me.ChallengeId);
            },
            error: function () {
                alert("Error");
            }
        });
    };

    self.Validate = function (item, evt) {

        evt = evt || window.event;
        var charCode = evt.which || evt.keyCode;
        var charStr = String.fromCharCode(charCode);
        var CHAR_DELETE = 46;
        var CHAR_BACKSPACE = 8;
        var CHAR_TAB = 9;
        var CHAR_ENTER = 13;

        if (/\d/.test(charStr) == false && charCode != CHAR_DELETE && charCode != CHAR_BACKSPACE && charCode != CHAR_TAB && charCode != CHAR_ENTER) {
            $(evt.target).css("border", "1px solid red");
            return false;
        }
        $(evt.target).removeAttr("style");
        return true;
    };

    self.GetModel = function () {
        loadChallenge();
    };


    var loadChallenge = function () {
        $.ajax({
            url: '../Challenges/GetChallenges',
            cache: false,
            success: function (data) {
                if (data.length > 0) {

                    $(data).each(function () {


                        if (this.HasFinished) {
                            this.toDisplay = false;
                        } else if (this.HasStarted && !this.HasFinished) {
                            this.toDisplay = true;
                        } else if (!this.HasStarted && !this.HasFinished) {
                            this.toDisplay = false;
                        }
                        
                        /*Cheking if there are only challenges with start day in future returned.
                        * loop throuh all challenges and check if they should be displayed
                        * if there is no any challenge that has started , display appropriate message*/
                        if (self.ActiveChallengesExist() == false) {
                            if (this.toDisplay == true) {
                                self.ActiveChallengesExist(true);
                        } else if (  (this.toDisplay == false) || !this.HasFinished ) {
                                self.ActiveChallengesExist(false);
                             }
                        }

                        /* Check If this is one time only event*/

                        if (this.ChallengeQuestions[0].QuestionFrequencyDatePart == 'yy' && this.ChallengeQuestions[0].QuestionFrequency == 255) {
                            this.lastTrackedDayData = this.TrackedDaysInfo.TrackedDays[0];
                            this.lastTrackedDayData.answer = parseInt(this.TrackedDaysInfo.TrackedDays[0].answer);
                        } else if (this.ChallengeQuestions[0].QuestionFrequencyDatePart == 'wk' && this.ChallengeQuestions[0].QuestionFrequency == 1) {

                            var today = new Date();
                            var i = 0;
                            var isFirstIteration = true;
                            var incrementValue = 6;

                                for (var i = 0; i < this.TrackedDaysInfo.TrackedDays.length; i = i + incrementValue) {

                                    if (isFirstIteration) {

                                        if ( this.TrackedDaysInfo.TrackedDays[i + 6] == undefined) {
                                            this.lastTrackedDayData = this.TrackedDaysInfo.TrackedDays[0];
                                            this.lastTrackedDayData.answer = parseInt(this.TrackedDaysInfo.TrackedDays[0].answer);
                                            isFirstIteration = false;
                                            break;
                                        }
                                    
                                        var sameDayNextWeek = this.TrackedDaysInfo.TrackedDays[i + 6].Day;
                                        var trackedDayParsedToDate = new Date(sameDayNextWeek);
                                        if (trackedDayParsedToDate > today) {

                                            this.lastTrackedDayData = this.TrackedDaysInfo.TrackedDays[0];
                                            this.lastTrackedDayData.answer = parseInt(this.TrackedDaysInfo.TrackedDays[0].answer);
                                            isFirstIteration = false;
                                            break;
                                        }
                                        isFirstIteration = false;
                                    } else {

                                        if (this.TrackedDaysInfo.TrackedDays[i + 7] == undefined) {
                                            this.lastTrackedDayData = this.TrackedDaysInfo.TrackedDays[i];
                                            this.lastTrackedDayData.answer = parseInt(this.TrackedDaysInfo.TrackedDays[i].answer);
                                            isFirstIteration = false;
                                            break;
                                        }
                                        var sameDayNextWeek = this.TrackedDaysInfo.TrackedDays[i + 7].Day;
                                    
                                        var trackedDayParsedToDate = new Date(sameDayNextWeek);
                                        if (trackedDayParsedToDate > today) {
                                            this.lastTrackedDayData = this.TrackedDaysInfo.TrackedDays[i];
                                            this.lastTrackedDayData.answer = parseInt(this.TrackedDaysInfo.TrackedDays[i].answer);
                                            break;
                                        }
                                        incrementValue = 7;
                                    }
                                                                 
                                }
                        } else {
                            this.lastTrackedDayData = this.TrackedDaysInfo.TrackedDays[this.TrackedDaysInfo.TodaysTrackedDayIndex];
                            this.lastTrackedDayData.answer = parseInt(this.TrackedDaysInfo.TrackedDays[this.TrackedDaysInfo.TodaysTrackedDayIndex].answer);

                        }

                        try{
                            var tempString = this.BadgeFileName;
                            var tempStrings = tempString.split("/");
                            var lastItem = tempStrings.length;
                            this.BadgeID = tempStrings[lastItem-1];
                        } catch (err) {
                            this.BadgeID = "";
                        }

                        this.Url = "/challenge/" + this.ChallengeId;
                        
                        if (this.TrackedDaysInfo.TrackedDays.length > 0) {
                            this.lastTrackedDayData.Answer = parseInt(this.lastTrackedDayData.Answer);
                            if (this.ChallengeQuestions[0].AnswerType == 'YNO') {
                                if (this.lastTrackedDayData.Answer == 0) {
                                    this.lastTrackedDayData.Answer = "Yes";
                                } else {
                                    this.lastTrackedDayData.Answer = "No";
                                }
                                this.IsYesNoAnswer = true;
                            } else {
                                if (isNaN(this.lastTrackedDayData.Answer)) {
                                    this.lastTrackedDayData.Answer = 0;
                                }
                                this.IsYesNoAnswer = false;
                            }

                            if (this.lastTrackedDayData.IsMemberAnswer == true) {
                                this.IsAnswered = true;
                            } else {
                                this.IsAnswered = false;
                            }
                        } else {

                            var dataArray = [];
                            dataArray.push({
                                Answer: "No"
                            });

                            this.TrackedDaysInfo.TrackedDays.push(dataArray);

                            if (this.ChallengeQuestions[0].AnswerType == 'YNO') {
                                if (this.lastTrackedDayData.Answer == 0) {
                                    this.lastTrackedDayData.Answer = "Yes";
                                } else {
                                    this.lastTrackedDayData.Answer = "No";
                                }
                                this.IsYesNoAnswer = true;
                            } else {
                                if (isNaN(this.lastTrackedDayData.Answer)) {
                                    this.lastTrackedDayData.Answer = 0;
                                }
                                this.IsYesNoAnswer = false;
                            }
                        }
                    });
                    self.HaveChallenges(true);
                    self.Challenges(data);

                    if (self.HaveChallenges() == true && self.ActiveChallengesExist() == true) {
                        self.DisplayNoChallengesMessage(false);

                    } else if (self.HaveChallenges() == true && self.ActiveChallengesExist() == false) {
                        self.DisplayNoChallengesMessage(true);
                    } else if (self.HaveChallenges() == false) {
                        self.DisplayNoChallengesMessage(false);
                    }
                   
                    HideAnswerOptionsIfAnswered(data);
                    InitializeChangeAnswer();
                }
            },
            error: function () {
                //console.log("uh oh, couldn't get challenges");
            }
        });
    };

    self.GetModel();

};

function HideAnswerOptionsIfAnswered(data) {
    $(data).each(function () {
        if (this.IsAnswered) {
            $(".tracking-item[data-challengeid='" + this.ChallengeId + "']").find(".change-answer").show();
            $(".tracking-item[data-challengeid='" + this.ChallengeId + "']").find(".new-answer").hide();
        } else {
            $(".tracking-item[data-challengeid='" + this.ChallengeId + "']").find(".change-answer").hide();
            $(".tracking-item[data-challengeid='" + this.ChallengeId + "']").find(".new-answer").show();

        }
    });
};

function InitializeChangeAnswer() {
    $(".change-answer a").click(function (e) {
        var challengeId = $(this).closest("div.tracking-item").data("challengeid");
        ToggleAnswerOptions(parseInt(challengeId));
        e.preventDefault();
    });
}

function ToggleAnswerOptions(challengeId) {
    var changeAnswer = $(".tracking-item[data-challengeid='" + challengeId + "']").find(".change-answer");
    var newAnswer = $(".tracking-item[data-challengeid='" + challengeId + "']").find(".new-answer");

    changeAnswer.toggle('very fast');
    newAnswer.toggle('very fast');

    //    if ( changeAnswer.is( ":visible" ) == true ) {
    //            changeAnswer.hide();
    //            newAnswer.show('slow').animate('margin-left',0);
    //            changeAnswer.css( 'margin-left', '500px' );
    //    } else {
    //            newAnswer.hide();
    //            changeAnswer.show('slow').animate('margin-left',0);
    //            newAnswer.css( 'margin-left', '500px' );
    //    }
};
