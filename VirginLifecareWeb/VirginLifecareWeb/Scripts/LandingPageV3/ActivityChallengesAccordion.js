﻿$(function () {
    onPageLoad();
});

function onPageLoad() {
    if ($('#activity-challenges').html() != undefined) {
        //        ko.setTemplateEngine(new ko.nativeTemplateEngine());
        ko.applyBindings(new ActivityChallengeModel(), document.getElementById("activity-challenges"));
    }
}
var ActivityChallengeModel = function () {
    var self = this;

    self.Creator = ko.observable();
    self.Rewards = ko.observableArray([]);
    self.ActivityChallenges = ko.observableArray([]);

    self.CalculateDaysBetween = function (startingDate, endingDate) {
        var startDateConvertedToMomentsFormats = moment(startingDate);
        var endDateConvertedToMomentsFormats = moment(endingDate);

        var totalDifferenceInDays = Math.abs(endDateConvertedToMomentsFormats.diff(startDateConvertedToMomentsFormats, 'days'));
        return totalDifferenceInDays;

    };

    self.GetModel = function () {
        loadChallenges();
    };

    self.parseDateString = function (dateString) {
        var a = dateString.split('T'),
            year = a[0].split('-')[0],
            month = a[0].split('-')[1],
            day = a[0].split('-')[2],
            hour = a[1].split(':')[0],
            min = a[1].split(':')[1];

        return new Date(year, month - 1, day, hour, min);
    };

    self.GetOridinal = function (num) {
        var mod1 = num % 100; // get the divided "remainder" of 100
        var mod2 = num % 10; // get the divided "remainder" of 10
        var ord; // ste the ordinal variable

        if ((mod1 - mod2) == 10) { // capture 10
            ord = "th"; // set the oridnal to th as in: 10th
        } else { // for everything else
            switch (mod2) {
                // check the remainder of the 10th place                         
                case 1:
                    // if 1 as in 1st
                    ord = "st"; // set the ordinal
                    break;
                case 2:
                    // if 2 as in 2nd
                    ord = "nd"; // set the ordinal
                    break;
                case 3:
                    // if 3 as in 3rd
                    ord = "rd"; // set the ordinal
                    break;
                default:
                    // for everything else
                    ord = "th"; // set the ordinal
                    break;
            }

        }
        return ord;
    };


    var API_BASE_URL = location.protocol + '//' + location.host + '/rest/v1/challenges?type=device';

    var loadChallenges = function () {
        $.ajax({
            url: API_BASE_URL,
            type: "GET",
            success: function (challenges) {
                if (challenges.length > 0) {
                    $(challenges).each(function () {
                        self.Rewards = this.Rewards;
                        self.Creator = this.Creator;
                        this.ChallengeFinished = false;
                        this.Url = "/secure/challenge/rankings.aspx?challengeid=" + this.ChallengeID;

                        //Calculate the full duration of game in days
                        this.GameLengthInDays = self.CalculateDaysBetween(self.parseDateString(this.StartDate), self.parseDateString(this.EndDate));

                        //Calculate how many days there is left in the game
                        this.DaysUntilEnd = self.CalculateDaysBetween(new Date(), self.parseDateString(this.EndDate));

                        //Calculate the actual width in percentages that has alreay passed
                        this.RemainingWidthAsInteger = (((this.GameLengthInDays - this.DaysUntilEnd) / this.GameLengthInDays) * 100);
                        if (this.RemainingWidthAsInteger > 100)
                            this.RemainingWidthAsInteger = 100;
                        if (this.RemainingWidthAsInteger < 1)
                            this.RemainingWidthAsInteger = 1;

                        // This is the width that is passed already
                        this.PassedWidth = this.RemainingWidthAsInteger + '%';

                        // What remains in percentages is calculated below
                        this.Width = (100 - this.RemainingWidthAsInteger) + '%';

                        this.OridinalParticipantRank = self.GetOridinal(this.ParticipantRank);
                        this.OridinalTeamRank = self.GetOridinal(this.TeamRank);

                        this.Width = isNaN(parseInt(this.Width)) ? "" : this.Width;
                        this.DaysUntilEnd = isNaN(parseInt(this.DaysUntilEnd)) ? "" : this.DaysUntilEnd;

                        if (this.TeamRank > 0 && this.TeamCount > 0) {
                            this.ShouldShowTeamRanks = true;
                        }
                        else {
                            this.ShouldShowTeamRanks = false;
                        }

                        if (this.ParticipantRank > 0 && this.ParticipantCount > 0) {
                            this.ShouldShowYourRanks = true;
                        }
                        else {
                            this.ShouldShowYourRanks = false;
                        }

                        if (this.Rewards[0] != undefined) {
                            this.BadgeImageUrl = this.Rewards[0].BadgeUrl;
                        } else {
                            this.BadgeImageUrl = "";
                        }
                        var Today = new Date();
                        if (Today > self.parseDateString(this.EndDate)) {
                            this.ChallengeFinished = true;
                            this.Width = 0;
                            this.PassedWidth = 0;
                            this.RemainingWidthAsInteger = 0;

                        }
                    });

                    self.ActivityChallenges(challenges);
                }
            },
            error: function (error) {
            }
        });
    };
    self.GetModel();
};


