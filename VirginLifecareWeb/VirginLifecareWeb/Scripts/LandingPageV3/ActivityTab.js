﻿VHM = VHM || {};
VHM.AJ = VHM.AJ || {};
VHM.AJ.type = "steps";
VHM.AJ.CaloriesStepsGraphDays = new Array();
VHM.AJ.Activities = [];
VHM.AJ.wsNamespace = "/VirginLifeCare/WebServices/";
VHM.AJ.wsUrl = '/WebServices/WSVHMActivityJournal.asmx/';
VHM.AJ.API_BASE = location.protocol + '//' + location.host;
VHM.AJ.API_PROFILE_URL = VHM.AJ.API_BASE + '/rest/v1/profiles';
VHM.AJ.API_ACTIVITIES_URL = VHM.AJ.API_BASE + '/rest/v1/activities/';
VHM.AJ.MemberActivityList = null;
VHM.AJ.ActivityEntries = [];
VHM.AJ.LastEntry = {};
//Member profile data
VHM.AJ.Age = 0;
VHM.AJ.Weight = 0;
VHM.AJ.StepsTarget = 0;
VHM.AJ.CaloriesTarget = 0;
//light, moderate & vigorous metabolic equivalent
VHM.AJ.LME = 0;
VHM.AJ.MME = 0;
VHM.AJ.VME = 0
VHM.AJ.selectedActivityItems = 0;
VHM.AJ.Activity = function (id, name, selected, isCustom) {
    this.Id = id;
    this.Name = name;
    this.Selected = selected;
    this.IsCustom = isCustom;
};

VHM.AJ.Intensity = function (id, name) {
    this.Id = id;
    this.Name = name;
};

VHM.AJ.Category = function (id, name) {
    this.Id = id;
    this.Name = name;
    this.Activities = [];
};

VHM.AJ.Category.prototype = {
    addActivity: function (id, name, selected, isCustom) {
        this.Activities.push(new VHM.AJ.Activity(id, name, selected, isCustom));
    }
};

VHM.AJ.callWs = function (methodName, contentType, dataType, data, success, error) {
    $.ajax({
        type: 'POST',
        url: VHM.AJ.wsUrl + methodName,
        contentType: contentType,
        dataType: dataType,
        data: data,
        success: function (responseData, textStatus) {
            if (typeof success === "function") {
                success(responseData, textStatus);
            }

        },
        error: error
    });
};

VHM.AJ.callRESTService = function (methodName, methodType, data, success, error) {
    $.ajax({
        type: methodType,
        url: methodName,
        data: data,
        success: function (responseData, textStatus) {
            if (typeof success === "function") {
                success(responseData, textStatus);
            }
        },
        error: function (response) {
            error(response);
        }
    });
}

VHM.AJ.onKeyPressOnlyNum = function (e) {
    e = e || window.event;
    var keyunicode = e.charCode || e.keyCode;
    return keyunicode >= 48 && keyunicode <= 57 || keyunicode == 8;
};

VHM.AJ.onKeyPressOnlyNumDec = function (o, e) {
    e = e || window.event;
    var keyunicode = e.charCode || e.keyCode;
    return keyunicode >= 48 && keyunicode <= 57 || keyunicode == 8 || (o.value.indexOf(".") == -1 && keyunicode == 46);
};

VHM.AJ.GetMemberProfile = function() {
    var f_success = function( data ) {
        VHM.AJ.Age = data.Age;
        VHM.AJ.Weight = data.Weight;
        VHM.AJ.LME = data.MetabolicEquivalents.Light;
        VHM.AJ.MME = data.MetabolicEquivalents.Moderate;
        VHM.AJ.VME = data.MetabolicEquivalents.Vigorous;
        VHM.AJ.StepsTarget = data.Goals.Steps;
        VHM.AJ.CaloriesTarget = data.Goals.CaloriesBurned;
        VHM.AJ.GetRecentEntries();
    };

    var f_error = function( xhr ) {
        if ( xhr.status == 500 ) {
           
            $(".activity-data-loading-div").hide();
        } else {
            var text = jQuery.parseJSON( xhr.responseText );
            alert( text.Message );
        }
    };

    VHM.AJ.callRESTService( VHM.AJ.API_PROFILE_URL, "GET", null, f_success, f_error );
};

VHM.AJ.GetRecentEntries = function() {
    var currentDate = new Date();
    currentDate = moment( currentDate ).subtract( "days", 6 ).format( "MM/DD/YYYY" );
    var f_success = function( data ) {
        VHM.AJ.ActivityEntries = data;
        LoadGraphData();
    };

    var f_error = function (xhr) {
        if ( xhr.status == 500 ) {
           
            $( ".activity-data-loading-div" ).hide();
        }else{
            var text = jQuery.parseJSON( xhr.responseText );
            alert(text.Message);
            $('.activity-data-loading-div').hide();
        }
    };
    VHM.AJ.callRESTService( VHM.AJ.API_ACTIVITIES_URL + "entries?start=" + currentDate, "GET", null, f_success, f_error );

};

$(document).ready(function () {
    VHM.AJ.GetMemberProfile();

    $("#txtALSteps").hide();
    $("#LabelALSteps").hide();
    $(".activity-graph").hide();
    $('.activity-data-loading-div').show();

    if (document.getElementById("activity-graph-container")) {
        VHM.AL.OpenAddEntryDlgAL(null, true);
    }


    $('#AL-btnSave').click(function () {
        var activityText = ($('#AL-ActivityList').find(":selected").text());
        var validDataActivity = false;
        var validDataDuration = false;
        var validDataSteps = true;
        var activityList = $('#AL-ActivityList').val();
        if ($('#AL-ActivityList').val() == null || $('#AL-ActivityList').val().length === 0 || $('#AL-ActivityList').val() == 'add') {
            $('.activity-label.activity').css('color', 'red');
            validDataActivity = false;
        }
        else {
            $('.activity-label.activity').css('color', '#666666');
            validDataActivity = true;
        }

        if (($('#txtALActivityHours').val().length === 0 || $('#txtALActivityHours').val() == 0) && ($('#txtALActivityMinutes').val().length === 0 || $('#txtALActivityMinutes').val() == 0)) {
            /*Added additional checking. If activiyty type is pedometer, duration is note required field*/
            if (activityText != "Pedometer") {
                $('.activity-label.duration').css('color', 'red');
                validDataDuration = false;
            } else {
                $('.activity-label.duration').css('color', '#666666');
                validDataDuration = true;
                $("#txtALActivityHours").val(0);
                $("#txtALActivityMinutes").val(0);
            }
        } else {
            $('.AL-DurationLabel').css('color', '#666666');
            validDataDuration = true;
        }

        /*Checking is Steps value for pedometer set*/
        if (activityText == "Pedometer") {
            if (($('#txtALSteps').val() == null || $('#txtALSteps').val() == 0)) {
                $('#LabelALSteps').css('color', 'red');
                validDataSteps = false;
            } else {
                validDataSteps = true;
            }

        } else {
            validDataSteps = true;
        }
        if (validDataDuration == true && validDataActivity == true && validDataSteps == true) {
            $('#AL-btnSave').val(i18n.t("WolverineLandingPage.GlobalLabels.Saving", { defaultValue: "Saving..." })).attr('disabled', 'disabled');
            $(".NewEntry-ph").hide();
            VHM.AL.SaveEntry();
            return true;
        }
        else
            return false;
    });


    //$('#inputDateAL-AddEntry').datepicker({ maxDate: new Date });

    CreateActivityCalendar();

    $('.ui-datepicker-trigger.al-date2').click(function () {
        $('#inputDateAL-AddEntry').focus();
    });

    $('#ui-datepicker-div').css('clip', 'auto');

    $("#AL-btnBack").click(function (e) {
        $(".activity-message").hide();
        $(".acitvity-form-placeholder").slideDown();
        e.preventDefault();
    });
    $("#AL-dismiss").click(function (e) {
        VHM.AL.ClearEntryData();
        e.preventDefault();
    });

    $('.button-additional-options').on('click', function () {
        $('.additional-options-ph').slideToggle();
    });

    $('#StepsSwitch').on('click', function () {
        try {
            VHM.GraphHandler.remove();
        } catch (e) {
        }
        $(this).addClass('active');
        $('#CaloriesSwitch').removeClass('active');
        VHM.AJ.type = "steps";
        LoadGraphData();
    });
    $('#CaloriesSwitch').on('click', function () {
        try {
            VHM.GraphHandler.remove();
        } catch (e) {

        }
        $(this).addClass('active');
        $('#StepsSwitch').removeClass('active');
        VHM.AJ.type = "calories";
        LoadGraphData();
    });


});

VHM.AL = VHM.AL || {};
VHM.AL.ClearEntryData = function () {
    $('#txtALActivityHours').val('');
    $('#txtALActivityMinutes').val('');
    $('#txtALDistance').val('');
    $('.AL-New-Entry label').css('color', '#666666');
    $('#AL-ActivityList').val('');
    $('#AL-btnSave').val(i18n.t("WolverineLandingPage.GlobalLabels.Save", { defaultValue: "Save" }));
    $('#AL-btnSave').prop("disabled", false);
    $("#LabelALSteps").hide();
    $("#txtALSteps").val("");
    $("#txtALSteps").hide();
};
VHM.AL.CalsCalculatingV2 = false;
VHM.AL.CalculateCalsFailedV2 = false;
VHM.AL.CalsCalculatedV2 = false;

VHM.AL.GetCalcCaloriesV2 = function () {
    if (!VHM.AL.CalsCalculatingV2) {
        VHM.AL.CalculateCalsFailedV2 = false;
        VHM.AL.CalsCalculatingV2 = true;
        VHM.AL.CalsCalculatedV2 = false;
        var intensity = parseInt($("#ddAlIntensity").val());
        var durationMin;
        var durationHour;
        if ($('#AL-ActivityList').find(":selected").text() == "Pedometer" &&
            (($('#txtALActivityHours').val().length === 0 || $('#txtALActivityHours').val() == 0) &&
           ($('#txtALActivityMinutes').val().length === 0 || $('#txtALActivityMinutes').val() == 0))) {
            durationHour = 0;
            durationMin = 0;
        } else {
            durationHour = parseInt($("#txtALActivityHours").val());
            durationMin = parseInt($("#txtALActivityMinutes").val());
        }

        durationMin = (isNaN(durationHour) ? 0 : durationHour * 60) + (isNaN(durationMin) ? 0 : durationMin);

        if (durationMin >= 0 && !isNaN(intensity)) {
            var calories = VHM.AL.CalcCaloriesV2(VHM.AJ.Weight, VHM.AJ.Age, intensity, durationMin);
        } else {
            VHM.AL.CalsCalculatingV2 = false;
        }

        if (calories > 0) {
            VHM.AL.CalsCalculatedV2 = true;
            VHM.AL.CalsCalculatingV2 = false;
            $("#txtACLCalories").val(parseInt(calories));
        }
        else {
            VHM.AL.CalculateCalsFailedV2 = true;
            VHM.AL.CalsCalculatingV2 = false;
        };
    }
};

VHM.AL.CalcCaloriesV2 = function (weight, age, intensity, duration) {
    var activityText = ($('#AL-ActivityList').find(":selected").text());
    var caloriesBurned;
    var data = { weight: weight, age: age, intensity: intensity, duration: duration };

    if (activityText == "Pedometer") {
        caloriesBurned = VHM.AL.CalcPedometerCalories(weight, age, intensity);
        VHM.AL.CalsCalculatingV2 = false;
    } else {
        switch (intensity) {
            case 95577:
                caloriesBurned = ((VHM.AJ.LME * 1.587572 * weight) / 200) * duration
                break;
            case 95576:
                caloriesBurned = ((VHM.AJ.MME * 1.587572 * weight) / 200) * duration
                break;
            case 95572:
                caloriesBurned = ((VHM.AJ.VME * 1.587572 * weight) / 200) * duration
                break;
            default:
                caloriesBurned = ((VHM.AJ.MME * 1.587572 * weight) / 200) * duration
                break;

        }
    }

    return caloriesBurned;
};

VHM.AL.CalcPedometerCalories = function (weight, age, intensity) {

    var caloriesBurnedInOneMile = weight * 0.57;
    var conversionFactor;
    if (intensity == 95577) {
        conversionFactor = caloriesBurnedInOneMile / 2000;
    } else if (intensity == 95576) {
        conversionFactor = caloriesBurnedInOneMile / 1750;
    } else if (intensity == 95572) {
        conversionFactor = caloriesBurnedInOneMile / 1500;
    }
    var steps = ($('#txtALSteps').val());
    var caloriesBurned = steps * conversionFactor;

    return caloriesBurned;
}

VHM.AJ.LastEnteredActivityEntryDate = null;
VHM.AL.SaveEntry = function( callback ) {
    var saveEntry = function( callback, error ) {
        $( ".loading-div" ).show();
        var $selActivity = $( "#AL-ActivityList" );
        var $txtDate = $( "#inputDateAL-AddEntry" );
        var $selIntensity = $( "#ddAlIntensity" );
        var $txtDurationHour = $( "#txtALActivityHours" );
        var $txtDurationMin = $( "#txtALActivityMinutes" );
        var $txtDistance = $( "#txtALDistance" );
        var $txtCalories = $( "#txtACLCalories" );
        var $txtDate = $( "#inputDateAL-AddEntry" );
        var $txtNotes = $( "#txtALNotes" );
        var $txtSteps = $( "#txtALSteps" );
        var activityId = $selActivity.val();
        var isCustom = $( "#AL-ActivityList option:selected" ).attr( "iscustom" );

        var date = $txtDate.val();
        var intensity = parseInt( $selIntensity.val() );
        var durationHour = parseInt( $txtDurationHour.val() );
        durationHour = isNaN( durationHour ) ? 0 : durationHour;
        var durationMin = parseInt( $txtDurationMin.val() );
        durationMin = isNaN( durationMin ) ? 0 : durationMin;
        var distance = parseFloat( $txtDistance.val() );
        distance = isNaN( distance ) ? 0 : distance;
        var calories = parseInt( $txtCalories.val() );
        var notes = $txtNotes.val();
        var steps = $txtSteps.val();
        if ( steps == null || steps == "" )
            steps = 0;
        var duration = durationHour * 60 + durationMin;
        var data = { activityID: activityId, Date: date, intensity: intensity, duration: duration, distance: distance, calories: calories, note: notes, steps: steps };
        var methodName = "entries";
        var methodType = "POST";
        var f_success = function( data ) {
            VHM.AJ.LastEntry = data;
            callback = null;
            VHM.AL.ClearEntryData();
            $('#AL-btnSave').val(i18n.t("WolverineLandingPage.GlobalLabels.Save", { defaultValue: "Save" }));
            $( '#AL-btnSave' ).prop( "disabled", false );
            $( "#txtALNotes" ).val( '' );
            $( ".loading-div" ).hide();

            // Reset form
            $( '#txtACLCalories' ).val( '' );
            VHM.AL.ClearEntryData();

            activityDaysCalories = {};
            activityDaysSteps = {};
            VHM.GraphHandler.remove();
            $( '#CaloriesSwitch' ).trigger( 'click' );

            var htmlMessage = '<i class="icon-ok" style="font-size:15px; color:#468847;margin-right:5px;"></i><strong  data-i18n="WolverineLandingPage.GlobalLabels.Success">Success!</strong>';
            htmlMessage += ' <span data-i18n="WolverineLandingPage.GlobalMessages.YouHaveAddedANewActivity">You have added a new Activity!</span>';
            htmlMessage += "<a data-i18n='[value]WolverineLandingPage.GlobalLabels.GoToActivityJournal' href='../../secure/member/activityjournal.aspx'> Go to Activity Journal</a>";

            VHM.AL.ShowMessenger( htmlMessage );
        };

        var f_error = function(xhr) {
            $( ".loading-div" ).hide();
            $( ".NewEntry-ph" ).show();
            $('#AL-btnSave').val(i18n.t("WolverineLandingPage.GlobalLabels.Save", { defaultValue: "Save" }));
            $('#AL-btnSave').prop("disabled", false);

            if (xhr.status == 500) {
                VHM.AL.ClearEntryData();
               
            } else {
                var text = jQuery.parseJSON( xhr.responseText );
                alert( text.Message );
            }
        };
        VHM.AJ.callRESTService( VHM.AJ.API_ACTIVITIES_URL + methodName, methodType, data, f_success, f_error );
    };

    VHM.Core.Util.ExecFuncWhenCondition( function() { // calc calories before saving
        saveEntry( callback );
        saveEntry = null;
    }, function() {
        return VHM.AL.CalsCalculatedV2 || VHM.AL.CalculateCalsFailedV2;
    } );
};

VHM.AL.ShowMessenger = function (innerHtml) {
    Messenger.options = {
        extraClasses: 'messenger-fixed messenger-on-top',
        type: 'success',
        theme: 'block'
    };
    
    Messenger().post({
        message: innerHtml,
        extraClasses: 'messenger-fixed messenger-on-top',
        type: 'success',
        theme: 'block',
        hideAfter: 5
    });

};

VHM.AL.PopulateActivitiesAL = function (data) {
    var unique = {};
    var uniqueCategories = [];
    var categories = [];

    //distinct only those categories which user have selected for dropdown
    for (var i = 0; i < data.length; i++) {
        if (!unique[data[i].CategoryID] && data[i].IsSelected) {
            uniqueCategories.push(data[i].CategoryName);
            unique[data[i].CategoryID] = true;
        }
    }

    //get all supported categories in order to populate predefine activities if user doesn't have any
    for (var i = 0; i < data.length; i++) {
        if (!unique[data[i].CategoryID]) {
            categories.push(data[i].CategoryName);
            unique[data[i].CategoryID] = true;
        }

    }

    var selActivity = $("#AL-ActivityList")[0];
    while (selActivity.hasChildNodes()) {
        selActivity.removeChild(selActivity.firstChild);
    }
    var category, activities, actId, actName, isCustom, nnSelActivityLength, optgroup, option;
    activities = data;

    option = document.createElement("option");
    option.value = "add";
    option.appendChild(document.createTextNode("+ Add an activity"));
    selActivity.appendChild(option);

    var selectedItems = 0;

    for (var i = 0, cl = uniqueCategories.length; i < cl; i++) {

        optgroup = document.createElement("optgroup");
        optgroup.label = uniqueCategories[i];
        option = document.createElement("option");
        for (var j = 0, al = activities.length; j < al; j++) {
            if (activities[j].IsSelected == true && activities[j].CategoryName == uniqueCategories[i]) {
                actId = activities[j].ActivityID;
                actName = activities[j].ActivityName;
                isCustom = activities[j].IsCustom;
                option = document.createElement("option");
                option.value = actId;
                option.appendChild(document.createTextNode(actName));
                optgroup.appendChild(option);
                selectedItems++;
            }
        }
        selActivity.appendChild(optgroup);
    }
   VHM.AJ.selectedActivityItems = selectedItems;
    if (VHM.AJ.selectedActivityItems == 0) {
        var $activityList = $(VHM.AJ.domActivityList);
        $("#custom-activity-footer").find('img').show();
        $activityList.html(VHM.AJ.parseHTMLChkListLoading($("#hidSaving").val(), true));

        var f_success = function (data) {
            if (data.d == false) {
                return;
            }
            $('#divActivityList').innerHTML = VHM.AJ.parseHTMLChkListLoading($("#hidSavedSuccessfully").val(), false);
            VHM.AL.OpenAddEntryDlgAL(true);
            setTimeout(function () {
                VHM.AJ.HideActivityListForm();
            }, 1000);
        };

        var data = { selectedIDs: "102074,102067,95701, 102069, 102068, 102065 ,c:96,c:311" }
     
        VHM.AJ.callWs("SaveActivityPreference", "application/json; charset=utf-8", "json", JSON.stringify(data), f_success);
    }

    $("#AL-ActivityList").prepend("<option value=''></option>").val('');

    //if there is no activities append predifined ones
    if (selectedItems == 0) {
        $("#AL-ActivityList").find('optgroup').remove();
        for (var i = 0, cl = categories.length; i < cl; i++) {
            category = categories[i];

            optgroup = document.createElement("optgroup");
            optgroup.label = category;

            for (var j = 0, al = activities.length; j < al; j++) {
                actId = activities[j].ActivityID;
                actName = activities[j].ActivityName;
                if (actId == 102074 || actId == 95701 || actId == 102068
                || actId == 102067 || actId == 102069 || actId == 102065) {
                    if (category == activities[j].CategoryName) {
                        option = document.createElement("option");
                        option.value = actId;
                        option.appendChild(document.createTextNode(actName));
                        optgroup.appendChild(option);
                    }
                }
            }
            selActivity.appendChild(optgroup);
        }
    }

    //removes category group without any activities
    $("#AL-ActivityList").find('optgroup').each(function () {
        if (this.childNodes.length == 0) {
            $(this).remove();
        }
    });
};

VHM.AL.GetActivityList = function (callback, error) {
    var f_success = function (data) {
        VHM.AJ.MemberActivityList = data;
        if (typeof callback === "function") {
            callback(data);
        }
        callback = null;
    };

    VHM.AJ.callRESTService(VHM.AJ.API_ACTIVITIES_URL, "GET", null, f_success, error);
};

VHM.AL.OpenAddEntryDlgAL = function (afterLoaded) {
    $('.ActivityLogAddEntry #AL-btnSave').val(i18n.t("WolverineLandingPage.GlobalLabels.Save", { defaultValue: "Save" }));
    var f = function () {
        $("#AL-ActivityList2")[0].selectedIndex = -1;
        if (typeof afterLoaded === "function") {
            afterLoaded();
            afterLoaded = null;
        }
    };
    var error = function () {
    };

    VHM.AL.MemberActivityListAL = VHM.AL.GetActivityList(VHM.AL.PopulateActivitiesAL, error);
    VHM.Core.Util.ExecFuncWhenCondition(f, function () {
        return (VHM.AJ.MemberActivityListAL);
    });
};

VHM.AL.onChangeSelActivity = function (o) {
    if (o.selectedIndex > -1) {
        var activityName = $(o.options[o.selectedIndex]).text();
        var $txtALSteps = $("#txtALSteps");
        if (activityName == "Pedometer") {
            $txtALSteps.show();
            $("#LabelALSteps").show();
        } else {
            $txtALSteps.val("");
            $txtALSteps.hide();
            $("#LabelALSteps").hide();
        }

        if (o.options[o.selectedIndex].value === "add") {
            VHM.AJ.addCustomActivity();
            $("#newEntryModal").modal("hide");
        } else {
            var catId = $(o.options[o.selectedIndex]).attr("category");
            var $txtDistance = $("#txtALDistance");
            switch (parseInt(catId)) {
                case 95697:
                case 95698:
                    $txtDistance.val("");
                    $txtDistance.parent().css("color", "#cccccc");
                    $txtDistance.attr("disabled", "disabled");
                    break;
                default: //95696, 95699
                    $txtDistance.removeAttr("disabled");
                    $txtDistance.parent().css("color", "");
                    break;
            }
        }
        $("#newEntryModal").modal("hide");
    }
};

VHM.AJ.addCustomActivity = function () {
    var element = $("#custom-activity-content table").length;
    
    //if (element > 0) {
    //    $("#customActivityModal").modal('show');
    //    return;
    //}

    $("#customActivityModal h1.red").text($("#hidActivityYourList").val());

    $("#custom-activity-footer").html("");

    $('#divActivityList').innerHTML = VHM.AJ.parseHTMLChkListLoading("Loading...", true);

    var f_success = function (data) {
        var xml = VHM.XML.parseTextToXMLDOM(data);
        var html = VHM.XML.xsltTransform(xml, "../../XSL/ActivityListChkBxList.xslt");

        $("#tdCreateYourOwnActivity").html("<strong>" + $("#hidCreateYourOwnActivity").val() + "</strong>");
        // TODO: yet two value from xlst

        $("#custom-activity-content").html(html);
        var save = $("#customActivityModal input[data-value='save']");
        var cancel = $("#customActivityModal input[data-value='cancel']");
        $("#customActivityModal input[value='Add']").css("margin-bottom", "10px");
        cancel.attr("class", "btn");
        save.attr("class", "btn yellow-button");
        //        $("#custom-activity-footer").append("<img src='../../Images/ActivityJournal/loading.gif' alt='Alternate Text' class='ajax-loader'>");
        $("#custom-activity-footer").append(cancel);
        $("#custom-activity-footer").append(save);
        $("#customActivityModal input[data-value='save']").val("Save");
        $("#customActivityModal input[data-value='cancel']").val("Cancel");
        $("#customActivityModal input[data-value='add']").val("Add");
        $("#txtNewActivity").addClass("bootstrap bootstrap-input");
        $("#ddCategory").addClass("bootstrap bootstrap-select");
        $("#newEntryModal").modal("hide");
    };



    $("#customActivityModal").modal('show');

    VHM.AJ.callWs("GetAllActivityList", "text/xml", "text", null, f_success);
};
VHM.AJ.HideActivityListForm = function () {
    $("#divActivityList").siblings('.ui-dialog-buttonpane').hide();
    //    $( "#divActivityList" ).dialog( 'close' );
    $("#customActivityModal").modal("hide");
    $('#hdnFormOpen').value = 1;
    $('#customActivityModal').modal('hide');
};
VHM.AJ.SaveSelectedActivityList = function () {
    $("#divActivityList").siblings('.ui-dialog-buttonpane').hide();
    $("#custom-activity-content").find('.alert').hide();
    VHM.AJ.MemberActivityList = null;
    var $divActivityList = $("#custom-activity-content").find(":checkbox");
    var aSelected = [];
    for (var l = $divActivityList.length, i = l - 1; i >= 0; --i) {
        var e = $divActivityList[i];
        if (e.type == "checkbox" && e.name == "chkActivityID") {
            if (e.checked) {
                aSelected.push(e.value);
            }
        }
    }

    if (!aSelected.length) {
        var is_notificated = $("#custom-activity-content").find('.alert').length;
        if (is_notificated == 0) {
            var notification = "<div class='alert alert-error'>" +
                               "<button type='button' class='close' data-dismiss='alert'>&#215;</button>" +
                               "<strong>Please check at least one item.</strong></div>";
            $("#custom-activity-content").prepend(notification);
        }

    } else {
        var $activityList = $(VHM.AJ.domActivityList);
        $("#custom-activity-footer").find('img').show();
        $activityList.html(VHM.AJ.parseHTMLChkListLoading($("#hidSaving").val(), true));
        //        $( VHM.AJ.domActivityList ).dialog( 'open' );

        var f_success = function () {
            $('#divActivityList').innerHTML = VHM.AJ.parseHTMLChkListLoading($("#hidSavedSuccessfully").val(), false);
            VHM.AL.OpenAddEntryDlgAL(true);
            setTimeout(function () {
                VHM.AJ.HideActivityListForm();
            }, 1000);

        };
        var data = { selectedIDs: aSelected.join(",") };
        VHM.AJ.callWs("SaveActivityPreference", "application/json; charset=utf-8", "json", JSON.stringify(data), f_success);
    }
};

VHM.AJ.SaveNewActivityAndClose = function () {
    VHM.AJ.SaveNewActivity();
    setTimeout(function () {
        VHM.AL.OpenAddEntryDlgAL();
    }, 1500);
};

VHM.AJ.SaveNewActivity = function () {
    VHM.AJ.MemberActivityList = null;
    var $txtNewActivityVal = $("#txtNewActivity").val();
    var $selddCategory = $("#ddCategory");

    if ($txtNewActivityVal === "") {
        //alert("Please enter activity name");
        alert($("#hidPleaseEnterActivityName").val());
    } else {
        var nCategoryID = -1;
        nCategoryID = parseInt($selddCategory.val());

        if (isNaN(nCategoryID)) {
            alert($("#hidInvalidEntry").val());
        } else {
            $('#divActivityList').innerHTML = VHM.AJ.parseHTMLChkListLoading($("#hidSaving").val(), true);

            var f_success = function (data) {
                $('#divActivityList').innerHTML = VHM.AJ.parseHTMLChkListLoading($("#hidSavedSuccessfullyPoints").val(), true);
                setTimeout(function () { VHM.AJ.HideActivityListForm(); }, 1000);
            };

            var data = { CategoryID: nCategoryID, ActivityName: $txtNewActivityVal };
            VHM.AJ.callRESTService(VHM.AJ.API_ACTIVITIES_URL, "POST", data, f_success);
        }
    }
}
VHM.AJ.DeleteCustomActivity = function (customActivityID) {
    var f_success = function (data) {
        VHM.AJ.addCustomActivity();
    };

    var f_error = function (error) {
        var errorCode = error.statusText;
        switch (errorCode) {
            case 409:
                alert("Can not delete custom activity associated with activity entry")
                break;
            default:

        }
    };
    VHM.AJ.callRESTService(VHM.AJ.API_ACTIVITIES_URL + customActivityID, "DELETE", null, f_success, f_error);
};

VHM.AJ.parseHTMLChkListLoading = function (text, hasAnime) {
    aStr = new VHM.Core.StringBuilder();

    aStr.append("<div>\n");

    if (hasAnime) {
        aStr.append("<img src=\"../../images/general/bigrotation2.gif\"><br/>");
    }

    aStr.append("<strong>" + text + "</strong>");
    aStr.append("</div>\n");

    return aStr.toString();
};

VHM.AJ.setCaloriesStepsGraphDays = function (datesJson) {

    var SelectionDateFormat = 'yy-mm-dd';
    for (var i = 0; i < datesJson.length; i++) {
        var parsedDate = $.datepicker.parseDate(SelectionDateFormat, datesJson[i].Date.split("T")[0]);
        VHM.AJ.CaloriesStepsGraphDays[i] = $.datepicker.formatDate("mm-dd-yy", parsedDate);
    }
    return VHM.AJ.CaloriesStepsGraphDays;
};

function CreateActivityCalendar() {
    var todaysDate = new Date();
    $.ajax( {
        cache: false,
        async: false,
        contentType: "application/json; charset=utf-8",
        data: null,
        dataType: 'json',
        type: 'GET',
        url: "../member/GetMemberCurrentTime",
        success: function( responseData ) {
            if ( responseData != null && responseData.indexOf( "Service Error" ) < 0 ) {
                var result = $.parseJSON(responseData);
                var dateParse = new Date(moment(result.MemberTime));
                todaysDate = dateParse;
            }
        },
        error: function( xhr, ajaxOptions, error ) {
        }
    } );
    $('#inputDateAL-AddEntry').datepicker({ maxDate: todaysDate });
    var prettyDate = ("0" + (todaysDate.getMonth() + 1)).slice(-2) + "/" + ("0" + (todaysDate.getDate())).slice(-2) + "/" + todaysDate.getFullYear();
    $('#inputDateAL-AddEntry').val(prettyDate);
}

function LoadGraphData() {
    var ReturnData = {};

    var d = new Date();
    d.setDate(d.getDate() - 6);
    var curr_date = d.getDate();
    var curr_month = d.getMonth();
    curr_month++;
    var curr_year = d.getFullYear();
    var endDate = curr_month + "/" + curr_date + "/" + curr_year;

    var methodUrl = VHM.AJ.API_ACTIVITIES_URL + "summaries?start=" + endDate;

    var measurement = $("#measurementsType").val();
    var unit;
    if (measurement == "Imperial") {
        unit = "Miles";
    } else {
        unit = "Kilometers";
    }
    var customValues = [];
    for (var i = 0; i < 7; i++) {
        var myDate = moment(d).format("YYYY-MM-DD");
        var object = {
            Date: myDate,
            Calories: 0,
            Steps: 0,
            Duration: 0,
            Distance: 0,
            Unit: unit
        }
        d.setDate(d.getDate() + 1);
        customValues.push(object);
    }

    ReturnData["JSONData"] = customValues;
    ReturnData["CaloriesTarget"] = VHM.AJ.CaloriesTarget;
    ReturnData["StepsTarget"] = VHM.AJ.StepsTarget;
    ReturnData["JSONLastStepsEntry_ActivityName"] = "null";
    ReturnData["JSONLastStepsEntry_Steps"] = "";
    ReturnData["JSONLastStepsEntry_Calories"] = "";
    ReturnData["JSONLastStepsEntry_Date"] = "";
    ReturnData["JSONLastCaloriesEntry_ActivityName"] = "";
    ReturnData["JSONLastCaloriesEntry_Steps"] = "";
    ReturnData["JSONLastCaloriesEntry_Calories"] = "";
    ReturnData["JSONLastCaloriesEntry_Date"] = "";

    var f_success = function (data) {
        $('.activity-data-loading-div').hide();
        $(".activity-graph").show();

        if (data == null || data.length == 0) {
            if (VHM.AJ.type == "steps") {
                setActivityStepsArray(ReturnData);
            }
            if (VHM.AJ.type == "calories") {
                setActivityCaloriesArray(ReturnData);
            }
            return;
        }

        var lastStepsEntry = {};
        for (var i = 0; i < data.length; i++) {
            if (data[i].StepsTaken > 0) {
                lastStepsEntry.Steps = data[i].StepsTaken;
                lastStepsEntry.Calories = data[i].CaloriesBurned;
                lastStepsEntry.Date = moment(data[i].Date).format("MM/DD/YYYY");
                break;
            }
        }
        if (!jQuery.isEmptyObject(VHM.AJ.LastEntry)) {
            ReturnData["JSONLastCaloriesEntry_ActivityName"] = VHM.AJ.LastEntry.ActivityName;
            ReturnData["JSONLastCaloriesEntry_Steps"] = VHM.AJ.LastEntry.Steps;
            ReturnData["JSONLastCaloriesEntry_Calories"] = VHM.AJ.LastEntry.Calories;
            ReturnData["JSONLastCaloriesEntry_Date"] = moment(VHM.AJ.LastEntry.Date).format("MM/DD/YYYY");
        }
        else {
            if (VHM.AJ.ActivityEntries.length > 0) {
                ReturnData["JSONLastCaloriesEntry_ActivityName"] = VHM.AJ.ActivityEntries[0].ActivityName;
                ReturnData["JSONLastCaloriesEntry_Steps"] = VHM.AJ.ActivityEntries[0].Steps;
                ReturnData["JSONLastCaloriesEntry_Calories"] = VHM.AJ.ActivityEntries[0].Calories;
                ReturnData["JSONLastCaloriesEntry_Date"] = moment(VHM.AJ.ActivityEntries[0].Date).format("MM/DD/YYYY");
            }
        }

        if (!jQuery.isEmptyObject(VHM.AJ.LastEntry) && VHM.AJ.LastEntry.Steps > 0) {
            ReturnData["JSONLastStepsEntry_ActivityName"] = VHM.AJ.LastEntry.ActivityName;
            ReturnData["JSONLastStepsEntry_Steps"] = VHM.AJ.LastEntry.Steps;
            ReturnData["JSONLastStepsEntry_Calories"] = VHM.AJ.LastEntry.Calories;
            ReturnData["JSONLastStepsEntry_Date"] = moment(VHM.AJ.LastEntry.Date).format("MM/DD/YYYY");
        }
        else{
            if (!jQuery.isEmptyObject(lastStepsEntry)) {
                ReturnData["JSONLastStepsEntry_Steps"] = lastStepsEntry.Steps;
                ReturnData["JSONLastStepsEntry_Calories"] = lastStepsEntry.Calories;
                ReturnData["JSONLastStepsEntry_Date"] = lastStepsEntry.Date;
            }
        }


        for (var i = 0; i < customValues.length; i++) {
            for (var j = 0; j < data.length; j++) {
                if (customValues[i].Date == data[j].Date) {
                    customValues[i].Calories = data[j].CaloriesBurned;
                    customValues[i].Steps = data[j].StepsTaken;
                    customValues[i].Duration = data[j].TotalDuration;
                    customValues[i].Distance = data[j].TotalDistance;
                }
            }
        }

        VHM.AJ.setCaloriesStepsGraphDays(customValues);

        ReturnData["JSONData"] = customValues;

        if (VHM.AJ.type == "steps") {

            setActivityStepsArray(ReturnData);
        }
        if (VHM.AJ.type == "calories") {

            setActivityCaloriesArray(ReturnData);
        }
    };

    var f_error = function (xhr) {
        $('.activity-data-loading-div').hide();

        if ( xhr.status == 500 ) {
          
        } else {
            var text = jQuery.parseJSON( xhr.responseText );
            
        }

        $(".activity-graph").show();
        if (VHM.AJ.type == "steps") {
            setActivityStepsArray(ReturnData);
        }
        if (VHM.AJ.type == "calories") {
            setActivityCaloriesArray(ReturnData);
        }
    };


    VHM.AJ.callRESTService(methodUrl, "GET", null, f_success, f_error);
}

VHM.ValidateDistanceSteps = function ValidateDistanceSteps(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
};

function addTip(classname, txt, tipId) {

    var tipText = "";
    var over = false;

    var tip = $("#" + tipId).hide();
    $(document).mousemove(function (e) {
        e = e || window.event;
        if (over) {
            if (tipId == 'tip' && $.browser.msie && ($.browser.version >= 8.0 && $.browser.version < 9.0)) {
                var str = classname.split("-");
                str = str[str.length - 1];
                tip.css("margin-left", 35 * str + 15);
                tip.css("margin-top", -35);
            } else if (!$.browser.mozilla) {
                if (tipId != 'tip') {
                    tip.css("left", e.offsetX - 20).css("top", e.offsetY + 5);
                } else {
                    tip.css("left", e.offsetX).css("top", e.offsetY + 75);
                }

            } else {
                var str = classname.split("-");
                str = str[str.length - 1];
                tip.css("margin-left", 35 * str + 15);
                tip.css("margin-top", -35);
                //tip.css("left", e.pageXOffset).css("top", e.pageYOffset);
            }

            $("#" + tipId + " #tip-data").html(tipText);
        }
    });

    var tip = $("#" + tipId).hide();
    $("#" + classname).mouseover(function (e) {
        tip.hide();
        tipText = txt;
        tip.fadeIn();
        over = true;

        e = e || window.event;

        if (over) {
            if (tipId == 'tip' && $.browser.msie && ($.browser.version >= 8.0 && $.browser.version < 9.0)) {
                var str = classname.split("-");
                str = str[str.length - 1];
                tip.css("margin-left", 35 * str + 15);
                tip.css("margin-top", -35);
            } else if (!$.browser.mozilla) {
                if (tipId != 'tip') {
                    tip.css("left", e.offsetX - 20).css("top", e.offsetY + 5);
                } else {
                    tip.css("left", e.offsetX).css("top", e.offsetY + 75);
                }

            } else {
                var str = classname.split("-");
                str = str[str.length - 1];
                tip.css("margin-left", 35 * str + 15);
                tip.css("margin-top", -35);
                //tip.css("left", e.pageXOffset).css("top", e.pageYOffset);
            }


            $("#" + tipId + " #tip-data").html(tipText);
        }

    }).mouseout(function (e) {
        tip.hide();
        over = false;
        setTimeout(function () {
            if (over == false) {
                tip.hide();
            }
        }, 1500);
    });
}

function sortDaysArray(arr) {
    var tempArr = [];
    var indexes = [];
    for (var i = 0; i < arr.length; i++) {
        var obj = { value: arr[i], day: i, color: "" };
        tempArr.push(obj);
    }
    // sort values (numeric sort)
    tempArr.sort(function (a, b) { return (a.value > b.value) ? 1 : ((b.value > a.value) ? -1 : 0); });


    // put sorted values back into the indexes in the original array that were used
    for (var i = 0; i < indexes.length; i++) {
        arr[indexes[i]] = tempArr[i];
    }
    return (arr);
}
var activityDaysCalories = new Array();
function setActivityCaloriesArray(jsonResponse) {
    var activityDaysCalories = new Array();
    for (var i = 0; i < 7; i++) {
        if (jsonResponse.JSONData.length > 0) {
            activityDaysCalories.push(jsonResponse.JSONData[i].Calories);
        } else {
            activityDaysCalories.push(0);
        }
    }
    if (jsonResponse.JSONLastCaloriesEntry_ActivityName != '' && jsonResponse.JSONLastCaloriesEntry_Calories != '' && jsonResponse.JSONLastCaloriesEntry_Date != '') {
        $('.last-entry').show();
        $('.no-data-msg').hide();
        if (jsonResponse.JSONLastCaloriesEntry_ActivityName == 'null') {
            $('.data-row.activity-name .row-value').html('');
        } else {
            $('.data-row.activity-name').show();
            $('.data-row.activity-name .row-value').html(jsonResponse.JSONLastCaloriesEntry_ActivityName);
        }

        if (jsonResponse.JSONLastCaloriesEntry_Steps != 0) {
            $('.data-row.steps .row-value').html(jsonResponse.JSONLastCaloriesEntry_Steps);
            $('.data-row.steps').show();
        }
        else
            $('.data-row.steps').hide();

        $('.data-row.calories .row-value').html(jsonResponse.JSONLastCaloriesEntry_Calories);
        $('.data-row.date .row-value').html(jsonResponse.JSONLastCaloriesEntry_Date);
    } else {
        $('.last-entry').hide();
        $('.no-data-msg').show();
    }
    if (jsonResponse.CaloriesTarget == "")
        jsonResponse.CaloriesTargety = 0;

    try {
        VHM.GraphHandler.remove();
    } catch (e) { }




    var days = ["M", "T", "W", "T", "F", "S", "S"];
    var sortedDays = sortDays(days);
    try {
        VHM.GraphHandler.remove();
    } catch (e) {

    }
    VHM.GraphHandler = GraphPluginModule.DrawGraph(activityDaysCalories, jsonResponse.CaloriesTarget, 7, 146, 27, ['#e49f9a', '#c55e5c', '#bbd9d5'], 'activity-graph-container', '/v2/Content/Images/LandingPageV3/activitygraph-bg-', sortedDays, "calories", jsonResponse.JSONData);
}

var activityDaysSteps = new Array();
function setActivityStepsArray(jsonResponse) {
    var activityDaysSteps = new Array();

    for (var i = 0; i < 7; i++) {
        if (jsonResponse.JSONData.length > 0) {
            activityDaysSteps.push(jsonResponse.JSONData[i].Steps);
        } else {
            activityDaysSteps.push(0);
        }
    }
    if (jsonResponse.JSONLastStepsEntry_Steps != "" && jsonResponse.JSONLastStepsEntry_Calories != "" && jsonResponse.JSONLastStepsEntry_Date != "") {
        $('.last-entry').show();
        $('.no-data-msg').hide();
        if (jsonResponse.JSONLastStepsEntry_ActivityName == 'null') {
            $('#ac-name').text('');
            $('.data-row.activity-name').hide();
        } else {
            $('.data-row.activity-name').show();
            $('#ac-name').text(jsonResponse.JSONLastStepsEntry_ActivityName);
        }

        if (jsonResponse.JSONLastStepsEntry_Steps != 0) {
            $('#ac-steps').html(jsonResponse.JSONLastStepsEntry_Steps);
            $('.data-row.steps').show();
        }
        else
            $('.data-row.steps').hide();


        if (jsonResponse.JSONLastStepsEntry_Calories == 'null') {
            $('#ac-calories').text('');
        } else {
            $('#ac-calories').text(jsonResponse.JSONLastStepsEntry_Calories);
        }



        if (jsonResponse.JSONLastStepsEntry_Date == 'null') {
            $('#ac-date').text('');
        } else {
            $('#ac-date').text(jsonResponse.JSONLastStepsEntry_Date);
        }

    } else {
        $('.last-entry').hide();
        $('.no-data-msg').show();
    }
    if (jsonResponse.StepsTarget == "")
        jsonResponse.StepsTarget = 0;

    try {
        VHM.GraphHandler.remove();
    } catch (e) { }


    var days = ["M", "T", "W", "T", "F", "S", "S"];
    var sortedDays = sortDays(days);
    try {
        VHM.GraphHandler.remove();
    } catch (e) {

    }
    VHM.GraphHandler = GraphPluginModule.DrawGraph(activityDaysSteps, jsonResponse.StepsTarget, 7, 146, 27, ['#8ddfdc', '#3cb5af', '#f5e4e1'], 'activity-graph-container', '/v2/Content/Images/LandingPageV3/activitygraph-bg-', sortedDays, "steps", jsonResponse.JSONData);

}

function setActivityCaloriesTarget(data) {

    return GraphResponseData.Target;
}


function sortDays(days) {
    var daysOfWeek = ["M", "T", "W", "T", "F", "S", "S"];
    var today = new Date().getDay();
    for (var i = 0; i < today; i++) daysOfWeek.push(daysOfWeek.shift());
    return $.grep(daysOfWeek, function (n, i) {
        return days.indexOf(n) >= 0;
    });
}
function textToXML(text) {
    try {
        var xml = null;

        if (window.DOMParser) {

            var parser = new DOMParser();
            xml = parser.parseFromString(text, "text/xml");

            var found = xml.getElementsByTagName("parsererror");

            if (!found || !found.length || !found[0].childNodes.length) {
                return xml;
            }

            return null;
        } else {

            xml = new ActiveXObject("Microsoft.XMLDOM");

            xml.async = false;
            xml.loadXML(text);

            return xml;
        }
    } catch (e) {
        // suppress
    }
}