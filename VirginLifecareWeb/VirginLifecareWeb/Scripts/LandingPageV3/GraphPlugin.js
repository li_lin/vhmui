﻿var GraphPluginModule = (function () {
    "use strict";

    var COUNTERFISHED = false
    var DEFAULT_TARGET = 0;
    var smallRectShadow = '/v2/Content/Images/LandingPageV3/weightgraph-bg.png';
    function generateGraph  (state) {
        
        var arrayOfValues = state.arrayOfValues;
        var target = state.target;
        var numberOfGraphBars = state.numberOfGraphBars;
        var graphBarHeight =state.graphHeight;
        var graphBarWidth = state.graphWidth;
        var colors = state.colors;
        var elementToAppend = state.elementToAppend;
        var smallRectValues = state.smallRectValues;
        var jsonData = state.JSONData;

        /*This varible sets maximum graph bar height*/
        if (arrayOfValues.length != numberOfGraphBars) {
            
        }

        var relativeLargestValueInGraph;
        var isArrayEmpty = 0;

        for (var i = 0; i < numberOfGraphBars; i++) {
            if (arrayOfValues[i] == 0 || arrayOfValues[i] == 1) {
                isArrayEmpty++;
                arrayOfValues[i] = 0.1;
            }
        }

        var TARGET = parseInt(target);
        if (TARGET == 0) {
            relativeLargestValueInGraph = DEFAULT_TARGET + 10;
            TARGET = DEFAULT_TARGET;
        }
        var INITIAL_TARGET_HEIGHT = 0.95;
        var array = arrayOfValues;

        var LargestValueInGraph = Math.max.apply(Math, array);

        /*This part of code sets height of target line, and scales graph if target is much bigger then 
        graph bars.
        relativeLargestValueInGraph determines value of highest point in graph */
        /* If TARGET (steps/calories) is higher then DEFAULT_TARGET and at least 10% higher then
        largest number of steps, relativeLargestValueInGraph is TARGET incresed by 10%*/

        if (TARGET > DEFAULT_TARGET && TARGET > (LargestValueInGraph)) {
            relativeLargestValueInGraph = TARGET + 0.1 * TARGET;
        } else if (LargestValueInGraph < TARGET) {
            relativeLargestValueInGraph = TARGET * 1.1;
        } else if (LargestValueInGraph == TARGET) {
            relativeLargestValueInGraph = 1.1 * TARGET;
        } else if (LargestValueInGraph > TARGET && LargestValueInGraph < TARGET * 1.1) {
            relativeLargestValueInGraph = 1.1 * TARGET;
        } else {
            relativeLargestValueInGraph = LargestValueInGraph;
        }

        /*This sets height of target line if number of stepes is higher then target*/
        var targetPositionPixel;
        var targetPosition = (TARGET / relativeLargestValueInGraph);
        /*If target line position, exceed 90% height of space for graph limit it to 90%*/
        if (targetPosition > INITIAL_TARGET_HEIGHT) {
            targetPosition = INITIAL_TARGET_HEIGHT;
        }
        /*Setting Target line height in pixels        */
        if (TARGET > 0) {
            targetPositionPixel = graphBarHeight - (targetPosition * graphBarHeight);
        } else if (TARGET == 0 && LargestValueInGraph <= 1) {
            targetPositionPixel = 50;
        } else {
            targetPositionPixel = graphBarHeight - (targetPosition * graphBarHeight);
        }
        var graphBarsOptions = [];
        var xCord;
        for (var i = 0; i < numberOfGraphBars; i++) {
            xCord = i * (parseInt(graphBarWidth) + 10) + 20;
            var tempOptions = { width: graphBarWidth, height: graphBarHeight, x: xCord, y: graphBarHeight };
            graphBarsOptions[i] =  tempOptions;

        }

        /* Width of div depends on bar widths + 40 px offset on right side*/
        var divWidth = xCord + graphBarWidth + 40;
        var animationSpeed = 1000;
        var div = elementToAppend;
        var ActivityGraph = new Raphael(div, divWidth, graphBarHeight + 40);

        var graphBars = [];
        for (var i = 0; i < numberOfGraphBars; i++) {
            var singleRectTemp = ActivityGraph.rect(graphBarsOptions[i].x, graphBarsOptions[i].y, graphBarsOptions[i].width, 0);
            graphBars.push(singleRectTemp);
        }

        for (var i = 0; i < graphBars.length; i++) {

            if (arrayOfValues[i] < TARGET) {
                graphBars[i].attr({ fill: colors[0], stroke: 'none' });
            } else {
                graphBars[i].attr({ fill: colors[1], stroke: 'none' });
            }

        }
        
        /*Adjusting position of text in small rectangle */
        var datePosition = new Array();
        for (var i = 0; i < arrayOfValues.length; i++) {

            if (arrayOfValues[i].length == 3) {
                datePosition.push(5);
            } else {
                if (state.graphType == "nutrition") {
                    datePosition.push(2);
                } else {
                    datePosition.push(10);
                }
            }
        }


        /*Calculating y position of text in small rect. */
        var yPosition;
        if(state.graphType == "nutrition") {
            if ( $.browser.msie && parseInt($.browser.version, 10) === 8) {
             yPosition = graphBarHeight + 20;
                } else {
            yPosition = graphBarHeight - 87;
                }
        }else{
            yPosition  = graphBarHeight + 15;
        }

        if ($.browser.msie && parseInt($.browser.version, 10) === 8) {
            dateAttributes = {
                fill: '#fff', "text-anchor": "start", width: 25, height: 20,
                "font-size": 11, "font-family": "Conv_it -Medium,Arial,Helvetica,sans-serif"
            };

        }
        var dateAttributes = {
            fill: '#fff',
            "text-anchor": "start",
            width: graphBarWidth,
            height: 27,
            "font-size": 17,
            "font-family": "Conv_Tungsten-Medium,Arial,Helvetica,sans-serif"
        };
        var smallRect = [];
        var smallRectText = [];
        for (var i = 0; i < numberOfGraphBars; i++) {
            smallRect[i] = ActivityGraph.rect(graphBarsOptions[i].x, graphBarHeight, graphBarWidth, 27);
            smallRect[i].attr({ fill: colors[1], stroke: "none", opacity: 0.6 });
            smallRectText[i] = ActivityGraph.text(graphBarsOptions[i].x + datePosition[i], yPosition, smallRectValues[i]);
            smallRectText[i].attr(dateAttributes);
       }

        var external = new Image(graphBarHeight, 33);
        external.src = smallRectShadow;
        ActivityGraph.image(external.src, 0, graphBarHeight, divWidth, external.height);

        var animation = [];
        for (var i = 0; i < numberOfGraphBars; i++) {
            var tempGraphHeight = (arrayOfValues[i] / relativeLargestValueInGraph) * graphBarsOptions[i].height;
            animation[i] = Raphael.animation({ y: graphBarsOptions[i].y - tempGraphHeight, height: tempGraphHeight }, animationSpeed);
        }

     
        if (LargestValueInGraph >= 1) {
            for (var i = 0; i < numberOfGraphBars; i++) {
                graphBars[i].animate(animation[i]);
            }
        }

        /*    TO BE REIMPLEMENTED */
        /**** Tooltip part ***/
        if (state.graphType == "steps" || state.graphType == "calories") {
        for (var j = 0; j < graphBars.length; j++) {
            graphBars[j].node.id = "graph-bar-tooltip-" + j;
        }
        
        var date;
        for (var i = 0; i < graphBars.length; i++) {
            date = VHM.AJ.CaloriesStepsGraphDays[i];
            if (state.graphType == "steps") {
                var tipHtml = "<span id='date-span'>" + date + "</span><br>" + "<span id='steps-cals-span' >" + ((arrayOfValues[i] < 1) ? 0 : arrayOfValues[i]) + "</span><span id='data-span'>  " + i18n.t("WolverineLandingPage.GlobalLabels.Steps", { defaultValue: "STEPS" }) + "</span>";
                tipHtml += "<br><span id='data-span'>" + jsonData[i].Calories + "</span><span id='data-span' >" + " " + i18n.t("WolverineLandingPage.GlobalLabels.Calories", { defaultValue: "CALORIES" }) + "<steps>";
                tipHtml += "<br><span id='data-span'>" + jsonData[i].Distance + " " + "</span><span id='data-span'>" + jsonData[i].Unit + " </span>";
                addTip("graph-bar-tooltip-" + i, tipHtml, 'tip');
            } else if (state.graphType == "calories") {
                var tipHtml = "<span id='date-span'>" + date + "</span><br>" + "<span id='steps-cals-span' >" + ((arrayOfValues[i] < 1) ? 0 : arrayOfValues[i]) + "</span><span id='data-span'> " + i18n.t("WolverineLandingPage.GlobalLabels.Calories", { defaultValue: "CALORIES" }) + "</span>";
                tipHtml += "<br><span id='data-span'>" + jsonData[i].Distance + " " + "</span><span id='data-span'>" + jsonData[i].Unit + " </span>";
                tipHtml += "<br><span id='data-span'>" + jsonData[i].Steps + "</span><span id='data-span' >" + " " + i18n.t("WolverineLandingPage.GlobalLabels.Steps", { defaultValue: "STEPS" }) + "<steps>";
                addTip("graph-bar-tooltip-" + i, tipHtml, 'tip');
            }
           
        }
    }
     
        /** end of tooltip part ****/

        var targetLine = ActivityGraph.path("M10 " + targetPositionPixel + "L280 " + targetPositionPixel + "");
        var targetLineText = ActivityGraph.text(divWidth - 25, (targetPositionPixel - 10), TARGET);

        targetLine.attr({
            stroke: colors[1],
            "stroke-dasharray": "."
        });
        targetLineText.attr({
            stroke: colors[1],
            "stroke-dasharray": ".",
            "text-anchor": "start"
        });

        if (isArrayEmpty == numberOfGraphBars) {

            var noDataText = ActivityGraph.text(140, (targetPositionPixel + 15), i18n.t("WolverineLandingPage.GlobalMessages.NoDataAvailableForThisWeek", { defaultValue: "No data available for this week" }));
            noDataText.attr({
                fill: '#333333',
                "text-anchor": "left",
                "font-size": "12"
            });
        }
        return ActivityGraph;
    };

/* Method for drawing levels graph*/
  function drawLevelsGraph(state) {

      var ActualProgress = state.ActualProgress;
      var currentMilestone = state.currentMilestone;
      var milestoneValues = state.milestoneValues;
      var currentProgress = state.currentProgress;
      var numberOfMilestones = state.numberOfMilestones;
      var elementToAppend = state.elementToAppend;
      var pathToImage = state.pathToImage;
      var barHeights = state.barHeight;
      var barWidth = state.barWidth;

      var heightOfOneLevel = barHeights / (numberOfMilestones)

      ActualProgress = ActualProgress / 100;
      var score = 0;
         
      var level = [];
      for (var i = 0; i < numberOfMilestones; i++) {
          if (numberOfMilestones != (i + 1)) {
              level[i] = milestoneValues[i + 1];
          } else {
              level[i] = "";//milestoneValues[i - 1] * 2;
          }
      }

      score = currentProgress;

      var rectOptions = [];
      
      for (var i = 0; i < numberOfMilestones; i++) {
          var xCord = i * (parseInt(barWidth) + 10) ;
          rectOptions[i] = { width: barWidth, height: (heightOfOneLevel * i + heightOfOneLevel), x: xCord, y: barHeights };
      }

      var animationSpeed = 1000;
      //var div = document.getElementById('canvas_container');
      var raphaelObject = new Raphael(elementToAppend, 225, 200);

      var rectObject = [];
      for (var i = 0; i < numberOfMilestones; i++) {
          rectObject[i] = raphaelObject.rect(rectOptions[i].x, rectOptions[i].y, rectOptions[i].width, 0);
      }


      var textAttr = { font: '12px Verdana', 'font-size': 12, 'fill': '#0c784d', "text-anchor": "middle" };

      var graphBarText = [];
      for (var i = 0; i < numberOfMilestones; i++) {
          graphBarText[i] = raphaelObject.text(rectOptions[i].x + barWidth / 2 , rectOptions[i].y - rectOptions[i].height + 10, ((level[i] > 999) ? (level[i] / 1000 + "K") : (level[i])));
          graphBarText[i].attr(textAttr);
      }


      $(setCounter(score, currentMilestone, milestoneValues));

      var animation = [];
      var fillOpacity = 0.6;
      for (var i = 0; i < currentMilestone; i++) {
          if (score < level[0]) {
              rectObject[i].attr({ fill: '#439f54', stroke: 'none', 'fill-opacity': fillOpacity });
              var graphHeight = (score / level[0]) * rectOptions[0].height;
              animation[i] = Raphael.animation({ y: rectOptions[i].y - graphHeight, height: graphHeight }, animationSpeed);

          } else if (score < level[i]) {
              rectObject[i].attr({ fill: '#439f54', stroke: 'none', 'fill-opacity': fillOpacity });
              var graphHeight = ((score - level[i - 1]) / (level[i] - level[i - 1])) * rectOptions[i].height;
              animation[i] = Raphael.animation({ y: rectOptions[i].y - graphHeight, height: graphHeight }, animationSpeed);
              fillOpacity += 0.10;
          } else {
              animation[i] = Raphael.animation({ y: rectOptions[i].y - rectOptions[i].height, height: rectOptions[i].height }, animationSpeed);
              rectObject[i].attr({ fill: '#439f54', stroke: 'none', 'fill-opacity': fillOpacity });
              fillOpacity += 0.10;
          }
          rectObject[i].animate(animation[i].delay(i*animationSpeed));
      }

  }

  function setCounter(levelGraphScore, currentMilestone, milestoneValues) {
      var currCount = parseInt($('.counter').html());
      var startingValue = milestoneValues[currentMilestone - 1];
      $('.counter').text(startingValue);
      incCounter(levelGraphScore, currentMilestone, milestoneValues);
  }


  function incCounter(levelGraphScore, currentMilestone, milestoneValues) {

      var currCount = parseInt($('.counter').html());
      
      if (currCount >= levelGraphScore) {
          $('.counter').text(levelGraphScore);
          return;
      }
      
      $('.counter').text(currCount + 30);
      if ( currCount < levelGraphScore ) {
          setTimeout( function() { incCounter( levelGraphScore, currentMilestone, milestoneValues ); }, 1 );
      } else {
          COUNTERFISHED = true;
      }

  }

    return {
     
        DrawGraph: function (ArrayOfValues, Target, NumberOfGraphBars, GraphBarHeight, GraphBarWidth, Colors, elementToAppend, PathToImage, smallRectValues , graphType , jsondata) {
           var graphHandler = generateGraph({
                arrayOfValues: ArrayOfValues,
                target:  Target,
                numberOfGraphBars: NumberOfGraphBars,
                graphHeight: GraphBarHeight,
                graphWidth: GraphBarWidth,
                colors: Colors,
                elementToAppend: elementToAppend,
                pathToImage: PathToImage,
                smallRectValues: smallRectValues,
                JSONData: jsondata,
                graphType: graphType
                
            });
           return graphHandler;
        },

        DrawLevelsGraph: function (ActualProgress, currentMilestone, milestoneValues, currentProgress, numberOfMilestones, elementToAppend, pathToImage, barHeights, barWidth) {
            var graphHandler = drawLevelsGraph({
            actualProgress: ActualProgress,
            currentMilestone: currentMilestone,
            milestoneValues: milestoneValues,
            currentProgress: currentProgress,
            numberOfMilestones: numberOfMilestones,
            elementToAppend: elementToAppend,
            pathToImage: pathToImage,
            barHeight: barHeights,
            barWidth: barWidth
        });
    }
    };
})();
