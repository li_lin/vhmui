﻿//var LevelsGraphModule = (function () {
"use strict";

var animationSpeed = 1000;

function drawLevelsGraph(ActualProgress, currentMilestone , milestoneValues , currentProgress) {

    ActualProgress = ActualProgress / 100;
   // console.log( "[Actual progress]" + ActualProgress + "[current Milestone]" + currentMilestone );
    var score = 0;
    var milestoneValues = milestoneValues;
    var currentMilestone = currentMilestone;
    //var score = 10000;
    var level1 = milestoneValues[1];
    var level2 = milestoneValues[2];
    var level3 = milestoneValues[3];
    var level4 = milestoneValues[4];
    var level5 = milestoneValues[4] + ( milestoneValues[4]-milestoneValues[3]);
    

    if (currentMilestone == 1) {
        score = ActualProgress * level1;
    } else if (currentMilestone == 2) {
        score = level1 + ActualProgress * ( level2 - level1 );
    } else if (currentMilestone == 3) {
        score = level2 + ActualProgress * (level3 - level2);
    } else if (currentMilestone == 4) {
        score = level3 + ActualProgress * (level4 - level3 ) ;
    } else if (currentMilestone == 5) {
        score = currentProgress;
    }

    var rectOptions1 = { width: 37, height: 33, x: 0, y: 166 };
    var rectOptions2 = { width: 37, height: 66, x: 47, y: 166 };
    var rectOptions3 = { width: 37, height: 100, x: 94, y: 166 };
    var rectOptions4 = { width: 37, height: 133, x: 141, y: 166 };
    var rectOptions5 = { width: 37, height: 166, x: 188, y: 166 };

    var animationSpeed = 1000;

    var div = document.getElementById('canvas_container');

    var raphObject1 = new Raphael(div, 225, 200);
    //raphObject1.attr("left" : "0px")
    var rect1 = raphObject1.rect(rectOptions1.x, rectOptions1.y, rectOptions1.width, 0);
    var rect2 = raphObject1.rect(rectOptions2.x, rectOptions2.y, rectOptions2.width, 0);
    var rect3 = raphObject1.rect(rectOptions3.x, rectOptions3.y, rectOptions3.width, 0);
    var rect4 = raphObject1.rect(rectOptions4.x, rectOptions4.y, rectOptions4.width, 0);
    var rect5 = raphObject1.rect(rectOptions5.x, rectOptions5.y, rectOptions5.width, 0);

    var textAttr = { font: '12px Verdana', 'font-size': 12, 'fill': '#0c784d' };

    var graphBar1Text = raphObject1.text(rectOptions1.x + 16, rectOptions1.y - rectOptions1.height + 10, ( (level1 > 999) ? (level1/1000 +"K" ) :  (level1) ) );
    graphBar1Text.attr(textAttr);
    var graphBar2Text = raphObject1.text(rectOptions2.x + 16, rectOptions2.y - rectOptions2.height + 10, ((level2 > 999) ? (level2 / 1000 + "K") : (level2)));
    graphBar2Text.attr(textAttr);
    var graphBar3Text = raphObject1.text(rectOptions3.x + 16, rectOptions3.y - rectOptions3.height + 10, ((level3 > 999) ? (level3 / 1000 + "K") : (level3)));
    graphBar3Text.attr(textAttr);
    var graphBar4Text = raphObject1.text(rectOptions4.x + 16, rectOptions4.y - rectOptions4.height + 10, ((level4 > 999) ? (level4 / 1000 + "K") : (level4)));
    graphBar4Text.attr(textAttr);
    var graphBar5Text = raphObject1.text(rectOptions5.x + 16, rectOptions5.y - rectOptions5.height + 10, "");
    graphBar5Text.attr(textAttr);

    $(setCounter(score,currentMilestone, milestoneValues));

    if (1) {
        rect1.attr({
            fill: '#7bc95c',
            stroke: 'none'
        });
        //console.log("[score]" + score);
        if (score <= level1) {
            var graphHeight = (score / level1) * rectOptions1.height;
            var anim1 = Raphael.animation({ y: rectOptions1.y - graphHeight, height: graphHeight }, animationSpeed);
            //console.log(rectOptions1.height);
        } else {
            var anim1 = Raphael.animation({ y: rectOptions1.y - rectOptions1.height, height: rectOptions1.height }, animationSpeed);
        }

        rect1.animate(anim1);
    }
    if (score > level1) {

        rect2.attr({
            fill: '#63b759',
            stroke: 'none'
        });

        if (score <= level2) {
            var graphHeight = ((score - level1) / (level2 - level1)) * rectOptions2.height;
            var anim2 = Raphael.animation({ y: rectOptions2.y - graphHeight, height: graphHeight }, animationSpeed);
        } else {
            var anim2 = Raphael.animation({ y: rectOptions2.y - rectOptions2.height, height: rectOptions2.height }, animationSpeed);
        }

        rect2.animate(anim2.delay(animationSpeed));
    }

    if (score > level2) {

        rect3.attr({
            fill: '#439f54',
            stroke: 'none'
        });

        if (score <= level3) {
            var graphHeight = ((score - level2) / (level3 - level2)) * rectOptions3.height;
            var anim3 = Raphael.animation({ y: rectOptions3.y - graphHeight, height: graphHeight }, animationSpeed);
        } else {
            var anim3 = Raphael.animation({ y: rectOptions3.y - rectOptions3.height, height: rectOptions3.height }, animationSpeed);
        }

        rect3.animate(anim3.delay(2 * animationSpeed));

    }
    if (score > level3) {

        rect4.attr({
            fill: '#268b51',
            stroke: 'none'
        })

        if (score <= level4) {
            var graphHeight = ((score - level3) / (level4 - level3)) * rectOptions4.height;
            var anim4 = Raphael.animation({ y: rectOptions4.y - graphHeight, height: graphHeight }, animationSpeed);
        } else {
            var anim4 = Raphael.animation({ y: rectOptions4.y - rectOptions4.height, height: rectOptions4.height }, animationSpeed);
        }

        rect4.animate(anim4.delay(3 * animationSpeed));

    }
    if (score > level4) {
        rect5.attr({
            fill: '#0c784d',
            stroke: 'none'
        });

        if (score <= level5) {
            var graphHeight = ((score - level4) / (level5 - level4)) * rectOptions5.height;
            var anim5 = Raphael.animation({ y: rectOptions5.y - graphHeight, height: graphHeight }, animationSpeed);
        } else {
            var anim5 = Raphael.animation({ y: rectOptions5.y - rectOptions5.height, height: rectOptions5.height }, animationSpeed);
        }

        rect5.animate(anim5.delay(4 * animationSpeed));
    }
}

function setCounter( levelGraphScore,currentMilestone, milestoneValues   ) {

    var currCount = parseInt($('.counter').html());
   
    var startingValue = milestoneValues[currentMilestone-1];
    $('.counter').text(startingValue);
    incCounter( levelGraphScore, currentMilestone, milestoneValues );
}


function incCounter(levelGraphScore,currentMilestone, milestoneValues) {
   
    var currCount = parseInt($('.counter').html());
    
    if (currCount <= milestoneValues[1] && currCount <= levelGraphScore) {
        $('.counter').text(currCount + 10);

    } else if (currCount <= milestoneValues[2] && currCount <= levelGraphScore) {
        $('.counter').text(currCount + 10);

    } else if (currCount <= milestoneValues[3] && currCount <= levelGraphScore) {
        $('.counter').text(currCount + 10);

    } else if (currCount <= milestoneValues[4] && currCount <= levelGraphScore) {
        $('.counter').text(currCount + 10);

    } else if (currCount >= milestoneValues[4] && currCount <= levelGraphScore) {
        $('.counter').text(currCount + 10);
        
    }


    if (currCount + 10 != levelGraphScore) {
        setTimeout(function () { incCounter(levelGraphScore,currentMilestone, milestoneValues); }, 1);
        //console.log('test');
    }
}

//})();