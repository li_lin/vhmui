﻿var KEY_ENTER = 13;
var MAX_POST_LENGTH = 1000;
var IS_SEC = false;

VHM.Social.NewsFeed = VHM.Social.NewsFeed || {};
VHM.Social.NewsFeed.WebServiceUrl = "../../webservices/WSVHMSocial.asmx";
VHM.Social.NewsFeed.ChallengeURL = "../../Secure/Webservices/Challenge.asmx";
VHM.Social.NewsFeed.PagesLoaded = 0;
VHM.Social.NewsFeed.isMini;
VHM.Social.NewsFeed.displayMode; //can be 'Member' or 'Space' - depending on what the news feed is showing
VHM.Social.NewsFeed.ReloadTimerID = "";
VHM.Social.NewsFeed.InitialLoad = true;

$(function () {

    var seemsLikeText = 'Seems like there is a problem loading';
    var refreshingPageText = 'Please refresh the page';

    VHM.Social.NewsfeedFailMessage = "<div class='nfFailMessage'>" + seemsLikeText.replace('{0}', "<a href='#'>" + refreshingPageText + "</a>") + "</div>";
    VHM.Social.NewsFeed.NotificationHolder;

    VHM.Social.NewsFeed.GetCFUserProfData = function (e) {
        e.preventDefault;
        var cFfriendUserId = (typeof ($(e.target).attr('CFUserID')) != "undefined") ? $(e.target).attr('CFUserID') : $(e.target).closest('li').attr('CFUserID');
        if (typeof (cFfriendUserId) != "undefined") {
            VHM.SocialFrnds.GetCFUserProfData(cFfriendUserId, 'divNfMemProfile');
        }
        return false;
    };
    VHM.Social.NewsFeed.GroupProfData = function (e) {
        var groupId = (typeof ($(e.target).attr('GroupID')) != "undefined") ? $(e.target).attr('GroupID') : $(e.target).closest('li').attr('GroupID');
        VHM.SocialGroups.GetPublicGroup(groupId, 'divNFGroupPublicProfile');
    };
    VHM.Social.NewsFeed.AddNewPost = function () {
        if ($('#msgInputBox')[0] != null && $('#msgInputBox')[0].value.length > MAX_POST_LENGTH) {
            alert($('#hdnLocalizationExceeded').val().replace('{0}', MAX_POST_LENGTH), { title: "Posting Issue", redMessageText: " " });
            return;
        }
        $('#msgInputBox').attr('disabled', 'disabled');
        $('#msgPostButton').attr('disabled', 'disabled');

        var messagText = $('#msgInputBox').val();
        messagText = messagText.StripOutHTMLTags();
        var groupId = ($('#hdnGroupId').val()) || 0;

        var f_success = function (responseData, textStatus) {
            var wallObject = (typeof (responseData) != "object") ? eval("(" + responseData + ")") : responseData;

            var msgBox = $('#msgInputBox');
            msgBox.val(msgBox.attr("initialText")).css({ height: '30px', color: "grey", fontStyle: "italic" });
            var ul = $('#newsContainer > ul').eq(0);

            var noOfPosts = ul.children().length;
            if (VHM.Social.NewsFeed.isMini == 'true' && noOfPosts >= 8) {
                $(".OlderPostLinkContainer").show();
                ul.children().eq(noOfPosts - 1).remove();
            }
            if (wallObject.PostText.indexOf('posted in') < 0 || IS_SEC == false) {
                var li = $("#wallPostTemplate").tmpl({
                    ActionText: wallObject.PostText,
                    NewsFeedItemDateText: wallObject.NewsFeedItemDateText,
                    YourActivity: "true",
                    ActivityEntityID: wallObject.ActivityEntityID,
                    NewsFeedID: wallObject.PostID,
                    ActivityEntityType: "Wall",
                    CFUserID: wallObject.CFUserID,
                    AbuseReported: "false",
                    YouLikedIt: "false",
                    PostTypeID: wallObject.PostTypeID,
                    YouLikedID: "0",
                    SpaceID: wallObject.SpaceID
                }).prependTo(ul);

                $('#msgInputBox').removeAttr('disabled').bind('focus', function (e) {
                    $('#msgInputBox').val("").css({ color: "black", fontStyle: "normal" }).unbind("focus");
                });
               // VHM.Social.NewsFeed.AttachEvents(VHM.Social.NewsFeed.isMini, li);
            }
        };
        if (messagText != "" && $.trim(messagText).length >= 3) {
            var pl = new SOAPClientParameters();
            pl.add("groupId", groupId);
            pl.add("messageText", messagText);
            pl.add("sId", VHM.Social.SessionID);
            if ($("#hdnSECID").val() != null) {
                pl.add("noReward", true);
            }
            SOAPClient.invoke(VHM.Social.CFServiceUrl, "AddNewPost", pl, false, f_success);
        }
    };

    var LOADED = false;
    $(document).bind("ready", function (e) {

        if ($("#hdnSECID").val() != null) {
            IS_SEC = true;
        }
        if (typeof VHM.Social.NewsFeed.SightlessReload != 'undefined') {
            setTimeout(VHM.Social.NewsFeed.SightlessReload, 100);
        }

        VHM.Social.NewsFeed.isMini = $("#newsFeedContainer").attr('isMini') ? $("#newsFeedContainer").attr('isMini').toString().toLowerCase() : false;
        VHM.Social.NewsFeed.displayMode = $(".hiddenDisplayField").val();


        if (typeof (VHM.Social.SessionID) == "undefined") {
            VHM.Social.SessionID = $("#hdnWp1").val();
        }


        $("#imgNotifIcon").click(function () {
            VHM.Notification.openNotifications('divVHMNotification');
            VHM.Social.NewsFeed.loadNotifications();
        });
        $(document).on("click", "div.nfFailMessage a", function () {
            window.location.reload();
        });

        $(document).bind('click', function (e) {
            var isPopupOpened = $('#RemoveAbusePopup').attr('opened');
            if (isPopupOpened === "true") {
                VHM.Social.NewsFeed.HidePopup($(e.target));
            }
        });
        $('#newsContainer').bind('scroll', function () {
            $(document).trigger('click');
        });
        if (VHM.Social.NewsFeed.isMini == 'true') {
            VHM.Social.NewsFeed.ReloadTimerID = setInterval(VHM.Social.NewsFeed.SightlessReload, 600000);
        }

        if (IS_SEC == true) {
            $(".spanActionText").each(function () {
                if ($(this).html().indexOf("posted in") >= 0) {
                    $(this).parent().parent().parent().hide();
                }
            });
        }
    });

    VHM.Social.NewsFeed.UpdateCharsLeftCount = function (inputElement, counterDiv) {
        var maxLength = parseInt(inputElement.attr('maxLength'));
        var length = inputElement.val().length;
        counterDiv.text($('#hdnLocalizationCharactersLeft').val().replace('{0}', (maxLength - length)));
    };
    VHM.Social.NewsFeed.TruncateStringOnPaste = function (inputElementId) {
        var target = $('#' + inputElementId);
        var maxLength = parseInt(target.attr('maxLength'));
        target.val(target.val().substring(0, maxLength));
        VHM.Social.NewsFeed.UpdateCharsLeftCount(target, $('#ReportAbuseCharsLeftCount'));
    };
    
    VHM.Social.NewsFeed.AttachTxtBoxEvents = function ($txt, $btnPost) {
        $btnPost.attr('disabled', 'disabled');
        $txt.val($txt.attr("initialText")).css({ color: "grey", fontStyle: "italic" });

        $txt.bind('focus', function (e) {
            $(this).val("").css({ color: "black", fontStyle: "normal" }).unbind("focus");
        });

        $txt.bind('blur', function (e) {
            var $this = $(this);
            if ($.trim($this.val()) == "") {
                $this.val($this.attr("initialText")).css({ color: "grey", fontStyle: "italic" }).bind('focus', function () {
                    $(this).val("").css({ color: "black", fontStyle: "normal" }).unbind("focus");
                });
            }
        });

        $txt.bind('keyup', function (e) {
            var $this = $(this);
            var inputValue = $.trim($this.val());
            if (inputValue.length >= 3) {
                $btnPost.removeAttr('disabled');
            } else {
                $btnPost.attr('disabled', 'disabled');
            }
            var code = (e.keyCode ? e.keyCode : e.which);
        });
    };

    VHM.Social.NewsFeed.AttachCloseIconEvents = function (li) {
        var isYourActivity = li.attr('YourActivity').toLowerCase();
        var isAbuseReported = li.attr('AbuseReported').toLowerCase();
        var handler = (isYourActivity == 'true') ? VHM.Social.NewsFeed.ShowRemovePostPopup : VHM.Social.NewsFeed.ShowReportAbusePopup;
        if (isYourActivity == "true") {
            li.bind('mouseenter mouseleave', function (e) {
                li.find('.closeIconContainer').eq(0).toggle();
            });
        } else if (isAbuseReported == "false") {
            li.bind('mouseenter mouseleave', function (e) {
                li.find('.closeIconContainer').eq(0).toggle();
            });
        }
        li.find('.closeIconContainer').eq(0).bind('click', handler);
    };

    VHM.Social.NewsFeed.ShowRemovePostPopup = function (e) {
        VHM.Social.NewsFeed.ShowBubble($(e.target), "false", $('#hdnLocalizationRemovePost').val());
        return false;
    };
    VHM.Social.NewsFeed.ShowBubble = function (target, isAbuse, text) {
        var popup = $('#RemoveAbusePopup');
        var isOpened = popup.attr('opened');
        var li = target.closest('li');
        var textContainer = popup.find('.popupTextContainer').eq(0);
        var lastActivityID = textContainer.attr('activityentityid');
        if (isOpened === "true") {
            var openedBubbleLi = $("li.newsFeedItems[newsfeedid='Wall_" + lastActivityID + "']");
            VHM.Social.NewsFeed.HidePopup(target);
            if (openedBubbleLi.find(target).length > 0) {
                return;
            }
        }
        var currentActivityID = li.attr('activityentityid');
        var postTypeID = li.attr('PostTypeID');
        var groupid = li.attr('SpaceID');

        if (currentActivityID != lastActivityID) {
            textContainer.attr('activityentityid', currentActivityID);
            textContainer.attr('reportAbuse', isAbuse);
            textContainer.attr('posttypeid', postTypeID);
            textContainer.attr('groupid', groupid);
            textContainer.text(text);
            var coords = target.offset();
            popup.css({ top: coords.top - 11, left: coords.left - 118 }).toggle();
            popup.attr('opened', 'true');
            li.unbind('mouseenter').unbind('mouseleave');
        }
    };
    VHM.Social.NewsFeed.ShowReportAbusePopup = function (e) {
        VHM.Social.NewsFeed.ShowBubble($(e.target), "true", $('#hdnLocalizationReportAsAbuse').val());
        return false;
    };
    VHM.Social.NewsFeed.HidePopup = function (target) {
        var li;
        var popup = $('#RemoveAbusePopup');
        popup.toggle();
        popup.attr('opened', 'false');
        var popupTextContainer = popup.find('.popupTextContainer').eq(0);
        var wallID = popupTextContainer.attr('activityentityid');
        if (wallID) {
            li = $("li.newsFeedItems[newsfeedid='Wall_" + wallID + "']");
            if (li.length == 0) {
                return;
            }
            if (li.find(target).length == 0) {
                li.find('.closeIconContainer').eq(0).hide();
            } //&& $(e.target).hasClass('closeIconContainer') == false
            li.unbind('mouseenter').unbind('mouseleave').find('.closeIconContainer').eq(0).unbind('click');
            VHM.Social.NewsFeed.AttachCloseIconEvents(li);
            popupTextContainer.attr('activityentityid', '');
        }
    };
    VHM.Social.NewsFeed.CloseAbusePopup = function (e) {
        $('#ReportAbuseModal').hide();
    };

    //    VHM.Social.NewsFeed.Reload = function () {

    //        $('#newsContainer').html("").addClass("Loading");
    //        if (VHM.Social.NewsFeed.isMini == 'true') {
    //            clearInterval(VHM.Social.NewsFeed.ReloadTimerID);
    //        }
    ////        $.ajax({
    ////            type: 'POST',
    ////            url: "../ajax/NewsFeed.aspx",
    ////            data: { "isMini": VHM.Social.NewsFeed.isMini, "DisplayMode": VHM.Social.NewsFeed.displayMode, "GroupID": $('#hdnGroupId').val(), "EntityID": $("#hdnEntityID").val() },
    ////            success: function (responseData, textStatus) {

    ////                $('#newsContainer').removeClass("Loading");
    ////                if (responseData == "fail") {
    ////                    responseData = VHM.Social.NewsfeedFailMessage;
    ////                }

    ////                var cleanText = convertInHtml(responseData)


    ////                $('#newsContainer').hide().html(cleanText);

    ////                if (IS_SEC == true) {
    ////                    $(".spanActionText").each(function () {
    ////                        if ($(this).html().indexOf("posted in") >= 0) {
    ////                            $(this).parent().parent().parent().hide();
    ////                        }
    ////                    });
    ////                }
    ////                $('#newsContainer').show();

    ////                cutLink();
    ////                //RebuildNewsContainre();
    ////                VHM.Social.NewsFeed.AttachEvents(VHM.Social.NewsFeed.isMini);
    ////                $("#NewsFeedEloquaContent").remove();
    ////                if (VHM.Social.NewsFeed.isMini == 'true') {
    ////                    VHM.Social.NewsFeed.ReloadTimerID = setInterval(VHM.Social.NewsFeed.SightlessReload, 600000);
    ////                    var jqoRockStarWinn = $('.rockstarWinner');
    ////                    VHM.Badges.GetRockStarWinner(VHM.Social.SessionID, jqoRockStarWinn);
    ////                }

    ////                expandables();
    ////            }
    ////        });
    //    }

    VHM.Social.NewsFeed.LoadPage = function (pageNo) {
        var loadingMarkup = '<li>&nbsp;</li>';
        var $sel = $('li', '#newsContainer').last();
        if (pageNo <= 1) {
            loadingMarkup = '<ul>' + loadingMarkup + '</ul>';
            $sel = $('#newsContainer');
        }

        var loadingLi = $(loadingMarkup).addClass("Loading", 1000).appendTo($sel);

        if (!VHM.Social.NewsFeed.displayMode) {
            VHM.Social.NewsFeed.displayMode = $(".hiddenDisplayField").val();
        }
        if (!VHM.SocialGroups.GroupID) {
            VHM.SocialGroups.GroupID = $("#hdnGroupId").val();
        }
        var weeklyStatus;
        if ($("#hdnWeeklyStatusUpdatePrompt")) {
            weeklyStatus = $("#hdnWeeklyStatusUpdatePrompt").val().replace('&apos;', "'");
        } else {
            weeklyStatus = "";
        }
        $.ajax({
            type: 'POST',
            url: "../ajax/NewsFeed.aspx",
            data: { "isMini": VHM.Social.NewsFeed.isMini, "PageNo": pageNo, "DisplayMode": VHM.Social.NewsFeed.displayMode, "GroupID": VHM.SocialGroups.GroupID, "EntityID": $("#hdnEntityID").val(), "WeeklyStatus": weeklyStatus },
            success: function (responseData, textStatus) {
                loadingLi.remove();
                if (responseData == "fail") {
                    responseData = VHM.Social.NewsfeedFailMessage;
                }

                var cleanContent = convertInHtml(responseData);

                try {
                    var newContent = $('#newsContainer').append(cleanContent);
                } catch (ex) {
                    // there was an invalid character in the content
                } finally {
                    if (IS_SEC == true) {
                        $(".spanActionText").each(function () {
                            if ($(this).html().indexOf("posted in") >= 0) {
                                $(this).parent().parent().parent().hide();
                            }
                        });
                    }

                    cutLink();
                    var f_removeGT1 = function (elmts) {
                        for (var i = 1, l = elmts.length; i < l; i++) {
                            elmts.eq(i).remove();
                        }
                    }

                    f_removeGT1($('div[id=msgBoxContainer]'));
                    f_removeGT1($('div.horizontalDelimiter'));

                    $("#NewsFeedEloquaContent").remove();
                    $("#NewsFeedEloquaContent").hide();

                    $("#shareResults").removeProp("disabled"); // on self entry challenge page

                    //VHM.Social.NewsFeed.AttachEvents(VHM.Social.NewsFeed.isMini, newContent);

                    expandables();
                }

            }
        });
    };

    //    VHM.Social.NewsFeed.ReloadComments = function (NewsFeedItem, noOfComments) {
    //        var div = NewsFeedItem.find('.LikesAndCommentsInfo').eq(0);
    //        var entityID = NewsFeedItem.attr('activityentityid');
    //        var activityFeedID = NewsFeedItem.attr('NewsFeedID');
    //        var entityType = NewsFeedItem.attr('activityentitytype');
    //        var groupID = NewsFeedItem.attr('SpaceID');
    //        div.find('.CommentsInfo').empty();
    //        div.addClass("Loading", 1000);
    //        $.ajax({
    //            type: 'POST',
    //            url: "../ajax/NewsFeedItemComments.aspx",
    //            data: { "EntityID": entityID, "ActivityFeedId": activityFeedID, "EntityType": entityType, "NoOfComments": (typeof (noOfComments) != "undefined") ? noOfComments : "", "GroupID": groupID },
    //            success: function (responseData, textStatus) {
    //                div.removeClass("Loading", 1000);
    //                div.find('.CommentsInfo').html(convertInHtml(responseData));
    //                cutLink();
    //                VHM.Social.NewsFeed.AttachEvents(VHM.Social.NewsFeed.isMini, div);
    //                NewsFeedItem.trigger('mouseleave');
    //            }
    //        });
    //    }

    VHM.Social.NewsFeed.LikeDislikeNewsFeedItem = function (e) {
        var f_success = function (responseData, textStatus) {
            if (typeof (responseData) != "object") {
                responseData = eval('(' + responseData + ')');
            }
            var YouLikedIt = responseData.YouLikedIt.toLowerCase();
            var YouLikedID = responseData.YouLikedID;
            li.attr('YouLikedIt', YouLikedIt);
            li.attr('YouLikedID', YouLikedID);
            li.find('.LikesInfo').find('span').eq(0).text(responseData.LikeString);
            $(target).text((YouLikedIt == "true") ? "Unlike" : "Like");
            if (responseData.LikeCount == 0) {
                li.find('.imgIconLikes').hide();
            } else {
                li.find('.imgIconLikes').show();
            }
        };
        var target = e.target;
        var li = $(target).closest('li');
        var ActivityEntityID = li.attr('ActivityEntityID');
        var activityFeedID = li.attr('NewsFeedID');
        var ActivityEntityType = li.attr('ActivityEntityType');
        var YouLikedID = li.attr('YouLikedID');
        var GroupID = li.attr('SpaceID');

        ActivityEntityID = isNaN(parseInt(activityFeedID)) ? ActivityEntityID : activityFeedID;
        var YouLikedIt = li.attr('YouLikedIt');
        var pl = new SOAPClientParameters();
        pl.add("ActivityEntityID", ActivityEntityID);
        pl.add("sId", VHM.Social.SessionID);
        pl.add("ActivityEntityType", ActivityEntityType);
        pl.add("YouLikedID", YouLikedID);
        pl.add("GroupID", GroupID);
        var method = (YouLikedIt && YouLikedIt.toLowerCase() == "true") ? "UnlikeActivity" : "LikeActivity";
        SOAPClient.invoke(VHM.Social.CFServiceUrl, method, pl, false, f_success);
        return false;
    };
    VHM.Social.NewsFeed.LikeDislikeWallComment = function (e) {
        var target = e.target;
        var li = $(target).closest('li');
        var ActivityEntityID = li.attr('ActivityEntityID');
        var ActivityEntityType = li.attr('ActivityEntityType');
        var YouLikedIt = li.attr('YouLikedIt');
        var f_success = function (responseData, textStatus) {
            if (typeof (responseData) != "object") {
                responseData = eval('(' + responseData + ')');
            }
            var YouLikedIt = responseData.YouLikedIt.toLowerCase();
            li.attr('YouLikedIt', YouLikedIt);
            $(target).text((YouLikedIt == "true") ? "Unlike" : "Like");
            //VHM.Social.NewsFeed.Reload();
        };
        var pl = new SOAPClientParameters();
        pl.add("ActivityEntityID", ActivityEntityID);
        pl.add("sId", VHM.Social.SessionID);
        pl.add("ActivityEntityType", ActivityEntityType);
        var method = (YouLikedIt && YouLikedIt.toLowerCase() == "true") ? "UnlikeActivity" : "LikeActivity";
        SOAPClient.invoke(VHM.Social.CFServiceUrl, method, pl, false, f_success);
        return false;
    };

    /************************************************************************************************************************************************
    **************************************NEW NOTIFICATIONS POP UP**********************************************************************************/
    VHM.Social.NewsFeed.loadNotifications = function () {
        //get class name
        VHM.Social.NewsFeed = VHM.Social.NewsFeed || {};
        VHM.Social.NewsFeed.NotificationHolder = VHM.Social.NewsFeed.NotificationHolder || $("#divVHMNotification");
        var clsName = VHM.Social.NewsFeed.NotificationHolder[0].className;
        if (typeof clsName === "undefined" || clsName == null) {
            clsName = "";
        }
        var spinner = '<img src="/v2/Content/Images/Shared/spinner.gif" alt="" style="height: 35px; width: 35px;"/>';
        $.ajax({
            url: "/secure/Social/openVHMNotifications.aspx",
            cache: false,
            dataType: "html",
            beforeSend: function () {
                VHM.Social.NewsFeed.NotificationHolder.removeClass(clsName);
                VHM.Social.NewsFeed.NotificationHolder.html(spinner).addClass("Loading");
            },
            complete: function () {
                VHM.Social.NewsFeed.NotificationHolder.removeClass("Loading");
                VHM.Social.NewsFeed.NotificationHolder.addClass(clsName);
            },
            success: function (data, textStatus) {
                $("#divVHMNotification").css("max-height", "none");
                VHM.Social.NewsFeed.NotificationHolder.html(data);
                $(".divNotificContainer input").addClass("btn yellow-button");
                VHM.Notification.LoadData();
            },
            error: function (data, textStatus) {
                VHM.Social.NewsFeed.NotificationHolder.hide();
                $("#divVHMNotification").dialog("close");
                //alert($('#hdnLocalizationOops').val(), { title: $('#hdnLocalizationWentWrong').val() });
                alert("Something went wrong", { title: "Notifications" });
            }
        });
    };

    function expandables() {

        var items = $(".dynamicCommentBox");
        for (var i = 0; i < items.length; i++) {
            var a = items[i];
            a.style.overflow = "hidden";
            var e = a.rows = a.rows > 0 ? a.rows : 2;
            b = a.cols = a.cols > 0 ? a.cols : 50;
            var g = RegExp("([^\r\n]{" + b + "})([^\r\n])"),
                f = RegExp("[^\n]{" + b + "}\n?$|[^\n]{0," + b + "}\n");
            a.onkeyup = a.onkeydown = function () {
                //            this.value = this.value.replace(g, "$1\r\n$2");
                //            for (var c = 0, d = this.value; d.search(f) >= 0; ) {
                //                c++;
                //                d = d.replace(f, "")
                //            }
                //            c += 2;
                //            if (c < e) c = e;
                //            this.rows = c;

                var getText = this.value;
                var getRegs = getText.match(/^.*(\r\n|\n|$)/gim);
                var setText = false;
                for (var i = 0; i < getRegs.length; i++) {
                    getText = getRegs[i].replace(/\r|\n/g, "");
                    setText += getText.length ? Math.ceil(getText.length / 50) : 1;
                }
                this.rows = setText + 2;
            };
        }
    }

    function RebuildNewsContainre() {
        var text = $("#newsContainer").html();
        text.replace(/&lt;/g, '<');
    }

    function convertInHtml(convert) {
        var str = convert.replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&amp;/g, '&').replace(/&#39;/g, '\x22').replace(/<script>/g, '');

        return str;
    }

    ;

    function cutLink() {
        var itemsa = $(".spanActionText").find('a');
        for (var sch = 0; sch < itemsa.length; sch++) {
            if ($(itemsa[sch]).html().length > 60) {
                $(itemsa[sch]).html($(itemsa[sch]).html().substring(0, 60) + "...");
            }
        }

        itemsa = $(".CommentText").find('a');
        for (var sch = 0; sch < itemsa.length; sch++) {
            if ($(itemsa[sch]).html().length > 60) {
                $(itemsa[sch]).html($(itemsa[sch]).html().substring(0, 60) + "...");
            }

        }
    }

    function trim(str, chars) {
        return ltrim(rtrim(str, chars), chars);
    }

    function ltrim(str, chars) {
        chars = chars || "\\s";
        return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
    }

    function rtrim(str, chars) {
        chars = chars || "\\s";
        return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
    }

    //VHM.Social.NewsFeed.AttachEvents(VHM.Social.NewsFeed.isMini);

});
