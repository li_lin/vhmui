

var showNewsFeed = true;

$(document).ready(function () {
    $('.accordion').on('show', function (e) {
        $(e.target).prev('.accordion-heading').find('.accordion-toggle').addClass('activeAccordion');
        $(e.target).prev('.accordion-heading').find('.accordion-icon').removeClass('icon-plus').addClass('icon-minus');
    });

    $('.accordion').on('hide', function (e) {
        $(this).find('.accordion-toggle').not($(e.target)).removeClass('activeAccordion');
        $(this).find('.accordion-icon').removeClass('icon-minus').addClass('icon-plus');
    });

    $("#landing-accordion .accordion-group .accordion-heading a.accordion-toggle").each(function (index, element) {
        $(element).addClass(index == 0 ? "activeAccordion" : "");
    });

    $("#landing-accordion .accordion-group .accordion-body").each(function (index, element) {
        $(element).addClass(index == 0 ? "in" : "");
    });

    $('input, textarea').placeholder();
    $('.CTTrigger').tooltip();

    $(".modal-header button").hover(function () {
        $(this).addClass("ui-state-hover");
    });
    $(".modal-header button").mouseleave(function () {
        $(this).removeClass("ui-state-hover");
    });

    //  /*Calling RunKeeper service to get the data from Runkeeper in Asynchronous*/
    $.ajax({
        url: '/v2/Activity/SyncRunkeeperData',
        data: '',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function () { },
        error: function () { }
    });

    if (showPopUpForTermsPage == true || showPopUpForTermsToAcknowledge == true) {
        $('#termsPage').modal('show');
    }


    $('.main-nav .nav > li.menu-item-li').hover(
        function () { $(this).addClass('active') },
        function () { $(this).removeClass('active') });

    var base = '../../WebServices/WSVHMActivityJournal.asmx/';
    var dateInput = new Date(Date.parse($('#inputDateAL-AddEntry').val()));
    
    /*
    if (document.getElementById("collapseTwo")) {
        $.ajax({
            type: 'POST',
            url: base + "GetDailyCaloriesEntries",
            contentType: "application/json; charset=utf-8",
            data: "{'sessionID':'" + VHM.AJ.ReadCookie("SessionID") + "','selectedDate': '" + ("0" + (dateInput.getMonth() + 1)).slice(-2) + "/" + ("0" + (dateInput.getDate())).slice(-2) + "/" + dateInput.getFullYear() + "', span: 'DAY' }",
            dataType: "json",
            async: false,
            success: function (response) {
            },
            complete: function () {
            }
        });
    }*/

    $(".thicker-info a").click(function (e) {
        TrackGoogleAnalyticEvent('Wolverine', 'Click', 'Old Landing Page');
        UseNewLanding(false);
        e.preventDefault();
    });


    ko.applyBindings(new UpForGrabsModel(), document.getElementById("upForGrabs"));

    var sumlandingPageSection = 0;
    var numberOfRecentActivites = $("#upForGrabsSection .upForGrabsItemRecently").length;

    $('.upForGrabsItemRecently:lt(3)').each(function () {
        sumlandingPageSection += $(this).outerHeight();

    });


    sumlandingPageSection = sumlandingPageSection + 4;
    $('#upForGrabsSection').css({ height: sumlandingPageSection + "", overflow: 'hidden' });
    if (numberOfRecentActivites > 3) {

        $("#recentActivityList-ShowMore").click(function () {

            $this = $("#upForGrabsSection");
            if ($this.data('open')) {
                $this.animate({ height: sumlandingPageSection + 'px' });
                $this.data('open', 0);
                $('#recentActivityList-ShowMore').html(i18n.t("WolverineLandingPage.GlobalLabels.ShowMore", {defaultValue:"Show more"} ));
            } else {
                $this.animate({ height: '100%' });
                $this.data('open', 1);
                $('#recentActivityList-ShowMore').html(i18n.t("WolverineLandingPage.GlobalLabels.ShowLess", { defaultValue: "Show less" }));
            }
            return false;
        });
    } else {
        $('#upForGrabsSection').css({ height: 100 + "%", overflow: 'hidden' });
        $('#recentActivityList-ShowMore').hide();
    }
    var dialogDefaults = { autoOpen: false, modal: true, resizable: false, draggable: false };

    $("#divActivityList").dialog($.extend({
        position: ['center', 100],
        width: 770,
        title: i18n.t("WolverineLandingPage.GlobalLabels.AddActivityToYourList", { defaultValue: "Add Activity to your list" })
    }, dialogDefaults));

    //Welcome journey steps
    $('#wj-step1-next').on('click', function () {
        if (ValidateWJData() == true) {
            if (saveWJv3($('#measurementsType').val(), $('#cultureId').val())) {
                $('#wj-step1').hide();
                $('#wj-step2').fadeIn();
                return true;
            } else {
                return false;
            }

        }
    });

    $('#wj-step2-next').on('click', function () {
        if (ValidateWJData() == true) {
            $('#wj-step1').hide();
            $('#wj-step2').hide();
            $('#wj-step3').fadeIn();
            if (hraSetting == -1 || hraSetting == 0 || hraSetting == 3)
            {
                $('#wj-step3-next').show();
                $('#wj-hs').hide();
                $('#setPrivacy').show();
            } else if (hraSetting == 1) {
                $('#wj-step3-next').show();
                $('#wj-hs').show();
                $('#setPrivacy').show();
            } else if (hraSetting == 2) {
                $('#wj-step3-next').hide();
                $('#wj-hs').show();
                $('#setPrivacy').hide();
            }
        }
    });
    //finish wj
    $('#wj-step3-next').on('click', function () {
        if (hraSetting != 2) {
            $('#divWelcomeJourneyV3').modal('hide');
        }
    });
    $('#wj-hs').on('click', function () {
        window.location = "/secure/healthsnapshot/hrassessment.aspx"
    });

    //Redeem a Voucher - Message reset
    $('#divRedeemAVoucherNewLanding').on('hide', function () {
        $('.VHMRedeemError').css('display', 'none');
        $('.check-code').css('display', 'none');
        $('#btnSubmitVoucher').removeAttr("disabled");
        $('#inpFirstRedeem').val("");
        $('#inpSecondRedeem').val("");
        $('#inpThirdRedeem').val("");
    });

    function setLapsContent() {
        var content = i18n.t("WolverineLandingPage.GlobalMessages.WaysToEarnMonthlyPrizeDrawings", { currency: $("#gameCurrency").val().toLowerCase() });
        $("#WaysToEarnMonthlyPrizeDrawings").html(content);

        content = i18n.t("WolverineLandingPage.HowItWorks.CompleteChallengeDoTask", { currency: $("#gameCurrency").val().toLowerCase() });
        $("#CompleteChallengeDoTask").html(content);

        content = i18n.t("WolverineLandingPage.HowItWorks.CompleteLap", { currency: $("#gameCurrency").val().toLowerCase() });
        $("#CompleteLap").html(content);

        content = i18n.t("WolverineLandingPage.HowItWorks.AccelerateWithPedometer", { currency: $("#gameCurrency").val().toLowerCase() });
        $("#AccelerateWithPedometer").html(content);

        content = i18n.t("WolverineLandingPage.HowItWorks.YouCouldWin", { currency: $("#gameCurrency").val().toLowerCase() });
        $("#YouCouldWin").html(content);

        content = i18n.t("WolverineLandingPage.HowItWorks.WaysToEarnEntries", { currency: $("#gameCurrency").val() });
        $("#WaysToEarnEntries").html(content);
    }

    $('#gameHelp').on("click", function () {

        setLapsContent();
        $( "#challenges-how-it-works" ).modal( 'show' );
    } );

    //Game and Prizes Modal
    $('#numberOfEntries').on("click", function () {
        setLapsContent();
        
        $('#game-prizes').modal('show');
    });

//    $.ajax({
//        url: '../social/ContentArea',
//        cache: false,
//        data: { contentAreaId: "0797af26-fd1e-45a1-ac15-d805dc5f803f" },
//        success: function (view) {
//            if (view.indexOf('Level') >= 0) {
//                $("#twoBytwo").hide().html(view).fadeIn('normal');
//            }

//        },
//        error: function () {
//            LoadError("twoBytwo");
//        }
    //    });
  
});

//Welcome Journey validations
function ValidateWJData() {
    if ($('#measurementsType').val() == "Imperial") {
        var measureSystem = $('#measurementsType').val();
        ft = $(".height-field-feet").val();
        inch = $(".height-field-inches").val();
        sysVal = ConversionUtil.Height.ImperialToSys(parseInt(ft), parseInt(inch));
        oHeight = sysVal;
        oWeight = ConversionUtil.Weight.ImperialUSToSys($(".weight-field").val());


        var aError = new Array();
        if (oHeight == "" || oHeight === "NaN") {
            aError.push(i18n.t("WolverineLandingPage.WelcomeJourney.ErrorMessages.HeightEmpty", {defaultValue:"Please enter the height."} ));
        } else if ('NaN' === oHeight || isNaN(parseFloat(oHeight))) {
            aError.push(i18n.t("WolverineLandingPage.WelcomeJourney.ErrorMessages.HeightNonNumeric", {defaultValue:"Height must be a number."} ));
        } else if (parseFloat(oHeight) < 120 || parseFloat(oHeight) > 300) {
            if (measureSystem == 'Imperial' || measureSystem == 'UK Imperial') {
                aError.push(i18n.t("WolverineLandingPage.WelcomeJourney.ErrorMessages.HeightRangeIncorrectImp", {defaultValue:"Height must be greater than 4 feet but less than 9 feet."} ));
            } else if (measureSystem == 'Metric') {
                aError.push(i18n.t("WolverineLandingPage.WelcomeJourney.ErrorMessages.HeightRangeIncorrectMet", {defaultValue:"Height must be greater than 120 cm but less than 300 cm."} ));
            }
        }

        if (oWeight == "") {
            aError.push(i18n.t("WolverineLandingPage.WelcomeJourney.ErrorMessages.WeightEmpty", {defaultValue:"Please enter the weight."} ));
        } else if ('NaN' === oWeight || isNaN(parseFloat(oWeight))) {
            aError.push(i18n.t("WolverineLandingPage.WelcomeJourney.ErrorMessages.WeightNonNumeric", {defaultValue:"Weight must be a number."} ));
        } else if (parseFloat(oWeight) < 22.5 || parseFloat(oWeight) > 204.5) {
            if (measureSystem == 'Imperial') {
                aError.push(i18n.t("WolverineLandingPage.WelcomeJourney.ErrorMessages.WeightRangeIncorrectImp", {defaultValue:"Weight must be greater than 50 lbs but less than 450 lbs."} ));
            } else if (measureSystem == 'UK Imperial') {
                aError.push(i18n.t("WolverineLandingPage.WelcomeJourney.ErrorMessages.WeightRangeIncorrectUK", {defaultValue:"Weight must be greater than 3 stone 8 lbs but less than 32 stone 2 lbs."} ));
            } else if (measureSystem == 'Metric') {
                aError.push(i18n.t("WolverineLandingPage.WelcomeJourney.ErrorMessages.WeightRangeIncorrectMet", {defaultValue:"Weight must be greater than 23 kg but less than 204 kg."} ));
            }
        }
        if (aError.length > 0) {
            alert(aError.join("\n"));
            return false;

        } else {
            return true;
        }
    }
};

function saveWJv3(measureSystem, cultureId) {
    debugger;
    var oWeight;
    var oHeight; //to system value
    var ft;
    var inch;
    var sysVal;
    if (measureSystem == 'Imperial') {
        ft = $(".height-field-feet").val();
        inch = $(".height-field-inches").val();
        sysVal = ConversionUtil.Height.ImperialToSys(parseInt(ft), parseInt(inch));
        oHeight = sysVal; // ConversionUtil.Height.sysToImperial(sysVal);
        oWeight = ConversionUtil.Weight.ImperialUSToSys($(".weight-field").val());
    } else if (measureSystem == 'UK Imperial') {
        ft = $("#ddHtFtUK").val();
        inch = $("#ddHtInUK").val(); //stones
        sysVal = ConversionUtil.Height.ImperialToSys(parseInt(ft), parseInt(inch));
        oHeight = sysVal;
        var lbs = ($("#txtWtLbsUK").val() != "") ? $("#txtWtLbsUK").val() : 0;
        var stn = ($("#txtWtStUK").val() != "") ? $("#txtWtStUK").val() : 0;
        oWeight = ConversionUtil.Weight.ImperialUKToSys(stn, lbs);
    } else {
        oHeight = ($(".height-field-feet").val() != "") ? $(".height-field-feet").val() : 0;
        oWeight = ($(".weight-field").val() != "") ? $(".weight-field").val() : 0;
    }
    $.ajax({
        cache: false,
        contentType: "application/json; charset=utf-8",
        url: "FinishExpressWelcomeJourney",
        data: { weight: oWeight, height: oHeight, cultureId: cultureId },
        dataType: "html",
        success: function (responseData) {
            if (responseData != null && responseData.indexOf("Service Error") < 0) {


            } else {
                alert('Something went wrong. Please try again later. ', { title: 'Something went wrong' });
            };
        },
        error: function (xhr, ajaxOptions, error) {
            alert('Something went wrong. Please try again later. ', { title: 'Something went wrong' });
        }
    });



    return true;

};


function UseNewLanding(answer) {
    $.ajax({
        url: '../Member/UseNewLanding',
        type: 'post',
        data: { useNewLanding: answer },
        success: function () {
            if (!answer) {
                location.reload();
            }
        },
        error: function () {
            alert("Something went wrong please try again!");
        }
    });
};

VHM.AJ.ReadCookie = function (name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0)
            return c.substring(nameEQ.length, c.length);
    }
    return null;
};



function StripOutHTMLTags(text) {
    return text.replace(/<\/?[^>]+>/gi, '');
};

function LoadNewsFeed() {
    $.ajax({
        url: '../social/NewsFeedV3',
        cache: false,
        success: function (view) {
            if (showNewsFeed) {
                $("#newsfeedSection").hide().html(view).fadeIn('normal');
                LoadNewsFeedFunctions();
                //LoadSideNavDisplaySettings($("#newsfeedSection"));
                $(".newsfeedContainer").i18n({ ns: 'LocalizationLibrary' });
            } else {
                $("#newsfeedSection").hide().html(view);
                LoadNewsFeedFunctions();
                //LoadSideNavDisplaySettings($("#newsfeedSection"));
            }
        }
    });
}

var UpForGrabsModel = function () {
    var self = this;

    self.Grabs = ko.observableArray([]);
    self.hasRewards = ko.observable(true);
    self.hasNoRewards = ko.observable(false);
    self.GetModel = function () {
        loadUpForGrabs();
    };
    var loadUpForGrabs = function () {
        var numberOfRewards = 0;
        $.ajax({
            url: 'WhatsNextForMe',
            cache: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.Rewards.length == 0) {
                    self.hasNoRewards(true);
                    self.hasRewards(false);
                    $('#upForGrabs-ShowMore').hide();
                } else {
                    self.hasRewards( true );
                    self.hasNoRewards( false );
                    if(typeof String.prototype.trim !== 'function') {
                        String.prototype.trim = function() {
                            return this.replace(/^\s+|\s+$/g, ''); 
                        }
                    }
                    $(data.Rewards).each( function() {

                        var rewardType = ( this.Rewards[0].RewardType === "HMILE" ) ? "healthmiles" : this.Rewards[0].RewardTypeDescription;
                        
                        this.isText = function( t ) {
                            try {
                                return t.trim() === 'TEXT';
                            } catch( e ) {
                                return false;
                            }
                        };
                        this.isBadge = function (t) {
                            try {
                                return t.trim() === 'BADGE';
                            } catch ( e ) {
                                return false;
                            }
                        };
                        numberOfRewards++;
                        if ( rewardType == "null" || rewardType == null || !rewardType )
                            rewardType = "healthmiles";
                        this.Comment = "<h2>" + this.UFGTaskName + "</h2>";

                        if (this.isText(this.Rewards[0].RewardType)) {
                            this.Comment = this.Comment + this.Rewards[0].Text;
                        } else if (this.isBadge(this.Rewards[0].RewardType) && this.Rewards[0].Amount == 0 ) {
                            this.Comment = this.Comment + i18n.t( "WolverineLandingPage.MiscLabels.YouCanEarn", { defaultValue: "You can earn" } ) + " <span class='grabPoints'>" + i18n.t( "WolverineLandingPage.MiscLabels.ABadge", { defaultValue: "a Badge" }) + "</span> " + i18n.t( "WolverineLandingPage.MiscLabels.ByCompletingThisActivity", { defaultValue: "by completing this activtiy." } );
                        } else {
                            this.Comment = this.Comment + i18n.t( "WolverineLandingPage.MiscLabels.YouCanEarn", { defaultValue: "You can earn" } ) + " <span class='grabPoints'>" + this.Rewards[0].Amount + " " + rewardType + "</span> " + i18n.t( "WolverineLandingPage.MiscLabels.ByCompletingThisActivity", { defaultValue: "by completing this activtiy." } );
                        }
                        this.Title = i18n.t( "WolverineLandingPage.MiscLabels.LetsGo", { defaultValue: "Let's go" } );
                        if ( this.UFGTaskRewardURL != null )
                            this.hasUrl = 1;
                        else {
                            this.hasUrl = 0;
                        }
                    } );
                    
                    self.Grabs(data.Rewards);
                }
                var sumUpForGrabsItem = 0;
                $('.upForGrabsItem:lt(3)').each(function () {
                    sumUpForGrabsItem += $(this).outerHeight();
                });
                sumUpForGrabsItem = sumUpForGrabsItem + 4;
                $('#upForGrabsSectionWhatNext').css({ height: sumUpForGrabsItem + "px", overflow: 'hidden' });
                if (numberOfRewards > 3) {
                    $("#upForGrabs-ShowMore").click(function () {

                        $this = $("#upForGrabsSectionWhatNext");
                        if ($this.data('open')) {
                            $this.animate({ height: sumUpForGrabsItem + "px" });
                            $this.data('open', 0);
                            $('#upForGrabs-ShowMore').html(i18n.t("WolverineLandingPage.GlobalLabels.ShowMore", { defaultValue: "Show more" }));
                        } else {
                            $this.animate({ height: '100%' });
                            $this.data('open', 1);
                            $('#upForGrabs-ShowMore').html(i18n.t("WolverineLandingPage.GlobalLabels.ShowLess", { defaultValue: "Show less" }));
                        }
                        return false;
                    });
                } else {
                    $('#upForGrabsSectionWhatNext').css({ height: 100 + "%", overflow: 'hidden' });
                    $("#upForGrabs-ShowMore").hide();
                }
            },
            error: function () {
                
            }
        });


    };

    self.GetModel();
};



function LoadNewsFeedFunctions() {
    $("#newPostText").keyup(function () {
        var hasText = $(this).val().length > 0;
        $("input#submitPost").removeClass("disabled");
        $(this).attr("hasText", hasText ? "true" : "false");
    });

    $("#newPostText").focus(function () {
        var hasText = $(this).attr("hasText");

        $(this).removeClass("disabled").addClass("active")
            .val(hasText == "true" ? $(this).val() : "");
    });

    $("#newPostText").blur(function () {
        var hasText = $(this).attr("hasText");
        if (hasText == "true") {
            $("input#submitPost").removeClass("disabled");
        } else {
            $("input#submitPost").addClass("disabled");
        }
        $(this).removeClass("active")
            .addClass("disabled")
            .val(hasText == "true" ? $(this).val() : $(this).attr("message"));
    });

    $("#submitPost").click(function () {
        if ($(this).hasClass("disabled")) {
            return false;
        }
        TrackGoogleAnalyticEvent('Wolverine', 'Click', 'Newsfeed Post');
        var text = StripOutHTMLTags($("#newPostText").val());
        var defaultText = $("#newPostText").attr("message");
        appendNewPost(text);
        $("#newPostText").val(defaultText)
            .attr("hasText", "false");

        $("input#submitPost").addClass("disabled");
        return true;
    });

    $("a.profileLink").click(function () {

        var friendId = $(this).attr("friendId");
        var memberId = $(this).attr("memberId");

        var data = { friendId: friendId };
        $.ajax({
            url: '../Social/UserProfile',
            data: data,
            success: function (view) {

                $(view).dialog({
                    title: false,
                    autoOpen: true,
                    width: 520,
                    resizable: false,
                    open: function () {
                        $(".ui-dialog-titlebar").css("height", "0px");
                    },
                    close: function () {
                        $("div#divUserProfile").remove();
                    },
                    modal: true
                });

                $("#ahrfRemovefr").click(function () {
                    $("div#divRemoveFriend").dialog({
                        modal: true,
                        width: 300,
                        position: ['center', 50],
                        zIndex: 4000,
                        title: "Remove as Friend",
                        resizable: false
                    });
                    return false;
                });

                $("#btnAddAsFriend").click(function () {
                    $(this).unbind("click");
                    AddFriend(friendId);
                });
                $("#btnRemove").click(function () {
                    $(this).unbind("click");
                    RemoveFriend(friendId);
                });
                $("#btnChallengeMe").click(function () {
                    $(this).unbind("click");
                    window.location.href = '/Secure/Challenge/create.aspx?mid=' + memberId + '&challengeid=';
                });

            }
        });

        return false;
    });

    $("a.likeLink").click(function () {
        var $this = $(this);
        var postId = $this.attr('postId');
        var youLikedIt = $this.attr('youLikedIt');
        var youLikedId = $this.attr('youLikedId');
        var postById = $this.attr('postById');
        var groupId = $this.attr('groupId');

        if (youLikedIt && youLikedIt.toLowerCase() == "true") {
            UnLikePost(youLikedId, postId, groupId);
            TrackGoogleAnalyticEvent('Wolverine', 'Click', 'Newsfeed Unlike');
        } else {
            LikePost(postId, postById, groupId);
            TrackGoogleAnalyticEvent('Wolverine', 'Click', 'Newsfeed Like');
        }

        return false;
    });
}

function LikePost(postId, postById, groupId) {

    var data = { postId: postId, postById: postById, groupId: groupId };
    $.ajax({
        data: data,
        url: '/v2/Social/LikePost',
        success: function () {
            LoadNewsFeed();
        },
        error: function () {

        }
    });
}

function UnLikePost(youLikedId, activityEntityId, groupId) {

    var data = { youLikedId: youLikedId, activityEntityId: activityEntityId, groupId: groupId };
    $.ajax({
        data: data,
        url: '/WebServices/WSVHMSocial.asmx/UnlikePost',
        success: function () {
            LoadNewsFeed();
        },
        error: function () {

        }
    });
}

function appendNewPost(text) {
    submitPost(text);
}

function submitPost(messageText) {
    //check for null/empty message - do not allow
    //already message was StripOutHTMLTags
    if (messageText != "" && $.trim(messageText).length >= 3) {
        var url = "../social/SubmitPost"; // '@Url.Action("SubmitPost","Social")'; //'/social/SubmitPost';
        try {
            var data = { messageText: messageText, isGroupPost: false, groupId: 0 };
            var sData = JSON.stringify(data);
            $.ajax({
                cache: false,
                contentType: "application/json; charset=utf-8",
                data: sData,
                dataType: 'json',
                type: 'POST',
                url: url,
                success: function (responseData) {
                    if (responseData != null && responseData.indexOf("Service Error") < 0) {
                        var result = $.parseJSON(responseData);
                        if (result.Success) {
                            LoadNewsFeed();
                        }
                    }
                },
                error: function (xhr, ajaxOptions, error) {
                    alert("error");
                }
            });
        } catch (ex) {
            alert("There was a problem saving your post");
        }
    }
}


/**************************************************************************************
Redeem a voucher
***************************************************************************************/
function openRedeemVoucher() {

    var redeemDialog = $("#divRedeemAVoucherNewLanding");
    redeemDialog.dialog({
        title: false,
        autoOpen: true,
        width: 500,
        resizable: false,
        open: function () {
            $(".ui-dialog-titlebar").css("height", "0px");
            var urlToOpen = "ShowRedeemVoucher";
            $(this).load(urlToOpen, {}, function (response, status) {
                if (status != "success" || response.indexOf("Service Error") >= 0) {
                    alert('Sorry, something went wrong. Please try again later.', { title: 'Something went wrong' });
                } else {
                    updatePageContent();
                }
            });
        },
        close: function () { },
        modal: true
    });
}
function CheckRedeemField(event, input, maxChars, index) {
    if (event.which || event.keyCode) {
        if (event.which == 13 || event.keyCode == 13 ||
            event.which == 46 || event.keyCode == 46 ||
            event.which == 8 || event.keyCode == 8) {
            return false;
        }
    }

    var length = input.value.length;

    if (length >= maxChars) {
        switch (index) {
            case 1:
                $('#inpSecondRedeem').focus();
                break;
            case 2:
                $('#inpThirdRedeem').focus();
                break;
            case 3:
                $("div.VHMvoucherRedeemPopup .VHMpopup .VHMSubmitRedeemCode").focus();
                break;
        }

        if (length > maxChars) {
            input.value = input.value.substring(0, maxChars);
        }
    }
}

function OpenCloseRedeem(option) {
    var redeemDialog = $("#divRedeemAVoucherNewLanding");
    if (option) {
        redeemDialog.dialog('open');
    } else {
        redeemDialog.dialog('close');
    }
}

function redeemVoucher(hdnErrText, btnObj) {
    var codeFirst = $("#inpFirstRedeem").val();
    var alertMessText = $('#' + hdnErrText).html();
    if (codeFirst.length > 0) {
        //onclick - disable button
        $(btnObj).attr('disabled', 'disabled');
        // inpFirstRedeem, inpSecondRedeem, inpThirdRedeem - parts of the Redeem code
        var message = $("#errorMessage");
        message.hide();

        var redeemCode = '';
        if (codeFirst.length == 4 && $("#inpSecondRedeem").val().length == 4 && $("#inpThirdRedeem").val().length == 2) {
            redeemCode = codeFirst + '-' + $("#inpSecondRedeem").val() + '-' + $("#inpThirdRedeem").val();
        }
        else {
            $(btnObj).removeAttr('disabled');
            alert(alertMessText);
            return;
        }

        var param = { redeemCode: redeemCode };
        var sParam = JSON.stringify(param);
        $.ajax({
            cache: false,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "RedeemVoucher",
            data: sParam,
            dataType: "json",
            success: function (responseData) {

                if (responseData != null && responseData.indexOf("Service Error") < 0) {
                    var result = $.parseJSON(responseData);
                    var suportUrl = getRootUrl();
                    if (!result.Success) {
                        message.html(result.ErrorMessage + ' <a target="_self" href="' + suportUrl + 'secure/help/contactusform.aspx">' + result.ErrorLinkText + '.</a>');
                        message.show();
                        document.getElementById('inpFirstRedeem').value = document.getElementById('inpSecondRedeem').value = document.getElementById('inpThirdRedeem').value = "";
                        codeFirst = "";
                        redeemCode = "";
                        $(btnObj).removeAttr('disabled');
                    } else {
                        $('.check-code').show();
                        window.setTimeout(function () {
                            $("#divRedeemAVoucherNewLanding").modal("hide");
                        }, 2000);
                    }
                } else {
                    alert('Something went wrong. Please try again later. ', { title: 'Something went wrong' });
                };
            },
            error: function () {
                alert('Something went wrong. Please try again later. ', { title: 'Something went wrong' });
            }
        });
    } else {
        alert(alertMessText);
    }
}

function getRootUrl() {
    var rootUrl = window.location.host.toLowerCase();
    if (rootUrl.indexOf('http://') == -1 && rootUrl.indexOf('https://') == -1) {
        rootUrl = 'https://' + rootUrl;
    }
    if (rootUrl.indexOf('/v2') >= 0) {
        rootUrl = rootUrl.replace("/v2", "");
    }
    if (rootUrl.lastIndexOf('/') != (rootUrl.length - 1)) {
        rootUrl += '/';
    }
    return rootUrl;
}
var intervalIndex;
var temp;
function RotateTicker(isPaused, increment) {
    var pause = 5000; // in milliseconds) 
    var length = $("#tickerContent li").length;

    this.getLi = function () {

        var liIndex;
        if (temp + increment == 0 && increment == -1) {//if the increment is -1 countdown the li index 
            liIndex = length;
        } else if (temp == -1 || temp == null || (temp + increment == 0 && increment == 1) || (temp == length && increment == 1)) {
            liIndex = 1;
        } else {
            liIndex = temp + increment;
        }
        return liIndex;
    };
    this.show = function () {
        var currentLi = getLi();
        temp = currentLi;
        $("#tickerContent li").fadeOut("Slow");
        $("#tickerContent li:nth-child(" + currentLi + ")").fadeIn(1000);


    };

    show();

    if (isPaused == true || length == 1) {

        clearInterval(intervalIndex);
    } else {
        intervalIndex = setInterval(show, pause);
    };
}

function ManualRotation(increment) {
    RotateTicker(true, increment);

}



var cSpeed = 9;
var cWidth = 128;
var cHeight = 128;
var cTotalFrames = 20;
var cFrameWidth = 128;
var cImageSrc = '/v2/Content/Images/Shared/loading-sprites.gif';
var loaderElement = 'loading-element';

var cImageTimeout = false;
var cIndex = 0;
var cXpos = 0;
var cPreloaderTimeout = false;
var SECONDS_BETWEEN_FRAMES = 0;

function startAnimation() {
    $("#loading-element").css("background-image", 'url(' + cImageSrc + ')');
    $("#loading-element").css("width", cWidth);
    $("#loading-element").css("height", cHeight);
    $("#loading-element").css("margin", "0 auto");

    //FPS = Math.round(100/(maxSpeed+2-speed));
    FPS = Math.round(100 / cSpeed);
    SECONDS_BETWEEN_FRAMES = 1 / FPS;

    cPreloaderTimeout = setTimeout('continueAnimation()', SECONDS_BETWEEN_FRAMES / 1000);
}

function continueAnimation() {

    cXpos += cFrameWidth;
    //increase the index so we know which frame of our animation we are currently on
    cIndex += 1;

    //if our cIndex is higher than our total number of frames, we're at the end and should restart
    if (cIndex >= cTotalFrames) {
        cXpos = 0;
        cIndex = 0;
    }

    if ( ( "#loading-element" ) )
        $( "#loading-element" ).css( "background-position", -cXpos );

    cPreloaderTimeout = setTimeout('continueAnimation()', SECONDS_BETWEEN_FRAMES * 1000);
}

function stopAnimation() {//stops animation
    clearTimeout(cPreloaderTimeout);
    cPreloaderTimeout = false;
}

function imageLoader(fun)//Pre-loads the sprites image
{
    clearTimeout(cImageTimeout);
    cImageTimeout = 0;
    genImage = new Image();
    genImage.onload = function () { cImageTimeout = setTimeout(fun, 0) };
    genImage.onerror = new Function('alert(\'Could not load the image\')');
    genImage.src = cImageSrc;
}
