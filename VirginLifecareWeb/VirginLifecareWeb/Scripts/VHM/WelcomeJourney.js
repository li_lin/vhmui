﻿/***************** Welcome Journey - STEP ONE: Culture Format for weight/height and Date/Time *****************/
var InitializeWJStepOne = function () {
    //set the on change event for culture format dropdown
    //check if any selection was made (probably not). if so, show options, otherwise, hide them
    hideSelectionFieldsForCulture();

    //on first step, default to en-US if no selection is made
    var DEFAULT_CULTURE_ONLOAD = "86";
    $('#CultureFormats').val(DEFAULT_CULTURE_ONLOAD);
    showSelectionFieldsForCulture();
    GetCultureFormat(DEFAULT_CULTURE_ONLOAD);

    $('#CultureFormats').change(function () {
        var selValue = $(this).val();
        if (selValue == '') {
            hideSelectionFieldsForCulture();
        } else {
            showSelectionFieldsForCulture();
            GetCultureFormat(selValue);
        }
    });
    $("#SponsorLanguages").change(function () {
        saveLanguageId();
        window.location.reload(true);
        updatePageContent();
    });
};

var GetCultureFormat = function (cultureId) {
    if (typeof cultureId != "undefined" && parseInt(cultureId) > 0) {
        var param = { cultureId: parseInt(cultureId) };
        var sParam = JSON.stringify(param);
        $.ajax({
            cache: false,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "GetSpecificCulture",
            data: sParam,
            dataType: "json",
            success: function (responseData) {
                if (responseData != null && responseData.indexOf("Service Error") < 0) {
                    var result = $.parseJSON(responseData);
                    showSelectionFieldsForCulture();
                    $("#lblDateFormat").html(result.DateFormat);
                    $("#lblMeasuremFormat").html(result.MeasureSystem);
                } else {
                    alert('Something went wrong. Please try again later. ', { title: 'Something went wrong' });
                };
            },
            error: function (xhr, ajaxOptions, error) {
                alert('Something went wrong. Please try again later. ', { title: 'Something went wrong' });
            }
        });
    }
};

var hideSelectionFieldsForCulture = function () {
    $("#divCulture_Dates").hide();
    $("#divCulture_MsrmntFormat").hide();
};

var showSelectionFieldsForCulture = function () {
    $("#divCulture_Dates").show();
    $("#divCulture_MsrmntFormat").show();
};

var saveWJStepOne = function () {
    //not sure if i need this
    var oHdnFormat = $("#CultureFormats option:selected");
    var selectedCulture = $('#CultureFormats').val();


    var cultureId = parseInt(selectedCulture);
    var aErrorMissCountry = [];


//    var languageId = $("#SponsorLanguages").val();
//    if (isNaN(languageId)) {
//        languageId = 0;
//    } else {
//        languageId = parseInt($("#SponsorLanguages").val());
//    }
    if (oHdnFormat.val() == '' || isNaN(cultureId) || oHdnFormat.val() === 'Select One' ) {
        aErrorMissCountry.push("Please select a format.");
    };
    if (aErrorMissCountry.length > 0) {
        alert(aErrorMissCountry.join("\n"));
        return false;
    } else {
        $("#LoadingSpinner").dialog("open");
        var $stepDialog = $("#divWelcomeJourney");
        $stepDialog.html("");
        $.ajax({
            cache: false,
            contentType: "application/json; charset=utf-8",
            url: "SaveWJFirstStep",
            data: { cultureId: cultureId, maxWJSteps: maxWJSteps },
            dataType: "html",
            success: function (responseData) {
                if (responseData != null && responseData.indexOf("Service Error") < 0) {
                    $stepDialog.html(responseData);
                } else {
                    alert('Something went wrong. Please try again later. ', { title: 'Something went wrong' });
                };
                updatePageContent();
              },
            error: function (xhr, ajaxOptions, error) {
                alert('Something went wrong. Please try again later. ', { title: 'Something went wrong' });
            }
        });
    }
};

var saveLanguageId = function (parameters) {
    var languageId = $("#SponsorLanguages").val();
    if (isNaN(languageId)) {
        languageId = 0;
    } else {
        languageId = parseInt($("#SponsorLanguages").val());
    }

    $.ajax({
        cache: false,
        async: false,
        contentType: "application/json; charset=utf-8",
        url: "SaveLanguageId",
        data: { languageId: languageId },
        dataType: "html",
        success: function (responseData) {
            if (responseData == null || responseData.indexOf("Service Error") >= 0) {

                alert('Something went wrong. Please try again later. ', { title: 'Something went wrong' });
            };

            updatePageContent();
        },
        error: function (xhr, ajaxOptions, error) {
            alert('Something went wrong. Please try again later. ', { title: 'Something went wrong' });
        }
    });
};
/*****************Welcome Journey - STEP TWO: set up weight and height *****************/
function changeEvtFt(evt) {
    var t = $(this);
    var sysVal = ConversionUtil.Height.ImperialToSys(parseInt(t.val()), parseInt($("#ddHtInUS").val()));

    if (!$("#uk_unitset").is(":hidden")) {
        if ($("#unit_imperial").is(":hidden")) {
            var aImp = ConversionUtil.Height.sysToImperial(sysVal);
            $("#ddHtFtUK").val(aImp["ft"]);
            $("#ddHtInUK").val(aImp["inc"]);
        } else {
            $("#txtHtCmUK").val(sysVal);
        }
    }
}
function changeEvtIn(evt) {
    var t = $(this);
    var sysVal = ConversionUtil.Height.ImperialToSys(parseInt($("#ddHtFtUS").val()), t.val());

    if (!$("#uk_unitset").is(":hidden")) {
        if ($("#unit_imperial").is(":hidden")) {
            var aImp = ConversionUtil.Height.sysToImperial(sysVal);
            $("#ddHtFtUK").val(aImp["ft"]);
            $("#ddHtInUK").val(aImp["inc"]);
        } else {
            $("#txtHtCmUK").val(sysVal);
        }
    }
}
var handleWtStUK = function () {
    var v = $("#txtWtStUK").val();
    if (isNaN(v)) {
        alert("Invalid entry. The value must be numeric.");
        return false;
    }
};
var handleLbsStUK = function () {
    var v = $("#txtWtLbsUK").val();
    v = (v != "") ? v : 0;
    if (isNaN(v)) {
        alert("Invalid entry. The value must be numeric.");
        return false;
    } else if (!(parseInt(v) >= 0 && parseInt(v) <= 13)) {
        alert("Pound must be between 0-13 pounds");
        return false;
    }
};
var handletxtHtCm = function (txtId) {
    var txtJqo = $("#" + txtId);
    var v = txtJqo.val();
    v = (v != "") ? v : 0;
    if (isNaN(v)) {
        alert("Invalid entry. The value must be numeric.");
        return false;
    }
};
var handletxtHtKg = function (txtId) {
    var txtJqo = $("#" + txtId);
    var v = txtJqo.val();
    if (isNaN(v)) {
        alert("Invalid entry. The value must be numeric.");
        return false;
    }
};
var saveExpressWJ = function (measureSystem, cultureId) {
    debugger;
    var oWeight;
    var oHeight; //to system value
    var ft;
    var inch;
    var sysVal;
    if (measureSystem == 'Imperial') {
        ft = $("#ddHtFtUS").val();
        inch = $("#ddHtInUS").val();
        sysVal = ConversionUtil.Height.ImperialToSys(parseInt(ft), parseInt(inch));
        oHeight = sysVal; // ConversionUtil.Height.sysToImperial(sysVal);
        oWeight = ConversionUtil.Weight.ImperialUSToSys($("#txtWtLbsUS").val());
    } else if (measureSystem == 'UK Imperial') {
        ft = $("#ddHtFtUK").val();
        inch = $("#ddHtInUK").val(); //stones
        sysVal = ConversionUtil.Height.ImperialToSys(parseInt(ft), parseInt(inch));
        oHeight = sysVal;
        var lbs = ($("#txtWtLbsUK").val() != "") ? $("#txtWtLbsUK").val() : 0;
        var stn = ($("#txtWtStUK").val() != "") ? $("#txtWtStUK").val() : 0;
        oWeight = ConversionUtil.Weight.ImperialUKToSys(stn, lbs);
    } else {
        oHeight = ($("#txtHtCmUK").val() != "") ? $("#txtHtCmUK").val() : 0;
        oWeight = ($("#txtWtKgUK").val() != "") ? $("#txtWtKgUK").val() : 0;
    }
    var aError = new Array();
    if (oHeight == "" || oHeight === "NaN") {
        aError.push(i18n.t("WolverineLandingPage.WelcomeJourney.ErrorMessages.HeightEmpty", {defaultValue:"Please enter the height."} ));
    } else if ('NaN' === oHeight || isNaN(parseFloat(oHeight))) {
        aError.push(i18n.t("WolverineLandingPage.WelcomeJourney.ErrorMessages.HeightNonNumeric", {defaultValue:"Height must be a number."} ));
    } else if (parseFloat(oHeight) < 120 || parseFloat(oHeight) > 300) {
        if (measureSystem == 'Imperial' || measureSystem == 'UK Imperial') {
            aError.push(i18n.t("WolverineLandingPage.WelcomeJourney.ErrorMessages.HeightRangeIncorrectImp", {defaultValue:"Height must be greater than 4 feet but less than 9 feet."} ));
        } else if (measureSystem == 'Metric') {
            aError.push(i18n.t("WolverineLandingPage.WelcomeJourney.ErrorMessages.HeightRangeIncorrectMet", {defaultValue:"Height must be greater than 120 cm but less than 300 cm."} ));
        }
    }

    if (oWeight == "") {
        aError.push(i18n.t("WolverineLandingPage.WelcomeJourney.ErrorMessages.WeightEmpty", {defaultValue:"Please enter the weight."} ));
    } else if ('NaN' === oWeight || isNaN(parseFloat(oWeight))) {
        aError.push(i18n.t("WolverineLandingPage.WelcomeJourney.ErrorMessages.WeightNonNumeric", {defaultValue:"Weight must be a number."} ));
    } else if (parseFloat(oWeight) < 22.5 || parseFloat(oWeight) > 204.5) {
        if (measureSystem == 'Imperial') {
            aError.push(i18n.t("WolverineLandingPage.WelcomeJourney.ErrorMessages.WeightRangeIncorrectImp", {defaultValue:"Weight must be greater than 50 lbs but less than 450 lbs."} ));
        } else if (measureSystem == 'UK Imperial') {
            aError.push(i18n.t("WolverineLandingPage.WelcomeJourney.ErrorMessages.WeightRangeIncorrectUK", {defaultValue:"Weight must be greater than 3 stone 8 lbs but less than 32 stone 2 lbs."} ));
        } else if (measureSystem == 'Metric') {
            aError.push(i18n.t("WolverineLandingPage.WelcomeJourney.ErrorMessages.WeightRangeIncorrectMet", {defaultValue:"Weight must be greater than 23 kg but less than 204 kg."} ));
        }
    }
    if (aError.length > 0) {
        alert(aError.join("\n"));
        return false;
    } else {
        
        $.ajax({
            cache: false,
            contentType: "application/json; charset=utf-8",
            url: "FinishExpressWelcomeJourney",
            data: { weight: oWeight, height: oHeight, cultureId: cultureId },
            dataType: "html",
            success: function (responseData) {
                if (responseData != null && responseData.indexOf("Service Error") < 0) {
                    if ($("#self-entered-activity-enabled").val().toLowerCase() == 'true') {
                        initSelfEnteredActivity();
                    }
                    $stepDialog.html(responseData);
                    updatePageContent();
                } else {
                    alert('Something went wrong. Please try again later. ', { title: 'Something went wrong' });
                };
            },
            error: function (xhr, ajaxOptions, error) {
                alert('Something went wrong. Please try again later. ', { title: 'Something went wrong' });
            }
        });
    }

    $("#ExpressWelcomeJourney").modal('hide');

    return true;

};
var saveWJStepTwo = function (measureSystem, cultureId) {
    var oWeight;
    var oHeight; //to system value
    var ft;
    var inch;
    var sysVal;
    if (measureSystem == 'Imperial') {
        ft = $("#ddHtFtUS").val();
        inch = $("#ddHtInUS").val();
        sysVal = ConversionUtil.Height.ImperialToSys(parseInt(ft), parseInt(inch));
        oHeight = sysVal; // ConversionUtil.Height.sysToImperial(sysVal);
        oWeight = ConversionUtil.Weight.ImperialUSToSys($("#txtWtLbsUS").val());
    } else if (measureSystem == 'UK Imperial') {
        ft = $("#ddHtFtUK").val();
        inch = $("#ddHtInUK").val(); //stones
        sysVal = ConversionUtil.Height.ImperialToSys(parseInt(ft), parseInt(inch));
        oHeight = sysVal;
        var lbs = ($("#txtWtLbsUK").val() != "") ? $("#txtWtLbsUK").val() : 0;
        var stn = ($("#txtWtStUK").val() != "") ? $("#txtWtStUK").val() : 0;
        oWeight = ConversionUtil.Weight.ImperialUKToSys(stn, lbs);
    } else {
        oHeight = ($("#txtHtCmUK").val() != "") ? $("#txtHtCmUK").val() : 0;
        oWeight = ($("#txtWtKgUK").val() != "") ? $("#txtWtKgUK").val() : 0;
    }
    var aError = new Array();
    if (oHeight == "" || oHeight === "NaN") {
        aError.push(i18n.t("WolverineLandingPage.WelcomeJourney.ErrorMessages.HeightEmpty", {defaultValue:"Please enter the height."} ));
    } else if ('NaN' === oHeight || isNaN(parseFloat(oHeight))) {
        aError.push(i18n.t("WolverineLandingPage.WelcomeJourney.ErrorMessages.HeightNonNumeric", {defaultValue:"Height must be a number."} ));
    } else if (parseFloat(oHeight) < 120 || parseFloat(oHeight) > 300) {
        if (measureSystem == 'Imperial' || measureSystem == 'UK Imperial') {
            aError.push(i18n.t("WolverineLandingPage.WelcomeJourney.ErrorMessages.HeightRangeIncorrectImp", {defaultValue:"Height must be greater than 4 feet but less than 9 feet."} ));
        } else if (measureSystem == 'Metric') {
            aError.push(i18n.t("WolverineLandingPage.WelcomeJourney.ErrorMessages.HeightRangeIncorrectMet", {defaultValue:"Height must be greater than 120 cm but less than 300 cm."} ));
        }
    }

    if (oWeight == "") {
        aError.push(i18n.t("WolverineLandingPage.WelcomeJourney.ErrorMessages.WeightEmpty", {defaultValue:"Please enter the weight."} ));
    } else if ('NaN' === oWeight || isNaN(parseFloat(oWeight))) {
        aError.push(i18n.t("WolverineLandingPage.WelcomeJourney.ErrorMessages.WeightNonNumeric", {defaultValue:"Weight must be a number."} ));
    } else if (parseFloat(oWeight) < 22.5 || parseFloat(oWeight) > 204.5) {
        if (measureSystem == 'Imperial') {
            aError.push(i18n.t("WolverineLandingPage.WelcomeJourney.ErrorMessages.WeightRangeIncorrectImp", {defaultValue:"Weight must be greater than 50 lbs but less than 450 lbs."} ));
        } else if (measureSystem == 'UK Imperial') {
            aError.push(i18n.t("WolverineLandingPage.WelcomeJourney.ErrorMessages.WeightRangeIncorrectUK", {defaultValue:"Weight must be greater than 3 stone 8 lbs but less than 32 stone 2 lbs."} ));
        } else if (measureSystem == 'Metric') {
            aError.push(i18n.t("WolverineLandingPage.WelcomeJourney.ErrorMessages.WeightRangeIncorrectMet", {defaultValue:"Weight must be greater than 23 kg but less than 204 kg."} ));
        }
    }
    if (aError.length > 0) {
        alert(aError.join("\n"));
        return false;
    } else {
        $("#LoadingSpinner").dialog("open");
        var $stepDialog = $("#divWelcomeJourney");
        $stepDialog.html("");
        $.ajax({
            cache: false,
            contentType: "application/json; charset=utf-8",
            url: "SaveWJFSecondStep",
            data: { weight: oWeight, height: oHeight, maxWJSteps: maxWJSteps, cultureId: cultureId },
            dataType: "html",
            success: function (responseData) {
                if (responseData != null && responseData.indexOf("Service Error") < 0) {
                    $stepDialog.html(responseData);
                    updatePageContent();
                } else {
                    alert('Something went wrong. Please try again later. ', { title: 'Something went wrong' });
                };
            },
            error: function (xhr, ajaxOptions, error) {
                alert('Something went wrong. Please try again later. ', { title: 'Something went wrong' });
            }
        });
    }
    return true;
};

/*****************Welcome Journey - STEP THREE: sponsor content managed message (for social stuff)*****************/
var finishWelcomeJourney = function (stepNo) {
    $("#LoadingSpinner").dialog("open");
    var $stepDialog = $("#divWelcomeJourney");
    $stepDialog.html("");
    $.ajax({
        cache: false,
        contentType: "application/json; charset=utf-8",
        url: "FinishWelcomeJourney",
        data: { stepNumber: stepNo },
        dataType: "html",
        success: function (responseData) {
            if (responseData != null && responseData.indexOf("Service Error") < 0) {
                $stepDialog.dialog("close");
            } else {
                alert('Something went wrong. Please try again later. ', { title: 'Something went wrong' });
            };
            StartTicker();
        },
        error: function (xhr, ajaxOptions, error) {
            alert('Something went wrong. Please try again later. ', { title: 'Something went wrong' });
        }
    });
};

var saveWjStepThree = function (maxWjSteps) {
    $("#LoadingSpinner").dialog("open");
    var $stepDialog = $("#divWelcomeJourney");
    $stepDialog.html("");
    $.ajax({
        cache: false,
        contentType: "application/json; charset=utf-8",
        url: "SaveWJFThirdStep",
        data: { maxWJSteps: maxWjSteps },
        dataType: "html",
        success: function (responseData) {
            if (responseData != null && responseData.indexOf("Service Error") < 0) {
                $stepDialog.html(responseData);
            } else {
                alert('Something went wrong. Please try again later. ', { title: 'Something went wrong' });
            };
            updatePageContent();
        },
        error: function (xhr, ajaxOptions, error) {
            alert('Something went wrong. Please try again later. ', { title: 'Something went wrong' });
        }
    });
};

/*****************Welcome Journey - STEP FOUR: member privacy settings *****************/
var saveWjStepFour = function (maxWjSteps, currentStep) {
    $("#LoadingSpinner").dialog("open");
    var $stepDialog = $("#divWelcomeJourney");

    var bPrivacy = $("input:radio[name='MemberProfile.Privacy']:checked").val();
    var bAge = $("input:radio[name='MemberProfile.Member.ShowAge']:checked").val();
    var bGender = $("input:radio[name='MemberProfile.Member.ShowGender']:checked").val();
    var bTotStps = $("input:radio[name='MemberProfile.Member.ShowTotalSteps']:checked").val();
    var bAvrgSteps = $("input:radio[name='MemberProfile.Member.ShowStepsAvg']:checked").val();
    var bLastGzUpload = $("input:radio[name='MemberProfile.Member.ShowLastGZUpload']:checked").val();
    var bChallenges = $("input:radio[name='MemberProfile.Member.ShowChallengeHistory']:checked").val();
    if (typeof (bPrivacy) == 'undefined') {
        bPrivacy = false;
    }
    if (typeof (bAge) == 'undefined') {
        bAge = -1;
    }
    if (typeof (bGender) == 'undefined') {
        bGender = -1;
    }
    if (typeof (bTotStps) == 'undefined') {
        bTotStps = -1;
    }
    if (typeof (bLastGzUpload) == 'undefined') {
        bLastGzUpload = -1;
    }
    if (typeof (bAvrgSteps) == 'undefined') {
        bAvrgSteps = -1;
    }
    if (typeof (bTotStps) == 'undefined') {
        bTotStps = -1;
    }
    if (typeof (bChallenges) == 'undefined') {
        bChallenges = -1;
    }
    if (maxWjSteps == currentStep) { //this is last step so we need to saveprofile and finish WJ
        $.ajax({
            cache: false,
            contentType: "application/json; charset=utf-8",
            url: "FinishWelcomeJourneyFromStepFour",
            data: { stepNumber: currentStep, sAge: bAge, sGender: bGender, sTotalSt: bTotStps, sAvrgSteps: bAvrgSteps, sLastGzUpload: bLastGzUpload, sChallenge: bChallenges, privacy: bPrivacy },
            dataType: "html",
            success: function (responseData) {
                if (responseData != null && responseData.indexOf("Service Error") < 0) {
                    $stepDialog.html("");
                    updatePageContent();
                    StartTicker();
                    $stepDialog.dialog("close");
                } else {
                    alert('Something went wrong. Please try again later. ', { title: 'Something went wrong' });
                };
            },
            error: function (xhr, ajaxOptions, error) {
                alert('Something went wrong. Please try again later. ', { title: 'Something went wrong' });
            }
        });

    } else { //save this step and go to next one
        $.ajax({
            cache: false,
            contentType: "application/json; charset=utf-8",
            url: "SaveWJFourthStep",
            data: { maxWJStep: maxWjSteps, sAge: bAge, sGender: bGender, sTotalSt: bTotStps, sAvrgSteps: bAvrgSteps, sLastGzUpload: bLastGzUpload, sChallenge: bChallenges, privacy: bPrivacy },
            dataType: "html",
            success: function (responseData) {
                if (responseData != null && responseData.indexOf("Service Error") < 0) {
                    $stepDialog.html("");
                    $stepDialog.html(responseData);
                } else {
                    alert('Something went wrong. Please try again later. ', { title: 'Something went wrong' });
                }
                updatePageContent();
            },
            error: function (xhr, ajaxOptions, error) {
                alert('Something went wrong. Please try again later. ', { title: 'Something went wrong' });
            }
        });
    }
};
/*****************Welcome Journey - STEP FIVE: HRA info and available options *****************/
var saveWjFinalStep = function (stepNo, isHraMandatory) {
    $("#LoadingSpinner").dialog("open");
    var $stepDialog = $("#divWelcomeJourney");

    var bHraTakeMeThere = $("#hraMandatory").prop('checked');
    var bHraRemindLater = $("#hraOpt_remind").prop('checked');
    var bHraDontRemind = $("#hraOpt_dontRemind").prop('checked');
    //selectedHraOption: 0 hasn't selected, 1:Don't remind, -1:remind me later, 2 - take me there - no update needed
    var selectedHraOption = 0;

    if (bHraTakeMeThere) {
        selectedHraOption = 2;
    } else if (bHraRemindLater) {
        selectedHraOption = -1;
    } else if (bHraDontRemind) {
        selectedHraOption = 1;
    }

    if (!bHraDontRemind && !bHraRemindLater && !bHraTakeMeThere) { //of if (selectedHraOption == 0) {
        alert('Please select an option!');
    } else {
        //complete WelcomeJourney
        $.ajax({
            cache: false,
            contentType: "application/json; charset=utf-8",
            url: "FinishWelcomeJourneyStepFive",
            data: { stepNumber: stepNo, hraOption: selectedHraOption },
            dataType: "html",
            success: function (responseData) {

                StartTicker();
                if (responseData != null && responseData.indexOf("Service Error") < 0) {
                    $stepDialog.html("");
                    $stepDialog.dialog("close");

                    //we need to redirect to apropriate page
                    if (isHraMandatory || bHraTakeMeThere) {
                        var urlToRedirect = getRootUrl() + "secure/healthsnapshot/hrassessment.aspx";
                        window.location.href = urlToRedirect;
                    }
                } else {
                    alert('Something went wrong. Please try again later. ', { title: 'Something went wrong' });
                };

            },
            error: function (xhr, ajaxOptions, error) {
                alert('Something went wrong. Please try again later. ', { title: 'Something went wrong' });
            }
        });
    }
}
