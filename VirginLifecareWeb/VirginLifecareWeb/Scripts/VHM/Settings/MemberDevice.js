﻿var URL = $( "#MemberDeviceSettingsPath" ).val();

$(function () {
    $(".loadingWaitdiv").hide();
    $(".loadingWaitdiv").show();
    $(".settings-content").hide();
    GetMemberDeviceSettings();
    InitSaveMemberDeviceSettings();
    //set the onchange event for turning on/off the Clock screen - hide/show the Distance unit preference
    $('[name=caloriesDistance]').on('switch-change', function (e, data) {
        var value = data.value;
        if (value) {
            $("#distanceUnit").parent().fadeIn();
        } else {
            $("#distanceUnit").parent().fadeOut();
        }
    });
    $('[name=clockScreen]').on('switch-change', function (e, data) {
        var value = data.value;
        toggleClockScreenSelBool(value);
    });
    $('#distanceDisplay').bootstrapSwitch('setOnLabel', 'KM');
    $('#distanceDisplay').bootstrapSwitch('setOffLabel', 'M');
    $('#clock').bootstrapSwitch('setOnLabel', '24');
    $('#clock').bootstrapSwitch('setOffLabel', '12');
    $('#distanceDisplay').bootstrapSwitch('setOnClass', '');
    $('#distanceDisplay').bootstrapSwitch('setOffClass', '');
});

function GetMemberDeviceSettings() {
    $.ajax({
        url: URL + "GetMemberSettings",
        type: "GET",
        dataType: "json",
        cache: false,
        success: function (data) {
            if (data != null && data.MemberData != null && data.MemberData.SerialNumberWithDashes.length != "") {
                if (data.MemberData.SerialNumberWithDashes.substring(0, 2) === "03") {
                    AlterMemberSettings(data.EngagementMessagingOn, "messaging", data.EngagementMessagingOn);
                    AlterMemberSettings(data.BumpScreenOn, "bump", data.BumpScreenOn);

                    //set the clock screen selections and show()/Hide the settings depending on value
                    AlterMemberSettings(data.ClockScreenOn, "clockScreen", data.ClockScreenOn);
                    AlterMemberSettings(data.HrTimeFormat, "clock", data.HrTimeFormat);
                    if (data.ClockScreenOn) {
                        $("#divHrFormat").show();
                    } else {
                        $("#divHrFormat").hide();
                    }
                    //set the calories/distance screen selections and show()/Hide the settings depending on value
                    AlterMemberSettings(data.CalDistScreenOn, "caloriesDistance", data.CalDistScreenOn);
                    AlterMemberSettings(data.DistanceDisplayMetric, "distanceDisplay", data.DistanceDisplayMetric);
                    if (data.CalDistScreenOn) {
                        $("#distanceUnit").show();
                    } else {
                        $("#distanceUnit").hide();
                    }

                    AlterMemberSettings(data.SyncScreenOn, "sync", data.SyncScreenOn);
                    AlterMemberSettings(data.SunScreenOn, "sun", data.SunScreenOn);
                    $('input:text[name=nickname]').val(data.MemberData.NickName);
                    $(".loadingWaitdiv").hide();
                    $(".settings-content").show();
                    return 0;
                } else {
                    //don't allow acces to this page for users that don't have a GZ3(Max device) registered
                    //ignore sponsor module, if member has Max device, they should be able to access this page
                    document.location.href = "/v2/member/landingpage";
                }
            }
            $("#errorMessage p").text("Looks like you don't have gozone device");
            $(".loadingWaitdiv").hide();
            $("#errorMessage").show();
            return true;
        },
        complete: function (e, xhr) {
            if (e.status != 200) {
                $("#errorMessage").show();
                $(".loadingWaitdiv").hide();
            }
        }
    });
};

function AlterMemberSettings(settingsValue, name, value) {
    $("[name=" + name + "]").bootstrapSwitch('setState', value);
}

function toggleClockScreenSelBool(isOn) {
    if (isOn) {
        $("#divHrFormat").parent().fadeIn();
    } else {
        $("#divHrFormat").parent().fadeOut();
    }
}


function InitSaveMemberDeviceSettings() {
    $("#saveChanges").click(function () {


        var engagementMessaging = $("[name=messaging]").bootstrapSwitch('status');
        var turnBumpOn = $('[name=bump]').bootstrapSwitch('status');
        var hrTimeFormat = $('[name=clock]').bootstrapSwitch('status') ? 1 : 0;
        var distanceDisplayMetric = $('[name=distanceDisplay]').bootstrapSwitch('status');
        var syncScreenOn = $('[name=sync]').bootstrapSwitch('status');
        var calDistScreenOn = $('[name=caloriesDistance]').bootstrapSwitch('status');
        var sunScreenOn = $('[name=sun]').bootstrapSwitch('status');
        var clockScreenOn = $('[name=clockScreen]').bootstrapSwitch('status');
        var nickname = $('input:text[name=nickname]').val().trim(); //nickname
        
        var settingsRequest = {
            EngagementMessagingOn: engagementMessaging,
            BumpScreenOn: turnBumpOn,
            MidnightReset: true,
            HrTimeFormat: hrTimeFormat,
            DistanceDisplayMetric: distanceDisplayMetric,
            SyncScreenOn: syncScreenOn,
            CalDistScreenOn: calDistScreenOn,
            SunScreenOn: sunScreenOn,
            ClockScreenOn: clockScreenOn,
            Nickname: nickname
        };
        $.ajax({
            url: URL + "SaveMemberSettings",
            method: "PUT",
            data: { settingsRequest: JSON.stringify(settingsRequest) },
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            cache: false,
            success: function (data) {
                $("#myModal").modal("show");
            },
            complete: function (e, xhr) {
                if (e.status != 200) {
                    $("#errorMessage").show();
                    $(".loadingWaitdiv").hide();
                }
            }
        });
    });
}