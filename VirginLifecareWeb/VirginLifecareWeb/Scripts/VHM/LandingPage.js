﻿
var PAGE_CONTENT = PAGE_CONTENT || {},
    showNewsFeed = true;

$(document).ready(function () {

    if (badBrowser() != true) {
        $('.browser-alert').hide();
    } else {
        $('.browser-alert').show();
    }



    VHM.Social.NewsFeed.isMini = true;

    /*toggle left side sections and set cookie value*/
    $(".sectionTitle").click(function () {
        var newSectionDisplay,
            currentSection = $(this),
            displayIconSpan,
            colapsable;

        try {
            var cookie = getCookieValue("VHMUserSettings"),
                currentSectionId = currentSection.next(".sideNavSection").attr("id");


            if (!cookie) {
                //Create cookie
                var newCookie = {};
                newCookie.LandingPage = {};
                newCookie.LandingPage.SideNav = CreateSideNavSettings();
                cookie = newCookie;
            } else {
                cookie = JSON.parse(cookie);
                //Check if current cookie has sidenav settings
                if (!cookie.LandingPage) {
                    if (!cookie.LandingPage.SideNav) {
                        cookie.LandingPage.SideNav = CreateSideNavSettings();
                    }
                }
            }

            newSectionDisplay = !cookie.LandingPage.SideNav[currentSectionId];
            cookie.LandingPage.SideNav[currentSectionId] = newSectionDisplay;
            setCookie("VHMUserSettings", JSON.stringify(cookie));

            //set newsfeed specific variable (to account for ajax delay)
            if (currentSectionId === "newsfeedSection") {
                showNewsFeed = newSectionDisplay;
            }

            displayIconSpan = currentSection.find("span.sideNavStatusArrow");
            displayIconSpan.toggleClass("sideNavClosed");

        }
        catch (ex) {
            console.log("There was an error with your cookies, please try again.");
        }

        currentSection.next(".sideNavSection").toggle('fast');
    });

    //Prevent links from setting the sidenav open/closed state
    $(".sectionTitle > a").click(function (event) {
        event.stopPropagation();
    });

    LoadUpForGrabs();
    if ($("#newsfeedSection").html() != null) {
        LoadNewsFeed();
    }
    LoadRedirectPopUps();
    LoadContentAreas();
    LoadContentLabels();
    LoadGameDisplay();

    // if more than one ticker message is shown, remove the default message
    var tickerCount = $("#tickerContent li").length;
    if (tickerCount > 1) {
        $("#defaultTickerMessage").remove();
    }

    

    if (showWJ.toLowerCase() == 'true') {
        var welcJrneyDlg = $("#divWelcomeJourney");
        var $stepDialog = $("#wjSteps");
        welcJrneyDlg.dialog({
            title: wjTitle + " " + companyName + "!",
            autoOpen: true,
            width: 500,
            resizable: false,
            position: {
                my: "center center",
                offset: "0 -170"
            },
            open: function () {
                var closeTitle = $(".ui-dialog-titlebar-close");
                closeTitle.unbind('mouseenter mouseleave');
                closeTitle.hide();
                var urlToOpen = "WelcomeJourney";
                $stepDialog.load(urlToOpen, { 'stepNo': stepNo, 'maxStepNumber': maxWJSteps }, function (response, status, xhr) {

                    if (status != "success" || response.indexOf("Service Error") >= 0) {
                        alert('Sorry, something went wrong while trying to load the Welcome Journey. Please try again later.', { title: 'Something went wrong' });
                    } else { //content loaded successfully
                        if (stepNo == '1' || stepNo == '0') {
                            InitializeWJStepOne();
                        }
                    }

                    LoadContentLabels();
                    updatePageContent();
                    populateFormattedContent();
                });
            },
            close: function () { },
            modal: true
        });
    } else {
        //loadDialogs();
    }

    $("li.reward").hover(function () {
        $(this).find(".rewardTip").show();
    },
                function () {
                    $(this).find(".rewardTip").hide();
                });

    //  /*Calling RunKeeper service to get the data from Runkeeper in Asynchronous*/

    $.ajax({
        url: '/v2/Activity/SyncRunkeeperData',
        data: '',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function () { },
        error: function () { }
    });

    LoadSideNavDisplaySettings($("#activitySideNav"));
    LoadSideNavDisplaySettings($("#recentRewardsSideNav"));
    LoadSideNavDisplaySettings($("#measurementSideNav"));
    LoadSideNavDisplaySettings($("#programsSideNav"));

    $("#useNewLanding").click(function (e) {
        TrackGoogleAnalyticEvent('Wolverine', 'Click', 'New Landing Page');
        UseNewLanding(true);
        e.preventDefault();
    });
    $("#useOldLanding").click(function (e) {
        TrackGoogleAnalyticEvent('Wolverine', 'Click', 'Stay On Old Landing Page');
        UseNewLanding(false);
        e.preventDefault();
    });
    $( '#beta-landing-alert' ).click( function( e ) {
        $( "#newLandingModal" ).modal( "show" );
    } );

    if ($("#showNewLandingModal").val() == "True") {
        $("#newLandingModal").modal("show");
    }

});

function UseNewLanding(answer) {
    $.ajax({
        url: '../Member/UseNewLanding',
        type: 'post',
        data: { useNewLanding: answer },
        success: function () {
            if (answer) {
                location.reload();
            } else {
                $("#newLandingModal").modal("hide");
            }
        },
        error: function () {
            alert("Error");
        }
    });
};

function CreateSideNavSettings() {
    var sideNavSettings = {};
    $.each($(".sideNavSection"), function (key, value) {
        var sectionId = $(this).attr("id");
        if (sectionId) {
            sideNavSettings[sectionId] = true;
        }
    });    
    return sideNavSettings;
}

function LoadSideNavDisplaySettings(sideNavSection) {
    var cookie = getCookieValue("VHMUserSettings");
    if (!cookie) {
        //Create cookie
        var newCookie = {};
        newCookie.LandingPage = {};
        newCookie.LandingPage.SideNav = CreateSideNavSettings();
        setCookie("VHMUserSettings", JSON.stringify(newCookie));
        return false;
    } else {
        cookie = JSON.parse(cookie);
        //Check if current cookie has sidenav settings
        if (!cookie.LandingPage) {
            if (!cookie.LandingPage.SideNav) {
                cookie.LandingPage.SideNav = CreateSideNavSettings();
                return false;
            }
        } else {
            var sectionId = sideNavSection.attr("id");
            var displayValue = cookie.LandingPage.SideNav[sectionId];

            //account for newsfeed ajax delay
            if (sectionId === "newsfeedSection") {
                showNewsFeed = displayValue;
            }

            if (!displayValue) {
                sideNavSection.hide();
                var headerSection = sideNavSection.prev("h3.sectionTitle");            
                var arrowIcon = headerSection.find(".sideNavStatusArrow");
                arrowIcon.toggleClass("sideNavClosed");
            } else {
                sideNavSection.show();
            }
        }      
    }
}

function StartTicker() {
    $('.tickerContent ul  li').hide();
    RotateTicker(false, 1);
}

function LoadKnowAndGo() {
    var len = $("div.SECTaskItem").length;
    $("div.SECTaskItem").each(function () {

        var $this = $(this);
        var min = parseInt($this.attr("min"));
        var max = parseInt($this.attr("max"));
        var start = $this.attr("startdate");
        var score = $this.attr("score");
        var promoId = $this.attr("promoid");
        var freqType = $this.attr("frequency");
        var respType = $this.attr("response");
        var isYesNo = respType == "YesNo";
        var taskType = $this.attr("tasktype");
        var today = $this.attr("currentDate");

        var SEC_TASK_TYPE_WEIGHT = "WGT";
        if (!score || score.length == 0) {
            score = null;
        }
        var yesContent = $("#hdnKagResponseYes").val();
        var noContent = $("#hdnKagResponseNo").val();
        
        LoadTrackerDate($this, promoId, today);
    });
    LoadSideNavDisplaySettings($("#challengesSideNav"));
}

function loadDialogs() {
    // this popup is 'non-standard' and required jqModal
    $("div.recentRewardsPopup").jqm({
        modal: false,
        closeOnEsc: true,
        onShow: function (o) {

            var modal = $(o.w);
            modal.show("blind", { direction: "horizontal" }, 500);
            modal.unbind("keydown");

            if (o.c.closeOnEsc) {
                modal.attr("tabindex", 0);
                modal.bind("keydown", function (event) {
                    if (event.keyCode == 27) {
                        event.preventDefault();
                        modal.jqmHide();
                    }
                });
                modal.focus();
            }
        }
    });

    //show popup rewards if applicable
    if ($(".recentRewardsPopupRow").length > 0) {
        $("div.recentRewardsPopup").jqmShow(); // uses jqModel because it is 'non-standard'
    }
    
    $("#KAGCompletePopup").dialog({
        modal: false,
        autoOpen: false,
        resizable: false,
        closeOnEsc: true,
        height: 400
    });
}

function LoadUpForGrabs() {
    $.ajax({
        url: 'UpForGrabs',
        cache: false,
        success: function (view) {
            $("#upForGrabsListContainer").hide().html(view);
            if ($("#upForGrabsList li").length > 0) {
                $("#upForGrabsListContainer").fadeIn('normal');
            } else {
                $("#upForGrabsListContainer").parent().hide();
                $( "hr#vouncherDivider" ).hide();
            }
        },
        error: function () {
            LoadError("upForGrabsListContainer");
        }
    });
}
function LoadContentAreas() {

//    $.ajax({
//        url: '../social/ContentArea',
//        cache: false,
//        data: {contentAreaId: "f89fd910-7b85-49f0-b64d-f57ac85df840"},
//        success: function (view) {
//            $("#bannerContent").hide().html(view).fadeIn('normal');
//        },
//        error: function () {
//            LoadError("bannerContent");
//        }
//    });

//    $.ajax({
//        url: '../social/ContentArea',
//        cache: false,
//        data: { contentAreaId: "cd51f4df-02d9-43a6-90b5-a158706d6f0b" },
//        success: function (view) {
//            $("#vhmContent").hide().html(view).fadeIn('normal');
//        },
//        error: function () {
//            LoadError("vhmContent");
//        }
//    });

    $.ajax({
        url: '../social/SponsorContent',
        cache: false,       
        success: function (view) {
            $("#sponsorContent").hide().html(view).fadeIn('normal');
        },
        error: function () {
            LoadError("sponsorContent");
        }
    });

    if ($('#lap-display').length === 0) {
        $.ajax( {
            url: '../social/ContentArea',
            cache: false,
            data: { contentAreaId: "0797af26-fd1e-45a1-ac15-d805dc5f803f" },
            success: function (view) {
                if (view.indexOf('Congratulations') >= 0) {
                    $( "#twoBytwo" ).hide().html( view ).fadeIn( 'normal' );
                }
                
            },
            error: function() {
                LoadError( "twoBytwo" );
            }
        });
    }
}

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function LoadGameDisplay() {

    var params = getUrlVars(); // hm can be used as a testing parameter to test various displays of hm values
    var testing = params.hm ? '?hm=' + params.hm : '';

    $.ajax({
        url: '../member/GameDisplay' + testing,
        cache: false,
        success: function (view) {
            $("#GameDisplay").hide().html(view).fadeIn('normal');

            if ($("img.level5").html() != null) {   // manual positioning of the summary table for levels only
                updatePageContent();
                populateGameContent();
            }
            var onTrackAmt = $("#OnTrackToEarnAmount").val();
            if ( onTrackAmt > 0 ) {
                $( 'img#runningMan' ).popover();
            }
        },
        error: function () {
            LoadError("GameDisplay");
        }
    });
    
    if ( $('#lap-display').length > 0) {
            $.ajax({
                url: '../member/LapsDisplay',
                cache: false,
                success: function (view) {
                    $("#lap-display").html(view).fadeIn('normal');
                },
                error: function () {
                    LoadError("GameDisplay");
                }
            });
    }

	// Handle invite friends & family modal logic
	if ($('#friends-family').length) {
		initFriendsAndFamilyInviteDialog();
	}
}

// This function initializes the "Invite x friends" alert message and associated dialog to invite Friends & Family to Ignite
function initFriendsAndFamilyInviteDialog() {
	"use strict";
	
	$('#friends-family').on('show', function () {
		setTimeout(function () {
			// Focus on first error, otherwise first field
			$($('#friends-family .error input')[0] || $('#friends-family input')[0]).focus();
		}, 500);
	});

	// Hook <Enter> key within input field to submit form
	$("#friends-family input").keypress(function (event) {
		if (event.which == 13) {
			event.preventDefault();
			$(this).closest('form').submit();
		}
	});
	
	// Keep track of email addresses previously used
	var sentAddresses = {};
	
	// Define a helper function to display validation errors
	var hasValidationError;
	function showValidationError(field, msg) {
		$(field).tooltip({ title: msg, placement: 'top', trigger: 'manual', html: true }).tooltip('show').select().focus();
		$(field).closest('.control-group').addClass('error');
		hasValidationError = true;
	}

	// Handle form submit
	$('#friends-family form').submit(function (e) {
		e.preventDefault();
		var inputs$ = $('#friends-family .textInput');

		// Clear prior validation errors (if any)
		inputs$.tooltip('destroy');
		$('#friends-family .control-group').removeClass('error');
		hasValidationError = false;

		if (inputs$.length == 0) {
			$('#friends-family').modal('hide');
			return false;
		}

		// Validate email address fields
		var validInputs = [];
		var validAddresses = [];
		var pendingAddresses = {};
		for (var i = 0; i < inputs$.length; i++) {
			var input$ = $(inputs$[i]);
			var address = $.trim(input$.val());
			if (address) {
				if (!VHM.Core.Validation.CheckEmail(address)) {
					showValidationError(input$, 'Please enter a valid email address.');
					break;
				} else if (sentAddresses[address.toLowerCase()]) {
					showValidationError(input$, 'You have already sent an invite to this email address.');
					break;
				} else if (pendingAddresses[address.toLowerCase()]) {
					showValidationError(input$, 'You have used this email address more than once.');
					break;
				} else {
					validInputs.push(input$);
					validAddresses.push(address);
					pendingAddresses[address] = true;
				}
			}
		}

		// Send emails if there are no validation errors
		if (!hasValidationError) {
			if (validInputs.length) {
				// TODO:  actually send emails here using validAddresses array

				for (var i = 0; i < validInputs.length; i++) {
					sentAddresses[validAddresses[i]] = true;
					validInputs[i].closest('.control-group').replaceWith('<div class="alert alert-success"><b>Well done!</b> Your email was successfully sent to ' + validAddresses[i].htmlEncode() + '</div>');
				}

				var fieldsLeft = inputs$.length - validInputs.length;
				if (fieldsLeft == 0) {
					$('#friends-family .btn-main').html('Close');
					$('#friends-family .countdown').css('visibility', 'hidden');
					$('#friends-family-alert').fadeOut();
				} else {
					$('#friends-family .countdown span').html((fieldsLeft == 1) ? '1 more friend' : (fieldsLeft + ' more friends'));
					$('#friends-family-alert .countdown').html((fieldsLeft == 1) ? '1 friend' : (fieldsLeft + ' friends'));
					$('#friends-family .textInput:first').focus();
				}
			} else {
				showValidationError(inputs$[0], 'Please enter an email address.');
			}
		}

		return false;
	});
}

function LoadError(containerId) {
    $.ajax( {
        url: '../member/LandingPageSectionError',
        success: function( view ) {
            $("#" + containerId).hide().html(view).fadeIn('normal');
        }
    } );
}
function LoadNewsFeed() {
    $.ajax({
        url: '../social/NewsFeed',
        cache: false,
        success: function (view) {
            if (showNewsFeed) {
                $("#newsfeedSection").hide().html(view).fadeIn('normal');
                LoadNewsFeedFunctions();
                LoadSideNavDisplaySettings($("#newsfeedSection"));
            } else {
                $("#newsfeedSection").hide().html(view);
                LoadNewsFeedFunctions();
                LoadSideNavDisplaySettings($("#newsfeedSection" ));
            }
        }
    });
}

function LoadNewsFeedFunctions() {
    $("#newPostText").keyup(function () {
        var hasText = $(this).val().length > 0;
        $("input#submitPost").removeClass("disabled");
        $(this).attr("hasText", hasText ? "true" : "false");
    });

    $("#newPostText").focus(function () {
        var hasText = $(this).attr("hasText");

        $(this).removeClass("disabled").addClass("active")
            .val(hasText == "true" ? $(this).val() : "");
    });

    $("#newPostText").blur(function () {
        var hasText = $(this).attr("hasText");
        if (hasText == "true") {
            $("input#submitPost").removeClass("disabled");
        } else {
            $("input#submitPost").addClass("disabled");
        }
        $(this).removeClass("active")
            .addClass("disabled")
            .val(hasText == "true" ? $(this).val() : $(this).attr("message"));
    });

    $("#submitPost").click(function () {
        if ($(this).hasClass("disabled")) {
            return false;
        }
        var text = $("#newPostText").val().StripOutHTMLTags();
        var defaultText = $("#newPostText").attr("message");
        appendNewPost(text);
        $("#newPostText").val(defaultText)
            .attr("hasText", "false");

        $("input#submitPost").addClass("disabled");
        return true;
    });

    $("a.profileLink").click(function () {

        var friendId = $(this).attr("friendId");
        var memberId = $(this).attr("memberId");

        var data = { friendId: friendId };
        $.ajax({
            url: '../Social/UserProfile',
            data: data,
            success: function (view) {

                $(view).dialog({
                    title: false,
                    autoOpen: true,
                    width: 520,
                    resizable: false,
                    open: function () {
                        $(".ui-dialog-titlebar").css("height", "0px");
                    },
                    close: function () {
                        $("div#divUserProfile").remove();
                    },
                    modal: true
                });

                $("#ahrfRemovefr").click(function () {
                    $("div#divRemoveFriend").dialog({
                        modal: true,
                        width: 300,
                        position: ['center', 50],
                        zIndex: 4000,
                        title: "Remove as Friend",
                        resizable: false
                    });
                    return false;
                });

                $("#btnAddAsFriend").click(function () {
                    $(this).unbind("click");
                    AddFriend(friendId);
                });
                $("#btnRemove").click(function () {
                    $(this).unbind("click");
                    RemoveFriend(friendId);
                });
                $("#btnChallengeMe").click(function () {
                    $(this).unbind("click");
                    window.location.href = '/Secure/Challenge/create.aspx?mid=' + memberId + '&challengeid=';
                });

            }
        });

        return false;
    });

    $("a.likeLink").click(function () {

        var $this = $(this);
        var postId = $this.attr('postId');
        var youLikedIt = $this.attr('youLikedIt');
        var youLikedId = $this.attr('youLikedId');
        var postById = $this.attr('postById');
        var groupId = $this.attr('groupId');

        if (youLikedIt && youLikedIt.toLowerCase() == "true") {
            UnLikePost(youLikedId, postId, groupId);
        } else {
            LikePost(postId, postById, groupId);
        }

        return false;
    });
}

function AddFriend(friendId) {
    var params = { friendId: friendId };
    var sParams = JSON.stringify(params);
    $.ajax({
        cache: false,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/WebServices/WSVHMSocial.asmx/AddFriend',
        data: sParams,
        dataType: "json",
        success: function () {
            $("#btnAddAsFriend").hide();
            $("#friendReqSentMsg").show();
        }
    });
}

function RemoveFriend(fId) {
    var params = { exFriend: fId };
    var sParams = JSON.stringify(params);
    $.ajax({
        cache: false,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/WebServices/WSVHMSocial.asmx/SaveRemoveFriend",
        data: sParams,
        dataType: "json",
        success: function () {
            window.location.reload();
        },
        error: function () { }
    });
}

function LikePost(postId, postById, groupId) {

    var data = { postId: postId, postById: postById, groupId: groupId };
    $.ajax({
        data: data,
        url: '/v2/Social/LikePost',
        success: function () {
            LoadNewsFeed();
        },
        error: function () {

        }
    });
}

function UnLikePost(youLikedId, activityEntityId, groupId) {

    var data = { youLikedId: youLikedId, activityEntityId: activityEntityId, groupId: groupId };
    $.ajax({
        data: data,
        url: '/WebServices/WSVHMSocial.asmx/UnlikePost',
        success: function () {
            LoadNewsFeed();
        },
        error: function () {

        }
    });
}

function appendNewPost(text) {

    submitPost(text);

}

function submitPost(messageText) {
    //check for null/empty message - do not allow
    //already message was StripOutHTMLTags
    if (messageText != "" && $.trim(messageText).length >= 3) {
        var url = "../social/SubmitPost"; // '@Url.Action("SubmitPost","Social")'; //'/social/SubmitPost';
        try {
            var data = { messageText: messageText, isGroupPost: false, groupId: 0 };
            var sData = JSON.stringify(data);
            $.ajax({
                cache: false,
                contentType: "application/json; charset=utf-8",
                data: sData,
                dataType: 'json',
                type: 'POST',
                url: url,
                success: function (responseData) {
                    if (responseData != null && responseData.indexOf("Service Error") < 0) {
                        var result = $.parseJSON(responseData);
                        if (result.Success) {
                            LoadNewsFeed();
                        }
                    }
                },
                error: function (xhr, ajaxOptions, error) {
                    alert( "error" );
                }
            });
        } catch (ex) {
            console.log(ex);
            alert("There was a problem saving your post");
        }
    }    
}

function GetDateFromStringWithoutTimezone(dateString) {
    if (dateString == null || dateString == "")
        return null;
    var matches = dateString.match(/^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})\.*\d*([+|-])(\d{2}):(\d{2})$/);
    var date = new Date(matches[1], matches[2] - 1, matches[3], matches[4], matches[5], matches[6]);
    return date;
}

function loadSECTracker(secTaskItem, oSEC, dt) {
    
    var RESPONSE_TYPE_YESNO = 2;
    var SEC_TASK_TYPE_WEIGHT = 'WGT';
    var FREQ_TYPE_WEEK = 3;
    var FREQ_TYPE_ONCE = 1;
    var yesContent = $("#hdnKagResponseYes").val();
    var noContent = $("#hdnKagResponseNo").val();
    var isYesNo = oSEC.ResponseType == RESPONSE_TYPE_YESNO;
    var minDate = oSEC.Tracker[0].TrackerDate;
    var minVal = oSEC.MinValue;
    var maxVal = oSEC.MaxValue;
    
   
    var paramDate = new Date(dt);

    VHM.SelfEntryChallenge.EditableDays = Math.min(oSEC.DisplayDays, oSEC.Tracker.length);
    var today = new Date();
    var endOfTrackerWeek = new Date();
    
    for (var j = 0; j < oSEC.Tracker.length; j++) {
        
        var trackerDate = new Date(oSEC.Tracker[j].TrackerDate);
        var fourteenDaysAfterTrackerDate = new Date(trackerDate);
        fourteenDaysAfterTrackerDate.setDate(fourteenDaysAfterTrackerDate.getDate() + VHM.SelfEntryChallenge.EditableDays);
        
        endOfTrackerWeek.setDate(trackerDate.getDate() + 6);

        
        var fourteenDaysAfterTrackerWeek = new Date(endOfTrackerWeek);
        fourteenDaysAfterTrackerWeek.setDate(endOfTrackerWeek.getDate() + VHM.SelfEntryChallenge.EditableDays);
        
        // today can be no more than (EditableDays) days after tracker date
        var allowInput = oSEC.FrequencyType == FREQ_TYPE_WEEK
            ? today <= fourteenDaysAfterTrackerWeek
            : today <= fourteenDaysAfterTrackerDate;

        // for frequency == Once or Daily, display the tracker for today, 
        // for frequency == Weekly, display if today falls within the current week
        var display = oSEC.FrequencyType == FREQ_TYPE_WEEK
            ? paramDate >= trackerDate && paramDate <= endOfTrackerWeek
            : paramDate.toString() == trackerDate.toString();
        
        if (display) {
            
            $(secTaskItem).html("").ActivityTracker(
                    {
                        itemID: oSEC.SECId,
                        allowInput: allowInput,
                        showTracker: true,
                        showDate: oSEC.FrequencyType != FREQ_TYPE_ONCE && oSEC.FrequencyType != FREQ_TYPE_WEEK,
                        gridSize: 20,
                        startDate: oSEC.Tracker[j].TrackerDate,
                        minDate: minDate,
                        isYesNo: isYesNo,
                        showYesNoLabels: isYesNo,
                        initialValue: oSEC.Tracker[j].TrackerValue,
                        baseCSSClass: isYesNo ? 'ActivityTrackerYesNo' : 'ActivityTracker',
                        minValue: minVal,
                        maxValue: maxVal,
                        valueTranslateFunction: oSEC.TaskType == SEC_TASK_TYPE_WEIGHT
                            ? function (kg) { return weightTranslate(kg); }
                            : function (val) { return val; },
                        onSave: function (tracker) {
                            SaveTrackedDay(tracker);
                        },
                        onDateChange: function (id, dt) {
                            LoadTrackerDate(secTaskItem, id, dt);
                        },
                        usePositionForTracker: true,
                        YesContent: yesContent,
                        NoContent: noContent,
                        minValueLabel: yesContent,
                        maxValueLabel: noContent,
                        noEditText: 'Sorry, you can only track the last ' + VHM.SelfEntryChallenge.EditableDays + ' days',
                        useAltYesNoInput: isYesNo // landing page needs to use alternate yes/no input
                    });
            break;
        }
    }
}

function LoadTrackerDate(tracker, secId, dt) {
    try {

        var data = { secId: secId, dt: dt };
        var sData = JSON.stringify(data);
        $.ajax({
            url: '/secure/WebServices/Challenge.asmx/GetTrackedDay',
            contentType: "application/json; charset=utf-8",
            data: sData,
            type: 'POST',
            dataType: 'json',
            success: function (sec) {
                var oSEC = JSON.parse(sec.d);
                loadSECTracker(tracker, oSEC, dt);
            }
        });
    } catch (ex) {
        console.log(ex);
        alert("There was a problem saving your data");
    }
}

function SaveTrackedDay(tracker) {

    var url = '/secure/WebServices/Challenge.asmx/SaveTrackedDay';

    try {
        var secId = tracker.ItemID;
        var val = tracker.TrackerValue;
        var dt = tracker.TrackerDate;
        var startingToTrackMsg = $("#hdnStartingToTrackMsg").val();
        var halfWayThereMsg = $("#hdnHalfWayThereMsg").val();
        var justEarnedMsg = $("#hdnJustEarnedMsg").val();

        var data = { secId: secId, val: val, dt: dt, startingToTrackMessage: startingToTrackMsg, halfWayMessage: halfWayThereMsg, justEarnedMessage: justEarnedMsg };
   
        var sData = JSON.stringify(data);
       
        $.ajax({
            url: url,
            contentType: "application/json; charset=utf-8",
            data: sData,
            cache: false,
            type: 'POST',
            dataType: 'json',
            success: function (sec) {
                var oSEC = JSON.parse(sec.d);
                var inputSecComplete = null;

                $("input.secCompleted").each(function () {
                    if ($(this).attr("secId") == oSEC.SECId && inputSecComplete == null) {
                        inputSecComplete = this;
                    }
                });
                if (oSEC.IsComplete == true && inputSecComplete != null && $(inputSecComplete).val().toLowerCase() == "false") {
                    ShowKAGCompleted(sec);
                    $(inputSecComplete).val(true);
                }
            }
        });
    } catch (ex) {
        console.log(ex);
        alert("There was a problem saving your results");
    }
}

function ShowKAGCompleted(sec) {
    var url = '/V2/Member/KAGComplete';
    var oKAG = JSON.parse(sec.d);
    var data = { KAGTitle: oKAG.Title,
        RewardImage: oKAG.CompletedRewardImage,
        BadgeImage: oKAG.CompletedBadge,
        RewardAmount: oKAG.RewardTotal,
        ShowOthers: oKAG.Stats.ShowOthers,
        ShowAverage: oKAG.Stats.ShowAverage,
        ShowDaysTracked: oKAG.Stats.ShowDaysTracked,
        ShowTargetDays: oKAG.Stats.ShowTargetDays,
        ShowStretchDays: oKAG.Stats.ShowStretchDays,
        ShowYesDays: false,
        ShowNoDays: false,
        TrackingTimeSpan: oKAG.FrequencyType,
        TotalTrackableDays: oKAG.Stats.TotalTrackableDays,
        PersonalDaysTracked: oKAG.Stats.PersonalDaysTracked,
        ChallengeDaysTracked: oKAG.Stats.ChallengeDaysTracked,
        PersonalTargetDays: oKAG.Stats.PersonalTargetDays,
        ChallengeTargetDays: oKAG.Stats.ChallengeTargetDays,
        PersonalStretchDays: oKAG.Stats.PersonalStretchDays,
        ChallengeStretchDays: oKAG.Stats.ChallengeStretchDays,
        PersonalYesDays: oKAG.Stats.PersonalYesDays,
        ChallengeYesDays: oKAG.Stats.ChallengeYesDays,
        PersonalNoDays: oKAG.Stats.PersonalNoDays,
        ChallengeNoDays: oKAG.Stats.ChallengeNoDays,
        PersonalAverage: oKAG.Stats.PersonalAverage,
        ChallengeAverage: oKAG.Stats.ChallengeAverage,
        IsCashReward: oKAG.IsCashReward,
        ResponseType: oKAG.ResponseType,
        TaskType: oKAG.TaskType
    };

    var kagThanksMessage = $( "#hdnThanksForParticpating" ).val();
    jQuery( '#KAGCompletePopup' )
        .html( '' )
        .load( url, data )
        .dialog(
            {
                title: kagThanksMessage + " " + oKAG.Title,
                autoOpen: true,
                width: 400,
                resizable: false,
                modal: true
            } );
}

function LoadRedirectPopUps() {

    if (showPopUpForMemebershipStatusChanged.toLowerCase() == 'true') {

        $("#memebershipStatusChanged").dialog(
        { title: 'Important Account Information',
            width: '400px',
            modal: true,
            resizable: false,
            closeOnEscape: false,
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close", ui.dialog || ui).hide();
            }
        });
    }
    if (showPopUpForCreditCardInfo.toLowerCase() == 'true') {
        $("#creditCardInfo").dialog(
        { title: ' Credit Card Declined',
            width: '400px',
            modal: true,
            resizable: false,
            closeOnEscape: false,
            open: function (event, ui) {
                var closeTitle = $(".ui-dialog-titlebar-close");
                closeTitle.unbind('mouseenter mouseleave');
                closeTitle.hide();
                $(".ui-dialog-titlebar-close", ui.dialog || ui).hide();
            }
        });
    }
    if (showPopUpForBillingInfo.toLowerCase() == 'true') {
        $("#billingInfo").dialog({
            title: 'Important Account Information',
            width: '400px',
            modal: true,
            resizable: false,
            closeOnEscape: false,
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            }
        });
    }
    if (showPopUpForTermsPage.toLowerCase() == 'true' || showPopUpForTermsToAcknowledge.toLowerCase() == 'true') {
        $("#termsPage").dialog({
            title: 'Important Account Information',
            width: '400px',
            modal: true,
            resizable: false, closeOnEscape: false,
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close", ui.dialog || ui).hide();
            }
        });

    }

}
/**************************************************************************************
Redeem a voucher
***************************************************************************************/
function openRedeemVoucher() {
    var redeemDialog = $("#divRedeemAVoucher");
    redeemDialog.dialog({
        title: false,
        autoOpen: true,
        width: 500,
        resizable: false,
        open: function () {
            $(".ui-dialog-titlebar").css("height", "0px");
            var urlToOpen = "ShowRedeemVoucher";
            $(this).load(urlToOpen, {}, function (response, status) {
                if (status != "success" || response.indexOf("Service Error") >= 0) {
                    alert('Sorry, something went wrong. Please try again later.', { title: 'Something went wrong' });
                }else {
                    updatePageContent();
                }
            });
        },
        close: function () { },
        modal: true
    });
}
function CheckRedeemField(event, input, maxChars, index) {
    if (event.which || event.keyCode) {
        if (event.which == 13 || event.keyCode == 13 ||
            event.which == 46 || event.keyCode == 46 ||
            event.which == 8 || event.keyCode == 8) {
            return false;
        }
    }

    var length = input.value.length;

    if (length >= maxChars) {
        switch (index) {
            case 1:
                $('#inpSecondRedeem').focus();
                break;
            case 2:
                $('#inpThirdRedeem').focus();
                break;
            case 3:
                $("div.VHMvoucherRedeemPopup .VHMpopup .VHMSubmitRedeemCode").focus();
                break;
        }

        if (length > maxChars) {
            input.value = input.value.substring(0, maxChars);
        }
    }
}

function OpenCloseRedeem(option) {
    var redeemDialog = $("#divRedeemAVoucher");
    if (option) {
        redeemDialog.dialog('open');
    } else {
        redeemDialog.dialog('close');
    }
}

function redeemVoucher(hdnErrText, btnObj) {
    var codeFirst = $("#inpFirstRedeem").val();
    var alertMessText = $( '#' + hdnErrText ).val();
    if (codeFirst.length > 0) {
        //onclick - disable button
        $(btnObj).attr('disabled', 'disabled');
        // inpFirstRedeem, inpSecondRedeem, inpThirdRedeem - parts of the Redeem code
        var message = $("#errorMessage");
        message.hide();

        var redeemCode = '';
        if (codeFirst.length == 4 && $("#inpSecondRedeem").val().length == 4 && $("#inpThirdRedeem").val().length == 2) {
            redeemCode = codeFirst + '-' + $("#inpSecondRedeem").val() + '-' + $("#inpThirdRedeem").val();
        }
        else {
            $(btnObj).removeAttr('disabled');
            alert(alertMessText);
            return;
        }

        var param = { redeemCode: redeemCode };
        var sParam = JSON.stringify(param);
        $.ajax({
            cache: false,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "RedeemVoucher",
            data: sParam,
            dataType: "json",
            success: function (responseData) {
                
                if (responseData != null && responseData.indexOf("Service Error") < 0) {
                    var result = $.parseJSON(responseData);
                    var suportUrl = getRootUrl();
                    if (!result.Success) {
                        message.html(result.ErrorMessage + ' <a target="_self" href="' + suportUrl + 'secure/help/contactusform.aspx">' + result.ErrorLinkText + '.</a>');
                        message.show();
                        document.getElementById('inpFirstRedeem').value = document.getElementById('inpSecondRedeem').value = document.getElementById('inpThirdRedeem').value = "";
                        codeFirst = "";
                        redeemCode = "";
                        $(btnObj).removeAttr('disabled');
                    } else {
                        OpenCloseRedeem(false);
                    }
                } else {
                    alert('Something went wrong. Please try again later. ', { title: 'Something went wrong' });
                };
            },
            error: function () {
                alert('Something went wrong. Please try again later. ', { title: 'Something went wrong' });
            }
        });
    } else {
        alert( alertMessText );
    }
}

function getRootUrl() {
    var rootUrl = window.location.host.toLowerCase();
    if (rootUrl.indexOf('http://') == -1 && rootUrl.indexOf('https://') == -1) {
        rootUrl = 'https://' + rootUrl;
    }
    if (rootUrl.indexOf('/v2') >= 0) {
        rootUrl = rootUrl.replace("/v2", "");
    }
    if (rootUrl.lastIndexOf('/') != (rootUrl.length - 1)) {
        rootUrl += '/';
    }
    return rootUrl;
}
var intervalIndex;
var temp;
function RotateTicker(isPaused, increment) {
    var pause = 5000; // in milliseconds) 
    var length = $("#tickerContent li").length;

    this.getLi = function () {

        var liIndex;
        if (temp + increment == 0 && increment == -1) {//if the increment is -1 countdown the li index 
            liIndex = length;
        } else if (temp == -1 || temp == null || (temp + increment == 0 && increment == 1) || (temp == length && increment == 1)) {
            liIndex = 1;
        } else {
            liIndex = temp + increment;
        }
        return liIndex;
    };
    this.show = function () {
        var currentLi = getLi();
        temp = currentLi;
        $("#tickerContent li").fadeOut("Slow"); 
        $("#tickerContent li:nth-child(" + currentLi + ")").fadeIn(1000);


    };

    show();

    if (isPaused == true || length == 1) {

        clearInterval(intervalIndex);
    } else {
        intervalIndex = setInterval(show, pause);
    };
}

function ManualRotation(increment) {
    RotateTicker(true, increment);

}
/**************************************************************************************
Game Display = PRG - Progress bar
***************************************************************************************/
function ShowProgressBarInfo(currentHm, milestoneThreshold, milestoneReward, holderId, taskObj, cndImagePath) {
    
    var holder = $("#" + holderId);
    holder.html( "" );
    try {
        //use offset
        var offset = $( taskObj ).offset();
        var top = offset.top;
        var left = offset.left - 25; //  - 48;
        var leftToGo = milestoneThreshold - currentHm;

        var strInfo = "<div>";
        if (leftToGo > 0) {
            strInfo += "You have <strong>" + leftToGo + "</strong>&nbsp;<img alt='HealthMiles' src='/v2/Content/Images/Shared/hm_11.gif' class=\"iconHM\" />&nbsp;to go<br />";
        }
        if (milestoneReward != "") {
            strInfo += "The reward for this goal is <span class=\"VHMMultipleTargetReward\">" + milestoneReward + "</span>";
        }
        strInfo += "</div>";
        strInfo += "<div class=\"bottomBubbleArrow\"><img class=\"imgBottomArrowPointer\" src=\"" + cndImagePath + "bubble-tail2.png\" /><div>";
        holder.html( strInfo );
        holder.css( { left: left + 'px', top: top + 'px' } );
        holder.show();
    } catch (ex) { console.log(ex); }
};

function HideProgressBarInfo(holderId) {
    var holder = $("#prgBubbleInfo");
    holder.html("");
    holder.hide();
};

/**************************************************************************************
UP FOR GRABS
***************************************************************************************/

function ShowUpForGrabInfo(taskObj, triggerObj, rewardTypeImageUrl, cndImagePath, cdnBadgeImagePath) {
    var bubbleHodler = $("#UFGBubbleInfo");
    bubbleHodler.html("");
    var offset = $(triggerObj).offset();
    var top = offset.top - 20;
    var left = offset.left;
    var upForGrabItem = taskObj;
    var strInfo = "";
    var rewardsCount = upForGrabItem.Rewards.length;
    var taskComment = "";
    if (upForGrabItem.UFGTaskComment != null && upForGrabItem.UFGTaskComment != "") {
        taskComment = upForGrabItem.UFGTaskComment;
    } else {
        taskComment = upForGrabItem.UFGTaskName;
    }
    if (rewardsCount > 1) {
        strInfo = "<div style=\"float:left; width: 50%;\"><h4 class=\"hoverSub lightGrey\">" + taskComment + "</h4></div>";
        strInfo += "<div style=\"float:right; width: 45%; padding:5px;\">";
        //add image for each reward
        for (var i = 0; i < rewardsCount; i++) {
            var item = upForGrabItem.Rewards[i];
            var cashClass = item.IsCash ? "cash" : "";
            var rewardImageSrc = "";
            if (item.BadgeId != null && item.BadgeId > 0) {
                rewardImageSrc = cdnBadgeImagePath + item.RewardImage + ".png";
            } else {
                rewardImageSrc = rewardTypeImageUrl + item.RewardImage + "_on.gif";
            }
            strInfo += "<img alt=\"reward\" class=\"ufgReward\" style=\"margin-left:10px;z-index: 102; width:50px; height:50px!important;\" src=\"" + rewardImageSrc + "\" />";
            if (item.Amount != null && item.Amount > 0) {
                //@(reward.IsCash ? reward.Amount.ToString("C0") : reward.Amount.ToString())
                if (item.IsCash) {
                    strInfo += "<label class=\"amountLabel " + cashClass + "\" >" + "$" + item.Amount + "</label>";
                } else {
                    strInfo += "<label class=\"amountLabel " + cashClass + "\" >" + item.Amount + "</label>";
                }
            }
            //if this is not last index, add the + sign
            if ((i + 1) != rewardsCount) {
                strInfo += "<span style=\"margin-top:20px;position:absolute;font-size:1.5em;\">+</span>";
            }
        }
        strInfo += "</div>";
    } else {
        strInfo = "<div style=\"float:left; width: 70%;\"><h4 class=\"hoverSub lightGrey\">" + taskComment + "</h4></div>";
        strInfo += "<div style=\"float:right; width: 20%; padding:5px; \">";
        var sigleItem = upForGrabItem.Rewards[0];
        var cashCls = sigleItem.IsCash ? "cash" : "";
        strInfo += "<img alt=\"reward\" class=\"ufgReward\" style=\"z-index: 102; height:50px;\" src=\"" + rewardTypeImageUrl + sigleItem.RewardImage + "_on.gif\" />";
        if (sigleItem.Amount > 0) {
            if (sigleItem.IsCash) {
                strInfo += "<label class=\"amountLabel " + cashCls + "\" >" + "$" + sigleItem.Amount + "</label>";
            } else {
                strInfo += "<label class=\"amountLabel " + cashCls + "\" >" + sigleItem.Amount + "</label>";
            }
        }
        strInfo += "</div>";
    }
    strInfo += "<div class=\"bottomBubbleArrow\"><img class=\"imgBottomArrowPointer\" src=\"" + cndImagePath + "bubble-tail2.png\" /><div>";
    bubbleHodler.html(strInfo);
    bubbleHodler.css({ left: left + 'px', top: top + 'px', width: ' 280px' });

    bubbleHodler.show();
}

function HideUpForGrabInfo() {
    var holder = $("#UFGBubbleInfo");
    holder.html("");
    holder.hide();
};

function ShowHoverRewardSummaryInfo(triggerObj, stringText, cndImagePath) {
    var bubbleHodler = $("#UFGBubbleInfo");
    bubbleHodler.html("");
    
    var offset = $(triggerObj).offset();
    var top = offset.top + 15;
    var left = offset.left - 30;
    //<h4 class=\"hoverSub lightGrey\">
    var strInfo = "<h4 class=\"hoverSub lightGrey\" >" + stringText + "</h4>";
    strInfo += "<div class=\"bottomBubbleArrow\"><img class=\"imgBottomArrowPointer\" src=\"" + cndImagePath + "bubble-tail2.png\" /><div>";
    bubbleHodler.html(strInfo);
    bubbleHodler.css({ left: left + 'px', top: top + 'px', width: '100px' });

    bubbleHodler.show();
}

function populateTickerContent() {
    var content;
    //ticker data
    if ($("#TotalHCEarn").html() != null) {
        content = $(".TotalHCForThisYear").html();
        content = content.replace('{0}', $("#TotalHCEarn").val());
        $(".TotalHCForThisYear").html(content);
    }
     if ($("#YouTookMoreSteps").html() != null) {
        content = $(".YouTookMoreSteps").html();
        content = content.replace('{0}', $("#YouTookMoreSteps").val());
        $(".YouTookMoreSteps").html(content);
    }

    if ($("#SponsorAverageSteps").html() != null) {
        content = $(".YourOrganization").html();
        content = content.replace('{0}', $("#SponsorAverageSteps").val());
        $(".YourOrganization").html(content);
    }

    if ($("#AverageDailySteps").html() != null && $(".AverageDailySteps").html() != null) {
        content = $(".AverageDailySteps").html();
        if (content != null) {
            content = content.replace('{0}', $("#AverageDailySteps").val());
            $(".AverageDailySteps").html(content);
        }
    }

    if ($("#YesterdayUploadedSteps").html() != null) {
        content = $(".YesterdayUploadedSteps").html();
        content = content.replace('{0}', $("#YesterdayUploadedSteps").val());
        $(".YesterdayUploadedSteps").html(content);
    }

    if ($("#AgeAverageSteps").html() != null) {
        content = $(".OthersYourAge").html();
        content = content.replace('{0}', $("#AgeAverageSteps").val());
        $(".OthersYourAge").html(content);
    }

    if ($("#GenderAverageSteps").html() != null && $(".OtherMales").html()) {
        content = $(".OtherMales").html();
        content = content.replace('{0}', $("#GenderAverageSteps").val());
        $(".OtherMales").html(content);
    }

    if ($("#GenderAverageSteps").html() != null && $(".OtherFemales").html()) {
        content = $(".OtherFemales").html();
        content = content.replace('{0}', $("#GenderAverageSteps").val());
        $(".OtherFemales").html(content);
    }

    if ($("#AverageUploadPerDay").html() != null) {
        content = $(".AverageUploadPerDay").html();
        content = content.replace('{0}', $("#AverageUploadPerDay").val());
        $(".AverageUploadPerDay").html(content);
    }

    if ($("#LastWeekMoreActiveMinutes").html() != null) {
        content = $(".LastWeekMoreActiveMinutes").html();
        content = content.replace('{0}', $("#LastWeekMoreActiveMinutes").val());
        $(".LastWeekMoreActiveMinutes").html(content);
    }
}

function populateFormattedContent() {
    
    // popup rewards
    var content = $(".EarnedHealthMiles").html();
    content = content.replace('{0}', $("#sumPopupRewards").val());
    $(".EarnedHealthMiles").html(content);
    
    populateTickerContent();
    
    // terms of use section    
    content = $(".ToContinueAsMember").html();
    content = content.replace('{0}', $("#ToContinueAsMember").val());
    $(".ToContinueAsMember").html(content);

    content = $(".WeUpdatedAgreement").html();
    content = content.replace('{0}', $("#WeUpdatedAgreement").val());
    $(".WeUpdatedAgreement").html(content);

    //challenges
    //to make sure we don't display same content for multiple challenges running at the same time, first check if element exists and then do a foreach for all of them. It can happen to have multiple challenges running at the same time
    if ($(".ChallengeSignedUpStartsInOneDay").length > 0) {
        $(".ChallengeSignedUpStartsInOneDay").each(function () {
            var challSignedUpStartsTomorrowElem = $(this);
            if ($(challSignedUpStartsTomorrowElem).html() != null) {
                content = $(challSignedUpStartsTomorrowElem).html();
                content = content.replace('{0}', $(challSignedUpStartsTomorrowElem).attr("formatVal"));
                $(challSignedUpStartsTomorrowElem).html(content);
            }
        });
    }
    if ($('.ChallengeSignedUpStartsInNDays').length > 0) {
        $('.ChallengeSignedUpStartsInNDays').each(function () {
            var challSignedUpStartsDaysItem = $(this);
            if ($(challSignedUpStartsDaysItem).html() != null) {
                content = $(challSignedUpStartsDaysItem).html();
                content = content.replace('{0}', $(challSignedUpStartsDaysItem).attr("formatVal1")).replace('{1}', $(challSignedUpStartsDaysItem).attr("formatVal2"));
                $(challSignedUpStartsDaysItem).html(content);
            }
        });
    }
    if ($(".ChallengeSignedUpInProgress").length > 0) {
        $( ".ChallengeSignedUpInProgress" ).each( function() {
            var challSignedUpInProgressElem = $(this);
            if ($(challSignedUpInProgressElem).html() != null) {
                content = $(challSignedUpInProgressElem).html();
                content = content.replace('{0}', unescape($(challSignedUpInProgressElem).attr("formatVal")));
                $(challSignedUpInProgressElem).html(content);
            }
        } );
    }
    if ($('.ChallengeSignedUpNoRank').length > 0) {
        $('.ChallengeSignedUpNoRank').each(function () {
            var challengeSignedupnoRankElem = $(this);
            if ($(challengeSignedupnoRankElem).html() != null) {
                content = $(challengeSignedupnoRankElem).html();
                content = content.replace('{0}', $(challengeSignedupnoRankElem).attr("formatVal"));
                $(challengeSignedupnoRankElem).html(content);
            }
        });
    }
    if ($('.ChallengeSignedUpFinished').length > 0) {
        $('.ChallengeSignedUpFinished').each(function () {
            var challengeSignedUpFinishedRankElem = $(this);
            if ($(challengeSignedUpFinishedRankElem).html() != null) {
                content = $(challengeSignedUpFinishedRankElem).html();
                content = content.replace('{0}', $(challengeSignedUpFinishedRankElem).attr("formatVal1")).replace('{1}', $(challengeSignedUpFinishedRankElem).attr("formatVal2"));
                $(challengeSignedUpFinishedRankElem).html(content);
            }
        });
    }
    
    
    //Know and Go
    content = $(".Exer_ForStarting").html();
    $( "#hdnStartingToTrackMsg" ).val( " " + content ); // needs extra leading space

    content = $(".Exer_CompleteHalfwayThere").html();
    $("#hdnHalfWayThereMsg").val(" " + content); // needs extra leading space

    content = $(".Exer_JustEarned").html();
    $("#hdnJustEarnedMsg").val(content);

    content = $(".KagYes").html();
    $("#hdnKagResponseYes").val(content);

    content = $(".KagNo").html();
    $("#hdnKagResponseNo").val(content);

    content = $(".ThanksForParticpating").html();
    $("#hdnThanksForParticpating").val(content);

    
}

function populateGameContent() {
    if ($("#gameStart").html() != null) {
       
        var content = $(".graphDaysLeft").html();
        if (content != null && content.length > 0) {
            content = content.replace('{0}', $("#DaysLeft").val()) ;
            $(".graphDaysLeft").html(content);
        }else {
            setTimeout('populateGameContent()', 1000);
            return;
        }

        content = $(".GameStart").html();
        content = content+" "+$("#gameStart").val();
        $(".GameStart").html(content);

        if ($("#gameEnd").val() != null) {
            content = $( ".GameEnd" ).html();
            content = content+" "+ $( "#gameEnd" ).val();
            $( ".GameEnd" ).html( content );
        }
        content = $(".ToNextLevelAmt").val();
        content = content.replace('{0}', $('#AmountToNextLevel').val());
        content += '<br/><br/>';
        content += $(".OnTrackToEarn").val();
        content = content.replace('{0}', $('#OnTrackToEarnAmount').val());
        content = content.replace('{1}', $('#gameEnd').val());

        $(".runningManMsg").popover({ html: true, content: content });
    } 
}

function updatePageContent() {

    if (PAGE_CONTENT) {
        for ( var key in PAGE_CONTENT ) {
            var val = PAGE_CONTENT[key];
           
            try {
                var $element = $("." + key);

                if ($element.hasClass('content')) {
                    $element.each( function() {
                        if ( $( this ).html() != null ) {
                            if ( $( this ).attr( "type" ) == "hidden" ) {
                                $( this ).val( val );
                            } else {
                                $( this ).html( val );
                            }
                            $( this ).removeClass( "content" );
                        }
                    } );
                    if (!($.browser.msie && $.browser.version <= 9)) {
                        $element.html(val);
                    }
                }

            } catch (ex) {
                
                console.log(ex);
            }
        }
    }
}

function LoadContentLabels() {

    $.ajax({
        cache: false,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "GetLandingPageContentLabels",
        dataType: "json",
        success: function (content) {

            PAGE_CONTENT = {};

            for (var i = 0; i < content.length; i++) {

                PAGE_CONTENT[content[i].Item1] = content[i].Item2;
                $("#contentSrcContainer").append('<input class="contentSource" type="hidden" id=hdn' + content[i].Item1 + '" key="' + content[i].Item1 + '" value="' + content[i].Item2 + '" />');
            }

            updatePageContent();

            populateFormattedContent();
            LoadKnowAndGo();
            StartTicker();
        },
        error: function (ex) {
            console.log( ex );
        }
    });
}

function badBrowser() {
    if ($.browser.msie && parseInt($.browser.version) <= 6) { return true; }
    if ($.browser.opera && parseInt($.browser.version) <= 9) { return true; }
    if (/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent)) {
        var ffversion = new Number(RegExp.$1)
        if (ffversion <= 4) { return true; } 
    }
    if ($.browser.safari && (navigator.appVersion.indexOf("1") != -1)) { return true; }
    if ($.browser.safari && (navigator.appVersion.indexOf("2") != -1)) { return true; }
    if ($.browser.safari && (navigator.appVersion.indexOf("3") != -1)) { return true; }
    return false;
}
