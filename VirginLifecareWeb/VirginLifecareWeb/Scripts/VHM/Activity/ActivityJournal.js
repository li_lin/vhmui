﻿
var VHM = VHM || {};
VHM.AJ = VHM.AJ || {};

function drawChart() {

    // Create the data table.
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Activity Date');
    data.addColumn('number', 'Steps');
    data.addRows( [
            ['3/1/2013', 2000],
            ['3/2/2013', 1500],
            ['3/3/2013', 100],
            ['3/4/2013', 1200],
            ['3/5/2013', 2250],
            ['3/6/2013', 3000],
            ['3/7/2013', 10000],
            ['3/8/2013', 8500],
            ['3/9/2013', 4500],
            ['3/10/2013', 3500],
            ['3/11/2013', 3268],
            ['3/12/2013', 4000]
        ] );

    // Set chart options
    var options = { 'title': '',
        'width': 750,
        'height': 570,
        'hAxis.titleTextStyle': {color: 'black', fontName: <global-font-name>, fontSize: <global-font-size>}
    };

    // Instantiate and draw our chart, passing in some options.
    var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
    chart.draw(data, options);
}