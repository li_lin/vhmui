﻿// requires inflection.js

var LapGraphModule = (function () {
	"use strict";

	// Graph look & feel
	var labelFont = '"VAG Rounded", "Arial Rounded MT Bold", Arial, sans-serif';
	var simpleLabelFont = 'Helvetica, Arial, sans-serif';
	var lapColors = ['#ae2323', '#009999', '#f79521', '#c80875', '#08ac55'];
	var width = 275;
	var height = 310;
	var flipTransform = 't145,0s0,1,0,0';
	var flipDuration = 750;

	// Current state
	var paper;
	var frontState;
	var backState;
	var visibleState;
	var flipTimer;

	function addEntriesAndRedraw(state, entriesToAdd) {
		if (!paper) {
			// The graph hasn't been drawn yet so there's nothing to update
			return;
		}

		state.currentProgress += entriesToAdd;
		state.totalEarned += entriesToAdd;

		// TODO:  Currently assuming 100 entries per lap, but this may change

		// Advance laps, adding bonus points, until we're out of laps or under 100% for the lap
		var originalLap = state.currentLap;
		while (state.currentProgress >= 100 && state.currentLap < state.totalLaps) {
			state.currentLap++;
			state.currentProgress += state.bonusEntries - 100;
			state.totalEarned += state.bonusEntries;
		}

		// If we're out of laps, max out current progress at 100% so graphs don't look weird
		if (state.currentProgress > 100) {
			state.currentProgress = 100;
		}

		// Redraw the graph to show progress
		state.set.remove();
		drawLapGraph(state);
		flipToLapGraph(state, false);

		// Notify the user if he/she has advanced 1+ laps
		if (originalLap < state.currentLap) {
			$('#challenges-congrats .value').html(state.bonusEntries);
			$('#challenges-congrats .way-to-go-msg').css('color', getLapColor(originalLap));
			$('#challenges-congrats').modal();
		}
	}

	function flipToLapGraph(state, animate) {
		if (visibleState == state) {
			// This side is already visible
			return;
		}

		clearTimeout(flipTimer);
		if (animate === undefined) {
			animate = true;
		}
		if (animate) {
			visibleState.set.animate({ transform: flipTransform }, flipDuration, 'easeIn');
			flipTimer = setTimeout(function () {
				state.set.animate({ transform: '' }, flipDuration, 'easeOut');
			}, flipDuration);
		} else {
			visibleState.set.transform(flipTransform);
			state.set.transform('');
		}
		visibleState = state;
	}

	function arcAttrFunction(xloc, yloc, value, total, radius) {
		// Calc start position
		var startAngle = 260;
		var xStart = xloc + Math.cos(startAngle * Math.PI / 180) * radius;
		var yStart = yloc - Math.sin(startAngle * Math.PI / 180) * radius;

		// Calc arc length
		var alpha = 360 / total * value;
		var a = (startAngle - alpha) * Math.PI / 180;
		var xArc = xloc + radius * Math.cos(a);
		var yArc = yloc - radius * Math.sin(a);

		return {
			path: [
				["M", xStart, yStart],
				["A", radius, radius, 0, +(alpha > 180), 1, xArc, yArc]
			]
		};
	}

	function getLapColor(lap) {
		return lapColors[(lap - 1) % lapColors.length];
	}

	function drawLapGraph(state) {
		// Create paper on which graph is rendered
		if (!paper) {
			paper = Raphael("lap-display", width, height);
			paper.customAttributes.arc = arcAttrFunction;
		}

		// Draw elements on paper
		paper.setStart();
		if (state.currentProgress !== null) {
			drawLapGraphGauge(state);
		}
		drawLabels(state);
		if (state.totalLaps > 1) {
			drawLapLegend(state);
		}
		state.set = paper.setFinish();

		return state;
	}

	function drawLapGraphGauge(state) {
		var xOffset = 0;
		var yOffset = 0;
		var radius = 100;
		var xArc = xOffset + width / 2 + 5;
		var yArc = yOffset + height / 2 + 2;

		// Lap border image
		paper.image('/v2/Content/Images/Ignite/dial-empty.png', xOffset + 15, yOffset + 30, 253, 251).toBack();

		// Draw arc that grows from 0% to drawProgress% with a bounce animation
		var arcColor = getLapColor(state.currentLap);
		var drawProgress = (state.currentProgress * 0.943); // Lap is 94% of a complete circle, so scale percentage appropriately
		var arc = paper.path().attr({
			"stroke": arcColor,
			"stroke-width": 40,
			arc: [xArc, yArc, (state.animate !== false) ? 0 : drawProgress, 100, radius]
		});
		if (state.animate !== false) {
			arc.animate({
				"stroke-width": 40,
				arc: [xArc, yArc, drawProgress, 100, radius]
			}, 2200, "bounce");
		}
	}

	function drawLabels(state) {
		var xOffset = 142,
		    yOffset = 120,
		    shadowColor = '#222',
			fontColor = '#666',
			entryFillColor1 = Raphael.getRGB($('.js-gradient-color').css('color')).hex,
			entryFillColor2 = Raphael.getRGB($('.js-gradient-color').css('background-color')).hex,
			entryFill = '90-' + entryFillColor1 + ':5-' + entryFillColor2 + ':95';

		// Entries count (big label) shadow
		paper.text(xOffset + 2, yOffset + 1, state.totalEarned).attr({
			'font-size': 60,
			'font-family': labelFont,
			'fill': shadowColor,
			'letter-spacing': '-4px'
		});

		// Entries count (big label)
		paper.text(xOffset, yOffset, state.totalEarned).attr({
			'font-size': 60,
			'font-family': labelFont,
			'fill': entryFill,
			'letter-spacing': '-4px'
		});

		// "Entries" label
		paper.text(xOffset, yOffset + 34, (state.totalEarned == 1) ? state.currency : state.pluralCurrency).attr({
			'font-size': 18,
			'font-family': labelFont,
			'fill': entryFill
		});

		// Title label above entries
		if (state.title) {
			paper.text(xOffset, yOffset - 36, state.title).attr({
				'font-size': 18,
				'font-family': simpleLabelFont,
				'fill': fontColor,
				'font-weight': 'lighter'
			});
		}

		// Description label below entries (e.g. "x days left")
		paper.text(xOffset, yOffset + 56, state.description).attr({
			'font-size': 18,
			'font-family': simpleLabelFont,
			'fill': fontColor,
			'font-weight': 'lighter'
		});

		// "Lap x/y" label
		paper.text(xOffset, yOffset + ((state.currentProgress === null) ? 110 : 76), state.details).attr({
			'font-size': 13,
			'font-family': simpleLabelFont,
			'fill': fontColor,
			'font-weight': 'lighter'
		});

		// "x bonus entries!" label with image (star and pointer)
		if (state.bonusEntries > 0 && state.currentLap < state.totalLaps) {
			paper.image('/v2/Content/Images/Ignite/bonus-pointer.png', xOffset + 13, yOffset + 120, 34, 52);
			paper.text(xOffset + 8, yOffset + 172, state.bonusEntries + ((state.bonusEntries == 1) ? ' bonus ' + state.currency +'!' : ' bonus ' + state.pluralCurrency + '!')).attr({
				'font-size': 15,
				'font-family': simpleLabelFont,
				'fill': fontColor,
				width: 100,
				'text-anchor': 'end'
			});
		}
	}

	function drawLapLegend(state) {
		var itemWidth = 26;
		var itemHeight = 26;
		var paddingWidth = 3;
		var xOffset = width - itemWidth - paddingWidth * 2;
		var yOffset = 0;
		var strokeWidth = 4;
		var radius = Math.sqrt(itemWidth) + strokeWidth + 1;
		
		for (var lap = state.totalLaps; lap >= 1; lap--, xOffset -= itemWidth + paddingWidth) {
			var xCenter = xOffset + itemWidth / 2;
			var yCenter = yOffset + itemWidth / 2;

			// Legend image
			paper.image('/v2/Content/Images/Ignite/dial-legend.png', xOffset, yOffset, itemWidth, itemHeight);

			// Legend lap number
			paper.text(xCenter, yCenter, lap).attr({
				'font-size': 10,
				'font-family': simpleLabelFont,
				'fill': '#aaa',
				width: itemWidth
			});

			// Legend fill
			if (lap <= state.currentLap) {
				var arcColor = lapColors[(lap - 1) % lapColors.length];
				var arcPct = (lap == state.currentLap) ? state.currentProgress : 100;

				paper.path().attr({
					"stroke": arcColor,
					"stroke-width": strokeWidth,
					arc: [xCenter, yCenter, arcPct * 0.96, 100, radius]
				});
			}
		}
	}

	// Expose the LapGraphModule API
	return {
	    DrawLapGraphSideNav: function (currentProgress, totalEarned, daysLeft, currentLap, totalLaps, bonusEntries, currency) {
	       
			visibleState = frontState = drawLapGraph({
				currentProgress: currentProgress,
				totalEarned: totalEarned,
				description: daysLeft + ((daysLeft == 1) ? ' day left' : ' days left'),
				details: 'lap ' + currentLap + '/' + totalLaps,
				currentLap: currentLap,
				totalLaps: totalLaps,
				bonusEntries: (bonusEntries === undefined) ? 10 : bonusEntries, // TODO: Get bonusEntries from backend
				currency: currency,
				pluralCurrency: currency.pluralize()
			});

			$('#lap-display .laps-flip').click(function () {
				var showBack = (visibleState == frontState);
				if (showBack && !backState) {
					// TODO: Create back state for real.  Right now this is dummy data.
					backState = drawLapGraph({
						currentProgress: null,
						totalEarned: (frontState.totalEarned > 25) ? 25 : frontState.totalEarned,
						title: 'You earned',
						description: 'last month!',
						details: 'The winners will be announced on the\n3rd of every month.',
						totalLaps: 0
					});
					backState.set.transform(flipTransform); // start hidden
				}

				flipToLapGraph((showBack) ? backState : frontState);

				$('#lap-display')[(showBack) ? 'addClass' : 'removeClass']('back-side');
				return false;
			});
		},

		AddEntriesToLapGraph: function (entriesToAdd) {
			if (visibleState != frontState) {
				$('#lap-display .laps-flip').click();
			}
			addEntriesAndRedraw(frontState, entriesToAdd);
		}
	};
})();
