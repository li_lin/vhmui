﻿
var VHM = VHM || {};
VHM.Social = VHM.Social || {}; //Social -->Social object - general through the site
VHM.SocialFrnds = VHM.SocialFrnds || {}; //Social --> My friends object
VHM.SocialSpaces = VHM.SocialSpaces || {}; //Social --> My spaces object
VHM.MyProfile = VHM.MyProfile || {};
VHM.MyAccount = VHM.MyAccount || {};

VHM.Social.SessionID;
VHM.Social.SearchClicked = false;
VHM.Social.CFServiceUrl = "../../webservices/WSVHMSocial.asmx";
VHM.Social.appPath = "";
var appPath = "";
VHM.SocialFrnds.relationshipChanged = false;
//Disable the enter key
function stopRKey(evt) {
    evt = (evt) ? evt : ((event) ? event : null);
//    var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
    if ((evt.keyCode == 13)) { return false; } // && (node.type == "text")
}  
document.onkeypress = stopRKey;

VHM.Social.CloseModal = function (modalId) {
    var jqoModal = $("#" + modalId);
    jqoModal.jqmHide();
};

VHM.Social.specCharact = function (strToCheck) {
    strToCheck = strToCheck.replace("&amp;amp;", /&/g).replace("&amp;lt;", /</g).replace("&amp;gt;", />/g);
    strGoalInterest = strGoalInterest.replace("&#96;", /`/g);
    return strToCheck;
};
//this parameter will be used to know the location on the page. If a member adds a friend, will use this global var to know what part of page to reload.
//Use: page - to reload entire page; search - to reload just searchResults area; suggestMoreFriends - to reload suggest more; nothing else (maybe form newsfeed... )
VHM.SocialFrnds.loadPartOfPage = "";
$.extend({
    getUrlVars: function () {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        if (typeof hashes != "undefined") {
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
        }
        return vars;
    },
    getUrlVar: function (name) {
        return $.getUrlVars()[name];
    }
});
VHM.Social.GetSession = function () {
    if (VHM.Social.SessionID == null) {
        VHM.Social.SessionID = VHM.Core.ReadCookie("SessionID");
    }
    return VHM.Social.SessionID;
};


//--------------------------------------------------------------------------------------------------------------------------------------------
//-------------------------------Load data - My Friends page functionality -------------------------------------------------------------------
VHM.SocialFrnds.reloadMyFriends = function (html) {
    $("#divAllMyFriends").html(html);
};

VHM.SocialFrnds.loadMyFriends = function (sWhat, bSearching) {
    var strWhat = "";
    var whatToPop = "";
    var srcVal = "";
    
    if (typeof sWhat != "undefined" && sWhat != "") {
        strWhat = sWhat;
    } else {
        strWhat = $.getUrlVar('WhatToPopulate');
    }
    
    //if the "what tot populate" var is not in the url by any chance, then show friends
    if (typeof strWhat != "undefined" && strWhat != "") {
        whatToPop = strWhat;
    } else {
        whatToPop = 'friends';
    }

    var itemsPerPage;
    var pageNumber;
    var whatSize = $.getUrlVar('pageSize');
    var pagNo = $.getUrlVar('pageNumber');
    
    if (typeof bSearching == 'undefined' && typeof whatSize != "undefined" && whatSize != "") {
        itemsPerPage = whatSize;
    } else {
        itemsPerPage = 24;
    }

    if (typeof bSearching == 'undefined' && typeof pagNo != "undefined" && pagNo != "") {
        pageNumber = pagNo;
    } else {
        pageNumber = 1;
    }

    //display the loading div while loading info
    var loadMess = "";
    var jqoLoad = $("#Loading");
    
    if (whatToPop == 'friends') {
        loadMess = "We're loading your Friends.";
        srcVal = "";
    } else {
        loadMess = "We're completing your search request.";
        var srcURLVal = $.getUrlVar('searchStr');
        if (typeof bSearching == 'undefined') {
            srcVal = srcURLVal;
        } else {
            var jqoSearch = $("#searchBox");
            srcVal = jqoSearch.val();
        }
        srcVal = srcVal.trim();
        srcVal = srcVal.StripOutHTMLTags();
    }
    
    $("#prog_msg").html(loadMess);
    var htmlLoad = jqoLoad.html();
    $("#divAllMyFriends").html(htmlLoad);

    $.ajax({
        url: "../Social/loadFriendsORSearch.aspx",
        data: { WhatToPopulate: whatToPop, pageSize: itemsPerPage, pageNumber: pageNumber, searchStr: srcVal },
        dataType: "html",
        success: function (data, textStatus) {
            if (whatToPop == 'friends') {
                VHM.SocialFrnds.loadPartOfPage = "page";
            } else {
                VHM.SocialFrnds.loadPartOfPage = "search";
            }
            VHM.SocialFrnds.reloadMyFriends(data);
        }
    });

    if (bSearching != true) {
        var fList = '';
        $.ajax({
            url: '../../webservices/WSVHMSocial.asmx/GetFriendsAutoSuggest',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function(data) {
                fList = eval('(' + data.d + ')');
                $("#hdnAllMyFriends").val(JSON.stringify(fList));
                $("#searchBox").keyup(function() {
                    VHM.SocialFrnds.populateSearchSugg();
                });
            },
            error: function() {
                $("#hdnAllMyFriends").val('');
            }
        });
    }
};

VHM.SocialFrnds.GetCFUserProfDataCache = [];

VHM.SocialFrnds.GetCFUserProfData = function (FriendUserID, dialogId) {
    var openDialog = false;
    $(".centerLoading").fadeIn("fast");
    var dialog = $("#" + dialogId);
    dialog.find(".divUserProfile").dialog('close');

    var params = { pId: FriendUserID, sId: VHM.Social.GetSession(), appPath: VHM.Social.appPath };
    var sParams = JSON.stringify(params);
    cleanUserProf(dialog);

    if (VHM.SocialFrnds.GetCFUserProfDataCache[FriendUserID] && !VHM.SocialFrnds.relationshipChanged) {
        VHM.SocialFrnds.GetCFUserProfDataCallback(VHM.SocialFrnds.GetCFUserProfDataCache[FriendUserID], dialog, openDialog);
    } else {
        //removed the SOAPClient.invoke() method of getting the webservice responsoe as FireFox breaks result sets too big        
        $.ajax({
            cache: false,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: VHM.Social.CFServiceUrl + "/GetMemberProfile",
            data: sParams,
            dataType: "json",
            success: function (responseData, textStatus) {
                if (responseData != null && responseData.d != null) {
                    VHM.SocialFrnds.GetCFUserProfDataCache[FriendUserID] = responseData.d;
                    VHM.SocialFrnds.GetCFUserProfDataCallback(responseData.d, dialog, openDialog);
                };
            }
        });
    };
};
VHM.SocialFrnds.GetCFUserProfData_Dialog = function (FriendUserID, dialogId) {
    var openDialog = true;
    $(".centerLoading").fadeIn("fast");
    var dialog = $("#" + dialogId);

    var Params = { pId: FriendUserID, sId: VHM.Social.GetSession(), appPath: VHM.Social.appPath };
    var sParams = JSON.stringify(Params);
    cleanUserProf(dialog);

    if (VHM.SocialFrnds.GetCFUserProfDataCache[FriendUserID] && !VHM.SocialFrnds.relationshipChanged) {
        VHM.SocialFrnds.GetCFUserProfDataCallback(VHM.SocialFrnds.GetCFUserProfDataCache[FriendUserID], dialog, openDialog);
    } else {
        //removed the SOAPClient.invoke() method of getting the webservice responsoe as FireFox breaks result sets too big  

        $.ajax({
            cache: false,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: VHM.Social.CFServiceUrl + "/GetMemberProfile",
            data: sParams,
            dataType: "json",
            beforeSend: function () {
                dialog.dialog('close');
            },
            complete: function () {
                dialog.dialog('open');
            },
            success: function (responseData, textStatus) {
                if (responseData != null && responseData.d != null) {
                    VHM.SocialFrnds.GetCFUserProfDataCache[FriendUserID] = responseData.d;
                    VHM.SocialFrnds.GetCFUserProfDataCallback(responseData.d, dialog, openDialog);
                };
            },
            error: function (xhr, ajaxOptions, error) { }
        });
    }
};
VHM.SocialFrnds.getMoreSuggestedFriends = function () {
    $(".centerLoading").fadeIn("fast");
    
    var pageNo;
    var pageSize;

    if (VHM.SocialFrnds.showMoreSgstPageNo == 0) {
        pageNo = 1;
        pageSize = 15;
    } else {
        pageNo = VHM.SocialFrnds.showMoreSgstPageNo;
        pageSize = 15;
    }

    $.ajax({
        type: 'POST',
        url: '../../../webservices/WSVHMSocial.asmx/TopNSuggestedFriends',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: JSON.stringify({
            start: pageNo,
            pageSize: pageSize
        }),
        success: VHM.SocialFrnds.getMoreSuggestedFriendsDataCallBack
    });

    VHM.SocialFrnds.loadPartOfPage = "suggestMoreFriends";
};
VHM.SocialFrnds.populateSearchSugg = function () {
    var searchBox = $("#searchBox");
    searchBox.removeData('events');
    var strSearch = searchBox.val();
    strSearch = strSearch.trim();
    strSearch = strSearch.StripOutHTMLTags();
    
    var handleSearch = function () {
        var strSrch = $("#searchBox").val();
        strSrch = strSrch.trim();
        if (strSrch.length > 2) {
            $("#btnSearchGroup").removeAttr("disabled");
        } else {
            $("#btnSearchGroup").attr("disabled", "true");
        };
    };
    
    if (strSearch !== "") {
        var jqoHidden = $(".hdnFriends");
        var strRespData = jqoHidden.val();
        if (typeof strRespData !== "undefined" && strRespData != "") {
            var srcSugg =  $.parseJSON(strRespData);
            var acCbo = {
                delay: 500,
                matchContains: 'word',
                minChars: 1,
                cacheLength: 20,
                mustMatch: false,
                autoFill: false,
                max: 25,
                selectFirst: true,
                scroll: false
            };
            $("#searchBox").autocomplete(srcSugg, acCbo)
                   .result(function (event, item) { handleSearch(); });
        }
        handleSearch();
    } else {
        var searchListHold = $("#searchSuggestions");
        searchListHold.html("");
        var searchDiv = $("#divSearchSuggestions");
        searchDiv.hide();
        handleSearch();
    }
};
VHM.SocialFrnds.getMoreSuggestedFriendsDataCallBack = function (data) {
    var arrayResponseData = data.d;
    var getMoreSgstFriends = arrayResponseData.topNFriends;

    if (VHM.SocialFrnds.showMoreSgstPageNo < 2) {
        $("#suggestMoreFriends").html("");
    }
    var hasMoreSugg = arrayResponseData.hasMore;
    if (!hasMoreSugg) {//do not show "See More..." link
        $("#divContinue").hide();
    } else {
        $("#divContinue").show();
        VHM.SocialFrnds.showMoreSgstPageNo = (VHM.SocialFrnds.showMoreSgstPageNo + 1);
    }

    var suggMoreFrMarkup = "<li class=\"suggestFRListItems\"><div class=\"axero-section-list-avatar\"><a href=\"#\" class=\"jqModalOnTop\" cfId=\"${UserID}\" onclick=\"javascript:VHM.SocialFrnds.GetCFUserProfData('${UserID}', 'divSuggestMoreUP');return false;\"><img alt=\"${MemberName}\" class=\"jqModalOnTop contentImg\" src=\"${ProfilePicturePath}\" onclick=\"javascript:VHM.SocialFrnds.GetCFUserProfData('${UserID}', 'divSuggesedUserProfile');return false;\" /></a></div><div  class=\"axero-section-list-content\"><div class=\"axero-section-list-content-title\"><a href=\"#\" class=\"jqModalOnTop\" onclick=\"javascript:VHM.SocialFrnds.GetCFUserProfData('${UserID}', 'divSuggesedUserProfile');return false;\">${MemberName}</a></div></div><div class=\"axero-section-list-content-date\"><small class=\"small\">${OrganizOrLoc}</small></div></li>";
    $.template("suggestMoreTempl", suggMoreFrMarkup);
    $.tmpl("suggestMoreTempl", getMoreSgstFriends).appendTo("#suggestMoreFriends");
    
    $(".centerLoading").fadeOut("fast");
};

VHM.SocialFrnds.GetCFUserProfDataCallback = function (soapResult, dialog, openDialog) {
    //clear SocialFrnds.CFUserID - make sure there is no record
    VHM.SocialFrnds.CFotherPersUserID = -1; //Privacy can be: Private - to be seen only by friends; Public - to be seen by anyone; DoNotShow - not visible for anyone
    //var objRespData = eval('(' + soapResult + ')');
    var objRespData = $.parseJSON(soapResult);
    //set up the profile image source URL; name + org or Loc 
    var friendStatus;

    friendStatus = objRespData.Relationship;
    VHM.SocialFrnds.CFotherPersUserID = objRespData.CFuserID;
    var imgProf = dialog.find("[vlc_name='VHMimgProf']");
    imgProf.attr("src", objRespData.ProfileImgPath);

    var usrName = dialog.find("[vlc_name='divName_Loc']");
    var usrMotto = dialog.find("[vlc_name='userMotto']");
    var usrLocOrg = dialog.find("[vlc_name='userOrganizOrLoc']");
    usrName.html(objRespData.Name);

    if (objRespData.Motto != "") {
        strMotto = "<small class=\"small\">\"" + objRespData.Motto + "\"</small>";
        usrMotto.append(strMotto);
    }

    if (objRespData.OrganizOrLoc != "") {
        usrLocOrg.html(objRespData.OrganizOrLoc);
    }

    var socialProfile = objRespData.UserPrivacySett;
    VHM.SocialFrnds.buildUserProfile(socialProfile, dialog);

    var isFriendOptions = dialog.find("[vlc_name = 'divIsFriend']");
    var isFriendPending = dialog.find("[vlc_name = 'divPendingFriend']");
    var isNotFriend = dialog.find("[vlc_name = 'divAddFriend']");
    var isRequested = dialog.find("[vlc_name = 'divFriendReq']");

    if (friendStatus == 'ConfirmedFriends') { //show "Challenge me" and "Remove as Friend" options
        $(".ahrfRemovefr").unbind("click"); //removes any events assigned previously
        $(".ahrfRemovefr").click(function () {
            $(VHM.StringTemplates.divRemoveFriend).dialog({
                modal: true,
                width: 300,
                position: ['center', 50],
                zIndex: 4000,
                title: "Remove as Friend",
                resizable: false
            });
            $('div.ui-dialog-titlebar').find('a.ui-dialog-titlebar-close').attr('style', 'background-image: none;');
            $('div.ui-dialog-titlebar').find('span.ui-icon-closethick').html('');
        });
        //build "challenge me" onclick event
        var jqoChallenge = dialog.find("[vlc_name = 'btCallengeMe']");
        var vhmMID = objRespData.VHMuserID;
        jqoChallenge.unbind('click').click(function () {
            var strChallURL = VHM.Social.appPath + '/Secure/Challenge/create.aspx?mid=' + vhmMID + '&challengeid=';
            window.location.replace(strChallURL);
        });
        isFriendOptions.show();
        isFriendPending.hide();
        isNotFriend.hide();
        isRequested.hide();
    } else if (friendStatus == 'FriendshipRequestPending') { // show "Add as friend" and "Decline" options
        isFriendOptions.hide();
        isFriendPending.show();
        isNotFriend.hide();
        isRequested.hide();

        var accptBtn = dialog.find("[vlc_name = 'btnAcceptFr']");
        var declineLnk = dialog.find("[vlc_name = 'hrefDecline']");
        //make sure we populate the right NotificId
        VHM.SocialFrnds.NotificationID = objRespData.MemberNotificationId;

        accptBtn.unbind('click').click(function () {
            $(this).attr("disabled", "true");
            VHM.SocialFrnds.relationshipChanged = true;
            VHM.SocialFrnds.acceptFriendship('Accept', VHM.SocialFrnds.CFotherPersUserID, VHM.SocialFrnds.NotificationID);

            dialog.dialog('close');

            return false;
        });

        declineLnk.unbind('click').click(function () {
            VHM.SocialFrnds.relationshipChanged = true;
            VHM.SocialFrnds.declineFriendship('Decline', VHM.SocialFrnds.CFotherPersUserID, VHM.SocialFrnds.NotificationID);

            dialog.dialog('close');

            return false;
        });
    } else if (friendStatus == 'FriendshipRequestSent') {
        isFriendOptions.hide();
        isFriendPending.hide();
        isNotFriend.hide();
        isRequested.show();
        var accptBtn = dialog.find("[vlc_name = 'btnRequested']");
        accptBtn.attr("disabled", "true");
    } else if (friendStatus == 'NoRelation') { // show "Add as Friend" option
        isFriendOptions.hide();
        isFriendPending.hide();
        isNotFriend.show();
        isRequested.hide();
        var addFriendBtn = dialog.find("[vlc_name = 'btnAddAsFriend']");
        addFriendBtn.show();
        addFriendBtn.unbind('click').click(function () {
            VHM.SocialFrnds.relationshipChanged = true;
            addFriendBtn.hide();
            isRequested.show();
            VHM.SocialFrnds.addAsFriend(dialog, VHM.SocialFrnds.CFotherPersUserID, 0, "");
            //return false;
        });
    } else { //relationship is self
        isFriendOptions.hide();
        isFriendPending.hide();
        isNotFriend.hide();
        isRequested.hide();
        var accptBtn = dialog.find("[vlc_name = 'btnRequested']");
        accptBtn.attr("disabled", "true");
    }

    $(".centerLoading").fadeOut("fast");

    dialog.find('.divUserProfile').show();

    if (!openDialog) {
        dialog.dialog('open');
    }
};

VHM.SocialFrnds.buildUserProfile = function (socialProfile, dialog) {
    $(".centerLoading").fadeIn("fast");
    if (socialProfile.ShowNickname) {
        var divNickname = dialog.find("[vlc_name='divProfNickName']");
        divNickname.show();
        var spanNickName = dialog.find("[vlc_name='lblNickname']");
        spanNickName.html(socialProfile.Nickname);
    }
    if (socialProfile.ShowGender) {
        var divGender = dialog.find("[vlc_name='divProfGender']");
        divGender.show();
        var spanGender = dialog.find("[vlc_name='labelGender']");
        spanGender.html(socialProfile.Gender);
    }
    if (socialProfile.ShowAge) {
        var divAge = dialog.find("[vlc_name='divProfAge']");
        divAge.show();
        var spanAge = dialog.find("[vlc_name='labelAge']");
        spanAge.html(socialProfile.Age);
    }
    if (socialProfile.ShowCity) {
        var divCity = dialog.find("[vlc_name='divProfCity']");
        divCity.show();
        var spanCity = dialog.find("[vlc_name='labelCity']");
        spanCity.html(socialProfile.City);
    }
    if (socialProfile.ShowSponsors) {
        var divOrganiz = dialog.find("[vlc_name='divProfSponsors']");
        divOrganiz.show();
        var spanOrganiz = dialog.find("[vlc_name='labelSponsor']");
        spanOrganiz.html(socialProfile.Sponsors);
    }
    if (socialProfile.ShowTotSteps) {
        var divtotalSt = dialog.find("[vlc_name='divProfTotSteps']");
        divtotalSt.show();
        var spanTotalST = dialog.find("[vlc_name='labelTotalSt']");
        spanTotalST.html(socialProfile.TotSteps);
    }
    if (socialProfile.ShowStepsAvg) {
        var divAverageSt = dialog.find("[vlc_name='divProfAvrgSteps']");
        divAverageSt.show();
        var spanAverageSt = dialog.find("[vlc_name='labelAverageSt']");
        spanAverageSt.html(socialProfile.StepsAvg);
    }
    if (socialProfile.ShowLastGZUpload) {
        var divLastUpload = dialog.find("[vlc_name='divProfLastUpload']");
        divLastUpload.show();
        var spanLastUpload = dialog.find("[vlc_name='labelLastUpld']");
        spanLastUpload.html(socialProfile.LastGZUpload);
    }
    if (socialProfile.ShowChallengeHistory) {
        var divChalHist = dialog.find("[vlc_name='divProfClngHist']");
        divChalHist.show();
        var spanChalHist = dialog.find("[vlc_name='labelHistory']");
        spanChalHist.html(socialProfile.ChallengeHistory);
    }
    if (socialProfile.ShowOccupation) {
        var divOccupation = dialog.find("[vlc_name='divProfOccupation']");
        divOccupation.show();
        var spanOccupation = dialog.find("[vlc_name='labelOccupat']");
        spanOccupation.html(socialProfile.Occupation);
    }
    if (socialProfile.ShowGoals) {
        var divProfGoals = dialog.find("[vlc_name='divProfGoals']");
        divProfGoals.show();
        var spanProfGoals = dialog.find("[vlc_name='labelGoal']");
        spanProfGoals.html(socialProfile.Goals);
    }

    if (socialProfile.ShowInterests) {
        var divProfInterests = dialog.find("[vlc_name='divProfInterests']");
        divProfInterests.show();
        var spanProfInterests = dialog.find("[vlc_name='labelInterest']");
        spanProfInterests.html(socialProfile.Interests);
    };
    if (socialProfile.ShowBadges) {
        var divBadges = dialog.find("[vlc_name='divBadges']");
        divBadges.show();
        var badgesHolder = dialog.find("[vlc_name='userErnedBadges']");
        var allErnBadges = eval(socialProfile.EarnedBadges);
        var hrefShowMore = dialog.find("[vlc_name='hrefShowMore']");
        var hrefShowTopFive = dialog.find("[vlc_name='hrefHideAll']");
        var totBadgesErnd = dialog.find("[vlc_name='totalNoBadges']");

        if (allErnBadges.length > 5) {
            totBadgesErnd.show();
            totBadgesErnd.html(allErnBadges.length + " Total");
            hrefShowTopFive.hide();
            hrefShowMore.show();
            hrefShowMore.click(function (e) {
                //onclick of this link show all badges that were earned by this member
                hrefShowMore.hide();
                hrefShowTopFive.show();
                //document.documentElement.scrollTop = 0;
                VHM.Social.GetMyEarnedBadgesHtml(socialProfile.EarnedBadges, badgesHolder, allErnBadges.length);
                hrefShowTopFive.click(function () {
                    hrefShowMore.show();
                    hrefShowTopFive.hide();
                    VHM.Social.GetMyEarnedBadgesHtml(socialProfile.EarnedBadges, badgesHolder, 5);
                });
            });
        } else {
            totBadgesErnd.hide();
            totBadgesErnd.html("");
            hrefShowMore.hide();
            hrefShowTopFive.hide();
        };
        VHM.Social.GetMyEarnedBadgesHtml(socialProfile.EarnedBadges, badgesHolder, 5);
    };
    $(".centerLoading").fadeOut("fast");
    dialog.fadeIn("slow");
};
VHM.Social.GetMyEarnedBadgesHtml = function (badgesObj, jqoHolder, noOfBadges) { //RewardedTaskTransactionID
    var badgesMarkup = "<li class=\"liEarnedBadges_UP\" style=\"position:relative;\"><div class=\"smBadgeEnrdLFT_UP VintStamp_div\"><img onclick=\"javascript:VHM.Badges.GetBadgeInfo(${BadgeID}, 'divUserBadges', 'false');return false;\" alt=\"${BadgeName}\" class=\"smallErndBadgeImg_UP clsCursHand VintStamp_img\" src=\"${BadgeImage}\" onmouseout=\"VHM.Badges.HideBadgeName('divBadgeHover');\" onmouseover=\"VHM.Badges.ShowBadgeName_UserProf('${BadgeName}','divUPBadgeHover', this);\" style=\"width:50px; height:50px;\" /><span class=\"VintStamp_span\" style=\"font-size:7px;top:28px;left:-7px;\"></span></div></li>";

    jqoHolder.html("");
    if (badgesObj !== "") {
        var allErnBadges = eval(badgesObj);
        var countedObj = [];
        for (var i = 0; i < noOfBadges; i++) {
            countedObj.push(allErnBadges[i]);
        };

        $.template("earnedBadgeTempl", badgesMarkup);
        $.tmpl("earnedBadgeTempl", countedObj).appendTo(jqoHolder);
    };
};
VHM.SocialFrnds.addAsFriend = function (dialog, CFotherUserID, categ, strMessage) {
    $(".centerLoading").fadeIn("fast");

    var addAsFriendDataCallback = function (soapResult, dialog) {
        var objRespData = eval('(' + soapResult + ')');
        //check if element is dialog as we started to introduce them and trying to close a modal will break
        var dialClass = dialog.attr('class');
        if (objRespData.Success) {
            VHM.SocialFrnds.FriendWasAdded = true;
            if (VHM.SocialFrnds.loadPartOfPage == 'page') {
                $(".centerLoading").fadeOut("fast");
            } else if (VHM.SocialFrnds.loadPartOfPage == 'search') {
                var isNotFriend = dialog.find("[vlc_name = 'divAddFriend']");
                var isRequested = dialog.find("[vlc_name = 'divFriendReq']");
                isNotFriend.hide();
                isRequested.show();
                $(".centerLoading").fadeOut("fast");
            } else if (VHM.SocialFrnds.loadPartOfPage == 'suggestMoreFriends') {
                if (dialClass.toLowerCase().indexOf("ui-dialog") >= 0) {
                    dialog.dialog('close');
                } else {
                    dialog.jqmHide();
                }
                var jqoShowMore = $("#divContinue");
                if (jqoShowMore.is(":visible")) {
                    VHM.SocialFrnds.showMoreSgstPageNo = (VHM.SocialFrnds.showMoreSgstPageNo - 1);
                };
                VHM.SocialFrnds.getMoreSuggestedFriends();
            } else {
                if (dialClass.toLowerCase().indexOf("ui-dialog") >= 0) {
                    dialog.dialog('close');
                } else {
                    dialog.jqmHide();
                }
                $(".centerLoading").fadeOut("fast");
            }
        } else {
            alert( "Sorry, but an error occurred. The operation was not successful!", { title: 'An error occurred' } );
            $(".centerLoading").fadeOut("fast");
        }
    };
    var params = { sId: VHM.Social.GetSession(), friendId: CFotherUserID, category: categ, message: strMessage.StripOutHTMLTags() };
    var sParams = JSON.stringify(params);
    $.ajax({
        cache: false,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: VHM.Social.CFServiceUrl + "/AddAsFriend",
        data: sParams,
        dataType: "json",
        success: function (responseData, textStatus) {
            if (responseData != null && responseData.d != null) {
                addAsFriendDataCallback(responseData.d, dialog);
            }
        }
    });
};
VHM.SocialFrnds.removeFriend = function () {
    var pl = new SOAPClientParameters();
    var removeFriendDataCallback = function (soapResult) {
        var strRespData = soapResult;
        var objRespData = eval('(' + strRespData + ')');
        if (objRespData.Success) {
            window.location.reload();
        } else {
            alert("There was an error. Remove was not successful!", { title: 'Something went wrong' });
        }
    };
    if (VHM.SocialFrnds.CFotherPersUserID > 0) {
        
        var Params = { sId: VHM.Social.GetSession(), exFriend: VHM.SocialFrnds.CFotherPersUserID };
        var sParams = JSON.stringify(Params);
        $.ajax({
            cache: false,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: VHM.Social.CFServiceUrl + "/RemoveFriend",
            data: sParams,
            dataType: "json",
            success: function (responseData, textStatus) {
                if (responseData != null && responseData.d != null) {
                    removeFriendDataCallback(responseData.d);
                };
            },
            error: function (xhr, ajaxOptions, error) { }
        });
    };
};
VHM.SocialFrnds.hideRemoveFriend = function (dialogId) {
    $("#" + dialogId).hide();
    $("#cover").remove();
};

VHM.SocialFrnds.acceptFriendship = function (strOpt, cfOtherUserID, notificationID) {
    
    VHM.SocialFrnds.CFotherPersUserID = cfOtherUserID;
    notificationID = parseFloat(notificationID);
    VHM.SocialFrnds.NotificationID = notificationID;
    if (notificationID > 0) {
        var Params = { sessionID: VHM.Social.GetSession(), notificationId: notificationID };
        var sParams = JSON.stringify(Params);
       
        $.ajax({
            cache: false,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: VHM.Social.CFServiceUrl + "/AcceptFriendRequest",
            data: sParams,
            dataType: "json",
            success: function (responseData, textStatus) {
                if (responseData != null && responseData.d != null) {
                    VHM.SocialFrnds.acceptFriendshipCallback(responseData.d, strOpt, VHM.SocialFrnds.CFotherPersUserID);
                };
            },
            error: function( xhr, ajaxOptions, error ) {
                alert('We could not add this person as a friend. They may belong to a community that does not allow this.');
            }
        });
    }
};
VHM.SocialFrnds.declineFriendship = function (strOpt, cfotherUserID, notificationID) {
    VHM.SocialFrnds.CFotherPersUserID = cfotherUserID;
   
    if (VHM.SocialFrnds.CFotherPersUserID > 0) {
        var Params = { sId: VHM.Social.GetSession(), notificationID: notificationID };
        var sParams = JSON.stringify(Params);
        $.ajax({
            cache: false,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: VHM.Social.CFServiceUrl + "/DeclineFriend",
            data: sParams,
            dataType: "json",
            success: function (responseData, textStatus) {
                if (responseData != null && responseData.d != null) {
                    VHM.SocialFrnds.acceptFriendshipCallback(responseData.d, strOpt, cfotherUserID);
                };
            },
            error: function (xhr, ajaxOptions, error) { }
        });        
    }
};
VHM.SocialFrnds.acceptFriendshipCallback = function ( soapResult, strOpt, cfUserID ) {
    var objRespData = $.parseJSON( soapResult );  //eval('(' + strRespData + ')');
    var makeSelection = function ( strOpt, cfUserID ) {
        var btnID = "btnAcceptFr_" + cfUserID;
        var lnkDeclID = "hrefDecline_" + cfUserID;
        var spanID = "spSelectionVal_" + cfUserID;

        var btnAcc = $.find( "[vlc_name='" + btnID + "']" );
        var lnkDecline = $.find( "[vlc_name='" + lnkDeclID + "']" );
        var spanOption = $.find( "[vlc_name='" + spanID + "']" );

        $( btnAcc ).hide();
        $( lnkDecline ).hide();
        $( spanOption ).show();
        if ( strOpt == "Accept" ) {
            $( spanOption ).text( "Friend Accepted" );
            $( spanOption ).val( "Friend Accepted" );
        } else {
            $( spanOption ).val( "Friend Declined" );
            $( spanOption ).text( "Friend Declined" );
        }
    };
    if ( objRespData.Success ) {
        VHM.SocialFrnds.actionPerformed = true;
        makeSelection( strOpt, cfUserID );
    } else {
        alert( "There is some error. Remove was not successful!", { title: 'Something went wrong' } );
    };
};

VHM.SocialFrnds.BackToMyFriends = function () {
    var strFriendsUrl = VHM.Social.appPath + '/Secure/Social/MyFriends.aspx';
    window.location.href = strFriendsUrl;
    //please do not try using VHM.SocialFrnds.loadMyFriends("friends"); unless u apply the pagination for the call. When calling the VHM.SocialFrnds.loadMyFriends() - will load the page with the found query strings for page number, search value or if should load search results or my friends. I did a simply reload of page so that all parameters get reset in the url and if user does a new search and starts navigating through pages they can do so.

};

/************************************************************************************************************************
**********************************************My Profile / My Account settings*******************************************
************************************************************************************************************************/
//TABS
VHM.MyProfile.reloadMyAccount = function (html) {
    var jqo = $("#divTabMyAccount");
    jqo.html(html);
    VHM.MyAccount.page_load();
};
VHM.MyProfile.loadMyProfile = function () {
    $( ".centerLoading" ).fadeIn( "fast" );
    var reloadMyProfile = function ( html ) {
        var jqo = $( "#divTabMyProfile" );
        jqo.html( html );
        if ( typeof VHM.MyProfile.memPicAjaxUpload === "undefined" ) {
            var jqoLnlNewPic = $( "#lnbNewPicture" );
            VHM.MyProfile.memPicAjaxUpload = new AjaxUpload( jqoLnlNewPic, {
                action: VHM.Social.appPath + '/secure/Social/UploadPicture.aspx',
                name: 'memberpic',
                responseType: 'text/html',
                onSubmit: function ( file, extension ) {
                    if ( !( extension && /^(jpg|png|jpeg|gif|bmp)$/.test( extension ) ) ) {
                        alert( 'Error: invalid file format', { title: 'Something went wrong' } );
                        return false;
                    } else {
                        return true;
                    }
                },
                onComplete: function ( file, response ) {
                    if ( response === 'success' ) {
                        VHM.MyProfile.MPuploaded = true;
                    }
                    VHM.MyProfile.onSaveComplete( response );
                },
                onError: function ( file, e ) { alert( 'An error occurred while saving profile information.\n\n Please make sure that profile picture size does not exceed maximum allowed limit.', { title: 'Something went wrong' } ); }
            } );
        }
    };
    $.ajax( {
        url: "../Social/MyProfile.aspx",
        dataType: "html",
        success: function ( data, textStatus ) {
            reloadMyProfile( data );
            $( ".centerLoading" ).fadeOut( "fast" );
        }
    } );
};
VHM.MyProfile.loadMyAccount = function () {
    $(".centerLoading").fadeIn("fast");
    $.ajax({
        url: "../Member/MyAccount.aspx",
        dataType: "html",
        success: function (data, textStatus) {
            VHM.MyProfile.reloadMyAccount(data);
            $(".centerLoading").fadeOut("fast");
        }
    });
};
//BUTTONS - privacy settings
VHM.MyProfile.loadPrivacySett = function (container, showButtons, basicOnly, showBadgesOpt) {
    if (showButtons == null) showButtons = true;
    if (basicOnly == null) basicOnly = false;
    if (container == null) container = '#divLeftContent';
    if (showBadgesOpt == null) { //it's null on Member Details page so we need to grab value of hdnSponsAllowsBadges - which is build in header and lets us know if sponsor has badges enabled
        var hdnHasBadgesEnabled = $('#hdnSponsAllowsBadges').val();
        if (hdnHasBadgesEnabled.toLowerCase() === 'true') {
            showBadgesOpt = true;
        } else {
            showBadgesOpt = false;
        }
    }

    $.ajax({
        url: "../Social/PrivacySettings.aspx",
        data: { ShowSaveCancelBtns: showButtons },
        dataType: "html",
        success: function (data, textStatus) {
            VHM.MyProfile.reloadPrivacySett(data, container, basicOnly, showBadgesOpt);
        }
    });
};
VHM.MyProfile.reloadPrivacySett = function (html, container, basicOnly, showBadgesOpt) {
    var jqo = $(container);
    jqo.html(html);
    if (basicOnly) $("tr.Extended").hide();
    //we shouldn't display the Show Badges option if members are part of a sponsor that doesn't allow badges to be earned!
    if (!showBadgesOpt) $("tr.clsBadges").hide();
    $("#profileLoadingWait").hide();
};
VHM.MyProfile.loadNewsFeedSett = function () {
    $.ajax({
        url: "../Social/NewsFeedSettings.aspx",
        dataType: "html",
        success: function (data, textStatus) {
            VHM.MyProfile.reloadNewsFeedSett(data);
        }
    });
};
VHM.MyProfile.reloadNewsFeedSett = function (html) {
    var jqo = $("#divLeftContent");
    jqo.html(html);
};

VHM.MyProfile.SaveMyNewsfeedSettings = function () {

    var params = { sId: VHM.Social.GetSession(),
        my_usr: $("#rdoMY_USR_ON").is(":checked"), my_tgt: $("#rdoMY_TGT_ON").is(":checked"), my_act: $("#rdoMY_ACT_ON").is(":checked"), my_prf: $("#rdoMY_PRF_ON").is(":checked"),
        friend_usr: $("#rdoFRIEND_USR_ON").is(":checked"), friend_tgt: $("#rdoFRIEND_TGT_ON").is(":checked"), friend_act: $("#rdoFRIEND_ACT_ON").is(":checked"), friend_prf: $("#rdoFRIEND_PRF_ON").is(":checked")
    };

    $.ajax({
        url: VHM.Social.CFServiceUrl + "/SaveCFNewsFeedPrivacy",
        data: params,
        dataType: "html",
        success: function (data, textStatus) {
            VHM.MyProfile.loadMyProfile();
        }
    });

};



VHM.MyProfile.SaveProfile = function (memId) {
    var errorMsg = " ";
    if ($("#Forename").val() == "") { errorMsg += ' - First name\n'; }
    if ($("#Surname").val() == "") { errorMsg += ' - Last name\n'; }
    if (errorMsg != " ") {
        alert("Please complete the following:\n\n" + errorMsg);
        return false;
    };
    VHM.MyProfile.savePersonal(memId, 'false');
};

VHM.MyProfile.UploadImage = function (linkID) {
    if (typeof VHM.MyProfile.memPicAjaxUpload !== "undefined") {
        VHM.MyProfile.memPicAjaxUpload.destroy();
    }
    VHM.MyProfile.memPicAjaxUpload = new AjaxUpload('#' + linkID, {
        action: VHM.Social.appPath + '/secure/Social/UploadPicture.aspx',
        name: 'memberpic',
        //autoSubmit: false,
        responseType: 'text/html',
        onChange: function (file, extension) { },
        onSubmit: function (file, extension) {
            if (!(extension && /^(jpg|png|jpeg|gif|bmp)$/.test(extension))) {
                alert('Error: invalid file format', { title: 'Something went wrong' });
                return false;
            }
        },
        onComplete: function ( file, response ) {
            if ( response == 'success' ) {
                VHM.MyProfile.MPuploaded = true;
            }
             VHM.MyProfile.onSaveComplete(response);
        },
        onError: function (file, e) { alert('An error occurred while saving profile information.\n\n Please make sure that profile picture size does not exceed maximum allowed limit.', { title: 'Something went wrong' }); }
    });
};

VHM.MyProfile.savePic = function (memId) {
    var jqoEditMemProfile = $( '#' + VHM.MyProfile.divEditMemProfile );
    var firstName = $( "#Forename" ).val();
    var lastName = $( "#Surname" ).val();
    var displayName = jqoEditMemProfile.find("[vlc_name='txtDisplayName']").val();
    var motto = jqoEditMemProfile.find("[vlc_name='txtMotto']").val();
    var deletePic = jqoEditMemProfile.find(":hidden").get(0).value;
    VHM.MyProfile.MPMemID = memId;
    VHM.MyProfile.MPdeleted = deletePic;
    VHM.MyProfile.memPicAjaxUpload.setData({ 'firstName': firstName, 'lastName': lastName, 'displayName': displayName, 'motto': motto, 'deletePic': deletePic, 'ts': (new Date()).getTime() });
    if (VHM.MyProfile.memPicAjaxUpload._input.value === '') {
        VHM.MyProfile.MPuploaded = false;
        // there is no file
        $.ajax({
            url: VHM.Social.appPath + '/secure/member/SaveMemberProfile.aspx',
            data: { firstName: firstName, lastName: lastName, displayName: displayName.StripOutHTMLTags(), motto: motto.StripOutHTMLTags(), deletePic: deletePic, ts: ( new Date() ).getTime() },
            dataType: "html",
            success: function (data, textStatus) {
                VHM.MyProfile.onSaveComplete( data );
                VHM.MyProfile.loadMyProfile();
            }
        });
    } else {
        VHM.MyProfile.MPuploaded = true;
        $.ajax({
            url: VHM.Social.appPath + '/secure/member/SaveMemberProfile.aspx',
            data: { firstName: firstName, lastName: lastName, displayName: displayName.StripOutHTMLTags(), motto: motto.StripOutHTMLTags(), deletePic: deletePic, ts: (new Date()).getTime() },
            dataType: "html",
            success: function (data, textStatus) {
                VHM.MyProfile.onSaveComplete(data);
                VHM.MyProfile.loadMyProfile();
            }
        });
//        VHM.MyProfile.memPicAjaxUpload.submit();
    }
};

VHM.MyProfile.SaveOccupation = function () {
    if ($("#OccupationID").val() != undefined) OccupationID = $("#OccupationID").val();
    if ($("#OccupationOther").val() != undefined) {
        OccupationOther = $("#OccupationOther").val();
        OccupationOther = OccupationOther.StripOutHTMLTags();
    }

    $.ajax({
        type: 'POST',
        url: '../webservices/MemberDetails.asmx/SaveOccupation',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: '{ "occupationId": "' + OccupationID + '", "occupationOther": "' + OccupationOther + '" }',
        success: function (data) {
            if (data.d) {
                VHM.MyProfile.loadMyProfile();
            } else {
                alert('Something went wrong. Please try again later.', { title: 'Something went wrong' });
            }
        },
        error: function () {
            alert('Something went wrong. Please try again later.', { title: 'Something went wrong' });
        }
    });
};

VHM.MyProfile.savePersonal = function ( memID, bRelPage ) {
    var Title = "dummy";
    var memberID = parseInt( memID );
    if ( $( "#Title" ).val() != undefined ) Title = $( "#Title " ).val();

    try {
        $.webservice( {
            url: "../webservices/MemberDetails.asmx",
            methodName: "SavePersonalInfo",
            nameSpace: "http://tempuri.org/",
            data: {
                title: Title
            },
            success: function ( data, textStatus ) {
                VHM.MyProfile.savePic( memberID );
                var res = data.getElementsByTagName( "SavePersonalInfoResponse" )[0].firstChild.firstChild.nodeValue;
                if ( res != "true" ) {
                    alert( 'Something went wrong. Please try again later.', { title: 'Something went wrong' } );
                } else {
                    if ( bRelPage == 'true' ) {
                        VHM.MyProfile.loadMyProfile();
                    }
                }
            },
            error: function ( XMLHttpRequest, textStatus, errorThrown ) {
                alert( 'Something went wrong. Please try again later.', { title: 'Something went wrong' } );
            }
        } );
    } catch ( e ) {
        alert( 'Something went wrong. Please try again later.' + e.description, { title: 'Something went wrong' } );
    }
    return false;
};

VHM.MyProfile.onSaveComplete = function ( res ) {
    var lblPicMsgId = $( '#lblPicMsg' ); //lblPicMsgId
    if ( res === 'success' ) {
        lblPicMsgId.html( "(Your picture must be no larger than 1.5MB)" );
        if ( VHM.MyProfile.MPuploaded ) {
            $( '#divDeletePic' ).show();
            var getMid = $( "#imgProfilePic" ).attr( "vhmMid" );
            if ( getMid != null && typeof getMid != "undefined" && getMid != "" ) {
                VHM.MyProfile.MPMemID = getMid;
            }
            $('#imgProfilePic1').attr('src', '/secure/Member/memprofilepic.aspx');
             //$('#imgProfilePic1').attr('src', VHM.Social.ImageSourcePath + VHM.MyProfile.MPIGuid );
            $( ":hidden", $( '#' + VHM.MyProfile.divEditMemProfile ) ).get( 0 ).value = "false";
        } else if ( VHM.MyProfile.MPdeleted !== "false" ) {
            $( '#divDeletePic' ).hide();
        }
        
    } else if ( res === 'The picture excides max size limit of 1.5MB' ) {
        lblPicMsgId.html( "<span style='background-color:yellow;'>Your picture is larger then 1.5MB. Please select another picture.</span>" );
    } else if ( res == "Upload file too large" ) {
        lblPicMsgId.html( "<span style='background-color:yellow;'>Your picture is larger then 1.5MB. Please select another picture.</span>" );
    } else if ( res === 'fail' ) {
        alert( 'Error occured while saving profile changes. Please try again later.', { title: 'Something went wrong' } );
    }
};

VHM.MyProfile.delProfPic = function (jqoThirdPop) {
    $('#hdnDltProfilePic').val('true');
    $('#imgProfilePic1').attr('src', VHM.Social.appPath + '/images/challenges/default_profile.gif');
    VHM.Social.CloseModal('divRemovePicture');
};

VHM.MyProfile.showOtherOccupation = function (field) {
    with (document.forms['frmMyProfile']) {
        if (field == "Occupation") {
            if (eval(field + "ID.options[" + field + "ID.selectedIndex].text == 'Other'"))
                $("#Other" + field + "Div").show();
            else
                $("#Other" + field + "Div").hide();
        } else {
            if (eval(field + "Other.checked"))
                $("#Other" + field + "Div").show();
            else
                $("#Other" + field + "Div").hide();
        }
    }
};

VHM.MyProfile.saveProfPrivacySett = function (isFromProfile) {
    VHM.MyProfile.savePrivateSett(isFromProfile);
};

VHM.MyProfile.saveProfSrcbDataCallBack = function (soapResult) {
    var strRespData = soapResult;
    var objRespData = eval('(' + strRespData + ')');
    if (objRespData.Success) {
        //if success then continue saving all other VHM Settings - call VHM.MyProfile.savePrivateSett();
        VHM.MyProfile.savePrivateSett(true);
    } else {
        alert("There is some error. Could not save data!", { title: 'Something went wrong' });
    };
};

VHM.MyProfile.savePrivateSett = function ( isFromProfile ) {
    var reloadMyProfile;
    if ( typeof isFromProfile == "undefined" || isFromProfile == "" ) {
        reloadMyProfile = true;
    } else {
        reloadMyProfile = isFromProfile;
    }
    var sNickName = "", sGender = "", sAge = "", sCity = "", sTotSteps = "", sStepAvg = "", sSponsor = "",
    sLastUpload = "", sHistory = "", sOrganiz = "", sOccup = "", sGoals = "", sInterests = "", deletePic = 'false', sBadgesOption = "", sPrivacy = "";

    if ( $( "#showNickName input:checked" ).val() != undefined ) sNickName = $( "#showNickName input:checked" ).val();
    if ( $( "#showGender input:checked" ).val() != undefined ) sGender = $( "#showGender input:checked" ).val();
    if ( $( "#showAge input:checked" ).val() != undefined ) sAge = $( "#showAge input:checked" ).val();
    if ( $( "#showCity input:checked" ).val() != undefined ) sCity = $( "#showCity input:checked" ).val();
    if ( $( "#showTotalSteps input:checked" ).val() != undefined ) sTotSteps = $( "#showTotalSteps input:checked" ).val();
    if ( $( "#showAvrgSteps input:checked" ).val() != undefined ) sStepAvg = $( "#showAvrgSteps input:checked" ).val();
    if ( $( "#showLastGZUpload input:checked" ).val() != undefined ) sLastUpload = $( "#showLastGZUpload input:checked" ).val();
    if ( $( "#showChallenges input:checked" ).val() != undefined ) sHistory = $( "#showChallenges input:checked" ).val();
    if ( $( "#showSponsors input:checked" ).val() != undefined ) sSponsor = $( "#showSponsors input:checked" ).val();
    if ( $( "#showOccupation input:checked" ).val() != undefined ) sOccup = $( "#showOccupation input:checked" ).val();
    if ( $( "#showGoals input:checked" ).val() != undefined ) sGoals = $( "#showGoals input:checked" ).val();
    if ( $( "#showInterests input:checked" ).val() != undefined ) sInterests = $( "#showInterests input:checked" ).val();
    if ( $( "#showBadges input:checked" ).val() != undefined ) sBadgesOption = $( "#showBadges input:checked" ).val();
    sPrivacy = $( "#rdo_Srchability input:checked" ).val();
    try {
        var mvcURL = "/v2/member/SavePrivateSettings";

        $.ajax( {
            url: mvcURL,
            type: "POST",
            data: {
                Privacy: sPrivacy,
                ShowNickname: sNickName,
                ShowGender: sGender,
                ShowAge: sAge,
                ShowCity: sCity,
                ShowState: sTotSteps,
                ShowTotalSteps: sTotSteps,
                ShowStepsAvg: sStepAvg,
                ShowLastGZUpload: sLastUpload,
                ShowChallengeHistory: sHistory,
                ShowSponsors: sSponsor,
                ShowGoals: sGoals,
                ShowOccupation: sOccup,
                ShowInterests: sInterests,
                ShowBadges: sBadgesOption,
                ShowOrganization: sOrganiz
            },
            dataType: "json",
            success: function () {
                if ( reloadMyProfile ) {
                    VHM.MyProfile.loadMyProfile();
                }
            },
            error: function () {
                alert( "Something went wrong. Please try again later.", { title: 'Something went wrong' } );
            }
        } );
    } catch ( e ) {
        alert( 'Something went wrong. Please try again later.', { title: 'Something went wrong' } );
    }
};

VHM.MyProfile.OpenEditGoalsInterest = function (strType, dialogID) {
    VHM.MyProfile.GetGoalsInterests(strType, "predefined");
    VHM.MyProfile.GetGoalsInterests(strType, "custom");
};

VHM.MyProfile.GetGoalsInterests = function (strType, strWhat) {
    var paramAcc = { strType: strType, sId: VHM.Social.GetSession(), strWhat: strWhat };
    var sParam = JSON.stringify( paramAcc );
    $.ajax( {
        cache: false,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: VHM.Social.CFServiceUrl + "/GetMemGoalsAndInterests",
        data: sParam,
        dataType: "json",
        success: function ( responseData ) {
            if ( responseData != null || responseData.d != null) {
                VHM.MyProfile.GetMemberGoalsInterestsDataCallback( responseData.d, strType, strWhat );
            } 
        },
        error: function ( xhr, ajaxOptions, error ) {
            alert( 'Something went wrong. Please try again later. ', { title: 'Something went wrong' } );
        }
    } );
};

VHM.MyProfile.GetMemberGoalsInterestsDataCallback = function ( responseData, strType, strWhat ) {
    var objRespData = $.parseJSON( responseData ); // eval( '(' + soapResult + ')' );
    var goalsInterests = objRespData;
    if (goalsInterests.length > 0) {
        if (strWhat == "custom") {
            VHM.MyProfile.buildCustomGoalsInterests(goalsInterests, strType);
        } else if (strWhat == "predefined") {
            VHM.MyProfile.buildPredefGoalsInterests(goalsInterests, strType);
        };
    } else {
        if (strType == 'goal') {
            $("#moreGoals").html("");
        }
        else if (strType == 'interest') {
            $("#moreInterests").html("");
        }
    };
};

VHM.MyProfile.buildCustomGoalsInterests = function (userDefGlsIntrs, strType) {
    var moreGoalsMarkup = "<li class=\"goalsListItems\"><input type=\"checkbox\" id=\"GoalInterest${MemberGoalsInterestsID}\" name=\"${name}\" onchange=\"javascript:VHM.MyProfile.chkIfCanStillSel('${Type}', this);\" value=\"${MemberGoalsInterestsID}\" ${isGoalSelected} />&nbsp;<label for=\"GoalInterest${MemberGoalsInterestsID}\" class=\"wrapText\">${filteredName}&nbsp;&nbsp;</label><a class=\"deleteHREF\" href=\"#\" onclick='javascript:VHM.MyProfile.deleteGoalInterest(\"${Type}\",\"${MemberGoalsInterestsID}\"); return false;' >X</a><br /></li>";

    // Compile the markup as a named template
    if (strType == 'goal') {
        $("#moreGoals").html("");
        $.template("addMoreGoalsTempl", moreGoalsMarkup);
        $.tmpl("addMoreGoalsTempl", userDefGlsIntrs).appendTo("#moreGoals");
    } else if (strType == 'interest') {
        $("#moreInterests").html("");
        $.template("addMoreInterestsTempl", moreGoalsMarkup);
        $.tmpl("addMoreInterestsTempl", userDefGlsIntrs).appendTo("#moreInterests");
    }
};

VHM.MyProfile.enableButton = function (obj, btnID, maxLength) {
    var strOtherGoal = obj.value;
    strOtherGoal = strOtherGoal.trim();

    var jqoBtn = $("#" + btnID);
    if (strOtherGoal.length > 2) {
        jqoBtn.removeAttr("disabled");
    } else {
        jqoBtn.prop("disabled", true);
    };
    if (strOtherGoal.length > maxLength) {
        obj.value = obj.value.substring(0, maxLength);
    }
};

VHM.MyProfile.disableButton = function (obj) {
    var jqoBTN = $("#" + obj.id);
    jqoBTN.prop("disabled", true);
};

VHM.MyProfile.addGoalInterest = function (type, txtBoxID) {
    var jqoTxtBox = $("#" + txtBoxID);
    var strGoalInterest = jqoTxtBox.val();
    strGoalInterest = strGoalInterest.trim();
    strGoalInterest = strGoalInterest.StripOutHTMLTags();
    strGoalInterest = strGoalInterest.htmlEncode();
    
    var jqoGoalIntrList;
    var countGoalsIntr;

    if (type == 'goal') {
        jqoGoalIntrList = $("#moreGoals");
    } else if (type == 'interest') {
        jqoGoalIntrList = $("#moreInterests");
    }

    var chkCustG = jqoGoalIntrList.find("INPUT[type='checkbox']");
    countGoalsIntr = chkCustG.length;

    if (countGoalsIntr < 9) {
        $.ajax({
            type: 'POST',
            url: '../../webservices/WSVHMSocial.asmx/AddMemberGoalInterest',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: '{ "type": "' + type + '", "description": "' + strGoalInterest + '" }',
            success: function (data) {
                if (data.d === true) {
                    VHM.MyProfile.GetGoalsInterests(type, 'custom');
                    jqoTxtBox.val("");
                    var errID = "error" + type;
                    var errSpan = $("#" + errID);
                    errSpan.hide();
                }
            },
            error: function () {
                alert('Something went wrong. Please try again later.', { title: 'Something went wrong' });
            }
        });
    } else {
        var errID = "error" + type;
        var errSpan = $("#" + errID);
        errSpan.show();
    }
};

VHM.MyProfile.deleteGoalInterest = function (strType, memberGoalInterestId) {
    $.ajax({
        type: 'POST',
        url: '../../webservices/WSVHMSocial.asmx/DeleteMemberGoalInterest',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: '{ "memberGoalInterestId": ' + memberGoalInterestId + ' }',
        success: function (data) {
            if(data.d === true) {
                VHM.MyProfile.GetGoalsInterests(strType, 'custom');
                var errID = "error" + strType;
                var errSpan = $("#" + errID);
                errSpan.hide();
            }
        },
        error: function () {
            alert('Something went wrong. Please try again later.', { title: 'Something went wrong' });
        }
    });
};

VHM.MyProfile.SaveSelGoalsInterests = function (strType, maxAllowed) {
    var jqoPrefGIList; //predefined goals/interests
    var jqoCustomGIList; //custom goals/interests
    var countGoalsIntr;
    if (strType == 'goal') {
        jqoPrefGIList = $("#ulPredefinedGoals"); //the list holding all predefined goals
    } else if (strType == 'interest') {
        jqoPrefGIList = $("#ulPredefinedIntersts"); //the div holding all predefined interests
    }
    var chkPredef = jqoPrefGIList.find("INPUT[type='checkbox']");
    var chkdPredChecked = chkPredef.filter(':checked');

    if (strType == 'goal') {
        jqoCustomGIList = $("#moreGoals"); //the list holding all custom goals
    } else if (strType == 'interest') {
        jqoCustomGIList = $("#moreInterests"); //the list holding all custom interests
    }
    var chkCustom = jqoCustomGIList.find("INPUT[type='checkbox']");
    var chkCustomChecked = chkCustom.filter(':checked');
    countGoalsIntr = chkdPredChecked.length + chkCustomChecked.length;
    var predefinedValues = {};
    var customValues = {};
    if (countGoalsIntr <= maxAllowed) {
        chkPredef.each(function (index) {//for predefined Goals/Interests pass in PredefinedGoalsInterestsID          
            var predefID = $(this).val();
            var predefDescr = $(this)[0].name;
            var isChckd = ($(this)[0].checked) ? "1" : "0";
            predefinedValues[index] = {};
            predefinedValues[index]['PredefinedGoalsInterestsID'] = predefID;
            predefinedValues[index]['Selected'] = isChckd;
        });
        chkCustom.each(function (index) {//for custom Goals/Interests pass in MemberGoalsInterestsID  
            var customID = $(this).val();
            var customDescr = $(this)[0].name;
            var isChckd = ($(this)[0].checked) ? "1" : "0";
            customValues[index] = {};
            customValues[index]["MemberGoalsInterestsID"] = customID;
            customValues[index]["Selected"] = isChckd;
        });

        var replacer = function (key, value) {
            if (typeof value === 'number' && !isFinite(value)) {
                return String(value);
            }
            return value;
        };
        var myJSONtxtPred = JSON.stringify(predefinedValues, replacer);
        var myJSONtxtCustom = JSON.stringify(customValues, replacer);
        
        VHM.MyProfile.UpdateGoalsInterest(myJSONtxtPred, myJSONtxtCustom, strType);

    } else {
        alert("You have selected too many " + strType + "s! You can only select " + maxAllowed + "!", { title: ' ' });
    }
};

VHM.MyProfile.UpdateGoalsInterest = function (perdefVals, customVals, strType) {
    var paramInp = { sId: VHM.Social.GetSession(), jsonPredefined: perdefVals, jsonCustom: customVals };
    var sParam = JSON.stringify( paramInp );
    $.ajax( {
        cache: false,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: VHM.Social.CFServiceUrl + "/UpdateMemberGoalInterest",
        data: sParam,
        dataType: "json",
        success: function ( responseData ) {
            if ( responseData != null || responseData.d != null ) {
                var objRespData = $.parseJSON( responseData.d );
                if (!objRespData.Success) {
                    alert("An error has occured while saving your " + strType + "! Please try again later!", { title: 'Something went wrong' });
                }else {
                    VHM.MyProfile.loadMyProfile();
                }
            }
        },
        error: function ( xhr, ajaxOptions, error ) {
            alert( 'Something went wrong. Please try again later. ', { title: 'Something went wrong' } );
        }
    } );
};

VHM.MyProfile.chkIfCanStillSel = function (strType, obj) {
    var jqoPrefGIList; //predefined goals/interests
    var jqoCustomGIList; //custom goals/interests
    var countGoalsIntr;
    var maxAllowed;
    if (strType == 'goal') {
        jqoPrefGIList = $("#ulPredefinedGoals"); //the list holding all predefined goals
        maxAllowed = 3;
    } else if (strType == 'interest') {
        jqoPrefGIList = $("#ulPredefinedIntersts"); //the div holding all predefined interests
        maxAllowed = 5;
    }
    var chkPredef = jqoPrefGIList.find("INPUT[type='checkbox']");
    var chkdPredChecked = chkPredef.filter(':checked');

    if (strType == 'goal') {
        jqoCustomGIList = $("#moreGoals"); //the list holding all custom goals
    } else if (strType == 'interest') {
        jqoCustomGIList = $("#moreInterests"); //the list holding all custom interests
    }
    var chkCustom = jqoCustomGIList.find("INPUT[type='checkbox']");
    var chkCustomChecked = chkCustom.filter(':checked');
    countGoalsIntr = chkdPredChecked.length + chkCustomChecked.length;
    if (countGoalsIntr > maxAllowed) {
        obj.checked = false;
        alert("You have exceeded the number of " + strType + "s.", { title: ' ', redMessageText: ' ' });
        return false;
    }
};

VHM.MyProfile.buildPredefGoalsInterests = function (predefGlsIntrs, strType) {
    var moreGoalsintrMarkup = "<li class=\"goalsListItems\"><label><input type=\"checkbox\" onchange=\"javascript:VHM.MyProfile.chkIfCanStillSel('${Type}', this);\" name=\"${name}\" value=\"${PredefinedGoalsInterestsID}\" ${isGoalSelected} />&nbsp;${filteredName}</label><br /></li>";
    // Compile the markup as a named template
    if (strType == 'goal') {
        $("#ulPredefinedGoals").html("");
        $.template("predefGoalsIntrTempl", moreGoalsintrMarkup);
        $.tmpl("predefGoalsIntrTempl", predefGlsIntrs).appendTo("#ulPredefinedGoals");
    } else if (strType == 'interest') {
        $("#ulPredefinedIntersts").html("");
        $.template("predefGoalsIntrTempl", moreGoalsintrMarkup);
        $.tmpl("predefGoalsIntrTempl", predefGlsIntrs).appendTo("#ulPredefinedIntersts");
    }
};

/************************************************************************************************************************
**********************************************To be placed in right place*******************************************
************************************************************************************************************************/
var closeDiv = function () {
    dialog.dialog('close');
};

var cleanUserProf = function (dialog) {
    var usrName = dialog.find("[vlc_name='divName_Loc']");
    var usrMotto = dialog.find("[vlc_name='userMotto']");
    var usrLocOrg = dialog.find("[vlc_name='userOrganizOrLoc']");
    var divNN = dialog.find("[vlc_name='divProfNickName']");
    var divGend = dialog.find("[vlc_name='divProfGender']");
    var divAge = dialog.find("[vlc_name='divProfAge']");
    var divPrCity = dialog.find("[vlc_name='divProfCity']");
    var divSpons = dialog.find("[vlc_name='divProfSponsors']");
    var divTotSt = dialog.find("[vlc_name='divProfTotSteps']");
    var divAvSt = dialog.find("[vlc_name='divProfAvrgSteps']");
    var divLastUpld = dialog.find("[vlc_name='divProfLastUpload']");
    var divChalHist = dialog.find("[vlc_name='divProfClngHist']");
    var divUserOcc = dialog.find("[vlc_name='divProfOccupation']");
    var divUserGoals = dialog.find("[vlc_name='divProfGoals']");
    var divUserInterests = dialog.find("[vlc_name='divProfInterests']");

    usrName.html(""); usrMotto.html(""); usrLocOrg.html("");
    var nameLoc1 = dialog.find("[vlc_name='divName_Loc']");

    var usrNickN = dialog.find("[vlc_name='lblNickname']");
    var usrGender = dialog.find("[vlc_name='labelGender']");
    var usrAge = dialog.find("[vlc_name='labelAge']");
    var usrCity = dialog.find("[vlc_name='labelCity']");
    var usrSponsor = dialog.find("[vlc_name='labelSponsor']");
    var usrTotSt = dialog.find("[vlc_name='labelTotalSt']");
    var usrAvrgSt = dialog.find("[vlc_name='labelAverageSt']");
    var usrLastUpld = dialog.find("[vlc_name='labelLastUpld']");
    var usrChalHist = dialog.find("[vlc_name='labelHistory']");
    var usrOccup = dialog.find("[vlc_name='labelOccupat']");
    var usrGoals = dialog.find("[vlc_name='labelGoal']");
    var usrIntrsts = dialog.find("[vlc_name='labelInterest']");

    var hrefShowMore = dialog.find("[vlc_name='hrefShowMore']"); hrefShowMore.hide();
    var hrefShowTopFive = dialog.find("[vlc_name='hrefHideAll']"); hrefShowTopFive.hide();
    var totBadgesErnd = dialog.find("[vlc_name='totalNoBadges']"); totBadgesErnd.html("");
    divNN.hide(); divGend.hide(); divAge.hide(); divPrCity.hide(); divSpons.hide(); divTotSt.hide();
    divAvSt.hide(); divLastUpld.hide(); divChalHist.hide(); divUserGoals.hide(); divUserOcc.hide(); divUserInterests.hide();
    usrNickN.html(""); usrGender.html(""); usrAge.html(""); usrCity.html(""); usrSponsor.html(""); usrTotSt.html(""); usrAvrgSt.html("");
    usrChalHist.html(""); usrLastUpld.html(""); usrOccup.html(""); usrGoals.html(""); usrIntrsts.html("");
    nameLoc1.html("");
};

var reloadPage = function () {
    if (VHM.SocialFrnds.actionPerformed) {
        VHM.SocialFrnds.actionPerformed = false;
        window.location.reload();
    }
};
var endTime = null;
var timerID = null;

function StartVHMWidgetTimer() {
    var now = new Date().getTime();
    endTime = now;
}
/****************************************************************************************************************************************************
***********The below functions will be used to determine the scroll position on page. These are browser compatible***********************************
*****************************************************************************************************************************************************/
function f_clientHeight() {
    return f_filterResults(
		window.innerHeight ? window.innerHeight : 0,
		document.documentElement ? document.documentElement.clientHeight : 0,
		document.body ? document.body.clientHeight : 0
	);
};
function f_scrollTop() {
    return f_filterResults(
		window.pageYOffset ? window.pageYOffset : 0,
		document.documentElement ? document.documentElement.scrollTop : 0,
		document.body ? document.body.scrollTop : 0
	);
};
function f_filterResults(n_win, n_docel, n_body) {
    var n_result = n_win ? n_win : 0;
    if (n_docel && (!n_result || (n_result > n_docel)))
        n_result = n_docel;
    return n_body && (!n_result || (n_result > n_body)) ? n_body : n_result;
};
