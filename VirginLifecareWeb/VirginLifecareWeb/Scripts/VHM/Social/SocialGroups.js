﻿var VHM = VHM || {};
VHM.Social = VHM.Social || {};
VHM.SocialGroups = VHM.SocialGroups || {};
VHM.SocialGroups.Invite = VHM.SocialGroups.Invite || {};
VHM.SocialGroups.nameSpace = "http://VirginHealthMiles.com/VirginLifeCare/WebServices/";
VHM.Social.NewsfeedFailMessage = "<div class='nfFailMessage'>Seems like there's a problem loading your News Feed. Try <a href='#'>refreshing the page.</a></div>";
VHM.Social.SearchButtonClicked = false;
VHM.Social.StripOutHTMLTags = function (text) {
    return text.replace(/<\/?[^>]+>/gi, '');
};
VHM.Social.GetSession = function () {
    if (VHM.Social.SessionID == null) {
        VHM.Social.SessionID = VHM.Core.ReadCookie("SessionID");
    }
    return VHM.Social.SessionID;
};
$(function () {
    VHM.Social.CFServiceUrl = "../../webservices/WSVHMSocial.asmx";
    VHM.Social.appPath = "";
    VHM.SocialGroups.showMoreSgstPageNo = 1;
    VHM.SocialGroups.NextSuggFriends = 0;
    VHM.SocialGroups.sponsorSpaceName = $("#hdnSponsorID").val();

    $(document).on("click", "div.nfFailMessage a", function () {
        window.location.reload();
    });
});

$.extend({
    getUrlVars: function () {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        if (typeof hashes != "undefined") {
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
        }
        return vars;
    },
    getUrlVar: function (name) {
        return $.getUrlVars()[name];
    }
});


VHM.SocialGroups.GetPublicGroup = function (groupID, dialogID) {
    var jqoDialog;
    if (dialogID.id) {
        jqoDialog = $("#" + dialogID.id);
    } else {
        jqoDialog = $("#" + dialogID);
    };

    var jqoloading = jqoDialog.find("[vlc_name='vlc_GrpLoading']");
    var jqoButtonsOpt = jqoDialog.find("[vlc_name='divButtons']");
    var jqoGrpPublicInfo = jqoDialog.find("[vlc_name='divPublicGrpProfile']");
    
    jqoloading.show();
    jqoButtonsOpt.hide();
    jqoGrpPublicInfo.hide();
    
    VHM.SocialGroups.cleanGroupProf(jqoDialog);
    
    var params = { sId: VHM.Social.GetSession(), grId: groupID, appPath: VHM.Social.appPath };
    var sData = JSON.stringify(params);
    $.ajax({
        cache: false,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: VHM.Social.CFServiceUrl + "/GetPublicGroupProfile",
        data: sData,
        dataType: "json",
        success: function (responseData, textStatus) {
            if (responseData != null && responseData.d != null) {
                VHM.SocialGroups.GetPublicGroupDataCallback(responseData.d, jqoDialog);

                jqoGrpPublicInfo.show();
                jqoButtonsOpt.show();
                jqoloading.hide();
            };
        },
        error: function (xhr, ajaxOptions, error) { }
    });
};
VHM.SocialGroups.GetPublicGroupDataCallback = function (soapResult, dialog) {
    var strRespData = soapResult;
    var objRespData = eval('(' + strRespData + ')');
    //set up the profile image source URL; name + org or Loc 
    var imgProf = dialog.find("[vlc_name='VHMimgProf']");
    imgProf.attr("src", objRespData.ProfilePicturePath);

    var groupName = dialog.find("[vlc_name='txtGroupName']");
    var grpCateg = dialog.find("[vlc_name='spanGroupCateg']");
    var grpCreator = dialog.find("[vlc_name='spanCreatorName']");
    var grpDescription = dialog.find("[vlc_name='spanDescription']");

    if (objRespData.GroupID > 0) {
        groupName.html(objRespData.GroupName);
        strCateg = "<span class=\"small\">\"" + objRespData.GroupCategory + "\"</span>";
        grpCateg.html(strCateg);
        grpCreator.html(objRespData.AdministratorName);
        grpDescription.html(objRespData.GroupDescription);

        var grpMembers = objRespData.GroupMembers;
        var membHTML = "";
        var membersList = dialog.find("[vlc_name='ulGroupMembers']");
        if (grpMembers != "" && objRespData.TotalGroupMembers > 0) {
            membHTML = VHM.SocialGroups.buildGroupMembers(grpMembers);
        }

        membersList.html(membHTML);

        $(".groupMemberCount").each(function () {
            var memberCnt = "";
            var intMembCnt = parseInt(objRespData.TotalGroupMembers);
            if (intMembCnt == 1) {
                memberCnt = "1 Member";
                $(this).html(memberCnt);
            } else {
                memberCnt = objRespData.TotalGroupMembers + " Members";
                $(this).html(memberCnt);
            }
        });

        var btnProceed = dialog.parent().find(".primary");
        var role = objRespData.GroupRole;
        
        if (role == 'invited') {
            $(btnProceed).val("Accept Invitation");
            
            btnProceed.click(function () {
                VHM.SocialGroups.JoinGroup(objRespData.GroupID);
                $(btnProceed).attr("disabled", "true");
            });
        } else if (role == 'none') {
            $(btnProceed).val("Join this Group");
            
            btnProceed.click(function () {
                $(".centerLoading").fadeIn("fast");
                $(btnProceed).attr("disabled", "true");
                VHM.SocialGroups.JoinGroup(objRespData.GroupID);
                $(".centerLoading").fadeOut("fast");
            });
        } else {
            btnProceed.click(function () {
                var joinedGroupURL = VHM.Social.appPath + "/Secure/Social/ShowGroup.aspx?gID=" + objRespData.GroupID;
                window.location.href = joinedGroupURL;
            });
        }
    } else {
        $("#divPublicGrpProfile").html("There was a problem retrieving the group information. Refreshing the page and trying again may help.")
            .parent().find(".actionButtons").remove();
    }
};
VHM.SocialGroups.JoinGroup = function (groupID) {
    var pl = new SOAPClientParameters();
    var joinGroupResult = function (soapResult) {
        var objRespData = eval('(' + soapResult + ')');
        if (objRespData.Success) {
            var joinedGroupURL = VHM.Social.appPath + "/Secure/Social/ShowGroup.aspx?gID=" + groupID;
            window.location.href = joinedGroupURL;
        } else {
            alert("There is some error. Operation was not successful!", { title: 'Something went wrong' });
        }
    };
    pl.add("sId", VHM.Social.GetSession());
    pl.add("GroupID", groupID);
    SOAPClient.invoke(VHM.Social.CFServiceUrl, "JoinGroup", pl, false, function (soapResult, responseXML) { joinGroupResult(soapResult); });
};

VHM.SocialGroups.DeclineGroupInvitation = function (groupID) {
    
    var declineGroupInvResult = function(soapResult) {
        var objRespData = $.parseJSON(soapResult);
        if (objRespData.Success) {
            VHM.SocialGroups.makeSelection('Decline', groupID);
        } else {
            alert("There is some error. Operation was not successful!", { title: 'Something went wrong' });
        }
    };
    var params = { sId: VHM.Social.GetSession(), GroupID: groupID };
    var sData = JSON.stringify(params);
    $.ajax({
        cache: false,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: VHM.Social.CFServiceUrl + "/DeclineGroupInvitation",
        data: sData,
        dataType: "json",
        success: function(data) {
            if (data != null && data.d != null) {
                declineGroupInvResult(data.d);
            }
        },
        error: function(xhr, ajaxOptions, error) {
            alert('Something went wrong. Please try again later. ', { title: 'Something went wrong' });
        }
    });
};

VHM.SocialGroups.makeSelection = function (strOpt, groupID) {
    var btnID = "btnAcceptGrInv_" + groupID;
    var lnkDeclID = "hrefDecline_" + groupID;
    var spanID = "spSelectionVal_" + groupID;

    var btnAcc = $.find("[vlc_name='" + btnID + "']");
    var lnkDecline = $.find("[vlc_name='" + lnkDeclID + "']");
    var spanOption = $.find("[vlc_name='" + spanID + "']");

    $(btnAcc).hide();
    $(lnkDecline).hide();
    $(spanOption).show();
    if (strOpt == "Accept") {
        $(spanOption).text("Invitation Accepted");
        $(spanOption).val("Invitation Accepted");
    } else {
        $(spanOption).val("Invitation Declined");
        $(spanOption).text("Invitation Declined");
    }
};

VHM.SocialGroups.buildGroupMembers = function (grpMembers) {
    var mebersMarkup = "<li class=\"liGrpMembers\"><img alt=\"${MemberName}\" class=\"smallThImg\" src=\"${ProfImage}\" /></li>";
    var allGrpMemb = eval(grpMembers);
    $("#ulGroupMembers").html("");
    $.template("grpMembersTempl", mebersMarkup);
    $.tmpl("grpMembersTempl", allGrpMemb).appendTo("#ulGroupMembers");
    var strHTML = $("#ulGroupMembers").html();
    return strHTML;
};

VHM.SocialGroups.enableDisableBtn = function (strText, btnID) {
    var btn = $("#" + btnID);
    strText = strText.trim();
    strText = strText.StripOutHTMLTags();
    if (strText.length > 2) {
        $(btn).removeAttr("disabled");
    } else {
        $(btn).attr("disabled", "true");
    };
};

VHM.SocialGroups.validateEntry = function (jqoPopUp) {
    var msg = "";
    var txtGrpName = jqoPopUp.find("[vlc_name='txtGroupName']");
    var txtGrpDescr = jqoPopUp.find("[vlc_name='txtDescr']");
    var strGrpDescr = $(txtGrpDescr).val();
    strGrpDescr = strGrpDescr.trim();
    strGrpDescr = strGrpDescr.StripOutHTMLTags();
    var strGrpName = $(txtGrpName).val();
    strGrpName = strGrpName.trim();
    strGrpName = strGrpName.StripOutHTMLTags();
    var selCateg = $(".ddlCrEdCategory").val();

    if (strGrpName.length < 4) {
        msg += "Please enter a Group name that contains more than 3 characters! <br />";
    }
    if (!isNaN(strGrpName)) {
        msg += "Your Group name must contain at least one letter.";
    }
    if (strGrpDescr.length < 2) {
        msg += "Please enter a group description that contains more than 1 character! <br />";
    }
    if (selCateg == 'Select one') {
        msg += "Please select a group category for your group! <br />";
    }
    return msg;
};

VHM.SocialGroups.cleanGroupProf = function ( jqoDialog ) {
    var imgProf = jqoDialog.find( "[vlc_name='groupImgProf']" );
    var grpName = jqoDialog.find( "[vlc_name='txtGroupName']" );
    var grpCateg = jqoDialog.find( "[vlc_name='spanGroupCateg']" );
    var grpCreator = jqoDialog.find( "[vlc_name='spanCreatorName']" );
    var grpDescript = jqoDialog.find( "[vlc_name='spanDescription']" );
    var ulMembers = jqoDialog.find( "[vlc_name='ulGroupMembers']" );
    var btnProceed = jqoDialog.find( "[vlc_name='btnAction']" );
    var defProfPicURL = VHM.Social.appPath + '/images/Social/img-no-group.png';
    $( imgProf ).attr( 'src', defProfPicURL );
    grpName.html( "" ); grpCateg.html( "" ); grpCreator.html( "" ); grpDescript.html( "" ); ulMembers.html( "" );
    $( btnProceed ).val( "" );
    btnProceed.unbind( 'click' ); btnProceed.removeAttr( 'onclick' );
    $( btnProceed ).removeAttr( "disabled" );
};

VHM.SocialGroups.BackToMyGroups = function () {
    var strMyGroupsURL = VHM.Social.appPath + '/v2/Social/MyGroups';
    //use href rather than replace. The replace function is replacing the curent url in browser history so the "Back" button doesn't work.
    //window.location.replace(strMyGroupsURL);
    window.location.href = strMyGroupsURL;
};

var endTime = null;
var timerID = null;

function StartVHMWidgetTimer() {
    var now = new Date().getTime();
    endTime = now;
}
String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g, "");
};
/*****************************************************************************************************************************************
**********************************************SOCIAL BASICS- GROUP ENHANCEMENTS***********************************************************/
VHM.SocialGroups.LeaveAGroup = function ( groupId ) {
    var Param = { sId: VHM.Social.GetSession(), GroupID: groupId };
    var sParams = JSON.stringify( Param );
    $.webservice( {
        requestType: "httpget",
        url: VHM.Social.CFServiceUrl,
        data: Param,
        contentType: "text/xml",
        dataType: "xml",
        nameSpace: VHM.SocialGroups.nameSpace,
        methodName: "LeaveGroup",
        success: function ( data, textStatus ) {
            window.location.href = VHM.Social.appPath + '/secure/Social/MyGroups.aspx';
        }
    } );
};
VHM.SocialGroups.BecomeGroupAdmin = function ( groupID ) {
    var Param = { sId: VHM.Social.GetSession(), groupId: groupID };
    var sParams = JSON.stringify( Param );
    $.webservice( {
        requestType: "httpget",
        url: VHM.Social.CFServiceUrl,
        data: Param,
        contentType: "text/xml",
        dataType: "xml",
        nameSpace: VHM.SocialGroups.nameSpace,
        methodName: "BecomeGroupAdmin",
        success: function ( data, textStatus ) {
            window.location.reload();
        }
    } );
};

/*****************************************************************************************************************************************
************************************GROUP INVITATIONS CONTROL*****************************************************************************/
(function (grinv) {

    var searchUrl;
    var $divSearchLstWrap;
    var $txt;
    var $ul;

    var createSearchResDiv = function () {
        return $divSearchLstWrap = $ul.parent().after(
            '<div class="selectBoxSearchResWrap" >' +
                '<div class="selectBoxSearchRes">' +
                '</div>' +
            '</div>').next();
    };
    var insertHtmlInSearchResDiv = function (html) {
        if (!$divSearchLstWrap) {
            $divSearchLstWrap = createSearchResDiv();
        }

        $divSearchLstWrap.find('.selectBoxSearchRes').empty().html(html);

        var h = $divSearchLstWrap.find('.selectBoxSearchRes').height();
        $divSearchLstWrap.height(h > 135 ? 150 : (h + 15));
    };
    var removeSearchResDiv = function () {
        if ($divSearchLstWrap) {
            $divSearchLstWrap.remove();
            $divSearchLstWrap = null;
        }
    };
    var unselectSearchResItem = function (o) {
        $(o).removeClass('divSearchLstItemOver');
    };
    var selectSearchResItem = function (o) {
        $(o).addClass('divSearchLstItemOver');
    };
    var onMouseOverSearchRes = function () {
        $txt.unbind('blur');
    };
    var onMouseOutSearchRes = function () {
        $txt.bind('blur', function () {
            removeSearchResDiv();
        });
    };
    var onMouseOverSearchResItem = function () {
        selectSearchResItem(this);
    };
    var onMouseOutSearchResItem = function () {
        unselectSearchResItem(this);
    };
    var onClickSearchResItem = function () {
        removeSearchResDiv();
        resetTxt();
        $ul.find('li').last().
            before('<li class="liInvteeLstItem" user_id="' + this.getAttribute('user_id') + '">' +
                $(this).text() +
                ' <a href="#">&nbsp;&nbsp;&nbsp;</a></li>').
            prev().find('a').click(
                function () {
                    removeItem(this);
                    $txt.focus();
                    return false;
                });
        $txt.focus();
    };
    var getPosibleInvites_callback = function (d) {
        if (d.success && d.rows.length && d.rows[0].DisplayName) {
            var html = '';
            var invitees = getInvitees();
            var f;
            for (var i = 0, l = d.rows.length; i < l; i++) {
                f = true;
                for (var k = 0, linv = invitees.length; k < linv; k++) {
                    if (invitees[k] == d.rows[i].UserID) {
                        f = false;
                        break;
                    }
                }

                if (f) {
                    html += '<div class="divSearchLstItem" user_id="' + d.rows[i].UserID + '">' + d.rows[i].DisplayName + '</div>';
                }
            }

            if (html === '') {
                removeSearchResDiv();
            } else {
                insertHtmlInSearchResDiv(html);
                var $div = $divSearchLstWrap.find('.selectBoxSearchRes');
                $div.
                    bind('mouseover', onMouseOverSearchRes).
                    bind('mouseout', onMouseOutSearchRes);
                $div.find('div').
                    bind('mouseover', onMouseOverSearchResItem).
                    bind('mouseout', onMouseOutSearchResItem).
                    bind('click', onClickSearchResItem);
            }
        } else {
            removeSearchResDiv();
        }
    };
    var callGetPosibleInvites = function (query, succ_callback, err_callback) {
        $.vhmAjax({
            url: searchUrl,
            type: "POST",
            data: { 'query': query },
            success: function (response) {
                try {
                    succ_callback(eval('(' + response + ')'));
                } catch (ex) {
                    err_callback(ex);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                err_callback(thrownError);
            }
        });
    };
    var onKeyUp = function (txtDom) {
        callGetPosibleInvites(txtDom.value, getPosibleInvites_callback, function (err) { removeSearchResDiv(); if (err != "") { alert(err); } });
    };
    var attachTxtEvents = function () {
        var execKU = function () {
            onKeyUp($txt[0]);
        };
        var timerKU;
        var prevValue;

        $txt.keydown(function () {
            adjustWidth(this.value);
            if (timerKU) {
                clearTimeout(timerKU);
            }
        }).keyup(function (e) {
            var keycode = e.keyCode ? e.keyCode : e.which;
            if (this.value.trim() != "" && (keycode == 8 || keycode == 46 || (keycode >= 48 && keycode <= 90))) {
                insertHtmlInSearchResDiv('Loading...');
                timerKU = setTimeout(execKU, 400);
            } else if (keycode == 8) {
                $(this).parent().prev().remove();
            }
        });
    };

    var adjustWidth = function (val) {
        $txt.width(25 + getTextWidth(val));
    };
    var getTextWidth = function (val) {
        var d = $('<div style="position: absolute; visibility:hidden; font-size:13px; height: auto;width: auto;"></div>').insertAfter($ul);
        getTextWidth = function (val) {
            return d.text(val).width();
        };
        return getTextWidth(val);
    };
    var resetTxt = function () {
        $txt.val('').width(25);
    };
    var removeItem = function (a) {
        $(a).parent().remove();
    };
    var init = function (ulId, txtId, url) {
        $ul = $('#' + ulId);
        $txt = $('#' + txtId);
        searchUrl = url;
        $ul.parent().click(function () { $txt.focus(); });

        attachTxtEvents();
    };
    var getInvitees = function () {

        var inv, res = [];
        var item = $( ":input[vhm_memid]", ChI.$divInviteList );
        for ( var i = 0, l = item.length; i < l; i++ ) {
            inv = item[i].getAttribute( 'vhm_memid' );
            if ( inv ) {
                res.push( inv );
            }
        }
        return res;
    };
    return $.extend(grinv, {
        init: init,
        getInvitees: getInvitees
    }
    );
})(VHM.SocialGroups.Invite);