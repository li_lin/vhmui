﻿VHM.SocialGroups.PublicGroupPopup;
VHM.SocialGroups.GroupInvitationDialog;
VHM.SocialGroups.PublicGrpProfId;
VHM.SocialGroups.LoaderHTML;
VHM.SocialGroups.SearchValue;
VHM.SocialGroups.SearchCriteria;
VHM.SocialGroups.SgstGroupsPerPage;
VHM.SocialGroups.GetSgstdGroupsDataCache = {};
//VHM.SocialGroups.GetSgstdGroupsDataCache.Groups = {};
VHM.SocialGroups.HasAcceptedOrDeclinedInv = false;
VHM.SocialGroups.ListOfGrpNames = {};
VHM.SocialGroups.FriendNames = {};

$( function () {
    var dialogDefaults = {
        modal: true
        , autoOpen: false
        , resizable: false
        , draggable: false
        , position: ['center', 50]
    };

    //display loading div while loading all info
    //populate global vars
    if ( listOfGrpNames != null && listOfGrpNames != "" ) {
        VHM.SocialGroups.ListOfGrpNames = $.parseJSON( listOfGrpNames );
    }
    if ( listOfFriendsNames != null && listOfFriendsNames != "" ) {
        VHM.SocialGroups.FriendNames = $.parseJSON( listOfFriendsNames );
    }
    //this is the spinning image for load groups and/or search results;
    $( "#Loading" ).fadeIn( "fast" );
    VHM.SocialGroups.PublicGroupPopup = $( "#divGroupProfile" );
    VHM.SocialGroups.PublicGroupPopup.dialog( $.extend( {
        width: '450px'
        , dialogClass: 'no-title'
        , open: function ( event, ui ) {
            if ( VHM.SocialGroups.PublicGrpProfId != null && typeof VHM.SocialGroups.PublicGrpProfId != "undefined" && VHM.SocialGroups.PublicGrpProfId > 0 ) {
                var urlProfFromId = "_GetPublicGroupProfileFromID";
                $( this ).load( urlProfFromId, { 'groupId': VHM.SocialGroups.PublicGrpProfId }, function ( response, status, xhr ) {
                    if ( status != "success" || response.indexOf( "Service Error" ) >= 0 ) {
                        alert( $( '#hdnLocalization_SorrySomethingWentWrong' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
                        VHM.SocialGroups.PublicGroupPopup.dialog( "close" );
                    }
                } );
            }
        }
        , beforeClose: function ( event, ui ) { }
    }, dialogDefaults ) );
    //set up the spinning "loading content" for suggested groups
    $( "#divSpinner" ).attr( "class", "centerSpinInDialog" );
    VHM.SocialGroups.LoaderHTML = $( "#ContentLoading" ).html();
    VHM.SocialGroups.SgstGroupsPerPage = 15;
    VHM.SocialGroups.SuggestedGroups = $( "#divSgstdGrpsPopUp" );
    VHM.SocialGroups.showMoreSgstPageNo = 0;
    VHM.SocialGroups.clickedShowMore = false;
    VHM.SocialGroups.SuggestedGroups.dialog( $.extend( {
        width: '670px'
        , title: "Suggested Groups"
        , open: function ( event, ui ) {
            VHM.SocialGroups.showMoreSgstPageNo = 0;
            VHM.SocialGroups.GetMoreSuggestedGroups();
            $( ".centerLoading" ).fadeOut( "fast" );
        }
        , beforeClose: function ( event, ui ) { }
    }, dialogDefaults ) );
    VHM.SocialGroups.HasAcceptedOrDeclinedInv = false;
    VHM.SocialGroups.GroupInvitationDialog = $( "#divPendGrpInvPopUp" );
    VHM.SocialGroups.GroupInvitationDialog.dialog( $.extend( {
        width: '470px'
        , title: "Pending Group Invitations"
        , open: function () {
            $( this ).load( '_GroupPendingInvitations', {}, function ( response, status, xhr ) {
                if ( status != "success" || response.indexOf( "Service Error" ) >= 0 ) {
                    alert( $( '#hdnLocalization_SorryTheGroupFor' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
                    VHM.SocialGroups.GroupInvitationDialog.dialog( "close" );
                }
            } );
        }
        , close: function () {
            if ( VHM.SocialGroups.HasAcceptedOrDeclinedInv ) {
                window.location.reload();
            }
        }
    }, dialogDefaults ) );
    VHM.SocialGroups.LoadMyGroupsPage();
} );
/***************************************************************************************************
---------------------------------PENDING GROUPS INVITATIONS -----------------------------------
***************************************************************************************************/
VHM.SocialGroups.OpenGroupInvitations = function () {
    $( "#divSpinner" ).removeClass( "centerSpinInDialog" );
    $( "#divSpinner" ).attr( "class", "centerSpinInProfDialog" );
    VHM.SocialGroups.LoaderHTML = $( "#ContentLoading" ).html();
    VHM.SocialGroups.GroupInvitationDialog.html( VHM.SocialGroups.LoaderHTML );
    VHM.SocialGroups.GroupInvitationDialog.dialog( "open" );
};
VHM.SocialGroups.respondInvitation = function ( strOption, membNotificId, groupId, isGroupPrivate ) {
    $( ".divApproveRej" ).each( function () {
        if ( $( this ).attr( 'groupId' ) == groupId ) {
            var btnAcc = $( this ).find( "input" );
            var lnkDecline = $( this ).find( "a" );
            var spanOption = $( this ).find( "span" );
            if ( strOption === "Accept" ) {
                $( btnAcc ).hide();
                $( lnkDecline ).hide();
                $( spanOption ).show();
                $.vhmAjax( {
                    cache: false,
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "_AddMemberToGroupByNotificationId", //"_AddMemberToGroup",
                    data: '{ "id": ' + membNotificId + ' }',
                    dataType: "json",
                    success: function ( data ) {
                        if ( data === true ) {
                            $( spanOption ).text( "Invitation Accepted" );
                            $( spanOption ).val( "Invitation Accepted" );
                            VHM.SocialGroups.CreateGroupRewardTrigger();
                            VHM.SocialGroups.HasAcceptedOrDeclinedInv = true;
                        } else {
                            alert( $( '#hdnLocalization_SomethingWentWrongTryAgain' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
                            VHM.SocialGroups.GroupInvitationDialog.dialog( "close" );
                        };
                    },
                    error: function ( xhr, ajaxOptions, error ) {
                        alert( $( '#hdnLocalization_SomethingWentWrongTryAgain' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
                        VHM.SocialGroups.GroupInvitationDialog.dialog( "close" );
                    }
                } );
            } else {
                var paramDeclined = { nMemberNotificId: membNotificId };
                var sParamDecl = JSON.stringify( paramDeclined );
                //when declining a group invitation, if group is private, make sure we first show confirm pop-up
                if ( isGroupPrivate == "1" || isGroupPrivate == "True" ) {
                    vhmconfirm( "You are about to decline this invitation. This is a private group and you will not be able to join unless you are invited again. Are you sure you want to decline this invitation?",
                        function ( res, data ) {
                            $( btnAcc ).hide();
                            $( lnkDecline ).hide();
                            $( spanOption ).show();
                            $.vhmAjax( {
                                cache: false,
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                url: "_DeclineNotification",
                                data: sParamDecl,
                                dataType: "json",
                                success: function ( responseData ) {
                                    if ( responseData != null && responseData.indexOf( "Service Error" ) < 0 ) {
                                        var result = $.parseJSON( responseData );
                                        if ( result ) {
                                            $( spanOption ).val( "Invitation Declined" );
                                            $( spanOption ).text( "Invitation Declined" );
                                            VHM.SocialGroups.HasAcceptedOrDeclinedInv = true;
                                        }
                                    } else {
                                        alert( $( '#hdnLocalization_SomethingWentWrongTryAgain' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
                                        VHM.SocialGroups.GroupInvitationDialog.dialog( "close" );
                                    };
                                },
                                error: function ( xhr, ajaxOptions, error ) {
                                    alert( $( '#hdnLocalization_SomethingWentWrongTryAgain' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
                                    VHM.SocialGroups.GroupInvitationDialog.dialog( "close" );
                                }
                            } );
                        },
                        null,
                        { 'title': "" }

                    );
                } else {
                    $( btnAcc ).hide();
                    $( lnkDecline ).hide();
                    $( spanOption ).show();
                    $.vhmAjax( {
                        cache: false,
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "_DeclineNotification",
                        data: sParamDecl,
                        dataType: "json",
                        success: function ( responseData ) {
                            if ( responseData != null && responseData.indexOf( "Service Error" ) < 0 ) {
                                var result = $.parseJSON( responseData );
                                if ( result ) {
                                    $( spanOption ).val( "Invitation Declined" );
                                    $( spanOption ).text( "Invitation Declined" );
                                    VHM.SocialGroups.HasAcceptedOrDeclinedInv = true;
                                }
                            } else {
                                alert( $( '#hdnLocalization_SomethingWentWrongTryAgain' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
                                VHM.SocialGroups.GroupInvitationDialog.dialog( "close" );
                            };
                        },
                        error: function ( xhr, ajaxOptions, error ) {
                            alert( $( '#hdnLocalization_SomethingWentWrongTryAgain' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
                            VHM.SocialGroups.GroupInvitationDialog.dialog( "close" );
                        }
                    } );
                }
            }
        }
    } );
    VHM.SocialGroups.HasAcceptedOrDeclinedInv = true;
};

/***************************************************************************************************
----------------------------My Groups and paging functionality ------------------------------------
***************************************************************************************************/
VHM.SocialGroups.LoadMyGroupsPage = function () {
    //load my first 24 groups
    var grp = modelData;
    var jsonGroups = JSON.stringify( grp );
    // use this to decode any remaining html.
    jsonGroups = $( '<textarea/>' ).html( jsonGroups ).val();
    var url = "_BuildPagedGroups";
    $( "#divPagedGroups" ).load( url, { 'jsonGroupList': jsonGroups }, function ( response, status, xhr ) {
        if ( status != "success" || response.indexOf( "Service Error" ) >= 0 ) {
            alert( $( '#hdnLocalization_SomethingWentWrongTryAgain' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
            $( ".centerLoading" ).fadeOut( "fast" );
            $( "#Loading" ).fadeOut( "fast" );
        } else {
            $( ".centerLoading" ).fadeOut( "fast" );
            $( "#Loading" ).fadeOut( "fast" );
        }
    } );
    VHM.SocialGroups.PopulatePageTitleAndSummary( 'myGroups', 0 );
    //on page load - set the Search option to default - By Group name
    var srchBox = $( "#searchGroup" );
    var srcBoxByAdmin = $( "#searchGroupByAdmin" );
    var dropCateg = $( "#AllGroupCategory" );

    var rdo_SearchOption = $( "input[name='rdo_GroupSearch']" );
    $( 'input[name=rdo_GroupSearch]:checked' ).removeAttr( 'checked' );
    $( 'input[name=rdo_GroupSearch]:eq(0)' ).attr( 'checked', 'checked' );
    var srcBtn = $( "input[name='btnSearchGroup']" );
    $( srcBtn ).attr( "disabled", "true" );
    srchBox.show(); dropCateg.hide();
    srcBoxByAdmin.hide();
    srchBox.val( "" );
    srchBox.attr( 'srcBy', 'byGroupName' );
    srchBox.keyup( function () {
        VHM.SocialGroups.populateSearchSuggestions( 'byGroupName' );
    } );
    srcBoxByAdmin.keyup( function () {
        VHM.SocialGroups.populateSearchSuggestions( 'byAdministrator' );
    } );

    rdo_SearchOption.change( function () {
        var chd = $( this );
        if ( chd[0].defaultValue == 'GroupName' ) {
            srchBox.show(); srchBox.val( "" ); $( srcBtn ).attr( "disabled", "true" );
            srcBoxByAdmin.hide(); srcBoxByAdmin.val( "" );
            dropCateg.hide();
            VHM.Social.enableDisableBtn( srchBox.val(), "btnSearchGroup" );
            srchBox.keyup( function () {
                VHM.SocialGroups.populateSearchSuggestions( 'byGroupName' );
            } );
            srchBox.attr( 'srcBy', 'byGroupName' );
        } else if ( chd[0].defaultValue == 'Administrator' ) {
            srchBox.hide(); srchBox.val( "" ); $( srcBtn ).attr( "disabled", "true" );
            srcBoxByAdmin.show(); srcBoxByAdmin.val( "" );
            dropCateg.hide();
            VHM.Social.enableDisableBtn( srchBox.val(), "btnSrcByAdmin" );
            srchBox.attr( 'srcBy', 'byAdministrator' );
            srchBox.keyup( function () {
                VHM.SocialGroups.populateSearchSuggestions( 'byAdministrator' );
            } );
        } else if ( chd[0].defaultValue == 'Category' ) {
            srchBox.hide();
            srcBoxByAdmin.hide(); srcBoxByAdmin.val( "" );
            srchBox.attr( 'srcBy', 'byCategory' );
            $( srcBtn ).attr( "disabled", "true" );
            var selVal = $( "#AllGroupCategory option:selected" ).val();
            if ( selVal !== "" ) {
                dropCateg.val( "" );
            }

            dropCateg.show();
            dropCateg.change( function () {
                var selectedCateg = $( this ).val();
                if ( selectedCateg !== '' ) {
                    $( srcBtn ).removeAttr( "disabled" );
                } else {
                    $( srcBtn ).attr( "disabled", "true" );
                };
            } );
        };
    } );
};
VHM.SocialGroups.getGroupPage = function ( pagedSource, pageNo, pageSize ) {
    $( "#Loading" ).fadeIn( "fast" );
    VHM.SocialGroups.HidePageTitleAndSummary();
    $( "#divPagedGroups" ).html( "" );
    if ( pagedSource === 'myGroups' ) {
        var param = { count: pageSize, pageNumber: pageNo - 1 };
        $.vhmAjax( {
            cache: false,
            type: "GET",
            url: "_GetPagedGroups",
            data: param,
            dataType: "html",
            success: function ( responseData, status ) {
                if ( responseData != null && responseData.indexOf( "Service Error" ) < 0 && responseData.indexOf( "a problem occured" ) < 0 ) {
                    $( "#divPagedGroups" ).html( responseData );
                    $( ".centerLoading" ).fadeOut( "fast" );
                    $( "#Loading" ).fadeOut( "fast" );
                } else {
                    alert( $( '#hdnLocalization_SomethingWentWrongTryAgain' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
                    VHM.SocialGroups.LoadMyGroupsPage();
                };
            },
            error: function ( xhr, ajaxOptions, error ) {
                alert( $( '#hdnLocalization_SomethingWentWrongTryAgain' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
                VHM.SocialGroups.LoadMyGroupsPage();
            }
        } );
    } else if ( pagedSource === 'search' ) {
        if ( VHM.SocialGroups.SearchCriteria === 'byGroupName' ) {
            var input = { 'srcName': VHM.SocialGroups.SearchValue, 'pageNumber': pageNo - 1, 'groupsPerPage': pageSize };
            $.vhmAjax( {
                cache: false,
                type: "GET",
                url: "_SearchGroupByName",
                data: input,
                dataType: "html",
                success: function ( responseData ) {
                    if ( responseData != null && responseData.indexOf( "Service Error" ) < 0 && responseData.indexOf( "a problem occured" ) < 0 ) {
                        $( "#divPagedGroups" ).html( responseData );
                        $( ".centerLoading" ).fadeOut( "fast" );
                        $( "#Loading" ).fadeOut( "fast" );
                    } else {
                        alert( $( '#hdnLocalization_SomethingWentWrongTryAgain' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
                        VHM.SocialGroups.LoadMyGroupsPage();
                    };
                },
                error: function ( xhr, ajaxOptions, error ) {
                    alert( $( '#hdnLocalization_SomethingWentWrongTryAgain' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
                    VHM.SocialGroups.LoadMyGroupsPage();
                }
            } );
        } else if ( VHM.SocialGroups.SearchCriteria === 'byAdministrator' ) {
            var paramByAdm = { 'srcName': VHM.SocialGroups.SearchValue, 'pageNumber': pageNo - 1, 'groupsPerPage': pageSize };

            $.vhmAjax( {
                cache: false,
                type: "GET",
                url: "_SearchGroupByAdminName",
                data: paramByAdm,
                dataType: "html",
                success: function ( responseData ) {
                    if ( responseData != null && responseData.indexOf( "Service Error" ) < 0 && responseData.indexOf( "a problem occured" ) < 0 ) {
                        $( "#divPagedGroups" ).html( responseData );
                        $( ".centerLoading" ).fadeOut( "fast" );
                        $( "#Loading" ).fadeOut( "fast" );
                    } else {
                        alert( $( '#hdnLocalization_SomethingWentWrongTryAgain' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
                        VHM.SocialGroups.LoadMyGroupsPage();
                    };
                },
                error: function ( xhr, ajaxOptions, error ) {
                    alert( $( '#hdnLocalization_SomethingWentWrongTryAgain' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
                    VHM.SocialGroups.LoadMyGroupsPage();
                }
            } );
        } else if ( VHM.SocialGroups.SearchCriteria === 'byCategory' ) {
            var srcValue = $( "#AllGroupCategory option:selected" ).val();
            var paramByCat = { 'category': srcValue, 'pageNumber': pageNo - 1, 'groupsPerPage': pageSize };
            $.vhmAjax( {
                cache: false,
                type: "GET",
                url: "_SearchGroupByCategory",
                data: paramByCat,
                dataType: "html",
                success: function ( responseData ) {
                    if ( responseData != null && responseData.indexOf( "Service Error" ) < 0 && responseData.indexOf( "a problem occured" ) < 0 ) {
                        $( "#divPagedGroups" ).html( responseData );
                        $( ".centerLoading" ).fadeOut( "fast" );
                        $( "#Loading" ).fadeOut( "fast" );
                    } else {
                        alert( $( '#hdnLocalization_SomethingWentWrongTryAgain' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
                        VHM.SocialGroups.LoadMyGroupsPage();
                    };
                },
                error: function ( xhr, ajaxOptions, error ) {
                    alert( $( '#hdnLocalization_SomethingWentWrongTryAgain' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
                    VHM.SocialGroups.LoadMyGroupsPage();
                }
            } );
        }
    }
};
VHM.SocialGroups.changeItemsPerPage = function ( pagedSource, obj ) {
    $( "#Loading" ).fadeIn( "fast" );
    var newPageSize = $( "#ddItemsPerPage" ).val();
    $( "#divPagedGroups" ).html( "" ); //clear all html
    //VHM.SocialGroups.HidePageTitleAndSummary();
    if ( pagedSource === 'myGroups' ) {
        var param = { count: newPageSize, pageNumber: 0 };
        $.vhmAjax( {
            cache: false,
            type: "GET",
            url: "_GetPagedGroups",
            data: param,
            dataType: "html",
            success: function ( responseData, status ) {
                if ( responseData != null && responseData.indexOf( "Service Error" ) < 0 && responseData.indexOf( "a problem occured" ) < 0 ) {
                    $( "#divPagedGroups" ).html( responseData );
                    $( ".centerLoading" ).fadeOut( "fast" );
                    $( "#Loading" ).fadeOut( "fast" );
                } else {
                    alert( $( '#hdnLocalization_SomethingWentWrongTryAgain' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
                    VHM.SocialGroups.LoadMyGroupsPage();
                };
            },
            error: function ( xhr, ajaxOptions, error ) {
                alert( $( '#hdnLocalization_SomethingWentWrongTryAgain' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
                VHM.SocialGroups.LoadMyGroupsPage();
            }
        } );
    } else if ( pagedSource === 'search' ) {
        var input = { 'srcName': VHM.SocialGroups.SearchValue, 'pageNumber': 0, 'groupsPerPage': newPageSize };
        if ( VHM.SocialGroups.SearchCriteria === "byGroupName" ) {
            $.vhmAjax( {
                cache: false,
                type: "GET",
                url: "_SearchGroupByName",
                data: input,
                dataType: "html",
                success: function ( responseData ) {
                    if ( responseData != null && responseData.indexOf( "Service Error" ) < 0 && responseData.indexOf( "a problem occured" ) < 0 ) {
                        $( "#divPagedGroups" ).html( responseData );
                        $( ".centerLoading" ).fadeOut( "fast" );
                        $( "#Loading" ).fadeOut( "fast" );
                    } else {
                        alert( $( '#hdnLocalization_SomethingWentWrongTryAgain' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
                        VHM.SocialGroups.LoadMyGroupsPage();
                    };
                },
                error: function ( xhr, ajaxOptions, error ) {
                    alert( $( '#hdnLocalization_SomethingWentWrongTryAgain' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
                    VHM.SocialGroups.LoadMyGroupsPage();
                }
            } );
        } else if ( VHM.SocialGroups.SearchCriteria === "byAdministrator" ) {

            $.vhmAjax( {
                cache: false,
                type: "GET",
                url: "_SearchGroupByAdminName",
                data: input,
                dataType: "html",
                success: function ( responseData ) {
                    if ( responseData != null && responseData.indexOf( "Service Error" ) < 0 && responseData.indexOf( "a problem occured" ) < 0 ) {
                        $( "#divPagedGroups" ).html( responseData );
                        $( ".centerLoading" ).fadeOut( "fast" );
                        $( "#Loading" ).fadeOut( "fast" );
                    } else {
                        alert( $( '#hdnLocalization_SomethingWentWrongTryAgain' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
                        VHM.SocialGroups.LoadMyGroupsPage();
                    };
                },
                error: function ( xhr, ajaxOptions, error ) {
                    alert( $( '#hdnLocalization_SomethingWentWrongTryAgain' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
                    VHM.SocialGroups.LoadMyGroupsPage();
                }
            } );
        } else if ( VHM.SocialGroups.SearchCriteria === "byCategory" ) {
            var ddOption = $( "#AllGroupCategory option:selected" ).val();
            var paramByCat = { 'category': ddOption, 'pageNumber': 0, 'groupsPerPage': newPageSize };
            $.vhmAjax( {
                cache: false,
                type: "GET",
                url: "_SearchGroupByCategory",
                data: paramByCat,
                dataType: "html",
                success: function ( responseData ) {
                    if ( responseData != null && responseData.indexOf( "Service Error" ) < 0 && responseData.indexOf( "a problem occured" ) < 0 ) {
                        $( "#divPagedGroups" ).html( responseData );
                        $( ".centerLoading" ).fadeOut( "fast" );
                        $( "#Loading" ).fadeOut( "fast" );
                    } else {
                        alert( $( '#hdnLocalization_SomethingWentWrongTryAgain' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
                        VHM.SocialGroups.LoadMyGroupsPage();
                    };
                },
                error: function ( xhr, ajaxOptions, error ) {
                    alert( $( '#hdnLocalization_SomethingWentWrongTryAgain' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
                    VHM.SocialGroups.LoadMyGroupsPage();
                }
            } );
        }
    }
};
VHM.SocialGroups.BackToMyGroups = function () {
    VHM.SocialGroups.LoadMyGroupsPage();
};
/***************************************************************************************************
-------------------------------------SUGGESTED GROUPS ------------------------------------
***************************************************************************************************/
VHM.SocialGroups.OpenSuggestGroup = function () {
    $( "#loadingSuggested" ).show();
    VHM.SocialGroups.SuggestedGroups.dialog( "open" );
};
VHM.SocialGroups.ShowMoreGroups = function () {
    VHM.SocialGroups.showMoreSgstPageNo = VHM.SocialGroups.showMoreSgstPageNo + 1;
    VHM.SocialGroups.GetMoreSuggestedGroups();
};
// need to store the suggested groups in an JS object = VHM.SocialGroups.GetSgstdGroupsDataCache
VHM.SocialGroups.GetMoreSuggestedGroups = function () {
    $( "#loadingSuggested" ).show();
    if ( typeof VHM.SocialGroups.GetSgstdGroupsDataCache[VHM.SocialGroups.showMoreSgstPageNo] !== "undefined" ) {
        VHM.SocialGroups.BuildSuggestedGroups(); //get the objects from javascript cached obj VHM.SocialGroups.GetSgstdGroupsDataCache and pass in the current page number.
    } else {
        $.vhmAjax( {
            cache: false,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: "_GetMoreSuggestedGroups",
            data: { 'pageNumber': VHM.SocialGroups.showMoreSgstPageNo, 'groupsPerPage': VHM.SocialGroups.SgstGroupsPerPage },
            dataType: "json",
            success: function ( responseData ) {
                if ( responseData != null ) { //&& responseData.indexOf( "Service Error" ) < 0
                    VHM.SocialGroups.GetSgstdGroupsDataCache[VHM.SocialGroups.showMoreSgstPageNo] = responseData;
                    VHM.SocialGroups.BuildSuggestedGroups();
                } else {
                    alert( $( '#hdnLocalization_SorrySomethingWentWrong' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
                    VHM.SocialGroups.SuggestedGroups.dialog( "close" );
                };
            },
            error: function ( xhr, ajaxOptions, error ) {
                alert( $( '#hdnLocalization_SorrySomethingWentWrong' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
                VHM.SocialGroups.SuggestedGroups.dialog( "close" );
            }
        } );
    };
};
VHM.SocialGroups.BuildSuggestedGroups = function () {
    var jqoListHolder = $( "#suggestMoreGroups" );
    if ( VHM.SocialGroups.showMoreSgstPageNo == 0 ) {
        jqoListHolder.empty();
    }
    //check if on last page we still get the flag of GroupsPaginationInfo.HasMoreItems set to true.
    var hasMoreSugg = VHM.SocialGroups.GetSgstdGroupsDataCache[VHM.SocialGroups.showMoreSgstPageNo].GroupsPaginationInfo.HasMoreItems;
    if ( !hasMoreSugg ) { //do not show "See More..." link
        $( "#divContinueShowing" ).hide();
    } else {
        $( "#divContinueShowing" ).show();
    }

    var groups = [];
    var currentPage = VHM.SocialGroups.showMoreSgstPageNo; // only append the new items to the list
    var iterateObject = VHM.SocialGroups.GetSgstdGroupsDataCache[currentPage].Groups;
    for ( var j = 0; j < iterateObject.length; j++ ) {
        var item = { GroupId: iterateObject[j].Group.Id, GroupName: iterateObject[j].Group.Name, Category: iterateObject[j].Group.Category, ShortGroupName: iterateObject[j].ShortGroupName, ProfilePicturePath: iterateObject[j].Group.ProfilePicturePath };
        groups.push( item );
    }
    $( "#suggestedGroupsPopUpTmpl" ).template( "Template" );
    $.tmpl( "Template", groups ).appendTo( "#suggestMoreGroups" );
    $( "#loadingSuggested" ).hide();
};
/***************************************************************************************************
-------------------------------------SEARCH GROUPS ------------------------------------
***************************************************************************************************/
VHM.SocialGroups.populateSearchSuggestions = function ( strSearchOption ) {
    var searchBox;
    if ( strSearchOption == "byGroupName" ) {
        searchBox = $( "input[name='searchGroup']" );
    } else {
        searchBox = $( "input[name='searchGroupByAdmin']" );
    }
    var jqoSearchBTN = $( "input[name='btnSearchGroup']" );
    searchBox.removeData( 'events' );
    var strSearch = searchBox.val();
    strSearch = strSearch.StripOutHTMLTags();
    var handleSearch = function () {
        var strSrch = searchBox.val();
        strSrch = strSrch.trim();
        strSrch = VHM.Social.StripOutHTMLTags( strSrch );
        if ( strSrch.length > 2 ) {
            $( jqoSearchBTN ).removeAttr( "disabled" );
        } else {
            $( jqoSearchBTN ).attr( "disabled", "true" );
        };
    };
    var acCbo = {
        delay: 500,
        matchContains: 'word',
        minChars: 1,
        cacheLength: 0,
        mustMatch: false,
        matchSubset: true,
        autoFill: false,
        max: 25,
        selectFirst: true,
        scroll: false,
        formatItem: function ( data, i, n, value ) {
            if ( i === 24 ) { //( this.max - 1 )
                //console.debug( "flushCache" );
                jQuery( this ).flushCache();
            }

            return data[0]; //data[1] + 
        }
    };
    //    searchBox.autocomplete().flushCache();
    if ( strSearch !== "" ) {
        if ( strSearchOption == 'byGroupName' ) {
            searchBox.autocomplete( $.extend( {
                source: VHM.SocialGroups.ListOfGrpNames,
                response: function ( e, data ) {
                    //searchBox.flushCache();
                    handleSearch();
                }
            }, acCbo ) );
        } else if ( strSearchOption == 'byAdministrator' ) {
            searchBox.autocomplete( $.extend( {
                source: VHM.SocialGroups.FriendNames,
                response: function ( e, data ) {
                    //searchBox.flushCache();
                    handleSearch();
                }
            }, acCbo ) );
        };
        handleSearch();
    } else {
        handleSearch();
    }
};
VHM.SocialGroups.SearchForGroup = function ( object ) {
    $( "#Loading" ).fadeIn( "fast" );
    $( "#divPagedGroups" ).html( "" ); //clear all html
    VHM.SocialGroups.HidePageTitleAndSummary();
    var srcValue;
    VHM.SocialGroups.SearchCriteria = $( "#searchGroup" ).attr( 'srcBy' );
    if ( VHM.SocialGroups.SearchCriteria === 'byCategory' ) {
        srcValue = $( "#AllGroupCategory option:selected" ).val();
        VHM.SocialGroups.SearchValue = $( "#AllGroupCategory option:selected" ).text();
        var paramByCat = { 'category': srcValue, 'pageNumber': 0, 'groupsPerPage': 24 };
        $.vhmAjax( {
            cache: false,
            type: "GET",
            url: "_SearchGroupByCategory",
            data: paramByCat,
            dataType: "html",
            success: function ( responseData ) {
                if ( responseData != null && responseData.indexOf( "Service Error" ) < 0 ) {
                    $( "#divPagedGroups" ).html( responseData );
                    var totNoOfResults = $( "#divPagedGroups" ).find( "input[name='hdnTotalNoOfItems']" ).val();
                    VHM.SocialGroups.PopulatePageTitleAndSummary( 'search', totNoOfResults );
                    $( ".centerLoading" ).fadeOut( "fast" );
                    $( "#Loading" ).fadeOut( "fast" );
                } else {
                    alert( $( '#hdnLocalization_SomethingWentWrongTryAgain' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
                    VHM.SocialGroups.LoadMyGroupsPage();
                };
            },
            error: function ( xhr, ajaxOptions, error ) {
                alert( $( '#hdnLocalization_SomethingWentWrongTryAgain' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
                VHM.SocialGroups.LoadMyGroupsPage();
            }
        } );
    } else if ( VHM.SocialGroups.SearchCriteria === "byGroupName" ) {
        srcValue = $( "#searchGroup" ).val();
        srcValue = srcValue.trim();
        srcValue = srcValue.StripOutHTMLTags();
        VHM.SocialGroups.SearchValue = srcValue;
        var paramByGN = { 'srcName': srcValue, 'pageNumber': 0, 'groupsPerPage': 24 };
        $.vhmAjax( {
            cache: false,
            type: "GET",
            url: "_SearchGroupByName",
            data: paramByGN,
            dataType: "html",
            success: function ( responseData ) {
                if ( responseData != null && responseData.indexOf( "Service Error" ) < 0 ) {
                    $( "#divPagedGroups" ).html( responseData );
                    var totNoOfResults = $( "#divPagedGroups" ).find( "input[name='hdnTotalNoOfItems']" ).val();
                    VHM.SocialGroups.PopulatePageTitleAndSummary( 'search', totNoOfResults );
                    $( ".centerLoading" ).fadeOut( "fast" );
                    $( "#Loading" ).fadeOut( "fast" );
                } else {
                    alert( $( '#hdnLocalization_SomethingWentWrongTryAgain' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
                    VHM.SocialGroups.LoadMyGroupsPage();
                };
            },
            error: function ( xhr, ajaxOptions, error ) {
                alert( $( '#hdnLocalization_SomethingWentWrongTryAgain' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
                VHM.SocialGroups.LoadMyGroupsPage();
            }
        } );
    } else if ( VHM.SocialGroups.SearchCriteria === "byAdministrator" ) {
        srcValue = $( "#searchGroupByAdmin" ).val();
        srcValue = srcValue.trim();
        srcValue = srcValue.StripOutHTMLTags();
        VHM.SocialGroups.SearchValue = srcValue;
        var paramByAdm = { 'srcName': srcValue, 'pageNumber': 0, 'groupsPerPage': 24 };

        $.vhmAjax( {
            cache: false,
            type: "POST",
            url: "_SearchGroupByAdminName",
            data: paramByAdm,
            dataType: "html",
            success: function ( responseData ) {
                if ( responseData != null && responseData.indexOf( "Service Error" ) < 0 ) {
                    $( "#divPagedGroups" ).html( responseData );
                    var totNoOfResults = $( "#divPagedGroups" ).find( "input[name='hdnTotalNoOfItems']" ).val();
                    VHM.SocialGroups.PopulatePageTitleAndSummary( 'search', totNoOfResults );
                    $( ".centerLoading" ).fadeOut( "fast" );
                    $( "#Loading" ).fadeOut( "fast" );
                } else {
                    alert( $( '#hdnLocalization_SomethingWentWrongTryAgain' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
                    VHM.SocialGroups.LoadMyGroupsPage();
                };
            },
            error: function ( xhr, ajaxOptions, error ) {
                alert( $( '#hdnLocalization_SomethingWentWrongTryAgain' ).val(), { title: $( '#hdnLocalization_SomethingWentWrong' ).val() } );
                VHM.SocialGroups.LoadMyGroupsPage();
            }
        } );
    }
};
//strOption = 'search' or 'myGroups'
VHM.SocialGroups.PopulatePageTitleAndSummary = function ( strOption, count ) {
    $( ".divContentSummaryCount" ).show();
    var jqoTitle = $( ".divContentSummaryCount" ).find( "#groupsPageTitle" );
    var jqoCount = $( ".divContentSummaryCount" ).find( "#lblGroupsCount" );
    var strDescrCount = "";
    if ( strOption === 'search' ) {
        jqoTitle.html( "Search Results" );
        if ( count == 0 ) {
            strDescrCount = $('#hdnLocalization_NoMatchesWereFound').val().replace("{0}", VHM.SocialGroups.SearchValue);
        } else if ( count == 1 ) {
            strDescrCount = $('#hdnLocalization_N1MatchWasFound').val().replace("{0}", VHM.SocialGroups.SearchValue);
        } else {
            strDescrCount =  $('#hdnLocalization_NumberMatchesWereFound').val().replace("{0}",count).replace("{1}",VHM.SocialGroups.SearchValue);
        }
        jqoCount.html( strDescrCount );
        $( "#divPendingInvitations" ).hide();
        $( "#divReturnToGroups" ).show();
    } else {
        jqoTitle.html( "Groups" );
        //we need to determine the groups page description
        var totalNoOfGroups = parseInt( myGroups_TotalNo );
        var myGroupsDescription = "";
        if ( totalNoOfGroups == 0 ) {
            myGroupsDescription = $( '#hdnLocalization_YouAreNotAMember' ).val();
        } else if ( totalNoOfGroups == 1 ) {
            myGroupsDescription = $( '#hdnLocalization_YouAreAMemberOf1' ).val();
        } else {
            myGroupsDescription = $('#hdnLocalization_YouAreAMemberOfNumber').val().replace("{0}",totalNoOfGroups);
        }
        jqoCount.html( myGroupsDescription );
        $( "#divPendingInvitations" ).show();
        $( "#divReturnToGroups" ).hide();
    }
};

VHM.SocialGroups.HidePageTitleAndSummary = function () {
    $( ".divContentSummaryCount" ).hide();
    $( "#divPendingInvitations" ).hide();
    $( "#divReturnToGroups" ).hide();
};