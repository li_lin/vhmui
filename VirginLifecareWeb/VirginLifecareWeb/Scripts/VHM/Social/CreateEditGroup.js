﻿$(function (  ) {
    $( "#deleteGroup" ).click( function () {
        var id = $( "#groupId" ).val();
        VHM.SocialGroups.DeleteGroup( id );
    } );
    $( "#txtGroupName" ).change( function () {
        var newGroupName = $( this ).val();
        var originalName = $( "#hidGroupName" ).val();
        if ( originalName == "" || originalName != newGroupName.trim() )
            VHM.SocialGroups.IsGroupNameAvalibale( newGroupName );
    } );
    $( "#btnCreateGroup" ).click( function () {
        if ( VHM.SocialGroups.ValidateGroupIM() != true ) {
            return false;
        }
        $( 'form' )[0].submit();
        return true;
    });
    //use this for sending group invitations and also validate group inputs
    $("#btnSaveSend").click(function () {
        var $this = $(this);
        
        if (VHM.SocialGroups.ValidateGroupIM() != true) {
            return false;
        }
        //disable button onclick to avoid sending multiple invitations/notifications
        $this.attr("disabled", "disabled");
        $('form')[0].submit();
        return true;
    });
    //use this for sending group invitations
    $("#btnSend").click(function () {
        var $this = $(this);
        //disable button onclick to avoid sending multiple invitations/notifications
        $this.attr("disabled", "disabled");
        $( 'form' )[0].submit();
    } );
} );