﻿var VHM = VHM || {};
VHM.Social = VHM.Social || {};
VHM.SocialGroups = VHM.SocialGroups || {};
VHM.SocialGroups.Invite = VHM.SocialGroups.Invite || {};
VHM.Social.NewsfeedFailMessage = "<div class='nfFailMessage'>Seems like there's a problem loading your News Feed. Try <a href='#'>refreshing the page.</a></div>";

/*************************************************************************************************
****************************TO BE MOVED UNDER javascript lib folder**********************************/
VHM.Social.StripOutHTMLTags = function ( text ) {
    return text.replace( /<\/?[^>]+>/gi, '' );
};
/************************************************************************************************************************************/

//To use This function please add the following html on the page this needs to be used:
/*
-- this is needed to be populated by group profile html
<div id="divGroupProfile" class="jqmDialogPopUp"></div>
--for the spinner
<div id="ContentLoading" class="centerLoading">
<div id="divSpinner"><img src="@Url.Content("~/Content/Images/Shared/spinner.gif")" alt="Loading Suggested groups"  /></div>
</div>
---also, make sure to declare the dialog on javascript document ready function
*/

VHM.SocialGroups.OpenPublicGroupProfileFromID = function (groupId) {
    $("#divSpinner").removeClass("centerSpinInDialog");
    $("#divSpinner").attr("class", "centerSpinInProfDialog");
    VHM.SocialGroups.LoaderHTML = $("#ContentLoading").html();
    VHM.SocialGroups.PublicGroupPopup.html(VHM.SocialGroups.LoaderHTML);
    VHM.SocialGroups.PublicGrpProfId = groupId;
    VHM.SocialGroups.PublicGroupPopup.dialog("open");
    $(document).on('click', ".closeGrpProfile", function () {
        VHM.SocialGroups.PublicGroupPopup.dialog('close');
    });
};
VHM.SocialGroups.JoinGroup = function (nGroupId) {
    $("#divSpinner").removeClass("centerSpinInDialog");
    $("#divSpinner").attr("class", "centerSpinInProfDialog");
    VHM.SocialGroups.LoaderHTML = $("#ContentLoading").html();
    VHM.SocialGroups.PublicGroupPopup.html(VHM.SocialGroups.LoaderHTML);

    $.vhmAjax({
        cache: false,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "_AddMemberToGroup",
        data: '{ "id": ' + nGroupId + ' }',
        dataType: "json",
        success: function (responseData) {
            if (responseData != null && responseData && responseData != "") {
                //if response is true - redirect to group page
                VHM.SocialGroups.CreateGroupRewardTrigger();
                var joinedGroupUrl = "/Secure/Social/ShowGroup.aspx?gID=" + nGroupId;
                window.location.href = joinedGroupUrl;
            } else {
                alert('Something went wrong. Please try again later. ', { title: 'Something went wrong' });
                VHM.SocialGroups.PublicGroupPopup.dialog("close");
            };
        },
        error: function (xhr, ajaxOptions, error) { }
    });
};
VHM.Social.enableDisableBtn = function ( strText, btnID ) {
    var btn = $( "#" + btnID );
    strText = strText.trim();
    strText = strText.StripOutHTMLTags();
    if ( strText.length > 2 ) {
        $( btn ).removeAttr( "disabled" );
    } else {
        $( btn ).attr( "disabled", "true" );
    };
};
VHM.SocialGroups.DeleteGroup = function ( id ) {
    vhmconfirm( $( '#hdnLocalization_YouAreAdministrator' ).val(),
        function ( result, data ) {
            // window.location.href = "../../social/deleteGroup/" + id;
            //rather then redirecting to "../../social/deleteGroup/" + id; we'll make an ajax call
            var inputs = { id: id };
            $.ajax( {
                type: "POST",
                url: AppPath + "/social/deleteGroup",
                data: JSON.stringify( inputs ),
                contentType: 'application/json; charset=utf-8',
                dataType: "json",
                success: function ( responseData ) {
                    var resp = $.parseJSON( responseData );
                    if ( resp ) {
                        window.location.href = "../../social/MyGroups";
                    }
                },
                error: function ( xhr, ajaxOptions, error ) { }
            } );
        } );
};

VHM.SocialGroups.IsGroupNameAvalibale = function ( groupName ) {
    groupName = groupName.trim();
    groupName = groupName.StripOutHTMLTags();
    $( "#txtGroupName" ).val( groupName );
    var data = { groupName: groupName };
    $.ajax( {
        type: "POST",
        url:AppPath +"/social/IsGroupNameAvalibale",
        data: JSON.stringify( data ),
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        success: function ( responseData ) {
            $( "#divGroupAvailability" ).val( groupName );
            if ( responseData == true ) {
                $( "#divGroupAvailability" ).attr( "isAvailable", "true" );
                $( "#divGroupAvailability" ).html( "<h5 class='green'>" + $( "#hdnLocalization_TextIsAvailable" ).val().replace( "{0}", groupName ) + "</h5>" );

            } else if ( responseData == false ) {
                $( "#divGroupAvailability" ).attr( "isAvailable", "false" );
                $( "#divGroupAvailability" ).html( "<h5 class='red'>" + $( "#hdnLocalization_TextIsAlreadyCreated" ).val().replace( "{0}", groupName ) + "</h5>" );
            }

        }, 
        error: function ( xhr, ajaxOptions, error ) {  }
    } );
};

VHM.SocialGroups.ValidateGroupIM = function () {
    var errors = [];
    var groupName = $("#txtGroupName").val();

    groupName = groupName.trim();
    groupName = groupName.StripOutHTMLTags();

    var groupDesctiption = $("#txtDescription").val();

    groupDesctiption = groupDesctiption.trim();
    groupDesctiption = groupDesctiption.StripOutHTMLTags();

    if (groupName.length < 3)
        errors.push( $( "#hdnLocalization_MoreThan3" ).val() );

    if (groupName == "")
        errors.push( $( "#hdnLocalization_NameIsRequired" ).val() );

    if ($("#divGroupAvailability").attr("isAvailable") == "false")
        errors.push( $( "#hdnLocalization_NameIsRequired" ).val() );

    if (groupDesctiption == "")
        errors.push( $( "#hdnLocalization_DescriptionIsRequired" ).val() );

    if ($("#CategoryId").val() == "")
        errors.push( $( "#hdnLocalization_CategoryIsRequired" ).val() );

    if (errors.length > 0) {
        var text = "<ul style='margin-left:30px'>";

        $.each(errors, function (key, value) {
            text += "<li style='list-style-type:circle '>" + value + "</li>";
        });

        text = text + "</ul>";
        alert( text, { title: "Oops!", redMessageText: $( '#hdnLocalization_MissingInformation' ).val() } );

        return false;
    }

    $("#txtGroupName").val(groupName);
    $("#txtDescription").val(groupDesctiption);

    return true;
};
VHM.SocialGroups.CreateGroupRewardTrigger = function () {
    $.ajax( {
        url: window.location.protocol+"//"+window.location.host+"/Secure/WebServices/Challenge.asmx/CreateGroupRewardTrigger",
        type: "POST",
        contentType: "application/json; charset=utf-8"
    } );
};
