﻿var VHM = VHM || {},
setCookie,
getCookie;

//Global page jquery
$(document).ready(function () {
    $('.menuHeaderItem').on('click', function (e) {
        if (navigator.userAgent.match(/iPad/i)) {
            var currentMenu = $(this).find('.menuHeaderSubMenu'),
            otherMenus = $('.menuHeaderSubMenu').not(currentMenu);
            otherMenus.hide();
            var test = $('.activeMenuHeader').not($(this));
            test.toggleClass('activeMenuHeader').toggleClass('inactiveMenuHeader');
            currentMenu.toggle();
            $(this).toggleClass('activeMenuHeader').toggleClass('inactiveMenuHeader');
        }
    });

    $('#applicationNavMenuHeader').on('click', function (e) {
        if (navigator.userAgent.match(/iPad/i)) {
            $('#applicationNavMenu').toggle();
            $(this).toggleClass('activeMenu');
        }
    });

    $('#menuProfile').on('click', function (e) {
        if (navigator.userAgent.match(/iPad/i)) {
            $('#profileMenu').toggle();            
        }
    });
});

//Global js functions

setCookie = function (name, val) {
    var expirationDate = new Date(),
    cookieValue = escape(val) + ";";
    document.cookie = escape(name) + "=" + cookieValue;    
};

getCookieValue = function (name) {
    var cookies = document.cookie.split(';'),
    returnCookie;
    $.each(cookies, function (key, value) {
        var tempName = value.split('=')[0].replace(/^\s+|\s+$/g, "");
        if (tempName === name) {
            returnCookie = value;
            return false;
        }
    });
    if (returnCookie) {
        returnCookie = unescape(returnCookie.split("=")[1]);
    };

    return returnCookie;
};