﻿// Controller logic for widgets on nutrition landing page
(function () {
    "use strict";
    // Nutrition data structures
    var _dataProvider;
    var _bioStats;
    var _history;
    var _historySelection;
    var _pollingTimerId;
    //Show help modal on first time visiting Nutrition page

    // dataChangeNotifications flags
    var DATA_PROVIDER = 0x1;
    var DATA_PROVIDER_CONNECTION = 0x2;
    var DATA_BIO_STATS = 0x4;
    var DATA_HISTORY = 0x8;
    var DATA_HISTORY_SELECTION = 0x10;

    var COLORS_CONSUMED = ['#075907', '#097609', '#70af1a', '#8dc476', '#a9e1ac'];
    var COLORS_BURNED = ['#601412', '#b02523', '#ce3d29', '#ff6634', '#ffaa8f'];
    var CHART_FONT_SIZE = '11px';
    var GRID_COLOR = '#c6c6c6';
    var ANIMATION_MS = 700;
    var API_BASE = location.protocol + '//' + location.host;
    var API_BASE_URL = API_BASE + '/rest/v1/nutrition';
    var CHALLENGE_API_URL = API_BASE + '/rest/v1/challenges';
    var ACTIVITIES_API_URL = API_BASE + '/rest/v1/activities/';
    var MAX_WEIGHT_HISTORY_DATA_POINTS = 5;

    var KG_PER_POUND = 2.20462262185;
    var CM_PER_INCH = 0.393700787402;
    var CALORIES_PER_POUND = 500;
    var MIN_CALORIES_FOR_MALE = 1500;
    var MIN_CALORIES_FOR_FEMALE = 1200;
    var SYNC_POLL_INTERVAL_MS = 3 * 1000; // 3 sec
    var TARGET_CHANGES_PER_WEEK;
    // Array of widgets used on this page
    var _widgets;

    //Current page holder (find friends feature).
    var CURRENT_PAGE = 0;
    var TOTAL_FRIEND_COUNT_PER_PAGE = 20;

    var _averageCaloriesBurned = 200;
    var _totalCaloriesGoal = 0;
    var LOSE_GAIN_WEIGHT;
    WhatIsNextInitiallization();

    // Nutrition public API
    window.Nutrition = {
        // Initializes the overview chart only (for the member landing page)
        initOverviewChart: function () {
            initWidgets([createOverviewWidget(),
                createWeightTrackingWidget(),
                createConnectionWidget()]);

        },

        // Initializes all widgets on the nutrition landing page
        initNutritionPage: function () {
            initWidgets([
                createOverviewWidget(),
                createConnectionWidget(),
                createWeightGoalDialog(),
                createTrackingWidget(),
                createProgressWidget(),
                createChallengeListWidget(),
                createRewardListWidget()
            ]);
        },

        getActivitySummaries: function (isWidget) {
            $.ajax({
                url: ACTIVITIES_API_URL + "summaries",
                type: 'GET',
                success: function (data) {
                    //TODO: this just seems wrong. need to find a better way to manager this dependency
                    if ( !isWidget ) { //only do this on the nutrition landing page 
                        window.Nutrition.initNutritionPage();
                    }
                    if (data == null || data.length == 0) {
                        return;
                    }

                    var entriesCount = data.length;
                    var sumCalories = 0;
                    $.each(data, function (item, value) {
                        sumCalories += value.CaloriesBurned;
                    });
                    _averageCaloriesBurned = Math.round(sumCalories / entriesCount);
                },
                error: function () {
                    $('#nut-loading').html('Sorry, we&rsquo;re unable to load your nutrition information at this time.  Please check back later.');
                    $("#loading-element").remove();
                }
            });
        }
    };

    function WhatIsNextInitiallization() {
        if ($('#nutrition-next-rewards').html() != undefined) {
            ko.applyBindings(new WhatsNextRewardsModel(), document.getElementById("nutrition-next-rewards"));
        }
        var RewardsURL = location.protocol + '//' + location.host + '/rest/v1/nutrition/rewards';

        function WhatsNextRewardsModel() {
            var self = this;
            self.Rewards = ko.observableArray([]);
            $.ajax({
                url: RewardsURL,
                type: "GET",
                success: function (rewards) {
                    if (rewards.length > 0) {
                        $('#nutrition-next-rewards').removeClass('hide');
                        $(rewards).each(function () {

                        });

                        self.Rewards(rewards);
                    }
                },
                error: function (error) {
                    console.log(JSON.stringify(error));
                }
            });
        }

    }

    // Fetches nutrition data from the Nutrition API and calls the loadData method of all nutrition widgets available on the page
    function initWidgets(widgets) {
        new imageLoader("startAnimation()");
        _widgets = widgets;

        for (var i = 0; i < _widgets.length; i++) {
            _widgets[i].init();
        }

        $.ajax({
            url: API_BASE_URL,
            type: 'GET',
            data: {
                timezoneOffset: new Date().getTimezoneOffset(),
                forceRefresh: !!getCookieValue('NUTRefresh')
            }
        }).done(function (data) {
            $("#loading-element").remove();
            $("#nutrition-content").fadeIn();
            $('#nutrition-challenge-list').fadeIn();

            // Remove cookie -- only force refresh once (upon login)
            document.cookie = 'NUTRefresh=;path=/;expires=Thu, 01 Jan 1970 00:00:01';

            createFriendListWidget();
            processNutritionData(data);
        }).fail(function () {
            $('#nut-loading').html('Sorry, we&rsquo;re unable to load your nutrition information at this time.  Please check back later.');
            $("#loading-element").remove();
        });
    }

    function pollUntilSyncComplete() {
        if (_pollingTimerId) {
            // Polling is already in progress. 
            return;
        }

        $('#nutrition-widget-content .btn-refreshing').show();
        _pollingTimerId = setTimeout(function () {
            _pollingTimerId = 0;
            $.ajax({
                url: API_BASE_URL,
                type: 'GET',
                data: {
                    timezoneOffset: new Date().getTimezoneOffset(),
                    pollMode: true
                }
            }).done(function (data) {
                if (data) {
                    // Sync is complete.
                    processNutritionData(data);
                    $('#nutrition-widget-content .btn-refreshing').hide();
                } else {
                    pollUntilSyncComplete();
                }
            }).fail(function () {
                // API failed.  Stop polling.
                onDataChange(DATA_PROVIDER_CONNECTION);
                $('#nutrition-widget-content .btn-refreshing').hide();
            });
        }, SYNC_POLL_INTERVAL_MS);
    }

    function processNutritionData(data) {
        _dataProvider = data.DataProviders['MFP'] || {};
        _bioStats = data.BioStats;
        _history = data.History;
        _historySelection = $.extend(true, {}, _history[_history.length - 1], { DayCount: 1 });

        // Convert JSON date strings to JavaScript Dates
        for (var i = 0; i < _history.length; i++) {
            _history[i].Date = parseIsoDate(_history[i].Date);
        }
        var weightHistory = _bioStats.WeightHistory;
        for (var i = 0; i < weightHistory.length; i++) {
            weightHistory[i].Date = parseIsoDate(weightHistory[i].Date);
        }
        
        // Notify widgets
        onDataChange(DATA_PROVIDER | DATA_BIO_STATS | DATA_HISTORY | DATA_HISTORY_SELECTION);

        // Update UI once more data is available
        if (_dataProvider.IsSyncInProgress) {
            pollUntilSyncComplete();
        }
    }

    function onDataChange(changeFlags) {
        for (var i = 0; i < _widgets.length; i++) {
            var w = _widgets[i];
            if ((w.dataChangeNotifications & changeFlags) != 0) {
                w.onDataChange(changeFlags);
            }
        }
    }

    function numberFormat(n, digits) {
        var c = (digits === undefined) ? 0 : digits;
        var d = '.', t = ',', s = n < 0 ? '-' : '', i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + '', j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : '');
    }

    function parseIsoDate(dateStr) {
        if (!dateStr) {
            return null;
        }
        var dateParts = dateStr.split('-');
        return new Date(dateParts[0], parseInt(dateParts[1]) - 1, dateParts[2]);
    }

    function markError(field$, msg) {
        field$.focus();
        alert(msg);
    }

    function confirmWarning(field$, msg) {
        field$.focus();
        return confirm(msg);
    }

    function handleApiError(msg, xhr) {
        try {
            msg += ': ' + JSON.parse(xhr.innerText).Message;
        } catch (e) {
            msg += '.';
        }
        alert(msg, null, true);
    }

    //Available challenge widget

    function createChallengeListWidget() {
        return {
            init: function () {
                try {
                    //without this, KO defaults to using a templating engine with less functionality
                    ko.setTemplateEngine(new ko.nativeTemplateEngine());
                    ko.applyBindings(new ChallengeListModel(),
                        document.getElementById("nutrition-challenge-list"));
                } catch (ex) {

                }
            }
        };
    }

    var ChallengeListModel = function () {
        var self = this;

        self.Challenges = ko.observableArray([]);
        self.HasChallenges = ko.observable(true);
        self.GetModel = function () {
            loadChallenges();
        };
        var loadChallenges = function () {
            var challengeFilterCategory = 'NUT',
                challengeFilterDetails = 'false',
                challengeFilterType = 'invited',
                challengeFilterBrowse = 'true',
                challengeFilterString = '?category=' + challengeFilterCategory + '&detail=' + challengeFilterDetails + '&type=' + challengeFilterType + '&includeBrowse=' + challengeFilterBrowse;

            $.ajax({
                url: CHALLENGE_API_URL + challengeFilterString,
                type: 'GET',
                cache: false
            })
                .done(function (data) {
                    if (data.length == 0) {
                        self.HasChallenges(false);
                    } else {
                        self.HasChallenges(true);
                        var returnData = $.grep(data, function (item) {
                            if (item.IsMemberParticipating || item.OwnerType === "PER") {
                                return false;
                            }

                            //get badge url if it exists
                            if (item.Rewards != undefined && item.Rewards.length > 0) {
                                item.BadgeImageUrl = item.Rewards[0].BadgeUrl;
                            } else {
                                item.BadgeImageUrl = null;
                            }

                            //get challenge url if it exists
                            if (item.ChallengeID != undefined) {
                                item.ChallengeUrl = '/challenge/' + item.ChallengeID;
                            } else {
                                item.ChallengeUrl = null;
                            }
                            return true;
                        });
                        if (returnData.length == 0) {
                            self.HasChallenges(false);
                        }
                        self.Challenges(returnData);
                    }
                })
                .fail(function (xhr) {
                    self.HasChallenges(false);
                    handleApiError('Unable to retrieve challenges', xhr);
                });

        };
        self.GetModel();
    };

    // Connection widget "class"
    function createConnectionWidget() {
        function getChannelUrl() {
            return API_BASE_URL + '/data-providers/' + encodeURIComponent(_dataProvider.ProviderName) + '/channel';
        }

        function mfpConnect() {
            //            var useServerSideAuth = !$.support.cors || ($.browser.msie && parseFloat($.browser.version) <= 9);  // IE9 and lower doesn't support 
            //            if (useServerSideAuth) {
            mfpConnectServerSide();
            //            } else {
            //                mfpConnectClientSide();
            //            }
        }

        function mfpConnectServerSide() {
            var authUrl = _dataProvider.AuthenticateUrl;
            authUrl = authUrl.replace('${client_id}', _dataProvider.ClientId);
            authUrl = authUrl.replace('${state}', encodeURIComponent(document.location.href));
            authUrl = authUrl.replace("${redirect_uri}", getChannelUrl());
            document.location.href = authUrl;
            
        }

        function mfpConnectClientSide() {
            window.mfp.auth.authorize('diary', function (ret) {
                if (ret.message) {
                    // Something went wrong
                    alert(ret.message, null, true);
                    return;
                }

                syncStatusLabel('Connecting...');

                // Notify the service we've established a connection and should start data integration
                $.ajax({
                    url: API_BASE_URL + '/data-providers/' + encodeURIComponent(_dataProvider.ProviderName) + '/connection',
                    type: 'POST',
                    data: {
                        accessToken: ret.access_token,
                        refreshToken: ret.refresh_token
                    }
                }).done(function (data) {
                    _dataProvider = data;
                    onDataChange(DATA_PROVIDER_CONNECTION);
                    pollUntilSyncComplete();
                }).fail(function (xhr) {
                    handleApiError('Unable to connect', xhr);
                }).always(function () {
                    syncStatusLabel();
                });

            }, function (ret) {
                // Failure handler
                if (ret.error == 'access_denied') {
                    alert("You denied the Virgin Pulse request to access your account.\n\nIf you'd still like to connect, please try again and select \"Allow\".", null, true);
                } else {
                    alert("Unable to connect:  " + ret.message, null, true);
                }
            });
        }

        function mfpDisconnect() {
            $.ajax({
                url: API_BASE_URL + '/data-providers/' + encodeURIComponent(_dataProvider.ProviderName) + '/connection',
                type: 'DELETE'
            }).done(function (data) {
                _dataProvider = data;
                onDataChange(DATA_PROVIDER_CONNECTION);
                try {
                    window.mfp.auth.revoke(function () {
                    });
                } catch (e) {
                    // MFP rejected the revoke, perhaps because it has no record of the token.  We can safely ignore this.
                }
            }).fail(function (xhr) {
                handleApiError('Unable to disconnect', xhr);
            }).always(function () {
                syncStatusLabel();
            });
        }

        function mfpRefresh() {

            $.ajax({
                url: API_BASE_URL + '/data-providers/' + encodeURIComponent(_dataProvider.ProviderName) + '/alerts/current',
                type: 'POST'
            }).done(function () {
                _dataProvider.IsSyncInProgress = true;
                syncStatusLabel();
                pollUntilSyncComplete();
            }).fail(function (xhr) {
                handleApiError('Unable to refresh', xhr);
            });
        }

        function syncStatusLabel(customStatus) {
            var el$ = $('#nutrition-partner-connect');
            var isConnected = _dataProvider.IsConnected;
            var isRefreshing = _dataProvider.IsSyncInProgress;

            el$.find('.status')[_dataProvider ? 'show' : 'hide']();
            el$.find('.status-label').removeClass('status-connected status-disconnected');
            el$.find('.btn-connect')[!isConnected ? 'show' : 'hide']();
            el$.find('.btn-disconnect')[isConnected ? 'show' : 'hide']();
            $('.btn-refresh')[(isConnected && !isRefreshing) ? 'show' : 'hide']();
            el$.find('.btn-refreshing')[(isConnected && isRefreshing) ? 'show' : 'hide']();

            $('#nutrition-overview .mask')[isConnected ? 'addClass' : 'removeClass']('hide');
            if (customStatus) {
                el$.find('.status-label').text(customStatus);
            } else if (isConnected) {
                var usernameSuffix = (_dataProvider.ProviderUserName) ? ' as ' + _dataProvider.ProviderUserName : '';
                el$.find('.status-label').html("<i class='icon-ok'></i>  Connected" + usernameSuffix).addClass('status-connected');

            } else {
                el$.find('.status-label').html("<i class='icon-remove'></i>  Disconnected").addClass('status-disconnected');
            }
        }

        return {
            init: function () {
                $('#nutrition-partner-connect .btn-connect').on('click', function (e) {
                    e.preventDefault();
                    mfpConnect();
                });
                $('#nutrition-partner-connect .btn-disconnect').on('click', function (e) {
                    e.preventDefault();
                    mfpDisconnect();
                });
                $('.btn-refresh').on('click', function (e) {
                    e.preventDefault();
                    mfpRefresh();
                });
                $('#connectMFPAccount').on('click', function (e) {
                    e.preventDefault();
                    mfpConnect();
                });
                $('#connectMFPAccountLanding').on('click', function (e) {
                    e.preventDefault();
                    mfpConnect();
                });
            },

            dataChangeNotifications: DATA_PROVIDER | DATA_PROVIDER_CONNECTION,

            onDataChange: function () {
                $('#nutrition-partner-connect').fadeIn();
                syncStatusLabel();

                window.mfp.init({
                    client_id: 'virgin_health_miles',
                    channelUrl: getChannelUrl()
                });
            }
        };
    }

    // Widget to draw the rainbow graph with "Net Cal", "Goal", and "Left" calorie counts
    function createOverviewWidget() {
        var paper;
        var lblNet$;
        var lblGoal$;
        var lblLeft$;
        var lblLeftContainer$;
        var lblLeftDirection$;
        var netCalCounterIntervalId;

        var CHART_WIDTH = 500;
        var CHART_HEIGHT = 250;
        var ARC_X = CHART_WIDTH / 2;
        var ARC_Y = CHART_HEIGHT - 100;
        var ARC_OUTER_RADIUS = 0.274 * CHART_WIDTH; // 137;
        var ARC_INNER_RADIUS = 0.246 * CHART_WIDTH; // 123;
        var ARC_ANIMATION_TICKS = 10;
        var ARC_STROKE_WIDTH = 20;

        function arcAttrFunction(xloc, yloc, value, total, radius) {
            // Calc start position
            var startAngle = 180;
            var xStart = xloc + Math.cos(startAngle * Math.PI / 180) * radius;
            var yStart = yloc - Math.sin(startAngle * Math.PI / 180) * radius;

            // Calc arc length
            var alpha = 360 / total * value;
            var a = (startAngle - alpha) * Math.PI / 180;
            var xArc = xloc + radius * Math.cos(a);
            var yArc = yloc - radius * Math.sin(a);

            return {
                path: [
                    ["M", xStart, yStart],
                    ["A", radius, radius, 0, +(alpha > 180), 1, xArc, yArc]
                ]
            };
        }

        function stopAnimation() {
            paper.clear();
            clearTimeout(netCalCounterIntervalId);
            lblLeftContainer$.hide().stop(true, true);
        }

        function calculateCurrentGoal() {
            var weeklyWeightLossValue;
            var weightValue;
            var goalWeightValue;

            weightValue = _bioStats.WeightInPounds;
            goalWeightValue = _bioStats.TargetWeightInPounds;
            
            weeklyWeightLossValue = Math.max(0.1, Math.abs(_bioStats.TargetWeightChangePerWeek));

            var weeklyWeightDelta = parsePositiveFloat(weeklyWeightLossValue);
            var currentWeight = parsePositiveFloat(weightValue);
            var goalWeight = parsePositiveFloat(goalWeightValue);
            var weightDelta = goalWeight - currentWeight;

            if (weightDelta < 0) {
                weeklyWeightDelta = -weeklyWeightDelta;
            }
            
            var isChangingWeight = weightDelta != 0;
            var goal = Math.max(0, calculateRmrCalories(currentWeight) + ((!isChangingWeight) ? 0 : weeklyWeightDelta * CALORIES_PER_POUND));
            
            goal += _averageCaloriesBurned - 1;
            return goal * ((_historySelection) ? _historySelection.DayCount : 1);
        }

        return {
            init: function () {
                lblNet$ = $('#nutrition-overview-lbl-net');
                lblGoal$ = $('#nutrition-overview-lbl-goal');
                lblLeft$ = $('#nutrition-overview-lbl-left');
                lblLeftDirection$ = $('#nutrition-overview-lbl-left-direction');
                lblLeftContainer$ = $('#nutrition-overview-lbl-left').parent();

                // Build goal chart
                paper = Raphael("nutrition-overview-chart", CHART_WIDTH, CHART_HEIGHT);
                paper.customAttributes.arc = arcAttrFunction;
            },

            dataChangeNotifications: DATA_HISTORY_SELECTION | DATA_BIO_STATS,

            onDataChange: function () {
                $('#nutrition-overview-chart-container').fadeIn();
                $("#nutrition-widget-content").fadeIn();

                var burnCal = (_historySelection) ? _historySelection.Burn.TotalCalories : 0;
                var consumptionCal = (_historySelection) ? _historySelection.Consumption.TotalCalories : 0;
                var net = consumptionCal - burnCal + ((_historySelection) ? _historySelection.Burn.RmrCalories : 0);
                var nonNegNet = Math.max(0, net);
                var netAnimated = 0;
                var netAnimationIncrement = nonNegNet / ARC_ANIMATION_TICKS;
                var goal = calculateCurrentGoal();
                var pctToGoal = Math.min(1, nonNegNet / goal);
                // Update labels
                if (goal == 0) {
                    goal = _totalCaloriesGoal;
                }
                lblNet$.text(numberFormat(netAnimated));
                lblGoal$.text(numberFormat(goal));
                lblLeft$.text(numberFormat(Math.abs(goal - net)));
                lblLeftDirection$.text('Calories remaining');
                lblLeftContainer$.removeClass('over-limit');


                // Update graph
                stopAnimation();
                var graphHandler = paper.path().attr({
                    "stroke": '#efb41c',
                    "stroke-width": ARC_STROKE_WIDTH,
                    arc: [ARC_X, ARC_Y, 1, 2, ARC_OUTER_RADIUS]
                });

                $(graphHandler.node).attr('id', 'NutritionTipHandler');
                // Add tooltip
                var tipHtml = "<div style='float:left'>" +
                    "<div id='date-span'>Daily goal <br/>" + "<span id='steps-cals-span' >" + numberFormat(goal) + " CALORIES</span>" + "</div>" +
                    " <div id='date-span' style='margin-top: 10px;'>I've eaten  <br/>" + "<span id='steps-cals-span' >" + numberFormat(_historySelection.Consumption.TotalCalories) + " CALORIES</span></div>" + "</div> " +
                    "<div style='float:left; margin-left:15px;'><div id='date-span'>I've burned  <br/>" + "<span id='steps-cals-span' >" + numberFormat(_historySelection.Burn.TotalCalories) + " CALORIES</span>";
                if ((goal - net) >= 0) {
                    tipHtml += "</div><div id='date-span' style='margin-top: 10px;'> I can still eat <br/> " + "<span id='steps-cals-span' >" + numberFormat(Math.abs(goal - net)) + " CALORIES</span>" + "</div></div>";
                } else {
                    tipHtml += "</div><div id='date-span' style='margin-top: 10px;'> I need to burn <br/> " + "<span id='steps-cals-span' >" + numberFormat(Math.abs(goal - net)) + " CALORIES</span>" + "</div></div>";
                }
                    
                addTipToNutrition("#NutritionTipHandler", tipHtml, "tip-nutrition");

                // Animate
                if (net <= 0) {
                    lblNet$.text(numberFormat(net));
                    lblLeftContainer$.fadeIn();
                } else {
                    var arcAnimatePct = pctToGoal / (net / goal);
                    paper.path().attr({
                        "stroke": '#a12b21',
                        "stroke-width": ARC_STROKE_WIDTH,
                        arc: [ARC_X, ARC_Y, 0.01, 2, ARC_INNER_RADIUS]
                    }).animate({
                        "stroke-width": ARC_STROKE_WIDTH,
                        arc: [ARC_X, ARC_Y, pctToGoal, 2, ARC_INNER_RADIUS]
                    }, ANIMATION_MS * arcAnimatePct, "linear");

                    netCalCounterIntervalId = setInterval(function () {
                        netAnimated += netAnimationIncrement;
                        if (netAnimated >= net) {
                            netAnimated = net;
                        }
                        lblNet$.text(numberFormat(netAnimated));
                        if (netAnimated > goal) {
                            lblLeftDirection$.text('Over');
                            lblLeftContainer$.addClass('over-limit');
                        }
                        if (netAnimated >= net) {
                            lblLeftContainer$.fadeIn();
                            clearInterval(netCalCounterIntervalId);
                        }
                    }, ANIMATION_MS / ARC_ANIMATION_TICKS);
                }

                //populate calories calculation div
                $(".calculation-goal p").html(numberFormat(goal) + " calories");
                $(".calculation-eaten p").html(numberFormat(_historySelection.Consumption.TotalCalories) + " calories");
                var burnedCalories = _historySelection.Burn.TotalCalories - _historySelection.Burn.RmrCalories;
                $(".calculation-burned p").html(numberFormat(burnedCalories) + " calories");
                $(".calculation-net p").html(numberFormat(_historySelection.Consumption.TotalCalories - burnedCalories) + " calories");

                if (!_dataProvider.IsConnected) {
                    $('.overlayMFPConnect').css("display", "block");
                    $('.overlayMFPConnectContainer').show();
                    $('.overlayMFPLandingPageConnect').css("display", "block");
                    $('#nutrition-widget-content').css("display", "none");
                    $('#nutrition-partner-connect').css("display", "none");
                    $('.EditGoalContainer').css("display", "none");
                    console.log($('#nutrition-progress'));
                    $('#nutrition-progress-wrapper').hide();
                    
                }
            }
        };
    }


    function addTipToNutrition(object, txt, tipId) {

        var tipText = "";
        var over = false;

        var tip = $("#" + tipId).hide();
        $(document).mousemove(function (e) {
            e = e || window.event;

            if (over) {
                
                if (tipId == 'tip' && $.browser.msie && ($.browser.version >= 8.0)) {
                    tip.css("margin-left", e.offsetX);
                    tip.css("margin-top", e.offsetY - tip.height());
                } else if (!$.browser.mozilla) {
                    if ($.browser.msie && $.browser.version == 8.0) {
                        tip.css("left", e.offsetX + 110).css("top", e.offsetY - 15);
                    }
                    else
                        tip.css("left", e.offsetX).css("top", e.offsetY + 15);

                } else {
                    var offsetX = e.offsetX;
                    var offsetY = e.offsetY;
                    
                    if (offsetX == undefined || offsetY == undefined) {
                        // if we can't be fancy and make the popup placement dynamic, just fix it in one known visible spot
                        // this is a know issue for firefox.
                        var offset = $( e.target ).offset();
                        offsetX = offset.left - 275;
                        offsetY = -40;
                    } else {
                        offsetY = offsetY - tip.height() - 17;
                    }
                    tip.css( "margin-top", offsetY + 'px' );
                    tip.css( "margin-left", offsetX + 'px' );

                }
                $("#" + tipId + " #tip-data-nutrition").html(tipText);
            }
        });

        var tip = $("#" + tipId).hide();
        $(object).mouseover(function (e) {

            tipText = txt;
            tip.show();
            over = true;
            e = e || window.event;
            if (over) {
                if (tipId == 'tip' && $.browser.msie && ($.browser.version >= 8.0)) {
                    tip.css("margin-left", 35);
                    tip.css("margin-top", -135);
                } else if (!$.browser.mozilla) {
                    if ($.browser.msie && $.browser.version == 8.0) {
                        tip.css("left", e.offsetX + 110).css("top", e.offsetY - 15);
                    }
                    else
                        tip.css("left", e.offsetX).css("top", e.offsetY + 15);
                } else {
                    tip.css("margin-left", e.offsetX + 200);

                }
                $("#" + tipId + " #tip-data-nutrition").html(tipText);
            }

        }).mouseout(function (e) {
            tip.fadeOut(200);
            over = false;
        });
    }

    // Widget to manage history nav and consumption/burn pie charts
    function createTrackingWidget() {
        var today;                  // Date:  the date to use as the basis for navigation; this is the last day of data available in the history array
        var todayOffset = 0;        // int:  offset of the date, ranging from -history.length + 1 to 0.  0 shows today's data, -1 shows yesterday's, etc; if groupByWeek is set, it's the start of the week (always a Sunday)
        var groupByWeek = false;    // bool:  whether this JS code should aggregate to the week level

        function formatTooltip() {
            return '<b>' + this.point.name + '</b><br />' + formatSeriesValue.call(this, true);
        }

        function formatLegend() {
            var lbl = this.name;
            lbl += ': ' + formatSeriesValue.call(this, false);
            return lbl;
        }

        function formatSeriesValue(defaultUnit, showPct) {
            var value = numberFormat(this.y);
            var unit = (this.point) ? this.point.unit : this.unit;
            var pct = (showPct) ? ' (' + numberFormat(this.percentage) + '%)' : '';
            var extra = (this.point && this.point.extra) ? '<br />' + this.point.extra : '';
            return value + ' ' + unit + pct + extra;
        }

        function buildPieChartConfig(colors, legendItemCols, data) {
            return {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    spacingBottom: 0,
                    spacingTop: 0,
                    spacingLeft: 0,
                    spacingRight: 0,
                    width: 267,
                    height: 300,
                    margin: [0, 1, 115, 0],
                    animation: {
                        duration: ANIMATION_MS,
                        easing: 'linear'
                    },
                    style: {
                        fontFamily: 'inherit'
                    }
                },
                credits: {
                    enabled: false
                },
                colors: colors,
                title: {
                    text: null
                },
                legend: {
                    enabled: true,
                    floating: true,
                    align: 'left',
                    borderWidth: 0,
                    symbolWidth: 15,
                    itemWidth: 240 / legendItemCols,
                    itemStyle: {
                        color: '#888',
                        fontSize: CHART_FONT_SIZE,
                        paddingBottom: '8px',
                        fontFamily: 'inherit'
                    },
                    labelFormatter: formatLegend,
                    useHTML: true,
                    x: 25,
                    y: (5 - data.length) * -22
                },
                tooltip: {
                    formatter: formatTooltip,
                    headerFormat: '',
                    style: { fontFamily: 'inherit', fontSize: CHART_FONT_SIZE }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        borderWidth: 0,
                        cursor: 'pointer',
                        dataLabels: {
                            color: '#fff',
                            style: { fontSize: CHART_FONT_SIZE },
                            enabled: true,
                            formatter: function () {
                                return (this.percentage < 0.5) ? '' : (numberFormat(this.percentage) + '%');
                            },
                            distance: -20
                        },
                        point: {
                            events: {
                                legendItemClick: function () {
                                    return false;
                                }
                            }
                        },
                        showInLegend: true
                    }
                },
                series: [{
                    type: 'pie',
                    data: data
                }]
            };
        }

        function redrawSeries(selector, data) {
            var chart = $(selector).highcharts();
            if (!chart) {
                return;
            }

            // Update series
            var series = chart.series[0];
            for (var i = 0; i < data.length; i++) {
                series.data[i].update(data[i]);

                // Update legend manually rather than using chart.redraw() or series.update() since this approach allows for slick animated slice transitions
                chart.legend.allItems[i].legendItem.attrSetters.text(formatLegend.call(series.data[i]));
            }
        }

        function getVisibleDateRange() {
            // Make sure the offset is within bounds
            if (todayOffset <= -_history.length) {
                todayOffset = -(_history.length - 1);
            } else if (todayOffset > 0) {
                todayOffset = 0;
            }

            // If we're grouping by week, align offset to the start of the week
            var alignedOffset = todayOffset;
            if (groupByWeek) {
                // Align offset to start of week (Sunday)
                var alignedDay = new Date(today);
                alignedDay.setDate(alignedDay.getDate() + alignedOffset);
                todayOffset -= new Date(alignedDay).getDay();

                // Make sure we're still within range
                while (todayOffset <= -_history.length) {
                    todayOffset += 7;
                }
            }

            // Compute date range bounds based on "today" and "alignedOffset"
            var min = new Date(today);
            min.setDate(min.getDate() + todayOffset);
            var max = new Date(min);
            max.setDate(max.getDate() + ((groupByWeek) ? 7 : 1));
            return {
                min: min,
                max: max
            };
        }

        function getDateLabel(dateRange) {
            var startDateLabel = dateRange.min.format('dddd, MMMM d');
            if (groupByWeek) {
                return 'Week starting ' + startDateLabel;
            }
            return startDateLabel;
        }

        function syncData() {
            var dateRange = getVisibleDateRange();
            _historySelection = getHistoryForDateRange(dateRange);

            // Update overview chart
            onDataChange(DATA_HISTORY_SELECTION);

            if (!_historySelection) {
                return;
            }

            // Update consumption data
            var consumptionData = _historySelection.Consumption;
            var percentageConsumptionData = consumptionData;
            percentageConsumptionData.CarbsGrams = percentageConsumptionData.CarbsGrams * 4;
            percentageConsumptionData.ProteinGrams = percentageConsumptionData.ProteinGrams * 4;
            percentageConsumptionData.FatGrams = percentageConsumptionData.FatGrams * 9;
            redrawSeries('#nutrition-overview-chart-consumed', [{
                y: percentageConsumptionData.CarbsGrams,
                extra: '--------------------<br />' +
                           'Sugar: ' + numberFormat(consumptionData.SugarGrams) + ' grams<br />' +
                           'Starch: ' + numberFormat(Math.max(0, consumptionData.CarbsGrams - consumptionData.SugarGrams - consumptionData.FiberGrams)) + ' grams<br />' +
                           'Fiber: ' + numberFormat(consumptionData.FiberGrams) + ' grams'
            },
                percentageConsumptionData.ProteinGrams,
                percentageConsumptionData.FatGrams,
                percentageConsumptionData.SodiumMg / 1000]);
            $('#nutrition-overview-lbl-consumed').html("I&rsquo;ve eaten: " + numberFormat(consumptionData.TotalCalories) + " calories");

            // Update burn data
            var burnData = _historySelection.Burn;
            redrawSeries('#nutrition-overview-chart-burned', [
                burnData.RmrCalories,
                burnData.CardioCalories,
                burnData.GroupExerciseCalories,
                burnData.SportsCalories,
                burnData.StrengthTrainingCalories
            ]);
            $('#nutrition-overview-lbl-burned').html("I&rsquo;ve burned: " + numberFormat(burnData.TotalCalories) + " calories");

            // Update date label
            $('#nutrition-overview .lbl-date').html(getDateLabel(dateRange));

            // Update "Add a food entry" link
            if (_dataProvider.IsConnected) {
                var targetDate = new Date(today);
                targetDate.setDate(targetDate.getDate() + todayOffset);
                $('#nutrition-overview .chart-col-eaten .add-entry').removeClass('hide').attr('href', _dataProvider.UserAccountUrl.replace('${date}', targetDate.format('yyyy-MM-dd')));
            }

            // Disable unavailable nav elements
            var canGoBack = todayOffset > -((groupByWeek) ? (_history.length - 7) : (_history.length - 1));
            var canGoForward = todayOffset <= ((groupByWeek) ? -7 : -1);
            $('#nutrition-overview .go-left')[canGoBack ? 'removeClass' : 'addClass']('disabled');
            $('#nutrition-overview .go-right')[canGoForward ? 'removeClass' : 'addClass']('disabled');
        }

        function getHistoryForDateRange(dateRange) {
            var data = null;

            for (var i = 0; i < _history.length; i++) {
                var dayData = _history[i];
                if (dayData.Date >= dateRange.min && dayData.Date < dateRange.max) {
                    if (!data) {
                        data = $.extend(true, {}, dayData);
                        data.DayCount = (groupByWeek) ? 7 : 1;
                    } else {
                        for (var key in dayData.Burn) {
                            data.Burn[key] += dayData.Burn[key];
                        }
                        for (var key in dayData.Consumption) {
                            data.Consumption[key] += dayData.Consumption[key];
                        }
                    }
                }
            }

            return data;
        }

        return {
            init: function () {
                // Build pie charts
                $('#nutrition-overview-chart-consumed').highcharts(buildPieChartConfig(COLORS_CONSUMED, 1, [
                        { name: 'Carbohydrates', unit: 'calories' },
                        { name: 'Protein', unit: 'calories' },
                        { name: 'Fat', unit: 'calories' },
                        { name: 'Sodium', unit: 'mg', unitScale: 1000 }
                ]));
                $('#nutrition-overview-chart-burned').highcharts(buildPieChartConfig(COLORS_BURNED, 1, [
                        { name: 'RMR', unit: 'calories', href: '#' },
                        { name: 'Cardiovascular', unit: 'calories' },
                        { name: 'Group Exercise', unit: 'calories' },
                        { name: 'Sports/Recreation', unit: 'calories' },
                        { name: 'Strength Training', unit: 'calories' }
                ]));

                // Hook navigation events
                $('#nutrition-overview .go-left').click(function () {
                    if (!$(this).hasClass('disabled')) {
                        todayOffset -= (groupByWeek) ? 7 : 1;
                        syncData();
                    }
                    return false;
                });
                $('#nutrition-overview .go-right').click(function () {
                    if (!$(this).hasClass('disabled')) {
                        todayOffset += (groupByWeek) ? 7 : 1;
                        syncData();
                    }
                    return false;
                });
                $('#nutrition-overview .groupby').click(function () {
                    groupByWeek = $(this).hasClass('groupby-week');
                    $('#nutrition-overview .groupby-day')[groupByWeek ? 'removeClass' : 'addClass']('active');
                    $('#nutrition-overview .groupby-week')[groupByWeek ? 'addClass' : 'removeClass']('active');
                    syncData();
                    return false;
                });

                // Hook help
                window.Nutrition.showRmrHelp = function () {
                    $("#helpModalRMR").modal({
                        show: true
                    });
                    return false;
                };
            },

            dataChangeNotifications: DATA_HISTORY,

            onDataChange: function () {
                $('#nutrition-overview').fadeIn();
                today = _history[_history.length - 1].Date;
                syncData();
            }
        };
    }

    // Weight goal & history chart widget
    function createProgressWidget() {
        return {
            init: function () {
                var detail$ = $('#nutrition-progress-detail');

                $('#nutrition-progress-hide').click(function () {
                    if (detail$.hasClass('hidden')) {
                        $(this).text('Hide');
                        detail$.removeClass('hidden');
                    } else {
                        $(this).text('Show');
                        detail$.addClass('hidden');
                    }
                    return false;
                });
            },

            dataChangeNotifications: DATA_BIO_STATS,

            onDataChange: function () {

                $('#nutrition-progress').fadeIn();

                var goal = Math.round(_bioStats.TargetWeightInPounds);
                var history = _bioStats.WeightHistory;
                var weights = [];
                var iStart = Math.max(0, history.length - MAX_WEIGHT_HISTORY_DATA_POINTS);
                weights.push({ name: null, date: '', y: null });
                for (var i = iStart; i < history.length; i++) {
                    var label = (i == history.length - 1) ? 'Latest' : (i == iStart) ? 'Start' : '';
                    weights.push({
                        name: label,
                        date: history[i].Date.format('MM/dd/yy'),
                        y: Math.round(history[i].Value)
                    });
                }

                $('#nutrition-progress-chart').highcharts({
                    chart: {
                        type: 'line',
                        backgroundColor: '#fff',
                        height: 170,
                        margin: [30, 0, 0, 0],
                        style: {
                            fontFamily: 'inherit'
                        }
                    },
                    title: {
                        text: null
                    },
                    colors: ['#444', '#008'],
                    credits: {
                        enabled: false
                    },
                    legend: {
                        enabled: false
                    },
                    xAxis: {
                        lineWidth: 0,
                        lineColor: GRID_COLOR,
                        labels: {
                            enabled: false
                        },
                        tickLength: 0,
                        minPadding: 0.15,
                        maxPadding: 0.15
                    },
                    yAxis: {
                        lineWidth: 0,
                        lineColor: GRID_COLOR,
                        labels: {
                            enabled: false
                        },
                        gridLineWidth: 0,
                        plotLines: [{
                            color: '#9bcb9b',
                            width: 3,
                            value: goal,
                            dashStyle: 'shortdot'
                        }],
                        title: {
                            text: null
                        }
                    },
                    tooltip: {
                        headerFormat: '',
                        borderWidth: 0,
                        backgroundColor: "rgba(255,255,255,0)",
                        borderRadius: 0,
                        shadow: false,
                        pointFormat: '<div class="chart-tooltip">{point.date}</div>',
                        useHTML: true,
                        style: {
                            fontFamily: 'inherit'
                        }
                    },
                    plotOptions: {
                        line: {
                            dataLabels: {
                                formatter: function () {
                                    var pointCssName = (this.point.name) ? 'name name-non-empty' : 'name';
                                    return '<div class="chart-label chart-label-' + this.series.name + '"><div class="' + pointCssName + '">' + this.point.name + '</div><div class="value">' + this.y + '</div></div>';
                                },
                                enabled: true,
                                verticalAlign: 'middle',
                                useHTML: true,
                                style: {
                                    fontFamily: 'inherit'
                                }
                            },
                            lineWidth: 1,
                            color: '#c9cacd',
                            states: {
                                hover: {
                                    enabled: false
                                }
                            }
                        }
                    },
                    series: [{
                        name: 'goal',
                        data: [{ name: 'Goal', y: goal }]
                    }, {
                        name: 'actual',
                        data: weights
                    }]
                });
            }
        };
    }

    function createWeightTrackingWidget() {
        var relativeLargestWeight;
        var target;
        var defaultTarget = 100;
        var weightArrayValues = new Array();
        var dateArrayValues = new Array();
        var largestNumberOfWeight;
        var maxArrayItems = 5;
        var lblWeight$;
        var lblTarget$;
        var lblWeightLost$;
        var lblTimeLeft$;
        var isFirefox;

        return {
            init: function () {
                lblWeight$ = $('#nutrition-current-weight-value');
                lblTarget$ = $('#nutrition-target-weight-value');
                lblWeightLost$ = $('#nutrition-weight-lost-value');
                lblTimeLeft$ = $('#nutrition-left-time-value');

                isFirefox = typeof InstallTrigger !== 'undefined';
            },

            dataChangeNotifications: DATA_BIO_STATS,

            onDataChange: function () {
                $('#nutrition-weight-container').html(''); // clear it out so we don't have a duplicate graph after refresh
                generateGraph();
                populateSummaryData();
            }
        };

        function populateSummaryData() {
            lblWeight$.text(Math.round(_bioStats.WeightInPounds));
            lblTarget$.text(Math.round(_bioStats.TargetWeightInPounds));
            var weightLost = Math.round(_bioStats.WeightHistory[0].Value - _bioStats.WeightInPounds);

            if (weightLost < 0) {
                $("#nutrition-weight-lost p").text("Weight gained:");
                lblWeightLost$.text(Math.abs(weightLost));
            } else {
                lblWeightLost$.text(Math.round(weightLost));
            }


            var weightDelta = parsePositiveFloat(lblTarget$.text()) - parsePositiveFloat(lblWeight$.text());
            var weeklyWeightDelta = parsePositiveFloat(_bioStats.TargetWeightChangePerWeek);
            if (weightDelta < 0) {
                weeklyWeightDelta = -weeklyWeightDelta;
            }
            var timeToGoal = Math.max(0, Math.ceil(Math.abs(weightDelta / weeklyWeightDelta)));
            if (timeToGoal == Infinity) {
                lblTimeLeft$.text('Never');
            } else {
                lblTimeLeft$.text(numberFormat(timeToGoal) + ' ' + (timeToGoal == 1 ? 'week' : 'weeks') + ' (est.)');
            }
        }

        function generateGraph() {
            var emptyItems = 0;
            for (var i = 0; i < maxArrayItems; i++) {
                if (_bioStats.WeightHistory[i] != null && _bioStats.WeightHistory[i].Value > 0) {
                    var date = new Date(_bioStats.WeightHistory[i].Date);
                    var dateString = date.getMonth() + 1 + "/" + date.getDate();
                    weightArrayValues.push(Math.round(_bioStats.WeightHistory[i].Value));
                    dateArrayValues.push(dateString);
                } else {
                    emptyItems++;
                    weightArrayValues.push(0.1);
                    dateArrayValues.push("");
                }
            }

            var nutrtionGraphHandler = GraphPluginModule.DrawGraph(weightArrayValues,
             _bioStats.TargetWeightInPounds,
             5,
             195,
             25,
            ['#268b51', "#268b51", '#268b51'],
            'nutrition-weight-container',
            '/v2/Content/Images/LandingPageV3/activitygraph-bg-',
            dateArrayValues,
            "nutrition",
           '');
        }

        function calculateRelativeLargestWeight() {
            if (target > defaultTarget && target > (largestNumberOfWeight + 0.1 * target)) {
                relativeLargestWeight = target + 0.1 * target;
            }
            else if (largestNumberOfWeight < target) {
                relativeLargestWeight = target * 1.1;
            } else if (largestNumberOfWeight == target) {
                relativeLargestWeight = 1.1 * target;
            } else if (largestNumberOfWeight > target && largestNumberOfWeight < target * 1.1) {
                relativeLargestWeight = 1.1 * target;
            } else {
                relativeLargestWeight = largestNumberOfWeight;
            }
            return relativeLargestWeight;
        }
    }

    function parsePositiveFloat(str) {
        var val = parseFloat(str);
        return (isNaN(val) || val < 0) ? 0 : val;
    }

    // Weight goal dialog widget
    function createWeightGoalDialog() {
        var currentWeight$;
        var goalWeight$;
        var weeklyWeightLoss$;
        var dailyCalorieGoal$;
        var timeToGoalWeight$;


        function syncCalorieForm() {
            // Calculate desired weight delta
            var currentWeight = parsePositiveFloat(currentWeight$.val());
            var goalWeight = parsePositiveFloat(goalWeight$.val());
            var weightDelta = goalWeight - currentWeight;
            var isChangingWeight = weightDelta != 0;

            // Get weekly weight delta
            var weeklyWeightDelta = parsePositiveFloat(weeklyWeightLoss$.val());
            if (weightDelta < 0) {
                weeklyWeightDelta = -weeklyWeightDelta;
            }

            // Update "Weekly weight gain/loss" group
            $('#nutrition-weight-weekly-group')[(!isChangingWeight) ? 'addClass' : 'removeClass']('invisible');
            $('#nutrition-weight-weekly-lbl').text((weightDelta > 0) ? 'Weekly weight gain:' : 'Weekly weight loss:');
            TARGET_CHANGES_PER_WEEK = weeklyWeightDelta;
            // Update daily calorie goal
            var calorieGoal = Math.max(0, calculateRmrCalories(currentWeight) + ((!isChangingWeight) ? 0 : weeklyWeightDelta * CALORIES_PER_POUND));

            calorieGoal += _averageCaloriesBurned;
            _totalCaloriesGoal = calorieGoal;

            dailyCalorieGoal$.text(numberFormat(calorieGoal)).attr('data-value', calorieGoal);

            // Update "Time to goal weight" group
            var timeToGoal = Math.max(0, Math.ceil(Math.abs(weightDelta / weeklyWeightDelta)));
            $('#nutrition-weight-time-group')[(!isChangingWeight) ? 'addClass' : 'removeClass']('invisible');
            if (timeToGoal == Infinity) {
                timeToGoalWeight$.text('Never');
            } else {
                timeToGoalWeight$.text(numberFormat(timeToGoal) + ' ' + (timeToGoal == 1 ? 'week' : 'weeks') + ' (est.)');
            }
        }

        function serializeAndValidateForm() {
            var data = {
                WeightInPounds: parseFloat(currentWeight$.val()),
                TargetWeightInPounds: parseFloat(goalWeight$.val()),
                TargetWeightChangePerWeek: parseFloat(weeklyWeightLoss$.val()),
                DailyCalorieConsumptionTarget: parseInt(dailyCalorieGoal$.attr('data-value'))
            };

            // Validate current weight
            if (isNaN(data.WeightInPounds)) {
                markError(currentWeight$, 'Please enter a number for your current weight.');
                return false;
            }
            if (data.WeightInPounds < 80) {
                markError(currentWeight$, 'Current weight must be at least 80 pounds.');
                return false;
            }
            if (data.WeightInPounds > 400) {
                markError(currentWeight$, 'Current weight cannot be more than 400 pounds.');
                return false;
            }

            // Validate goal weight
            if (isNaN(data.TargetWeightInPounds)) {
                markError(goalWeight$, 'Please enter a number for your goal weight.');
                return false;
            }
            if (data.TargetWeightInPounds < 80) {
                markError(goalWeight$, 'Goal weight must be at least 80 pounds.');
                return false;
            }
            if (data.TargetWeightInPounds > 400) {
                markError(goalWeight$, 'Goal weight cannot be more than 400 pounds.');
                return false;
            }

            // Validate weekly weight loss
            if (data.WeightInPounds == data.TargetWeightInPounds) {
                data.TargetWeightChangePerWeek = 0;
            } else {
                var weightLossName = (data.WeightInPounds > data.TargetWeightInPounds) ? 'loss' : 'gain';
                if (isNaN(data.TargetWeightChangePerWeek)) {
                    markError(weeklyWeightLoss$, 'Please enter a number for your weekly weight ' + weightLossName + '.');
                    return false;
                }
                if (data.TargetWeightChangePerWeek < 0.1) {
                    markError(weeklyWeightLoss$, 'Weekly weight ' + weightLossName + ' must be at least 0.1 pounds.');
                    return false;
                }
                if (data.TargetWeightChangePerWeek > 2) {
                    markError(weeklyWeightLoss$, 'Weekly weight ' + weightLossName + ' cannot exceed 2 pounds.');
                    return false;
                }

                // Validate calorie change
                var calorieLimit = (_bioStats.IsMale) ? MIN_CALORIES_FOR_MALE : MIN_CALORIES_FOR_FEMALE;
                var calories = data.DailyCalorieConsumptionTarget;
                if (calories < calorieLimit) {
                    if (!confirmWarning(weeklyWeightLoss$, 'WARNING:  U.S. dietary guidelines recommend ' + ((_bioStats.IsMale) ? 'men' : 'women') + ' should not consume fewer than ' + numberFormat(calorieLimit) + ' calories per day.\n\nClick OK to ignore these guidelines, or Cancel to revise your weekly weight ' + weightLossName + ' goal.')) {
                        return false;
                    }
                }
            }

            return data;
        }

        function setFormFields() {
            if (_bioStats.TargetWeightInPounds == 0) {
                _bioStats.TargetWeightInPounds = _bioStats.WeightInPounds;
            }
            currentWeight$.val(numberFormat(_bioStats.WeightInPounds));
            goalWeight$.val(numberFormat(_bioStats.TargetWeightInPounds));
            weeklyWeightLoss$.val(Math.max(0.1, Math.abs(_bioStats.TargetWeightChangePerWeek)));
            syncCalorieForm();
        }

        function showDialog(initialConnect) {
            setFormFields();
            $('#nutrition-weight-title-connect')[initialConnect ? 'show' : 'hide']();
            $('#nutrition-weight-title-edit')[!initialConnect ? 'show' : 'hide']();
            $('#nutrition-weight-dlg').modal('show');
        }

        return {
            init: function () {
                $('.btn-edit-goal').click(function () {
                    showDialog(false);
                    return false;
                });
                var dlg$ = $('#nutrition-weight-dlg');
                currentWeight$ = $('#nutrition-weight-weight');
                goalWeight$ = $('#nutrition-weight-goal');
                weeklyWeightLoss$ = $('#nutrition-weight-weekly');
                dailyCalorieGoal$ = $('#nutrition-weight-calories');
                timeToGoalWeight$ = $('#nutrition-weight-time');

                dlg$.find('input').on('change', syncCalorieForm);

                dlg$.on('shown', function () {
                    $('input:first').focus().select();
                });

                $('#nutrition-weight-save').click(function () {
                    var data = serializeAndValidateForm();
                    if (!data) {
                        return;
                    }

                    $.ajax({
                        url: API_BASE_URL + '/goals/weight',
                        type: 'POST',
                        data: data
                    }).done(function (bioStats) {
                        dlg$.modal('hide');
                        _bioStats = $.extend({}, _bioStats, bioStats);
                        onDataChange(DATA_BIO_STATS);
                    }).fail(function (xhr) {
                        handleApiError('Unable to update weight goal', xhr);
                    });
                });
            },

            dataChangeNotifications: DATA_PROVIDER_CONNECTION,

            onDataChange: function () {
                if (_dataProvider.IsConnected) {
                    showDialog(true);
                }
            }
        };
    }




    function createRewardListWidget() {
        return {
            init: function () {
                $.ajax({
                    url: API_BASE_URL + '/rewards',
                    type: 'GET',
                    success: function (data) {
                        if (data.length > 0) {
                            $('#nutritionRewards').removeClass('hide');
                            $(data).each(function () {
                                $('.rewards-content').append('<li class="reward">'
                                    + '<label>'
                                    + this.Amount
                                    + '</label>'
                                    + '<img src="https://mash.virginhealthmiles.com/static/images/RewardTypes/badge_hm_on.gif" alt="Reward Healthmile" style="width:50px; margin-left: 2px;">'
                                    + '<a class="tertiary reward-text" href="'
                                    + this.RewardUrl + '"'
                                    + '</a>'
                                    + this.Text + '</li>');

                            });
                        } else {
                            $('#nutritionRewards').addClass('hide');
                        }
                    },
                    error: function () {
                        $(".item-loading").remove();
                        $(".nutrition-next-rewards").empty();
                        $(".nutrition-next-rewards").append('<p>Something went wrong, please try again!</p>');
                    }
                });
            }
        };
    }


    function createFriendListWidget() {
        $(".modal-header button").hover(
        function () {
            $(this).addClass("ui-state-hover");
        }, function () {
            $(this).removeClass("ui-state-hover");
        });

        $(".friend-name").each(function () {
            if ($(this).text().length > 17) {
                $(this).tooltip();
            }
        });

        $(".tell-a-friend-list").append('<li class="tell-a-friend-item item-loading"></li>');
        $.ajax({
            url: API_BASE_URL + '/friends?pageNumber=' + 0 + '&count=' + 5,
            type: 'GET',
            success: function (data) {
                if (data == null || data.MemberFriends.length == 0)
                    return;

                $("#tell-a-friend").fadeIn();
                if (data.TotalCount > 5) {
                    $("#tell-a-friend-detail button").show();
                }
                $(".item-loading").remove();
                $("#tell-a-friend-detail").attr("data-friends", data.TotalCount);
                var html = friendItemTemplate(data);
                $(".tell-a-friend-list").append(html);
                $('.tell-a-friend-item a').tooltip();
                initShareClick();
            },
            error: function () {
                $(".item-loading").remove();
                $(".tell-a-friend-list").empty();
                $(".tell-a-friend-list").append('<li class="tell-a-friend-item"><p>Something went wrong, please try again!</p></li>');
            }
        });

        $(".more-friends").click(function (e) {
            if ($(".more-friends-list").children().length == 0) {
                getMemberFriends(CURRENT_PAGE, TOTAL_FRIEND_COUNT_PER_PAGE);
            }
        });
        $('.tell-a-friend-item a').tooltip();

        $(".more-friends-body button").click(function (e) {
            getMemberFriends(CURRENT_PAGE, TOTAL_FRIEND_COUNT_PER_PAGE);
        });
    }

    function getMemberFriends(pageNumber, count) {
        $(".more-friends-list").append('<li class="tell-a-friend-item item-loading"></li>');
        $.ajax({
            url: API_BASE_URL + '/friends?pageNumber=' + pageNumber + '&count=' + count,
            type: 'GET',
            success: function (data) {
                $(".item-loading").remove();
                var html = friendItemTemplate(data);
                $(".more-friends-list").append(html);
                $(".more-friends-list .tell-a-friend-item p").css("width", "223px");
                $('.tell-a-friend-item a').tooltip();
                CURRENT_PAGE++;
                initShareClick();
                if (!data.HasMore) {
                    $(".more-friends-body button").remove();
                }
            },
            error: function () {
                $(".item-loading").remove();
                $(".more-friends-list").empty();
                $(".more-friends-list").append('<li class="tell-a-friend-item"><p>Something wnet wrong, please try again!</p></li>');
            }
        });
    }

    function initShareClick() {
        $('.tell-a-friend-item a').click(function (e) {
            var event = $(this);
            $(this).hide();
            var memberId = $(this).data("friend");
            $.ajax({
                url: API_BASE_URL + '/friends/notifications',
                type: 'POST',
                data: { MemberId: memberId },
                dataType: 'json',
                success: function () {
                    event.closest("li").remove();
                },
                error: function () {
                    event.show();
                }
            });
            e.preventDefault();
        });
    }

    function friendItemTemplate(data) {
        var html = "";
        $.each(data.MemberFriends, function () {
            html += '<li class="tell-a-friend-item">';
            html += '<div>';
            html += '<img src="' + $(this)[0].ImageUrl + '" alt="Alternate Text">';
            html += '<p class="tertiary friend-name" data-placement="left" data-toggle="tooltip" data-original-title="' + $(this)[0].FirstName + ' ' + $(this)[0].LastName + '">' + $(this)[0].FirstName + ' ' + $(this)[0].LastName + '</p>';
            html += '<a href="#" data-toggle="tooltip" data-placement="left" data-friend=' + $(this)[0].MemberId + ' data-original-title="Recommend that  ' + $(this)[0].FirstName + ' check out Nutrition">Share</a>';
            html += '</div>';
            html += '</li>';
        });
        return html;
    }


    function calculateRmrCalories(latestWeight) {
        if (_bioStats.WeightInPounds == 0 || _bioStats.HeightInInches == 0 || _bioStats.AgeInYears == 0) {
            // Not enough data to calculate
            return 0;
        }

        var weightKg = latestWeight / KG_PER_POUND;
        var heightCm = _bioStats.HeightInInches / CM_PER_INCH;
        var genderOffset = (_bioStats.IsMale) ? 5 : -161;

        // Male:   RMR (kcal/day) = (WeightInPounds in kg x 10) + (HeightInInches in cm x 6.25) – (AgeInYears x 5) + 5
        // Female: RMR (kcal/day) = (WeightInPounds in kg x 10) + (HeightInInches in cm x 6.25) – (AgeInYears x 5) – 161
        return Math.round((weightKg * 10 + heightCm * 6.25 - _bioStats.AgeInYears * 5 + genderOffset));
    }

    //Nutrition guided goal
    var weightFinalGoal = 0;
    var weightDeltaGlobal = 0;

    $('#guided-goal-step1-next').on('click', function () {
        $('#guided-goal-step1').hide();
        var currentWeight = parsePositiveFloat($('#guided-current-weight').val());
        var targetWeight = parsePositiveFloat($('#lose-gain-weight').val());
        weightDeltaGlobal = currentWeight - targetWeight;
        if (weightFinalGoal == 2) {
            CalculateGuidedGoal();
            $('#guided-goal-step3').fadeIn();
        }
        else
            $('#guided-goal-step2').fadeIn();
    });
    $('#guided-goal-step2-next').on('click', function () {
        $('#guided-goal-step1').hide();
        $('#guided-goal-step2').hide();
        //STEP 3 Calculations
        CalculateGuidedGoal();
        $('#guided-goal-step3').fadeIn();
    });
    $('#guided-goal-step2-back').on('click', function () {
        $('#guided-goal-step1').fadeIn();
        $('#guided-goal-step2').hide();
        $('#guided-goal-step3').hide();
    });
    $('#guided-goal-step3-save').on('click', function () {
        var data = {
            WeightInPounds: parseFloat($('#nutrition-current-weight-value .value').text()),
            TargetWeightInPounds: parseFloat($('#nutrition-lose-weight-value .value').text()),
            TargetWeightChangePerWeek: parseFloat($('#nutrition-weekly-loss-weight-value .value').text()),
            DailyCalorieConsumptionTarget: parseFloat($('#nutrition-daily-calories-goal').text())
        };

        $.ajax({
            url: API_BASE_URL + '/goals/weight',
            type: 'POST',
            data: data
        }).done(function (bioStats) {
            $('#nutrition-weight-guided-dlg').modal('hide');
            _bioStats = $.extend({}, _bioStats, bioStats);
            onDataChange(DATA_BIO_STATS);
        }).fail(function (xhr) {
            handleApiError('Unable to update weight goal', xhr);
        });


    });
    $('#guided-goal-step3-back').on('click', function () {
        $('#guided-goal-step1').fadeIn();
        $('#guided-goal-step2').hide();
        $('#guided-goal-step3').hide();
    });

    $('.btn-edit-guided-goal').click(function () {
        $('#guided-current-weight').val(numberFormat(_bioStats.WeightInPounds));
        $('#lose-gain-weight').val(numberFormat(_bioStats.TargetWeightInPounds));
        $('#nutrition-weight-guided-dlg').modal('show');
    });

    //Select your goal type
    $('#loseWeight').click(function () {
        $('.div-currentweight').removeClass('hide').addClass('show');
        $('.div-howmuchtolose').removeClass('hide').addClass('show');
        weightFinalGoal = 1;
        $('.lose-gain-label').html('lose');
        $('.lose-gain-caps-label').html('Lose');
        $('.lose-gain-caps-label-step3').html('loss');
    });
    $('#maintainWeight').click(function () {
        $('.div-currentweight').removeClass('hide').addClass('show');
        weightFinalGoal = 2;
        $('.div-howmuchtolose').addClass('hide').removeClass('show');

    });
    $('#gainWeight').click(function () {
        $('.div-currentweight').removeClass('hide').addClass('show');
        $('.div-howmuchtolose').removeClass('hide').addClass('show');
        weightFinalGoal = 3;
        $('.lose-gain-label').html('gain');
        $('.lose-gain-caps-label').html('Gain');
        $('.lose-gain-caps-label-step3').html('gain');
    });
    //Select goal pace
    $('#loseItEasyWeight').click(function () {
        $('#weight-goal-value').val('0.5');
        $('.agressive-weight-loss-notification').addClass('hide');
    });
    $('#loseItModerateWeight').click(function () {
        $('#weight-goal-value').val('1.0');
        $('.agressive-weight-loss-notification').addClass('hide');
    });
    $('#loseItAgressivesWeight').click(function () {
        $('#weight-goal-value').val('1.5');
        $('.agressive-weight-loss-notification').removeClass('hide');
        $('#weight-goal-value-text').text('1.5');
    });
    $('#loseItIntenseWeight').click(function () {
        $('#weight-goal-value').val('2.0');
        $('.agressive-weight-loss-notification').removeClass('hide');
        $('#weight-goal-value-text').text('2.0');
    });
    $('.try-doing-yourself-dialog').click(function () {
        $('#nutrition-weight-guided-dlg').modal('hide');
        $('.btn-edit-goal').trigger('click');

    });
    $('.try-guide-me-dialog').click(function () {
        $('#nutrition-weight-dlg').modal('hide');
        $('.btn-edit-guided-goal').trigger('click');
    });

    function CalculateGuidedGoal() {

        var weeklyWeightDelta = parsePositiveFloat($('#weight-goal-value').val());
        var currentWeight = parsePositiveFloat($('#guided-current-weight').val());
        var targetWeight = 0;
        if (weightFinalGoal != 2) {
            targetWeight = parsePositiveFloat($('#lose-gain-weight').val());
            $('.weeklyweightgoal').show();
            $('.timetogoal').show();
        } else {
            targetWeight = currentWeight;
            $('.weeklyweightgoal').hide();
            $('.timetogoal').hide();
        }
        var weightDelta = currentWeight - targetWeight;


        $('#nutrition-current-weight-value .value').text(currentWeight);
        $('#nutrition-lose-weight-value .value').text(targetWeight);
        $('#nutrition-weekly-loss-weight-value .value').text(weeklyWeightDelta);
        TARGET_CHANGES_PER_WEEK = weeklyWeightDelta;

        if (TARGET_CHANGES_PER_WEEK != undefined) {
            weeklyWeightDelta = TARGET_CHANGES_PER_WEEK;
        }
        if (weightDelta > 0 && weightFinalGoal == 1) {
            weeklyWeightDelta = -weeklyWeightDelta;
        }



        var isChangingWeight = weightDelta != 0;
        var goal = Math.max(0, calculateRmrCalories(currentWeight) + ((!isChangingWeight) ? 0 : weeklyWeightDelta * CALORIES_PER_POUND));

        goal += _averageCaloriesBurned - 1;
        var calorieGoal = goal * ((_historySelection) ? _historySelection.DayCount : 1);
        $('#nutrition-daily-calories-goal').text(numberFormat(calorieGoal));

        var timeToGoal = Math.max(0, Math.ceil(Math.abs(weightDelta / weeklyWeightDelta)));
        $('#nutrition-weight-time-group')[(!isChangingWeight) ? 'addClass' : 'removeClass']('invisible');
        if (timeToGoal == Infinity) {
            $('#nutrition-weekly-goal-weight').text('Never');
        } else {
            $('#nutrition-weekly-goal-weight').text(numberFormat(timeToGoal) + ' ' + (timeToGoal == 1 ? 'week' : 'weeks') + ' (est.)');
        }

    }
})();
$(document).ready(function () {
    if ($.browser.version == 8.0) {
        $('.control-group .guided-goal-input').addClass('input-radioie8');
        $('#guided-current-weight, #lose-gain-weight').addClass('input-textie8');
        $('.guided-goal-label').addClass('input-radioie8-nobg');
        $('.weight-intensity-note').addClass('weight-intensity-note-margl');
    }
});