﻿
function ChallengeController( $scope ) {

    $.ajax({
        url: '/v2/challenges/GetChallenge',
        data: { challengeId: self.ChallengeId },
        cache: false,
        success: function (c) {
            $scope.Challenge = c;
            self.ChallengeQuestion(c.ChallengeQuestions[0].Question);
            startDate = parseDate(c.StartDate, 'MM/dd/yyyy');
            self.StartDate(parseDate(c.StartDate, 'MMM/dd'));
            endDate = parseDate(c.EndDate, 'MM/dd/yyyy');
            self.EndDate(parseDate(c.EndDate, 'MMM/dd/yyyy'));
        }
    });
    
}