﻿var ChAbout = ChAbout || {}; //this is the "about challenges" object - defined on the AboutChallenges.cshtml
var ChCreateKnowAndGo = ChCreateKnowAndGo || {}; //this is the "about challenges" object - defined on the AboutChallenges.cshtml
/* **************ABOUT CHALLENGES ************************** */
ChAbout.PopulateButtonsTextAndOnClick = function () {
    //populate the buttons' text
    ChAbout.$btnIndividuals.prop('value', ChAbout.IndividualsOnly);
    ChAbout.$btnTeams.prop('value', ChAbout.Teams);
    ChAbout.$btnLetsDoIt.prop('value', ChAbout.LetsGo);
    //populate the onclick events for buttons

    ChAbout.$btnIndividuals.click(ChAbout.RedirectToIndividualPlayers);
    ChAbout.$btnTeams.click(ChAbout.RedirectToTeams);
    ChAbout.$btnLetsDoIt.click(ChAbout.RedirectToCreateKnowAnGo);
};

ChAbout.RedirectToIndividualPlayers = function () {
    window.location.href = '/Secure/Challenge/create.aspx?teams=N&challengeid=';
};

ChAbout.RedirectToTeams = function () {
    window.location.href = '/Secure/Challenge/create.aspx?teams=Y&challengeid=';
};

ChAbout.RedirectToCreateKnowAnGo = function () {
    //use href rather than replace. The replace function is replacing the curent url in browser history so the "Back" button doesn't work.
    //window.location.replace(strMyGroupsURL);
    window.location.href = '/v2/challenges/createPersonalChallenge?persChallengeType=KAG';
};
ChAbout.RedirectToInvite = function () {
    //use href rather than replace. The replace function is replacing the curent url in browser history so the "Back" button doesn't work.
    //window.location.replace(strMyGroupsURL);
    window.location.href = '/v2/challenges/Invitation';
};
/* **************CREATE A PERSONAL KNOW AND GO************************** */
ChCreateKnowAndGo.PopulateButtonsTextAndOnClick = function () {
    //populate the buttons' text
    ChCreateKnowAndGo.$btnOnlyForMe.prop('value', ChCreateKnowAndGo.OnlyForMe);
    ChCreateKnowAndGo.$btnInviteFriends.prop('value', ChCreateKnowAndGo.InviteFriends);
    
    //populate the onclick events for buttons
    //ChCreateKnowAndGo.$btnOnlyForMe.click(ChAbout.RedirectToIndividualPlayers);
    ChCreateKnowAndGo.$btnInviteFriends.click(ChAbout.RedirectToInvite);

};
ChCreateKnowAndGo.PopulateTextAndTextAreas = function () {
    //populate the text box and text area's watermarks and behaviours
    ChCreateKnowAndGo.$questionBox.focus(function () {
        $(this).removeClass('grayWatermark');
        $(this).val("");
    });
    ChCreateKnowAndGo.$questionBox.blur(function () {
        $(this).addClass('grayWatermark');
        $(this).val(ChCreateKnowAndGo.QuestionBoxWatermark);
    });
    //TODO need to add the onlcik event to remove the watermark and allow typing
//    ChCreateKnowAndGo.$questionBox.keyup(function () {
//        if (ChCreateKnowAndGo.$questionBox.val() != ChCreateKnowAndGo.QuestionBoxWatermark) {
//            var content = ChCreateKnowAndGo.$questionBox.val();
//            alert('Content has been changed');
//        }
//    });
    ChCreateKnowAndGo.$detailsTextArea.addClass('grayWatermark');
    ChCreateKnowAndGo.$detailsTextArea.val(ChCreateKnowAndGo.DetailsTextAreaWatermark);
    ChCreateKnowAndGo.$detailsTextArea.focus(function () {
        $(this).removeClass('grayWatermark');
        $(this).val("");
    });
    ChCreateKnowAndGo.$detailsTextArea.blur(function () {
        $(this).addClass('grayWatermark');
        $(this).val("Any rules? Prizes?");
    });

};

/* **************GENERAL AND GLOBAL FUNCTIONALITY************************** */
//Use this to help removing any of the special encoding that can happen while retriving content back from server - Ektron (e.g. & will return as &amp; ; ' will return as &#39; and so on)
function jDecode(str) {
    return $("<div/>").html(str).text();
}