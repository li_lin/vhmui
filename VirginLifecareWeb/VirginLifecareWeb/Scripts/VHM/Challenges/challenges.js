﻿
Number.prototype.toOrdinal = function () {
    var n = this % 100;
    var suffix = ['th', 'st', 'nd', 'rd', 'th'];
    var ord = n < 21 ? (n < 4 ? suffix[n] : suffix[0]) : (n % 10 > 4 ? suffix[0] : suffix[n % 10]);

    if (this == 0) {
        return '';
    } else {
        return this + ord;
    }
};

function ChallengeModel(memberImage) {
    var self = this;
    var chats = [];
    var trackedDays = [];
    var challengeMembers = [];

    var yesCount = 0;
    var startDate = '';
    var endDate = '';
    var img = memberImage;
    var viewerName = '';

    self.ALLOWED_CHARS = 340;
    self.ShowErrors = true;//this is mainly for testing
    self.Post = ko.observable('');
    self.Messages = ko.observableArray(chats);
    self.TrackedDays = ko.observableArray(trackedDays);
    self.ChallengeMembers = ko.observableArray(challengeMembers);
    self.ViewerName = ko.observable(viewerName);

    // get challengeId from url
    self.ChallengeId = $(location).attr('href').substring($(location).attr('href').lastIndexOf('/') + 1);


    self.Today = ko.computed(function () {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        today = mm + '/' + dd + '/' + yyyy;
        return today;
    }, this);

    self.Challenge = ko.observable({});
    self.MemberImage = img;
    self.Content = ko.observable({});
    self.ChallengeQuestion = ko.observable('');
    self.YesCount = ko.observable(yesCount);
    self.Rank = ko.observable(0);
    // leader board page size attributes
    self.PageSizes = ko.observableArray([5, 15, 30, 50, 100]);
    self.SelectedPageSize = ko.observable(self.PageSizes()[0]);

    self.StartDate = ko.observable(startDate);
    self.EndDate = ko.observable(endDate);

    self.PostLength = ko.computed(function () {
        var len = self.ALLOWED_CHARS - self.Post().length;
        return len;
    }, this);

    self.AllowTyping = function () {
        return self.PostLength() > 0;
    };
    self.InspriationalMessage = function () {

        try {

            var inspirationalMessages = ['Way To Go', 'Good Job', 'Keep It Up', 'Woo hoo!', 'Nice One', 'Hooray', 'High 5!', 'Awesome!'];
            var random = Math.floor((Math.random() * inspirationalMessages.length - 1) + 1);
            return inspirationalMessages[random];
        } catch (e) {
            return '&nbsp;';
        }
    };

    /*ADD CHAT MESSAGE*/
    self.addMessage = function () {

        var message = self.Post();
        if (message.length > 0) {
            self.Messages.unshift({ NickName: self.ViewerName(), ImgSrc: self.MemberImage, FilteredMessageText: message, CreatedDateString: self.Today() });

            $.ajax({
                url: '/challenge/PersonalKag/PostChatMessage',
                data: { challengeId: self.ChallengeId, nickName: self.ViewerName(), message: message },
                cache: false,
                success: function () {
                    self.Post('');
                },
                error: function () {
                    if (self.ShowErrors) {
                        alert("uh oh, couldn't post your message");
                    }
                }
            });
        }
    };

    self.LoadLeaderBoard = function () {
        loadLeaderBoard();
    };

    self.GetModel = function () {
        loadChallenge();
    };

    var parseDate = function (jsonDate, pattern) {
        var d = jsonDate;
        var dtString = '';
        var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

        if (d.substring(0, 6) == "/Date(") {
            var dt = new Date(parseInt(d.substring(6, d.length - 2)));
            switch (pattern) {
                case 'MMM/dd':
                    {
                        dtString = months[dt.getMonth()] + " " + dt.getDate();
                        break;
                    }
                case 'MMM/dd/yyyy':
                    {
                        dtString = months[dt.getMonth()] + " " + dt.getDate() + ", " + dt.getFullYear();
                        break;
                    }
                default:
                    {
                        dtString = (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear();
                        break;
                    }
            }
        }
        return dtString;
    };

    var loadChallenge = function () {

        $.ajax({
            url: '/challenge/PersonalKag/GetChallenge',
            data: { challengeId: self.ChallengeId },
            cache: false,
            success: function (c) {
                self.Challenge(c.Challenge);
                self.ChallengeQuestion(c.Challenge.ChallengeQuestions[0].Question);
                startDate = parseDate(c.Challenge.StartDate, 'MM/dd/yyyy');
                self.StartDate(parseDate(c.Challenge.StartDate, 'MMM/dd'));
                endDate = parseDate(c.Challenge.EndDate, 'MM/dd/yyyy');
                self.EndDate(parseDate(c.Challenge.EndDate, 'MMM/dd/yyyy'));

                self.Messages(c.Chat);
                self.ChallengeMembers(c.LeaderBoard);
                self.Content(c.ContentRankings);
                self.ViewerName(c.ViewerName);
                $(c.TrackedDaysInfo.TrackedDays).each(function () {
                    var d = new self.TrackedDay(this.Answer, this.Day, this.ChallengeScoreId, this.ChallengeId, this.IsMemberAnswer);
                    trackedDays.push(d);
                });
                self.TrackedDays(trackedDays);
                yesCount = c.TrackedDaysInfo.DaysOfYes;
                self.YesCount(yesCount);
                self.Rank(c.Rank.toOrdinal());
            }
        });
    };

    var loadLeaderBoard = function () {

        $.ajax({
            url: '/challenge/PersonalKag/GetLeaderBoard',
            data: { challengeId: self.ChallengeId, pageNumber: 0, pageSize: self.SelectedPageSize() },
            cache: false,
            //async: false,
            success: function (members) {
                self.ChallengeMembers(members);
            }
        });
    };

    self.TrackedDay = function (answer, day, challengeScoreId, challengeId, isMemberAnswer) {

        var me = this;

        me.Answer = ko.observable(answer);
        me.Day = day;

        me.ChallengeScoreId = challengeScoreId;
        me.IsMemberAnswer = ko.observable(isMemberAnswer);
        me.ChallengeId = challengeId;


        var start = new Date(startDate);
        var end = new Date(endDate);
        var today = new Date(self.Today());
        var clickDate = new Date(day);

        me.ToggleAnswer = function () {
            if (clickDate >= start && clickDate <= end && clickDate <= today) {

                me.Answer((me.Answer() == 'True' ? 'False' : 'True'));

                $.ajax({
                    url: '/challenge/PersonalKag/SaveChallengeScore',
                    data: { challengeScoreId: me.ChallengeScoreId, challengeId: me.ChallengeId, scoreDate: me.Day, answer: me.Answer(), isMemberAnswer: me.IsMemberAnswer },
                    cache: false,
                    success: function () {
                        me.IsMemberAnswer(true);
                        yesCount += (me.Answer() == 'True') ? 1 : -1;
                        self.YesCount(yesCount);
                    },
                    error: function () {
                        if (self.ShowErrors) {
                            alert("uh oh, couldn't save your score");
                        }
                    }
                });

            }
        };
    };

    // get all the things...
    self.GetModel();

}




