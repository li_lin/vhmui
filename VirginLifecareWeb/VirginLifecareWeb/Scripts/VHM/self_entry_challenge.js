﻿var VHM = VHM || {};

var RESPONSE_TYPE_NUMERIC = 1;
var RESPONSE_TYPE_YESNO = 2;
var SEC_TASK_TYPE_WEIGHT = 'WGT';
var SEC_TASK_TYPE_WEIGHTLOSS = 'WTL';
var FREQ_TYPE_WEEKLY = "3";

VHM.SelfEntryChallenge = VHM.SelfEntryChallenge || {};

VHM.SelfEntryChallenge.ServiceURL = '../WebServices/Challenge.asmx/';
VHM.SelfEntryChallenge.ResponsType = VHM.SelfEntryChallenge.ResponsType || {};

VHM.SelfEntryChallenge.ResponsType.NUM = RESPONSE_TYPE_NUMERIC;
VHM.SelfEntryChallenge.ResponsType.YN = RESPONSE_TYPE_YESNO;
VHM.SelfEntryChallenge.FrequencyType = "2";

VHM.SelfEntryChallenge.TrackerDays = [];
VHM.SelfEntryChallenge.TrackerDays.startIdx = 0;
VHM.SelfEntryChallenge.DisplayDays = 0;
VHM.SelfEntryChallenge.FeatureID = -1;
VHM.SelfEntryChallenge.SECId = -1;
VHM.SelfEntryChallenge.IsComplete = false;
VHM.SelfEntryChallenge.HasEnded = false;
VHM.SelfEntryChallenge.MeasureSystem = "";

VHM.SelfEntryChallenge.TrackerDays.Next = function() {

    if ( VHM.SelfEntryChallenge.TrackerDays.startIdx < VHM.SelfEntryChallenge.TrackerDays.length - VHM.SelfEntryChallenge.DisplayDays ) {
        VHM.SelfEntryChallenge.TrackerDays.startIdx++;
    }
    ReBuildTracker( VHM.SelfEntryChallenge.DisplayDays );
};

VHM.SelfEntryChallenge.TrackerDays.Previous = function() {
    if ( VHM.SelfEntryChallenge.TrackerDays.startIdx > 0 ) {
        VHM.SelfEntryChallenge.TrackerDays.startIdx--;
        if ( VHM.SelfEntryChallenge.TrackerDays.startIdx < 0 ) {
            VHM.SelfEntryChallenge.TrackerDays.startIdx = 0;
        }
        ReBuildTracker( VHM.SelfEntryChallenge.DisplayDays )
    }
};

VHM.SelfEntryChallenge.Friends = {};

function weightTranslate( val ) {
    // only UK Imperial needs to be converted. Imperial is done on the server. Metric is just metric.
    switch ( VHM.SelfEntryChallenge.MeasureSystem ) {
        case 'Metric':
            {
                return val;
                break;
            }
        case 'Imperial':
            {
                return val;
                break;
            }
        case 'UK Imperial':
            {
                var stones = ConversionUtil.Weight.sysToImperialUK( val );
                return stones["stone"] + '&nbsp;st&nbsp;' + stones["lbs"];
                break;
            }
        default:
            {
                return val;
                break;
            }
    }
}

function SendInvites() {

    var sendTo = new Array();
    $( "input:checked" ).each( function () {
        sendTo.push( $( this ).attr( "user_id" ) );
    } );

    var data = { secId: VHM.SelfEntryChallenge.SECId, toIds: sendTo.join( ',' ) };
    var sData = JSON.stringify( data );

    $.ajax( {
        url: VHM.SelfEntryChallenge.ServiceURL + 'SaveTellFriends',
        contentType: "application/json; charset=utf-8",
        data: sData,
        type: 'POST',
        dataType: 'json',
        success: function ( ret ) {

            var oReturn = JSON.parse( ret.d );
            if ( oReturn.RefreshNewsFeed ) {
                $( "#newsContainer" ).empty();
                VHM.Social.NewsFeed.LoadPage( 0 );
            }
        }
    } );
}

function SetTrackerBackWidth( oTrackerMap, maxVal ) {

    // Get the base top and left
    var backPosition = $( "#trackerTable tbody" ).offset();
    $( "div#trackerBack" ).show().css( { "top": ( backPosition.top - 10 ) + "px", "left": backPosition.left + $( "td.trackerDate:last" ).width() + "px" } );

    // Get the base width
    var target = oTrackerMap.Target;
    var stretch = oTrackerMap.Stretch;

    if ( oTrackerMap.Target == null ) {
        target = maxVal;
    }
    if ( stretch == null ) {
        stretch = maxVal;
    }

    var wPct = target / maxVal;
    if ( wPct == 0 ) {
        wPct = 1;
    }
    var baseWidth = $( "#trackerTable" ).width() - $( "td.trackerDate:last" ).width();
    var backWidth = ( baseWidth * wPct );

    $( "div#trackerBack" ).width( backWidth + 'px' );
    $( "div#trackerBack" ).height( ( $( "#trackerTable tbody" ).height() + 10 ) + "px" );

    var targetLeft = parseInt( $( "div#trackerBack" ).css( "left" ) ) + $( "div#trackerBack" ).width();

    // Get the target percentage
    var targetDif = ( stretch > 0 ? stretch : maxVal ) - target;
    var targetWidth = 0;
    if ( target > 0 ) {
        $( "div#trackerBackTarget" ).show().css( { "top": backPosition.top - 10 + "px", "left": targetLeft + "px" } );
        $( "img.tbackTarget" ).show().attr( "style", "top:" + ( backPosition.top - 10 - 16 ) + "px; " + "left:" + ( targetLeft + 14 ) + "px; " + "z-index: 3; position: absolute;" );

        var wTargetPct = targetDif / maxVal;
        targetWidth = $( "#trackerTable" ).width();
        var targetBackWidth = ( baseWidth * wTargetPct );
        $( "div#trackerBackTarget" ).width( targetBackWidth + 'px' );
        $( "div#trackerBackTarget" ).height( ( $( "#trackerTable tbody" ).height() + 30 ) + "px" );
    }

    // Get the stretch percentage
    var stretchDif = maxVal - stretch;
    if ( stretch > 0 && stretchDif > 0 ) {
        $( "div#trackerBackStretch" ).show().css( { "top": backPosition.top - 10 + "px", "left": ( targetLeft + targetBackWidth ) + "px" } );
        $( "img.tbackStretch" ).show().attr( "style", "top:" + ( backPosition.top - 10 - 16 ) + "px; " + "left:" + ( targetLeft + targetBackWidth + 9 ) + "px; " + "z-index: 3; position: absolute" );

        var wStretchPct = stretchDif / maxVal;
        var stretchWidth = $( "#trackerTable" ).width();
        var stretchBackWidth = ( baseWidth * wStretchPct );
        $( "div#trackerBackStretch" ).width( stretchBackWidth + 'px' );
        $( "div#trackerBackStretch" ).height( ( $( "#trackerTable tbody" ).height() + 35 ) + "px" );
    }
}

function findToday(oSEC) {
    var idxToday = -1;
    
    if (oSEC.HasEnded) {
        idxToday = oSEC.Tracker.length - 1;
    } else {
        for (var i = 0; i < oSEC.Tracker.length; i++) {
            if (oSEC.Tracker[i].IsToday) {
                idxToday = i;
                break;
            }
        }
    }
    
    return idxToday;
}

function setSeeMoreLink( oChallenge ) {
    var link = '';
    var text = '';
    switch ( oChallenge.TaskType ) {
        case "ACT":
            {
                link = '../Member/ActivityJournal.aspx';
                text = 'more activity tracking';
                break;
            }
        case "CAL":
            {
                link = '../Member/ActivityJournal.aspx';
                text = 'more activity tracking';
                break;
            }
        case SEC_TASK_TYPE_WEIGHTLOSS:
            {
                // no link for this
                break;
            }
        case SEC_TASK_TYPE_WEIGHT:
            {
                // no link for this
                break;
            }
    }

    if ( link == '' ) {
        $( "a#moreTracking" ).hide();

    } else {
        $( "a#moreTracking" ).show().prop( "href", link ).html( text );
    }
}

function setTrackerStyle( oChallenge ) {
    var yesContent = $("#hdnKagResponseYes").val();
    var noContent = $("#hdnKagResponseNo").val();
    
    switch ( parseInt( oChallenge.ResponseType ) ) {
        case RESPONSE_TYPE_YESNO:
            {
                $( "table#trackerTable" ).addClass( "yesno" );
                $("th.yes").css("text-align", "left").html(yesContent);
                $(".yes").html(yesContent).css({ "text-align": "left", "padding-left": "10px" });
                $(".no").html(noContent).css("padding-right", "15px");
                $( "div.TrackerMessage" ).css( "margin-top", "-22px" );
                break;
            }
        case RESPONSE_TYPE_NUMERIC:
            {
                if ( oChallenge.TaskType == SEC_TASK_TYPE_WEIGHT || oChallenge.Target == null ) {
                    $( "table#trackerTable" ).addClass( "numeric" );
                    $( "th.Stretch" ).html( '' );
                } else {
                    $( "table#trackerTable" ).removeClass( "numeric" );
                }
                $( ".Stretch, .Finish" ).removeClass( "yes" ).removeClass( "no" );
                break;
            }
    }
}

function ReBuildTracker( maxDays ) {

    var currentIdx = VHM.SelfEntryChallenge.TrackerDays.startIdx + 1;
    $( ".trackerRow" ).hide();

    var trackerRow = $( ".trackerRow" );

    for ( var rowCount = 0; rowCount < maxDays; rowCount++ ) {
        var currentRow = trackerRow[currentIdx];
        $( currentRow ).show();
        $( ".trackerVal", currentRow ).css( "left", $( ".trackerVal", currentRow ).css( "left" ) );
        currentIdx++;
    }

    var elems = $( ".trackerRow:visible" );
    for ( var sc = 0; sc < elems.length; sc++ ) {
        $( elems[sc] ).find( '.trackerVal' ).addClass( 'hasValue' );
    }

    elems = $( ".trackerRow:hidden" );
    for ( var sc = 0; sc < elems.length; sc++ ) {
        $( elems[sc] ).find( '.trackerVal' ).removeClass( 'hasValue' );
    }
    
    elems = $( ".noValue" );
    for ( var sc = 0; sc < elems.length; sc++ ) {
        $( elems[sc] ).find( 'a' ).html( "?" );
    }

}

function ShowComplete() {

    $( "#dvChallengeCompletedPopup" ).jqmShow();//.dialog( 'open' )
    $( "#hdnChallengeComplete" ).val( "true" )
}


function formatJSONDate( jsonDate ) {
    var newDate = eval( jsonDate.replace( /\/Date\((\d+)\)\//gi, "new Date($1)" ) );
    return newDate;
}


/*------------Document Ready---------------------------------*/
$(document).ready(function () {

    if (VHM.SelfEntryChallenge && VHM.SelfEntryChallenge.Load) {
        VHM.SelfEntryChallenge.Load();
    }
});




// Date and time formatting
var dateFormat = function () {
    var token = /d{1,4}|M{1,4}|yy(?:yy)?|([HhmsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
		timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
		timezoneClip = /[^-+\dA-Z]/g,
		pad = function ( val, len ) {
		    val = String( val );
		    len = len || 2;
		    while ( val.length < len ) val = "0" + val;
		    return val;
		};

    // Regexes and supporting functions are cached through closure
    return function ( date, mask, utc ) {
        var dF = dateFormat;

        // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
        if ( arguments.length == 1 && Object.prototype.toString.call( date ) == "[object String]" && !/\d/.test( date ) ) {
            mask = date;
            date = undefined;
        }

        // Passing date through Date applies Date.parse, if necessary
        date = date ? new Date( date ) : new Date;
        if ( isNaN( date ) ) throw SyntaxError( "invalid date" );

        mask = String( dF.masks[mask] || mask || dF.masks["default"] );

        // Allow setting the utc argument via the mask
        if ( mask.slice( 0, 4 ) == "UTC:" ) {
            mask = mask.slice( 4 );
            utc = true;
        }

        var _ = utc ? "getUTC" : "get",
			d = date[_ + "Date"](),
			D = date[_ + "Day"](),
			M = date[_ + "Month"](),
			y = date[_ + "FullYear"](),
			H = date[_ + "Hours"](),
			m = date[_ + "Minutes"](),
			s = date[_ + "Seconds"](),
			L = date[_ + "Milliseconds"](),
			o = utc ? 0 : date.getTimezoneOffset(),
			flags = {
			    d: d,
			    dd: pad( d ),
			    ddd: dF.International.dayNames[D],
			    dddd: dF.International.dayNames[D + 7],
			    M: M + 1,
			    MM: pad( M + 1 ),
			    MMM: dF.International.monthNames[M],
			    MMMM: dF.International.monthNames[M + 12],
			    yy: String( y ).slice( 2 ),
			    yyyy: y,
			    h: H % 12 || 12,
			    hh: pad( H % 12 || 12 ),
			    H: H,
			    HH: pad( H ),
			    m: m,
			    mm: pad( m ),
			    s: s,
			    ss: pad( s ),
			    l: pad( L, 3 ),
			    L: pad( L > 99 ? Math.round( L / 10 ) : L ),
			    t: H < 12 ? "a" : "p",
			    tt: H < 12 ? "am" : "pm",
			    T: H < 12 ? "A" : "P",
			    TT: H < 12 ? "AM" : "PM",
			    Z: utc ? "UTC" : ( String( date ).match( timezone ) || [""] ).pop().replace( timezoneClip, "" ),
			    o: ( o > 0 ? "-" : "+" ) + pad( Math.floor( Math.abs( o ) / 60 ) * 100 + Math.abs( o ) % 60, 4 ),
			    S: ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : ( d % 100 - d % 10 != 10 ) * d % 10]
			};

        return mask.replace( token, function ( $0 ) {
            return $0 in flags ? flags[$0] : $0.slice( 1, $0.length - 1 );
        } );
    };
} ();

// Some common format strings
dateFormat.masks = {
    "default": "ddd MMM dd yyyy HH:mm:ss",
    shortDate: "M/d/yy",
    mediumDate: "MMM d, yyyy",
    longDate: "MMMM d, yyyy",
    fullDate: "dddd, MMMM d, yyyy",
    shortTime: "h:mm TT",
    mediumTime: "h:mm:ss TT",
    longTime: "h:mm:ss TT Z",
    isoDate: "yyyy-MM-dd",
    isoTime: "HH:mm:ss",
    isoDateTime: "yyyy-MM-dd'T'HH:mm:ss",
    isoUtcDateTime: "UTC:yyyy-MM-dd'T'HH:mm:ss'Z'"
};

// Internationalization strings
dateFormat.International = {
    dayNames: [
		"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
		"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
	],
    monthNames: [
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
		"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
	]
};

// Define prototype for date formatting
Date.prototype.format = function ( mask, utc ) {
    return dateFormat( this, mask, utc );
};