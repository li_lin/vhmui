﻿jQuery.vhmAjax = function (opt) {
    var f_succ = opt.success;
    var f_err = opt.error;
    // do not allow to get the cached urls if any AJAX call is made.
    var strURL = opt.url.toLowerCase();
    if (strURL.indexOf('?') == -1) {
        var r = Math.floor(Math.random() * 10000000);
        opt.url += "?rnd=" + r; // if no params in the URL
    }
    else {
        var r = Math.floor(Math.random() * 10000000);
        opt.url += "&rnd=" + r;
    }
    opt.success = function (data, textStatus) {
        var strData;
        if (typeof data.xml != "undefined") {
            strData = data.xml;
        } else if (typeof data.toString() != "undefined") {
            strData = data.toString();
        } else strData = data;

        if (data === "fail") {
            if (f_err) {
                f_err(data, textStatus);
            } else {
                alert('Oops, something went wrong. Please try refreshing the page.');
            }
        } else if (data === "Session has expired" || strData.indexOf('oSession_memberlogin') > 0 || strData.indexOf("Virgin HealthMiles: Home") > 0) {
            location.reload(true);
        } else if (f_succ) {
            f_succ(data, textStatus);
        }
        f_succ = f_err = null;
    };
    $.ajax(opt);
};
/*
* jQuery vhm feedback
* @copyright Aleg Yelshov, Edgar A.
* @version 0.3.0
*
* Ex.:
* html
<div id="divFeedback"></div> 
* js
$( '#divFeedback' ).vhmFeedback ( { 
fid: 1, - feature id (challenge id/promo id/etc.), required param on init,
fc: 'SECH' - feature category code (SECH - self-entry challenge, BDGE - badge, PROM - promo, CHLG - challenge, RTSK - reward task, OTHE - external feature), required param on init,
[, type: 'summary'] - feedback control type, values - "like", "disabledlike", "rating", "yourrating", "summary", "summarypopup". default - summary.
[, avgRatingLabel: 'Average rating:'] - feedback control text for average rating, default - 'Average rating:'.
[, yourRatingLabel: 'Your rating:'] - feedback control text for your rating, default - 'Your rating:'.
} );

*       control static variables:
*       web service url for SaveMemberFeedback and GetMemberFeedback methods.
$.fn.vhmFeedback.wsurl = "../../webservices/Feedback.asmx";
*       Summary rating edit pop up title
$.fn.vhmFeedback.popupTitle = "We want to know what you think";
*
*/
(function ($) {

    // Feedback object to call vhmFeedback plugin methods (not implemented)
    // Ex: $(<selector>).vhmFeedback('edit');
    function Feedback() {
        this._param = false;
    };

    $.extend(Feedback.prototype, {
        edit: function (target) {
        }
    });

    var _defaults = {
        type: 'summary',
        avgRatingLabel: "Average rating:",
        yourRatingLabel: "Your rating:"
    }

    $.fn.vhmFeedback = function (options) {
        if (!this.length) {
            return this;
        }

        var otherArgs = Array.prototype.slice.call(arguments, 1);

        return this.each(function () {
            if ($(this).data("feedback") == null) {

                if (isNaN(options.fid)) {
                    throw "parameter fid (feature Id) must be specified";
                }

                if (typeof options.fc == "undefined" || options.fc === "") {
                    throw "parameter fc (feature category code) must be specified";
                }

                $(this).data("feedback", _defaults);
            }

            if (typeof options == 'string') {
                Feedback[options].apply(this, $.makeArray(arguments).slice(1) || []);
            } else if ($.isPlainObject(options)) {
                $(this).data("feedback", $.extend({}, $(this).data("feedback"), options));

                var feedback = $(this).data("feedback"),
                fid = feedback.fid,
                fc = feedback.fc,
                ffid = getFeatureFeedbackId(fid, fc);
                build(this);
                queueGetMemberFeedback(fid, fc, setFeedback, $(this));
            }
        });
    };

    $.fn.vhmFeedback.wsurl = "../../webservices/Feedback.asmx";
    $.fn.vhmFeedback.popupTitle = "We want to know what you think";

    var stars = [0, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5],
    memberFeedbackCallQueue = {},
    feedbackData = {},
    queueIds = [],
    feedbacks = []
    getQueueId = function (fid, fc) {
        for (var i = 0, l = queueIds.length; i < l; i++) {
            if (queueIds[i].fid === fid && queueIds[i].fc === fc) {
                return ++i;
            }
        }
        return -1;
    },
    getFeatureFeedbackId = function (fid, fc) {
        for (var f in feedbackData) {
            if (f && feedbackData[f].FeatureCategoryCode === fc && feedbackData[f].FeatureId === fid) {
                return feedbackData[f].FeatureFeedbackId;
            }
        }
        return null;
    },
queueGetMemberFeedback = function (fid, fc, callback, $o) {
    if (feedbacks[fid] != null || feedbacks[fid] != undefined) {
        var ffid = getFeatureFeedbackId(fid, fc);
        if (feedbackData[ffid] != null && feedbackData[ffid] != undefined) {
            $o.data("feedback", $.extend({}, $o.data("feedback"), feedbackData[ffid]));
            feedbacks[fid][feedbacks[fid].length] = $o;
            setFeedback($o);
        }
        else {
            feedbacks[fid][feedbacks[fid].length] = $o;
        }
    }
    else {
        feedbacks[fid] = [];
        feedbacks[fid][0] = $o;
        callGetMemberFeedback(fid, fc, $o)
    }

},
callGetFeedbackCallback = function (fid, fc, $o, data) {
    if (null != data && !isNaN(data.FeatureFeedbackId)) {
        feedbackData[data.FeatureFeedbackId] = data;
    }

    if (feedbacks[fid] != null || feedbacka[fid] != undefined) {
        for (var i = 0; i < feedbacks[fid].length; i++) {
            if (feedbacks[fid][i] != null || feedbacka[fid][i] != undefined)
                feedbacks[fid][i].data("feedback", $.extend({}, feedbacks[fid][i].data("feedback"), data));
            setFeedback(feedbacks[fid][i]);
        }
    }

},
    callSaveMemberFeedbackCallback = function (ffid, data) {
        feedbackData[ffid] = data;
    },
    callGetMemberFeedback = function (fid, fc, $o) {
        $.vhmAjax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: $.fn.vhmFeedback.wsurl + "/GetMemberFeedback",
            data: "{'featureId': '" + fid + "', 'featureCategoryCode': '" + fc + "'}",
            dataType: "json",
            success: function (data, textStatus, jqXHR) { callGetFeedbackCallback(fid, fc, $o, data.d, textStatus, jqXHR); },
            error: function (xhr, ajaxOptions, error) { alert(error); }
        });
    },
    callSaveMemberFeedback = function (ffid) {
        $.vhmAjax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: $.fn.vhmFeedback.wsurl + "/SaveMemberFeedback",
            data: "{'feedback': " + JSON.stringify(feedbackData[ffid]) + "}",
            dataType: "json",
            success: function (data, textStatus, jqXHR) { callSaveMemberFeedbackCallback(ffid, data.d, textStatus, jqXHR); },
            error: function (xhr, ajaxOptions, error) { alert(error); }
        });
    },
    getAvgRating = function (fid) {
        return parseInt((feedbackData[fid] && feedbackData[fid].AvgRating) || 0);
    },
    getRating = function (fid) {
        return parseInt((feedbackData[fid] && feedbackData[fid].Rating) || 0);
    },
    getNumLikes = function (fid) {
        return parseInt((feedbackData[fid] && feedbackData[fid].TotalLikes) || 0);
    },
    getComment = function (fid) {
        if (feedbackData[fid] != null && feedbackData[fid].Comment != null)
            return feedbackData[fid].Comment;
        return "";
    },
    getFeedback = function ($o) {
        return $o.data('feedback') || {};
    },
    setFeedback = function ($o) {
        var fb = $o.data("feedback"),
        ffid = getFeatureFeedbackId(fb.fid, fb.fc);

        switch (fb.type.toLowerCase()) {
            case "like":
            case "disabledlike":
                setLike($o, ffid);
                break;
            case "fullrating":
                setFullRating($o, ffid);
                break;
            case "rating":
                setAvgRating($o, ffid);
                break;
            case "yourrating":
                setYourRating($o, ffid);
                break;
            case "allinone":
                $o.find('.fdb-commentInputBox').val(getComment(ffid));
                break;
            case "summarypopup":
            case "summary":
            default:
                setSummary($o, ffid);
                break;
        }
    },
    setLike = function ($o, ffid) {
        if (ffid != null) {
            if (feedbackData[ffid].IsUserLikes === true) {
                $o.find('.fdb-likelbl span').text('Unlike');
            } else {
                $o.find('.fdb-likelbl span').text('Like');
            }
            buildNumLikes($o, getNumLikes(ffid));
        }
    },
    setRating = function ($o, r) {
        var lis = $o.find('li');

        for (var i = 0, l = lis.length; i < l; i++) {
            if (i < Math.ceil(r)) {
                $(lis[i]).addClass('cover');
            } else {
                $(lis[i]).removeClass('cover');
            }
        }
    },
    setAvgRating = function ($o, ffid) {
        var r = stars[getAvgRating(ffid)];
        setRating($o, r);

        if (Math.round(r * 2) % 2 === 1) {
            $($o.find('li')[parseInt(r)]).html("<div class='half-star'></div>");
        }
    },
    setYourRating = function ($o, ffid) {
        var r = stars[getRating(ffid)];
        setRating($o, r);
    },
    setSummary = function ($o, ffid) {
        var numLikes = buildNumLikes($o, getNumLikes(ffid));
        $o.find(".fdb-totalLikes").remove();
        $(numLikes).appendTo($o.find('.fdb-likelbl'));
        setAvgRating($o, ffid);
    }
    setFullRating = function ($o, ffid) {
        $o.find('.fdb-commentInputBox').val(getComment(ffid));
    }
    var build = function (dom) {
        var $o = $(dom),
        prm = $o.data("feedback");
        switch ($o.data("feedback").type.toLowerCase()) {
            case "like":
                buildLike($o);
                break;
            case "disabledlike":
                buildDisabledLike($o);
                break;
            case "rating":
                buildAvgRating($o);
                break;
            case "yourrating":
                buildYourRating($o);
                break;
            case "fullrating":
                buildFullRating($o, prm);
                break;
            case "summarypopup":
                buildSummaryPopup($o, prm);
                break;
            case "allinone":
                buildAllInOne($o);
                break;
            case "summary":
            default:
                buildSummary($o);
                break;
        }
    },
    buildNumLikes = function ($o, n) {
        var fb = $o.data("feedback"),
        ffid = getFeatureFeedbackId(fb.fid, fb.fc),
        totalLikes = getNumLikes(ffid),
        addNumLikes = function (n) {
            var s = "<div class='fdb-totalLikes'>(";
            if (n == 0) {
                s += "be the first";
                return s + ")</div>"
            } else if (n == 1) {
                s += "1 likes";
            } else {
                s += n + " like";
            }
            return s + " this)</div>";
        }

        $o.find('.fdb-totalLikes').remove().end().append(addNumLikes(totalLikes));

        return addNumLikes(totalLikes);
    },
    buildLike = function ($o, isDis) {
        var s, a = ["", "like-inactive.gif", ""];
        if (isDis !== true) {
            a = ["<a href='#'>", "icon-likes.gif", "</a>"];
        }
        s = "<div class='fdb-likelbl'>" + a[0] + "<image src='../../images/feedback/" + a[1] + "'/><span>Like</span>" + a[2] + "</div>";
        $o.html(s);

        if (isDis !== true) {
            attachEvents($o);
        }
    },
    buildDisabledLike = function ($o) {
        buildLike($o, true);
    },
    buildRatingHtml = function (cssClass, lbl) {
        var s = "<span class='fdb-ratinglbl'>" + lbl + "</span><div>";
        s += "<ul class='" + cssClass + "'>";
        for (var i = 0, l = stars.length / 2; i < l; i++) {
            s += "<li></li>";
        }
        s += "</ul></div>";

        return s;
    },
    buildAvgRating = function ($o) {
        var arLbl = getFeedback($o).avgRatingLabel,
        s = buildRatingHtml('fdb-rating', arLbl);
        $o.html(s);
    },
    buildYourRating = function ($o) {
        var yrLbl = getFeedback($o).yourRatingLabel,
        s = buildRatingHtml('fdb-rating-bl', yrLbl);
        $o.html(s);
        attachEvents($o);
    },
    buildSummaryPopup = function ($o, m) {
        buildSummary($o);
        $o.find('div.fdb-summary').addClass('fdb-summarypopup');
        attachEvents($o);

    },
    buildFullRating = function ($o) {
        fb = $o.data("feedback");
        fid = fb.fid;
        fc = fb.fc;
        var divCommet = "<div class='fdb-sumpopcomment'><span class='fdb-comment'>Comments:</span>" +
                "<textarea class='fdb-commentInputBox' rows='2' cols='60'></textarea></div>" +
                "<input type='button' class='vbtn_red_a_sm fdb-doneButton post' style='display:none;' value='Done' /></div>";
        var html = "<div class='fdb-sumpoplikerating'><div class='fdb-sumpoplike' />" +
                "<div class='fdb-sumpopyourRating' />" + divCommet + "</div>";
        $o.html(html);
        $o.find('div.fdb-sumpopyourRating').vhmFeedback({ fid: fid, fc: fc, type: 'YourRating' }).end()
        $o.find('div.fdb-sumpoplike').vhmFeedback({ fid: fid, fc: fc, type: 'Like' });
        $o.find(".fdb-doneButton").click(function (e) {
            ffid = getFeatureFeedbackId(fid, fc);
            feedbackData[ffid].Comment = $o.find(".fdb-commentInputBox").val();
            callSaveMemberFeedback(ffid);
        });
        attachEvents($o);
    },
    buildSummary = function ($o) {
        var s = "<div class='fdb-summary'><div class='fdb-likelbl'><image src='../../images/social/icon-likes.gif' /></div>";
        s += "<div>";
        s += "<ul class='fdb-rating'>";
        for (var i = 0, l = stars.length / 2; i < l; i++) {
            s += "<li></li>";
        }
        s += "</ul></div></div>";
        $o.html(s);
    },
    buildAllInOne = function ($o) {
        fb = $o.data("feedback");
        fid = fb.fid;
        fc = fb.fc;
        ffid = getFeatureFeedbackId(fid, fc);
        var divCommet = "<div><div class='fdb-sumpopcomment'>" +
                "<span class='fdb-comment'>Comments:</span>" +
                "<textarea class='fdb-commentInputBox' rows='2' cols='60'></textarea>" +
                "<input type='button' class='vbtn_red_a_sm fdb-doneButton post' value='Done' /></div>";

        $("<div><div class='fdb-sumpoplikerating'/><div class='fdb-sumpoplike' />" +
                "<div class='fdb-sumpopyourRating' /></div>" + divCommet + "</div>").appendTo($o);

        $o.find('.fdb-sumpopyourRating').vhmFeedback({ fid: fid, fc: fc, type: 'YourRating' });
        $o.find('.fdb-sumpoplike').vhmFeedback({ fid: fid, fc: fc, type: 'Like' });

        attachEventsForComment($o);
    }
    var attachEvents = function ($o) {
        $o.find('.fdb-likelbl a').unbind('click').click(function (e) {

            var $o = $(e.target).parents('div.fdb-likelbl').parent(),
            fb = $o.data("feedback"),
            ffid = getFeatureFeedbackId(fb.fid, fb.fc);

            if (null != ffid) {
                feedbackData[ffid].IsUserLikes = !feedbackData[ffid].IsUserLikes;
                feedbackData[ffid].IsUserLikes ? ++feedbackData[ffid].TotalLikes : --feedbackData[ffid].TotalLikes;
                callSaveMemberFeedback(ffid);
                setLike($o, ffid);
            }

            return false;
        });

        $o.find('ul.fdb-rating-bl')
        .unbind().click(function (e) {
            var r = $(e.target).prevAll().length + 1;
            r = r * 2 - 1;
            var $o = $(e.target).parents('ul.fdb-rating-bl').parent().parent(),
            fb = $o.data("feedback"),
            ffid = getFeatureFeedbackId(fb.fid, fb.fc);

            if (null != ffid) {
                feedbackData[ffid].Rating = r;
                callSaveMemberFeedback(ffid);
                setYourRating($(e.target).parent('ul'), ffid);
            }
        })
        .undelegate().delegate('li', 'mouseenter', function (e) {

            $(this).addClass('cover').prevAll().empty().addClass('cover').end()
                .nextAll().removeClass('cover');
        })
        .delegate('li', 'mouseleave', function () {

            var ul = $(this).parent();
            ul.find('li').removeClass('cover');

            var $o = ul.parent().parent(),
            fb = $o.data("feedback"),
            ffid = getFeatureFeedbackId(fb.fid, fb.fc);

            setYourRating(ul, ffid);
        });

        $o.find(".fdb-summarypopup").unbind('click').click(function (e) {
            var $t = $(e.target),
            $o = $t.hasClass('fdb-summarypopup') ? $t.parent() : $t.parents('div.fdb-summarypopup').parent(),
            fb = $o.data("feedback"),
            fid = fb.fid,
            fc = fb.fc,
            ffid = getFeatureFeedbackId(fid, fc),
            $spdlg = $('.fdb-summaryPopupDlg');

            if ($spdlg.length) {
                var dlgItm;
                for (var i = 0, l = $spdlg.length; i < l; i++) {
                    dlgItm = $($spdlg[i]);
                    if (parseInt(dlgItm.attr('ffid')) === ffid) {
                        dlgItm.dialog('open');
                        return;
                    }
                }
            }

            var divCommet = "<div class='fdb-sumpopcomment'>" +
                "<span class='fdb-comment'>Comments:</span>" +
                "<textarea class='fdb-commentInputBox' rows='2' cols='60'></textarea>" +
                "<input type='button' class='vbtn_red_a_sm fdb-doneButton post' value='Done' /></div>";


            $("<div class='fdb-summaryPopupDlg' >" +
               "<table cellpadding='0' id='jqmPopUpTableContaner' cellspacing='0' border='0'>" +
                '<tr class="tblVHMTrow"><td class="tblVHMTcellL"></td><td class="tblVHMTcellC"></td>' +
                '<td class="tblVHMTcellR"><div class="jqmClose closeButton"></div></td></tr><tr class="tblVHMCrow">' +
                '<td class="tblVHMCcellL"></td><td class="tblVHMCcellC">' +
               "<span class='fdb-summaryPopupTitle'>" + $.fn.vhmFeedback.popupTitle + "</span>" +
                "<div class='fdb-sumpoplikerating'/><div class='fdb-sumpoplike' />" +
                "<div class='fdb-sumpopyourRating' /></div>" + divCommet +
                '</td><td class="tblVHMCcellR"></td>' +
                '</tr><tr  class="tblVHMBrow"><td class="tblVHMBcellL"></td><td class="tblVHMBcellC"></td><td class="tblVHMBcellR"></td></tr></table>' +
                "</div>")
                .appendTo($o);

            $o.find('.fdb-commentInputBox').val(getComment(ffid));
            $o.find('.fdb-sumpopyourRating').vhmFeedback({ fid: fid, fc: fc, type: 'YourRating' });
            $o.find('.fdb-sumpoplike').vhmFeedback({ fid: fid, fc: fc, type: 'Like' });
            attachEventsForComment($o);
            $o.find('.closeButton').click(function (e) {
                var dlg = $(e.target).parents(".fdb-summaryPopupDlg");
                $(dlg).dialog('close');
            }
             );
            $o.find('div.fdb-summaryPopupDlg').attr('ffid', ffid).dialog({
                modal: true
                    , resizable: false
                    , 'height': 300
                    , 'width': 450
                    , 'beforeClose': function () { }
            }).parent().addClass('fdb-sumpopbd').find('.ui-dialog-titlebar').remove().end().
            find('.fdb-commentInputBox').focus();
        });
    }
    var attachEventsForComment = function ($o) {
        $o.find('.fdb-commentInputBox').removeAttr('disabled').bind('focus', function (e) {
            $(e.target).parent().find('.fdb-commentInputBox').addClass('fdb-commentDflt');
        });

        $o.find('.fdb-commentInputBox').removeAttr('disabled').bind('blur', function (e) {
            $(e.target).parent().find('.fdb-commentInputBox').removeClass('fdb-commentDflt');
        });

        $o.find(".fdb-doneButton").click(function (e) {

            $dlg = $(e.target).parents('#jqmPopUpTableContaner');
            if ($dlg.length == 0) {
                $dlg = $o.parent();
            }
            $comment = $dlg.find(".fdb-commentInputBox");
            if ($comment.val().length > 500) {

                alert("Oops, your comment can only have up to 500 characters")

                var msgBoxObj = $("#VHMCommonAlertDialog");
                var msgParent = msgBoxObj.parent();
                msgParent.removeClass('Fix_AlertParent').addClass('Fix_AlertParent');

                var firstDiv = $('#ui-dialog-title-VHMCommonAlertDialog');
                firstDiv.removeClass('Fix_FirstDiv').addClass('Fix_FirstDiv');

                var firstDivParent = firstDiv.parent();
                firstDivParent.removeClass('Fix_FirstDivParent').addClass('Fix_FirstDivParent');

                var closelink = firstDivParent.find("a");
                closelink.css('background-image', 'none');
                closelink.css('right', '0px');

                var closespan = closelink.find('span');
                closespan.removeClass('Fix_closespan').addClass('Fix_closespan');
                closespan.css('margin-top', '8px');
                closespan.text('');

                return;
            }

            fb = $o.data("feedback");
            ffid = getFeatureFeedbackId(fb.fid, fb.fc);
            feedbackData[ffid].Comment = $comment.val();
            callSaveMemberFeedback(ffid);

            var dlg = $(e.target).parents(".ui-dialog-content");
            $(dlg).dialog('close');
        });
    }
})(jQuery);