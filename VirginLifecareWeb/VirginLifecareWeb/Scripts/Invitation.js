﻿//V3

var VHM = VHM || {};
VHM.Invitation = VHM.Invitation || {};
VHM.Invitation.GetMoreFriends = function (s, e, paging) {
    $.ajax({
        url: AppPath + "/PartialView/GetMoreFriends",
        type: "POST",
        data: paging,
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            if (data == [] || data == null || data.length == 0) {
                $("#divInviteFriends").html("You currently do not have any friends.");
                return;
            }
            if (s != null)
                s(data);
            EnableAlphabet("LastName");
            $(this).find("td").css({ "border-spacing": "10px" });
        },
        error: function (jqXHR, textStatus, errorThrown) { if (e != null) e(jqXHR, textStatus, errorThrown); }
    });
};
VHM.Invitation.GetMoreGroups = function (s, e, paging) {
    $.ajax({
        url: AppPath + "/PartialView/GetMoreGroups",
        type: "POST",
        data: paging,
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            s(data);
        },
        error: function (jqXHR, textStatus, errorThrown) { e(jqXHR, textStatus, errorThrown); }
    });
};
VHM.Invitation.SelectedMembers = [];
var memberCell;
var groupCell;
$(function () {
    memberCell = $("#memberTemplate");
    groupCell = $("#groupTemplate");
    $("#divInviteFriends").find("#viewAll").click(function () {
        var s = function (data) {
            var settings = $("#divInviteFriends").find("#invitation").data("dataGrid");
            $("#divInviteFriends").find("#invitation").vhmDataGrid($.extend(settings, { values: data, perPage: data.length, totalPages: 0, currPage: 0 }));
        };
        VHM.Invitation.GetMoreFriends(s, null, null);
    });
    $("#divInviteGroups").find("#viewAll").click(function () {
        var s = function (data) {
            var settings = $("#divInviteGroups").find("#invitation").data("dataGrid");
            $("#divInviteGroups").find("#invitation").vhmDataGrid($.extend(settings, { values: data, perPage: data.length, totalPages: 0, currPage: 0 }));
        };
        VHM.Invitation.GetMoreGroups(s, null, null);
    });
    $(".selectAll").unbind("change").bind("change", function (e, target) {
        selectAll(this);
    });
});



function selectAll(target) {
    if (target.checked) {
        if (typeof window.chrome === "object") {
            $(".checkbox", "#divInviteFriends").attr("checked", true);
            $(".checkbox", " #divInviteGroups ").attr("checked", true);
        } else {
            $(".checkbox", "#divInviteFriends").attr("checked", "checked");
            $(".checkbox", " #divInviteGroups ").attr("checked", "checked");
        }
    } else if (!target.checked) {
        $(".checkbox", "#divInviteFriends").removeAttr("checked");
        $(".checkbox", "#divInviteGroups").removeAttr("checked");
    }
}

function OpenFriendsPopUp() {
    $("#divInviteFriends").find("#invitation").vhmDataGrid({
        cells: [memberCell],
        hasViewMore: true,
        type: "column",
        defaultText: "You have no friends.",
        columnCount: 3,
        width: 600,
        perPage: 9,
        totalPages: totalPages,
        getData: VHM.Invitation.GetMoreFriends,
        callback: function ($this) {
            $this.find("#divToScroll").css({ "max-height": "300px", "overflow-y": "auto" });
        }
    });
    $("#divInviteFriends").dialog({
        title: "Friends"
        , modal: true
        , width: 700
        , resizable: false
        , draggable: false
        , buttons: {
            "Add To Invite List": { text: "Add To Invite List", 'class': 'vbtn_red_a_md', click: function () {
                var newMembersIds = GetSelectedMembers();
                var callback = function (data) {
                    AddNewMembersToList(data);
                };
                var error = function (a, b, c) {
                    alert(a);
                };
                PopulateInvitedFriendsList(newMembersIds, [], callback, error);
                $(this).dialog("close");
            }
            }
        }
       , open: function (event, ui) {
           var closeTitle = $(".ui-dialog-titlebar-close");
           closeTitle.unbind('mouseenter mouseleave');
       }
    });
}

var cell1 = $("<input type='checkbox' value='${MemberId}'  style='padding:5px; margin-left:10px;'/><input type='hidden' value='${MemberId}' name='InvitedMembers'/> ");
var cell2 = $("<div style='margin:5px;'><span class='anchorSpan' onclick='OpenMemberPopUp(${MemberId})' > ${FirstName} ${LastName}</span></div>");

function OpenMemberPopUp(id) {
    var mProfile = $("<div id='mProfile'><div id='divSpinner' algin='center'><img src='../../Content/Images/Shared/spinner.gif')' alt='Loading '  /></div></div>");
    $.ajax({
        url: AppPath + "/PartialView/GetMemberProfile",
        type: "POST",
        data: '{"id":' + id + '}',
        contentType: 'application/json; charset=utf-8',
        success: function (data, textStatus, jqXHR) {
            mProfile.find("#divSpinner").remove();
            mProfile.append(data);
        },
        error: function (jqXHR, textStatus, errorThrown) { }
    });
    $(mProfile).dialog({
        modal: true
        , width: '450px'
        , draggable: false
        , resizable: false
        //, dialogClass: 'dialog_red_width_brd ui-corner-all'
        , open: function (event, ui) {
            $(".ui-dialog-title").hide();
            $(".ui-dialog-titlebar").css({ padding: '0px' });
            var closeTitle = $(".ui-dialog-titlebar-close");
            closeTitle.unbind('mouseenter mouseleave');
        }
    });
}

function GetSelectedMembers() {
    var memberList = [];
    $(":checked", "#divInviteFriends").each(function () {
        memberList.push(this.id);
    });
    return memberList;
}

function GetSelectedGroups() {
    var groupList = [];
    $(":checked", "#divInviteGroups").each(function () {
        groupList.push(this.id);
    });
    return groupList;
}


function gridFooter() {
    var gridFooter = $("#gridFooter");
    gridFooter.find("#showPerPage").change(function () {
        var selVal = $("#showPerPage").val();
        var settings = $(".inv_list").data("dataGrid");
        $(".inv_list").vhmDataGrid($.extend(settings, { perPage: $("#showPerPage option:selected").text(), totalPages: 0 }));
        $("#showPerPage").val(selVal);
    });
    gridFooter.find('#footerSelectAll').click(function () {
        $(this).parents("#dataGrid").find('#tblDataGrid [type="checkbox"]').attr("checked", "checked");
    });
    gridFooter.find('#footerSelectNone').click(function () {
        $(this).parents("#dataGrid").find('#tblDataGrid [type="checkbox"]').removeAttr("checked");
    });

    gridFooter.find('#footerRemove').click(function () {
        var rmvList = $(this).parents("#dataGrid").find("#tblDataGrid :checked");
        var count = VHM.Invitation.SelectedMembers.length;
        var newArray = [];
        var inList = false;
        for (var i = 0; i < count; i++) {
            inList = false;
            $.each(rmvList, function (k, v) {
                if (VHM.Invitation.SelectedMembers[i].MemberId.toString() == v.value) {
                    inList = true;
                }

            });
            if (inList == false)
                newArray.push(VHM.Invitation.SelectedMembers[i]);
        }
        VHM.Invitation.SelectedMembers = newArray;
        $(".inv_list").vhmDataGrid($.extend({ values: VHM.Invitation.SelectedMembers }, inviteGridParams));
        $("#divTotal").html("<h5 class='gray'>" + VHM.Invitation.SelectedMembers.length + " Total</h5>");

    });
    return gridFooter;
}

var inviteGridParams = {
    type: "Row",
    hasPaging: true,
    columnCount: 3,
    perPage: 24,
    width: 555,
    headers: ["", "<h4 style='margin-left:5px;'>Name</h4>"],
    cells: [cell1, cell2],
    sortable: true,
    callback: function ($this) {
        $this.find("#divToScroll").css({ "height": "350px", "overflow-y": "auto", "overflow-x": "hidden" });
        $this.find("tr").find("th").css({ "background-color": "#ffffcc", "height": 30 });
        $this.find("tr").find("td").first().attr("width", 40);
        $this.find("#pager").css({ "float": "right", "margin-top": "-10px", "margin-right": "10px" });
        $this.find("#tblDataGrid").first().find("tbody tr").each(function (key, value) {
            if ((key + 1) % 2 == 0)
                $(value).css({ "background-color": "white" });
            else
                $(value).css({ "background-color": "#c6e4ff" });
        });
    },
    footer: gridFooter()
};

function AddNewMembersToList(newmemberList) {
    if (newmemberList == null || newmemberList == undefined || newmemberList.length == 0) {
        alert("We couldn’t match one or more of the entered email addresses with a HealthMiles member");
        return;
    }
    $.each(newmemberList, function (key, value) {
        VHM.Invitation.SelectedMembers.push(value);
    });
    $("#divTotal").html("<h5 class='gray'>" + VHM.Invitation.SelectedMembers.length + " Total</h5>");
    $(".inv_list").vhmDataGrid($.extend({ values: VHM.Invitation.SelectedMembers }, inviteGridParams));
}

function OpenGroupsPopUp() {
    $("#divInviteGroups").find("#invitation").vhmDataGrid({
        cells: [groupCell],
        hasViewMore: true,
        type: "column",
        columnCount: 3,
        defaultText: "You are not part of any group.",
        perPage: 9,
        width: 600,
        totalPages: totalPages,
        getData: VHM.Invitation.GetMoreGroups,
        callback: function ($this) {
            $this.find("#divToScroll").css({ "max-height": "300px", "overflow-y": "auto", "overflow-x": "hidden" });
        }
    }
     );
    $("#divInviteGroups").dialog({
        title: "Groups"
        , modal: true
        , width: 700
        , resizable: false
        //, dialogClass: 'dialog_red_width_brd ui-corner-all',
        , buttons: { "Add To Invite List": { text: "Add To Invite List", 'class': 'vbtn_red_a_md', click: function () {
            var groupIds = GetSelectedGroups();
            var callback = function (data) {
                AddNewMembersToList(data);
            };
            var error = function (a, b, c) {
                alert(a);
            };
            PopulateInvitedFriendsList([], groupIds, callback, error);
            $(this).dialog("close");
        }
        }
        }
        , open: function (event, ui) {
            var closeTitle = $(".ui-dialog-titlebar-close");
            closeTitle.unbind('mouseenter mouseleave');
        }
    });
}

function GetMembersIds(groupsId) {
    $.ajax({
        url: AppPath + "/PartialView/GetMembersIds",
        type: "POST",
        data: groupsId,
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        success: function (data, textStatus, jqXHR) { s(data); },
        error: function (jqXHR, textStatus, errorThrown) { e(jqXHR, textStatus, errorThrown); }
    });
}

function OpenWriteInPopUp() {
    $("#writeIn").dialog({
        title: "Write-in"
        , modal: true
        , width: 400
        , resizable: false
        //, dialogClass: 'dialog_red_width_brd ui-corner-all',
        , buttons: { "Add To Invite List": { text: "Add To Invite List", 'class': 'vbtn_red_a_md', click: function () {
            var mIds = $("#emails").val().split(/[\n\r;,]/);
            $.each(mIds, function (key, val) {
                mIds[key] = val.trim();
            });
            $.ajax({
                url: AppPath + "/PartialView/GetMembersByEmailsOrIdentityNumbers",
                type: "POST",
                data: JSON.stringify(mIds),
                contentType: 'application/json; charset=utf-8',
                dataType: "json",
                success: function (responseText) {
                    var callback = function (data) {
                        AddNewMembersToList(data);
                    };
                    var error = function (a, b, c) {
                        alert(a);
                    };
                    PopulateInvitedFriendsList(responseText, [], callback, error);
                }
            });
            $(this).dialog("close");
        }
        }
        }
        , open: function (event, ui) {
            var closeTitle = $(".ui-dialog-titlebar-close");
            closeTitle.unbind('mouseenter mouseleave');
        }
    });
}

function OpenUloadCSVPopUp() {
    var dlg = $("#importCSV");
    $(dlg).dialog(
        {
            title: "Import a list"
         , modal: true
         , resizable: false
         , buttons: {
             "Add To Invite List": {
                 "class": "vbtn_red_a_md",
                 click: function () {
                     $("#uploadCSVFile").ajaxSubmit(
                            {
                                url: AppPath + "/PartialView/UploadCSVFile",
                                success: function (responseText, statusText, xhr, $form) {
                                    dlg.dialog("close");
                                    var callback = function (data) {
                                        AddNewMembersToList(data);
                                    };
                                    var error = function (a, b, c) {
                                        alert(a);
                                    };
                                    PopulateInvitedFriendsList(responseText, [], callback, error);
                                },
                                target: "#output",
                                dataType: 'json',
                                error: function (a, b, c) {
                                    dlg.dialog("close");
                                    error(a, b, c);
                                }
                            });
                 },
                 Id: "btnUploadCSV",
                 text: "Add To Invite List"
             }
         }
         , open: function (event, ui) {
             var closeTitle = $(".ui-dialog-titlebar-close");
             closeTitle.unbind('mouseenter mouseleave');
         }
        });

}

function PopulateInvitedFriendsList(newMemberIds, groupIds, s, e) {
    //    if (newMemberIds.length > 0) { //nothing to populate if no members found
    var spinner = $("<div id='divSpinner' class='ui-widget-overlay' style='height:" + $(document).height() + "px;width:" + $(document).width() + "px;' algin='center'><img style='margin-top:30%; margin-left:50%' src='../../Content/Images/Shared/spinner.gif')' alt='Loading '  /></div>");
    if (VHM.Invitation.SelectedMembers == undefined) {
        VHM.Invitation.SelectedMembers = [];
    }
    var mIds = [];
    $.each(VHM.Invitation.SelectedMembers, function (key, value) {
        mIds.push(value.MemberId);
    });
    $.ajax({
        url: "/v2/PartialView/PopulateInvitedFriendsList",
        type: "POST",
        beforeSend: function () {
            $("body").append(spinner);
        },
        complete: function () {
            spinner.hide();
        },
        data: '{"memberIds":' + JSON.stringify(newMemberIds) + ',"groupIds":' + JSON.stringify(groupIds) + ' , "existingList":' + JSON.stringify(mIds) + '}',
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        success: function (data, textStatus, jqXHR) { s(data); },
        error: function (jqXHR, textStatus, errorThrown) {
            spinner.hide();
            e(jqXHR, textStatus, errorThrown);
        }
    });
    //    }
}

function EnableAlphabet(filterBy) {
    var alphabet = $("#divAlphabet span");
    var list = $("#divInviteFriends").find("#invitation").data("dataGrid").values;
    $.each(list, function (key, value) {
        alphabet.each(function (k, v) {
            if (value != undefined && value[filterBy].substr(0, 1).toLowerCase() == $(v).html().trim().toLowerCase()) {
                $(v).removeClass("disabled");

                $(v).unbind("click").bind("click", function () {
                    var $this = $(this);
                    var filterData = [];
                    $.each(list, function (i, j) {
                        if (j[filterBy].substr(0, 1).toLowerCase() == $this.html().trim().toLowerCase()) {
                            filterData.push(j);
                        }
                    });
                    var settings = $("#divInviteFriends").find("#invitation").data("dataGrid");
                    var originalCurrPage = $("#divInviteFriends").find("#invitation").data("dataGrid").currPage;
                    $("#divInviteFriends").find("#invitation").vhmDataGrid($.extend(settings, { values: filterData, currPage: 0 }));
                    $("#invitation", "#divInviteFriends").find("#pager span").bind("click", function () {
                        $("#divInviteFriends").find("#invitation").vhmDataGrid($.extend(settings, { values: list, currPage: originalCurrPage }));
                        // $("#invitation", "#divInviteFriends").find("#pager a").click();
                    });
                });
            }

        });
    });
}

(function ($) {
    var values;
    var perPage;
    var currPage;
    var cells;
    var columnCount;
    var headers;
    var totalPages;
    var getData;
    var hasViewMore;
    var hasPaging;
    var callback;
    var width;
    var footer;
    var defaultText;
    $.fn.vhmDataGrid = function (options) {
        if (this == null || this == undefined || this.length == 0) {
            return false;
        }
        var settings = $.extend({ type: "Row", hasPaging: false, hasViewMore: false,
            totalPages: 0, columnCount: 1, cells: [], headers: [], values: [],
            columns: [], sortable: false, currPage: 0, perPage: 10
        }, options);
        $(this).data("dataGrid", settings);
        setVariable(this);
        createBaseStructure(this);

        return { data: values, a: settings };
    };

    var setVariable = function ($obj) {
        var dataGrid = $obj.data("dataGrid");

        type = dataGrid.type;
        values = dataGrid.values;
        perPage = dataGrid.perPage;
        currPage = dataGrid.currPage;
        cells = dataGrid.cells;
        columnCount = dataGrid.columnCount;
        columns = dataGrid.columns;
        totalPages = dataGrid.totalPages;
        getData = dataGrid.getData;
        hasViewMore = dataGrid.hasViewMore;
        hasPaging = dataGrid.hasPaging;
        headers = dataGrid.headers;
        width = dataGrid.width;
        callback = dataGrid.callback;
        footer = dataGrid.footer;
        defaultText = dataGrid.defaultText;
    };

    var fillData = function (grid, $obj) {
        if (values[(currPage) * perPage] == undefined && getData != null) {
            var paging = '{"PageNumber":"' + currPage + '", "Count":"' + perPage + '"}';
            var e = function (a, b, c) {
                grid.append(defaultText);
            };
            var s = function (data) {
                if (data.length == 0 || data == null || data == undefined) {
                    return;
                }
                else {
                    for (var i = (currPage) * perPage; i < (currPage + 1) * perPage; i++) {
                        $obj.data("dataGrid").values[i] = data[i - (currPage) * perPage];
                    }
                    grid.find("#divSpinner").parent().parent().remove();
                }
                setVariable($obj);
                fillData(grid, $obj);
            };
            if (getData != null) {
                grid.find("#tblDataGrid tbody")
                    .append('<tr><td><div id="divSpinner" algin="center"><img src="../../Content/Images/Shared/spinner.gif")" alt="Loading "  /></div></td></tr>');
                getData(s, e, paging);
            }
            return;
        }
        if (hasViewMore == false) {
            $obj.find("#tblDataGrid tbody").html("");
            $obj.find("#tblDataGrid thead tr").html("");
            grid.find("#tblDataGrid").removeClass("tablesorter");
            $obj.find("#tblDataGrid tbody").removeAttr("config");
        }
        var startPoint = 0;
        var maxVal = perPage;
        if (currPage > 0) {
            startPoint = (currPage) * perPage;
            if (values.length < currPage * perPage)
                maxVal = values.length;
            else
                maxVal = (currPage + 1) * perPage;
        }
        if (type == "column") {
            fillColumnedData(grid, $obj, startPoint, maxVal);
        } else {
            fillRowedData(grid, $obj, startPoint, maxVal);
        }
        if (hasViewMore == false) {

            if (headers) {
                $.each(headers, function (k, v) {
                    grid.find("thead tr").append("<th>" + v + "</th>");
                });
                if ($obj.data("dataGrid").sortable == true) {
                    // grid.find("#tblDataGrid").addClass("tablesorter");
                    grid.find("#tblDataGrid").tablesorter(
                        {

                            textExtraction: function (node) {
                                return $(node).find(".anchorSpan").text();
                            },

                            cssAsc: "headerSortUp", 	// class name for ascending sorting action to header
                            cssDesc: "headerSortDown"	// class name for headers (th's)
                        }
                     );
                    grid.find("#tblDataGrid").find("th").eq(1).click();
                }
            }
        }

        if (callback) callback(grid);
    };


    var pagerClick = function ($this, grid, $obj) {
        $obj.data("dataGrid").currPage = $this.text() - 1;
        setVariable($obj);
        fillData(grid, $obj);
        $($this).parents("ul").find("li").find("span").removeClass("disabled");
        $($this).find("span").addClass("disabled");
        if (Math.ceil($obj.data("dataGrid").totalPages) > 3) {
            $($this).parents("ul").find("li").last().show();
        }
        if ($this.text() == "1") {
            $($this).parents("ul").find("li").first().hide();
            return;
        } else {
            $($this).parents("ul").find("li").first().show();
        }
        if ($obj.data("dataGrid").currPage == Math.ceil($obj.data("dataGrid").totalPages) - 1) {
            $($this).parents("ul").find("li").last().hide();
            return;
        }
        var pagers = $($this).parents("ul").find("li .anchorSpan");
        if (eval($this.text()) + 2 > Math.ceil($obj.data("dataGrid").totalPages) && $this.next().find(".anchorSpan").length == 0) {
            $.each(pagers, function (key, value) {
                value.innerText++;
            });
            return;
        }
        else if (eval($this.text()) - 2 == 0 && $this.prev().find(".anchorSpan").length == 0) {
            $.each(pagers, function (key, value) {
                value.innerText--;
            });
            return;
        }
        if (eval($this.text()) >= 3) {

            if (eval($this.text()) + 2 > Math.ceil($obj.data("dataGrid").totalPages)) {
                $($this).parents("ul").find("li").find(".anchorSpan").removeClass("disabled");
                $($this).find(".anchorSpan").addClass("disabled");
                return;
            }
            var crrPage = eval($this.text());
            $(pagers[0]).text(crrPage - 2);
            $(pagers[1]).text(crrPage - 1);
            $(pagers[3]).text(crrPage + 1);
            $(pagers[4]).text(crrPage + 2);
            $(pagers[2]).text(crrPage);
            $($this).parents("ul").find("li").find(".anchorSpan").removeClass("disabled");
            $(pagers[2]).addClass("disabled");

        }
    };

    var createBaseStructure = function ($obj) {
        $obj.html("");
        var grid = $("<div id='dataGrid'><div id='divToScroll'><table id='tblDataGrid' width=" + width + "><thead><tr></tr></thead><tbody></tbody></table></div><div id='dataGridFooter' style='margin:10px;'/><div id='pager'></div></div>");
        //        if ( headers ) {
        //            $.each( headers, function ( k, v ) {
        //                grid.find( "thead tr" ).append( "<th>" + v + "</th>" );
        //            } );
        //        }
        if (values) {
            fillData(grid, $obj);
        }
        totalPages = $obj.data("dataGrid").totalPages == 0 ? values.length / perPage : $obj.data("dataGrid").totalPages;
        $obj.data("dataGrid").totalPages = totalPages;
        if (totalPages > 1) {
            if (hasPaging) {
                var pager = grid.find("#pager");
                pager.append("<ul></ul>");
                var pCount = totalPages > 5 ? 5 : totalPages;
                pager.find("ul").append("<li style='float:left; margin:3px; display:none'><img src='/v2/Content/Images/Social/icon_arrow_left.gif'/></li>");
                for (var j = 0; j < pCount; j++) {
                    var newPage = $("<li style='float:left; margin:3px;'><span class='anchorSpan'>" + eval(j + 1) + "</span></li>");
                    if (j == 0)
                        newPage.find("span").addClass("disabled");
                    newPage.click(function () {
                        pagerClick($(this), grid, $obj);
                    });
                    pager.find("ul").append(newPage);
                }


                if (Math.ceil(totalPages) > 5) {
                    var totalOf = "<li style='float:left; margin:3px; '><span> of " + Math.ceil(totalPages) + "</span></li>";
                    pager.find("ul").append(totalOf);
                }
                pager.find("ul").append("<li style='float:left; margin:3px;'><img src='/v2/Content/Images/Social/icon_arrow_right.gif'/></li>");
                pager.find("ul img").first().click(function () {
                    var li = $(this).parents("ul").find(".anchorSpan.disabled").parent().prev();
                    if (eval(li.text()) > Math.ceil(totalPages) || li.length == 0)
                        return;
                    pagerClick(li, grid, $obj);
                });

                pager.find("ul img").last().click(function () {
                    var li = $(this).parents("ul").find(".anchorSpan.disabled").parent().next();
                    if (eval(li.text()) > Math.ceil(totalPages) || li.length == 0)
                        return;
                    pagerClick(li, grid, $obj);
                });
            }
            if (hasViewMore) {
                var viewMore = grid.find("#pager");
                var viewMoreHtml = $("<span class='anchorSpan' currPage='1'>View more</span>");
                viewMoreHtml.click(function () {
                    $obj.data("dataGrid").currPage = $(this).attr("currPage");
                    if (totalPages == $(this).attr("currPage")) {
                        $(this).css({ display: "none" });
                    }
                    else {
                        $(this).attr("currPage", eval($obj.data("dataGrid").currPage) + 1);
                    }
                    setVariable($obj);
                    fillData(grid, $obj);
                });
                viewMore.append(viewMoreHtml);
            }
        }
        //        if ( $obj.data( "dataGrid" ).sortable == true )
        //            grid.find( "#tblDataGrid" ).tablesorter ( { debuge:true,
        //                cssAsc: "headerSortUp", 	// class name for ascending sorting action to header
        //                cssDesc: "headerSortDown"	// class name for headers (th's)
        //            } );
        grid.find("#tblDataGrid").bind("sortEnd", function () {
            callback($obj);
        });
        if (footer) {
            var tempFooter = footer.clone(true);
            grid.find("#dataGridFooter").append(tempFooter);
        }
        $obj.append(grid);

    };

    var fillColumnedData = function (grid, $obj, startPoint, maxVal) {
        var newRow = "";
        var start = 0;
        for (var j = startPoint; j < maxVal; j++) {
            if (start == 0 || start == columnCount) {
                newRow = $("<tr/>");
                start = 0;
            }
            var cellTemplate = cells[0] == undefined ? $("<div id='none'/>") : cells[0];
            var temp = $("<td/>");
            if (values[j] == undefined) {
                grid.find("#tblDataGrid tbody").append(newRow);
                break;
            }

            $.tmpl(cellTemplate, values[j]).appendTo(temp);
            newRow.append(temp);
            start++;
            if (j == maxVal - 1 || start == columnCount)
                grid.find("#tblDataGrid tbody").append(newRow);

        }
    };

    var fillRowedData = function (grid, $obj, startPoint, maxVal) {

        var newRow = $("<tr/>");
        for (var l = 0; l < cells.length; l++) {
            var cell = $("<td/>");
            var cc = cells[l] == undefined ? $("<div id='none'/>") : cells[l].clone(true);
            cell.append(cc);
            newRow.append(cell);
        }
        for (var i = startPoint; i < maxVal; i++) {
            if (values[i] == undefined)
                break;
            var temp = $("<div id='removeMe'/>");
            $.tmpl(temp.append(newRow), values[i]).appendTo(grid.find("#tblDataGrid tbody"));
        }
    };

})(jQuery);