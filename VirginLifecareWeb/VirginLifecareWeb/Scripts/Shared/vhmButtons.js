var aClass = new Array();
aClass = aClass.concat(["vbtn_gray_a_sm","vbtn_gray_a_sm_bgdg","vbtn_gray_a_sm_bglg","vbtn_gray_b_sm","vbtn_gray_b_sm_bgdg","vbtn_gray_b_sm_bglg"]);
aClass = aClass.concat(["vbtn_red_a_sm","vbtn_red_a_sm_bgbl","vbtn_red_a_sm_bgdg","vbtn_red_a_sm_bglg","vbtn_red_a_arw_sm","vbtn_red_a_arwbk_sm","vbtn_red_a_arwbk_sm_bgbl","vbtn_red_b_sm","vbtn_red_b_sm_bgbl","vbtn_red_b_sm_bgdg","vbtn_red_b_sm_bglg","vbtn_red_b_arw_sm","vbtn_red_b_arw_sm_bgbl","vbtn_red_b_arwbk_sm","vbtn_red_b_arwbk_sm_bgbl"]);
aClass = aClass.concat(["vbtn_gray_a_md","vbtn_red_a_md","vbtn_white_a_md","vbtn_gray_a_md_long"]);
aClass = aClass.concat(["vbtn_gray_b_md","vbtn_red_b_md","vbtn_white_b_md","vbtn_white_b_md_bgr"]);
aClass = aClass.concat(["vbtn_gray_d","vbtn_red_d","vbtn_red_d_bgbl"]);
aClass = aClass.concat(["vbtn_gray_c_md","vbtn_red_c_md","vbtn_white_c_md"]);
aClass = aClass.concat(["vbtn_gray_b","vbtn_red_b"]);
aClass = aClass.concat(["vbtn_a"]);
aClass = aClass.concat(["vbtn_b"]);
aClass = aClass.concat(["vbtn_c"]);
aClass = aClass.concat(["vbtn_d"]);

$(document).ready(function(){
	setImageButtonEvents();
});
var setImageButtonEvents = function (id) {
    if ($.browser.msie && parseInt($.browser.version.substring(0, 2), 10) <= 6) {
        var jqo;
        var jqoSubmit;
        if (typeof id === "undefined") {
            jqo = $(":button");
            jqoSubmit = $(":submit");
        } else {
            jqo = $("#" + id + " :button");
            jqoSubmit = $("#" + id + " :submit");
        }
        jqo.each(function () {
            var t = $(this);
            $.each(aClass,
			function (i, v) {
			    if (t.hasClass(v)) {
			        t.bind("mouseover", function () {
			            var b = $(this);
			            var sCls = b.attr("class").toUpperCase();
			            b.removeClass(sCls);
			            b.addClass(sCls + "_hover");
			        });
			        t.bind("mouseout", function () {
			            var b = $(this);
			            var sCls = b.attr("class").toUpperCase();
			            b.removeClass(sCls);
			            b.addClass(sCls.replace("_hover", ""));
			        });
			    }
			}
		);
	});
	jqoSubmit.each(function(){
	var t = $(this);
		$.each(aClass, 
			function(i,v){
				if(t.hasClass(v)){
					t.bind("mouseover", function(){
						var b = $(this);
						var sCls = b.attr("class").toUpperCase();
						b.removeClass(sCls);
						b.addClass(sCls + "_hover");
					});
					t.bind("mouseout", function(){
						var b = $(this);
						var sCls = b.attr("class").toUpperCase();
						b.removeClass(sCls);
						b.addClass(sCls.replace("_hover",""));
					});		
				}			
			}
		);
        });
    }
};