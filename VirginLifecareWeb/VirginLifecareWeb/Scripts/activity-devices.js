﻿//Activity devices JS
var DeviceList = new Object();
DeviceList = {
    "Devices": [
        {
            "DeviceName": "Fitbit ",
            "IsConnected": false,
            "ShortDescription": "(includes most Fitbit trackers)",
            "ImageUrl": "V2/Content/Images/Activity/fitbit-devices-v2.gif",
            "ConnectionUrl": "https://api.fitbit.com/oauth/authorize?oauth_token=9d1a0d4218b6a145ca6d048826705d7a&oauth_token_secret=0d758c56c594c560c0ba1572fca3a4ac",
            "MoreInfoUrl": "http://www.fitbit.com"
        },
        {
            "DeviceName": "RunKeeper ",
            "IsConnected": false,
            "ShortDescription": "(includes devices from Wahoo Fitness, Withings and others)",
            "ImageUrl": "/V2/Content/Images/Activity/runkeeper-icon.png",
            "ConnectionUrl": "https://runkeeper.com/apps/authorize?client_id=d4c4c0186c5d4814945f6b31741fee51&response_type=code&redirect_uri=https://qrt.virginhealthmiles.com/v2/Activity/AddRunkeeperToken",
            "MoreInfoUrl": "http://www.runkeeper.com"
        },
        {
            "DeviceName": "Misfit",
            "IsConnected": true,
            "ShortDescription": "",
            "ImageUrl": "/V2/Content/Images/Activity/misfit.png",
            "ConnectionUrl": "http://www.misfitwearables.com/",
            "MoreInfoUrl": "http://www.misfitwearables.com/"
        }
    ]
};


$(document).ready(function () {
    //Load Activity devices list from JSON
    $.each(DeviceList.Devices,function(i,device) {

        var statusLabel = '';
        var deviceConnectionButton = '';
        if ( device.IsConnected == false ) {
            deviceConnectionButton = '<input type="button" class="btn yellow-button floatRight" value="Connect" onclick="window.location('+device.ConnectionUrl+')" />';
            statusLabel = '<span class="status-data red">Disconnected</span>';
        } else {
            deviceConnectionButton = '<input type="button" class="btn grey-button floatRight" value="Disconnect" onclick="window.location('+device.ConnectionUrl+')" />';
            statusLabel = '<span class="status-data green">Connected</span>';
        }
      
        var deviceHTml = '<div class="device-item '+device.DeviceName+'"><div class="device-image"><img src="' + device.ImageUrl + '" /></div><div class="device-connection"><h4>' + device.DeviceName + ' <br />' +
            '<span>'+device.ShortDescription+'</span></h4><div class="status">Status: '
            +statusLabel+ '<br/><a href="' + device.MoreInfoUrl + '" target="_blank">Learn more</a>' +
            '</div><div class="device-action">'+ deviceConnectionButton +
            '</div><div class="clearfix"></div></div><div class="clearfix"></div>';

        $('.devices-list').append(deviceHTml);
    });
});
