﻿var ConversionUtil = new Object();
ConversionUtil.Height = new Object();
ConversionUtil.Weight = new Object();
ConversionUtil.Cholesterol = new Object();
ConversionUtil.Glucose = new Object();
ConversionUtil.Triglycerides = new Object();

var fInc = parseFloat(0.3937);
var fLbs = parseFloat(0.4536);

var fCholesterol = parseFloat(0.0259);
var fGlucose = parseFloat(0.0555);
var fTriglycerides = parseFloat(0.0113);

/******* height ************/
ConversionUtil.Height.sysToImperial = function (sysVal) {
    var nSysVal = parseFloat(sysVal);
    var nTotal = Math.round(nSysVal * fInc);
    var aResult = new Array();
    aResult["ft"] = Math.floor(nTotal / parseFloat(12));
    aResult["inches"] = parseInt(Math.round(nTotal % 12));
    return aResult;
}
ConversionUtil.Height.ImperialToSys = function (ft, inch) {
    var nTotal = parseInt(ft) * 12 + parseInt(inch);
    return Math.round(nTotal / fInc);
}

/******* weight ************/
ConversionUtil.Weight.sysToImperialUS = function (sysVal) {
    return Math.round(parseFloat(sysVal) / fLbs);
}
ConversionUtil.Weight.sysToImperialUK = function (sysVal) {
    var nTotalLbs = this.sysToImperialUS(sysVal);
    var aResult = new Array();
    aResult["stone"] = Math.floor(nTotalLbs / parseFloat(14));
    aResult["lbs"] = Math.floor(nTotalLbs % parseFloat(14));
    return aResult;
}
ConversionUtil.Weight.ImperialUSToSys = function (lbs) {
    return parseFloat(lbs) * fLbs;
}
ConversionUtil.Weight.ImperialUKToSys = function (stn, lbs) {
    var nTotalLbs = parseInt(stn) * 14 + parseInt(lbs);
    return this.ImperialUSToSys(nTotalLbs);
}


/******* Cholesterol ************/
ConversionUtil.Cholesterol.sysToImperialUS = function (mmoll) {
    return Math.round(parseFloat(mmoll) / fCholesterol);
}

ConversionUtil.Cholesterol.ImperialUSToSys = function (mgdl) {
    return parseFloat(mgdl) * fCholesterol;
}

/******* Glucose ************/
ConversionUtil.Glucose.sysToImperialUS = function (mmoll) {
    return Math.round(parseFloat(mmoll) / fGlucose);
}

ConversionUtil.Glucose.ImperialUSToSys = function (mgdl) {
    return parseFloat(mgdl) * fGlucose;
}

/******* Triglycerides ************/
ConversionUtil.Triglycerides.sysToImperialUS = function (mmoll) {
    return Math.round(parseFloat(mmoll) / fTriglycerides);
}

ConversionUtil.Triglycerides.ImperialUSToSys = function (mgdl) {
    return parseFloat(mgdl) * fTriglycerides;
}