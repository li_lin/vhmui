﻿var VHM = VHM || {};
VHM.Notification = VHM.Notification || {}; //Notifications -->Notifications object - general through the site
VHM.Notification.ChallengesServiceUrl = "../../webservices/WSVHMChallenge.asmx";
VHM.Notification.SecureChallengeURL = "../../Secure/Webservices/Challenge.asmx";
VHM.Notification.WSSocialUrl = "../../webservices/WSVHMSocial.asmx";
VHM.Notification.JsonSocialUrl = "../../v2/Social";
VHM.Notification.SessionID;
var nameSpace = "http://VirginHealthMiles.com/VirginLifeCare/WebServices/";
VHM.Notification.appPath = "";
VHM.Notification.ReloadNotifications = false;
VHM.Notification.MaxHeight = 100;

VHM.Notification.API_BASE = location.protocol + '//' + location.host;
VHM.Notification.NUTRITION_BASE_URL = VHM.Notification.API_BASE + '/rest/v1/nutrition';

$( document ).ready( function() {
    $(document).on("click", "#lnkNotifications, #imgNotifIcon, #notificationPopupTrigger, .notificationPopupTrigger", function () {
        VHM.Notification.openNotifications('divVHMNotification');
        VHM.Social.NewsFeed.loadNotifications();
    });
} );
VHM.Notification.LoadData = function () {
    VHM.Notification.ReloadNotifications = false;
    VHM.Notification.MaxHeight = VHM.Notification.GetBrowserHeight() - 200;

    $('#btnViewMessages').click(function () {
        $(this).attr("disabled", "true");
        $.ajax({
            requestType: "httpget",
            url: VHM.Notification.WSSocialUrl + "/UpdateNotificationToViewedStatus",
            contentType: "text/xml",
            dataType: "xml",
            nameSpace: nameSpace,
            //methodName: "UpdateNotificationToViewedStatus",
            success: function (data, textStatus) {
                window.location.href = VHM.Notification.appPath + '/Secure/MessageCenter/MessageCentral.aspx?tb=tabinbox&st=read&pg=1&itm=20&dir=desc';
            },
            error: function (xhr, ajaxOptions, error) {
                //redirect in case of error
                window.location.href = VHM.Notification.appPath + '/Secure/MessageCenter/MessageCentral.aspx?tb=tabinbox&st=read&pg=1&itm=20&dir=desc';
            }
        });
    });
    $(".jqModalPendGr").click(function () {
        var grpID = $(this).attr("vlc_GroupID");
        VHM.SocialGroups.GetPublicGroup(grpID, "divNotifGrpInv");
        VHM.Notification.openPopUp("divNotifGrpInv", '480px', false);
        $("#divNotifGrpInv").dialog("open");
        $(".closeGrpProfile").live('click', function () {
            $("#divNotifGrpInv").dialog('close');
        });
    });
    $(".jqModalPendFr").click(function () {
        var cfUserID = $(this).attr("vlc_UserID");
        VHM.SocialFrnds.GetCFUserProfData_Dialog(cfUserID, "divPendUserProf");
        VHM.Notification.openPopUp("divPendUserProf", '490px', false);
        $("#divPendUserProf").dialog("open");
        $("#divPendUserProf").find(".closeUserProf").click(function () {
            $("#divPendUserProf").dialog("close");
        });
    });
    $(".jqModalComment").click(function () {
        var cfUserID = $(this).attr("vlc_UserID");
        VHM.SocialFrnds.GetCFUserProfData_Dialog(cfUserID, "divPendUserProf");

        VHM.Notification.openPopUp("divPendUserProf", '490px', false);
        $("#divPendUserProf").dialog("open");
        $("#divPendUserProf").find(".closeUserProf").click(function () {
            $("#divPendUserProf").dialog("close");
        });
    });
    $(".nudgeLink").click(function () {
        var inviteId = $(this).attr("inviteId");
        var challengeId = $(this).attr("secid");
        var data = { challengeId: challengeId, inviteId: inviteId };
        var sData = JSON.stringify(data);
        var link = "/Secure/Challenge/SEChallenge.aspx?secId=" + challengeId;
        var secUrl = VHM.Notification.appPath + link;

        $.ajax({
            url: VHM.Notification.SecureChallengeURL + '/SaveInviteViewed',
            contentType: "application/json; charset=utf-8",
            data: sData,
            type: 'POST',
            dataType: 'json',
            success: function () {
                window.location.href = secUrl;
            }
        });
        return false;
    });
    $(".trkLink").click(function () {
        var inviteId = $(this).attr("inviteId");
        var challengeId = $(this).attr("challengeId");
        var data = { challengeId: challengeId, inviteId: inviteId };
        var sData = JSON.stringify(data);
        var link = $(this).attr("challengeLink");
        var secUrl = VHM.Notification.appPath + link;

        $.ajax({
            url: VHM.Notification.SecureChallengeURL + '/SaveInviteViewed',
            contentType: "application/json; charset=utf-8",
            data: sData,
            type: 'POST',
            dataType: 'json',
            success: function () {
                window.location.href = secUrl;
            }
        });
        return false;
    });

    $(".modInviteLink").click(function () {
        var event = $(this);
        var notificationId = $(this).attr("inviteId");
        var notifiedById = $(this).attr("notifiedBy");
        var link = $(this).attr("moduleLink");
        var secUrl = VHM.Notification.appPath + link;
        event.prop('disabled', true);
        $.ajax({
            url: VHM.Notification.NUTRITION_BASE_URL + '/friends/notifications',
            contentType: "application/json; charset=utf-8",
            data: '{ "NotifiedById": ' + notifiedById + ',"MemberNotificationId": ' + notificationId + ' }',
            type: 'DELETE',
            dataType: 'json',
            success: function (data) {
                if (data) {
                    window.location.href = secUrl;
                    VHM.Notification.ReloadNotifications = true;
                } else {
                    event.prop('disabled', false);
                    alert("Something went wrong, please try again");
                }
            },
            error: function () {
                event.prop('disabled', false);
                alert("Something went wrong, please try again");
            }
        });
        return false;
    });

    $("#divUserBadges").dialog({
        modal: true,
        autoOpen: false,
        resizable: false,
        draggable: false
    });

    $(".divApproveRej").css({ 'float': 'right', 'width': '115px', 'text-align': 'center' });
    //display only 3 notifications per section on load.
    VHM.Notification.DisplayLess();
    var jqoNotificContain = $(".divNotificContainer");
    jqoNotificContain.css({ 'max-height': VHM.Notification.MaxHeight });
    //jqoNotificContain.parent().css({ 'padding-right': '10px' });
    //we need to make sure the GroupInvit pop up doesn't show.
};
VHM.Notification.openNotifications = function ( dialogId ) {
    VHM.Notification.openPopUp( dialogId, '630px', true );
    $( "#" + dialogId ).find( ".closeNotifications" ).live( "click", function () {
        $( "#" + dialogId ).dialog( "close" );
        if ( VHM.Notification.ReloadNotifications ) {
            location.reload();
        };
    } );
    $( "#" + dialogId ).parent().css( { 'top': '70px' } );
};

VHM.Notification.openPopUp = function ( dialogId, strWidth, bAutoOpen ) {
    $("#" + dialogId).dialog({
        modal: true
        , autoOpen: bAutoOpen
        , width: strWidth
        , resizable: false
        , maxHeight: VHM.Notification.MaxHeight
        , dialogClass: 'no-title'
        , position: 'center'
    });
};

VHM.Notification.GetBrowserHeight = function () {
    var winH = 460;
    if ( document.body && document.body.offsetWidth ) {
        winH = document.body.offsetHeight;
    }
    if ( document.compatMode == 'CSS1Compat' &&
    document.documentElement &&
    document.documentElement.offsetWidth ) {
        winH = document.documentElement.offsetHeight;
    }
    if ( window.innerWidth && window.innerHeight ) {
        winH = window.innerHeight;
    }
    return winH;
};
VHM.Notification.SaveWGChallengeInvite_Notific = function ( challId, option, notifId ) {
    var param = { sId: VHM.Social.SessionID, challengeID: challId, notificID: notifId, userSelection: option };
    $.ajax( {
        requestType: "httpget",
        url: VHM.Notification.WSSocialUrl + "/updateChallNotification",
        data: param,
        contentType: "text/xml",
        dataType: "xml",
        nameSpace: nameSpace,
        success: function ( data, textStatus ) {
            VHM.Notification.ReloadNotifications = true;
            if ( option === 'Accept' ) {
                window.location.href = VHM.Notification.appPath + '/secure/challenge/invitation.aspx?challengeid=' + challId;
            } else {
                VHM.Notification.makeSelection( 'Decline', challId );
            }
        }
    } );
};
VHM.Notification.makeSelection = function ( strOpt, challInvID ) {
    var btnID = "btnAcceptCHInv_" + challInvID;
    var lnkDeclID = "hrefDeclineCH_" + challInvID;
    var spanID = "spSlctCH_" + challInvID;

    var btnAcc = $.find( "[vlc_name='" + btnID + "']" );
    var lnkDecline = $.find( "[vlc_name='" + lnkDeclID + "']" );
    var spanOption = $.find( "[vlc_name='" + spanID + "']" );

    $( btnAcc ).hide();
    $( lnkDecline ).hide();
    $( spanOption ).show();
    if ( strOpt == "Decline" ) {
        $( spanOption ).val( "Challenge invitation declined." );
        $( spanOption ).text( "Challenge invitation declined." );
    }
    VHM.Notification.ReloadNotifications = true;
};
VHM.Notification.acceptGroupInvitation = function (groupId, obj) {
    $(obj).attr("disabled", "true");
    var divHolder = $( obj ).parent();
    var hdnNotifiIds = divHolder.find( "input:hidden" ).val();
    if ( typeof hdnNotifiIds !== "undefined" && hdnNotifiIds !== "" ) {
        $.ajax( {
            url: VHM.Notification.JsonSocialUrl + '/_AddMemberToGroupByNotificationId',
            contentType: "application/json; charset=utf-8",
            data: '{ "id": ' + hdnNotifiIds + ' }',
            type: 'POST',
            dataType: 'json',
            success: function () {
                VHM.SocialGroups.makeSelection('Accept', groupId);
                $.ajax({
                    url: window.location.protocol + "//" + window.location.host + "/Secure/WebServices/Challenge.asmx/CreateGroupRewardTrigger",
                    type: "POST",
                    contentType: "application/json; charset=utf-8"
                });
                $(obj).removeAttr("disabled");
                VHM.Notification.ReloadNotifications = true;
            }
        } );
    }
};
VHM.Notification.ConfirmGroupDecline = function (groupId, notifID) {
    var jqoConfirm = $("#divConfirmDecline");
    jqoConfirm.dialog({
        modal: true
        , autoOpen: true
        , resizable: false
        , maxHeight: 400
        , dialogClass: 'VHMConfirmPopUps'
        , beforeClose: function (event, ui) { }
    });
    $(".ui-dialog-titlebar").hide();
    var jqoOkBtn = $('input[name$="btnConfirmDecline"]');
    jqoOkBtn.unbind('click'); jqoOkBtn.removeAttr('onclick');
    jqoOkBtn.click(function () {
        VHM.SocialGroups.DeclineGroupInvitation(groupId);
        $("#divConfirmDecline").dialog("close");
        VHM.Notification.ReloadNotifications = true;
    });
    var jqoBtnCancel = $('input[name$="btnCancelDecline"]');
    jqoBtnCancel.click(function () {
        $("#divConfirmDecline").dialog("close");
    });
};

VHM.Notification.UpdateFrReqAction = function(strOption, usrID, obj) {
    var divHolder = $(obj).parent();
    var hdnNotifiId = divHolder.find("input:hidden").val();
    if (typeof hdnNotifiId !== "undefined" && hdnNotifiId !== "") {
        VHM.Notification.ReloadNotifications = true;
        VHM.SocialFrnds.relationshipChanged = true;
        if (strOption === 'Accept') {
            VHM.SocialFrnds.acceptFriendship('Accept', usrID, hdnNotifiId);
        } else if (strOption === 'Decline') {
            VHM.SocialFrnds.declineFriendship('Decline', usrID, hdnNotifiId);
        }
    }
};

VHM.Notification.updateCmtLikeNotific = function ( wallId, objClicked ) {
    var wallUrl = VHM.Notification.appPath + "/Secure/Social/MyNewsFeed.aspx?wID=" + wallId + "&isNotific=true&wto=comment";
    var divHolder = $( objClicked ).parent();
    var hdnNotifiId = divHolder.find( "input:hidden" ).val();
    if ( typeof hdnNotifiId !== "undefined" && hdnNotifiId !== "" ) {
        var param = { sId: VHM.Social.SessionID, notificIDs: hdnNotifiId };
        $.ajax( {
            requestType: "httpget",
            url: VHM.Notification.WSSocialUrl + "/updateMultipleNotifications",
            data: param,
            contentType: "text/xml",
            dataType: "xml",
            nameSpace: nameSpace,
          //  methodName: "",
            success: function ( data, textStatus ) {
                VHM.Notification.ReloadNotifications = true;
                window.location.href = wallUrl;
            }
        } );
    };
};

VHM.Notification.DisplayLess = function () {
    //Comments section    
    var commentItems = $("#divRptComments").find("ul").children();
    var showAllCmts = $("#divShowAllCmts");
    if (commentItems.length > 3) {
        commentItems.each(function (index) {
            if (index > 2) {
                $(this).hide();
            }
        });
        showAllCmts.show();
        VHM.Notification.BuildShowAllLink(showAllCmts, commentItems, "Comments");
    } else {
        showAllCmts.hide();
    };

    //Likes Section 
    var likesItems = $("#divRptLikes").find("ul").children();
    var showAllLikes = $("#divShowAllLikes");
    if (likesItems.length > 3) {
        likesItems.each(function (index) {
            if (index > 2) {
                $(this).hide();
            }
        });
        showAllLikes.show();
        VHM.Notification.BuildShowAllLink(showAllLikes, likesItems, "Likes");
    } else {
        showAllLikes.hide();
    };
    //Group Invitations Section
    var grpInvItems = $("#divRptGrpInvitations").find("ul").children();
    var showAllGrpInv = $("#divShowAllGrpInvit");
    if (grpInvItems.length > 3) {
        grpInvItems.each(function (index) {
            if (index > 2) {
                $(this).hide();
            }
        });
        showAllGrpInv.show();
        VHM.Notification.BuildShowAllLink(showAllGrpInv, grpInvItems, "Group Invitations");
    } else {
        showAllGrpInv.hide();
    };

    //Friend Requests Section 
    var frReqItems = $("#divRptFriendRequests").find("ul").children();
    var showAllFrReq = $("#divShowAllFrReq");
    if (frReqItems.length > 3) {
        frReqItems.each(function (index) {
            if (index > 2) {
                $(this).hide();
            }
        });
        showAllFrReq.show();
        VHM.Notification.BuildShowAllLink(showAllFrReq, frReqItems, "Friends Requests");
    } else {
        showAllFrReq.hide();
    };

    //Challenge Invitations Section
    var challIvitItems = $("#divRptChallInv").find("ul").children();
    var showAllChall = $("#divShowAllChallenges");
    if (challIvitItems.length > 3) {
        challIvitItems.each(function (index) {
            if (index > 2) {
                $(this).hide();
            }
        });
        showAllChall.show();
        VHM.Notification.BuildShowAllLink(showAllChall, challIvitItems, "Challenge Invitations");
    } else {
        showAllChall.hide();
    };

    //Nudges Section
    var nudgesItems = $("#divSECNudges").find("ul").children();
    var showAllNudges = $("#divShowAllNudges");
    if (nudgesItems.length > 3) {
        nudgesItems.each(function (index) {
            if (index > 2) {
                $(this).hide();
            }
        });
        showAllNudges.show();
        VHM.Notification.BuildShowAllLink(showAllNudges, nudgesItems, "Nudges");
    } else {
        showAllNudges.hide();
    };
};

VHM.Notification.ShowAllSectionNotific = function ( sectionItems, showAllDiv ) {
    sectionItems.each( function () {
        $( this ).show();
    } );
    showAllDiv.hide();
    var lnkShowAll = showAllDiv.find( "a" );
    lnkShowAll.text( "" );
    lnkShowAll.unbind( 'click' );
};

VHM.Notification.BuildShowAllLink = function ( showAllDiv, sectionItems, linkText ) {
    var lnk = showAllDiv.find( "a" );
    var lnkText = "See all " + sectionItems.length + " new " + linkText; //Challenge Invitations
    lnk.text( lnkText );
    lnk.click( function () {
        VHM.Notification.ShowAllSectionNotific( sectionItems, showAllDiv );
    } );
};
VHM.Notification.ClearAllNotific = function () {
    var hdnLastNotific = $( ".clearAllCls" ).find( "input:hidden" ).val();
    var allNudgeInvId;
    var invitesArr = [];
    $( ".nudgeLink" ).each( function ( i ) { //i = is actually the index of the loop
        var inviteId = $( this ).attr( "inviteId" );
        invitesArr[i] = inviteId;
    } );
    allNudgeInvId = JSON.stringify( invitesArr );
    if ( typeof hdnLastNotific !== "undefined" && hdnLastNotific !== "" && typeof allNudgeInvId !== "undefined" && allNudgeInvId !== "" ) {
        var param = { lastNotificID: hdnLastNotific, nudgeInviteIDs: allNudgeInvId };
        $.ajax( {
            requestType: "httpget",
            url: VHM.Notification.WSSocialUrl + "/ClearAllNotification",
            data: param,
            contentType: "text/xml",
            dataType: "xml",
            nameSpace: nameSpace,
            //methodName: "",
            success: function ( data, textStatus ) {
                window.location.reload();
            }
        } );
    }
};