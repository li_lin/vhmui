String.format = function(){
    if( arguments.length == 0 )
        return null;
	var str = arguments[0];
	
	for(var i=1;i<arguments.length;i++){
        var re = new RegExp('\\{' + (i-1) + '\\}','gm');
        str = str.replace(re, arguments[i]);
    }
	return str;
}
String.prototype.format = function () {
    var args = arguments;
    return this.replace( /{(\d+)}/g, function ( match, number ) {
        return typeof args[number] != 'undefined'
      ? args[number]
      : match
    ;
    } );
};
String.prototype.trim = function() {
    return this.replace( /^\s+|\s+$/g, "" );
}
String.prototype.ltrim = function() {
	return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
	return this.replace(/\s+$/,"");
}
String.prototype.convertLB = function(){
	return this.replace(/\n/g,"<br/>");
}
String.prototype.convertSC = function(){
	return this.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
}
String.prototype.wrapHtmlInCdata = function(){
	return "<![CDATA[" + this + "]]>";;
}
/*Format Number. i.e. 1000 to 1,000*/
String.prototype.addComma = function(){
	if(!isNaN(this)){
		outputArray = new Array();
		arrVal = this.split("");
		tmp = arrVal.reverse();
		
		for(i=0;i<tmp.length;i++){
			outputArray.push(tmp[i]);
			if(((i+1)%3) == 0)
				outputArray.push(",");
		}
		strOutput = outputArray.reverse().join("");
		if(strOutput.indexOf(",") == 0)
			strOutput = strOutput.substring(1,strOutput.length);
		return strOutput;
	}
	return this;
}
String.prototype.htmlEncode = function (  ) {
    return $( '<div/>' ).text( this.toString() ).html();
};
String.prototype.htmlDecode = function (  ) {
    return $( '<div/>' ).html( this.toString() ).text();
};
String.prototype.StripOutHTMLTags = function () {
    return this.replace( /<\/?[^>]+>/gi, '' );
}