﻿/*
* Plugin name :jquery.ActivityTracker
* Created on : 2011-10-15
* Description : A combination Slider + DatePicker with some enhanced functionality
* Depends: jquery.ui.draggable, jquery.ui.datepicker, ActivityTracker.css
OPTIONS:
        showDate: true              show/hide the datepicker
        startDate: new Date()       Initial value for the datepicker
        isYesNo: true               Indicates if the style of this ActivityTracker instance is a Yes/No, 
                                    in which case the initial position of the slider is in the middle and 
                                    there are only 3 positions.
        initialValue: 0             The initial value of the tracker
        minValueLabel: ''           The label on the left side of the tracker. "Yes" if isYesNo is true
        maxValueLabel: ''           The label on the right side of the tracker. "No" if isYesNo is true
        maxValue: 3                 The maximum value for the slider. Determines the interval for each slider increment.
RULES:
•	If maxValue <= 20 each slider interval is 1
•	If maxValue <= 100 each slider interval is 5
•	If maxValue <= 200 each slider interval is 10
•	If maxValue <= 1,000 each slider interval is 50
•	If maxValue <= 2,000 each slider interval is 100
•	If maxValue is <= 10,000 each slider interval is 500

*/
(function ($) {
    $.ActivityTracker = function (el, options) {

        // To avoid scope issues, use 'base' instead of 'this'
        // to reference this class from internal events and functions.
        var base = this;

        // Access to jQuery and DOM versions of element
        base.$el = $(el);
        base.el = el;

        base.OriginalMarkup = ''; //base.$el.html();

        // the value assigned to the tracker    
        base.TrackerValue = 0;

        base.TrackerDate = new Date();

        // Add a reverse reference to the DOM object
        base.$el.data('ActivityTracker', base);

        base._trackerPosition = 0;
        base.startValOffset = 0;
        base.startDate = null;
        base.gridX = 0;
        base.gridY = 0;
        base.interval = 0;
        base.baseWidth = 0;

        base.init = function () {

            base.options = $.extend({}, $.ActivityTracker.defaultOptions, options);

            base.TrackerDate = base.options.startDate;
            base.ItemID = base.options.itemID;

            if (base.options.isYesNo == false) {
                if (null != base.options.initialValue && base.options.initialValue < base.options.minValue) {
                    base.options.initialValue = base.options.minValue; // prevent this from wreaking havoc
                }
            }

            base.setValueInterval();

            base.buildMarkup();

            base.initializePlugins();

            base.setInitialState();

            base.attachEvents();

        };

        base.setInitialState = function () {
            var tracker = base.$el.find('div.trackerVal');
            if (base.options.initialValue != null || base.options.isYesNo) {
                setInitialValue(tracker);
            } else {
                $(tracker).removeClass('hasValue').addClass('noValue');
            }
        }

        base.setValueInterval = function () {

            var factor = 1;
            if (base.options.gridSize == null) {
                if (base.options.maxValue - base.options.minValue <= 10) {
                    factor = .5;
                } else if (base.options.maxValue - base.options.minValue <= 20) {
                    factor = 1;
                } else if (base.options.maxValue - base.options.minValue <= 100) {
                    factor = 5;
                } else if (base.options.maxValue - base.options.minValue <= 200) {
                    factor = 10;
                } else if (base.options.maxValue - base.options.minValue <= 1000) {
                    factor = 50;
                } else if (base.options.maxValue - base.options.minValue <= 2000) {
                    factor = 100;
                } else if (base.options.maxValue - base.options.minValue <= 10000) {
                    factor = 500;
                }
            } else {
                factor = (base.options.gridSize > 0)
                        ? (base.options.maxValue - base.options.minValue) / base.options.gridSize
                        : base.options.maxValue - base.options.minValue;
                //  factor = Math.round( factor );
                if (factor <= 0) {
                    factor = 1;
                }
            }
            base.interval = factor;

        }

        // add the html elements to the plugin
        base.buildMarkup = function () {

            var markup = '';
            markup += '<div>';

            var _val = base.options.initialValue || '';
            // datepicker
            if (base.options.showDate) {
                var dateCss = base.options.useAltYesNoInput ? 'trackerDateDisplayContainerYesNo' : 'trackerDateDisplayContainer';
                markup += '<div class="' + dateCss + '">';
                markup += '<input type="text" class="trackerDateDisplay" />';
                markup += '</div>';
            }

            //tracker
            var sliderTooltip = base.options.allowInput ? 'Select your answer by dragging the slider.' : '';
            if (base.options.trackerMessage.length > 0) {
                markup += '<div class="TrackerMessage">' + base.options.trackerMessage + '</div>';
            }
            markup += '<div class="' + base.options.baseCSSClass + '">';
            if (base.options.useAltYesNoInput) {
                // yes radio button
                var returnFalse = '';
                if ( base.options.allowInput == false ) {
                    returnFalse = ' onclick="return false;" ';
                }
                markup += '';
                markup += '<input type="button"' + returnFalse + ' name="trackerVal" class="btn btn-bool btn-bool-yes trackerVal" trackerValue="1" value="' + base.options.minValueLabel + '" />';
                
                // no radio button
                markup += '';
                markup += '<input type="button" ' + returnFalse + ' name="trackerVal" class="btn btn-bool btn-bool-no trackerVal"  trackerValue="-1" value="' + base.options.maxValueLabel + '" />';
            } else {
                markup += '<div class="trackerLabel lowVal">' + base.options.minValueLabel + '</div>';
                markup += '<div class="TrackerNoEditMessage">' + base.options.noEditText + '</div>';
                markup += '<div class="trackerVal" title="' + sliderTooltip + '">';
                if ( base.options.allowInput || _val != null ) {
                    markup += '<a href="#" onclick="return false;">' + _val + '</a>';
                }
                markup += '</div>';
                markup += '<div class="trackerLabel highVal">' + base.options.maxValueLabel + '</div>';
            }
            markup += '</div>';
            base.$el.append(markup);

            // apply the right tracker image based on the type of tracker and value
            if (base.options.initialValue == 0) {
                if ( base.options.useAltYesNoInput == false ) {
                    base.$el.find( '.trackerVal' ).addClass( 'noValue' );
                }
            } else {
                if ( base.options.useAltYesNoInput == false ) {
                    base.$el.find( '.trackerVal' ).addClass( 'hasValue' );
                }
            }

            if (base.options.showYesNoLabels == false) {
                base.$el.find('div.trackerLabel').hide();
            } else {
                base.$el.find('div.trackerLabel').show();
            }

            if (!base.options.showTracker) {
                base.$el.find('.trackerVal').hide();
            }
        }

        function setupGrid() {

            var width = parseInt(base.$el.find("." + base.options.baseCSSClass).css("width"));

            var $slider = base.$el.find('.trackerVal');
            base.gridY = $slider.height();

            if (base.options.isYesNo) {
                // Divide the slider into 3 parts. Left side is for Yes, Right side is for No. Middle is no value.
                var w = width / 3;
                base.gridX = Math.round(w) + 15;                

            } else {
                // Divide the slider into 20 equal parts. Because of the width of the slider and the container div, 
                // this makes the minimum grid interval to be 8.
                width -= $slider.width();
                var w = (base.options.gridSize > 0) ? width / base.options.gridSize : width;
                base.gridX = Math.round(w);
            }
            base.baseWidth = width;
        }

        function setInitialValue(tracker) {
            
            var $parent = $(tracker).parent();
            var positionLeft = $parent.position() ? $parent.position().left : 0;
            var baseLeft = positionLeft; // $.browser.mozilla ? $parent.offset().left : $parent.position().left; //$parent.offset().left;  
            if (base.options.usePositionForTracker == true) {
                baseLeft = positionLeft;
            }
            var newLeft = baseLeft;
            if (base.options.isYesNo && baseLeft > 0) {

                var NO_ANSWER = null;
                var YES = 1;
                var NO = -1;
                var DEFAULT = 0;

                switch (base.options.initialValue) {
                    case YES:
                        {
                            newLeft = baseLeft;
                            break;
                        }
                    case DEFAULT:
                        {
                            newLeft = baseLeft + base.gridX;
                            break;
                        }
                    case NO_ANSWER:
                        {
                            newLeft = baseLeft + base.gridX;
                            break;
                        }
                    case NO:
                        {
                            newLeft = baseLeft + (base.gridX * 2);
                            break;
                        }
                }
            } else {

                if (null != base.options.initialValue && base.options.initialValue > 0) {
                    for (var i = base.options.minValue; i < base.options.initialValue; i += base.interval) {
                        newLeft += base.gridX;
                    }
                }
            }
            if (base.options.useAltYesNoInput == true) {
                var yesButton = base.$el.find('.btn-bool-yes');
                var noButton = base.$el.find('.btn-bool-no');
                if ( base.options.initialValue == 1 ) {
                    $(yesButton).addClass("btn-main");
                    $(noButton).removeClass("btn-main");
                }
                if (base.options.initialValue == -1) {
                    $(yesButton).removeClass("btn-main");
                    $(noButton).addClass("btn-main");
                }
            } else {
                $(tracker).css('left', newLeft + 'px');                
            }
            setValue(tracker);
        }

        base.attachEvents = function() {

            // this allows the tracker value to update while it is being moved
            if ( base.options.allowInput ) {
                base.$el.mousedown( function() {
                    base.$el.mousemove( function() {
                        var tracker = base.$el.find('div.trackerVal');
                        setValue( tracker );
                    } );
                } );

                base.$el.mouseup( function() {
                    base.$el.unbind( "mousemove" );
                } );

                // Add functionality to move slider on the row click
                if ( base.options.isTrackClick ) {
                    base.$el.find( 'div.trackerVal' ).parent().mouseup( function( event, ui ) {
                        var tracker = base.$el.find( 'div.trackerVal' );
                        var $parent = $( tracker ).parent();

                        var parentLeft = $parent.offset().left;
                        var trackerWidth = $( tracker ).width() / 2;
                        var thisLeft = event.pageX - parentLeft;
                        
                        if ( thisLeft < trackerWidth ) thisLeft = trackerWidth;
                        else if ( thisLeft > $parent.width() - trackerWidth ) thisLeft = $parent.width() - trackerWidth

                        var val = Math.round( ( thisLeft - trackerWidth ) / base.gridX );
                        thisLeft = val * base.gridX + parentLeft;
                        $( tracker ).css( 'left', thisLeft + 'px' );
                        
                        setValue( tracker );
                        base.options.onSave( base );
                    } );
                }
            } else {
                var $tracker = $( base.$el.find( 'div.trackerVal' ) );
                $tracker.hover( function() {
                    var p = $.browser.mozilla ? $tracker.offset() : $tracker.position();
                    $tracker.parent().find( 'div.TrackerNoEditMessage' ).css( "left", p.left + "px" ).fadeIn( 'fast' );
                }, function() {
                    $tracker.parent().find( 'div.TrackerNoEditMessage' ).hide();
                } );
            }

        };

        function setValue(tracker) {
            
            if ( base.options.useAltYesNoInput == false ) {
                var $parent = $( tracker ).parent();
                var baseLeft = $parent.position().left; // $.browser.mozilla ? $parent.offset().left : $parent.position().left;

                var thisLeft = parseInt( $( tracker ).css( "left" ) ); //$.browser.mozilla ? $( tracker ).offset().left : $( tracker ).position().left;
                var marginLeft = parseInt( $( tracker ).css( 'margin-left' ) );

                if ( isNaN( marginLeft ) ) marginLeft = 0;

                var val = parseInt( thisLeft - baseLeft - marginLeft + 1 );
                
                //val = Math.max( baseLeft, val );
                //expose the value
                base.TrackerValue = val;
            }
            //update the display value
            if (base.options.isYesNo) {
                if (base.options.useAltYesNoInput == true) {
                  
                    base.TrackerValue = $(tracker).attr('trackerValue');
                    
                    var yesButton = base.$el.find('.btn-bool-yes');
                    var noButton = base.$el.find('.btn-bool-no');
                    if (base.TrackerValue == 1) {
                        $(yesButton).addClass("btn-main");
                        $(noButton).removeClass("btn-main");
                    }
                    if (base.TrackerValue == -1) {
                        $(yesButton).removeClass("btn-main");
                        $(noButton).addClass("btn-main");
                    }
                    
                } else {
                    if ( val < base.gridX ) {
                        $( tracker ).removeClass( 'noValue' ).addClass( 'hasValue' ).find( "a" ).html( base.options.YesContent );
                        base.TrackerValue = 1;
                    } else if ( val >= base.gridX * 2 ) {
                        // The middle value for the yes-no option is about 80, so to determine if the user selected
                        // No, test for value between 85 and 170
                        $( tracker ).removeClass( 'noValue' ).addClass( 'hasValue' ).find( "a" ).html( base.options.NoContent );
                        base.TrackerValue = -1;
                    } else {
                        $( tracker ).removeClass( 'hasValue' ).addClass( 'noValue' ).find( "a" ).html( '' );
                        base.TrackerValue = 0;
                    }
                }
            } else {
                var _trackerVal = (base.gridX > 0) ? Math.round(val / base.gridX) : val;
                var displayVal = base.options.minValue + (_trackerVal * base.interval);
                displayVal = Math.round(displayVal);
                displayVal = base.options.valueTranslateFunction(displayVal);

                $(tracker).find("a").html(displayVal);
                $(tracker).removeClass('noValue').addClass('hasValue');
                base.TrackerValue = base.options.minValue + (_trackerVal * base.interval);
            }
            return true;
        }

        // wire up datepicker and draggable
        base.initializePlugins = function() {
            
            if ( base.options.showDate ) {
                var param = {
                    buttonImage: '../../images/challenges/icon_calendar_date.gif',
                    buttonImageOnly: true,
                    defaultDate: new Date( base.options.startDate ).format( 'dddd MM/dd' ),
                    dateFormat: 'DD mm/dd',
                    duration: 'fast',
                    maxDate: '0d',
                    minDate: new Date( base.options.minDate ).format( 'dddd MM/dd' ),
                    showAnim: 'fadeIn',
                    showOn: 'both',
                    close: function() { $( ".trackerVal" ).css( "z-index", 2 ); },
                    beforeShow: function( input, inst ) {
                        $( ".trackerVal" ).css( "z-index", 1 );
                        //.removeClass( "ui-widget-content" );
                        $( "#ui-datepicker-div" ).addClass( "trackerDatePicker" );
                        if ( $( "div.anythingSlider .anythingControls ul a.cur" ).html() != null ) {
                            $( "div.anythingSlider .anythingControls ul a.cur" ).click();
                        }
                    },
                    onSelect: function( dateText, inst ) {
                        base.TrackerDate = ( inst.currentMonth + 1 ) + '/' + inst.currentDay + '/' + inst.currentYear;
                        base.options.onDateChange( base.options.itemID, base.TrackerDate );
                        $( "#ui-datepicker-div" ).removeClass( "trackerDatePicker" );
                    }
                };
                base.$el.find( '.trackerDateDisplay' ).datepicker( param ).val( new Date( base.options.startDate ).format( 'dddd MM/dd', true ) );
            }

            setupGrid();

            if (base.options.allowInput) {
                var _gridX = base.gridX;
                if (base.gridX == 87 && base.options.isYesNo && ($.browser.msie && parseInt($.browser.version, 10) > 8) || $.browser.mozilla) {
                    _gridX = 86.5; // the value 87 does not work in IE 10 for yes/no.
                }
                base.$el.find( 'div.trackerVal' ).draggable( {
                    containment: 'parent',
                    opacity: .4,
                    axis: 'x',
                    grid: [_gridX, base.gridY],
                    stop: function( event, ui ) {

                        setValue( event.target || event.srcElement );
                        base.options.onSave( base );
                    }
                } );
            }

            if ( base.options.useAltYesNoInput == true ) {

                if ( base.options.allowInput == true ) {
                    base.$el.find( "input.trackerVal" ).click( function( event ) {
                        setValue( event.target || event.srcElement );
                        base.options.onSave( base );

                        try {
                            event.cancelBubble( false );

                        } catch( e ) {
                            // IE has a problem with cancelBubble();
                        }
                        return false;
                    } );
                }
            }

        };

        base.moveToLeft = function() {
            base.moveTo( -base.gridX );
        };

        base.moveToRight = function() {
            base.moveTo(base.gridX);            
        };

        base.moveTo = function( addedVal ) {
            var tracker = base.$el.find( 'div.trackerVal' );
            var $parent = $( tracker ).parent();
            var parentLeft = $parent.offset().left;

            var thisLeft = $( tracker ).offset().left;
            thisLeft += addedVal;
           
            if ( thisLeft >= parentLeft && thisLeft + $( tracker ).width() <= parentLeft + $parent.width() ) {
                $( tracker ).css( 'left', thisLeft + 'px' );

                setValue( tracker );
                base.options.onSave( base );
            }
        };

        base.select = function() {
            base.$el.closest( ".trackerRow" ).addClass( "selected" );
        };

        base.unSelect = function() {
            base.$el.closest( ".trackerRow" ).removeClass( "selected" );
        };

        // Run initializer
        base.init();
    };

    $.ActivityTracker.items = {};

    $.ActivityTracker.defaultOptions = {
        itemID: -1,
        showDate: true,
        startDate: new Date(),
        minDate: new Date(),
        isYesNo: true,
        isTrackClick: false,
        showYesNoLabels: false,
        initialValue: null,
        gridSize: 20,
        minValueLabel: '',
        maxValueLabel: '',
        minValue: 0,
        maxValue: 3,
        baseCSSClass: 'ActivityTracker',
        trackerMessage: '',
        noEditText: '',
        valueTranslateFunction: function (v) { return v; },
        allowInput: true,
        onSave: function (t) { return true; },
        onDateChange: function (id, dt) { return true; },
        usePositionForTracker: false,
        YesContent: '',
        NoContent: '',
        useAltYesNoInput: false
    };

    $.fn.ActivityTracker = function (options) {
        return this.each(function () {
            var tracker = new $.ActivityTracker(this, options);
            var id = $(this).closest(".trackerRow").attr("id");
            if (id != 'undefined' && id != undefined && id.length > 0) {
                $.ActivityTracker.items[id] = tracker;
            }
        });
    };

    $("div.trackerVal a").live("focus", function () {
        
    });

})(jQuery);