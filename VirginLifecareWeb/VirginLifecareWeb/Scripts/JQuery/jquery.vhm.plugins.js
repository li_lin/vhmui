﻿/*
*	Plugin name : jquery.vhmtabs
*	Author : Jimmy Hattori
*	Created on : 2009-08-28
*	Description : Initialize tabs control.
*	Dependancies : jquery.js, style_master.css
*
*	Change History:
*	28-08-2009	MJH	Initial Version
*	31-08-2009	MJH	Added condition to override if onclick attribute is specified.
*
*	Example:
*		<script type="text/javascript">
*			$(document).ready(function(){
*				$("#tabs").vhmtabs();
*			});
*			function byAjax(){
*				$.get("dummy.aspx",function(data){
*					$("#TestDiv2").html(data);
*				});
*			}
*		</script>	
*		<div id="tabs">
*			<ul>
*				<li><a href="#">Tab 1</a></li>
*				<li><a href="#" onclick="byAjax()">Tab 2</a></li>
*				<li><a href="#">Tab 3</a></li>
*			</ul>
*			<div>
*				<p>Content 1</p>
*			</div>
*			<div id="TestDiv2">
*				<p>Content 2</p>
*			</div>
*			<div>
*				<p>Content 3</p>
*			</div>
*		</div>
*/
jQuery.fn.vhmtabs = function(options){
	var settings = {size:""};
	
	$.extend(settings,options);
					
	/* Adding a class to the outer wrapper(div tag) */
	if(!this.hasClass("tabdiv"))
		this.addClass("tabdiv");
	
	/* Loop child elements of the outer wrapper */
	this.children().each(function(){
		var t = $(this);
		switch(this.tagName.toLowerCase()){
			case "div":/* div for each content. Initially set to invisible(display:none). */
				/* Add the class to the content div if the class was not assigned. */
				if(t.is("div") && !t.hasClass("tabcontent")){
					t.addClass("tabcontent");
				}
				break;
			case "ul":
				/*Add the class to UL tag */
				if(!t.hasClass("tablist")){
					t.addClass("tablist");
				}
				/*Loop LI tags nested inside of this UL tag */
				t.children().each(function(i){
					var li = $(this);
					/* Add the class to LI tag */
					if(!li.hasClass("tabitem") && !li.hasClass("tabitem_large") && !li.hasClass("tabitem_small")){
						switch(settings.size.toLowerCase()){
							case "large":
								li.addClass("tabitem_large");
								break;
							case "small":
								li.addClass("tabitem_small");
								break;
							default:
								li.addClass("tabitem");
								break;
						}
					}
					
					
					li.attr("index",i);
					/* Loop the child elements of LI tag */	
					li.children("a").each(function(){
						var a = $(this);
						/* Specify the behavior on click event */
						var sJs = (a.prop("onclick") != null && a.prop("onclick") != "") ? a.prop("onclick") : "";
						
						a.parent().append("<span class='tab_on'>" + a.html() + "</span>");
						
						a.bind("click",function(e){
							var s = $(this);
							
							/* Display anchor and hide span of previously selected tab*/
							t.children().each(function(){
								var ss = $(this).find("a");
								if(ss.hasClass("tab_on"))
									ss.removeClass("tab_on");
								$(this).children().each(function(){
									switch(this.tagName.toLowerCase()){
										case "a":
											$(this).show();
											break;
										case "span":
											$(this).hide();
											break;
									}
								});
							});
							
							/* Add the class to current element. */
							if(!s.hasClass("tab_on"))
								s.addClass("tab_on");
							
							/* Show/Hide all tab contents. */
							t.parent().children("div").each(function(v){
								var d = $(this);
								if(i==v){
									d.show();
								}
								else
									d.hide();
							});
							eval(sJs);
							a.hide();
							s.parent().children("span").show();
						});
					});
				});
				break;
		}
	});
	var aLi = this.find("ul").children("li");
	if(aLi.length > 0){
		$(aLi[0]).find("a").trigger("click");
	}
};
/*
*	Plugin name : jquery.vhmDialog (Jimmy Hattori), jquery.cover, jquery.uncover, jquery.vhmExtendableButton
*	Author : Aleg Yelshov
*	Created on : 2010-01-19
*	Dependancies : style_buttons.css
*/
(function($) {
	$.fn.vhmDialog = function(opt){
		var defOpt = {resizable:false,
					open:function(event,ui){
						$($(this).parent().children()[0]).hide();
						$(this).css("text-align","center");
					}};
		$.extend(defOpt,opt);
		$(this).dialog(defOpt);
		return this;
	};
	$.fn.cover = function(options) {
		if(!this.hasCover){
			var settings = {'opacity': 0.75};
			if (options) $.extend(settings, options);
			this.each(function() {
				var $this = jQuery(this);
				var h = $this.height();
				var w = $this.width();
				var zIx = $this.css('z-index');
				zIx = (zIx === 'auto' || zIx === 'inherit') ? '2' : zIx + '';
				var cv = jQuery("<div / >").css({'position':'absolute', 'left':'0px', 'top':'0px', 'background-color':'#fff','opacity':settings.opacity.toString(),'filter':'alpha(opacity=' + (settings.opacity * 100).toString() + ')', 'z-index':zIx}).height(h).width(w);
				$this.prepend(cv).wrapInner("<div style='position:relative;'/>");
			});
		}
		this.hasCover = true;
		return this;
	};
	$.fn.uncover = function() {
		if(this.hasCover){
			this.each(function() {
				var $this = jQuery(this);
				var $child = $($this.children().get(0));
				$($child.children().get(0)).remove();
				var cnt = $child.contents();
				$child.replaceWith(cnt);
			});
			this.hasCover = false;
		}
		return this;
	};
	$.fn.vhmExtendableButton = function() {
		this.each(function() {
			if(this.nodeName !== "BUTTON") return;
			this.className = "extendable";
			var $this = $(this);
			var ul = $('<ul></ul>');
			ul.append("<li class='left'></li><li class='middle'>" + this.innerHTML + "</li><li class='right'></li>");
			$this.html(ul);
			if($this.width() === $("li.middle", ul).width()){// IE7: fix ul width
				$this.width($this.width() + 16);
			}
			$this.bind("mouseenter",function(){
				$("li.left", this).addClass("left_hover");
				$("li.middle", this).addClass("middle_hover");
				$("li.right", this).addClass("right_hover");
			}).bind("mouseleave",function(){
				$("li.left", this).removeClass("left_hover");
				$("li.middle", this).removeClass("middle_hover");
				$("li.right", this).removeClass("right_hover");
			});
		});
		return this;
	};
})(jQuery);

/*!
* jQuery Templates Plugin 1.0.0pre
* http://github.com/jquery/jquery-tmpl
* Requires jQuery 1.4.2
*
* Copyright 2011, Software Freedom Conservancy, Inc.
* Dual licensed under the MIT or GPL Version 2 licenses.
* http://jquery.org/license
*/
(function ( jQuery, undefined ) {
    var oldManip = jQuery.fn.domManip, tmplItmAtt = "_tmplitem", htmlExpr = /^[^<]*(<[\w\W]+>)[^>]*$|\{\{\! /,
		newTmplItems = {}, wrappedItems = {}, appendToTmplItems, topTmplItem = { key: 0, data: {} }, itemKey = 0, cloneIndex = 0, stack = [];

    function newTmplItem( options, parentItem, fn, data ) {
        // Returns a template item data structure for a new rendered instance of a template (a 'template item').
        // The content field is a hierarchical array of strings and nested items (to be
        // removed and replaced by nodes field of dom elements, once inserted in DOM).
        var newItem = {
            data: data || ( data === 0 || data === false ) ? data : ( parentItem ? parentItem.data : {} ),
            _wrap: parentItem ? parentItem._wrap : null,
            tmpl: null,
            parent: parentItem || null,
            nodes: [],
            calls: tiCalls,
            nest: tiNest,
            wrap: tiWrap,
            html: tiHtml,
            update: tiUpdate
        };
        if ( options ) {
            jQuery.extend( newItem, options, { nodes: [], parent: parentItem } );
        }
        if ( fn ) {
            // Build the hierarchical content to be used during insertion into DOM
            newItem.tmpl = fn;
            newItem._ctnt = newItem._ctnt || newItem.tmpl( jQuery, newItem );
            newItem.key = ++itemKey;
            // Keep track of new template item, until it is stored as jQuery Data on DOM element
            (stack.length ? wrappedItems : newTmplItems )[itemKey] = newItem;
        }
        return newItem;
    }

    // Override appendTo etc., in order to provide support for targeting multiple elements. (This code would disappear if integrated in jquery core).
    jQuery.each( {
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function ( name, original ) {
        jQuery.fn[name] = function ( selector ) {
            var ret = [], insert = jQuery( selector ), elems, i, l, tmplItems,
				parent = this.length === 1 && this[0].parentNode;

            appendToTmplItems = newTmplItems || {};
            if ( parent && parent.nodeType === 11 && parent.childNodes.length === 1 && insert.length === 1 ) {
                insert[original]( this[0] );
                ret = this;
            } else {
                for ( i = 0, l = insert.length; i < l; i++ ) {
                    cloneIndex = i;
                    elems = ( i > 0 ? this.clone( true ) : this ).get();
                    jQuery( insert[i] )[original]( elems );
                    ret = ret.concat( elems );
                }
                cloneIndex = 0;
                ret = this.pushStack( ret, name, insert.selector );
            }
            tmplItems = appendToTmplItems;
            appendToTmplItems = null;
            jQuery.tmpl.complete( tmplItems );
            return ret;
        };
    } );

    jQuery.fn.extend( {
        // Use first wrapped element as template markup.
        // Return wrapped set of template items, obtained by rendering template against data.
        tmpl: function ( data, options, parentItem ) {
            return jQuery.tmpl( this[0], data, options, parentItem );
        },

        // Find which rendered template item the first wrapped DOM element belongs to
        tmplItem: function () {
            return jQuery.tmplItem( this[0] );
        },

        // Consider the first wrapped element as a template declaration, and get the compiled template or store it as a named template.
        template: function ( name ) {
            return jQuery.template( name, this[0] );
        },

        domManip: function ( args, table, callback, options ) {
            if ( args[0] && jQuery.isArray( args[0] ) ) {
                var dmArgs = jQuery.makeArray( arguments ), elems = args[0], elemsLength = elems.length, i = 0, tmplItem;
                while ( i < elemsLength && !( tmplItem = jQuery.data( elems[i++], "tmplItem" ) ) ) { }
                if ( tmplItem && cloneIndex ) {
                    dmArgs[2] = function ( fragClone ) {
                        // Handler called by oldManip when rendered template has been inserted into DOM.
                        jQuery.tmpl.afterManip( this, fragClone, callback );
                    };
                }
                oldManip.apply( this, dmArgs );
            } else {
                oldManip.apply( this, arguments );
            }
            cloneIndex = 0;
            if ( !appendToTmplItems ) {
                jQuery.tmpl.complete( newTmplItems );
            }
            return this;
        }
    } );

    jQuery.extend( {
        // Return wrapped set of template items, obtained by rendering template against data.
        tmpl: function ( tmpl, data, options, parentItem ) {
            var ret, topLevel = !parentItem;
            if ( topLevel ) {
                // This is a top-level tmpl call (not from a nested template using {{tmpl}})
                parentItem = topTmplItem;
                tmpl = jQuery.template[tmpl] || jQuery.template( null, tmpl );
                wrappedItems = {}; // Any wrapped items will be rebuilt, since this is top level
            } else if ( !tmpl ) {
                // The template item is already associated with DOM - this is a refresh.
                // Re-evaluate rendered template for the parentItem
                tmpl = parentItem.tmpl;
                newTmplItems[parentItem.key] = parentItem;
                parentItem.nodes = [];
                if ( parentItem.wrapped ) {
                    updateWrapped( parentItem, parentItem.wrapped );
                }
                // Rebuild, without creating a new template item
                return jQuery( build( parentItem, null, parentItem.tmpl( jQuery, parentItem ) ) );
            }
            if ( !tmpl ) {
                return []; // Could throw...
            }
            if ( typeof data === "function" ) {
                data = data.call( parentItem || {} );
            }
            if ( options && options.wrapped ) {
                updateWrapped( options, options.wrapped );
            }
            ret = jQuery.isArray( data ) ?
				jQuery.map( data, function ( dataItem ) {
				    return dataItem ? newTmplItem( options, parentItem, tmpl, dataItem ) : null;
				} ) :
				[newTmplItem( options, parentItem, tmpl, data )];
            return topLevel ? jQuery( build( parentItem, null, ret ) ) : ret;
        },

        // Return rendered template item for an element.
        tmplItem: function ( elem ) {
            var tmplItem;
            if ( elem instanceof jQuery ) {
                elem = elem[0];
            }
            while ( elem && elem.nodeType === 1 && !( tmplItem = jQuery.data( elem, "tmplItem" ) ) && ( elem = elem.parentNode ) ) { }
            return tmplItem || topTmplItem;
        },

        // Set:
        // Use $.template( name, tmpl ) to cache a named template,
        // where tmpl is a template string, a script element or a jQuery instance wrapping a script element, etc.
        // Use $( "selector" ).template( name ) to provide access by name to a script block template declaration.

        // Get:
        // Use $.template( name ) to access a cached template.
        // Also $( selectorToScriptBlock ).template(), or $.template( null, templateString )
        // will return the compiled template, without adding a name reference.
        // If templateString includes at least one HTML tag, $.template( templateString ) is equivalent
        // to $.template( null, templateString )
        template: function ( name, tmpl ) {
            if ( tmpl ) {
                // Compile template and associate with name
                if ( typeof tmpl === "string" ) {
                    // This is an HTML string being passed directly in.
                    tmpl = buildTmplFn( tmpl );
                } else if ( tmpl instanceof jQuery ) {
                    tmpl = tmpl[0] || {};
                }
                if ( tmpl.nodeType ) {
                    // If this is a template block, use cached copy, or generate tmpl function and cache.
                    tmpl = jQuery.data( tmpl, "tmpl" ) || jQuery.data( tmpl, "tmpl", buildTmplFn( tmpl.innerHTML ) );
                    // Issue: In IE, if the container element is not a script block, the innerHTML will remove quotes from attribute values whenever the value does not include white space.
                    // This means that foo="${x}" will not work if the value of x includes white space: foo="${x}" -> foo=value of x.
                    // To correct this, include space in tag: foo="${ x }" -> foo="value of x"
                }
                return typeof name === "string" ? ( jQuery.template[name] = tmpl ) : tmpl;
            }
            // Return named compiled template
            return name ? ( typeof name !== "string" ? jQuery.template( null, name ) :
				( jQuery.template[name] ||
            // If not in map, and not containing at least on HTML tag, treat as a selector.
            // (If integrated with core, use quickExpr.exec)
					jQuery.template( null, htmlExpr.test( name ) ? name : jQuery( name ) ) ) ) : null;
        },

        encode: function ( text ) {
            // Do HTML encoding replacing < > & and ' and " by corresponding entities.
            return ( "" + text ).split( "<" ).join( "&lt;" ).split( ">" ).join( "&gt;" ).split( '"' ).join( "&#34;" ).split( "'" ).join( "&#39;" );
        }
    } );

    jQuery.extend( jQuery.tmpl, {
        tag: {
            "tmpl": {
                _default: { $2: "null" },
                open: "if($notnull_1){__=__.concat($item.nest($1,$2));}"
                // tmpl target parameter can be of type function, so use $1, not $1a (so not auto detection of functions)
                // This means that {{tmpl foo}} treats foo as a template (which IS a function).
                // Explicit parens can be used if foo is a function that returns a template: {{tmpl foo()}}.
            },
            "wrap": {
                _default: { $2: "null" },
                open: "$item.calls(__,$1,$2);__=[];",
                close: "call=$item.calls();__=call._.concat($item.wrap(call,__));"
            },
            "each": {
                _default: { $2: "$index, $value" },
                open: "if($notnull_1){$.each($1a,function($2){with(this){",
                close: "}});}"
            },
            "if": {
                open: "if(($notnull_1) && $1a){",
                close: "}"
            },
            "else": {
                _default: { $1: "true" },
                open: "}else if(($notnull_1) && $1a){"
            },
            "html": {
                // Unecoded expression evaluation.
                open: "if($notnull_1){__.push($1a);}"
            },
            "=": {
                // Encoded expression evaluation. Abbreviated form is ${}.
                _default: { $1: "$data" },
                open: "if($notnull_1){__.push($.encode($1a));}"
            },
            "!": {
                // Comment tag. Skipped by parser
                open: ""
            }
        },

        // This stub can be overridden, e.g. in jquery.tmplPlus for providing rendered events
        complete: function ( items ) {
            newTmplItems = {};
        },

        // Call this from code which overrides domManip, or equivalent
        // Manage cloning/storing template items etc.
        afterManip: function afterManip( elem, fragClone, callback ) {
            // Provides cloned fragment ready for fixup prior to and after insertion into DOM
            var content = fragClone.nodeType === 11 ?
				jQuery.makeArray( fragClone.childNodes ) :
				fragClone.nodeType === 1 ? [fragClone] : [];

            // Return fragment to original caller (e.g. append) for DOM insertion
            callback.call( elem, fragClone );

            // Fragment has been inserted:- Add inserted nodes to tmplItem data structure. Replace inserted element annotations by jQuery.data.
            storeTmplItems( content );
            cloneIndex++;
        }
    } );

    //========================== Private helper functions, used by code above ==========================

    function build( tmplItem, nested, content ) {
        // Convert hierarchical content into flat string array
        // and finally return array of fragments ready for DOM insertion
        var frag, ret = content ? jQuery.map( content, function ( item ) {
            return ( typeof item === "string" ) ?
            // Insert template item annotations, to be converted to jQuery.data( "tmplItem" ) when elems are inserted into DOM.
				( tmplItem.key ? item.replace( /(<\w+)(?=[\s>])(?![^>]*_tmplitem)([^>]*)/g, "$1 " + tmplItmAtt + "=\"" + tmplItem.key + "\" $2" ) : item ) :
            // This is a child template item. Build nested template.
				build( item, tmplItem, item._ctnt );
        } ) :
        // If content is not defined, insert tmplItem directly. Not a template item. May be a string, or a string array, e.g. from {{html $item.html()}}.
		tmplItem;
        if ( nested ) {
            return ret;
        }

        // top-level template
        ret = ret.join( "" );

        // Support templates which have initial or final text nodes, or consist only of text
        // Also support HTML entities within the HTML markup.
        ret.replace( /^\s*([^<\s][^<]*)?(<[\w\W]+>)([^>]*[^>\s])?\s*$/, function ( all, before, middle, after ) {
            frag = jQuery( middle ).get();

            storeTmplItems( frag );
            if ( before ) {
                frag = unencode( before ).concat( frag );
            }
            if ( after ) {
                frag = frag.concat( unencode( after ) );
            }
        } );
        return frag ? frag : unencode( ret );
    }

    function unencode( text ) {
        // Use createElement, since createTextNode will not render HTML entities correctly
        var el = document.createElement( "div" );
        el.innerHTML = text;
        return jQuery.makeArray( el.childNodes );
    }

    // Generate a reusable function that will serve to render a template against data
    function buildTmplFn( markup ) {
        return new Function( "jQuery", "$item",
        // Use the variable __ to hold a string array while building the compiled template. (See https://github.com/jquery/jquery-tmpl/issues#issue/10).
			"var $=jQuery,call,__=[],$data=$item.data;" +

        // Introduce the data as local variables using with(){}
			"with($data){__.push('" +

        // Convert the template into pure JavaScript
			jQuery.trim( markup )
				.replace( /([\\'])/g, "\\$1" )
				.replace( /[\r\t\n]/g, " " )
				.replace( /\$\{([^\}]*)\}/g, "{{= $1}}" )
				.replace( /\{\{(\/?)(\w+|.)(?:\(((?:[^\}]|\}(?!\}))*?)?\))?(?:\s+(.*?)?)?(\(((?:[^\}]|\}(?!\}))*?)\))?\s*\}\}/g,
				function ( all, slash, type, fnargs, target, parens, args ) {
				    var tag = jQuery.tmpl.tag[type], def, expr, exprAutoFnDetect;
				    if ( !tag ) {
				        throw "Unknown template tag: " + type;
				    }
				    def = tag._default || [];
				    if ( parens && !/\w$/.test( target ) ) {
				        target += parens;
				        parens = "";
				    }
				    if ( target ) {
				        target = unescape( target );
				        args = args ? ( "," + unescape( args ) + ")" ) : ( parens ? ")" : "" );
				        // Support for target being things like a.toLowerCase();
				        // In that case don't call with template item as 'this' pointer. Just evaluate...
				        expr = parens ? ( target.indexOf( "." ) > -1 ? target + unescape( parens ) : ( "(" + target + ").call($item" + args ) ) : target;
				        exprAutoFnDetect = parens ? expr : "(typeof(" + target + ")==='function'?(" + target + ").call($item):(" + target + "))";
				    } else {
				        exprAutoFnDetect = expr = def.$1 || "null";
				    }
				    fnargs = unescape( fnargs );
				    return "');" +
						tag[slash ? "close" : "open"]
							.split( "$notnull_1" ).join( target ? "typeof(" + target + ")!=='undefined' && (" + target + ")!=null" : "true" )
							.split( "$1a" ).join( exprAutoFnDetect )
							.split( "$1" ).join( expr )
							.split( "$2" ).join( fnargs || def.$2 || "" ) +
						"__.push('";
				} ) +
			"');}return __;"
		);
    }
    function updateWrapped( options, wrapped ) {
        // Build the wrapped content.
        options._wrap = build( options, true,
        // Suport imperative scenario in which options.wrapped can be set to a selector or an HTML string.
			jQuery.isArray( wrapped ) ? wrapped : [htmlExpr.test( wrapped ) ? wrapped : jQuery( wrapped ).html()]
		).join( "" );
    }

    function unescape( args ) {
        return args ? args.replace( /\\'/g, "'" ).replace( /\\\\/g, "\\" ) : null;
    }
    function outerHtml( elem ) {
        var div = document.createElement( "div" );
        div.appendChild( elem.cloneNode( true ) );
        return div.innerHTML;
    }

    // Store template items in jQuery.data(), ensuring a unique tmplItem data data structure for each rendered template instance.
    function storeTmplItems( content ) {
        var keySuffix = "_" + cloneIndex, elem, elems, newClonedItems = {}, i, l, m;
        for ( i = 0, l = content.length; i < l; i++ ) {
            if ( ( elem = content[i] ).nodeType !== 1 ) {
                continue;
            }
            elems = elem.getElementsByTagName( "*" );
            for ( m = elems.length - 1; m >= 0; m-- ) {
                processItemKey( elems[m] );
            }
            processItemKey( elem );
        }
        function processItemKey( el ) {
            var pntKey, pntNode = el, pntItem, tmplItem, key;
            // Ensure that each rendered template inserted into the DOM has its own template item,
            if ( ( key = el.getAttribute( tmplItmAtt ) ) ) {
                while ( pntNode.parentNode && ( pntNode = pntNode.parentNode ).nodeType === 1 && !( pntKey = pntNode.getAttribute( tmplItmAtt ) ) ) { }
                if ( pntKey !== key ) {
                    // The next ancestor with a _tmplitem expando is on a different key than this one.
                    // So this is a top-level element within this template item
                    // Set pntNode to the key of the parentNode, or to 0 if pntNode.parentNode is null, or pntNode is a fragment.
                    pntNode = pntNode.parentNode ? ( pntNode.nodeType === 11 ? 0 : ( pntNode.getAttribute( tmplItmAtt ) || 0 ) ) : 0;
                    if ( !( tmplItem = newTmplItems[key] ) ) {
                        // The item is for wrapped content, and was copied from the temporary parent wrappedItem.
                        tmplItem = wrappedItems[key];
                        tmplItem = newTmplItem( tmplItem, newTmplItems[pntNode] || wrappedItems[pntNode] );
                        tmplItem.key = ++itemKey;
                        newTmplItems[itemKey] = tmplItem;
                    }
                    if ( cloneIndex ) {
                        cloneTmplItem( key );
                    }
                }
                el.removeAttribute( tmplItmAtt );
            } else if ( cloneIndex && ( tmplItem = jQuery.data( el, "tmplItem" ) ) ) {
                // This was a rendered element, cloned during append or appendTo etc.
                // TmplItem stored in jQuery data has already been cloned in cloneCopyEvent. We must replace it with a fresh cloned tmplItem.
                cloneTmplItem( tmplItem.key );
                newTmplItems[tmplItem.key] = tmplItem;
                pntNode = jQuery.data( el.parentNode, "tmplItem" );
                pntNode = pntNode ? pntNode.key : 0;
            }
            if ( tmplItem ) {
                pntItem = tmplItem;
                // Find the template item of the parent element.
                // (Using !=, not !==, since pntItem.key is number, and pntNode may be a string)
                while ( pntItem && pntItem.key != pntNode ) {
                    // Add this element as a top-level node for this rendered template item, as well as for any
                    // ancestor items between this item and the item of its parent element
                    pntItem.nodes.push( el );
                    pntItem = pntItem.parent;
                }
                // Delete content built during rendering - reduce API surface area and memory use, and avoid exposing of stale data after rendering...
                delete tmplItem._ctnt;
                delete tmplItem._wrap;
                // Store template item as jQuery data on the element
                jQuery.data( el, "tmplItem", tmplItem );
            }
            function cloneTmplItem( key ) {
                key = key + keySuffix;
                tmplItem = newClonedItems[key] =
					( newClonedItems[key] || newTmplItem( tmplItem, newTmplItems[tmplItem.parent.key + keySuffix] || tmplItem.parent ) );
            }
        }
    }

    //---- Helper functions for template item ----

    function tiCalls( content, tmpl, data, options ) {
        if ( !content ) {
            return stack.pop();
        }
        stack.push( { _: content, tmpl: tmpl, item: this, data: data, options: options } );
    }

    function tiNest( tmpl, data, options ) {
        // nested template, using {{tmpl}} tag
        return jQuery.tmpl( jQuery.template( tmpl ), data, options, this );
    }

    function tiWrap( call, wrapped ) {
        // nested template, using {{wrap}} tag
        var options = call.options || {};
        options.wrapped = wrapped;
        // Apply the template, which may incorporate wrapped content,
        return jQuery.tmpl( jQuery.template( call.tmpl ), call.data, options, call.item );
    }

    function tiHtml( filter, textOnly ) {
        var wrapped = this._wrap;
        return jQuery.map( 
			jQuery( jQuery.isArray( wrapped ) ? wrapped.join( "" ) : wrapped ).filter( filter || "*" ),
			function ( e ) {
			    return textOnly ?
					e.innerText || e.textContent :
					e.outerHTML || outerHtml( e );
			} );
    }

    function tiUpdate() {
        var coll = this.nodes;
        jQuery.tmpl( null, null, null, this ).insertBefore( coll[0] );
        jQuery( coll ).remove();
    }
} )( jQuery );






/*
* jQuery autoResize (textarea auto-resizer)
* @copyright James Padolsey http://james.padolsey.com
* @version 1.04
*/
(function ( a ) { a.fn.autoResize = function ( j ) { var b = a.extend( { onResize: function () { }, animate: true, animateDuration: 150, animateCallback: function () { }, extraSpace: 20, limit: 1000 }, j ); this.filter( 'textarea' ).each( function () { var c = a( this ).css( { resize: 'none', 'overflow-y': 'hidden' } ), k = c.height(), f = ( function () { var l = ['height', 'width', 'lineHeight', 'textDecoration', 'letterSpacing'], h = {}; a.each( l, function ( d, e ) { h[e] = c.css( e ) } ); return c.clone().removeAttr( 'id' ).removeAttr( 'name' ).css( { position: 'absolute', top: 0, left: -9999 } ).css( h ).attr( 'tabIndex', '-1' ).insertBefore( c ) } )(), i = null, g = function () { f.height( 0 ).val( a( this ).val() ).scrollTop( 10000 ); var d = Math.max( f.scrollTop(), k ) + b.extraSpace, e = a( this ).add( f ); if ( i === d ) { return } i = d; if ( d >= b.limit ) { a( this ).css( 'overflow-y', '' ); return } b.onResize.call( this ); b.animate && c.css( 'display' ) === 'block' ? e.stop().animate( { height: d }, b.animateDuration, b.animateCallback ) : e.height( d ) }; c.unbind( '.dynSiz' ).bind( 'keyup.dynSiz', g ).bind( 'keydown.dynSiz', g ).bind( 'change.dynSiz', g ) } ); return this } } )( jQuery );

/*
* jQuery vhm feedback
* @copyright Aleg Yelshov, Edgar A.
* @version 0.3.0
*
* Ex.:
* html
<div id="divFeedback"></div> 
* js
$( '#divFeedback' ).vhmFeedback ( { 
fid: 1, - feature id (challenge id/promo id/etc.), required param on init,
fc: 'SECH' - feature category code (SECH - self-entry challenge, BDGE - badge, PROM - promo, CHLG - challenge, RTSK - reward task, OTHE - external feature), required param on init,
[, type: 'summary'] - feedback control type, values - "like", "disabledlike", "rating", "yourrating", "summary", "summarypopup". default - summary.
[, avgRatingLabel: 'Average rating:'] - feedback control text for average rating, default - 'Average rating:'.
[, yourRatingLabel: 'Your rating:'] - feedback control text for your rating, default - 'Your rating:'.
} );

*       control static variables:
*       web service url for SaveMemberFeedback and GetMemberFeedback methods.
$.fn.vhmFeedback.wsurl = "../../webservices/Feedback.asmx";
*       Summary rating edit pop up title
$.fn.vhmFeedback.popupTitle = "We want to know what you think";
*
*/
(function ( $ ) {

    // Feedback object to call vhmFeedback plugin methods (not implemented)
    // Ex: $(<selector>).vhmFeedback('edit');
    function Feedback() {
        this._param = false;
    };

    $.extend( Feedback.prototype, {
        edit: function ( target ) {
        }
    } );

    var _defaults = {
        type: 'summary',
        avgRatingLabel: "Average rating:",
        yourRatingLabel: "Your rating:"
    }

    $.fn.vhmFeedback = function ( options ) {
        if ( !this.length ) {
            return this;
        }

        var otherArgs = Array.prototype.slice.call( arguments, 1 );

        return this.each( function () {
            if ( $( this ).data( "feedback" ) == null ) {

                if ( isNaN( options.fid ) ) {
                    throw "parameter fid (feature Id) must be specified";
                }

                if ( typeof options.fc == "undefined" || options.fc === "" ) {
                    throw "parameter fc (feature category code) must be specified";
                }

                $( this ).data( "feedback", _defaults );
            }

            if ( typeof options == 'string' ) {
                Feedback[options].apply( this, $.makeArray( arguments ).slice( 1 ) || [] );
            } else if ( $.isPlainObject( options ) ) {
                $( this ).data( "feedback", $.extend( {}, $( this ).data( "feedback" ), options ) );

                var feedback = $( this ).data( "feedback" ),
                fid = feedback.fid,
                fc = feedback.fc,
                ffid = getFeatureFeedbackId( fid, fc );
                build( this );
                queueGetMemberFeedback( fid, fc, setFeedback, $( this ) );
            }
        } );
    };

    $.fn.vhmFeedback.wsurl = "../../webservices/Feedback.asmx";
    $.fn.vhmFeedback.popupTitle = "We want to know what you think";

    var stars = [0, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5],
    memberFeedbackCallQueue = {},
    feedbackData = {},
    queueIds = [],
    feedbacks=[]
    getQueueId = function ( fid, fc ) {
        for ( var i = 0, l = queueIds.length; i < l; i++ ) {
            if ( queueIds[i].fid === fid && queueIds[i].fc === fc ) {
                return ++i;
            }
        }
        return -1;
    },
    getFeatureFeedbackId = function ( fid, fc ) {
        for ( var f in feedbackData ) {
            if ( f && feedbackData[f].FeatureCategoryCode === fc && feedbackData[f].FeatureId === fid ) {
                return feedbackData[f].FeatureFeedbackId;
            }
        }
        return null;
    },
queueGetMemberFeedback = function ( fid, fc, callback, $o ){
    if(feedbacks[fid]!=null || feedbacks[fid]!=undefined)
    {
        var ffid=getFeatureFeedbackId(fid,fc);
        if(feedbackData[ffid]!=null && feedbackData[ffid]!=undefined)
        {
            $o.data("feedback",$.extend( {},  $o.data( "feedback" ), feedbackData[ffid]));
            feedbacks[fid][feedbacks[fid].length]=$o;
            setFeedback($o);
         }
         else{
            feedbacks[fid][feedbacks[fid].length]=$o;
         }   
    }
    else
    {
        feedbacks[fid]=[];
        feedbacks[fid][0]=$o;
        callGetMemberFeedback(fid,fc,$o)
    }

},
callGetFeedbackCallback = function ( fid, fc, $o,data )
{

 if ( null != data && !isNaN( data.FeatureFeedbackId ) ) {
            feedbackData[data.FeatureFeedbackId] = data;
        }

    if(feedbacks[fid]!=null || feedbacka[fid]!=undefined)
    {
        for( var i=0; i<feedbacks[fid].length;i++)
        {
        if(feedbacks[fid][i]!=null || feedbacka[fid][i]!=undefined)
            feedbacks[fid][i].data("feedback",$.extend( {}, feedbacks[fid][i].data( "feedback" ), data));
            setFeedback(feedbacks[fid][i]);
        }
    }
   
},
    callSaveMemberFeedbackCallback = function ( ffid, data ) {
        feedbackData[ffid] = data;
    },
    callGetMemberFeedback = function ( fid, fc,$o ) {
        $.vhmAjax( {
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: $.fn.vhmFeedback.wsurl + "/GetMemberFeedback",
            data: "{'featureId': '" + fid + "', 'featureCategoryCode': '" + fc + "'}",
            dataType: "json",
            success: function ( data, textStatus, jqXHR ) { callGetFeedbackCallback( fid, fc, $o, data.d, textStatus, jqXHR ); },
            error: function ( xhr, ajaxOptions, error ) { alert( error ); }
        } );
    },
    callSaveMemberFeedback = function ( ffid ) {
        $.vhmAjax( {
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: $.fn.vhmFeedback.wsurl + "/SaveMemberFeedback",
            data: "{'feedback': " + JSON.stringify( feedbackData[ffid] ) + "}",
            dataType: "json",
            success: function ( data, textStatus, jqXHR ) { callSaveMemberFeedbackCallback( ffid, data.d, textStatus, jqXHR ); },
            error: function ( xhr, ajaxOptions, error ) { alert( error ); }
        } );
    },
    getAvgRating = function ( fid ) {
        return parseInt( ( feedbackData[fid] && feedbackData[fid].AvgRating ) || 0 );
    },
    getRating = function ( fid ) {
        return parseInt( ( feedbackData[fid] && feedbackData[fid].Rating ) || 0 );
    },
    getNumLikes = function ( fid ) {
        return parseInt( ( feedbackData[fid] && feedbackData[fid].TotalLikes ) || 0 );
    },
    getComment = function ( fid ) {
        if ( feedbackData[fid] != null && feedbackData[fid].Comment != null )
            return feedbackData[fid].Comment;
        return "";
    },
    getFeedback = function ( $o ) {
        return $o.data( 'feedback' ) || {};
    },
    setFeedback = function ( $o ) {
        var fb = $o.data( "feedback" ),
        ffid = getFeatureFeedbackId( fb.fid, fb.fc );

        switch ( fb.type.toLowerCase() ) {
            case "like":
            case "disabledlike":
                setLike( $o, ffid );
                break;
             case "fullrating":
                setFullRating( $o, ffid );
                break;
            case "rating":
                setAvgRating( $o, ffid );
                break;
            case "yourrating":
                setYourRating( $o, ffid );
                break;
            case "summarypopup":
            case "summary":
            default:
                setSummary( $o, ffid );
                break;
        }
    },
    setLike = function ( $o, ffid ) {
        if( ffid != null ){
            if ( feedbackData[ffid].IsUserLikes === true ) {
                $o.find( '.fdb-likelbl span' ).text( 'Unlike' );
            } else {
                $o.find( '.fdb-likelbl span' ).text( 'Like' );
            }
            buildNumLikes( $o, getNumLikes( ffid ) );
        }
    },
    setRating = function ( $o, r ) {
        var lis = $o.find( 'li' );

        for ( var i = 0, l = lis.length; i < l; i++ ) {
            if ( i < Math.ceil( r ) ) {
                $( lis[i] ).addClass( 'cover' );
            } else {
                $( lis[i] ).removeClass( 'cover' );
            }
        }
    },
    setAvgRating = function ( $o, ffid ) {
        var r = stars[getAvgRating( ffid )];
        setRating( $o, r );

        if ( Math.round( r * 2 ) % 2 === 1 ) {
            $( $o.find( 'li' )[parseInt( r )] ).html( "<div class='half-star'></div>" );
        }
    },
    setYourRating = function ( $o, ffid ) {
        var r = stars[getRating( ffid )];
        setRating( $o, r );
    },
    setSummary = function ( $o, ffid ) {
        var numLikes = buildNumLikes( $o, getNumLikes( ffid ) );
        $o.find( ".fdb-totalLikes" ).remove();
        $( numLikes ).appendTo( $o.find( '.fdb-likelbl' ) );
        setAvgRating( $o, ffid );
    }
    setFullRating= function( $o, ffid ){
        $o.find( '.fdb-commentInputBox' ).val( getComment( ffid ) ); 
    }
    var build = function ( dom ) {
        var $o = $( dom ),
        prm = $o.data( "feedback" );
        switch ( $o.data( "feedback" ).type.toLowerCase() ) {
            case "like":
                buildLike( $o );
                break;
            case "disabledlike":
                buildDisabledLike( $o );
                break;
            case "rating":
                buildAvgRating( $o );
                break;
            case "yourrating":
                buildYourRating( $o );
                break;
           case "fullrating":
                buildFullRating( $o, prm );
                break;
            case "summarypopup":
                buildSummaryPopup( $o, prm );
                break;
            case "summary":
            default:
                buildSummary( $o );
                break;
        }
    },
    buildNumLikes = function ( $o, n ) {
        var fb = $o.data( "feedback" ),
        ffid = getFeatureFeedbackId( fb.fid, fb.fc ),
        totalLikes = getNumLikes( ffid ),
        addNumLikes = function ( n ) {
            var s = "<div class='fdb-totalLikes'>(";
            if ( n == 0 ) {
                s += "be the first";
                return s+")</div>"
            } else if ( n == 1 ) {
                s += "1 likes";
            } else {
                s += n + " like";
            }
            return s + " this)</div>";
        }

        $o.find( '.fdb-totalLikes' ).remove().end().append( addNumLikes( totalLikes ) );

        return addNumLikes( totalLikes );
    },
    buildLike = function ( $o, isDis ) {
        var s, a = ["", "like-inactive.gif", ""];
        if ( isDis !== true ) {
            a = ["<a href='#'>", "icon-likes.gif", "</a>"];
        }
        s = "<div class='fdb-likelbl'>" + a[0] + "<image src='../../images/feedback/" + a[1] + "'/><span>Like</span>" + a[2] + "</div>";
        $o.html( s );

        if ( isDis !== true ) {
            attachEvents( $o );
        }
    },
    buildDisabledLike = function ( $o ) {
        buildLike( $o, true );
    },
    buildRatingHtml = function ( cssClass, lbl ) {
        var s = "<span class='fdb-ratinglbl'>" + lbl + "</span><div>";
        s += "<ul class='" + cssClass + "'>";
        for ( var i = 0, l = stars.length / 2; i < l; i++ ) {
            s += "<li></li>";
        }
        s += "</ul></div>";

        return s;
    },
    buildAvgRating = function ( $o ) {
        var arLbl = getFeedback( $o ).avgRatingLabel,
        s = buildRatingHtml( 'fdb-rating', arLbl );
        $o.html( s );
    },
    buildYourRating = function ( $o ) {
        var yrLbl = getFeedback( $o ).yourRatingLabel,
        s = buildRatingHtml( 'fdb-rating-bl', yrLbl );
        $o.html( s );
        attachEvents( $o );
    },
    buildSummaryPopup = function ( $o, m ) {
        buildSummary( $o );
        $o.find( 'div.fdb-summary' ).addClass( 'fdb-summarypopup' );
        attachEvents( $o );
        
    },
    buildFullRating=function ($o){
        fb = $o.data( "feedback" );
            fid = fb.fid;
            fc = fb.fc;
           var divCommet = "<div class='fdb-sumpopcomment'><span class='fdb-comment'>Comments:</span>" +
                "<textarea class='fdb-commentInputBox' rows='2' cols='60'></textarea></div>"+
                "<input type='button' class='vbtn_red_a_sm fdb-doneButton post' style='display:none;' value='Done' /></div>";
         var html="<div class='fdb-sumpoplikerating'><div class='fdb-sumpoplike' />" +
                "<div class='fdb-sumpopyourRating' />" + divCommet + "</div>" ;
                $o.html(html);
         $o.find( 'div.fdb-sumpopyourRating' ).vhmFeedback( { fid: fid, fc: fc, type: 'YourRating' } ).end()
         $o.find( 'div.fdb-sumpoplike' ).vhmFeedback( { fid: fid, fc: fc, type: 'Like' } );
         $o.find( ".fdb-doneButton" ).click( function ( e ) {
            ffid = getFeatureFeedbackId( fid, fc );
                feedbackData[ ffid ].Comment = $o.find( ".fdb-commentInputBox" ).val();
                callSaveMemberFeedback(ffid);
                });
         attachEvents($o);
    }
    buildSummary = function ( $o ) {
        var s = "<div class='fdb-summary'><div class='fdb-likelbl'><image src='../../images/social/icon-likes.gif' /></div>";
        s += "<div>";
        s += "<ul class='fdb-rating'>";
        for ( var i = 0, l = stars.length / 2; i < l; i++ ) {
            s += "<li></li>";
        }
        s += "</ul></div></div>";
        $o.html( s );
    }
    var attachEvents = function ( $o ) {
        $o.find( '.fdb-likelbl a' ).unbind( 'click' ).click( function ( e ) {

            var $o = $( e.target ).parents( 'div.fdb-likelbl' ).parent(),
            fb = $o.data( "feedback" ),
            ffid = getFeatureFeedbackId( fb.fid, fb.fc );

            if ( null != ffid ) {
                feedbackData[ffid].IsUserLikes = !feedbackData[ffid].IsUserLikes;
                feedbackData[ffid].IsUserLikes ? ++feedbackData[ffid].TotalLikes : --feedbackData[ffid].TotalLikes;
                callSaveMemberFeedback( ffid );
                setLike( $o, ffid );
            }

            return false;
        } );

        $o.find( 'ul.fdb-rating-bl' )
        .unbind().click( function ( e ) {
            var r = $( e.target ).prevAll().length + 1;
            r = r * 2 - 1;
            var $o = $( e.target ).parents( 'ul.fdb-rating-bl' ).parent().parent(),
            fb = $o.data( "feedback" ),
            ffid = getFeatureFeedbackId( fb.fid, fb.fc );

            if ( null != ffid ) {
                feedbackData[ffid].Rating = r;
                callSaveMemberFeedback( ffid );
                setYourRating( $( e.target ).parent( 'ul' ), ffid );
            }
        } )
        .undelegate().delegate( 'li', 'mouseenter', function ( e ) {

            $( this ).addClass( 'cover' ).prevAll().empty().addClass( 'cover' ).end()
                .nextAll().removeClass( 'cover' );
        } )
        .delegate( 'li', 'mouseleave', function () {

            var ul = $( this ).parent();
            ul.find( 'li' ).removeClass( 'cover' );

            var $o = ul.parent().parent(),
            fb = $o.data( "feedback" ),
            ffid = getFeatureFeedbackId( fb.fid, fb.fc );

            setYourRating( ul, ffid );
        } );

        $o.find( ".fdb-summarypopup" ).unbind( 'click' ).click( function ( e ) {
            var $t = $( e.target ),
            $o = $t.hasClass( 'fdb-summarypopup' ) ? $t.parent() : $t.parents( 'div.fdb-summarypopup' ).parent(),
            fb = $o.data( "feedback" ),
            fid = fb.fid,
            fc = fb.fc,
            ffid = getFeatureFeedbackId( fid, fc ),
            $spdlg = $( '.fdb-summaryPopupDlg' );

            if ( $spdlg.length ) {
                var dlgItm;
                for ( var i = 0, l = $spdlg.length; i < l; i++ ) {
                    dlgItm = $( $spdlg[i] );
                    if ( parseInt( dlgItm.attr( 'ffid' ) ) === ffid ) {
                        dlgItm.dialog( 'open' );
                        return;
                    }
                }
            }

            var divCommet = "<div class='fdb-sumpopcomment'>"+
                "<span class='fdb-comment'>Comments:</span>" +
                "<textarea class='fdb-commentInputBox' rows='2' cols='60'></textarea>" +
                "<input type='button' class='vbtn_red_a_sm fdb-doneButton post' value='Done' /></div>";


               $( "<div class='fdb-summaryPopupDlg' >"+
               "<table cellpadding='0' id='jqmPopUpTableContaner' cellspacing='0' border='0'>"+
                '<tr class="tblVHMTrow"><td class="tblVHMTcellL"></td><td class="tblVHMTcellC"></td>'+
                '<td class="tblVHMTcellR"><div class="jqmClose closeButton"></div></td></tr><tr class="tblVHMCrow">'+
                '<td class="tblVHMCcellL"></td><td class="tblVHMCcellC">'+
               "<span class='fdb-summaryPopupTitle'>" + $.fn.vhmFeedback.popupTitle + "</span>" +
                "<div class='fdb-sumpoplikerating'/><div class='fdb-sumpoplike' />" +
                "<div class='fdb-sumpopyourRating' /></div>" + divCommet + 
                '</td><td class="tblVHMCcellR"></td>'+
                '</tr><tr  class="tblVHMBrow"><td class="tblVHMBcellL"></td><td class="tblVHMBcellC"></td><td class="tblVHMBcellR"></td></tr></table>'+
                "</div>" )
                .appendTo( $o );

            $o.find( '.fdb-commentInputBox' ).val( getComment( ffid ) );
            $o.find( '.fdb-sumpopyourRating' ).vhmFeedback( { fid: fid, fc: fc, type: 'YourRating' } );
            $o.find( '.fdb-sumpoplike' ).vhmFeedback( { fid: fid, fc: fc, type: 'Like' } );
            $o.find( '.fdb-commentInputBox' ).removeAttr( 'disabled' ).bind( 'focus', function ( e ) {
                $( e.target ).parent().find( '.fdb-commentInputBox' ).addClass( 'fdb-commentDflt' );
            } );
            
            $o.find( '.fdb-commentInputBox' ).removeAttr( 'disabled' ).bind( 'blur', function ( e ) {
                $( e.target ).parent().find( '.fdb-commentInputBox' ).removeClass( 'fdb-commentDflt' );
            } );

             $o.find( '.closeButton' ).click(function (e){
                var dlg = $( e.target ).parents( ".fdb-summaryPopupDlg" );
                $( dlg ).dialog( 'close' );
             }
             );

            $o.find( ".fdb-doneButton" ).click( function ( e ) {
                var dlg = $( e.target ).parents( ".fdb-summaryPopupDlg" );
                if($( dlg ).find( ".fdb-commentInputBox" ).val().length>500){
                    alert("Ops, your comment can only have up to 500 characters")
                    return;
                }
                feedbackData[ffid].Comment = $( dlg ).find( ".fdb-commentInputBox" ).val();
                callSaveMemberFeedback( ffid );
                $( dlg ).dialog( 'close' );

            } );

             $o.find( 'div.fdb-summaryPopupDlg' ).attr( 'ffid', ffid ).dialog( {
                    modal: true
                    , resizable: false
                    , 'height': 300
                    , 'width': 450
                    , 'beforeClose': function () { }
            } ).parent().addClass( 'fdb-sumpopbd' ).find( '.ui-dialog-titlebar' ).remove().end().
            find( '.fdb-commentInputBox' ).focus();
        } );
    }
} )( jQuery );



$(function () {
    var caelmnts = $("div[vhm_caguid]");

    var loadca = function (elmnt) {
        var $e = $(elmnt);
        $.get("ContentAreaHandler.ashx", { vhm_caguid: $e.attr("vhm_caguid") }, function (data) {
            if (/<title>virgin healthmiles - a problem occured<\/title>/im.test(data)) {
                $e.html("");
            } else {
                $e.html(data);
            }
        });
    };

    var l = caelmnts.length;

    while (l--)
        loadca(caelmnts[l]);

    if (typeof jQuery != 'undefined') {
        window.nativeAlert = window.alert;

        window.alert = function (text, options, forceNative) {
            text = text || "";
            if (typeof forceNative == 'undefined' && !forceNative) {
                var alertDiv = $('#VHMCommonAlertDialog');
                options = $.extend({
                    bgiframe: true,
                    draggable: false,
                    autoOpen: false,
                    position: ['100px', '50%'],
                    closeOnEscape: true,
                    modal: true,
                    width: 320,
                    zIndex: 10000,
                    open: function (event, ui) {
                        var closeTitle = $(".ui-dialog-titlebar-close"); closeTitle.unbind('mouseenter mouseleave');
                        $(".ui-dialog-title").addClass("red");
                    },
                    buttons: {
                        OK: {
                            text: "OK", 'class': 'vbtn_red_a_md',
                            click: function () {
                                $(this).dialog('close');
                            }
                        }
                    },
                    title: 'Missing Information',
                    resizable: false,
                    beforeClose: function (event, ui) {
                        $(this).find('#VHMCommonAlertDialog_msg').html('');
                    }
                }, options);
                alertDiv = (alertDiv.length) ? alertDiv : $('<div>').attr('id', 'VHMCommonAlertDialog').appendTo('body').dialog(options);
                alertDiv.html('<h3>' + ((options.redMessageText) ? options.redMessageText : 'Oops!') + '</h3><div id="VHMCommonAlertDialog_msg">' + text.toString().replace(/\n/gi, "<br>") + '</div>');
                alertDiv.dialog('option', 'title', options.title).dialog('open').find('.alertDiv').hide().end().find('.ui-dialog-titlebar').css({ color: "#990000" });
            } else {
                window.nativeAlert(text);
            }
        };

        $.extend(window, {
            vhmconfirm: function (text, callback, data, options) {
                var confirmDiv = $('#VHMCommonConfirmDialog');

                options = $.extend({
                    bgiframe: true,
                    draggable: false,
                    autoOpen: false,
                    closeOnEscape: true,
                    modal: true,
                    width: 320,
                    position: ['100px', '50%'],
                    title: 'Confirm',
                    resizable: false,
                    beforeClose: function (event, ui) {
                        $("#tdCancel").attr('class', 'lnkSilverMedium');
                        $(this).find('#VHMCommonConfirmDialog_msg').html('');
                    },
                    open: function (event, ui) {
                        var $dlg = $(this);
                        $dlg.find("[vlc_name='btnYes']").click(function () {
                            $dlg.dialog('close');

                            if (typeof (callback) == "function")
                                callback(true, data);
                        }).css({ float: "right" });

                        $dlg.find("[vlc_name='btnNo']").click(function () {
                            $dlg.dialog('close');
                        }).css({ float: "right" });
                    }
                }, options);

                confirmDiv = (confirmDiv.length) ? confirmDiv : $('<div>').attr('id', 'VHMCommonConfirmDialog').appendTo('body').dialog(options);
                confirmDiv.html('<table cellpadding="5" cellspacing="0" border="0" width="97%"><tbody><tr><td id="VHMCommonConfirmDialog_msg" align="left">'
                    + text.toString().replace(/\n/gi, "<br>")
                    + '</td></tr>'
                    + '<tr><td colspan="2"><input  vlc_name="btnYes" class= "vbtn_red_a_sm" type="button" value="Yes"/><input vlc_name="btnNo" class= "vbtn_gray_a_sm" type="button" style="margin-right:10px;"  value="No"/></td></tr></tbody></table>');

                $(".ui-dialog-titlebar-close").attr('onclick', 'retCancelm();');
                confirmDiv.dialog({ title: options.title, dialogClass: 'dialog_red_width_brd  dialog_confirm_no_title dialog_no_close' }).dialog('open');
                return false;
            }
        });
    }
});