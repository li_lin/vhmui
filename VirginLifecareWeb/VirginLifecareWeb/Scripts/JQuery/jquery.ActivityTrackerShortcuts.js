﻿/*
* Plugin name :jquery.ActivityTrackerShortcuts
* Created on : 2012-02-15
* Description : Office of the slider keyboard tab + up/down and tab + left/right
* Depends: jquery.shortcuts (http://www.stepanreznikov.com/js-shortcuts), jquery.ActivityTracker
OPTIONS:
marginTop:    80
marginBottom: 50
*/
(function ( $ ) {

    $.fn.ActivityTrackerShortcuts = function ( options ) {

        var settings = {
            'marginTop': 80,
            'marginBottom': 50
        };

        var isDownTab = false;
        var activeTracker = -1;
        var masTrackers = [];

        if ( options ) {
            $.extend( settings, options );
        }

        function scrollToElement( theElement ) {
            var top = $( window ).scrollTop();
            var bottom = top + $( window ).height();
            var eltop = $( theElement ).offset().top;

            if ( eltop - settings.marginTop < top || eltop + settings.marginBottom > bottom ) 
                $( window ).scrollTop( eltop - settings.marginTop );
        }

        function selectTracker( trackerIndx ) {
            var id = masTrackers[trackerIndx];
            $.ActivityTracker.items[id].select();
            scrollToElement( "#" + id );
        }

        function unSelectTracker( trackerIndx ) {
            if ( trackerIndx >= 0 ) {
                var id = masTrackers[trackerIndx];
                $.ActivityTracker.items[id].unSelect();
            }
        }

        $.Shortcuts.add( {
            type: 'up',
            mask: 'Tab',
            handler: function () {
                isDownTab = false;
            }
        } );

        $.Shortcuts.add( {
            type: 'hold',
            mask: 'Tab',
            handler: function () {
                isDownTab = true;
            }
        } );

        $.Shortcuts.add( {
            type: 'hold',
            mask: 'Up',
            handler: function () {
                if ( isDownTab && masTrackers.length > 0 ) {
                    unSelectTracker( activeTracker );
                    if ( activeTracker - 1 < 0 ) activeTracker = masTrackers.length - 1;
                    else activeTracker--;
                    selectTracker( activeTracker );
                }
            }
        } );


        $.Shortcuts.add( {
            type: 'hold',
            mask: 'Down',
            handler: function () {
                if ( isDownTab && masTrackers.length > 0 ) {
                    unSelectTracker( activeTracker );
                    if ( activeTracker + 1 >= masTrackers.length ) activeTracker = 0;
                    else activeTracker++;
                    selectTracker( activeTracker );
                }
            }
        } );

        $.Shortcuts.add( {
            type: 'hold',
            mask: 'Left',
            handler: function () {
                if ( isDownTab && activeTracker >= 0 ) {
                    var id = masTrackers[activeTracker];
                    $.ActivityTracker.items[id].moveToLeft();
                }
            }
        } );

        $.Shortcuts.add( {
            type: 'hold',
            mask: 'Right',
            handler: function () {
                if ( isDownTab && activeTracker >= 0) {
                    var id = masTrackers[activeTracker];
                    $.ActivityTracker.items[id].moveToRight();
                }
            }
        } );


        $.Shortcuts.start();

        return this.each( function () {
            masTrackers.push( $( this ).attr( "id" ) );
        } );
    };
} )( jQuery );