﻿var VHM = VHM || {};
VHM.Badges = VHM.Badges || {}; //Badges -->Badges object - general through the site

VHM.Badges.SessionID;
VHM.Badges.ServiceUrl = "/WebServices/WSVHMBadges.asmx";
VHM.Badges.appPath = "";


VHM.Badges.GetRockStarWinner = function (sID, dialog) {
    var f_success = function (responseData, textStatus) {
        var rockStarObject = (typeof (responseData) != "object") ? eval("(" + responseData + ")") : responseData;
        var cookieRSVal = $.jStorage.get('LastRockStar');
        if (rockStarObject.sponsorHasRSEnabled) {
            $(".divRockStarHolder").css('display', 'block');
            dialog.show();
            var imgProf = dialog.find("[vlc_name='WinnerImgProf']");
            imgProf.attr("src", rockStarObject.WinnerProfilePicturePath);
            imgProf.attr("alt", rockStarObject.WinnerName);

            var badgeImg = dialog.find("[vlc_name='imgRSBadge']");
            badgeImg.attr("src", rockStarObject.BadgeImageUrl);
            badgeImg.click(function () {
                VHM.Badges.GetBadgeInfo(rockStarObject.BadgeID, 'divBagdeDetails');
            });
            badgeImg.hover(
                function () {
                    VHM.Badges.ShowBadgeName(rockStarObject.BadgeName, 'divBadgeHover', this);
                },
                function () {
                    VHM.Badges.HideBadgeName('divBadgeHover');
                }
            );

            var usrName = dialog.find("[vlc_name='divWinnerName']");
            usrName.html(rockStarObject.WinnerName);

            $('.imgWinnerUserProf').click(function () {
                VHM.SocialFrnds.GetCFUserProfData(rockStarObject.WinnerCFUserID, 'divNfMemProfile');
            });

            var RSBadgeName = dialog.find("[vlc_name='divBadgeName']");
            RSBadgeName.html(rockStarObject.BadgeName);
            var RSBadgeDesc = dialog.find("[vlc_name='divBadgeDescription']");

            //change the cookie value for the case when the requested badge was NOT enabled by Sponsor, so next one was shown.
            cookieRSVal = rockStarObject.badgeRemeberCache;
            if (cookieRSVal === 'step') {
                RSBadgeDesc.html("with the most steps last week");
            } else if (cookieRSVal === 'activity') {
                RSBadgeDesc.html("with the most active minutes last week");
            } else {
                RSBadgeDesc.html("with the highest step improvement last week");
            };
        }
        else {
            $(".divRockStarHolder").css('display', 'none');
            dialog.hide();
        }

        //make sure we save next Rock Star to cookie
        //pass in step, activity, improvement  
        if (cookieRSVal === 'step') {
            $.jStorage.set("LastRockStar", 'activity');
        } else if (cookieRSVal === 'activity') {
            $.jStorage.set("LastRockStar", 'rally');
        } else {
            $.jStorage.set("LastRockStar", 'step');
        }

    }
    //we can have 3 Rock Stars awarded each week. We'll store:
    // active - for Activity Rock Star; improved - for Improvement Rock Star; step - for Steps Rock star
    //pass in step, active, improved 
    // Check if "key" exists in the storage
    var value = $.jStorage.get('LastRockStar');
    if (!value) {
        // if not - set value to first visible rock star: activity
        value = 'step';
        // and save it
        $.jStorage.set("LastRockStar", value);
    }
    var appPath = "";
    var pl = new SOAPClientParameters();
    pl.add("rockStarName", value);
    pl.add("sId", sID);
    pl.add("appPath", appPath);
    SOAPClient.invoke(VHM.Badges.ServiceUrl, "GetWeeklyRockStarWinner", pl, false, f_success);
};
VHM.Badges.GetMoreFrWithBadge = function (nBadgeID, listHolderID) {
    var f_success = function (responseData, textStatus) { //allMembersWithBadge
        var membersMarkup = "<li class=\"liMembWithBadge\"><div class=\"profPicFLoatLft\" ><a href=\"#\" class=\"openCFProf_FWB UserFullName\" onclick=\"javascript:VHM.SocialFrnds.GetCFUserProfData('${FriendID}', 'divCF_FWB_Prof');return false;\" ><img alt=\"${MemberName}\" class=\"smallProfImg\" src=\"${ProfImage}\" onclick=\"javascript:VHM.SocialFrnds.GetCFUserProfData('${FriendID}', 'divCF_FWB_Prof');return false;\" /></a></div><div style=\"float:left;padding-top:10px;\"><a href=\"#\" class=\"openCFProf_FWB UserFullName\" onclick=\"javascript:VHM.SocialFrnds.GetCFUserProfData('${FriendID}', 'divCF_FWB_Prof');return false;\" >${MemberName}</a></div></li>";

        $("#" + listHolderID).html("");
        var frndsWithBadge = eval(responseData);
        $.template("frndWithBadgeTempl", membersMarkup);
        $.tmpl("frndWithBadgeTempl", frndsWithBadge).appendTo("#" + listHolderID);
        $("#divCF_FWB_Prof").jqm({
            trigger: '.openCFProf_FWB',
            modal: true,
            onShow: function (h) {
                h.w.css({ 'height': 'auto', 'width': '480px', 'z-index': '4000' }).slideDown(
                //to avoid orange borders in Chrome
                    function () { $("input:button:visible:last").focus().blur(); }
                );
                h.o.css("z-index", "3999");
            },
            onHide: function (h) {
                h.w.slideUp("normal", function () { if (h.o) h.o.remove(); });
            }
        });
    };

    var pl = new SOAPClientParameters();
    pl.add("nBadgeID", nBadgeID);
    if (typeof VHM.Badges.SessionID === "undefined") {
        VHM.Badges.SessionID = $("#hdnWp1").val();
    }
    pl.add("sId", VHM.Badges.SessionID);
    pl.add("appPath", VHM.Badges.appPath);
    SOAPClient.invoke(VHM.Badges.ServiceUrl, "GetFriendsWithBadge", pl, false, f_success);
};
//use this if we already have the object holding the friends and no need to make an ajax call to get that
VHM.Badges.getFriendsWithBadge = function (responseData) {
    var badgesMarkup = "<li class=\"liMembWithBadge\"><div class=\"profPicFLoatLft\" ><a href=\"#\" class=\"openCFProf_FWB UserFullName\" onclick=\"javascript:VHM.SocialFrnds.GetCFUserProfData('${FriendID}', 'divCF_FWB_Prof');return false;\" ><img alt=\"${MemberName}\" class=\"smallProfImg\" src=\"${ProfImage}\" onclick=\"javascript:VHM.SocialFrnds.GetCFUserProfData('${FriendID}', 'divCF_FWB_Prof');return false;\" /></a></div><div style=\"float:left;padding-top:10px;\"><a href=\"#\" class=\"openCFProf_FWB UserFullName\" onclick=\"javascript:VHM.SocialFrnds.GetCFUserProfData('${FriendID}', 'divCF_FWB_Prof');return false;\" >${MemberName}</a></div></li>";
    $("#allMembersWithBadge").html("");
    var frndsWithBadge = eval(responseData);
    $.template("frndWithBadgeTempl", badgesMarkup);
    $.tmpl("frndWithBadgeTempl", frndsWithBadge).appendTo("#allMembersWithBadge");
    $("#divCF_FWB_Prof").jqm({
        trigger: '.openCFProf_FWB',
        modal: true,
        onShow: function (h) {
            h.w.css({ 'height': 'auto', 'width': '480px', 'z-index': '4000' }).slideDown(
            //to avoid orange borders in Chrome
                    function () { $("input:button:visible:last").focus().blur(); }
                );
            h.o.css("z-index", "3999");
        },
        onHide: function (h) {
            h.w.slideUp("normal", function () { if (h.o) h.o.remove(); });
        }
    });
}

VHM.Badges.GetMyEarnedBadgesHtml = function (badgesObj, holderID) { //( badgeName, holderID, imgObj )
    var badgesMarkup = "<li class=\"liEarnedBadges\" style=\"position:relative;\"><div class=\"smBadgeEnrdLFT VintStamp_div\"><img onclick=\"javascript:VHM.Badges.GetBadgeInfo_Earned(${BadgeID},${RewardedTaskTransactionID}, 'divBagdeDetails');return false;\" alt=\"${BadgeName}\" class=\"smallErndBadgeImg clsCursHand VintStamp_img\" src=\"../../secure/Rewards/BadgeImage.aspx?bid=${BadgeID}&size=small\" onmouseout=\"VHM.Badges.HideBadgeName('divBadgeHover');\" onmouseover=\"VHM.Badges.ShowBadgeName('${BadgeName}','divBadgeHover', this);\" /><span class=\"VintStamp_span\" style=\"font-size:7px;top:28px;left:10px;\">${BadgeVintStamp}</span></div></li>";

    $("#" + holderID).html("");

    if (badgesObj !== "") {
        var allErnBadges = eval(badgesObj);
        //set up the pagination
        var noOfPages;
        var pageSize = 15;
        if (badgesObj !== "") {
            var allErnBadges = eval(badgesObj);
            //return paging objects only
            noOfPages = Math.ceil(allErnBadges.length / pageSize);
            if (noOfPages < VHM.Badges.EarnedB_pageNo) {
                VHM.Badges.EarnedB_pageNo = 1;
            };
            if (VHM.Badges.EarnedB_pageNo == 0) {
                VHM.Badges.EarnedB_pageNo = noOfPages;
            };
            var strPagingTitle = "Page " + VHM.Badges.EarnedB_pageNo + " of " + noOfPages;
            $("#spanEarned_Pag").html(strPagingTitle);
            var startInx = parseInt(VHM.Badges.EarnedB_pageNo * pageSize - pageSize);
            var objPerPage = parseInt(VHM.Badges.EarnedB_pageNo * pageSize);
            var endInd = Math.min(objPerPage, allErnBadges.length);
            var countedObj = [];
            for (var i = startInx; i < endInd; i++) {
                countedObj.push(allErnBadges[i]);
            }
            var navPrev = $("#ErndBadges_Nav_Prev");
            var navNext = $("#ErndBadges_Nav_Next");
            if (allErnBadges.length < pageSize + 1) {
                navNext.html("");
                navPrev.html("");
            } else {
                var arrowLeftHTML = "<img alt=\"previous\" border=\"0\" src=\"../../images/rewards/Badges/slices/arrow_left_noBGR.png\" style=\"vertical-align:middle;cursor:hand;cursor:pointer;\" onclick=\"javascript:VHM.Badges.Earned_GetPreviousPage('sortEarned');\" /> ";
                var arrowRightHTML = "<img alt=\"next\" border=\"0\" src=\"../../images/rewards/Badges/slices/arrow-right_noBGR.png\" style=\"vertical-align:middle;cursor:hand;cursor:pointer;\" onclick=\"javascript:VHM.Badges.Earned_GetNextPage('sortEarned');\" />";
                if (navPrev.html() === "") {
                    $(arrowLeftHTML).appendTo(navPrev);
                };
                if (navNext.html() === "") {
                    $(arrowRightHTML).appendTo(navNext);
                };
            };
            $.template("earnedBadgeTempl", badgesMarkup);
            $.tmpl("earnedBadgeTempl", countedObj).appendTo("#" + holderID);
        };
    }

    /* $( ".smallErndBadgeImg" ).draggable( { disabled: false } );
    $( "#ulTopFiveBadges" ).droppable( {
    drop: function () { alert( 'dropped' ); }
    } );*/
};
//To be released later!
VHM.Badges.GetMyTopFiveBadgesHtml = function (badgesObj, holderID) {
    var badgesMarkup = "<li class=\"liEarnedBadges\"><div class=\"smTopFiveBadge\" ><img alt=\"${BadgeName}\" class=\"smallErndBadgeImg clsCursHand\" src=\"../../secure/Rewards/BadgeImage.aspx?bid=${BadgeID}&size=small\" /></div></li>";
    $("#" + holderID).html("");
    if (badgesObj !== "") {
        var allErnBadges = eval(badgesObj);
        if (badgesObj !== "") {
            var allErnBadges = eval(badgesObj);
            //return paging objects only

            var startInx = 0; //parseInt( VHM.Badges.EarnedB_pageNo * pageSize - pageSize );
            //var objPerPage = 5; //parseInt( VHM.Badges.EarnedB_pageNo * pageSize );
            var endInd = Math.min(5, allErnBadges.length);
            var countedObj = [];
            for (var i = startInx; i < endInd; i++) {
                countedObj.push(allErnBadges[i]);
            }
            $.template("earnedBadgeTempl", badgesMarkup);
            $.tmpl("earnedBadgeTempl", countedObj).appendTo("#" + holderID);
        };
    }
};
//pass in what we sort by: 'topFive', 'mostRec', 'byCateg', 'byBadgeName', 'byExclusiv', 'byRating'
VHM.Badges.Earned_Sort_OnLoad = function () {
    var dataObj = ""; //this will hold all sorted badges
    //on page load we will default to My Top 5 option
    dataObj = $("#hdnTopFive").val(); //$( "#hdnMostRec" ).val();
    $("#divTopFive").show();
    if (typeof dataObj != undefined && dataObj != "") {
        VHM.Badges.GetMyEarnedBadgesHtml(dataObj, 'ulErndBadgesHolder');
    } else {
        VHM.Badges.NoMoreBadgesHTML('ulErndBadgesHolder', 'spanEarned_Pag');
        VHM.Badges.HideNavigationArrows('earned');
    };
};
VHM.Badges.Earned_Sort = function (obj, parentListID, sortOption) {
    VHM.Badges.onSortClick(obj, parentListID);
    var dataObj = ""; //this will hold all sorted badges
    $("#divTopFive").hide();
    if (sortOption === 'topFive') {
        if (VHM.Badges.hasSavedBadgeAsFav != undefined && VHM.Badges.hasSavedBadgeAsFav) {
            window.location.reload();
        }
        dataObj = $("#hdnTopFive").val();
        $("#divTopFive").show();
    } else if (sortOption === 'mostRec') {
        dataObj = $("#hdnMostRec").val();
    } else if (sortOption === 'byCateg') {
        dataObj = $("#hdnEarned_Categ").val();
    } else if (sortOption === 'byBadgeName') {
        dataObj = $("#hdnEarned_Name").val();
    } else if (sortOption === 'byExclusiv') {
        dataObj = $("#hdnEarned_Exclusiv").val();
    } else if (sortOption === 'byRating') {
        dataObj = $("#hdnEarned_Rating").val();
    }
    if (typeof dataObj != undefined && dataObj != "") {
        VHM.Badges.GetMyEarnedBadgesHtml(dataObj, 'ulErndBadgesHolder');
    } else {
        VHM.Badges.NoMoreBadgesHTML('ulErndBadgesHolder', 'spanEarned_Pag');
        VHM.Badges.HideNavigationArrows('earned');
    };
};
VHM.Badges.EarnedB_pageNo;
VHM.Badges.Earned_GetPreviousPage = function (parentListID) {
    //see what option is selected
    var jqoParentList = $("#" + parentListID);
    var listElems = jqoParentList.find('li');
    var sortOption;
    listElems.each(function () {
        var currItem = $(this);
        if ($(this).hasClass('slctdOption')) {
            sortOption = currItem.attr("vlc_option");
        };
    });
    var dataObj = ""; //this will hold all sorted badges
    /*$( "#divTopFive" ).hide();
    if ( sortOption === 'topFive' ) {
    dataObj = $( "#hdnTopFive" ).val();
    $( "#divTopFive" ).show();
    } else */
    if (sortOption === 'mostRec') {
        dataObj = $("#hdnMostRec").val();
    } else if (sortOption === 'byCateg') {
        dataObj = $("#hdnEarned_Categ").val();
    } else if (sortOption === 'byBadgeName') {
        dataObj = $("#hdnEarned_Name").val();
    } else if (sortOption === 'byExclusiv') {
        dataObj = $("#hdnEarned_Exclusiv").val();
    } else if (sortOption === 'byRating') {
        dataObj = $("#hdnEarned_Rating").val();
    };
    VHM.Badges.EarnedB_pageNo = VHM.Badges.EarnedB_pageNo - 1;
    if (typeof dataObj != undefined && dataObj != "") {
        VHM.Badges.GetMyEarnedBadgesHtml(dataObj, 'ulErndBadgesHolder');
    } else {
        VHM.Badges.NoMoreBadgesHTML('ulErndBadgesHolder', 'spanEarned_Pag');
        VHM.Badges.HideNavigationArrows('earned');
    };
};
VHM.Badges.Earned_GetNextPage = function (parentListID) {
    //see what option is selected
    var jqoParentList = $("#" + parentListID);
    var listElems = jqoParentList.find('li');
    var sortOption;
    listElems.each(function () {
        var currItem = $(this);
        if ($(this).hasClass('slctdOption')) {
            sortOption = currItem.attr("vlc_option");
        };
    });
    var dataObj = ""; //this will hold all sorted badges
    /*$( "#divTopFive" ).hide();
    if ( sortOption === 'topFive' ) {
    dataObj = $( "#hdnTopFive" ).val();
    $( "#divTopFive" ).show();
    } else */
    if (sortOption === 'mostRec') {
        dataObj = $("#hdnMostRec").val();
    } else if (sortOption === 'byCateg') {
        dataObj = $("#hdnEarned_Categ").val();
    } else if (sortOption === 'byBadgeName') {
        dataObj = $("#hdnEarned_Name").val();
    } else if (sortOption === 'byExclusiv') {
        dataObj = $("#hdnEarned_Exclusiv").val();
    } else if (sortOption === 'byRating') {
        dataObj = $("#hdnEarned_Rating").val();
    };
    VHM.Badges.EarnedB_pageNo = VHM.Badges.EarnedB_pageNo + 1;
    if (typeof dataObj != undefined && dataObj != "") {
        VHM.Badges.GetMyEarnedBadgesHtml(dataObj, 'ulErndBadgesHolder');
    } else {
        VHM.Badges.NoMoreBadgesHTML('ulErndBadgesHolder', 'spanEarned_Pag');
        VHM.Badges.HideNavigationArrows('earned');
    };
};
VHM.Badges.NextB_pageNo;
VHM.Badges.GetNextBadgesHtml = function (badgesObj, holderID, loadBB) {
    var badgesMarkup = "<li class=\"liNextBadges\" style=\"position:relative;\"><div class=\"smBadgeEnrdLFT VintStamp_div\"><img onclick=\"javascript:VHM.Badges.GetBadgeInfo(${BadgeID}, 'divBagdeDetails');return false;\" class=\"smallErndBadgeImg clsCursHand VintStamp_img\" src=\"../../secure/Rewards/BadgeImage.aspx?bid=${BadgeID}&size=small&showBW=yes\" vlc_BadgeID = \"${BadgeID}\" onmouseout=\"VHM.Badges.HideBadgeName('divBadgeHover');\" onmouseover=\"VHM.Badges.ShowBadgeName('${BadgeName}','divBadgeHover', this);\" /><span class=\"VintStamp_span\" style=\"font-size:7px;top:28px;left:10px;\">${BadgeVintStamp}</span></div></li>";
    $("#" + holderID).html("");
    //set up the pagination
    var noOfPages;
    var pageSize = 12;
    if (badgesObj !== "") {
        var allErnBadges = eval(badgesObj);
        //check if we need to load Best Bet For selected categ
        if (loadBB) {
            nBadgeID = allErnBadges[0].BadgeID;
            VHM.Badges.getBestBetInfo(nBadgeID);
        }
        //return paging objects only
        noOfPages = Math.ceil(allErnBadges.length / pageSize);
        if (noOfPages < VHM.Badges.NextB_pageNo) {
            VHM.Badges.NextB_pageNo = 1;
        };
        if (VHM.Badges.NextB_pageNo == 0) {
            VHM.Badges.NextB_pageNo = noOfPages;
        };
        var strPagingTitle = "Page " + VHM.Badges.NextB_pageNo + " of " + noOfPages;
        $("#spanNext_pagination").html(strPagingTitle);
        var startInx = parseInt(VHM.Badges.NextB_pageNo * pageSize - pageSize);
        var objPerPage = parseInt(VHM.Badges.NextB_pageNo * pageSize);
        var endInd = Math.min(objPerPage, allErnBadges.length);
        var countedObj = [];
        for (var i = startInx; i < endInd; i++) {
            countedObj.push(allErnBadges[i]);
        }
        var navPrev = $("#AvBadges_Nav_Prev");
        var navNext = $("#AvBadges_Nav_Next");
        if (allErnBadges.length < pageSize + 1) {
            navNext.html("");
            navPrev.html("");
        } else {
            var arrowLeftHTML = "<img alt=\"previous\" border=\"0\" src=\"../../images/rewards/Badges/slices/arrow_left_noBGR.png\" style=\"vertical-align:middle;cursor:hand;cursor:pointer;\" onclick=\"javascript:VHM.Badges.Next_GetPreviousPage('sortWhatSNext');\" />";
            var arrowRightHTML = "<img alt=\"next\" border=\"0\" src=\"../../images/rewards/Badges/slices/arrow-right_noBGR.png\" style=\"vertical-align:middle;cursor:hand;cursor:pointer;\" onclick=\"javascript:VHM.Badges.Next_GetNextPage('sortWhatSNext');\" />";
            if (navPrev.html() === "") {
                $(arrowLeftHTML).appendTo(navPrev);
            };
            if (navNext.html() === "") {
                $(arrowRightHTML).appendTo(navNext);
            };
        };
        $.template("earnedBadgeTempl", badgesMarkup);
        $.tmpl("earnedBadgeTempl", countedObj).appendTo("#" + holderID);
    };
};
VHM.Badges.NextBadges_Sort_OnLoad = function () {
    var dataObj = ""; //this will hold all sorted badges
    dataObj = $("#hdnAvlbl_Others").val();

    if (typeof dataObj != undefined && dataObj != "") {
        VHM.Badges.GetNextBadgesHtml(dataObj, 'ulNextBadgesHolder', true);
    } else {
        //if no badges are available to show, display an empty content and also, hide arrows
        VHM.Badges.NoMoreBadgesHTML('ulNextBadgesHolder', 'spanNext_pagination');
        VHM.Badges.HideNavigationArrows('available');
    }
};
VHM.Badges.NoMoreBadgesHTML = function (holderID, paginationHolderID) {
    $("#" + holderID).html("");
    $("#" + paginationHolderID).html("");
    var emptyHTML = "<li style=\"padding:10px; text-align:center;\"><span>There are no badges in this category</span></li>";
    $("#" + holderID).html(emptyHTML);
    var strPagingTitle = "Page 1 of 1";
    $("#" + paginationHolderID).html(strPagingTitle);
};
VHM.Badges.HideNavigationArrows = function (section) {
    var navNext;
    var navPrev;
    if (section === 'earned') {
        navPrev = $("#ErndBadges_Nav_Prev");
        navNext = $("#ErndBadges_Nav_Next");
    } else if (section == 'available') {
        navPrev = $("#AvBadges_Nav_Prev");
        navNext = $("#AvBadges_Nav_Next");
    };
    navNext.html("");
    navPrev.html("");
};
//pass in what we sort by: 'ByOthers', 'byCateg', 'byRating', 'All'
VHM.Badges.Next_Sort = function (obj, parentListID, sortOption) {
    VHM.Badges.onSortClick(obj, parentListID);
    var dataObj = ""; //this will hold all sorted badges
    if (sortOption === 'ByOthers') {
        dataObj = $("#hdnAvlbl_Others").val();
    } else if (sortOption === 'byCateg') {
        dataObj = $("#hdnAvlb_SameCateg").val();
    } else if (sortOption === 'byRating') {
        dataObj = $("#hdnAvlbl_HighRating").val();
    } else if (sortOption === 'All') {
        dataObj = $("#hdnAvlbl_All").val();
    }
    if (typeof dataObj != undefined && dataObj != "") {
        VHM.Badges.GetNextBadgesHtml(dataObj, 'ulNextBadgesHolder', true);
    } else {
        VHM.Badges.NoMoreBadgesHTML('ulNextBadgesHolder', 'spanNext_pagination');
        VHM.Badges.HideNavigationArrows('available');
    };
};
VHM.Badges.Next_GetNextPage = function (parentListID) {
    //see what option is selected
    var jqoParentList = $("#" + parentListID);
    var listElems = jqoParentList.find('li');
    var sortOption;
    //sortOption = currItem.find( "[vlc_name='WinnerImgProf']" );
    listElems.each(function () {
        var currItem = $(this);
        if ($(this).hasClass('slctdOption')) {
            sortOption = currItem.attr("vlc_option");
        };
    });
    var dataObj = ""; //this will hold all sorted badges
    if (sortOption === 'ByOthers') {
        dataObj = $("#hdnAvlbl_Others").val();
    } else if (sortOption === 'byCateg') {
        dataObj = $("#hdnAvlb_SameCateg").val();
    } else if (sortOption === 'byRating') {
        dataObj = $("#hdnAvlbl_HighRating").val();
    } else if (sortOption === 'All') {
        dataObj = $("#hdnAvlbl_All").val();
    }
    VHM.Badges.NextB_pageNo = VHM.Badges.NextB_pageNo + 1;
    if (typeof dataObj != undefined && dataObj != "") {
        VHM.Badges.GetNextBadgesHtml(dataObj, 'ulNextBadgesHolder', false);
    } else {
        VHM.Badges.NoMoreBadgesHTML('ulNextBadgesHolder', 'spanNext_pagination');
        VHM.Badges.HideNavigationArrows('available');
    };
};
VHM.Badges.Next_GetPreviousPage = function (parentListID) {
    //see what option is selected
    var jqoParentList = $("#" + parentListID);
    var listElems = jqoParentList.find('li');
    var sortOption;
    listElems.each(function () {
        var currItem = $(this);
        if ($(this).hasClass('slctdOption')) {
            sortOption = currItem.attr("vlc_option");
        };
    });
    var dataObj = ""; //this will hold all sorted badges
    if (sortOption === 'ByOthers') {
        dataObj = $("#hdnAvlbl_Others").val();
    } else if (sortOption === 'byCateg') {
        dataObj = $("#hdnAvlb_SameCateg").val();
    } else if (sortOption === 'byRating') {
        dataObj = $("#hdnAvlbl_HighRating").val();
    } else if (sortOption === 'All') {
        dataObj = $("#hdnAvlbl_All").val();
    }
    VHM.Badges.NextB_pageNo = VHM.Badges.NextB_pageNo - 1;
    if (typeof dataObj != undefined && dataObj != "") {
        VHM.Badges.GetNextBadgesHtml(dataObj, 'ulNextBadgesHolder', false);
    } else {
        VHM.Badges.NoMoreBadgesHTML('ulNextBadgesHolder', 'spanNext_pagination');
        VHM.Badges.HideNavigationArrows('available');
    };
};

VHM.Badges.onSortClick = function (obj, parentListID) {
    var jqoParentList = $("#" + parentListID);
    var listElems = jqoParentList.find('li');
    listElems.each(function () {
        var currItem = $(this);
        if ($(this).hasClass('slctdOption')) {
            $(this).removeClass('slctdOption');
            $(this).addClass('sortOption');
        }
    });

    var jqoListEleme = $(obj).parent();
    $(jqoListEleme).removeClass('sortOption');
    $(jqoListEleme).addClass("slctdOption");
};

VHM.Badges.getBestBetInfo = function (nBadgeID) {
    var f_success = function (responseData, textStatus) {
        var badgeObject = (typeof (responseData) != "object") ? $.parseJSON(responseData) : responseData;
        var bbBadgeName = $("#bbBadgeName");
        bbBadgeName.html(badgeObject.BadgeName);
        var BBImgDiv = $(".bestBetImage");
        var bbBadgeImg = BBImgDiv.find("[vlc_Name='bestBetImage']");
        bbBadgeImg.attr("src", badgeObject.ImageURL);
        bbBadgeImg.click(function () {
            VHM.Badges.GetBadgeInfo(nBadgeID, 'divBagdeDetails');
        });
        bbBadgeImg.hover(
            function () {
                VHM.Badges.ShowBadgeName(badgeObject.BadgeName, 'divBadgeHover', this);
            },
            function () {
                VHM.Badges.HideBadgeName('divBadgeHover');
            }
        );
        var vintStamp = BBImgDiv.find("[vlc_name='bestBetVint']");
        if (badgeObject.LevelYearStamp !== "") {
            vintStamp.html(badgeObject.LevelYearStamp);
            vintStamp.show();
        } else {
            vintStamp.html("");
            vintStamp.hide();
        };
        var bbDescr = $("#bbBadgeDescr");
        bbDescr.html(badgeObject.BadgeDescription);
        var frWithBBBadge = $("#frThatHaveBadge");
        if (badgeObject.HasFriends) {
            //clear holder first
            frWithBBBadge.html("");
            frWithBBBadge.show();
            if (badgeObject.NoOfFriendsWithBadge == 0) {
                frWithBBBadge.html("0 friends have this badge");
            } else if (badgeObject.NoOfFriendsWithBadge == 1) {
                var strFrText = "<a style=\"font-size:9px;padding:0px;cursor:hand;cursor:pointer;\" onclick=\"javascript:VHM.Badges.openFWB_PopUp('divFrndsWithBadge', " + badgeObject.BadgeID + ", 'allMembersWithBadge' ); return false;\" > 1 friend </a><span> has this badge</span>";
                $(strFrText).appendTo(frWithBBBadge);
            } else if (badgeObject.NoOfFriendsWithBadge > 1) {
                var strFrText1 = "<a style=\"font-size:9px;padding:0px;cursor:hand;cursor:pointer;\" onclick=\"javascript:VHM.Badges.openFWB_PopUp('divFrndsWithBadge', " + badgeObject.BadgeID + ", 'allMembersWithBadge' ); return false; \" > " + badgeObject.NoOfFriendsWithBadge + " friends </a><span> have this badge</span>";
                $(strFrText1).appendTo(frWithBBBadge);
            };
        } else {
            frWithBBBadge.hide();
        }
        var bbExclus = $("#bbBadgeExclus");
        if (badgeObject.Exclusivity === 0) {
            bbExclus.html("<span><strong>Exclusivity: </strong><span><span>You could be the first</span>");
        } else {
            var strExcl = "<span><strong>Exclusivity: </strong><span><span>" + badgeObject.Exclusivity + "% have it </span>";
            bbExclus.html(strExcl);
        }
        //get the rating module
        $('#divBBRating').vhmFeedback({ fid: badgeObject.BadgeID, fc: 'BDGE', type: 'rating', avgRatingLabel: '' });
        //You must have administrative credentials to perform this task. Contact your system administrator for assistance. ?????
    };
    var cID = $('#hdnCID').val();

    var params = { sId: VHM.Badges.SessionID, bId: nBadgeID, appPath: VHM.Badges.appPath, commID: cID, badgeImgSize: 'regular', rewardsTransID: '' };
    var sParams = JSON.stringify(params);
    $.vhmAjax({
        cache: false,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: VHM.Badges.ServiceUrl + "/GetBadgeInfo",
        data: sParams,
        dataType: "json",
        success: function (data) {
            f_success(data.d ? data.d : data);
        },
        error: function (xhr, ajaxOptions, error) { }
    });
};

VHM.Badges.LimTime_PageNo;
VHM.Badges.getLimitedTimeBadges = function () {
    var hdnLTB = $("#hdnLimTimeBadges").val();
    if (typeof hdnLTB != undefined && hdnLTB != "") {
        var countedObj = [];
        var objLTB = eval(hdnLTB);
        var jqoLTBadgesCont = $("#tblLimTimeBadges");
        var arrowLeft = jqoLTBadgesCont.find("[vlc_name='imgPrev']");
        var arrowRight = jqoLTBadgesCont.find("[vlc_name='imgNext']");
        var noOfPages;
        var pageSize = 1;
        noOfPages = objLTB.length;
        if (noOfPages == 1) {
            //hide arrows
            arrowLeft.hide();
            arrowRight.hide();
            VHM.Badges.LimTime_PageNo = 1;
            countedObj = objLTB[0];
        } else {
            if (noOfPages < VHM.Badges.LimTime_PageNo) {
                VHM.Badges.LimTime_PageNo = 1;
            };
            if (VHM.Badges.LimTime_PageNo == 0) {
                VHM.Badges.LimTime_PageNo = noOfPages;
            };
            countedObj = objLTB[VHM.Badges.LimTime_PageNo - 1]; //array index starts from 0
            arrowLeft.show();
            arrowRight.show();
        }
        //clean the holder first
        VHM.Badges.CleanLimitedTimeHolder(jqoLTBadgesCont);
        var ltBadgeName = jqoLTBadgesCont.find("[vlc_name='ltBadgeName']");
        var bName = "<strong>" + countedObj.BadgeName + "</strong>";
        ltBadgeName.html(bName);
        var ltBadgeImage = jqoLTBadgesCont.find("[vlc_name='imgLTBadge']");
        ltBadgeImage.attr("src", countedObj.BadgeImage);
        ltBadgeImage.unbind('click').click(function () {
            VHM.Badges.GetBadgeInfo(countedObj.BadgeID, 'divBagdeDetails');
        });
        ltBadgeImage.attr("alt", countedObj.BadgeName);
        ltBadgeImage.hover(
            function () {
                VHM.Badges.ShowBadgeName(countedObj.BadgeName, 'divBadgeHover', this);
            },
            function () {
                VHM.Badges.HideBadgeName('divBadgeHover');
            }
        );

        var ltBadgeDescr = jqoLTBadgesCont.find("[vlc_name='ltBadgeDescr']");
        ltBadgeDescr.html(countedObj.BadgeDescription);

        var ltBadgeExc = jqoLTBadgesCont.find("[vlc_name='ltBadgeExclusivity']");
        ltBadgeExc.html(countedObj.Exclusivity);

    }
};
VHM.Badges.LimTimeB_GetNext = function () {
    VHM.Badges.LimTime_PageNo = VHM.Badges.LimTime_PageNo + 1;
    VHM.Badges.getLimitedTimeBadges();
};
VHM.Badges.LimTimeB_GetPrev = function () {
    VHM.Badges.LimTime_PageNo = VHM.Badges.LimTime_PageNo - 1;
    VHM.Badges.getLimitedTimeBadges();
};

VHM.Badges.PopulateBadgeDet = function (responseData, dialogID, showNOFLink) {
    var dialog;
    if (dialogID.id) {
        dialog = $("#" + dialogID.id);
    } else {
        dialog = $("#" + dialogID);
    }
    //debugger;
    var badgeObject = $.parseJSON(responseData.d);
    VHM.Badges.CleanBadgeDetailsPop(dialog);

    var bbBadgeName = dialog.find("[vlc_name='vlcPop_badgeName']");
    bbBadgeName.html(badgeObject.BadgeName);

    var bbBadgeImg = dialog.find("[vlc_name='vlcPop_badgeImg']");
    bbBadgeImg.attr("src", badgeObject.ImageURL);
    bbBadgeImg.attr("alt", badgeObject.BadgeName);

    //get the Vintage stamp for Levels badges
    var vintageStamp = dialog.find("[vlc_name='VintageStamp']");

    if (badgeObject.LevelYearStamp !== "") {
        vintageStamp.html(badgeObject.LevelYearStamp);
        vintageStamp.show();
    } else {
        vintageStamp.html("");
        vintageStamp.hide();
    };

    var badgeDescr = dialog.find("[vlc_name='vlcPop_BadgeDescr']");
    badgeDescr.html(badgeObject.BadgeDescription);

    var badgeCat = dialog.find("[vlc_name='vlcPop_BadgeCateg']");
    var badgeCategTitle = dialog.find("[vlc_name='vlcPop_CatTitle']");

    if (typeof badgeObject.Category != "undefined" && badgeObject.Category != "") {
        badgeCat.html(badgeObject.Category);
        badgeCategTitle.show();
        badgeCategTitle.html("Category: ");
    } else {
        badgeCat.html("");
        badgeCategTitle.hide();
        badgeCategTitle.html("");
    }

    var bDescr_Name = dialog.find("[vlc_name='vlcPop_DescrName']");
    var bDateEarned = dialog.find("[vlc_name='vlcPop_DateEarned']");
    var badgeLikes = dialog.find("[vlc_name='divBadgeLikes']");
    var badgeRating = dialog.find("[vlc_name='divAvrRating']");
    var badgeMyRating = dialog.find("[vlc_name='divYourRating']");
    var divAddAsFav = dialog.find("[vlc_name='vlcPop_AddAsFav']");
    var divTopFiveB = dialog.find("[vlc_name='vlcPop_TopFiveFav']");

    if (badgeObject.IsBadgeEarned) {
        bDescr_Name.html("Earned By: ");
        bDateEarned.show();
        var strDateEarned = "<span class=\"VHMPop_smallName\">Date Earned: </span><span>" + badgeObject.DateEarned + "</span>";

        bDateEarned.html(strDateEarned);
        //earned badges can be saved as "Favorite"
        if (badgeObject.IsBadgeFavorite) {
            divAddAsFav.hide();
            divTopFiveB.show();
        } else {
            divAddAsFav.show();

            //TODO: add onclick event for clicking on "Add to favorite" link... added RewardedTaskTransactionID to badge object.
            var hrefAddBadgeAsFAv = dialog.find("[vlc_name='hrefAddFav']");
            hrefAddBadgeAsFAv.click(function () {
                divAddAsFav.hide();
                divTopFiveB.show();
                VHM.Badges.SaveBadgeAsFav(badgeObject.RewardedTaskTransactionID);
            });
            divTopFiveB.hide();
        }
        //get the rating module
        try {
            badgeLikes.vhmFeedback({ fid: badgeObject.BadgeID, fc: 'BDGE', type: 'like' });
            badgeRating.vhmFeedback({ fid: badgeObject.BadgeID, fc: 'BDGE', type: 'rating', avgRatingLabel: '<span style="font-weight:bold;margin-left:-8px;">Average rating:</span>' });
            badgeMyRating.vhmFeedback({ fid: badgeObject.BadgeID, fc: 'BDGE', type: 'yourrating', yourRatingLabel: '<span style="font-weight:bold;margin-left:5px;">Your rating:</span>' });
        } catch (e) {
            nativeAlert(e);
        }
    } else {
        bDescr_Name.html("How to get it: ");
        bDateEarned.hide();
        divAddAsFav.hide();
        //get the rating module
        badgeLikes.vhmFeedback({ fid: badgeObject.BadgeID, fc: 'BDGE', type: 'disabledlike' });
        badgeRating.vhmFeedback({ fid: badgeObject.BadgeID, fc: 'BDGE', type: 'rating', avgRatingLabel: '<span style="font-weight:bold;margin-left:-8px;">Average Rating:</span>' });
        //do not display info about favorite/top 5 badges
        divAddAsFav.hide();
        divTopFiveB.hide();
    }

    var frWithBBBadge = dialog.find("[vlc_name='vlcPop_FrWithBadge']");

    if (badgeObject.HasFriends && showNOFLink === 'true') {
        frWithBBBadge.show();

        if (badgeObject.NoOfFriendsWithBadge == 0) {
            frWithBBBadge.html("0 friends have this badge");
        } else if (badgeObject.NoOfFriendsWithBadge == 1) {
            var strFrText = "<a style=\"font-size:11px;padding:0px;cursor:hand;cursor:pointer;\" vlc_BadgeID=\"" + badgeObject.BadgeID + "\" onclick=\"javascript:VHM.Badges.openFWB_PopUp('divBadgeDet_FrWithBadge'," + badgeObject.BadgeID + ", 'ulBadgeDet_FrWithBadge');return false; \"> 1 friend </a><span> has this badge</span>";
            $(strFrText).appendTo(frWithBBBadge);
            //frWithBBBadge.html( strFrText );
        } else if (badgeObject.NoOfFriendsWithBadge > 1) {
            var strFrText = "<a style=\"font-size:11px;padding:0px;cursor:hand;cursor:pointer;\" vlc_BadgeID=\"" + badgeObject.BadgeID + "\" onclick=\"javascript:VHM.Badges.openFWB_PopUp('divBadgeDet_FrWithBadge'," + badgeObject.BadgeID + ", 'ulBadgeDet_FrWithBadge');return false; \"> " + badgeObject.NoOfFriendsWithBadge + " friends </a><span> have this badge</span>";
            //frWithBBBadge.html( strFrText );
            $(strFrText).appendTo(frWithBBBadge);
        };

        $("#divBadgeDet_FrWithBadge").dialog({
            modal: true
            , autoOpen: false
            , resizable: false
            , dialogClass: 'no-title'
            , open: function () {
                $("input:button:visible:last").focus().blur();
            }
        });

        $('.openFWTB').click(function () {
            $("#divBadgeDet_FrWithBadge").dialog('open');
        });
    } else {
        frWithBBBadge.hide();
    }
    var bbExclus = dialog.find("[vlc_name='vlcPop_Exclusiv']");
    if (badgeObject.Exclusivity === 0) {
        bbExclus.html("You could be the first");
    } else {
        var strExcl = badgeObject.Exclusivity + "% have it";
        bbExclus.html(strExcl);
    };

    //declare jqModal for open friends with badge pop up
    $("#divBadgeDet_FrWithBadge").dialog({
        modal: true
        , autoOpen: false
        , resizable: false
        , dialogClass: 'no-title'
    });

    $('.openFWTB').click(function () {
        $("#divBadgeDet_FrWithBadge").dialog('open');
    });

    dialog.dialog('open');
};
//if used when clicking a badge image/name from a UserProfile Pop Up (we don't show "Number of Friends here to avoid multiple pop-ups open on top of each other) - simply pass in third parameter 'false'
//if used when clicking a badge image/name and we need to show "### friends have this badge" link, either pass in third param 'true', or leave null.
VHM.Badges.GetBadgeInfo = function (nBadgeID, dialogID, showNOfFWTBLink) {

    // if the showNOfFWTBLink parameter is null, that means it was not passed and should default to 'true' meaning that pop up should display the "### friends have this badge" link
    if (showNOfFWTBLink === undefined) {
        showNOfFWTBLink = 'true';
    }
    //    var sessID;
    //    if (typeof VHM.Badges.SessionID !== "undefined") {
    //        sessID = VHM.Badges.SessionID;
    //    } else if (typeof VHM.Social.SessionID !== "undefined") {
    //        //if VHM.Badges.SessionID is undefined then call is made from landing page, so use VHM.Social.SessionID
    //        sessID = VHM.Social.SessionID;
    //    };
    var cID = $('#hdnCID').val();
    var sessID = '';
    if (typeof cID === "undefined" || cID === null) {
        cID = "1";
    }
   
    var Params = { bId: nBadgeID, sId: sessID, appPath: VHM.Badges.appPath, commID: cID, badgeImgSize: "regular", rewardsTransID: "" };
    var sParams = JSON.stringify(Params);
    $.vhmAjax({
        cache: false,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: VHM.Badges.ServiceUrl + "/GetBadgeInfo",
        data: sParams,
        dataType: "json",
        success: function (responseData, textStatus) {
            if (responseData != null && responseData.d != null) {
                VHM.Badges.PopulateBadgeDet(responseData, dialogID, showNOfFWTBLink);
                            
            };
        },
        error: function (xhr, ajaxOptions, error) {
            nativeAlert('error');
        }
    });
};
VHM.Badges.GetBadgeInfo_Earned = function (nBadgeID, nRewardTransID, dialogID) {
    var sessID;
    if (typeof VHM.Badges.SessionID !== "undefined") {
        sessID = VHM.Badges.SessionID;
    } else if (typeof VHM.Social.SessionID !== "undefined") { //if VHM.Badges.SessionID is undefined then call is made from landing page, so use VHM.Social.SessionID
        sessID = VHM.Social.SessionID;
    };
    var cID = $('#hdnCID').val();
    if (typeof cID === "undefined" || cID === null) {
        cID = "";
    };
    var Params = { bId: nBadgeID, sId: sessID, appPath: VHM.Badges.appPath, commID: cID, badgeImgSize: "regular", rewardsTransID: nRewardTransID };
    var sParams = JSON.stringify(Params);
    $.vhmAjax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: VHM.Badges.ServiceUrl + "/GetBadgeInfo",
        data: sParams,
        dataType: "json",
        success: function (responseData, textStatus) {
            if (responseData != null && responseData.d != null) {
                VHM.Badges.PopulateBadgeDet(responseData, dialogID, 'true');
            };
        },
        error: function (xhr, ajaxOptions, error) { }
    });
};
VHM.Badges.CleanBadgeDetailsPop = function (dialog) {
    var bbBadgeName = dialog.find("[vlc_name='vlcPop_badgeName']"); bbBadgeName.html("");

    var bbBadgeImg = dialog.find("[vlc_name='vlcPop_badgeImg']");
    bbBadgeImg.attr("src", ""); bbBadgeImg.attr("alt", "");

    var badgeDescr = dialog.find("[vlc_name='vlcPop_BadgeDescr']"); badgeDescr.html("");
    var badgeCat = dialog.find("[vlc_name='vlcPop_BadgeCateg']"); badgeCat.html("");
    var bDescr_Name = dialog.find("[vlc_name='vlcPop_DescrName']"); bDescr_Name.html("");
    var bDateEarned = dialog.find("[vlc_name='vlcPop_DateEarned']"); bDateEarned.html("");
    var badgeLikes = dialog.find("[vlc_name='divBadgeLikes']"); badgeLikes.html("");
    var badgeRating = dialog.find("[vlc_name='divAvrRating']"); badgeRating.html("");
    var badgeMyRating = dialog.find("[vlc_name='divYourRating']"); badgeMyRating.html("");
    var frWithBBBadge = dialog.find("[vlc_name='vlcPop_FrWithBadge']"); frWithBBBadge.html("");
    var bbExclus = dialog.find("[vlc_name='vlcPop_Exclusiv']"); bbExclus.html("");
    var badgeCategTitle = dialog.find("[vlc_name='vlcPop_CatTitle']"); badgeCategTitle.html("");
    var vintageStamp = dialog.find("[vlc_name='VintageStamp']"); vintageStamp.html(""); vintageStamp.hide();
};
VHM.Badges.CleanLimitedTimeHolder = function (jqoLTBadgesCont) {
    var ltBadgeName = jqoLTBadgesCont.find("[vlc_name='ltBadgeName']");
    ltBadgeName.html("");

    var ltBadgeImage = jqoLTBadgesCont.find("[vlc_name='imgLTBadge']");
    ltBadgeImage.attr("src", ""); ltBadgeImage.attr("alt", "");

    var ltBadgeDescr = jqoLTBadgesCont.find("[vlc_name='ltBadgeDescr']");
    ltBadgeDescr.html("");

    var ltBadgeExc = jqoLTBadgesCont.find("[vlc_name='ltBadgeExclusivity']");
    ltBadgeExc.html("");
};

VHM.Badges.openFWB_PopUp = function (dialogId, nBadgeID, ulHolderID) {
    var dialog = $("#" + dialogId);
    VHM.Badges.GetMoreFrWithBadge(nBadgeID, ulHolderID);
    dialog.dialog('open');
    //return false;
};

VHM.Badges.Congratulation = function (bName, bId, bVintStamp) {
    if (bName == null || bId == null || bVintStamp == null) {
        $.vhmAjax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: VHM.Badges.ServiceUrl + "/GetMemberCongratulationBadges",
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                if (data != null && data.d != null) {
                    var jObj = $.parseJSON(data.d);
                    VHM.Badges.ShowCongratulation(jObj.bName, jObj.bId, jObj.vintageYear);
                };
            }
        });
        return;
    }
    VHM.Badges.ShowCongratulation(bName, bId, bVintStamp);
};

VHM.Badges.ShowCongratulation = function (bName, bId, vintYear) {
    if (bName != null && bId != null && bId != 0 && bName != "") {
        imgSrc = this.appPath + "../../Secure/Rewards/BadgeImage.aspx?bid=" + bId + "&size=regular";
        var content = "<div align='center'><div class='congratulationHeader' >Congratulations!</div><div class='congratulationAnimation'><img style='margin-top:3px;margin-left:10px;position:relative; width:96px;' src='" + imgSrc + "'/>" + "<span class=\"celebrationVintageCls\" >" + vintYear + "</span>"
        + "<div class='congratulationText'><span>You’ve just earned the <span class='congratulationBName' >" + bName + "</span>.<br/> Well done!</span></div><div id='rating'/></div>";
        var $obj = $("<table cellpadding='0' id='jqmPopUpTableContaner' cellspacing='0' border='0'>" +
    '<tr class="tblVHMTrow"><td class="tblVHMTcellL"></td><td class="tblVHMTcellC">' +
    '</td><td class="tblVHMTcellR"><div class="jqmClose closeButton"></div></td>' +
    '</tr><tr class="tblVHMCrow"><td class="tblVHMCcellL"></td><td class="tblVHMCcellC">' + content +
    '</td><td class="tblVHMCcellR"></td></tr><tr class="tblVHMBrow"><td class="tblVHMBcellL">' +
    '</td><td class="tblVHMBcellC"></td><td class="tblVHMBcellR"></td></tr></table>');
        $obj.find('#rating').vhmFeedback({ fid: bId, fc: 'BDGE', type: 'YourRating' }).end().dialog({
            modal: true
        , resizable: false
        , 'height': 300
        , 'width': 380
        , dialogClass: 'congratulation'
        , beforeClose: function (event, ui) {
            $.vhmAjax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: VHM.Badges.ServiceUrl + "/ChangeMemberBadgeCongratulationState",
                dataType: "json",
                success: function (data, textStatus, jqXHR) { },
                error: function (xhr, ajaxOptions, error) { }
            });
        }
        });
        if ($.browser.msie && parseInt($.browser.version) == 7) {
            $obj.find(".fdb-rating-bl").css({ 'margin-left': '95px' });
            $(".ui-dialog").css({ 'position': 'absolute', 'top': '10%', 'left': '35%' });
        }
        $(".ui-dialog-titlebar").hide();
        $obj.find(".closeButton").click(function () {
            $obj.dialog("close");
        });
    };
};
VHM.Badges.SaveBadgeAsFav = function (rewardTransID) {
    var sessID = '';
//    if (typeof VHM.Badges.SessionID !== "undefined") {
//        sessID = VHM.Badges.SessionID;
//    } else if (typeof VHM.Social.SessionID !== "undefined") { //if VHM.Badges.SessionID is undefined then call is made from landing page, so use VHM.Social.SessionID
//        sessID = VHM.Social.SessionID;
//    };
    //VHM.Badges.hasSavedBadgeAsFav
    var Params = { sId: sessID, rewardTransID: rewardTransID };
    var sParams = JSON.stringify(Params);
    $.vhmAjax({
        cache: false,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: VHM.Badges.ServiceUrl + "/SaveBadgeAsFavorite",
        data: sParams,
        dataType: "json",
        success: function (responseData, textStatus) {
            if (responseData != null && responseData.d != null) {
                var objRespData = eval('(' + responseData.d + ')');
                if (objRespData.Success) {
                    if (typeof VHM.Badges.hasSavedBadgeAsFav !== "undefined") {
                        VHM.Badges.hasSavedBadgeAsFav = true;
                    };
                };
            };
        },
        error: function (xhr, ajaxOptions, error) {
            alert("There was an error. Couldn't add badge as a favorite. Please try again later!", { title: 'Something went wrong' });
        }
    });
};
/****************************************************************************************************************************************************
***********The below functions will be used to show the badge name hover functionality for ALL badges************************************************
*****************************************************************************************************************************************************/
VHM.Badges.hoverBadgePopHTML = '<div class="VHMlevelsGraph_1"> \
        <div id="divBadge1" class="VHMRWBubbleInfo VHMRewardsBubbleInfo VHMRWLevelsControlPopup" style="top:166px; left:46px;"> \
            <table class="popup" cellpadding="0" summary="Popup20" vlc_name="vlc_PopUpBadge" > \
        	    <tr> \
        		    <td class="corner topleft"></td> \
        		    <td class="top"></td> \
        		    <td class="corner topright"></td> \
        	    </tr> \
        	    <tr> \
        		    <td class="left"></td> \
        		    <td class="popup-contents"> \
                        <div class="VHMPopupReward" style="text-align:center;"></div></td> \
        		    <td class="right"></td> \
        	    </tr> \
        	    <tr> \
        		    <td class="corner bottomleft"></td> \
        		    <td class="bottom"><div></div></td> \
        		    <td class="corner bottomright"></td> \
        	    </tr> \
            </table> \
        </div> \
    </div>';
VHM.Badges.ShowBadgeName = function (badgeName, holderID, imgObj) {
    var holder = $("#" + holderID);
    $('div.VHMlevelsGraph_1', holder).remove();
    holder.append(VHM.Badges.hoverBadgePopHTML);

    var badgeImg = imgObj;
    var top;
    var left;
    // var position = $( badgeImg ).position();
    // top = position.top + 165;
    //left = position.left + 40;
    //use offset
    var offset = $(badgeImg).offset();
    top = offset.top - 165;
    left = offset.left - 48;
    $('#divBadge1 div.VHMPopupReward').html(badgeName);
    holder.css({ left: left + 'px', top: top + 'px' });
    var info = $('#divBadge1').find("[vlc_name='vlc_PopUpBadge']");
    holder.show();
    info.show();
};
VHM.Badges.HideBadgeName = function (holderID) {
    var holder = $("#" + holderID);
    $('div.VHMlevelsGraph_1', holder).remove();
    var info = $('#divBadge1').find("[vlc_name='vlc_PopUpBadge']"); // var info = $( '.popup popBadges' );
    holder.hide();
    info.hide();
};
VHM.Badges.ShowBadgeName_UserProf = function (badgeName, holderID, imgObj) {
    var holder = $("#" + holderID);
    $('div.VHMlevelsGraph_1', holder).remove();
    holder.append(VHM.Badges.hoverBadgePopHTML);

    var badgeImg = imgObj;
    var top;
    var left;
    // var scrlTop = f_scrollTop();
    var offset = $(badgeImg).offset();
    top = offset.top - 165; //don't use scrlTop as position is relative to badge image
    left = offset.left - 43;

    $('#divBadge1 div.VHMPopupReward').html(badgeName);
    holder.css({ left: left + 'px', top: top + 'px' });
    var info = $('#divBadge1').find("[vlc_name='vlc_PopUpBadge']");
    holder.show();
    info.show();
};
VHM.Badges.ShowBadgeNameFromID = function (badgeName, holderID, imgID) {
    var holder = $("#" + holderID);
    $('div.VHMlevelsGraph_1', holder).remove();
    holder.append(VHM.Badges.hoverBadgePopHTML);

    var badgeImg = $("#" + imgID);
    var top;
    var left;
    var offset = $(badgeImg).offset();
    top = offset.top - 165;
    left = offset.left - 40;

    $('#divBadge1 div.VHMPopupReward').html(badgeName);
    holder.css({ left: left + 'px', top: top + 'px' });
    var info = $('#divBadge1').find("[vlc_name='vlc_PopUpBadge']");
    holder.show();
    info.show();
};


/****************************************************************************************************************************************************
***********The below functions will be used to determine the scroll position on page. These are browser compatible***********************************
*****************************************************************************************************************************************************/
function f_clientHeight() {
    return f_filterResults(
		window.innerHeight ? window.innerHeight : 0,
		document.documentElement ? document.documentElement.clientHeight : 0,
		document.body ? document.body.clientHeight : 0
	);
};
function f_scrollTop() {
    return f_filterResults(
		window.pageYOffset ? window.pageYOffset : 0,
		document.documentElement ? document.documentElement.scrollTop : 0,
		document.body ? document.body.scrollTop : 0
	);
};
function f_filterResults(n_win, n_docel, n_body) {
    var n_result = n_win ? n_win : 0;
    if (n_docel && (!n_result || (n_result > n_docel)))
        n_result = n_docel;
    return n_body && (!n_result || (n_result > n_body)) ? n_body : n_result;
};