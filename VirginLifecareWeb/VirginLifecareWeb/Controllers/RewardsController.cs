﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vhm.Web.BL;
using Vhm.Web.BL.Interfaces;
using Vhm.Web.Models.ViewModels.Rewards;

namespace VirginLifecareWeb.Controllers
{
    public class RewardsController : ControllerBase
    {
        public RewardsController() : base()
        {
        }

        public PartialViewResult GetBadgeInfo(long bId, string sId, string appPath, long commID, string badgeImgSize, long? rewardsTransID)
        {
            var model = new BadgeDetails
                            {
                                Name = "Test Badge",
                                BadgeId = 1
                            };
            return PartialView( "_BadgeDetails", model );
        }
    }
}
