﻿using System;
using System.Globalization;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Vhm.Web.Common;
using Vhm.Web.Models;
using VirginLifecare.Utils;
using log4net;
using Vhm.Web.BL;

namespace VirginLifecareWeb.Controllers
{
    public class AccountController : ControllerBase
    {
        
        private readonly ILog _logger;
        public AccountController()
        {
            _logger = LogManager.GetLogger( GetType() );
        }

        [RequireHttps]
        public ActionResult Login()
        {
            var session = new Session();
 
            return View(session);                   
        }

        [RequireHttps]
        [AllowAnonymous]
        public JsonResult LoginExpress( string userName, string pwd )
        {

            if ( string.IsNullOrEmpty( userName ) || string.IsNullOrEmpty( pwd ) )
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            try
            {
                VhmSession = AccountBL.Login( userName, pwd, "WebSite", (int) FormsAuthentication.Timeout.TotalMinutes );


                if ( VhmSession != null )
                {
                    _logger.Info( "Logged in ok" );

                    // Create Session Cookie
                    var oCookie = new HttpCookie( "SessionID" );
                    string encryptedData = Encrypt( userName, VhmSession.SessionID );
                    oCookie.Value = encryptedData;
                    oCookie.Secure = true;
                    oCookie.Domain = HttpRequestUtils.GetCookieDomainNameWithSubdomain(Request);
                    Response.Cookies.Add( oCookie );

                    // Cookie saves the languageId for a user that the language has been selected and it's in the session.
                    SetCookie( ApplicationCookies.VHMLanguageId.ToString(),
                               VhmSession.LanguageID.ToString( CultureInfo.InvariantCulture ),
                               DateTime.Now.AddYears( 20 ) );
                    FormsAuthentication.SetAuthCookie( encryptedData, true );

                    return Json( true, JsonRequestBehavior.AllowGet );
                } else
                {
                    return Json( false, JsonRequestBehavior.AllowGet );
                }
            }
            catch ( Exception ex )
            {
                _logger.Error(ex);
            }

            return Json( false, JsonRequestBehavior.AllowGet );

        }

        public ActionResult LogOff()
        {
            AccountBL.KillSession(VhmSession.SessionID);

            AccountBL.Logout();
            
            return RedirectToAction("Index", "Home");
        }
    }
}
