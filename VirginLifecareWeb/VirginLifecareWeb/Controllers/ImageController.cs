﻿using System;
using System.Web.Mvc;
using Vhm.Web.BL;
using VirginLifecareWeb.Controllers.ActionResults;

namespace VirginLifecareWeb.Controllers
{
    public class ImageController : ControllerBase
    {
        //pass random parameter to prevent cached data
        public ActionResult LogoDisplay(string r)
        {
            var profile = SponsorBL.GetSponsorProfile(VhmSession.PrimarySponsorID);
            return new BinaryContentResult(profile.SponsorLogo, profile.LogoType);
        }

        protected override void Initialize(ActionExecutingContext filterContext)
        {
            base.Initialize(filterContext);
            if (filterContext.Result is RedirectResult)
            {
                filterContext.Result = null;
            }
        }
    }
}