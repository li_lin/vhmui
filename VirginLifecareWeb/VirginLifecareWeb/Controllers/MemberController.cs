﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;
using System.Xml;
using Glimpse.Core.Tab.Assist;
using log4net.Core;
using Newtonsoft.Json;
using Vhm.Providers;
using Vhm.Web.BL;
using Vhm.Web.Common;
using Vhm.Web.Models;
using Vhm.Web.Models.Enums;
using Vhm.Web.Models.RepositoryModels;
using Vhm.Web.Models.ViewModels.Challenges;
using Vhm.Web.Models.ViewModels.Measurments;
using Vhm.Web.Models.ViewModels.Member;
using Vhm.Web.Models.ViewModels.Shared;
using Vhm.Web.Models.ViewModels.Social;
using VirginLifecare.Exception;
using Vhm.Web.ContentBL;


namespace VirginLifecareWeb.Controllers
{
    public class MemberController : ControllerBase
    {
        private const string GZ_ACTIVITY_COOKIE = "GZActivity";
        private const string GZ_PICK_DEVICE_HIDE_COOKIE = "GZHidePickDevicePrompt";
        private const string GZ_PICK_DEVICE_PROMPT_CONTENT = "PickDevicePrompt";
        private const string TYPE_MEMEBER = "Member";
        private const string STATUS_LOC = "LOC";
        private const string STATUS_NEW = "NEW";
        private const string CRITERIA_TRM = "TRM";
        //private const string CRITERIA_REG = "REG";  NOT USED - included for future reference
        private const string CRITERIA_UCC = "UCC";
        //private const string CRITERIA_UPG = "UPG";  NOT USED - included for future reference
        //private const string CRITERIA_HRA = "HRA"; NOT USED - included for future reference
        private const string CRITERIA_ACT = "ACT";
        private const string DEFAULT_CULTURE = "en-US";
        private const string CALLING_SOURCE = "WebSite";
        private const int PROGRAMS_MODULE_ID = 8; //programs modules will have an id greater than 8 -> 9, ...

        private bool _showPopUpForMemebershipStatusChanged;
        private bool _showPopUpForCreditCardInfo;
        private bool _showPopUpForBillingInfo;
        private bool _showPopUpForTermsPage;
        private bool _showPopUpForTermsToAcknowledge;
        private string _title;
        private string _termsCode;
        private long _termsId;
        private bool _showModalForNewLandingPage;


        public MemberController()
        {
            log4net.Config.XmlConfigurator.Configure();
        }

        public bool _betaEnabledForSponsor
        {
            get
            {
                try
                {
                    string xmlPathName = ConfigurationManager.AppSettings["BetaUISponsorPath"];
                    XmlDocument betaUIList = new XmlDocument();
                    long primarySponsorID = VhmSession.PrimarySponsorID;
                    bool containsSponsorID = false;
                    var xmlFilePath = string.Format(@"{0}\BetaUiConfig\" + xmlPathName, Request.PhysicalApplicationPath);
                    betaUIList.Load(xmlFilePath);

                    if (!String.IsNullOrEmpty(betaUIList.SelectSingleNode("BetaUIForSponsor").InnerText))
                    {
                        string[] sponsorIds = betaUIList.SelectSingleNode("BetaUIForSponsor").InnerText.Split(',').ToArray();
                        containsSponsorID = sponsorIds.Contains(primarySponsorID.ToString());
                        return containsSponsorID;

                    }
                    else
                    {
                        return false;
                    }
                }
                catch (InvalidCastException e)
                {
                    return false;
                }

            }
        }


        public bool EnableNewLanding
        {
            get
            {
                if (VhmSession == null || string.IsNullOrEmpty(VhmSession.SessionID))
                {
                    return false;
                }
                var httpCookie = Request.Cookies["NewLanding" + VhmSession.MemberID.ToString()];
                if (SponsorProfile.GenesisUI == true)
                {
                    if (httpCookie != null)
                    {
                        if (httpCookie.Value == "no")
                        {
                            httpCookie.Value = "yes";
                        }
                    }
                    else
                    {
                        DateTime dt = new DateTime();
                        var oCookie = new HttpCookie("NewLanding" + VhmSession.MemberID.ToString());
                        oCookie.Value = "yes";
                        oCookie.Expires = DateTime.MaxValue;
                        Response.Cookies.Add(oCookie);
                    }
                    return true;
                }
                _showModalForNewLandingPage = false;


                bool useNewLandingPage = false;
                try
                {
                    string xmlPathName = ConfigurationManager.AppSettings["BetaUISponsorPath"];
                    XmlDocument betaUIList = new XmlDocument();
                    long primarySponsorID = VhmSession.PrimarySponsorID;
                    bool containsSponsorID = false;
                    var xmlFilePath = string.Format(@"{0}\BetaUiConfig\" + xmlPathName, Request.PhysicalApplicationPath);
                    betaUIList.Load(xmlFilePath);

                    if (!String.IsNullOrEmpty(betaUIList.SelectSingleNode("BetaUIForSponsor").InnerText))
                    {
                        string[] sponsorIds = betaUIList.SelectSingleNode("BetaUIForSponsor").InnerText.Split(',').ToArray();
                        containsSponsorID = sponsorIds.Contains(primarySponsorID.ToString());
                        useNewLandingPage = containsSponsorID;

                    }
                }
                catch (InvalidCastException e)
                {
                }


                if (useNewLandingPage == false)
                {
                    return false;
                }
                if (httpCookie != null && !string.IsNullOrEmpty(httpCookie.Value) && useNewLandingPage)
                {
                    if (httpCookie.Value == "yes")
                    {
                        return true;
                    }
                    return false;
                }
                _showModalForNewLandingPage = true;
                return false;
            }
        }

        [ErrorHandle(Action = "Error")]
        public ActionResult LandingPage()
        {
            var terms = VhmSession.TermsConditions;

            var member = MemberBL.GetMemberById(VhmSession.SessionID, VhmSession.MemberID);
            CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.Member, member);

            var memberType = TYPE_MEMEBER;
            if (CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.CheckMember) != null)
            {
                memberType = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.CheckMember).ToString();
            }

            if ((memberType == TYPE_MEMEBER && terms == 0) || VhmSession.MemberStatus.ToUpper() != CRITERIA_ACT || VhmSession.PrimarySponsorID == 0)
            {
                ShowPopForMemebershipStatusChanged();
                ShowPopForCreditCardInfo();
                ShowPopForBillingInfo();
                if (File2Redirect(CRITERIA_TRM))
                {
                    ActionResult result = ShowPopForTermsPage();
                    if (result != null)
                    {
                        return result;
                    }
                    result = ShowPopForTermsToAcknowledge();
                    if (result != null)
                    {
                        return result;
                    }
                }
                CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.SessionCheckTimestamp, null);
            }

            #region app settings
            var maxRewards = ApplicationSettings.MaxRecentlyEarnedRewardsToShow;
            var maxSummaryRewards = ApplicationSettings.MaxRecentlyEarnedRewardsSummaryToShow;
            var graphHeight = ApplicationSettings.ActivityGraphHeight;
            var activityDaysToShow = ApplicationSettings.ActivityDaysToShow;
            var stepsTargetDefault = ApplicationSettings.StepsTargetDefault;
            var challengeDisplayDays = ApplicationSettings.ChallengeEndedDisplayDays;
            var moduleImageUrl = ApplicationSettings.ModuleImageUrl;
            var imageCDN = ApplicationSettings.SiteImageUrl;
            var badgeImageUrl = ApplicationSettings.BadgeImageUrl;
            var rewardTypeImageUrl = ApplicationSettings.RewardTypeImageUrl;
            #endregion

            var modelErrors = new Dictionary<LandingPageVM.LandingPageModelErrors, string>();
            #region Recently Earned Reward
            //only do this if this is not a cancelled member
            var lastRewardsEntryDate = new DateTime();
            var cookieSet = false;
            if (VhmSession.PrimarySponsorID > 0 && VhmSession.MemberID > 0)
            {
                long? lastRewardsEntryDateTicks = GetRecentlyEarnedRewardsDateFromCookie();
                cookieSet = lastRewardsEntryDateTicks.HasValue;
                lastRewardsEntryDate = new DateTime(lastRewardsEntryDateTicks.GetValueOrDefault());
            }

            lastRewardsEntryDate = cookieSet
                                       ? lastRewardsEntryDate
                                       : member.LastLoginDate;


            #region Get recent level badge

            var recentLevelRewards = MemberBL.GetMemberBadges(VhmSession.MemberID, null, true);
            MemberBadge recentLevel = recentLevelRewards.LastOrDefault(b => b.BadgeCategoryCode.Equals("LEV"));

            #endregion


            #region Challenges

            var challenges = new List<ChallengeBase>();
            try
            {
                var tmpChallenges = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.MemberChallenges) as List<ChallengeBase>;
                if (tmpChallenges == null)
                {
                    tmpChallenges = MemberBL.GetMemberChallenges(VhmSession.MemberID, VhmSession.PrimarySponsorID, challengeDisplayDays, VhmSession.LanguageID);
                    var corpChallenges = new List<ChallengeBase>();
                    corpChallenges.AddRange(tmpChallenges);

                    CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.CorporateChallenges, corpChallenges);
                    var test =
                   CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.CorporateChallenges)
                       as List<ChallengeBase>;
                    // remove corporate tracking challenges for which the user has not signed up
                    tmpChallenges.RemoveAll(c => c.ShowInBrowseOnly && !c.IsMemberEnrolled);
                    CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.MemberChallenges, tmpChallenges);
                }
                challenges.AddRange(tmpChallenges);

                // Don't display an KAG which has ended.
                challenges.RemoveAll(c => c is Kag && ((Kag)c).Tasks.All(t => t.TaskEndDate <= DateTime.Now));

                // remove all 'browse only' challenge the member is not enrolled in
                challenges.RemoveAll(c => c.ShowInBrowseOnly && !c.IsMemberEnrolled);
            }
            catch (Exception ex)
            {
                ExceptionHandler.ProcessException(ex);
                modelErrors.Add(LandingPageVM.LandingPageModelErrors.Challenges, ex.Message);
            }

            #endregion

            #region Modules

            var modules = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.MemberModules) as List<MemberModule>;
            if (modules == null)
            {
                modules = MemberBL.GetMemberModules(VhmSession.MemberID, VhmSession.PrimarySponsorID);
                CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.MemberModules, modules);
            }

            #endregion

            #region Nutrition Data

            bool nutritionEnabled = modules.Any(n => n.ModuleTypeCode == "NTR");
            if (nutritionEnabled)
            {
                if (Session["NutRefreshStarted"] == null)
                {
                    Session["NutRefreshStarted"] = true;
                    Response.Cookies.Add(new HttpCookie("NUTRefresh", "1") { Path = "/" }); // gets cleared once refresh completes
                }
            }

            #endregion

            #region Self Tracking Challenge Data

            bool selfTrackingChallengeEnabled = modules.Any(n => n.ModuleTypeCode == "TRK");

            #endregion

            #region Activity Data
            var activityData = new ActivityGraphData();
            bool showActivity = false;
            bool showPickDevicePrompt = false;
            string htmlPickDevicePrompt = null;
            if (modules.Any(m => m.ModuleTypeCode == "VAC"))
            {
                showActivity = true;
                try
                {
                    activityData = MemberBL.GetMemberActivityGraphData(VhmSession.MemberID, graphHeight,
                                                                         activityDaysToShow, stepsTargetDefault);
                }
                catch (Exception ex)
                {
                    ExceptionHandler.ProcessException(ex);

                    modelErrors.Add(LandingPageVM.LandingPageModelErrors.Activity, ex.Message);
                }

                #region enable 'Pick a device' prompt if GZ has not been ordered and sponsor gozone ordering type is Included After Enrollment
                if (!Request.Cookies.AllKeys.Contains(GZ_PICK_DEVICE_HIDE_COOKIE + VhmSession.MemberID)) // !isHiddenByUser
                {
                    if (modules.Any(m => m.ModuleTypeCode == "VAC" && m.DeviceOrderTypeCode == "OPT")) // isInclAfterEnroll
                    {
                        var goZoneVersions = SponsorBL.GetSponsorGZVersions(VhmSession.PrimarySponsorID);
                        if (goZoneVersions.Any(v => DateTime.Now >= v.DateFrom && v.ProductCode.StartsWith("MAX3.") && v.Version.StartsWith("3."))) // isMAXAvailable
                        {
                            bool hasAnyDevice = MemberBL.CheckForRegisteredDevice(VhmSession.MemberID); // don't show if they've registered the MAX
                            var salesOrderItems = MemberBL.GetMemberDeviceOrders(VhmSession.MemberID);
                            showPickDevicePrompt = !salesOrderItems.Any(o => o.ProductCode.StartsWith("MAX3.")) && !hasAnyDevice; // !hasOrdered
                        }
                    }
                }
                #endregion
            }

            var showSelfEnteredActivity = modules.Any(m => m.ModuleTypeCode == "SEA");

            #endregion

            #region Badges Enabled
            var badgesEnabled = false;
            try
            {
                badgesEnabled = MemberBL.BadgesEnabledForSponsor(VhmSession.PrimarySponsorID);
            }
            catch (Exception ex)
            {
                ExceptionHandler.ProcessException(ex);
                modelErrors.Add(LandingPageVM.LandingPageModelErrors.Badges, ex.Message);
            }
            #endregion

            #region Ticker
            var tickersData = GetTicker();
            #endregion
            //CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.MemberModules, modules);
            #region Check if member has any Programs modules available

            var programModules = (from m in modules
                                  where m.ModuleID > PROGRAMS_MODULE_ID
                                  select m).ToList();
            bool hasProgramModulesAvailable = (programModules.Any());
            var partnerModules = new List<PartnerModule>();
            try
            {
                if (hasProgramModulesAvailable)
                {
                    partnerModules.AddRange(from memberModule in programModules
                                            where memberModule.PartnerGUID != Guid.Empty && !string.IsNullOrEmpty(memberModule.MaskModuleName)
                                            let imgGuid = new Guid(memberModule.ModuleImage)
                                            select new PartnerModule
                                            {
                                                PartnerGUID = memberModule.PartnerGUID,
                                                MaskModuleName = memberModule.MaskModuleName,
                                                IsPartner = memberModule.IsPartner,
                                                URL = memberModule.URL,
                                                ModuleImageSrc =
                                                    string.Format("{0}{1}.png",
                                                                   moduleImageUrl, imgGuid),
                                                SponsorModuleId = memberModule.SponsorModuleId
                                            });
                }
                if (partnerModules.Count == 0)
                {
                    hasProgramModulesAvailable = false;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.ProcessException(ex);
                modelErrors.Add(LandingPageVM.LandingPageModelErrors.Modules, ex.Message);
            }

            #endregion

            #region Measurements

            var measurmentsModel = new MeasurmentSummaryVM();
            bool showMeasurements = false;
            if (modules.Any(m => m.ModuleTypeCode == "VBI" || m.ModuleTypeCode == "SEB"))
            {
                showMeasurements = true;

                const int MAX_RESULTS = 1;
                var reward = MeasurementsBL.GetMemberLatestMeasurementSummary(VhmSession.MemberID, MAX_RESULTS);
                DateTime? mostRecentMeasurement = null;
                if (reward != null)
                {
                    mostRecentMeasurement = reward.MeasurementDate;
                }

                measurmentsModel = new MeasurmentSummaryVM
                {
                    Reward = reward,
                    LastMeasurementDate = mostRecentMeasurement,
                    HasHZDesktopAccess = Request.Cookies["HZSettings"] != null,
                    AwardNumberFormat = (reward != null && reward.Cash) ? "C0" : "N0",
                    RewardEarnedThisMonth = mostRecentMeasurement.GetValueOrDefault().Month == DateTime.Now.Month
                };
            }
            #endregion

            #region determine a culture. If VhmSession.Culture is null, then get member's browser culture
            CultureInfo memberCulture;
            SponsorProfile = SponsorProfile ?? GetSponsorProfile();
            CultureInfo sponsorCulture = string.IsNullOrEmpty(SponsorProfile.CultureCode)
                                             ? new CultureInfo("en-US")
                                             : new CultureInfo(SponsorProfile.CultureCode);
            if (!string.IsNullOrEmpty(VhmSession.Culture))
            {
                memberCulture = new CultureInfo(VhmSession.Culture);
            }
            else
            {
                //get browser settings culture
                var l = Request.UserLanguages;
                string browserlanguage = (l != null && l[0] != null) ? l[0] : DEFAULT_CULTURE;
                Thread.CurrentThread.CurrentCulture = new CultureInfo(browserlanguage);
                memberCulture = Thread.CurrentThread.CurrentCulture;
            }
            #endregion

            var selectedColorTheme = GetSelectedTheme();

            const string LANDING_PAGE_TITLE = "Landing Page";

            var game = RewardsBL.GetSponsorCurrentGame(VhmSession.SessionID, member, VhmSession.PrimarySponsorID);
            const bool MEASUREMENT_REWARDS_ONLY = false;
            List<RecentlyEarnedRewardsItem> recentlyEarnedRewards = MemberBL.GetRecentlyEarnedRewards(VhmSession.MemberID, maxRewards, MEASUREMENT_REWARDS_ONLY, false);
            List<RecentlyEarnedRewardsItem> recentlyEarnedBadges = FilterOutRecentlyEarnedBadges(maxRewards, modelErrors, MEASUREMENT_REWARDS_ONLY);

            #endregion

            var popupRewards = recentlyEarnedRewards.Where(r => (r.RewardType.ToLower() == "healthmiles" || r.RewardType.ToLower() == "entries") && r.EntryDate > lastRewardsEntryDate).ToList();
            var recentRewardsNoBadges = recentlyEarnedRewards.Where(r => (r.RewardType.ToLower() == "healthmiles" || r.RewardType.ToLower() == "entries" || r.RewardType.ToLower() == "text only")).ToList();

            var model = new LandingPageVM
            {
                MemberId = VhmSession.MemberID,
                MemberGender = string.IsNullOrEmpty(member.Gender) ? Char.Parse("") : member.Gender[0],
                PageTitle = new PageTitle(false, LANDING_PAGE_TITLE),
                MaxSummaryRewards = maxSummaryRewards,
                RecentlyEarnedRewards = (recentlyEarnedBadges != null && recentlyEarnedBadges.Count > 0) ? recentlyEarnedBadges : recentlyEarnedRewards,
                PopupRewards = popupRewards,
                Challenges = challenges,
                ChallengeEndedDisplayDays = challengeDisplayDays,
                ActivityData = activityData,
                SelfEnteredActivityEnabled = showSelfEnteredActivity,
                BadgesEnabled = badgesEnabled,
                NutritionEnabled = nutritionEnabled,
                SelfTrackingChallengesEnabled = selfTrackingChallengeEnabled,
                MeasuermentsSummary = measurmentsModel,
                CurrentCulture = memberCulture,
                CurrencyCulture = sponsorCulture,
                Modules = modules,
                ShowWelcomeJourney = false,
                WelcomeJourneyStep = -1,
                WelcomeJourneyTitle = string.Empty,
                MaxWelcomeJourneySteps = 4,
                HRASetting = -1,
                ConnectionsEnabled = VhmSession.CommunityOn == 1,
                ImageCDN = imageCDN,
                BadgeImageUrl = badgeImageUrl,
                RewardTypeImageUrl = rewardTypeImageUrl,
                ShowPopUpForMemebershipStatusChanged = _showPopUpForMemebershipStatusChanged,
                ShowPopUpForCreditCardInfo = _showPopUpForCreditCardInfo,
                ShowPopUpForBillingInfo = _showPopUpForBillingInfo,
                ShowPopUpForTermsPage = _showPopUpForTermsPage,
                ShowPopUpForTermsToAcknowledge = _showPopUpForTermsToAcknowledge,
                TermsId = _termsId,
                Title = _title,
                HasProgramsModules = hasProgramModulesAvailable,
                PartnerModules = partnerModules,
                MeasurementsEnabled = showMeasurements,
                ActivityEnabled = showActivity,
                ShowPickDevicePrompt = showPickDevicePrompt,
                TickersData = tickersData,
                ThemeCode = selectedColorTheme,
                PersonalChallengesEnabled = SponsorProfile.AllowPersonalChallengeAccess,
                RewardDisplay = game != null ? game.GameDisplayType : "No Game",
                ProgramType = game != null ? game.ProgramType : "None",
                genesisUI = SponsorProfile.GenesisUI,
                RecentlyEarnedRewardsNoBadges = recentRewardsNoBadges,
                LevelCelebration = recentLevel
            };

            model.ActivityTargetPosition = graphHeight > 0
                ? ((int)model.ActivityData.Target / graphHeight) * 100
                : 100;

            // map challenge links to the challenge types
            model.ChallengeTypes = new Dictionary<string, string>
                                       {
                                           {
                                               ChallengeBase.CHALLENGE_TYPE_KAG,
                                               "/secure/challenge/SEChallenge.aspx?secId={0}"
                                               },
                                           {
                                               ChallengeBase.CHALLENGE_TYPE_PROMO,
                                               "/secure/challenge/promo.aspx?promoid={0}"
                                               },
                                           {
                                               ChallengeBase.CHALLENGE_TYPE_QUEST,
                                               "/secure/quest/questregistration.aspx?quest_id={0}"
                                               },
                                           {
                                               ChallengeBase.CHALLENGE_TYPE_CORP,
                                               "/secure/challenge/rankings.aspx?challengeid={0}"
                                               },
                                           {
                                               ChallengeBase.CHALLENGE_TYPE_CORP_SIGN_UP,
                                               "/secure/challenge/invitation.aspx?challengeid={0}"
                                               },
                                               {
                                               ChallengeBase.CHALLENGE_TYPE_TRK,
                                               "/challenge/{0}"
                                               }
                                       };
            var maxEntryDate = DateTime.MinValue;
            if (recentlyEarnedRewards.Count > 0)
            {
                maxEntryDate = recentlyEarnedRewards.Max(r => r.EntryDate.GetValueOrDefault());
            }
            //do not set Recently earned cookie if member is cancelled
            if (VhmSession.PrimarySponsorID > 0 && VhmSession.MemberID > 0)
            {
                SetRecentlyEarnedRewardsCookie(maxEntryDate);
            }
            //TODO move the WJ region in a private method
            #region Welcome Journey - New Members

            var isNewMember = VhmSession.Membership == 1;

            // to check if member is NewMember we need to also check the CacheProvider.CacheItems.FinishedWelcomeJourney value. 
            // This is set only after WJ is finished, as now we're using nCache and oMembership["Value"] is not updated 
            // unless member actualy logs off and then logs back in. We only need to check the value if (oMembership["Value"]) != "0" 
            // as otherwise the member is not a new member anymore.
            var checkIfFinishedWJ = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.FinishedWelcomeJourney);
            if (checkIfFinishedWJ != null)
            {
                isNewMember = false;
            }



            bool isExpress = game != null && game.ProgramType == "EXP";// VhmSession.PrimarySponsorID == expressSponsorId;
            bool isExpressNewMember = isNewMember && isExpress;

            if (game != null && !String.IsNullOrEmpty(game.ProgramType))
            {
                if (isExpress)
                {
                    var newLandingCookie = new HttpCookie("NewLanding" + VhmSession.MemberID.ToString(CultureInfo.InvariantCulture)) { Value = "yes" };
                    newLandingCookie.Expires = DateTime.MaxValue;
                    Response.Cookies.Add(newLandingCookie);
                }
            }

            if (isExpressNewMember)
            {
                var newLandingCookie = new HttpCookie("NewLanding" + VhmSession.MemberID.ToString(CultureInfo.InvariantCulture)) { Value = "yes" };
                newLandingCookie.Expires = DateTime.MaxValue;
                Response.Cookies.Add(newLandingCookie);

                model.ShowExpressWelcomeJourney = true;
            }
            else if (isNewMember)
            {
                //try to get the HSCompletion
                if (modules.Count(m => m.ModuleTypeCode == "HRA") > 0)
                {
                    var hraSSetting = modules.First(m => m.ModuleTypeCode == "HRA").HRASetting;
                    if (hraSSetting >= 0)
                    {
                        model.HRASetting = Convert.ToInt16(hraSSetting);
                    }
                }

                model.ShowWelcomeJourney = !isExpress; // only show the 'old' welcome journey to non-express members

                var currentStep = MemberBL.GetMemberWelcomeJourneyCurrentStep(VhmSession.MemberID);
                if (currentStep != null && currentStep > 1)
                {
                    model.WelcomeJourneyStep = currentStep;
                }
                else
                {
                    model.WelcomeJourneyStep = 1;
                    //if currentStep is null, 0 or 1 and isNewMember = true - this is a step 1 member
                }
                //set the Welcome jounrey's dialog title

                #region need to find out what social tier member is part of,

                var communityType = VhmSession.CommunityType;
                //we can have up to 5 WJ steps: 1 - culture format; 2 - height/weight; 3 - social message from sponsor; 
                // 4 - member priv settings; 5 - HRA info
                var maxStepNumber = 5;
                if (communityType == CommunityType.COMMUNITY_TYPE_CLOSED)
                {
                    //for closed commumities we're not showing step 4 - member priv settings
                    maxStepNumber = maxStepNumber - 1;
                }
                // -1 = "Off", 0 = "Optional, not prompted", 1 = "Optional, but prompted", 
                // 2 = "Mandatory, after Welcome Screens", 3 = "Mandatory, before Welcome Screens"; 
                // except now -1 doesn't exist anymore
                if (model.HRASetting != 1 && model.HRASetting != 2)
                {
                    //for sponsors that don't have a Mandatory Health Snapshot
                    maxStepNumber = maxStepNumber - 1;
                }
                model.MaxWelcomeJourneySteps = maxStepNumber;

                #endregion
            }

            #endregion

            bool expressUser = false;
            if (game != null && game.ProgramType == "EXP")
                expressUser = true;

            if (EnableNewLanding || expressUser)
            {
                model.ShowNewLandingModal = _showModalForNewLandingPage;
                return View("LandingPageV3/MainView", model);
            }
            model.ShowNewLandingModal = _showModalForNewLandingPage;
            bool enabledBetaUI = _betaEnabledForSponsor;
            model.BetaEnabled = enabledBetaUI;
            return View(model);
        }

        public PartialViewResult LandingPageSectionError()
        {
            return PartialView("_LandingPageSectionError");
        }

        #region Redirects
        // Got the logic from MemeberMaster.cs in release project Line 142
        private void ShowPopForMemebershipStatusChanged()
        {
            _showPopUpForMemebershipStatusChanged = VhmSession.IWPMemberSince == string.Empty && File2Redirect("UPG");
        }
        // Got the logic from MemeberMaster.cs in release project Line 160/170
        private void ShowPopForCreditCardInfo()
        {
            //if member has a status of pending display CC declined message and 'freeze' site unless on update CC page
            _showPopUpForCreditCardInfo = VhmSession.MemberStatus.ToUpper() == STATUS_LOC && File2Redirect(CRITERIA_UCC);
        }

        // Got the logic from MemeberMaster.cs in release project Line 160/178
        private void ShowPopForBillingInfo()
        {
            //if member has a status of new display enter CC message and 'freeze' site unless on update CC page
            _showPopUpForBillingInfo = VhmSession.MemberStatus.ToUpper() == STATUS_NEW && File2Redirect(CRITERIA_UCC);
        }

        // Got the logic from MemeberMaster.cs in release project Line 188
        private ActionResult ShowPopForTermsPage()
        {
            //check member has seen latest version and popup is not already in use (i.e. for cancelled or pen or new member status)

            var termsAndConditions = MemberBL.GetTermsAndConditions(VhmSession.MemberID);
            var termsAndCondition = termsAndConditions.FirstOrDefault();
            if (termsAndCondition == null)
            {
                return null;
            }
            var terms = MemberBL.GetTerms(VhmSession.MemberID, termsAndCondition.TermsCode, termsAndCondition.Version);
            var term = terms.FirstOrDefault();
            if (term != null && (term.PreviousVersion != null && term.PreviousVersion != term.Version) && (!_showPopUpForBillingInfo && !_showPopUpForCreditCardInfo && !_showPopUpForMemebershipStatusChanged))
            {
                _title = term.Title;
                _termsCode = term.TermsCode;
                _termsId = term.TermsId;
                _showPopUpForTermsPage = true;
                var accepted = term.Accepted ?? false;
                MemberBL.UpdateAcknowledgeMemberTerms(VhmSession.SessionID, ApplicationSettings.TimeoutInterval, _termsCode, accepted, CALLING_SOURCE, VhmSession.MemberID);

            }
            else
            {
                if (term != null)
                {
                    return new RedirectResult(
                        string.Format(ApplicationSettings.BaseURL + "/secure/member/requiredterms.aspx?termscode={0}&version={1}",
                                      HttpUtility.UrlEncode(term.TermsCode),
                                      term.Version), false);
                }
            }
            return null;
        }

        // Got the logic from MemeberMaster.cs in release project Line 220
        private ActionResult ShowPopForTermsToAcknowledge()
        {
            var termsToAcknowledges = MemberBL.GetTermsToAcknowledge(VhmSession.MemberID);
            var termsToAcknowledge = termsToAcknowledges.FirstOrDefault();
            if (termsToAcknowledge == null)
            {
                return null;
            }

            if (termsToAcknowledge.PreviousVersion != null && termsToAcknowledge.PreviousVersion != termsToAcknowledge.Version)
            {
                var newTerms = MemberBL.GetTerms(VhmSession.MemberID, termsToAcknowledge.TermsCode, termsToAcknowledge.Version);
                var newTerm = newTerms.FirstOrDefault();
                if (newTerm != null)
                {
                    _title = newTerm.Title;
                    _termsCode = newTerm.TermsCode;
                    _termsId = newTerm.TermsId;
                    var accepted = newTerm.Accepted ?? false;
                    _showPopUpForTermsToAcknowledge = true;
                    MemberBL.UpdateAcknowledgeMemberTerms(VhmSession.SessionID, 30, _termsCode, accepted, CALLING_SOURCE, VhmSession.MemberID);
                }
            }
            else
            {
                return new RedirectResult(
                           string.Format(ApplicationSettings.BaseURL + "/secure/member/optionalterms.aspx?termscode={0}&version={1}",
                                          HttpUtility.UrlEncode(termsToAcknowledge.TermsCode),
                                         termsToAcknowledge.Version), false);
            }

            return null;
        }

        private Dictionary<string, List<string>> CompletionCheckList
        {
            get
            {//TODO: Theses URLs have to move to webconfig
                var completionCheckList = new Dictionary<string, List<string>>
                                              {
                                                  {
                                                      "TRM",
                                                      new List<string>
                                                          {
                                                              ApplicationSettings.BaseURL+ "/secure/member/requiredterms.aspx",
                                                               ApplicationSettings.BaseURL+"/secure/member/optionalterms.aspx",
                                                               ApplicationSettings.BaseURL+"/promotional/terms.aspx",
                                                              ApplicationSettings.BaseURL+ "/secure/fitnessassessment/fareport.aspx",
                                                               ApplicationSettings.BaseURL+"/secure/member/updatecc.aspx",
                                                               ApplicationSettings.BaseURL+"/secure/member/upgrade.aspx"
                                                          }
                                                      },
                                                  {
                                                      "REG",
                                                    new  List<string>
                                                          {
                                                              ApplicationSettings.BaseURL+"v2/member/landingpage",
                                                               ApplicationSettings.BaseURL+"/secure/member/requiredterms.aspx",
                                                               ApplicationSettings.BaseURL+"/secure/member/optionalterms.aspx",
                                                               ApplicationSettings.BaseURL+"/secure/member/terms.aspx", 
                                                               ApplicationSettings.BaseURL+"/secure/member/updatecc.aspx",
                                                               ApplicationSettings.BaseURL+"/secure/member/upgrade.aspx"
                                                          }
                                                      },
                                                  {
                                                      "HRA",
                                                      new List<string>
                                                          {
                                                               ApplicationSettings.BaseURL+"/secure/member/memberdetails.aspx",
                                                               ApplicationSettings.BaseURL+"/secure/healthsnapshot/hrassessment.aspx",
                                                               ApplicationSettings.BaseURL+"/secure/healthsnapshot/medical.aspx",
                                                               ApplicationSettings.BaseURL+"/secure/healthsnapshot/lifestyle.aspx",
                                                              ApplicationSettings.BaseURL+ "/secure/healthsnapshot/physical.aspx",
                                                               ApplicationSettings.BaseURL+"/secure/healthsnapshot/nutrition.aspx",
                                                               ApplicationSettings.BaseURL+"/secure/healthsnapshot/quality.aspx",
                                                               ApplicationSettings.BaseURL+"/secure/healthsnapshot/goals.aspx",
                                                               ApplicationSettings.BaseURL+"/secure/member/requiredterms.aspx",
                                                               ApplicationSettings.BaseURL+"/secure/member/optionalterms.aspx",
                                                               ApplicationSettings.BaseURL+"/secure/member/terms.aspx", 
                                                                ApplicationSettings.BaseURL+"/secure/member/updatecc.aspx"
                                                          }
                                                      },
                                                  { "UCC", new List<string> {  ApplicationSettings.BaseURL+"/secure/member/updatecc.aspx" } },
                                                  { "UPG", new List<string> {  ApplicationSettings.BaseURL+"/secure/member/upgrade.aspx" } }
                                              };

                return completionCheckList;
            }
        }

        private bool File2Redirect(string criteria)
        {
            return CompletionCheckList[criteria].All(fileName => Request.FilePath.Trim().ToLower().IndexOf(fileName, StringComparison.Ordinal) < 0);
        }

        #endregion
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SavePrivateSettings(Member member)
        {
            var updatedProfile = member;
            long memberId = VhmSession.MemberID;
            string sessionId = VhmSession.SessionID;

            MemberVM currentSettings = SocialBL.GetMemberProfile(sessionId, memberId, memberId, false);

            currentSettings.Member.ShowNickname = updatedProfile.ShowNickname;
            currentSettings.Member.ShowGender = updatedProfile.ShowGender;
            currentSettings.Member.ShowAge = updatedProfile.ShowAge;
            currentSettings.Member.ShowCity = updatedProfile.ShowCity;
            currentSettings.Member.ShowState = updatedProfile.ShowState;
            currentSettings.Member.ShowTotalSteps = updatedProfile.ShowTotalSteps;
            currentSettings.Member.ShowStepsAvg = updatedProfile.ShowStepsAvg;
            currentSettings.Member.ShowLastGZUpload = updatedProfile.ShowLastGZUpload;
            currentSettings.Member.ShowPoints = updatedProfile.ShowPoints;
            currentSettings.Member.ShowAnniversary = updatedProfile.ShowAnniversary;
            currentSettings.Member.ShowChallengeHistory = updatedProfile.ShowChallengeHistory;
            currentSettings.Member.ShowSponsors = updatedProfile.ShowSponsors;
            currentSettings.Member.ShowGoals = updatedProfile.ShowGoals;
            currentSettings.Member.ShowOccupation = updatedProfile.ShowOccupation;
            currentSettings.Member.ShowInterests = updatedProfile.ShowInterests;
            currentSettings.Member.PrivacySettingsSet = true;
            currentSettings.Member.ProfileExists = 1;
            currentSettings.Member.ShowBadges = updatedProfile.ShowBadges;
            currentSettings.Member.Privacy = updatedProfile.Privacy;

            bool success = MemberBL.SaveMemberPrivateSettings(sessionId, currentSettings.Member, false);
            string successString = "{ Success = " + success + "}";
            return Json(successString);
        }

        public JsonResult SaveKagEntry(long promoId, decimal score, DateTime date)
        {
            var memberId = VhmSession.MemberID;
            MemberBL.SaveKagEntry(promoId, memberId, score, date);

            return Json(new { Success = true });
        }

        public JsonResult GetMemberCurrentTime()
        {
            var memberTime = MemberBL.GetMemberTimeByUtcTime(VhmSession.MemberID, DateTime.UtcNow);
            return Json(JsonConvert.SerializeObject(new { MemberTime = memberTime }), JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult KAGComplete(KAGCompleteVM kagCompleted)
        {
            var model = kagCompleted;
            model.RewardImageUrl = ApplicationSettings.RewardTypeImageUrl;
            return PartialView("PartialViews/_KAGComplete", model);
        }

        public PartialViewResult UpForGrabs()
        {
            var upForGrabs = RewardsBL.GetMemberAvailableUpForGrabs(VhmSession.MemberID, VhmSession.PrimarySponsorID);

            // put the badges on the bottom of the list for each up for grabs item
            upForGrabs.ForEach(ufg => ufg.Rewards = ufg.Rewards.OrderBy(r => r.BadgeId).ToList());

            var imageCDN = ApplicationSettings.SiteImageUrl;

            var badgeImageUrl = ApplicationSettings.BadgeImageUrl;

            var rewardTypeImageUrl = ApplicationSettings.RewardTypeImageUrl;

            SponsorProfile = SponsorProfile ?? GetSponsorProfile();
            CultureInfo sponsorCulture = string.IsNullOrEmpty(SponsorProfile.CultureCode)
                                             ? new CultureInfo("en-US")
                                             : new CultureInfo(SponsorProfile.CultureCode);

            var model = new UpForGrabsVM
            {
                ImageCDN = imageCDN,
                BadgeImageUrl = badgeImageUrl,
                RewardTypeImageUrl = rewardTypeImageUrl,
                Rewards = upForGrabs,
                CurrencyCulture = sponsorCulture
            };
            return PartialView("_UpForGrabs", model);
        }

        public JsonResult WhatsNextForMe()
        {
            var upForGrabs = RewardsBL.GetMemberAvailableUpForGrabs(VhmSession.MemberID, VhmSession.PrimarySponsorID);

            // put the badges on the bottom of the list for each up for grabs item
            upForGrabs.ForEach(ufg => ufg.Rewards = ufg.Rewards.OrderBy(r => r.BadgeId).ToList());

            var imageCDN = ApplicationSettings.SiteImageUrl;

            var badgeImageUrl = ApplicationSettings.BadgeImageUrl;

            var rewardTypeImageUrl = ApplicationSettings.RewardTypeImageUrl;

            SponsorProfile = SponsorProfile ?? GetSponsorProfile();
            CultureInfo sponsorCulture = string.IsNullOrEmpty(SponsorProfile.CultureCode)
                                             ? new CultureInfo("en-US")
                                             : new CultureInfo(SponsorProfile.CultureCode);

            var model = new UpForGrabsVM
            {
                ImageCDN = imageCDN,
                BadgeImageUrl = badgeImageUrl,
                RewardTypeImageUrl = rewardTypeImageUrl,
                Rewards = upForGrabs,
                CurrencyCulture = sponsorCulture
            };

            return Json(JsonConvert.SerializeObject(model), JsonRequestBehavior.AllowGet);
        }

        #region Welcome Journey Steps
        public PartialViewResult ExpressWelcomeJourney()
        {
            var modelWJTwo = new WelcomeJourneyStepTwo();
            var cultureInfo = MemberBL.GetCultureFormats().Find(c => c.CultureCode == "en-US");
            modelWJTwo.CultureId = cultureInfo.CultureID;
            modelWJTwo.CultureInfo = cultureInfo.SpecCultureCode;
            modelWJTwo.MeasurementSystem = cultureInfo.MeasureSystem;
            modelWJTwo.WJCurrentStep = 1;
            modelWJTwo.WJMaxNumber = 2;
            modelWJTwo.ContentMeasurementLabels =
                        MemberBL.GetContentLabels<MeasurementLabelsContent>(PageNames.MeasurementLabels,
                                                                             LanguageId);
            modelWJTwo.MiscContent =
                        MemberBL.GetContentLabels<MiscellaneousLabelsContent>(PageNames.MiscellaneousLabels,
                                                                               LanguageId);
            return PartialView("_WelcomeJourneyExpress", modelWJTwo);
        }

        public PartialViewResult WelcomeJourneyV3()
        {
            var modelWJTwo = new WelcomeJourneyStepTwo();
            var cultureInfo = MemberBL.GetCultureFormats().Find(c => c.CultureCode == "en-US");
            modelWJTwo.CultureId = cultureInfo.CultureID;
            modelWJTwo.CultureInfo = cultureInfo.SpecCultureCode;
            modelWJTwo.MeasurementSystem = cultureInfo.MeasureSystem;
            modelWJTwo.WJCurrentStep = 1;
            modelWJTwo.WJMaxNumber = 2;
            return PartialView("_WelcomeJourneyV3", modelWJTwo);
        }

        public PartialViewResult WelcomeJourney(int stepNo, int maxStepNumber)
        {
            //all the logic regarding the curent step goes here... 
            //check if community is closed and open step 5 if so
            if (stepNo == 4)
            {
                if (VhmSession.CommunityType == CommunityType.COMMUNITY_TYPE_CLOSED)
                {
                    stepNo = 5;
                }
            }
            switch (stepNo)
            {
                default:
                    #region step 1 - enter culture format preferences
                    var model = new WelcomeJourneyStepOne
                    {
                        SponsorLanguages = SponsorBL.GetSponsorLanguages(VhmSession.PrimarySponsorID),
                        CultureFormats = MemberBL.GetCultureFormats(),
                        WJCurrentStep = 1,
                        WJMaxNumber = maxStepNumber,
                        MiscContent = MemberBL.GetContentLabels<MiscellaneousLabelsContent>(PageNames.MiscellaneousLabels, LanguageId),
                        LanguageId = LanguageId
                    };

                    return PartialView("_WelcomeJourneyStepOne", model);
                    #endregion
                case 2:
                    #region Step 2 - Enter Measurements - weight and height
                    var modelWJTwo = new WelcomeJourneyStepTwo();
                    var cultureInfo = MemberBL.GetCultureFormats().Find(c => c.CultureCode == VhmSession.Culture);
                    modelWJTwo.CultureId = cultureInfo.CultureID;
                    modelWJTwo.CultureInfo = cultureInfo.SpecCultureCode;
                    modelWJTwo.MeasurementSystem = cultureInfo.MeasureSystem;
                    modelWJTwo.WJCurrentStep = 2;
                    modelWJTwo.WJMaxNumber = maxStepNumber;
                    modelWJTwo.ContentMeasurementLabels =
                                MemberBL.GetContentLabels<MeasurementLabelsContent>(PageNames.MeasurementLabels,
                                                                                     LanguageId);
                    modelWJTwo.MiscContent =
                                MemberBL.GetContentLabels<MiscellaneousLabelsContent>(PageNames.MiscellaneousLabels,
                                                                                       LanguageId);
                    return PartialView("_WelcomeJourneyStepTwo", modelWJTwo);
                    #endregion
                case 3:
                    #region Step 3 - Sponsor Content managed area - message about community
                    var wjStepThree = new WelcomeJourneyStepThree
                    {
                        CommunityType = VhmSession.CommunityType,
                        WJCurrentStep = 3,
                        WJMaxNumber = maxStepNumber,
                        MiscContent = MemberBL.GetContentLabels<MiscellaneousLabelsContent>(PageNames.MiscellaneousLabels, LanguageId)
                    };
                    //OPN; PRI; CLO
                    return PartialView("_WelcomeJourneyStepThree", wjStepThree);
                    #endregion
                case 4:
                    #region Step 4 - Set up Profile preferences
                    var profile = SocialBL.GetMemberProfile(VhmSession.SessionID, VhmSession.MemberID, VhmSession.MemberID, true);
                    var wjStepFour = new WelcomeJourneyStepFour
                    {
                        MemberProfile = profile,
                        WJCurrentStep = 4,
                        WJMaxNumber = maxStepNumber,
                        MiscContent = MemberBL.GetContentLabels<MiscellaneousLabelsContent>(PageNames.MiscellaneousLabels, LanguageId),
                        WJContent = MemberBL.GetContentLabels<WelcomeJourneyContentLabels>(PageNames.WelcomeJourneyLabels, LanguageId),
                        CommonContent = MemberBL.GetContentLabels<CommonLabelsContent>(PageNames.CommonLabels, LanguageId)
                    };
                    return PartialView("_WelcomeJourneyStepFour", wjStepFour);
                    #endregion
                case 5:
                    #region Step 5 - HRA info and options
                    var stepFiveModel = new WelcomeJourneyStepFive();
                    //try to get the HSCompletion
                    var modules = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.MemberModules) as List<MemberModule> ?? MemberBL.GetMemberModules(VhmSession.MemberID, VhmSession.PrimarySponsorID);

                    if (modules.Count(m => m.ModuleTypeCode == "HRA") > 0)
                    {
                        var hraSett = modules.First(m => m.ModuleTypeCode == "HRA").HRASetting;
                        stepFiveModel.IsHraMandatory = hraSett == 2;
                        stepFiveModel.IsHraOptional = hraSett == 1;
                    }
                    stepFiveModel.WJCurrentStep = maxStepNumber == 5 ? 5 : maxStepNumber;
                    stepFiveModel.WJMaxNumber = maxStepNumber;
                    stepFiveModel.MiscContent =
                        MemberBL.GetContentLabels<MiscellaneousLabelsContent>(PageNames.MiscellaneousLabels, LanguageId);
                    //stepFiveModel.Content = wjContent;
                    return PartialView("_WelcomeJourneyStepFive", stepFiveModel);
                    #endregion
            }
        }

        public JsonResult GetSpecificCulture(Int16 cultureId)
        {
            var data = new object();
            try
            {
                var cultureFormats = MemberBL.GetCultureFormats();
                if (cultureFormats != null && cultureFormats.Count > 0)
                {
                    var res = cultureFormats.Find(x => x.CultureID == cultureId);
                    var ci = CultureInfo.GetCultureInfo(res.SpecCultureCode);
                    var dateForm = ci.DateTimeFormat.ShortDatePattern;
                    data = new
                    {
                        res.CultureCode,
                        res.CultureID,
                        res.MeasureSystem,
                        CultureName = res.Name,
                        DateFormat = dateForm
                    };
                }

                return Json(JsonConvert.SerializeObject(data), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionHandler.ProcessException(ex);
                return Json(JsonConvert.SerializeObject(data), JsonRequestBehavior.AllowGet);
            }
        }

        //to be used to save first WJ Step
        public PartialViewResult SaveWJFirstStep(short cultureId, int maxWJSteps)
        {
            try
            {
                List<CultureFormat> cultureFormats = MemberBL.GetCultureFormats();

                if (cultureId > 0)
                {
                    //get  MemberPersonalInformation --GetMemberPersonalInformation
                    var memberPersInfo = MemberBL.GetMemberPersonalInformation(VhmSession.MemberID);
                    //save MemberPersonalInformation
                    WelcomeJourneyStepOne model;
                    //get the content
                    //get the labels

                    var timeout = FormsAuthentication.Timeout.Minutes; //this is how is passed in old release solution, represents the session timeout
                    var cultureInfo = cultureFormats.Find(c => c.CultureID == cultureId);
                    if (memberPersInfo != null && memberPersInfo.MemberID > 0)
                    {
                        if (cultureId > 0)
                        {
                            memberPersInfo.CultureID = cultureId;

                            //make sure we update the session with the culture format as soon as we save it so if member navigates to pages using/determining the culture, they won't break - (e.g this would fix issues like member gets exception page if navigates to Monthly Statement as soon as they finish WJ)


                            VhmSession.Culture = cultureInfo.SpecCultureCode;
                        }

                        var infoSaved = MemberBL.SaveMemberPersonalInformation(memberPersInfo);
                        var sessionUpdated = MemberBL.UpdateVhmSession(VhmSession, timeout);
                        if (!sessionUpdated)
                        {
                            ExceptionHandler.ProcessException(new Exception("Unable to update VhmSession with culture: " + VhmSession.Culture));
                        }
                        //save step
                        if (infoSaved)
                        {
                            int nextStep = MemberBL.SaveMemberWelcomeJourneyCurrentStep(VhmSession.MemberID, 1);
                            if (nextStep == 2)
                            {
                                //load step 2
                                var wjStepTwo = new WelcomeJourneyStepTwo
                                {
                                    CultureId = cultureId,
                                    CultureInfo = cultureInfo.SpecCultureCode,
                                    MeasurementSystem = cultureInfo.MeasureSystem,
                                    WJCurrentStep = 2,
                                    WJMaxNumber = maxWJSteps,
                                    ContentMeasurementLabels = MemberBL.GetContentLabels<MeasurementLabelsContent>(PageNames.MeasurementLabels, LanguageId),
                                    MiscContent = MemberBL.GetContentLabels<MiscellaneousLabelsContent>(PageNames.MiscellaneousLabels, LanguageId)
                                };

                                return PartialView("_WelcomeJourneyStepTwo", wjStepTwo);
                            }
                            model = new WelcomeJourneyStepOne
                            {
                                CultureFormats = cultureFormats,
                                WJCurrentStep = 1,
                                WJMaxNumber = maxWJSteps,
                                MiscContent = MemberBL.GetContentLabels<MiscellaneousLabelsContent>(PageNames.MiscellaneousLabels, LanguageId)
                            };

                            return PartialView("_WelcomeJourneyStepOne", model);
                        }
                        //reload step 1
                        model = new WelcomeJourneyStepOne
                        {
                            CultureFormats = cultureFormats,
                            WJCurrentStep = 1,
                            WJMaxNumber = maxWJSteps,
                            MiscContent = MemberBL.GetContentLabels<MiscellaneousLabelsContent>(PageNames.MiscellaneousLabels, LanguageId)
                        };
                        return PartialView("_WelcomeJourneyStepOne", model);
                    }
                    //reload step 1
                    model = new WelcomeJourneyStepOne
                    {
                        CultureFormats = cultureFormats,
                        WJCurrentStep = 1,
                        WJMaxNumber = maxWJSteps,
                        MiscContent = MemberBL.GetContentLabels<MiscellaneousLabelsContent>(PageNames.MiscellaneousLabels, LanguageId)
                    };
                    return PartialView("_WelcomeJourneyStepOne", model);
                }
                else
                {
                    //reload step 1
                    var model = new WelcomeJourneyStepOne
                    {
                        CultureFormats = cultureFormats,
                        WJCurrentStep = 1,
                        WJMaxNumber = maxWJSteps,
                        MiscContent = MemberBL.GetContentLabels<MiscellaneousLabelsContent>(PageNames.MiscellaneousLabels, LanguageId)
                    };
                    return PartialView("_WelcomeJourneyStepOne", model);
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.ProcessException(ex);
                var model = new WelcomeJourneyStepOne
                {
                    CultureFormats = MemberBL.GetCultureFormats(),
                    WJCurrentStep = 1,
                    WJMaxNumber = maxWJSteps,
                    MiscContent = MemberBL.GetContentLabels<MiscellaneousLabelsContent>(PageNames.MiscellaneousLabels, LanguageId)
                };
                return PartialView("_WelcomeJourneyStepOne", model);
            }
        }
        //to be used to save second WJ Step
        public PartialViewResult SaveWJFSecondStep(double weight, double height, int maxWJSteps, short cultureId)
        {
            #region Get Welcome Journey Content


            var wjContent = MemberBL.GetContentLabels<WelcomeJourneyContent>(PageNames.WelcomeJourney, LanguageId);
            // CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.WelcomeJourney, wjContent);
            #endregion
            try
            {
                if (weight > 0 && height > 0)
                {
                    //We already saved the measurement preferences
                    //old code grouping type: grouping["type"] = 1;
                    var assessmentGroupingId = MemberBL.InsertMeasurementAssessmentGrouping(1);
                    WelcomeJourneyStepTwo wjStepTwo;
                    CultureFormat cultureInfo;
                    if (assessmentGroupingId > 0)
                    {
                        var memberTime = MemberBL.GetMemberNow(VhmSession.MemberID, DateTime.Now);
                        var saveMeasurementsHeight = MemberBL.MeasurementInsert(Convert.ToInt64(assessmentGroupingId), Convert.ToInt16(MeasurementType.HEIGHT), VhmSession.MemberID,
                                                                            VhmSession.MemberID, 1, 1, height, height, 0, memberTime, null, null, null);
                        //var saveMeasurementsWeight = 1;//
                        var saveMeasurementsWeight = MemberBL.MeasurementInsert(Convert.ToInt64(assessmentGroupingId), Convert.ToInt16(MeasurementType.WEIGHT), VhmSession.MemberID,
                                                                             VhmSession.MemberID, 1, 1, weight, weight, 0, memberTime, null, null, null); //sourceID and StatusID copied from old code
                        //save step
                        if (saveMeasurementsHeight > 0 && saveMeasurementsWeight > 0)
                        {
                            int nextStep = MemberBL.SaveMemberWelcomeJourneyCurrentStep(VhmSession.MemberID, 2);
                            if (nextStep == 3)
                            {
                                //load step 3
                                var wjStepThree = new WelcomeJourneyStepThree
                                {
                                    CommunityType = VhmSession.CommunityType,
                                    WJCurrentStep = 3,
                                    WJMaxNumber = maxWJSteps,
                                    MiscContent = MemberBL.GetContentLabels<MiscellaneousLabelsContent>(PageNames.MiscellaneousLabels, LanguageId)
                                }; //default to en-US as we dont know the code here
                                //OPN; PRI; CLO
                                return PartialView("_WelcomeJourneyStepThree", wjStepThree);
                            }
                            wjStepTwo = new WelcomeJourneyStepTwo { CultureId = cultureId };
                            cultureInfo = MemberBL.GetCultureFormats().Find(c => c.CultureID == cultureId);
                            wjStepTwo.CultureInfo = cultureInfo.SpecCultureCode;
                            wjStepTwo.MeasurementSystem = cultureInfo.MeasureSystem;
                            wjStepTwo.WJCurrentStep = 2;
                            wjStepTwo.WJMaxNumber = maxWJSteps;
                            wjStepTwo.ContentMeasurementLabels =
                                MemberBL.GetContentLabels<MeasurementLabelsContent>(PageNames.MeasurementLabels,
                                                                                     LanguageId);
                            wjStepTwo.MiscContent =
                                MemberBL.GetContentLabels<MiscellaneousLabelsContent>(PageNames.MiscellaneousLabels,
                                                                                       LanguageId);
                            return PartialView("_WelcomeJourneyStepTwo", wjStepTwo);
                        }
                        //reload step 2
                        wjStepTwo = new WelcomeJourneyStepTwo { CultureId = cultureId };
                        cultureInfo = MemberBL.GetCultureFormats().Find(c => c.CultureID == cultureId);
                        wjStepTwo.CultureInfo = cultureInfo.SpecCultureCode;
                        wjStepTwo.MeasurementSystem = cultureInfo.MeasureSystem;
                        wjStepTwo.WJCurrentStep = 2;
                        wjStepTwo.WJMaxNumber = maxWJSteps;
                        wjStepTwo.ContentMeasurementLabels =
                               MemberBL.GetContentLabels<MeasurementLabelsContent>(PageNames.MeasurementLabels,
                                                                                    LanguageId);
                        wjStepTwo.MiscContent =
                            MemberBL.GetContentLabels<MiscellaneousLabelsContent>(PageNames.MiscellaneousLabels,
                                                                                   LanguageId);
                        return PartialView("_WelcomeJourneyStepTwo", wjStepTwo);
                    }
                    //reload step 2
                    wjStepTwo = new WelcomeJourneyStepTwo { CultureId = cultureId };
                    cultureInfo = MemberBL.GetCultureFormats().Find(c => c.CultureID == cultureId);
                    wjStepTwo.CultureInfo = cultureInfo.SpecCultureCode;
                    wjStepTwo.MeasurementSystem = cultureInfo.MeasureSystem;
                    wjStepTwo.WJCurrentStep = 2;
                    wjStepTwo.WJMaxNumber = maxWJSteps;
                    wjStepTwo.ContentMeasurementLabels =
                               MemberBL.GetContentLabels<MeasurementLabelsContent>(PageNames.MeasurementLabels,
                                                                                    LanguageId);
                    wjStepTwo.MiscContent =
                        MemberBL.GetContentLabels<MiscellaneousLabelsContent>(PageNames.MiscellaneousLabels,
                                                                               LanguageId);
                    return PartialView("_WelcomeJourneyStepTwo", wjStepTwo);
                }
                else
                {
                    //reload step 2
                    var wjStepTwo = new WelcomeJourneyStepTwo { CultureId = cultureId }; //default to en-US as we dont know the code here
                    var cultureInfo = MemberBL.GetCultureFormats().Find(c => c.CultureID == cultureId);
                    wjStepTwo.CultureInfo = cultureInfo.SpecCultureCode;
                    wjStepTwo.MeasurementSystem = cultureInfo.MeasureSystem;
                    wjStepTwo.WJCurrentStep = 2;
                    wjStepTwo.WJMaxNumber = maxWJSteps;
                    wjStepTwo.ContentMeasurementLabels =
                               MemberBL.GetContentLabels<MeasurementLabelsContent>(PageNames.MeasurementLabels,
                                                                                    LanguageId);
                    wjStepTwo.MiscContent =
                        MemberBL.GetContentLabels<MiscellaneousLabelsContent>(PageNames.MiscellaneousLabels,
                                                                               LanguageId);
                    return PartialView("_WelcomeJourneyStepTwo", wjStepTwo);
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.ProcessException(ex);

                var wjStepTwo = new WelcomeJourneyStepTwo { CultureId = cultureId }; //default to en-US as we dont know the code here
                var cultureInfo = MemberBL.GetCultureFormats().Find(c => c.CultureID == cultureId);
                wjStepTwo.CultureInfo = cultureInfo.SpecCultureCode;
                wjStepTwo.MeasurementSystem = cultureInfo.MeasureSystem;
                wjStepTwo.WJCurrentStep = 2;
                wjStepTwo.WJMaxNumber = maxWJSteps;
                wjStepTwo.ContentMeasurementLabels =
                               MemberBL.GetContentLabels<MeasurementLabelsContent>(PageNames.MeasurementLabels,
                                                                                    LanguageId);
                wjStepTwo.MiscContent =
                    MemberBL.GetContentLabels<MiscellaneousLabelsContent>(PageNames.MiscellaneousLabels,
                                                                           LanguageId);
                return PartialView("_WelcomeJourneyStepTwo", wjStepTwo);
            }
        }
        public PartialViewResult SaveWJFThirdStep(int maxWJSteps)
        {
            #region Get Welcome Journey Content

            var wjContent = MemberBL.GetContentLabels<WelcomeJourneyContent>(PageNames.WelcomeJourney, LanguageId);
            //  CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.WelcomeJourney, wjContent);

            #endregion
            try
            {
                int nextStep = MemberBL.SaveMemberWelcomeJourneyCurrentStep(VhmSession.MemberID, 3);
                if (nextStep == 4)
                {
                    //load step 4
                    var profile = SocialBL.GetMemberProfile(VhmSession.SessionID, VhmSession.MemberID, VhmSession.MemberID, true);
                    //if closed community, load step 5
                    if (VhmSession.CommunityType == CommunityType.COMMUNITY_TYPE_CLOSED)
                    {
                        //save profile and open step 5
                        profile.Member.PrivacySettingsSet = true;
                        profile.Member.ProfileExists = 1;

                        var success = MemberBL.SaveMemberPrivateSettings(VhmSession.SessionID, profile.Member, false);
                        if (success)
                        {
                            var stepFiveModel = new WelcomeJourneyStepFive();
                            //try to get the HSCompletion
                            var modules = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.MemberModules) as List<MemberModule> ?? MemberBL.GetMemberModules(VhmSession.MemberID, VhmSession.PrimarySponsorID);

                            if (modules.Count(m => m.ModuleTypeCode == "HRA") > 0)
                            {
                                var hraSett = modules.First(m => m.ModuleTypeCode == "HRA").HRASetting;
                                stepFiveModel.IsHraMandatory = hraSett == 2;
                                stepFiveModel.IsHraOptional = hraSett == 1;
                            }
                            stepFiveModel.WJCurrentStep = maxWJSteps == 5 ? 5 : maxWJSteps;
                            stepFiveModel.WJMaxNumber = maxWJSteps;
                            stepFiveModel.MiscContent =
                                MemberBL.GetContentLabels<MiscellaneousLabelsContent>(PageNames.MiscellaneousLabels, LanguageId);
                            return PartialView("_WelcomeJourneyStepFive", stepFiveModel);
                        }
                        var wjStepThreeNoSuccess = new WelcomeJourneyStepThree
                        {
                            CommunityType = VhmSession.CommunityType,
                            WJCurrentStep = 3,
                            WJMaxNumber = maxWJSteps,
                            MiscContent = MemberBL.GetContentLabels<MiscellaneousLabelsContent>(PageNames.MiscellaneousLabels, LanguageId)
                        };
                        return PartialView("_WelcomeJourneyStepThree", wjStepThreeNoSuccess);
                    }
                    var wjStepFour = new WelcomeJourneyStepFour
                    {
                        MemberProfile = profile,
                        WJCurrentStep = 4,
                        WJMaxNumber = maxWJSteps,
                        MiscContent = MemberBL.GetContentLabels<MiscellaneousLabelsContent>(PageNames.MiscellaneousLabels, LanguageId),
                        WJContent = MemberBL.GetContentLabels<WelcomeJourneyContentLabels>(PageNames.WelcomeJourneyLabels, LanguageId),
                        CommonContent = MemberBL.GetContentLabels<CommonLabelsContent>(PageNames.CommonLabels, LanguageId)
                    };
                    return PartialView("_WelcomeJourneyStepFour", wjStepFour);
                }
                var wjStepThree = new WelcomeJourneyStepThree
                {
                    CommunityType = VhmSession.CommunityType,
                    WJCurrentStep = 3,
                    WJMaxNumber = maxWJSteps
                };
                //OPN; PRI; CLO
                return PartialView("_WelcomeJourneyStepThree", wjStepThree);
            }
            catch (Exception ex)
            {
                ExceptionHandler.ProcessException(ex);

                var wjStepThree = new WelcomeJourneyStepThree
                {
                    CommunityType = VhmSession.CommunityType,
                    WJCurrentStep = 3,
                    WJMaxNumber = maxWJSteps,
                    MiscContent = MemberBL.GetContentLabels<MiscellaneousLabelsContent>(PageNames.MiscellaneousLabels, LanguageId)
                };
                //OPN; PRI; CLO
                return PartialView("_WelcomeJourneyStepThree", wjStepThree);
            }
        }
        public PartialViewResult SaveWJFourthStep(int maxWJStep, short sAge, short sGender, short sTotalSt, short sAvrgSteps, short sLastGzUpload, short sChallenge, bool privacy)
        {
            try
            {
                var sessId = VhmSession.SessionID;
                var membId = VhmSession.MemberID;
                var currentSettings = SocialBL.GetMemberProfile(sessId, membId, membId, false);
                currentSettings.Member.ShowGender = sGender;
                currentSettings.Member.ShowAge = sAge;
                currentSettings.Member.ShowTotalSteps = sTotalSt;
                currentSettings.Member.ShowStepsAvg = sAvrgSteps;
                currentSettings.Member.ShowLastGZUpload = sLastGzUpload;
                currentSettings.Member.ShowChallengeHistory = sChallenge;
                currentSettings.Member.PrivacySettingsSet = true;
                currentSettings.Member.ProfileExists = 1;
                currentSettings.Member.Privacy = privacy;

                var success = MemberBL.SaveMemberPrivateSettings(sessId, currentSettings.Member, false);
                if (success)
                {
                    int nextStep = MemberBL.SaveMemberWelcomeJourneyCurrentStep(VhmSession.MemberID, 4);
                    if (nextStep == 5)
                    {
                        var stepFiveModel = new WelcomeJourneyStepFive();
                        //try to get the HSCompletion
                        var modules = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.MemberModules) as List<MemberModule> ?? MemberBL.GetMemberModules(VhmSession.MemberID, VhmSession.PrimarySponsorID);

                        if (modules.Count(m => m.ModuleTypeCode == "HRA") > 0)
                        {
                            var hraSett = modules.First(m => m.ModuleTypeCode == "HRA").HRASetting;
                            stepFiveModel.IsHraMandatory = hraSett == 2;
                            stepFiveModel.IsHraOptional = hraSett == 1;
                        }
                        stepFiveModel.WJCurrentStep = maxWJStep == 5 ? 5 : maxWJStep;
                        stepFiveModel.WJMaxNumber = maxWJStep;
                        stepFiveModel.MiscContent =
                            MemberBL.GetContentLabels<MiscellaneousLabelsContent>(PageNames.MiscellaneousLabels,
                                                                                   LanguageId);
                        //stepFiveModel.Content = wjContent;
                        return PartialView("_WelcomeJourneyStepFive", stepFiveModel);
                    }
                    //load step 4
                    var profile = SocialBL.GetMemberProfile(VhmSession.SessionID, VhmSession.MemberID, VhmSession.MemberID, true);
                    var wjStepFour = new WelcomeJourneyStepFour
                    {
                        MemberProfile = profile,
                        WJCurrentStep = 4,
                        WJMaxNumber = maxWJStep,
                        MiscContent = MemberBL.GetContentLabels<MiscellaneousLabelsContent>(PageNames.MiscellaneousLabels, LanguageId),
                        WJContent = MemberBL.GetContentLabels<WelcomeJourneyContentLabels>(PageNames.WelcomeJourneyLabels, LanguageId),
                        CommonContent = MemberBL.GetContentLabels<CommonLabelsContent>(PageNames.CommonLabels, LanguageId)
                    };
                    return PartialView("_WelcomeJourneyStepFour", wjStepFour);
                }
                else
                {
                    //load step 4
                    var profile = SocialBL.GetMemberProfile(VhmSession.SessionID, VhmSession.MemberID, VhmSession.MemberID, true);
                    var wjStepFour = new WelcomeJourneyStepFour
                    {
                        MemberProfile = profile,
                        WJCurrentStep = 4,
                        WJMaxNumber = maxWJStep,
                        MiscContent = MemberBL.GetContentLabels<MiscellaneousLabelsContent>(PageNames.MiscellaneousLabels, LanguageId),
                        WJContent = MemberBL.GetContentLabels<WelcomeJourneyContentLabels>(PageNames.WelcomeJourneyLabels, LanguageId),
                        CommonContent = MemberBL.GetContentLabels<CommonLabelsContent>(PageNames.CommonLabels, LanguageId)
                    };
                    return PartialView("_WelcomeJourneyStepFour", wjStepFour);
                }

            }
            catch (Exception ex)
            {
                ExceptionHandler.ProcessException(ex);
                //load step 4
                var profile = SocialBL.GetMemberProfile(VhmSession.SessionID, VhmSession.MemberID, VhmSession.MemberID, true);
                var wjStepFour = new WelcomeJourneyStepFour
                {
                    MemberProfile = profile,
                    WJCurrentStep = 4,
                    WJMaxNumber = maxWJStep,
                    MiscContent = MemberBL.GetContentLabels<MiscellaneousLabelsContent>(PageNames.MiscellaneousLabels, LanguageId),
                    WJContent = MemberBL.GetContentLabels<WelcomeJourneyContentLabels>(PageNames.WelcomeJourneyLabels, LanguageId),
                    CommonContent = MemberBL.GetContentLabels<CommonLabelsContent>(PageNames.CommonLabels, LanguageId)
                };
                return PartialView("_WelcomeJourneyStepFour", wjStepFour);
            }
        }
        //no view or partial view. This is a method used via ajax call to get a specific culture info
        public ActionResult FinishWelcomeJourney(int stepNumber)
        {
            var result = false;
            try
            {
                var sessId = VhmSession.SessionID;
                var membId = VhmSession.MemberID;
                var currentSettings = SocialBL.GetMemberProfile(sessId, membId, membId, false);
                //this is a closed community member
                currentSettings.Member.PrivacySettingsSet = true;
                currentSettings.Member.ProfileExists = 1;
                currentSettings.Member.Privacy = false;

                var success = MemberBL.SaveMemberPrivateSettings(sessId, currentSettings.Member, false);
                MemberBL.SaveMemberWelcomeJourneyCurrentStep(VhmSession.MemberID, Convert.ToByte(stepNumber));
                if (success)
                {
                    result = MemberBL.SaveEmailPromptFlag(VhmSession.MemberID, null, null, null, null, null, null, DateTime.Now, null, null, null);
                    UpdateSessionAfterWJSave(VhmSession, false, 0);
                }
                return Json(JsonConvert.SerializeObject(result), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionHandler.ProcessException(ex);
                return Json(JsonConvert.SerializeObject(result), JsonRequestBehavior.AllowGet);
            }

        }
        public JsonResult FinishWelcomeJourneyFromStepFour(int stepNumber, short sAge, short sGender, short sTotalSt, short sAvrgSteps, short sLastGzUpload, short sChallenge, bool privacy)
        {
            var result = false;
            try
            {
                var sessId = VhmSession.SessionID;
                var membId = VhmSession.MemberID;
                var currentSettings = SocialBL.GetMemberProfile(sessId, membId, membId, false);
                currentSettings.Member.ShowGender = sGender;
                currentSettings.Member.ShowAge = sAge;
                currentSettings.Member.ShowTotalSteps = sTotalSt;
                currentSettings.Member.ShowStepsAvg = sAvrgSteps;
                currentSettings.Member.ShowLastGZUpload = sLastGzUpload;
                currentSettings.Member.ShowChallengeHistory = sChallenge;
                currentSettings.Member.PrivacySettingsSet = true;
                currentSettings.Member.ProfileExists = 1;
                currentSettings.Member.Privacy = privacy;

                var success = MemberBL.SaveMemberPrivateSettings(sessId, currentSettings.Member, false);
                if (success)
                {
                    MemberBL.SaveMemberWelcomeJourneyCurrentStep(VhmSession.MemberID, Convert.ToByte(stepNumber));
                    result = MemberBL.SaveEmailPromptFlag(VhmSession.MemberID, null, null, null, null, null, null, DateTime.Now, null, null, null);
                    UpdateSessionAfterWJSave(VhmSession, false, 0);
                }
                return Json(JsonConvert.SerializeObject(result), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionHandler.ProcessException(ex);
                return Json(JsonConvert.SerializeObject(result), JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult FinishWelcomeJourneyStepFive(int stepNumber, int hraOption)
        {
            var result = false;
            try
            {
                #region Save the Welcome Journey step
                MemberBL.SaveMemberWelcomeJourneyCurrentStep(VhmSession.MemberID, Convert.ToByte(stepNumber));
                #endregion

                #region Update the HRA option member selected
                //hraOption can be: 0 hasn't selected; 1:Don't remind; -1:remind me later; 2:take me there - no update needed
                if (hraOption == -1) //-1:remind me later;
                {
                    //we only need to update the current session. The HRA pop up should display again next time member logs in
                    CacheProvider.Add(System.Web.HttpContext.Current, CacheProvider.CacheItems.HSnapshotDoNotRemind, -1);
                }
                #endregion

                #region finish the welcome Journey
                var saveMP = MemberBL.SaveEmailPromptFlag(VhmSession.MemberID, null, null, null, null, null, null, DateTime.Now, null, null, null);

                var updateSess = UpdateSessionAfterWJSave(VhmSession, true, hraOption);
                result = (saveMP && updateSess);
                #endregion

                #region Update cache with some hacks needed
                if (result)
                {
                    //update CacheProvider. This is a fix for the last step that is showing even though WJ is finished
                    // to check if member is NewMember we need to also check the CacheProvider.CacheItems.FinishedWelcomeJourney value. This is set only after WJ is finished, as now we're using nCache and oMembership["Value"] is not updated unless member actualy logs off and then logs back in. We only need to check the value if (oMembership["Value"]) != "0" as otherwise the member is not a new member anymore.
                    // Prevent the '<user> update their profile' message from being added to the newsfeed
                    CacheProvider.Add(System.Web.HttpContext.Current, CacheProvider.CacheItems.ProfileUpdated, bool.FalseString);
                    CacheProvider.Add(System.Web.HttpContext.Current, CacheProvider.CacheItems.FinishedWelcomeJourney, bool.TrueString);
                }
                //call this to invalidate the cached session and reload the fresh one.
                CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.SessionCheckTimestamp, null);
                #endregion

                return Json(JsonConvert.SerializeObject(result), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionHandler.ProcessException(ex);
                return Json(JsonConvert.SerializeObject(result), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult FinishExpressWelcomeJourney(double weight, double height, short cultureId)
        {
            var ok = true;

            try
            {
                #region Save Step 1 - Culture info
                var memberPersInfo = MemberBL.GetMemberPersonalInformation(VhmSession.MemberID);
                memberPersInfo.CultureID = cultureId;
                var infoSaved = MemberBL.SaveMemberPersonalInformation(memberPersInfo);
                var timeout = FormsAuthentication.Timeout.Minutes;

                List<CultureFormat> cultureFormats = MemberBL.GetCultureFormats();
                var cultureInfo = cultureFormats.Find(c => c.CultureID == cultureId);
                VhmSession.Culture = cultureInfo.SpecCultureCode;

                var sessionUpdated = MemberBL.UpdateVhmSession(VhmSession, timeout);

                var step1Saved = MemberBL.SaveMemberWelcomeJourneyCurrentStep(VhmSession.MemberID, 1);
                #endregion

                #region Save Step 2 - Height and Weight

                var memberTime = MemberBL.GetMemberNow(VhmSession.MemberID, DateTime.Now);
                var assessmentGroupingId = MemberBL.InsertMeasurementAssessmentGrouping(1);
                var saveMeasurementsHeight = MemberBL.MeasurementInsert(Convert.ToInt64(assessmentGroupingId), Convert.ToInt16(MeasurementType.HEIGHT), VhmSession.MemberID,
                                                                            VhmSession.MemberID, 1, 1, height, height, 0, memberTime, null, null, null);

                var saveMeasurementsWeight = MemberBL.MeasurementInsert(Convert.ToInt64(assessmentGroupingId), Convert.ToInt16(MeasurementType.WEIGHT), VhmSession.MemberID,
                                                                     VhmSession.MemberID, 1, 1, weight, weight, 0, memberTime, null, null, null); //sourceID and StatusID copied from old code

                MemberBL.SaveMemberWelcomeJourneyCurrentStep(VhmSession.MemberID, 2);

                #endregion

                #region Save Step 3 - SponsorContent Viewed
                MemberBL.SaveMemberWelcomeJourneyCurrentStep(VhmSession.MemberID, 3);
                #endregion

                #region Save Step 4 - Social/Profile Settings
                var profile = SocialBL.GetMemberProfile(VhmSession.SessionID, VhmSession.MemberID, VhmSession.MemberID, true);
                profile.Member.PrivacySettingsSet = true;
                profile.Member.ProfileExists = 1;
                var success = MemberBL.SaveMemberPrivateSettings(VhmSession.SessionID, profile.Member, false);

                var sessId = VhmSession.SessionID;
                var membId = VhmSession.MemberID;
                var currentSettings = SocialBL.GetMemberProfile(sessId, membId, membId, false);
                currentSettings.Member.ShowGender = 0;
                currentSettings.Member.ShowAge = 0;
                currentSettings.Member.ShowTotalSteps = 0;
                currentSettings.Member.ShowStepsAvg = 0;
                currentSettings.Member.ShowLastGZUpload = 0;
                currentSettings.Member.ShowChallengeHistory = 0;
                currentSettings.Member.PrivacySettingsSet = true;
                currentSettings.Member.ProfileExists = 1;
                currentSettings.Member.Privacy = false;

                var privacySet = MemberBL.SaveMemberPrivateSettings(sessId, currentSettings.Member, false);
                MemberBL.SaveMemberWelcomeJourneyCurrentStep(VhmSession.MemberID, Convert.ToByte(4));

                #endregion

                #region Save Step 5 - Health Snapshot settings

                var saveMP = MemberBL.SaveEmailPromptFlag(VhmSession.MemberID, null, null, null, null, null, null,
                                                           DateTime.Now, null, null, null);

                const int HRA_DO_NOT_REMIND = 1;
                UpdateSessionAfterWJSave(VhmSession, true, HRA_DO_NOT_REMIND);

                CacheProvider.Add(System.Web.HttpContext.Current, CacheProvider.CacheItems.ProfileUpdated, bool.FalseString);
                CacheProvider.Add(System.Web.HttpContext.Current, CacheProvider.CacheItems.FinishedWelcomeJourney,
                                   bool.TrueString);

                CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.SessionCheckTimestamp, null);

                #endregion

            }
            catch (Exception ex)
            {

            }

            return Json(ok, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult LapsDisplay()
        {
            //Get game
            var lapModel = new LapsGameDisplayVM();
            var member = MemberBL.GetMemberById(VhmSession.SessionID, VhmSession.MemberID);
            SponsorProfile = SponsorProfile ?? GetSponsorProfile();
            var game = RewardsBL.GetSponsorCurrentGame(VhmSession.SessionID, member, VhmSession.PrimarySponsorID);
            var currencies = RewardsBL.GetGameCurrencies().ToDictionary( c => c.Code, c => c.Name );

            if ( currencies.ContainsKey( game.Currency ) )
            {
                game.CurrencyDescription = currencies[game.Currency];
            }

            //Format and calculate days left
            var memberNow = MemberBL.GetMemberNow(VhmSession.MemberID, DateTime.Now);
            if (game != null && !game.EndDate.HasValue)
            {
                game.EndDate = game.StartDate.AddYears(1).AddDays(-1);
            }

            int daysLeft = game == null ? 0 : game.EndDate.GetValueOrDefault().Subtract(memberNow).Days;
            
            CultureInfo sponsorCulture = string.IsNullOrEmpty(SponsorProfile.CultureCode)
                                                 ? new CultureInfo("en-US")
                                                 : new CultureInfo(SponsorProfile.CultureCode);
            bool sponsorInternational = SponsorProfile.International;

            var ticker = GetTicker();
            decimal totalHealthCash = 0;
            bool isTickerAvailable = false;
            if (ticker != null)
            {
                totalHealthCash = ticker.TotalHCEarn;
                isTickerAvailable = true;
            }
            lapModel = new LapsGameDisplayVM
            {
                ImageCDN = ApplicationSettings.SiteImageUrl,
                AmountToNextLevel = MemberBL.GetHmToNextLevel(game),
                AvailableCashReward = game == null ? 0 : game.AvailableRewards.Amount.GetValueOrDefault(),
                CurrentGame = game,
                CurrentProgress = game == null ? 0 : game.MemberTotalHealthMiles,
                DaysLeft = daysLeft,
                SponsorCultureFormat = sponsorCulture,
                IsSponsorInternational = sponsorInternational,
                AllowSpendHealthCash = VhmSession.SpendVLCCash == 1,
                AvailableRewards = game == null
                                       ? new RewardsBalance()
                                       : game.AvailableRewards ?? new RewardsBalance(),
                IsTickerAvailable = isTickerAvailable
            };

            return PartialView("_LapsGraph", lapModel);
        }

        //No game display, even with no game we need some data
        public PartialViewResult NoGameDisplay()
        {
            //Get game
            var noGameModel = new LapsGameDisplayVM();
            var member = MemberBL.GetMemberById(VhmSession.SessionID, VhmSession.MemberID);
            SponsorProfile = SponsorProfile ?? GetSponsorProfile();
            var game = RewardsBL.GetSponsorCurrentGame(VhmSession.SessionID, member, VhmSession.PrimarySponsorID);

            CultureInfo sponsorCulture = string.IsNullOrEmpty(SponsorProfile.CultureCode)
                                                 ? new CultureInfo("en-US")
                                                 : new CultureInfo(SponsorProfile.CultureCode);

            var ticker = GetTicker();
            decimal totalHealthCash = 0;
            if (ticker != null)
            {
                totalHealthCash = ticker.TotalHCEarn;
            }
            noGameModel = new LapsGameDisplayVM
            {
                AvailableCashReward = totalHealthCash,
                SponsorCultureFormat = sponsorCulture,
                AvailableRewards = game == null
                                           ? new RewardsBalance()
                                           : game.AvailableRewards ?? new RewardsBalance(),
            };

            return PartialView("LandingPageV3/Partials/_NoGameGraph", noGameModel);
        }

        public PartialViewResult GameDisplay()
        {
            // Disable this debugging parameter when live
            //if (!System.Web.HttpContext.Current.IsDebuggingEnabled)
            //{
            //    hm = null;
            //}

            GameDisplayGraphVM gameModel;

            string gameDisplay = "_LevelsGraph";
            try
            {
                var member = MemberBL.GetMemberById(VhmSession.SessionID, VhmSession.MemberID);
                SponsorProfile = SponsorProfile ?? GetSponsorProfile();

                // Get the current Game
                var game = RewardsBL.GetSponsorCurrentGame(VhmSession.SessionID, member, VhmSession.PrimarySponsorID);

                // get all the different reward types offered in this game
                var rewardTypes = new List<Reward>();
                var rewardsSummary = new Dictionary<string, decimal>();
                var rewardsEarnedSummary = new Dictionary<string, decimal>();
                var otherRewards = new List<Reward>();

                Reward currentAvailableRewardToDisplay = null;
                decimal relProgressPerc = 7;

                var ticker = GetTicker();
                decimal totalHealthCash = 0;
                bool isTickerAvailable = false;
                if (ticker != null)
                {
                    totalHealthCash = ticker.TotalHCEarn;
                    isTickerAvailable = true;
                }
                if (game != null)
                {
                    rewardTypes = RewardsBL.GetGameRewardTypes(game.Milestones.Where(m => m.Rewards != null).ToList());

                    game.Milestones.ForEach(m => m.Rewards = m.Rewards ?? new List<Reward>());

                    var groupedRewards = (from milestone in game.Milestones
                                          where game.Milestones.Any()
                                          from reward in milestone.Rewards
                                          where reward.RewardType != "BADGE" &&
                                                reward.RewardType.Trim() != "TEXT" &&
                                                reward.RewardType.Trim() != "CERT"
                                          group reward by reward.RewardType.Trim()
                                              into r
                                              select r).ToList();

                    #region Earned rewards

                    // Populate a list of earned rewards. This will be the sum of all rewards (by type)
                    // for which the member has completed the milestone.
                    // Initialize a a dictionary with reward types. 
                    rewardsEarnedSummary =
                        groupedRewards.ToDictionary<IGrouping<string, Reward>, string, decimal>(
                            groupedReward => groupedReward.Key.Trim(), groupedReward => 0);

                    // Update the value with the milestone total for each reward where the member has completed the milestone
                    const string PREMIUM_REBATE = "PMRBT";
                    const string EMPLOYEE_DISCOUNT = "EMPD";
                    const string BADGE = "BADGE";

                    const string HEALTH_CASH = "HCASH";
                    foreach (var reward in groupedRewards)
                    {
                        var rewardTypeTrimmed = reward.Key.Trim();

                        var typeTotal = (from milestone in game.Milestones
                                         where game.Milestones.Any()
                                         from r in milestone.Rewards
                                         where r.RewardType.Trim() != BADGE
                                               && r.RewardType.Trim() == rewardTypeTrimmed
                                         select r.Amount).ToList();

                        // employee discount and premium rebates are percentages, and as such can't be summed up
                        // display the Max amount instead for these types
                        var displayTotal = typeTotal.Any()
                                               ? rewardTypeTrimmed == EMPLOYEE_DISCOUNT ||
                                                 rewardTypeTrimmed == PREMIUM_REBATE
                                                     ? typeTotal.Max()
                                                     : typeTotal.Sum()
                                               : 0;
                        rewardsSummary.Add(rewardTypeTrimmed, displayTotal);

                        var typeEarnedTotal = (from milestone in game.Milestones
                                               where game.Milestones.Any()
                                               from r in milestone.Rewards
                                               where milestone.RelativeProgress > 0
                                                     && r.RewardType.Trim() != BADGE
                                                     && r.RewardType.Trim() == rewardTypeTrimmed
                                               select r.Amount).ToList();


                        decimal typeEarnedTotalDisplay = 0;
                        switch (rewardTypeTrimmed)
                        {
                            case HEALTH_CASH:
                                typeEarnedTotalDisplay = totalHealthCash;
                                break;
                            default:
                                typeEarnedTotalDisplay = typeEarnedTotal.Any()
                                                                 ? rewardTypeTrimmed == EMPLOYEE_DISCOUNT ||
                                                                   rewardTypeTrimmed == PREMIUM_REBATE
                                                                       ? typeEarnedTotal.Max()
                                                                       : typeEarnedTotal.Sum()
                                                                 : 0;
                                break;
                        }
                        rewardsEarnedSummary[rewardTypeTrimmed] = typeEarnedTotalDisplay;
                    }

                    #endregion

                    #region Other rewards
                    otherRewards = game.TriggerRewards;
                    #endregion

                    #region Get View Name based on game display type. Fill in RelativeProgressPercentage  for tasks

                    switch (game.GameDisplayType)
                    {
                        case "LVL":
                            {
                                gameDisplay = "_LevelsGraph";
                                break;
                            }
                        case "PRG":
                        case "TSK":
                            {
                                gameDisplay = "_TasksGraph";
                                if (game.Milestones.Count > 0)
                                {
                                    var milestone = game.Milestones.SingleOrDefault(m => m.IsCurrentMilestone);
                                    decimal currProgress = 0;

                                    var maxProgress = game.Milestones.Max().Value;
                                    if (milestone == null)
                                    {
                                        game = null;
                                    }
                                    else if (game.MemberTotalHealthMiles >= maxProgress)
                                    {
                                        currProgress = 100;
                                    }
                                    else
                                    {
                                        int idx = game.Milestones.IndexOf(milestone);

                                        decimal percentPerMilestone = 100 / game.Milestones.Count;

                                        currProgress = (percentPerMilestone * idx) + (percentPerMilestone * (milestone.RelativeProgress / 100));
                                        currProgress = Math.Round(currProgress, 0);
                                        currentAvailableRewardToDisplay = GetTaskCurrentMilestoneAvailableRewardDisplay(milestone);
                                    }

                                    relProgressPerc = currProgress;
                                }

                                break;
                            }
                        case "IGN":
                            {
                                break;
                            }
                        default:
                            {
                                gameDisplay = "_LevelsGraph";
                                break;
                            }
                    }

                    #endregion
                }
                // Need to set the Health Cash currency symbol based on the Sponsor's culture, not the member.
                CultureInfo sponsorCulture = string.IsNullOrEmpty(SponsorProfile.CultureCode)
                                             ? new CultureInfo("en-US")
                                             : new CultureInfo(SponsorProfile.CultureCode);

                var regionInfo = new RegionInfo(sponsorCulture.LCID);

                // build lookup dictionary for reward images per type
                var rewardImageLookup = new Dictionary<string, string>
                                            {
                                                { "HCASH", regionInfo.CurrencySymbol },
                                                { "CASHI", regionInfo.CurrencySymbol },
                                                { "ENTRY", "icon-other.png" },
                                                { "HPP", "icon-other.png" },
                                                { "HRA", "HRA" },
                                                { "HS/RA", "HSA/HRA" },
                                                { "HSA", "HSA" },
                                                { "PMCDT", "icon-insurance.png" },
                                                { "PMRBT", "icon-insurance.png" },
                                                { "SLABS", "icon-other.png" },
                                                { "XXXXX", "icon-other.png" }
                                            };




                var memberNow = MemberBL.GetMemberNow(VhmSession.MemberID, DateTime.Now);
                if (game != null && !game.EndDate.HasValue)
                {
                    game.EndDate = game.StartDate.AddYears(1).AddDays(-1);
                }

                int daysLeft = game == null ? 0 : game.EndDate.GetValueOrDefault().Subtract(memberNow).Days;

                gameModel = new GameDisplayGraphVM
                {
                    ImageCDN = ApplicationSettings.SiteImageUrl,
                    CurrentGame = game,
                    CurrentProgress = game == null ? 0 : game.MemberTotalHealthMiles,
                    RewardsByType = rewardTypes,
                    DaysLeft = daysLeft,
                    RewardsImageLookup = rewardImageLookup,
                    RewardsAvailableSummary = rewardsSummary,
                    RewardsEarnedSummary = rewardsEarnedSummary,
                    AvailableRewards = game == null
                                           ? new RewardsBalance()
                                           : game.AvailableRewards ?? new RewardsBalance(),
                    OtherRewards = otherRewards,
                    RelativeProgressPercentage = relProgressPerc,
                    AllowSpendHealthCash = VhmSession.SpendVLCCash == 1,
                    SponsorCultureFormat = sponsorCulture,
                    ShowNoTasksInProgressContent = false,
                    ShowNoTasksToCompleteContent =
                        game == null || game.Milestones.Count == 0 || game.StartDate.Date > memberNow.Date,
                    MilestoneRewardDisplay = currentAvailableRewardToDisplay ?? new Reward(),
                    OnTrackToEarnAmount = GetOnTrackToEarnAmount(game, daysLeft),
                    AmountToNextLevel = MemberBL.GetHmToNextLevel(game),
                    AvialableHealthCash = totalHealthCash,
                    IsTickerAvailable = isTickerAvailable
                };
            }
            catch (Exception ex)
            {
                throw ExceptionHandler.ProcessException(ex);
            }

            return PartialView(gameDisplay, gameModel);
        }

        private double GetOnTrackToEarnAmount(Game game, int daysLeft)
        {

            if (game == null)
            {
                return 0;
            }

            long currentHm = game.MemberTotalHealthMiles;
            double daysPassedInGame = DateTime.Now.Subtract(game.StartDate).TotalDays;
            daysPassedInGame = Math.Ceiling(Math.Max(daysPassedInGame, 1));  // avoid divide by zero
            double averageHmPerDay = Math.Ceiling(currentHm / daysPassedInGame);
            double onTrackAmount = (averageHmPerDay * daysLeft) + currentHm;

            return onTrackAmount;
        }
        public JsonResult SaveLanguageId(int languageId)
        {
            try
            {


                //get  MemberPersonalInformation --GetMemberPersonalInformation
                var memberPersInfo = MemberBL.GetMemberPersonalInformation(VhmSession.MemberID);
                //save MemberPersonalInformation

                var timeout = FormsAuthentication.Timeout.Minutes; //this is how is passed in old release solution, represents the session timeout

                if (memberPersInfo != null && memberPersInfo.MemberID > 0)
                {

                    if (languageId > 0)
                    {
                        memberPersInfo.LanguageID = languageId;
                        VhmSession.LanguageID = languageId;
                        //Updating the cookie
                        var cookieName = ApplicationCookies.VHMLanguageId.ToString();
                        var httpCookie = Request.Cookies[cookieName];
                        var newCookie = httpCookie ?? new HttpCookie(cookieName);

                        newCookie.Value = languageId.ToString();
                        newCookie.Expires = DateTime.Now.AddYears(20);
                        System.Web.HttpContext.Current.Response.Cookies.Add(newCookie);

                    }
                    // For New users culture Code is null and SaveMemberPersonalInformation throws an exception 
                    // First I used browser's cultrue code, but somehow the CultureId was diffrent than what MT is expecting
                    // For example, CultureId for en-US is 86. That's why I used sponsor's defult cultureId
                    if (VhmSession.Culture == null)
                    {
                        SponsorProfile = SponsorBL.GetSponsorProfile(VhmSession.PrimarySponsorID);
                        memberPersInfo.CultureID = SponsorProfile.CultureID;
                        VhmSession.Culture = SponsorProfile.CultureCode;
                    }
                    var infoSaved = MemberBL.SaveMemberPersonalInformation(memberPersInfo);
                    var sessionUpdated = MemberBL.UpdateVhmSession(VhmSession, timeout);
                    if (!sessionUpdated)
                    {
                        ExceptionHandler.ProcessException(new Exception("Unable to update VhmSession with Lagnuage: " + VhmSession.LanguageID));
                    }
                    GetLandingPageContentLabels();
                }
                return Json(JsonConvert.SerializeObject(true), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionHandler.ProcessException(ex);

                return Json(JsonConvert.SerializeObject(false), JsonRequestBehavior.AllowGet);

            }

        }

        #endregion
        #region redeem a voucher
        public JsonResult RedeemVoucher(string redeemCode)
        {
            var content = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.MemberVoucher) as Dictionary<MemberVoucherContent, string>;
            if (content == null)
            {
                content = MemberBL.GetContentLabels<MemberVoucherContent>(PageNames.LandingPageRedeemVoucher, LanguageId);
                //  CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.MemberVoucher, content);
            }
            var bSuccess = false;
            var errorLinkText = content[MemberVoucherContent.ErrorMessageContactUs];
            var errorMessage = content[MemberVoucherContent.ErrorMessage];

            try
            {
                if (redeemCode != string.Empty)
                {
                    var checkVoucher = MemberBL.CheckVoucherByCode(redeemCode);
                    var oToday = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                    var bFound = (checkVoucher.Count > 0);

                    if (checkVoucher.Count > 0)
                    {
                        var voucher = checkVoucher.FirstOrDefault();
                        var bDeleted = voucher != null && (Convert.ToBoolean(voucher.Deleted));
                        var bRedeemed = voucher != null && (bFound && Convert.ToBoolean(voucher.Used));
                        var bExpired = voucher != null && ((voucher.Expiry != null) && (bFound && !bRedeemed && oToday.CompareTo(Convert.ToDateTime(voucher.Expiry)) > 0));
                        //(voucher.Expiry != null) ? (bFound && !bRedeemed && oToday.CompareTo(Convert.ToDateTime(voucher.Expiry)) > 0) : false;

                        //need to check if the voucher is a rewards one(MIL or CSH), otherwise, do not redeem as there is no reward attached to it. this is database logic moved to FE
                        var bIsRewardsVoucher = voucher != null && (voucher.CodeType == "MIL" || voucher.CodeType == "CSH");
                        if (bFound && !bRedeemed && !bExpired && !bDeleted && bIsRewardsVoucher)
                        {
                            voucher.VoucherCode = redeemCode;
                            bSuccess = MemberBL.RedeemRewardsVoucher(voucher, VhmSession.MemberID);
                        }
                    }
                }
                return Json(JsonConvert.SerializeObject(new { Success = bSuccess, ErrorLinkText = errorLinkText, ErrorMessage = errorMessage }), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionHandler.ProcessException(ex);
                return Json(JsonConvert.SerializeObject(new { Success = false, ErrorLinkText = errorLinkText, ErrorMessage = errorMessage }), JsonRequestBehavior.AllowGet);
            }
        }

        public PartialViewResult ShowRedeemVoucher()
        {
            var redeemVoucher = new MemberVoucher();
            return PartialView("_ShowRedeemVoucher", redeemVoucher);
        }

        [OutputCache(Location = OutputCacheLocation.Any, Duration = 60)]
        public JsonResult GetLandingPageContentLabels()
        {

            // if the member has not completed the welcome journey, get the languageId from the browser
            var isNewMember = VhmSession.Membership == 1;


            var voucherContent = MemberBL.GetContentLabels<MemberVoucherContent>(PageNames.LandingPageRedeemVoucher, LanguageId);
            var measurementContent = MemberBL.GetContentLabels<MeasurementSummaryContent>(PageNames.MeasurementsSummary, LanguageId);

            long expressSponsorId = long.Parse(ConfigurationManager.AppSettings["ExpressSponsorId"]);
            bool isExpress = VhmSession.PrimarySponsorID == expressSponsorId;

            // only load welcome journey content if this is a new member
            var welcomeJourneyContent = isNewMember || isExpress
                                            ? MemberBL.GetContentLabels<WelcomeJourneyContent>(
                                                PageNames.WelcomeJourney, LanguageId)
                                            : new Dictionary<WelcomeJourneyContent, string>();
            var landingPageContent = MemberBL.GetContentLabels<LandingPageContent>(PageNames.MemberLandingPage, LanguageId);
            var kagContent = MemberBL.GetContentLabels<KagContent>(PageNames.KnowAndGo, LanguageId);
            var miscContent = MemberBL.GetContentLabels<MiscellaneousLabelsContent>(PageNames.MiscellaneousLabels, LanguageId);

            // merge the 5 dictionaries into one
            var jsonContent = (
                    from label in voucherContent
                    select new Tuple<string, string>(label.Key.ToString(), label.Value))
                .Union(
                    from label in measurementContent
                    select new Tuple<string, string>(label.Key.ToString(), label.Value))
                .Union(
                    from label in welcomeJourneyContent
                    select new Tuple<string, string>(label.Key.ToString(), label.Value))
                .Union(
                    from label in landingPageContent
                    select new Tuple<string, string>(label.Key.ToString(), label.Value))
                .Union(
                    from label in kagContent
                    select new Tuple<string, string>(label.Key.ToString(), label.Value))
                .Union(
                    from label in miscContent
                    select new Tuple<string, string>(label.Key.ToString(), label.Value));

            return Json(jsonContent, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Private methods

        private Reward GetTaskCurrentMilestoneAvailableRewardDisplay(Milestone milestone)
        {
            //If cash reward available: Highest value cash reward (e.g. $10 HealthCash)
            //If no cash reward available: Highest value non cash reward (e.g. 500 points)
            //If no number value reward available: nothing displays 
            Reward cashReward = null;
            if (milestone.Rewards.Any(r => r.IsCash))
            {
                cashReward = milestone.Rewards.Where(r => r.IsCash && r.Amount > 0).Max();
            }
            Reward nonCashReward = null;
            if (milestone.Rewards.Any(r => !r.IsCash))
            {
                nonCashReward = milestone.Rewards.Where(r => !r.IsCash && r.Amount > 0).Max();
            }
            Reward reward = cashReward ?? nonCashReward;
            //for tasks, we need to show Certificate/Badge/text Only - reward type description if this is the only reward
            if (reward == null && milestone.Rewards.Count > 0)
            {
                reward = milestone.Rewards.FirstOrDefault();
            }
            return reward;
        }

        private void SetRecentlyEarnedRewardsCookie(DateTime entryDate)
        {
            // set a cookie with the latest entry date of the most recently earned rewards
            var gzActivityCookie = new HttpCookie("GZActivity") { Value = entryDate.Ticks.ToString(CultureInfo.InvariantCulture) };

            Response.Cookies.Add(gzActivityCookie);
        }

        private long? GetRecentlyEarnedRewardsDateFromCookie()
        {
            // set a cookie with the latest entry date of the most recently earned rewards
            var gzActivityCookie = Request.Cookies.AllKeys.Contains(GZ_ACTIVITY_COOKIE)
                                       ? Request.Cookies[GZ_ACTIVITY_COOKIE]
                                       : null;
            long? entryDate = null;

            if (gzActivityCookie != null)
            {
                long tmp;
                long.TryParse(gzActivityCookie.Value, out tmp);
                if (tmp > 0)
                {
                    entryDate = tmp;
                }
            }

            return entryDate;
        }

        private Tickers GetTicker()
        {
            return MemberBL.GetTickers(VhmSession.MemberID, VhmSession.PrimarySponsorID);
        }

        private bool UpdateSessionAfterWJSave(Session vhmSession, bool updateHraOption, int? hraOption)
        {
            bool result;
            // Prevent the '<user> update their profile' message from being added to the newsfeed
            CacheProvider.Add(System.Web.HttpContext.Current, CacheProvider.CacheItems.ProfileUpdated, bool.FalseString);
            CacheProvider.Add(System.Web.HttpContext.Current, CacheProvider.CacheItems.FinishedWelcomeJourney, bool.TrueString);
            //we need to update the session so it doesn't display the new member flag as true
            var timeout = FormsAuthentication.Timeout.Minutes; //this is how is passed in old release solution, represents the session timeout
            if (updateHraOption)
            {
                vhmSession.HealthSnapshot = Convert.ToInt32(hraOption);
                vhmSession.Membership = 0;
                result = MemberBL.SaveHealthSnapshotOption(vhmSession, timeout);
            }
            else
            {
                vhmSession.Membership = 0;
                result = MemberBL.UpdateVhmSession(vhmSession, timeout);
            }
            CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.SessionCheckTimestamp, null);
            return result;
        }
        #endregion

        public ActionResult NoSmoking()
        {
            if (VhmSession.AnniversaryDate != null)
            {
                var model = MemberBL.GetNoSmokingViewModel(VhmSession.MemberID, VhmSession.PrimarySponsorID, VhmSession.AnniversaryDate.Value);
                if (model.PageAccessible)
                {
                    var noSmokingContent = MemberBL.GetContentLabels<NoSmokingContent>(
                        PageNames.TobaccoFreeAgreement, LanguageId);
                    model.ContentNoSmoking = noSmokingContent;
                    model.ContentMisc = MemberBL.GetContentLabels<MiscellaneousLabelsContent>(PageNames.MiscellaneousLabels, LanguageId);
                    model.CommonContent = MemberBL.GetContentLabels<CommonLabelsContent>(PageNames.CommonLabels,
                                                                                          LanguageId);
                    var pageTitle = new PageTitle(true, noSmokingContent[NoSmokingContent.TobacoFreeAgreement]);
                    model.PageTitle = pageTitle;
                    var selectedColorTheme = GetSelectedTheme();
                    model.ThemeCode = selectedColorTheme;
                    model.MemberId = VhmSession.MemberID;
                    return View(model);
                }
            }
            return RedirectToAction("LandingPage");
        }

        public ActionResult SaveNoSmoking(bool isAgreed)
        {
            var rewardAmount = MemberBL.SaveNoSmoking(VhmSession.MemberID, isAgreed);
            return RedirectToAction("NoSmoking");
        }

        public PartialViewResult GameDisplayV3()
        {
            //Get game

            var lapModel = new LapsGameDisplayVM();
            var member = MemberBL.GetMemberById(VhmSession.SessionID, VhmSession.MemberID);
            SponsorProfile = SponsorProfile ?? GetSponsorProfile();
            var game = RewardsBL.GetSponsorCurrentGame(VhmSession.SessionID, member, VhmSession.PrimarySponsorID);
            var rewardTypes = new List<Reward>();
            var rewardsSummary = new Dictionary<string, decimal>();
            var rewardsEarnedSummary = new Dictionary<string, decimal>();
            var otherRewards = new List<Reward>();

            //Format and calculate days left
            var memberNow = MemberBL.GetMemberNow(VhmSession.MemberID, DateTime.Now);
            if (game != null && !game.EndDate.HasValue)
            {
                game.EndDate = game.StartDate.AddYears(1).AddDays(-1);
            }

            /* Calculating length of game */
            int gameLengthInDays = (game == null) ? 0 : game.EndDate.GetValueOrDefault().Subtract(game.StartDate).Days;
            /* Days left till game end */
            int daysLeft = game == null ? 0 : game.EndDate.GetValueOrDefault().Subtract(memberNow).Days;
            /* How much time has passed since challenge started*/
            int daysSpentSinceChallangeStarted = game == null ? 0 : memberNow.Subtract(game.StartDate).Days;

            CultureInfo sponsorCulture = string.IsNullOrEmpty(SponsorProfile.CultureCode)
                                                 ? new CultureInfo("en-US")
                                                 : new CultureInfo(SponsorProfile.CultureCode);
            if (game != null)
            {
                rewardTypes = RewardsBL.GetGameRewardTypes(game.Milestones.Where(m => m.Rewards != null).ToList());
                game.Milestones.ForEach(m => m.Rewards = m.Rewards ?? new List<Reward>());
                var groupedRewards = (from milestone in game.Milestones
                                      where game.Milestones.Any()
                                      from reward in milestone.Rewards
                                      where reward.RewardType != "BADGE" &&
                                            reward.RewardType.Trim() != "TEXT" &&
                                            reward.RewardType.Trim() != "CERT"
                                      group reward by reward.RewardType.Trim()
                                          into r
                                          select r).ToList();

                #region Earned rewards

                // Populate a list of earned rewards. This will be the sum of all rewards (by type)
                // for which the member has completed the milestone.
                // Initialize a a dictionary with reward types. 
                rewardsEarnedSummary =
                    groupedRewards.ToDictionary<IGrouping<string, Reward>, string, decimal>(
                        groupedReward => groupedReward.Key.Trim(), groupedReward => 0);

                // Update the value with the milestone total for each reward where the member has completed the milestone
                const string PREMIUM_REBATE = "PMRBT";
                const string EMPLOYEE_DISCOUNT = "EMPD";
                const string BADGE = "BADGE";
                foreach (var reward in groupedRewards)
                {
                    var rewardTypeTrimmed = reward.Key.Trim();
                    var typeTotal = (from milestone in game.Milestones
                                     where game.Milestones.Any()
                                     from r in milestone.Rewards
                                     where r.RewardType.Trim() != BADGE
                                           && r.RewardType.Trim() == rewardTypeTrimmed
                                     select r.Amount).ToList();

                    // employee discount and premium rebates are percentages, and as such can't be summed up
                    // display the Max amount instead for these types
                    var displayTotal = typeTotal.Any()
                                           ? rewardTypeTrimmed == EMPLOYEE_DISCOUNT ||
                                             rewardTypeTrimmed == PREMIUM_REBATE
                                                 ? typeTotal.Max()
                                                 : typeTotal.Sum()
                                           : 0;
                    rewardsSummary.Add(rewardTypeTrimmed, displayTotal);

                    var typeEarnedTotal = (from milestone in game.Milestones
                                           where game.Milestones.Any()
                                           from r in milestone.Rewards
                                           where milestone.RelativeProgress > 0
                                                 && r.RewardType.Trim() != BADGE
                                                 && r.RewardType.Trim() == rewardTypeTrimmed
                                           select r.Amount).ToList();

                    var typeEarnedTotalDisplay = typeEarnedTotal.Any()
                                                     ? rewardTypeTrimmed == EMPLOYEE_DISCOUNT ||
                                                       rewardTypeTrimmed == PREMIUM_REBATE
                                                           ? typeEarnedTotal.Max()
                                                           : typeEarnedTotal.Sum()
                                                     : 0;
                    rewardsEarnedSummary[rewardTypeTrimmed] = typeEarnedTotalDisplay;
                }

                #endregion
            }
            var regionInfo = new RegionInfo(sponsorCulture.LCID);
            var rewardImageLookup = new Dictionary<string, string>
                                            {
                                                { "HCASH", regionInfo.CurrencySymbol },
                                                { "CASHI", regionInfo.CurrencySymbol },
                                                { "ENTRY", "icon-other.png" },
                                                { "HPP", "icon-other.png" },
                                                { "HRA", "HRA" },
                                                { "HS/RA", "HSA/HRA" },
                                                { "HSA", "HSA" },
                                                { "PMCDT", "icon-insurance.png" },
                                                { "PMRBT", "icon-insurance.png" },
                                                { "SLABS", "icon-other.png" },
                                                { "XXXXX", "icon-other.png" }
                                            };

            var ticker = GetTicker();
            decimal totalHealthCash = 0;
            bool isTickerAvailable = false;
            if (ticker != null)
            {
                totalHealthCash = ticker.TotalHCEarn;
                isTickerAvailable = true;
            }
            lapModel = new LapsGameDisplayVM
            {
                ImageCDN = ApplicationSettings.SiteImageUrl,
                CurrentGame = game,
                CurrentProgress = game == null ? 0 : game.MemberTotalHealthMiles,
                RewardsByType = rewardTypes,
                DaysLeft = daysLeft,
                GameLengthInDays = gameLengthInDays,
                DaysSpentSinceChallangeStarted = daysSpentSinceChallangeStarted,
                RewardsImageLookup = rewardImageLookup,
                RewardsAvailableSummary = rewardsSummary,
                RewardsEarnedSummary = rewardsEarnedSummary,
                AvailableRewards = game == null
                                       ? new RewardsBalance()
                                       : game.AvailableRewards ?? new RewardsBalance(),
                OtherRewards = otherRewards,
                AllowSpendHealthCash = VhmSession.SpendVLCCash == 1,
                SponsorCultureFormat = sponsorCulture,
                AmountToNextLevel = MemberBL.GetHmToNextLevel(game),
                AvialableHealthCash = totalHealthCash,
                IsTickerAvailable = isTickerAvailable
            };

            return PartialView("LandingPageV3/Partials/_LevelsGraphV3", lapModel);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UseNewLanding(bool useNewLanding)
        {
            string answer;
            if (useNewLanding)
            {
                answer = "yes";
            }
            else
            {
                answer = "no";
            }
            DateTime dt = new DateTime();
            var oCookie = new HttpCookie("NewLanding" + VhmSession.MemberID.ToString());
            oCookie.Value = answer;
            oCookie.Expires = DateTime.MaxValue;
            Response.Cookies.Add(oCookie);
            return Json(JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public bool SaveRewardCelebration()
        {
            try
            {
                return MemberBL.SaveRewardedTaskCelebration(VhmSession.MemberID, null, true);
            }
            catch (Exception)
            {

                return false;
            }
        }

        public string GetSessionId()
        {
            string sessionId;
            try
            {
                sessionId = FormsAuthentication.Decrypt(Request.Cookies["SessionID"].Value).UserData;
            }
            catch (Exception e)
            {
                sessionId = string.Empty;
            }
            return sessionId;
        }


        /// <summary>
        /// A method that returns a list of badges if new landing page has been enabled
        /// </summary>
        /// <param name="maxRewards"></param>
        /// <param name="modelErrors"></param>
        /// <param name="measurementRewardsOnly"></param>
        /// <returns></returns>
        private List<RecentlyEarnedRewardsItem> FilterOutRecentlyEarnedBadges(int maxRewards, Dictionary<LandingPageVM.LandingPageModelErrors, string> modelErrors, bool measurementRewardsOnly)
        {
            try
            {
                if (EnableNewLanding)
                {
                    List<RecentlyEarnedRewardsItem> recentlyEarnedBadges = MemberBL.GetRecentlyEarnedRewards(VhmSession.MemberID, maxRewards, measurementRewardsOnly, true);
                    return recentlyEarnedBadges;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.ProcessException(ex);
                modelErrors.Add(LandingPageVM.LandingPageModelErrors.Rewards, ex.Message);
            }
            return new List<RecentlyEarnedRewardsItem>();
        }

    }
}
