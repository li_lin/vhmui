﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Xml;
using Vhm.Providers;
using Vhm.Web.BL;
using Vhm.Web.Common;
using Vhm.Web.DataServices.Adapters;
using Vhm.Web.Models;
using Vhm.Web.Models.RepositoryModels;
using Vhm.Web.Models.ViewModels.Shared;

namespace VirginLifecareWeb.Controllers
{
    [Authorize]
    public class ControllerBase : Controller
    {
        public IEnumerable<ColorTheme> ColorThemes { get; set; }
        public bool ShowPopUpForBillingInfoBase{get; set;}
        private const string STATUS_NEW = "NEW";

        public bool EnableNewLanding
        {
            get
            {
                if (VhmSession == null || string.IsNullOrEmpty(VhmSession.SessionID))
                {
                    return false;
                }
            
                var httpCookie = Request.Cookies["NewLanding" + VhmSession.MemberID.ToString()];

                if (httpCookie != null && !string.IsNullOrEmpty(httpCookie.Value))
                {
                    if (httpCookie.Value == "yes")
                    {
                        return true;

                    }
                    return false;
                }
                return false;
            }
        }

        // Got the logic from MemeberMaster.cs in release project Line 160/178
        public void ShowPopForBillingInfo()
        {
            //if member has a status of new display enter CC message and 'freeze' site unless on update CC page
            string absolutePath = Request.FilePath.Trim().ToLower();
            ShowPopUpForBillingInfoBase = VhmSession != null && VhmSession.MemberStatus.ToUpper() == STATUS_NEW && !absolutePath.Contains("updatecc.aspx");
        }

        public Session VhmSession { get; set; }
        protected SponsorProfile SponsorProfile { get; set; }
        private int languageId;
        protected int LanguageId
        {
            get
            {

                languageId = GetDefaultLanguage();

                return languageId;
            }
            set
            {
                languageId = GetDefaultLanguage();
            }
        }
        private Dictionary<HeaderContent, string> _contentHeader;
        protected Dictionary<HeaderContent, string> ContentHeader
        {
            get
            {
                {

                    ContentHeader = MemberBL.GetContentLabels<HeaderContent>(PageNames.PageHeader, LanguageId);

                    return _contentHeader;
                }
            }
            set
            {
                _contentHeader = value;
                //  CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.HeaderContent, _contentHeader);

            }
        }

        public ControllerBase()
        {


        }

        protected List<MemberModule> GetModules()
        {

            var modules = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.MemberModules) as List<MemberModule>;
            if (VhmSession != null && modules == null)
            {
                modules = MemberBL.GetMemberModules(VhmSession.MemberID, VhmSession.PrimarySponsorID);
                CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.MemberModules, modules);
            }
            return modules;
        }

        public IEnumerable<ColorTheme> GetColorThemes()
        {
            var colorThemes = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.SponsorThemes) as List<ColorTheme>;
            try
            {
                if (VhmSession != null && colorThemes == null)
                {
                    colorThemes = SponsorBL.GetAvailableSponsorColorThemes(VhmSession.PrimarySponsorID);
                }
                CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.SponsorThemes, colorThemes);
            }
            catch (Exception ex)
            {
                if (VhmSession == null)
                {
                    VirginLifecare.Exception.ExceptionHandler.ProcessException(new NullReferenceException("VhmSession cannot be null"), true);
                }
                else
                {
                    VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
                }
            }

            return colorThemes;
        }

        private int GetDefaultLanguage()
        {

            var httpCookie = Request.Cookies[ApplicationCookies.VHMLanguageId.ToString()];
            var isNewMember = VhmSession.Membership == 1;
            int langId;
            if (httpCookie != null && !string.IsNullOrEmpty(httpCookie.Value))
            {
                int.TryParse(httpCookie.Value, out langId);
            }
            else
                if (isNewMember)
                {

                    langId = GetBrowserLanguage();

                }
                else
                {
                    langId = VhmSession != null && VhmSession.LanguageID > 0 ? VhmSession.LanguageID : ApplicationSettings.DefaultLanguage;
                }
            SetCookie(ApplicationCookies.VHMLanguageId.ToString(), langId.ToString(), DateTime.Now.AddYears(20));
            return langId;


        }
        // Refactore the code and put in a method so that it will be acceassible thorough controllers.
        protected CultureInfo GetBrowserCulture()
        {
            var l = Request.UserLanguages;
            string browserCulture = (l != null && l[0] != null) ? l[0] : "en-US";
            if (browserCulture.IndexOf("es-", StringComparison.OrdinalIgnoreCase) == 0 || browserCulture == "es")
            {//Mapping all spanich dilects to Spanish-US
                browserCulture = "es-US";
            }
            if (browserCulture.IndexOf("en-", StringComparison.OrdinalIgnoreCase) == 0 || browserCulture == "en")
            {//Mapping all English dilects to English-US
                browserCulture = "en-US";
            }
            Thread.CurrentThread.CurrentCulture = new CultureInfo(browserCulture);
            var someCulture = Thread.CurrentThread.CurrentCulture;
            return someCulture;
        }
        private int GetBrowserLanguage()
        {
            var browserCulture = GetBrowserCulture();
            return browserCulture.LCID;

        }

        protected ColorTheme GetSelectedTheme()
        {
            var colorTheme = new ColorTheme { ColorThemeCode = "red", ColorThemeName = "Standard Red", BrandingThemeId = 1, isDefault = true }; //default
            if (VhmSession.PrimarySponsorID > 0)
            {
                SponsorProfile = SponsorProfile ?? GetSponsorProfile();
                colorTheme = ColorThemes.FirstOrDefault(ct => ct.BrandingThemeId == SponsorProfile.BrandingThemeID);
                VhmSession.ThemeCode = colorTheme;
            }

            return colorTheme;
        }

        protected SponsorProfile GetSponsorProfile()
        {
            var sponsorProfile = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.SponsorProfile) as SponsorProfile;
            if (sponsorProfile == null && VhmSession.PrimarySponsorID > 0)
            {
                sponsorProfile = SponsorBL.GetSponsorProfile(VhmSession.PrimarySponsorID);
                CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.SponsorProfile, sponsorProfile);
            }
            return sponsorProfile;
        }

        protected bool StartUp(ActionExecutingContext filterContext)
        {
            try
            {
                var session = CacheProvider.Get( System.Web.HttpContext.Current, CacheProvider.CacheItems.VHMSessionObject );
                if ( session == null )
                {
                    CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.VHMSessionObject, VhmSession);
                }
                //redirect to HRA page if required before welcome journey
                var modules = GetModules();
                bool redirectToHRA = false;
                var hraModule = modules.FirstOrDefault(m => m.ModuleTypeCode == "HRA");
                short? hraSett = 0;
                if (hraModule != null)
                { //check the hra setting
                    // -1 = "Off", 0 = "Optional, not prompted", 1 = "Optional, but prompted", 
                    // 2 = "Mandatory, after Welcome Screens", 3 = "Mandatory, before Welcome Screens"; 
                    hraSett = hraModule.HRASetting;
                }
                if (hraSett == 3 || (VhmSession.Membership != 1 && hraSett == 2))
                // According to Product Manager: if HRA is set to 2 or 3, User has to complete the HRA
                // For all members (new and old) if HRA is 3, then redirect to HRA page. New user, who hasn't completed the welcome journey and HRA is set to 2,
                // user has to complete the welcome Journey first then comlete the HRA.
                {
                    redirectToHRA = VhmSession.NeedHealthSnapShot;
                }
                if (redirectToHRA)
                {
                    filterContext.Result = new RedirectResult(string.Format("{0}/secure/healthsnapshot/hrassessment.aspx", ApplicationSettings.BaseURL), false);
                }
                LanguageId = GetDefaultLanguage();
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(ex, true);
            }
            return true;
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            Initialize(filterContext);
            if (filterContext.Result != null)
            {
                return;
            }

            if (VhmSession != null)
            {
                var sCulture = VhmSession.Culture;
                if (!string.IsNullOrEmpty(sCulture))
                {
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(sCulture);
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(sCulture);
                }
            }
            var viewBag = filterContext.Controller.ViewBag;

            viewBag.OldSiteUrl = ApplicationSettings.BaseURL;
            viewBag.CompanyName = ApplicationSettings.CompanyName;
            viewBag.ProgramName = ApplicationSettings.ProgramName;
            viewBag.EnableNewLanding = EnableNewLanding;
            ShowPopForBillingInfo();
            viewBag.ShowBillingInfo = ShowPopUpForBillingInfoBase;
        }

        [ErrorHandle(View = "Error")]
        protected virtual void Initialize(ActionExecutingContext filterContext)
        {
            //base.Initialize(requestContext);
            //cache = new CacheProvider(System.Web.HttpContext.Current);

            try
            {
                if (Request != null && Request.Cookies != null && Request.Cookies["SessionID"] != null)
                {
                    var httpCookie = Request.Cookies["SessionID"];
                    if (httpCookie != null)
                    {
                        string sessionId = httpCookie.Value;
                        if (!string.IsNullOrEmpty(sessionId))
                        {
                            int timeoutPeriod = ApplicationSettings.DBSessionTimeCheckInMinutes;

                            // Check the timestamp on the session. Need to keep the DB session alive
                            var timestampCheck =
                                CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.SessionCheckTimestamp) as DateTime?;

                            if (timestampCheck == null)
                            {

                                VhmSession = AccountBL.GetSession(Decrypt(sessionId));
                                if (VhmSession == null || string.IsNullOrEmpty(VhmSession.SessionID))
                                {
                                    filterContext.Result = new RedirectResult(FormsAuthentication.LoginUrl);
                                    return;
                                }
                                CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.VHMWebSessionObject, VhmSession);
                                CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.SessionCheckTimestamp, DateTime.UtcNow);
                            }
                            else
                            {
                                if (timestampCheck.GetValueOrDefault().AddMinutes(timeoutPeriod) < DateTime.UtcNow)
                                {
                                    var freshSession = AccountBL.GetSession(Decrypt(sessionId));
                                    if (freshSession == null || string.IsNullOrEmpty(freshSession.SessionID))
                                    {
                                        filterContext.Result = new RedirectResult(FormsAuthentication.LoginUrl);
                                        return;
                                    }
                                    CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.VHMWebSessionObject, freshSession);
                                    CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.SessionCheckTimestamp, DateTime.UtcNow);
                                    VhmSession = freshSession;
                                }
                                else
                                {
                                    VhmSession = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.VHMWebSessionObject) as Session;
                                    if (VhmSession == null)
                                    {
                                        VhmSession = AccountBL.GetSession(Decrypt(sessionId));
                                        if (VhmSession == null || string.IsNullOrEmpty(VhmSession.SessionID))
                                        {
                                            filterContext.Result = new RedirectResult(FormsAuthentication.LoginUrl);
                                            return;
                                        }
                                        CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.VHMWebSessionObject, VhmSession);
                                        CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.SessionCheckTimestamp, DateTime.UtcNow);
                                    }
                                }
                            }
                        }
                        #region redirect to CC payment if member is cancelled
                        //for cancelled members - redirect to cc upgrade
                        if (VhmSession != null && VhmSession.PrimarySponsorID == 0 && VhmSession.MemberID > 0)
                        {
                            //this is a cancelled member
                            filterContext.Result = new RedirectResult(string.Format("{0}/secure/member/upgrade.aspx", ApplicationSettings.BaseURL));
                        }
                        #endregion
                    }
                }
                ColorThemes = GetColorThemes();

                if (filterContext.Result == null)
                {
                    StartUp(filterContext);
                }
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
            }
        }

        protected string Encrypt(string userName, string dataToEncrypt)
        {
            var tkt = new FormsAuthenticationTicket(1, userName, DateTime.Now, DateTime.Now.AddMinutes((int)FormsAuthentication.Timeout.TotalMinutes), false, dataToEncrypt);
            string finalEncryptedData = FormsAuthentication.Encrypt(tkt);

            return finalEncryptedData;
        }

        protected static string Decrypt(string dataToDecrypt)
        {
            var formsAuthenticationTicket = FormsAuthentication.Decrypt(dataToDecrypt);

            return formsAuthenticationTicket.UserData;
        }

        public ActionResult Error()
        {
            var tmp = TempData["LastError"] as Exception;


            if (tmp == null)
            {
                tmp = new Exception("An unknown error has occured.");
            }

            TempData["LastError"] = tmp;
            var error = new HandleErrorInfo(tmp, "Base", "Error");
            return View(error);
        }

        public void SetCookie(string key, string value, DateTime expireDate)
        {
            var httpCookie = System.Web.HttpContext.Current.Request.Cookies[key];
            var newCookie = httpCookie ?? new HttpCookie(key);
            newCookie.Expires = expireDate;
            newCookie.Value = value;
            System.Web.HttpContext.Current.Response.Cookies.Add(newCookie);
        }

    }
}
