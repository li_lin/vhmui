﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using log4net;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Contrib;
using Vhm.Providers;
using Vhm.Web.BL;
using Vhm.Web.Common;
using Vhm.Web.DataServices.MiddleTierActivityService;
using Vhm.Web.Models.ViewModels.Activity;
using Vhm.Web.Models.ViewModels.Shared;
using VirginLifecareWeb.Helpers;

namespace VirginLifecareWeb.Controllers
{
    public class ActivityController:ControllerBase
    {
        private readonly ILog _logger;
        public ActivityController()
        {
            _logger = LogManager.GetLogger(GetType());
        }

        public ActionResult ActivityJournal()
        {
            var model = new ActivityJournalVM { ThemeCode = GetSelectedTheme() };

            ViewData["pageTitle"] = new PageTitle
                                        {
                                            PageTitleName = "Activity Journal",
                                            RightLinkText = string.Empty,
                                            ShowPageTitle = true,
                                            ShowRightTitleLink = false
                                        };
            return View(model);
        }

        public ActionResult Devices()
        {
            //If Callback from Runkeeper
            if(!String.IsNullOrEmpty(Request.QueryString["code"]))
            {
                return RedirectToAction("AddRunkeeperToken", new { code = Request.QueryString["code"].ToString() });
            }

            var content = MemberBL.GetContentLabels<DeviceContent>(PageNames.FitnessTrackingDevices, LanguageId);
            var selectedColorTheme = GetSelectedTheme();

            var isConnectedToRunKeeper = ActivityBL.IsMemberConnectedToRunKeeper(VhmSession.MemberID);
            string runkeeperUrl = string.Empty;
            if(!isConnectedToRunKeeper)
            {
                _logger.Info("User is NOT connected to Runkeeper;");
                runkeeperUrl = GetRunKeeperURL();
            }

            #region Fitbit
            //we added ApplicationSettings.SponsorIdsWithFitBitConnection = comma delimited string of Sponsor id's for which we need the "Connect to FitBit" Button to display
            //if ApplicationSettings.ShowDevicesFitbitSection == "true" -> "Connect to FitBit" displays to ALL sponsors, no need to check ApplicationSettings.SponsorIdsWithFitBitConnection
            //if ApplicationSettings.ShowDevicesFitbitSection == "false" -> we need to check and see if member's sponsor ID is listed in the ApplicationSettings.SponsorIdsWithFitBitConnection
            //if ApplicationSettings.ShowDevicesFitbitSection == "false" and ApplicationSettings.SponsorIdsWithFitBitConnection == string.Empty, we do not show the "Connect to FitBit" to any memeber
            var showFitBitSection = ShowConnectToProviderButton(DeviceType.Fitbit);
            var isConnectedToFitbit = ActivityBL.IsMemberConnectedToFitbit(VhmSession.MemberID) ;
            string fitbitUrl = string.Empty;
            if(!isConnectedToFitbit)
            {
                _logger.Info("User is NOT connected to Fitbit;");
                fitbitUrl = GetFitbitURL();
            }

            #endregion Fitbit

            #region get the IntelliResponse needed parameters
            var memberMajorGoZoneVersion = ActivityBL.GetMemberOrSponsorMajorGoZoneVersion(VhmSession.MemberID,
                                                                                   VhmSession.PrimarySponsorID);
            SponsorProfile = SponsorProfile ?? GetSponsorProfile();
            var intelliResponseMissingParams =
                string.Format("&amp;interfaceID={0}&amp;sponsorID={1}&amp;languageId={2}",
                               SponsorProfile.IntelliResponseID, VhmSession.PrimarySponsorID, LanguageId); //GZVersion={3}&amp; //, memberMajorGoZoneVersion
            string intelliRespPath = (memberMajorGoZoneVersion == "2")
                                         ? ApplicationSettings.RegisterYourGoZone
                                         : ApplicationSettings.RegisterYourMax;
            #endregion

            #region Endomondo

            bool showConnectToEm = ShowConnectToProviderButton(DeviceType.Endomondo);
            string endomondoUrl = null;
            bool isConnectedToEndom = false;
            if (showConnectToEm)
            {
                endomondoUrl = GetEndomondoUrl();
                //don't make this call unless we want to show endomondo section
                isConnectedToEndom = IsMemberConnectedToVendor(_vendorCode[DeviceType.Endomondo]);
            }
            
            var connToVendorObj = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.ConnectToVendorStatusResponse);
            string vendorConnectionStatus = (connToVendorObj != null) ? Convert.ToString( connToVendorObj ) : string.Empty;
            #endregion Endomondo

            #region Misfit
            bool showConnectToMisfit = ShowConnectToProviderButton(DeviceType.Misfit);
            bool isConnectedToMisfit = false;
            string pairMisfitUrl = string.Empty;
            _logger.InfoFormat("showConnectToMisfit = {0}", showConnectToMisfit);
            if ( showConnectToMisfit )
            {
                //don't make this call unless we want to show endomondo section
                isConnectedToMisfit = IsMemberConnectedToVendor(_vendorCode[DeviceType.Misfit]);
                _logger.InfoFormat("IsMemberConnectedToVendor(Misfit) = {0}", isConnectedToMisfit);

                pairMisfitUrl = "PairVendorAccount";
            }
            #endregion
            var modelDevices = new DevicesVM
                                   {
                                       PageT = new PageTitle(true, ContentHeader[HeaderContent.FitnessTrackingDevices]),
                                       Content = content,
                                       ThemeCode = selectedColorTheme,
                                       RunKeeperURL = runkeeperUrl,
                                       FitbitURL = fitbitUrl,
                                       IsRunKeeperConnected = isConnectedToRunKeeper,
                                       IsFitbitConnected = isConnectedToFitbit,
                                       IsMemberRegisterToGZ = IsMemberRegisterToGZ(),
                                       IsMemberLinkedToPolar = IsMemberLinkedToPolar(),
                                       GZSupportURL = ApplicationSettings.GZSupportURL,
                                       ManageYourAccountURL = ApplicationSettings.ManageYourAccountURL,
                                       DownloadGZURL = ApplicationSettings.DownloadGZURL,
                                       BuyPolarURL = ApplicationSettings.BuyPolarURL,
                                       BuyGoZoneURL = ApplicationSettings.BuyGoZoneURL,
                                       LearnMoreAboutPolar = ApplicationSettings.LearnMoreAboutPolar,
                                       SyncSoftwareURL = ApplicationSettings.SyncSoftwareURL,

                                       //IntelliResponse Urls
                                       HowToConnectRK = string.Format("{0}{1}", ApplicationSettings.HowToConnectRK, intelliResponseMissingParams),
                                       HowToConnectFitBit = string.Format("{0}{1}", ApplicationSettings.HowToConnectFitBit, intelliResponseMissingParams),
                                       RegisterGoZoneURL = string.Format("{0}{1}", intelliRespPath, intelliResponseMissingParams),
                                       ShowDevicesFitbitSection = showFitBitSection,
                                       ConnectFitbitThroughRunkeeperPdf = ApplicationSettings.ConnectThroughRunkeeperPairingPdf,
                                       IsDashAppOn = IsMemeberDashAppUser(),
                                       LearMoreAboutHowThisWork = string.Format("{0}{1}", ApplicationSettings.LearnMoreHowThisWork, intelliResponseMissingParams),

                                       VendorConnectResponseStatus = vendorConnectionStatus,
                                       ShowDevicesEndomondoSection = showConnectToEm,
                                       EndomondoURL = endomondoUrl,
                                       IsEndomondoConnected = isConnectedToEndom,
                                       HowToConnectEndomondo = string.Format("{0}{1}", ApplicationSettings.HowToConnectEndomondo, intelliResponseMissingParams),

                                       ShowDevicesMisfitSection = showConnectToMisfit,
                                       IsMisfitConnected = isConnectedToMisfit,
                                       MisfitConnectURL = pairMisfitUrl,
                                       HowToConnectMisfit = string.Format("{0}{1}", ApplicationSettings.HowToConnectMisfit, intelliResponseMissingParams)
                                   };
            
            return View(modelDevices);
        }

        public ActionResult AddRunkeeperToken(string code)
        {
            if(AddMemberRunkeeperToken(code))
            {
                _logger.Info("User has connected to Runkeeper" + code);
                CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.IsConnectedToRunKeeper, true);
                SyncRunkeeperData();
            }
            else
            {
                CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.IsConnectedToRunKeeper, false);
                _logger.Info("User was NOT able to connect to Runkeeper;");
            }
            return RedirectToAction("Devices");
        }

        public ActionResult AddFitbitToken(string oauth_token)
        {
            try
            {
                string oauth_verfier = Request.Params["oauth_verifier"];
                string oauth_Token_secret = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.OAuthFitbitSecret).ToString();

                OAuthAuthorizedInfo oAuthInfo = GetFitbitCredential(oauth_token, oauth_verfier, oauth_Token_secret);

                // Remove the oAuth secret from the cache...
                CacheProvider.Remove(System.Web.HttpContext.Current, CacheProvider.CacheItems.OAuthFitbitSecret);

                // Call the Api.Activity to pair Fitbit account to VMH MemeberId...
                HttpStatusCode statusCode = PairMemberIdAndVendorAccount(oAuthInfo);
                bool success = (statusCode == HttpStatusCode.Created || statusCode == HttpStatusCode.OK);

                CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.ConnectToVendorStatusResponse, statusCode);
                CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.IsConnectedToFitbit, success);

                // This will be now handled in the Api.Activity
                //SyncFitbitData();
            }
            catch (Exception e)
            {
                // Update ConnectToVendorStatusResponse cache item, this will show an message indicating the failure to the user!
                CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.ConnectToVendorStatusResponse, HttpStatusCode.InternalServerError);
                
                _logger.Error("Failure in AddFitbitToken", e);
            }

            return RedirectToAction("Devices");
        } 

        #region Runkeeper
        /// <summary>
        /// Getting the runkeeper settings from web config and creatting the URL for connecting to the RunKeeper.
        /// RunKeeper looks for following request parameters in URL
        /// client_id, response_type, redirect_uri:
        /// </summary>
        /// <returns></returns>
        private string GetRunKeeperURL()
        {
            //RunKeeper  configuration values,
            var authUrl = ApplicationSettings.AuthURL;
            var clientID = ApplicationSettings.ClientID;
            var returnURL = ApplicationSettings.ReturnURL;

            return String.Format("{0}?client_id={1}&response_type=code&redirect_uri={2}", authUrl, clientID, returnURL);
        }

        public ActionResult DeleteMemberRunkeeperToken()
        {
            if(ActivityBL.DeleteMemberRunkeeperToken(VhmSession.MemberID))
            {
                CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.IsConnectedToRunKeeper, false);
            }
            return RedirectToAction("Devices");
        }

        private bool AddMemberRunkeeperToken(string code)
        {
            return ActivityBL.AddMemberRunkeeperToken(VhmSession.MemberID, code);
        }

        /// <summary>
        /// Getting RunKeeper data. It's been called on Landingpage.js.
        /// </summary>
        public void SyncRunkeeperData()
        {
            try
            {
            // moved check cache logic to BL
                var isConnectedToRunKeeper = ActivityBL.IsMemberConnectedToRunKeeper(VhmSession.MemberID);
                if(isConnectedToRunKeeper)
                {
                    //don't force a sync. If already done, ignore
                    ActivityBL.SyncRunkeeperData(VhmSession.MemberID, false);
                }
            }
            catch(Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
            }
        }
        #endregion Runkeeper
      
        #region Fitbit

        //use this to force a sync to Runkeeper data, no matter if it was already done per this session. we'll store the IsConnectedToRunKeeper and IsRunKeeperDataSync, just in case so we don't sync again from landing page
        public JsonResult ForceSyncRunkeeperData()
        {
            try
            {
                // moved check cache logic to BL
                var isConnectedToRunKeeper = ActivityBL.IsMemberConnectedToRunKeeper(VhmSession.MemberID);              
                var isRunKeeperDataSync = false;
                if(isConnectedToRunKeeper)
                {
                    isRunKeeperDataSync = ActivityBL.SyncRunkeeperData(VhmSession.MemberID, true);
                }
                return new JsonResult { Data = isRunKeeperDataSync };
            }
             catch(Exception ex)
             {
                 VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
                 return new JsonResult { Data = false };
             }
        }
        private Authenticator FitbitAuthenticator()
        {
            var ConsumerKey = ApplicationSettings.ConsumerKey;
            var ConsumerSecret = ApplicationSettings.ConsumerSecret;

            return new Authenticator(ConsumerKey,
                                     ConsumerSecret,
                                     "http://api.fitbit.com/oauth/request_token",
                                     "http://api.fitbit.com/oauth/access_token",
                                     "http://api.fitbit.com/oauth/authorize");
        }

        private string GetFitbitURL()
        {

            Authenticator authenticator = FitbitAuthenticator();

            return authenticator.GetAuthUrlToken();
        }

        private OAuthAuthorizedInfo GetFitbitCredential(string token, string verifier, string tokenSecret)
        {
            Authenticator authenticator = FitbitAuthenticator();
            FitbitAuthInfo fitbitInfo = authenticator.ProcessApprovedAuthCallback(token, verifier, tokenSecret);

            OAuthAuthorizedInfo oAuthInfo = new OAuthAuthorizedInfo()
            {
                AuthToken = fitbitInfo.FitbitAuthToken,
                AuthTokenSecret = fitbitInfo.FitbitAuthTokenSecret,
                UserId = fitbitInfo.FitbitUserId,
                VendorCode = _vendorCode[DeviceType.Fitbit],
            };

            return oAuthInfo;
        }

        private class Authenticator
        {
            private string ConsumerKey;
            private string ConsumerSecret;
            private string RequestTokenUrl;
            private string AccessTokenUrl;
            private string AuthorizeUrl;
            private string RequestToken;
            private string RequestTokenSecret;

            public Authenticator(string ConsumerKey, string ConsumerSecret, string RequestTokenUrl, string AccessTokenUrl, string AuthorizeUrl)
            {
                this.ConsumerKey = ConsumerKey;
                this.ConsumerSecret = ConsumerSecret;
                this.RequestTokenUrl = RequestTokenUrl;
                this.AccessTokenUrl = AccessTokenUrl;
                this.AuthorizeUrl = AuthorizeUrl;
            }

            /// <summary>
            /// Use this method first to retrieve the url to redirect the user to to allow the url.
            /// Once they are done there, Fitbit will redirect them back to the predetermined completion URL
            /// </summary>
            /// <returns></returns>
            public string GetAuthUrlToken()
            {
                return GenerateAuthUrlToken(false);
            }

            private string GenerateAuthUrlToken(bool forceLogoutBeforeAuth)
            {
                var baseUrl = "https://api.fitbit.com";
                var client = new RestClient(baseUrl);
                client.Authenticator = OAuth1Authenticator.ForRequestToken(this.ConsumerKey, this.ConsumerSecret);

                var request = new RestRequest("oauth/request_token", Method.POST);
                var response = client.Execute(request);

                var qs = HttpUtility.ParseQueryString(response.Content);
                RequestToken = qs["oauth_token"];
                RequestTokenSecret = qs["oauth_token_secret"];
                //we need to store this in cache until Fitbit returns that the accounts have been paired
                var authSecretObj = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.OAuthFitbitSecret);
                if ( authSecretObj != null )
                {
                    //update secret
                    CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.OAuthFitbitSecret, RequestTokenSecret);
                } else
                {
                    //insert
                    CacheProvider.Add(System.Web.HttpContext.Current, CacheProvider.CacheItems.OAuthFitbitSecret, RequestTokenSecret);
                }
                

                if(response.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception("Request Token Step Failed");
                }

                request = forceLogoutBeforeAuth ? new RestRequest("oauth/logout_and_authorize") : new RestRequest("oauth/authorize");
                request.AddParameter("oauth_token", RequestToken);
                request.AddParameter("oauth_token_secret", RequestTokenSecret);
                var url = client.BuildUri(request).ToString();

                return url;
            }

            public FitbitAuthInfo ProcessApprovedAuthCallback(string TempAuthToken, string Verifier, string tokenSecret)
            {
                //var verifier = "123456"; // <-- Breakpoint here (set verifier in debugger)
                const string baseUrl = "https://api.fitbit.com";
                var client = new RestClient(baseUrl);

                var request = new RestRequest("oauth/access_token", Method.POST);
                client.Authenticator = OAuth1Authenticator.ForAccessToken(ConsumerKey, ConsumerSecret, TempAuthToken, tokenSecret, Verifier);

                var response = client.Execute(request);

                var qs = HttpUtility.ParseQueryString(response.Content); //not actually parsing querystring, but body is formatted like htat
                var oauth_token = qs["oauth_token"];
                var oauth_token_secret = qs["oauth_token_secret"];
                var encoded_user_id = qs["encoded_user_id"];

                return new FitbitAuthInfo()
                {
                    FitbitAuthToken = oauth_token,
                    FitbitAuthTokenSecret = oauth_token_secret,
                    FitbitUserId = encoded_user_id
                };
            }
        }

        #endregion

        #region Endomondo       
        /// <summary>                                                                       
        /// Gets the endomondo URL.                                                         
        /// </summary>                                                                      
        private string GetEndomondoUrl()
        {
            OAuth1AuthenticationHelper authenticator = GetEndomondoAuthenticator();

            // Set the callback url to AddEndomondoToken method in this controller
            string callbackUrl = GetCallbackUrl("AddEndomondoToken");

            // Get the Endomondo authorization Url
            OAuthUrlToken urlToken = authenticator.GetAuthUrlToken(callbackUrl);

            //  Keep the Endomondo oAuth secret in the session cache (we will need it to get the Authorization token)
            CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.OAuthSecret, urlToken.Secret);

            // For now return the url only...
            return urlToken.Url;
        }

        /// <summary>                                                                       
        /// Gets the endomondo authenticator.                                               
        /// </summary>                                                                      
        private static OAuth1AuthenticationHelper GetEndomondoAuthenticator()
        {
            string emConsumerKey = ApplicationSettings.EndomondoConsumerKey;
            string emConsumerSecret = ApplicationSettings.EndomondoConsumerSecret;


            OAuth1AuthenticationHelper authenticator = new OAuth1AuthenticationHelper(emConsumerKey, emConsumerSecret,
                                                                "https://api.endomondo.com",
                                                                "https://www.endomondo.com",
                                                                "https://api.endomondo.com");
            return authenticator;
        }

        /// <summary>                                                                       
        /// Adds the endomondo token.                                                       
        /// </summary>                                                                      
        /// <param name="oauth_token">The oauth_token.</param>                              
        public ActionResult AddEndomondoToken(string oauth_token)
        {
            //  We will get here by being redirected from the Endomondo Authorization page

            try
            {
                string oauth_verfier = Request.Params["oauth_verifier"];

                // Retrieve the oAuth secret from the session cache...
                string oAuthSecret = (string)CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.OAuthSecret);

                // Get the oAuth token info...
                //string vendorCode = GetVendorCode(DeviceType.Fitbit)
                OAuth1AuthenticationHelper authenticator = GetEndomondoAuthenticator();
                OAuthAuthorizedInfo oAuthInfo = authenticator.ProcessApprovedAuthCallback(_vendorCode[DeviceType.Endomondo], oauth_token, oauth_verfier, oAuthSecret);

                // Remove the oAuth secret from the cache...
                CacheProvider.Remove(System.Web.HttpContext.Current, CacheProvider.CacheItems.OAuthSecret);

                // Call the Api.Activity to pair Endomondo account to VMH MemeberId...
                HttpStatusCode statusCode = PairMemberIdAndVendorAccount( oAuthInfo );
                bool success = (statusCode == HttpStatusCode.Created || statusCode == HttpStatusCode.OK);
                CacheProvider.Update( System.Web.HttpContext.Current,
                                      CacheProvider.CacheItems.ConnectToVendorStatusResponse, statusCode );
                CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.IsConnectedToEndomondo, success);
            }
            catch (Exception e)
            {
                // Something failed, do we just log it and continue without informing the user?

                _logger.Error("Failure in AddEndomondoToken", e);
            }

            return RedirectToAction("Devices");
        }

        //Removed since we have a generic one  DeleteMemberVendorsTokens
        //public ActionResult DeleteMemberEndomondoTokens(string vendorcode)
        //{
        //    try
        //    {
        //        if(!string.IsNullOrEmpty(vendorcode))
        //        {
        //            //  Configure the client with the cookies needed by the API...
        //            string baseUrl = ApplicationSettings.ApiActivityUrl;
        //            RestClient client = new RestClient(baseUrl);

        //            // DELETE api.activity/accounts?vendorcode=... http://localhost:2171/v1/accounts?vendorcode=ENDOM
        //            string requestAction = string.Format("{0}{1}", ApplicationSettings.ApiIsConnectedToVendor, vendorcode.ToUpper());
        //            RestRequest request = new RestRequest(requestAction, Method.DELETE);

        //            //  Create a cookie to include in the request for the API to authenticate...
        //            System.Web.HttpCookie sessionIdCookie = Request.Cookies["SessionID"];

        //            request.AddCookie(sessionIdCookie.Name, sessionIdCookie.Value);
        //            request.RequestFormat = DataFormat.Json;

        //            IRestResponse response = client.Execute(request);

        //            if(response.StatusCode != HttpStatusCode.OK)
        //            {
        //                // The request failed
        //                _logger.ErrorFormat("Failure in call to delete Api.Activity  - {0}. Url={1}", response.StatusDescription, client.BaseUrl);
        //                return RedirectToAction("Devices");
        //            }
        //            //if it got here - it was a success so far
        //            CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.IsConnectedToEndomondo, false);
        //            CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.ConnectToVendorStatusResponse, HttpStatusCode.OK);
        //        }
        //        _logger.Warn("Failure in call to delete Api.Activity to check if member is connected to vendor. Vendor info missing.");
        //        return RedirectToAction("Devices");
        //    }
        //    catch(Exception ex)
        //    {
        //        VirginLifecare.Exception.ExceptionHandler.ProcessException(new Exception("Error trying to determine if member's connected to vendor: " + vendorcode, ex));
        //        return RedirectToAction("Devices");
        //    }
        //}

        /// <summary>                                                                        
        /// Pairs the member identifier and vendor account.                                  
        /// </summary>                                                                       
        /// <param name="oAuthInfo">The oAuth authentication information.</param>            
        private HttpStatusCode PairMemberIdAndVendorAccount(OAuthAuthorizedInfo oAuthInfo)
        {
            try
            {
                //  Create a cookie to include in the request for the API to authenticate...
                System.Web.HttpCookie sessionIdCookie = Request.Cookies["SessionID"];

                //  Configure the client with the cookies needed by the API...
                string baseUrl = ApplicationSettings.ApiActivityUrl;
                RestClient client = new RestClient(baseUrl);

                // POST Api.Activity/v1/Accounts
                RestRequest request = new RestRequest("accounts", Method.POST);
                
                request.AddCookie(sessionIdCookie.Name, sessionIdCookie.Value);
                request.RequestFormat = DataFormat.Json;

                request.AddBody(oAuthInfo);

                IRestResponse response = client.Execute(request);

                if(response.StatusCode != HttpStatusCode.OK || response.StatusCode != HttpStatusCode.Created)
                { 
                    // The request failed
                    _logger.ErrorFormat("Failure in call to Api.Activity  - {0}. Url={1}", response.StatusDescription, client.BaseUrl);
                }
                return response.StatusCode;

            }
            catch (Exception e)
            {
                _logger.Error("Failed to Pair MemeberId with Vendor account");

                return HttpStatusCode.InternalServerError;
            }
        }
        /// <summary>
        /// determine if member has paired vendor account with Virgin Pulse account and if device info is stored in our database (VLC_Health.dbo.RemoteDevice)
        /// Reson for not adding this to BL layer: cannot use an unsigned dll inside BL layer - will throw error: Assembly generation failed -- Referenced assembly 'RestSharp' does not have a strong name
        /// </summary>
        /// <param name="vendorcode">can be: ENDOM; FITBT; RNKPR</param>
        /// <param name="sessionIdCookie">needed for authentication on the Api</param>
        /// <returns></returns>
        private bool IsMemberConnectedToVendorFeed(string vendorcode, System.Web.HttpCookie sessionIdCookie)
        {
            try
            {
                if(!string.IsNullOrEmpty(vendorcode))
                {
                    //  Configure the client with the cookies needed by the API...
                    string baseUrl = ApplicationSettings.ApiActivityUrl;
                    RestClient client = new RestClient(baseUrl);

                    // GET "/v1/accounts?vendorcode=ENDOM"
                    string requestAction = string.Format("{0}{1}", ApplicationSettings.ApiIsConnectedToVendor, vendorcode.ToUpper());
                    RestRequest request = new RestRequest(requestAction, Method.GET);

                    request.AddCookie(sessionIdCookie.Name, sessionIdCookie.Value);
                    request.RequestFormat = DataFormat.Json;

                    IRestResponse response = client.Execute(request);

                    if(response.StatusCode != HttpStatusCode.OK)
                    {
                        // The request failed
                        _logger.ErrorFormat("Failure in call to Api.Activity  - {0}. Url={1}", response.StatusDescription, client.BaseUrl);
                        return false;
                    }
                    //else it was a success request so interpret result
                    bool isConnected = Convert.ToBoolean( response.Content );
                    return isConnected;
                }
                _logger.Warn("Failure in call to Api.Activity to check if member is connected to vendor. Vendor info missing.");
                return false;
            }
            catch(Exception ex)
            {
                _logger.Warn("AccountController.IsMemberConnectedToVendorFeed : Error trying to determine if member is connected to vendor: " + vendorcode, ex);
                VirginLifecare.Exception.ExceptionHandler.ProcessException(new Exception("Error trying to determine if member is connected to vendor: " + vendorcode, ex));
                return false;
            }
        }
        #endregion Endomondo

        #region Vendors - Endomondo + Misfit
        //use to unpair a vendor account from VP account - delete vendor token from our database
        public ActionResult DeleteMemberVendorsTokens(string vendorcode)
        {
            try
            {
                if(!string.IsNullOrEmpty(vendorcode))
                {
                    //  Configure the client with the cookies needed by the API...
                    string baseUrl = ApplicationSettings.ApiActivityUrl;
                    RestClient client = new RestClient(baseUrl);

                    string requestAction = string.Format("{0}{1}", ApplicationSettings.ApiIsConnectedToVendor, vendorcode.ToUpper());
                    RestRequest request = new RestRequest(requestAction, Method.DELETE);

                    //  Create a cookie to include in the request for the API to authenticate...
                    System.Web.HttpCookie sessionIdCookie = Request.Cookies["SessionID"];

                    request.AddCookie(sessionIdCookie.Name, sessionIdCookie.Value);
                    request.RequestFormat = DataFormat.Json;

                    IRestResponse response = client.Execute(request);

                    if(response.StatusCode != HttpStatusCode.OK && response.StatusCode != HttpStatusCode.Created)
                    {
                        // The request failed
                        _logger.ErrorFormat("Failure in call to delete Api.Activity  - {0}. Url={1}", response.StatusDescription, client.BaseUrl);
                        return RedirectToAction("Devices");
                    }

                    //if it got here - it was a success so far
                    if ( vendorcode == _vendorCode[DeviceType.Endomondo] )
                    {
                        CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.IsConnectedToEndomondo, false);
                    } 
                    else if(vendorcode == _vendorCode[DeviceType.Misfit])
                    {
                        CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.IsConnectedToMisfit, false);
                    }
                    else if (vendorcode == _vendorCode[DeviceType.Fitbit])
                    {
                        CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.IsConnectedToFitbit, false);
                    }

                    CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.ConnectToVendorStatusResponse, HttpStatusCode.OK);
                    return RedirectToAction("Devices");
                }
                _logger.Warn("Failure in call to delete Api.Activity to check if member is connected to vendor. Vendor info missing.");
                return RedirectToAction("Devices");
            }
            catch(Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(new Exception("Error trying to determine if member's connected to vendor: " + vendorcode, ex));
                return RedirectToAction("Devices");
            }
        }

        /// <summary>
        /// use this to clear the session cached message for the Endomondo pairing response as soon as member sees it.
        /// </summary>
        /// <returns></returns>
        public JsonResult RemoveStatusFromCache()
        {
            var data = new object();
            CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.ConnectToVendorStatusResponse, "");
            //_logger.Warn("AccountController.AddPlayerToCart method failed - someone tryied adding a player to cart but required info was missing.");
            return Json(JsonConvert.SerializeObject(data = new { Success = true }), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Misfit
        /// <summary>                                                                       
        /// Gets the endomondo authenticator.                                               
        /// </summary>                                                                      
        private static IOAuth1AuthenticationHelper GetMisfitAuthenticator()
        {
            string msConsumerKey = ApplicationSettings.MisfitConsumerKey;
            string msConsumerSecret = ApplicationSettings.MisfitConsumerSecret;


            MisfitOAuthAuthenticationHelper authenticator = new MisfitOAuthAuthenticationHelper(msConsumerKey, msConsumerSecret,
                                                                "https://api.misfitwearables.com",
                                                                "https://api.misfitwearables.com",
                                                                "https://api.misfitwearables.com");
            return authenticator;
        }
        /// <summary>                                                                       
        /// Gets the endomondo URL.                                                         
        /// </summary>                                                                      
        public ActionResult PairVendorAccount(string vendorCode)
        {
            try
            {
                IOAuth1AuthenticationHelper authenticator = null;
                Authenticator fitbitAuthenticator = null;

                string callbackUrl = string.Empty;
                if ( vendorCode == _vendorCode[DeviceType.Endomondo] )
                {
                    authenticator = GetEndomondoAuthenticator();
                    // Set the callback url to AddEndomondoToken method in this controller
                    callbackUrl = GetCallbackUrl( "AddEndomondoToken" );
                } else if ( vendorCode == _vendorCode[DeviceType.Misfit] )
                {
                    authenticator = GetMisfitAuthenticator();
                    // Set the callback url to AddEndomondoToken method in this controller
                    callbackUrl = GetCallbackUrl( "AddMisfitToken" );
                }
                else if (vendorCode == _vendorCode[DeviceType.Fitbit])
                {
                    fitbitAuthenticator = FitbitAuthenticator();

                    callbackUrl = GetCallbackUrl("AddFitbitToken");
                }

                if ( authenticator != null )
                {
                    // Get the Endomondo authorization Url
                    OAuthUrlToken urlToken = authenticator.GetAuthUrlToken( callbackUrl );

                    //  Keep the Endomondo oAuth secret in the session cache (we will need it to get the Authorization token)
                    CacheProvider.Update( System.Web.HttpContext.Current, CacheProvider.CacheItems.OAuthSecret,
                                          urlToken.Secret );

                    // For now return the url only...
                    if ( !string.IsNullOrEmpty( urlToken.Url ) )
                    {
                        //redirect to urlToken
                       return Redirect( urlToken.Url );
                    }
                }
                else if (fitbitAuthenticator != null)
                {
                    string url = fitbitAuthenticator.GetAuthUrlToken();

                    // Fitbit Authenticator stores the oAuth secret inthe session cache (CacheProvider.CacheItems.OAuthFitbitSecret)

                    if (!string.IsNullOrEmpty(url) )
                    {
                        return Redirect(url);
                    }
                }

                // Something went wrong, store BadRequest in the status code and return to Devices...
                CacheProvider.Update( System.Web.HttpContext.Current,
                                      CacheProvider.CacheItems.ConnectToVendorStatusResponse, HttpStatusCode.BadRequest );
                return RedirectToAction( "Devices" );
            }
            catch ( Exception ex )
            {
                _logger.Error( "Something went wrong while trying to authenticate vendor's account for pairing. see exception below.", ex );
                CacheProvider.Update(System.Web.HttpContext.Current,
                                      CacheProvider.CacheItems.ConnectToVendorStatusResponse, HttpStatusCode.BadRequest);
                return RedirectToAction("Devices");
            }
        }

        /// <summary>                                                                       
        /// Adds the endomondo token.                                                       
        /// </summary>                                                                      
        /// <param name="code">The code provided by Misfit after authentication was successfull.</param>                              
        public ActionResult AddMisfitToken(string code)
        {
            //  We will get here by being redirected from the Misfit callback on Authorization page
            try
            {
                // Retrieve the oAuth secret from the session cache...
                string oAuthSecret = (string)CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.OAuthSecret);

                // Get the oAuth token info...
                IOAuth1AuthenticationHelper authenticator = GetMisfitAuthenticator();
                //OAuthAuthorizedInfo authInfo = oAuth.ProcessApprovedAuthCallback("MISFIT", accessCode, null, oAuthTokenSecret);
                OAuthAuthorizedInfo oAuthInfo = authenticator.ProcessApprovedAuthCallback(_vendorCode[DeviceType.Misfit], code, null, oAuthSecret);

                // Remove the oAuth secret from the cache...
                CacheProvider.Remove(System.Web.HttpContext.Current, CacheProvider.CacheItems.OAuthSecret);

                // Call the Api.Activity to pair Misfit account to VMH MemeberId...
                HttpStatusCode statusCode = PairMemberIdAndVendorAccount(oAuthInfo);
                bool success = (statusCode == HttpStatusCode.Created || statusCode == HttpStatusCode.OK);
                CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.ConnectToVendorStatusResponse, statusCode);
                CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.IsConnectedToMisfit, success);
            }
            catch(Exception e)
            {
                _logger.Error("Failure in AddMisfitToken", e);
            }

            return RedirectToAction("Devices");
        }
        #endregion

        #region Private metods - helpers
        private bool IsMemberRegisterToGZ()
        {
            return ActivityBL.IsMemberRegisterToGZ(VhmSession.MemberID);
        }
        private bool IsMemberLinkedToPolar()
        {
            return ActivityBL.IsMemberLinkedToPolar(VhmSession.MemberID);
        }
        private bool IsMemeberDashAppUser()
        {
            var isMemeberDashAppUser = false;
            try
            {
                if(SponsorProfile != null)
                {
                    isMemeberDashAppUser = SponsorProfile.AllowMobileAppAccess;
                }

            }
            catch(Exception ex)
            {
                _logger.Error(ex);
            }
            return isMemeberDashAppUser;
        }
        private bool IsMemberConnectedToVendor(string vendor)
        {
            try
            {
                //  Create a cookie to include in the request for the API to authenticate...
                System.Web.HttpCookie sessionIdCookie = Request.Cookies["SessionID"];
                if(sessionIdCookie != null && !string.IsNullOrEmpty(sessionIdCookie.Name))
                {
                    if(vendor == _vendorCode[DeviceType.Endomondo])
                    {
                        #region Endomondo
                        //check Cache to see if "IsConnectedToEndomondo" is populated; if so, get the value from there
                        var isConnectedToEndomondoObj = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.IsConnectedToEndomondo);
                        var isConnectedToEndomondo = false;
                        if(isConnectedToEndomondoObj != null)
                        {
                            isConnectedToEndomondo = bool.Parse(isConnectedToEndomondoObj.ToString());
                        }
                        else
                        {
                            isConnectedToEndomondo = IsMemberConnectedToVendorFeed(vendor, sessionIdCookie);
                            CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.IsConnectedToEndomondo, isConnectedToEndomondo);
                        }
                        return isConnectedToEndomondo;
                        #endregion
                    }
                    else if(vendor == _vendorCode[DeviceType.Misfit])
                    {
                        #region Misfit
                        //check Cache to see if "IsConnectedToMisfit" is populated; if so, get the value from there
                        var isConnectedToMisfitObj = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.IsConnectedToMisfit);
                        var isConnectedToMisfit = false;
                        if(isConnectedToMisfitObj != null)
                        {
                            isConnectedToMisfit = bool.Parse(isConnectedToMisfitObj.ToString());
                        }
                        else
                        {
                            isConnectedToMisfit = IsMemberConnectedToVendorFeed(vendor, sessionIdCookie);
                            CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.IsConnectedToMisfit, isConnectedToMisfit);
                        }
                        return isConnectedToMisfit;
                        #endregion
                    }
                    //if vendor diferent than endomondo/misfit - check via api
                    return IsMemberConnectedToVendorFeed(vendor, sessionIdCookie);
                }
                _logger.Error("Failed to check if IsMemberConnectedToVendor for Vendor: " + vendor + ". Session cookie is not valid");
                return false;
            }
            catch(Exception ex)
            {
                _logger.Error("ActivityController. IsMemberConnectedToVendor - Something went wrong. Failed to check if IsMemberConnectedToVendor for Vendor: " + vendor + ". Exception thrown, see exception below:", ex);
                return false;
            }
        }
        #endregion

        /// <summary>
        /// Gets the callback URL. 
        /// </summary>
        /// <param name="actionName"></param>
        /// <returns></returns>
        private string GetCallbackUrl(string actionName)
        {
            string currentUrl = this.Url.Action(actionName, "Activity", null, this.Request.Url.Scheme);

            return currentUrl;
        }

        /// <summary>
        /// Shows the connect to provider button.
        /// </summary>
        /// <param name="device">Fitbit, Endomondo, Misfit</param>
        /// <returns></returns>          
        private bool ShowConnectToProviderButton(DeviceType device)
        {
            //  The logic below applies to the following providers: Fitbit, Endomondo, Misfit
            //we added ApplicationSettings.SponsorIdsWithFitBitConnection = comma delimited string of Sponsor id's for which we need the "Connect to FitBit" Button to display
            //if ApplicationSettings.ShowDevicesFitbitSection == "true" -> "Connect to FitBit" displays to ALL sponsors, no need to check ApplicationSettings.SponsorIdsWithFitBitConnection
            //if ApplicationSettings.ShowDevicesFitbitSection == "false" -> we need to check and see if member's sponsor ID is listed in the ApplicationSettings.SponsorIdsWithFitBitConnection
            //if ApplicationSettings.ShowDevicesFitbitSection == "false" and ApplicationSettings.SponsorIdsWithFitBitConnection == string.Empty, we do not show the "Connect to FitBit" to any memeber

            string showDeviceSection = GetShowDeviceSectionSetting(device);
            bool showVendorSectionWebConfig =  (String.Compare(showDeviceSection, "true", StringComparison.OrdinalIgnoreCase) == 0); // compare the setting value case insensitive.
            //(string.Compare(showDeviceSection, "true", true) == 0); // compare the setting value case insensitive.
            if (showVendorSectionWebConfig)
            {
                return true;
            }
            string sponsorIdsWithVendorConnection = GetSponsorIdsWithDeviceConnection(device);
            if(!string.IsNullOrEmpty(sponsorIdsWithVendorConnection) && sponsorIdsWithVendorConnection.Trim() != string.Empty)
            {
                List<long> sponsorIds = sponsorIdsWithVendorConnection.Split(',')
                    .Select(x => x.Trim())
                    .Select(long.Parse).ToList();

                if(sponsorIds.Count > 0 && sponsorIds.Contains(VhmSession.PrimarySponsorID))
                {
                    // check if member's primary sponsor id is listed in the sponsorIds
                    return true;
                }
                return false;
            }
            return false; //ApplicationSettings.ShowDevicesFitbitSection == "false" && ApplicationSettings.SponsorIdsWithFitBitConnection == "";
        }

        /// <summary>
        /// Gets the sponsor ids with device connection passed in the web.config keys for vendors (SponsorIdsWithFitBitConnection, SponsorIdsWithEndomondoConnection, SponsorIdsWithMisfitConnection).
        /// </summary>
        /// <param name="device">Fitbit, Endomondo, Misfit</param>
        /// <returns></returns>                                
        private string GetSponsorIdsWithDeviceConnection(DeviceType device)
        {
            switch (device)
            {
                case DeviceType.Fitbit:
                    return (ApplicationSettings.SponsorIdsWithFitBitConnection != null && !string.IsNullOrEmpty(ApplicationSettings.SponsorIdsWithFitBitConnection))
                        ? ApplicationSettings.SponsorIdsWithFitBitConnection
                        : string.Empty;

                case DeviceType.Endomondo:
                    return (ApplicationSettings.SponsorIdsWithEndomondoConnection != null && !string.IsNullOrEmpty(ApplicationSettings.SponsorIdsWithEndomondoConnection))
                        ? ApplicationSettings.SponsorIdsWithEndomondoConnection
                        : string.Empty;
                case DeviceType.Misfit:
                    return (ApplicationSettings.SponsorIdsWithMisfitConnection != null && !string.IsNullOrEmpty(ApplicationSettings.SponsorIdsWithMisfitConnection))
                        ? ApplicationSettings.SponsorIdsWithMisfitConnection
                        : string.Empty;
                default:
                    throw new Exception(string.Format("Device not supported. Device={0}", device));
            }
        }

        /// <summary>
        /// Gets the show device section setting from the web config.
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        private string GetShowDeviceSectionSetting(DeviceType device)
        {
            switch (device)
            {
                case DeviceType.Fitbit:
                    return (ApplicationSettings.ShowDevicesFitbitSection!= null && !string.IsNullOrEmpty(ApplicationSettings.ShowDevicesFitbitSection)) 
                        ?  ApplicationSettings.ShowDevicesFitbitSection 
                        : "false";

                case DeviceType.Endomondo:
                    return (ApplicationSettings.ShowDevicesEndomondoSection != null && !string.IsNullOrEmpty(ApplicationSettings.ShowDevicesEndomondoSection))
                        ? ApplicationSettings.ShowDevicesEndomondoSection
                        : "false";

                case DeviceType.Misfit:
                    return (ApplicationSettings.ShowDevicesMisfitSection != null && !string.IsNullOrEmpty(ApplicationSettings.ShowDevicesMisfitSection))
                        ? ApplicationSettings.ShowDevicesMisfitSection
                        : "false";

                default:
                    throw new Exception(string.Format("Device not supported. Device={0}", device) );
            }
        }

        #region Device/Vendor helpers
        private enum DeviceType { Fitbit, Endomondo, Misfit };

        // The Vendor Codes bellow must match the RemoteDeviceTypes in Api.Activity.Core.Model
        private static readonly Dictionary<DeviceType, string> _vendorCode = new Dictionary<DeviceType, string>()
        {
            {DeviceType.Endomondo, "ENDOM"},
            {DeviceType.Fitbit, "FITBT"},
            {DeviceType.Misfit, "MISFT"}
        };
        #endregion
    }
}
