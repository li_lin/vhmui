﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Vhm.Web.BL;
using Vhm.Web.Common;
using Vhm.Web.Models.ViewModels.Challenges;

namespace VirginLifecareWeb.Controllers
{
    public class ChallengesController : ControllerBase
    {
        //
        // GET: /Challenges/

        public JsonResult GetChallenges()
        {
            var personalChallenges = ChallengeBL.GetMemberAvailableChallengesByType( VhmSession.MemberID, "TRK" );
            if ( personalChallenges == null || personalChallenges.Count == 0 )
            {
                return null;
            }

            // select the challenge IDs to an array and put them in order of most recent first
            var challengeIds = ( from challenge in personalChallenges
                select challenge.ChallengeId ).OrderByDescending( id => id ).ToArray();

            var challenges = ChallengeBL.GetChallenges( challengeIds );
            
            var model = new List<PersonalChallenges>();

            foreach ( var item in challenges )
            {

                if ( item.EndDate < DateTime.Now )
                {
                    item.HasFinished = true;
                }
                if ( item.StartDate > DateTime.Now )
                {
                    item.HasStarted = false;
                } else
                {
                    item.HasStarted = true;
                }

                var days = new ChallengeScoreInfo();
                var trackedDays = from cs in new List<ChallengeScore>()
                                  select new
                                  {
                                      Day = string.Empty,
                                      Answer = bool.FalseString,
                                      MemberId = new long(),
                                      ChallengeId = new long(),
                                      ChallengeScoreId = new long(),
                                      ScoreDate = DateTime.Now,
                                      Value = new decimal?(),
                                      Score = new decimal(),
                                      IsMemberAnswer = false
                                  };
                int TodaysTrackedDayInfo = 0;

                    var badge = RewardsBL.GetBadge(
                       item.ChallengeQuestions[0].ChallengeTasks[0].ChallengeTaskRewards[0].BadgeID.GetValueOrDefault());

                try
                {
                    days = ChallengeBL.GetTrackedDays(item.StartDate, item.EndDate.GetValueOrDefault(), item.ChallengeId, VhmSession.MemberID);
                    trackedDays = days.ChallengeScores.Select( cs => new
                    {
                        Day = cs.ScoreDate.ToShortDateString(),
                        Answer = cs.Value.ToString(),
                        cs.MemberId,
                        cs.ChallengeId,
                        ChallengeScoreId = cs.ChallengeScoreID,
                        cs.ScoreDate,
                        cs.Value,
                        cs.Score,
                        IsMemberAnswer = cs.Value != null
                    } );
                    int i = 0;
                    foreach ( var scoreItem in trackedDays )
                    {
                        if ( scoreItem.Day == DateTime.Now.ToShortDateString() )
                            TodaysTrackedDayInfo = i;
                        i++;
                    }
                    
                }
                catch (Exception ex)
                {
                    VirginLifecare.Exception.ExceptionHandler.ProcessException(new Exception("Error getting challenge tracked days", ex));
                }

                var trackedDaysWithYesCount = new
                {
                    TodaysTrackedDayIndex = TodaysTrackedDayInfo,
                    TrackedDays = trackedDays,
                    DaysOfYes = days.ScoreCount
                };

                var challenge = new PersonalChallenges
                {
                    ChallengeId = item.ChallengeId,
                    Name = item.Name,
                    RulesCopy = item.RulesCopy,
                    ChallengeQuestions = item.ChallengeQuestions,
                    TrackedDaysInfo = trackedDaysWithYesCount,
                    HasFinished = item.HasFinished,
                    HasStarted = item.HasStarted,
                    BadgeFileName = badge != null ? ApplicationSettings.BadgeImageUrl + new Guid(badge.BadgeImage) : string.Empty
                };
                model.Add(challenge);
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public long SaveChallengeScore(long challengeScoreId, long challengeId, DateTime scoreDate, int answer, int score, bool isMemberAnswer)
        {
            long returnScoreId = challengeScoreId;
            try
            {

                var challengeScore = new ChallengeScore
                {
                    ChallengeScoreID = challengeScoreId,
                    ChallengeId = challengeId,
                    MemberId = VhmSession.MemberID,
                    ScoreDate = scoreDate,
                    Score = score, // from answer key
                    Value = answer  // what the person typed in
                };

                if (isMemberAnswer)
                {
                    // they are updating their score for this day
                    ChallengeBL.UpdateChallengeScore(challengeScore);
                }
                else
                {
                    // this is the first time they are entering a score for this day
                    returnScoreId = ChallengeBL.AddChallengeScore(challengeScore);
                }
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
            }

            return returnScoreId;
        } 

    }
}
