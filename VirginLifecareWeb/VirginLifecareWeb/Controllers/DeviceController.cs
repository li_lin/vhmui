﻿using System.Web.Mvc;
using System.Web.Script.Serialization;
using Vhm.Web.BL;
using Vhm.Web.Common;
using Vhm.Web.Models.ViewModels.Device;
using Vhm.Web.Models.ViewModels.Shared;

namespace VirginLifecareWeb.Controllers
{
    public class DeviceController : ControllerBase
    {
        [ErrorHandle(Action = "Error")]
        public ActionResult Configuration(string jsonData)
        {
            var viewModel = new SettingsVM
            {
                PageT = new PageTitle(true, "Configuration"),
                MemberDeviceSettingsPath = ApplicationSettings.MemberDeviceSettingsPath
            };
            return View(viewModel);
        }

        [ErrorHandle(Action = "Error")]
        public ActionResult MemberDeviceSettings()
        {
            var selectedColorTheme = GetSelectedTheme();

            var vacSponsorModule = new MemberDeviceSettingsBL().GetSponsorValidatedActivityModule( SponsorProfile.SponsorID );
            var viewModel = new SettingsVM
                {
                    PageT = new PageTitle(true, ContentHeader[HeaderContent.DeviceSettings]),
                    ThemeCode = selectedColorTheme,
                    SponsorModule = vacSponsorModule,
                    Content = MemberBL.GetContentLabels<DeviceMemberSettingsContentLabels>(PageNames.DeviceMemberSettingsContentLabels, LanguageId),
                    CommonContent = MemberBL.GetContentLabels<CommonLabelsContent>(PageNames.CommonLabels, LanguageId),
                    MemberDeviceSettingsPath = ApplicationSettings.MemberDeviceSettingsPath
                };
            return View(viewModel);
        }

        public JsonResult SaveMemberSettings(string settingsRequest)
        {
            if (!string.IsNullOrEmpty(settingsRequest))
            {
                var settingsObject = new JavaScriptSerializer().Deserialize<VhmMemberMaxSettings>(settingsRequest);
                settingsObject.MemberId = VhmSession.MemberID;
                settingsObject = MemberDeviceSettingsBL.SaveMemberDeviceSettigsBL(settingsObject, "PUT", ApplicationSettings.SettingsApiUri);
                return Json(settingsObject, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetMemberSettings()
        {
            var url = ApplicationSettings.SettingsApiUri + VhmSession.MemberID;
            VhmMemberMaxSettings memberMaxSettings = MemberDeviceSettingsBL.GetMemberDeviceSettingsBL(url);
            return Json(memberMaxSettings, JsonRequestBehavior.AllowGet);
        }
    }
}
