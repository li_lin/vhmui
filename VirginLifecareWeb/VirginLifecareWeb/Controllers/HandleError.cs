﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.ServiceModel.Dispatcher;

namespace VirginLifecareWeb.Controllers
{
    public class ErrorHandleAttribute : HandleErrorAttribute // ActionFilterAttribute //
    {
        public string WhatToDo { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
        public bool RedirectPreviousPage { get; set; }

        public override void OnException(ExceptionContext filterContext)
        {
            if (RedirectPreviousPage)
            {
                filterContext.RequestContext.HttpContext.Response.Redirect(filterContext.RequestContext.HttpContext.Request.UrlReferrer.ToString());
                filterContext.Controller.TempData.Add("Errors","Something went wrong");
            }
            filterContext.ExceptionHandled = true;
        }
    }
}