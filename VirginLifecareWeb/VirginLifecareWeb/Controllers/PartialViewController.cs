﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Vhm.Web.BL;
using Vhm.Web.Models.InputModels;
using System.Globalization;

namespace VirginLifecareWeb.Controllers
{
    public class PartialViewController : ControllerBase
    {
        //[ChildActionOnly]
        //public ActionResult Invite(long memberId)
        //{
        //    var viewModel = PartialViewsBL.GetInviteFriendsGroupsVM("", new PagingIM());

        //    return View("Invite", viewModel);
        //}

        public ActionResult PopulateInvitedFriendsList(List<long> memberIds, List<long> groupIds, List<long> existingList)
        {
            var memberId = VhmSession.MemberID;
            var sessionId = VhmSession.SessionID;
            var lst = PartialViewsBL.GetNewInviteList(sessionId, memberId, memberIds, groupIds, existingList);
            
            return Json(lst.Select(m => new { m.Member.MemberId, m.Member.FirstName, m.Member.LastName}));
        }

        [HttpPost]
        public string UploadCSVFile(HttpPostedFileBase fileBase)
        {
            var csvStreamReader = new StreamReader(fileBase.InputStream);
            var mIds = PartialViewsBL.GetMemeberIdFromCSV(VhmSession.SessionID, csvStreamReader);
            var serializer = new JavaScriptSerializer();

            return serializer.Serialize(mIds);
        }

        public ActionResult GetMoreFriends(PagingIM paging)
        {
            var sessionId = VhmSession.SessionID;
            paging.MemberId = VhmSession.MemberID;
            var lst = PartialViewsBL.GetMoreFriends(sessionId, paging);
            
            return Json(lst.Select(m => new { Id = m.Member.MemberId, m.Member.FirstName, m.Member.LastName, m.Member.ImageUrl, Sponsor = m.Member.Sponsors.Split(',').First() }));
        }

        public ActionResult GetMoreGroups(PagingIM paging)
        {
            var sessionId = VhmSession.SessionID;
            paging.MemberId = VhmSession.MemberID;
            var lst = PartialViewsBL.GetMoreGroups(sessionId, paging, false);

            return Json(lst.Select(g => new { g.Group.Id, g.Group.Name, g.Group.Category, g.Group.ProfilePicturePath }));
        }

        public string GetMemberImage(long id)
        {
		    if (!String.IsNullOrEmpty(Request.Headers["If-Modified-Since"]))
		    {
			    CultureInfo provider = CultureInfo.InvariantCulture;
			    var lastMod = DateTime.ParseExact(Request.Headers["If-Modified-Since"], "r", provider).ToLocalTime();
			    if (DateTime.Now.CompareTo(lastMod) < 30)
			    {
				    Response.StatusCode = 304;
				    Response.StatusDescription = "Not Modified";
				    //return Content(String.Empty);
			    }
		    }
            var imagePath = PartialViewsBL.GetMemberImagePath(id);
            return imagePath;// base.File( imagePath, "image/jpeg" );
        }

        //this method is used by some of the front end functionality, so we'll return a string with the image path
        public ActionResult GetGroupImage(long id)
        {

	        if (!String.IsNullOrEmpty(Request.Headers["If-Modified-Since"]))
	        {
		        CultureInfo provider = CultureInfo.InvariantCulture;
		        var lastMod = DateTime.ParseExact(Request.Headers["If-Modified-Since"], "r", provider).ToLocalTime();
		        if (DateTime.Now.CompareTo(lastMod) < 30)
		        {
			        Response.StatusCode = 304;
			        Response.StatusDescription = "Not Modified";
			        return Content(String.Empty);
		        }
	        }

            var defaultImagePath = Server.MapPath("~/Content/images/social/img-no-group.png");
            var imageData = PartialViewsBL.GetGroupImage(id, defaultImagePath);
	        var stream = new MemoryStream(imageData);
	        Response.Cache.SetCacheability(HttpCacheability.Public);
	        Response.Cache.SetLastModified(DateTime.Now);
	        return File(stream, "image/jpeg");
        }

        //this method is used by some of the front end functionality, so we'll return a string with the image path
        public ActionResult GetGroupImagePath(long id)
        {

	        if (!String.IsNullOrEmpty(Request.Headers["If-Modified-Since"]))
	        {
		        CultureInfo provider = CultureInfo.InvariantCulture;
		        var lastMod = DateTime.ParseExact(Request.Headers["If-Modified-Since"], "r", provider).ToLocalTime();
		        if (DateTime.Now.CompareTo(lastMod) < 30)
		        {
			        Response.StatusCode = 304;
			        Response.StatusDescription = "Not Modified";
			        return Content(String.Empty);
		        }
	        }
            var imageData = PartialViewsBL.GetGroupImagePath(id);
            return File(imageData, "image/jpeg");
        }

        /// <summary>
        /// Get the profile with the appropriate permissions for the current user.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult GetMemberProfile(long id)
        {
            var sessionId = VhmSession.SessionID;
            var currentMemberId = VhmSession.MemberID;
            var memberVm = SocialBL.GetMemberProfile(sessionId, id, currentMemberId, false);
            memberVm.Culture = new CultureInfo(VhmSession.Culture);

            return View("Member/MemberProfile", memberVm);
        }

        public ActionResult GetMembersByEmailsOrIdentityNumbers(IEnumerable<string> emailsAndidentityNumbers)
        {
            var members = SocialBL.GetMembersByEmailsOrIdentityNumbers(VhmSession.SessionID, emailsAndidentityNumbers);
            var mIds = members.Select(m => m.Member.MemberId).ToList();

            return Json(mIds);
        }
    }
}
