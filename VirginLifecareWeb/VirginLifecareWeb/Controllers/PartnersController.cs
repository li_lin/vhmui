﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Vhm.Providers;
using Vhm.Web.BL;
using Vhm.Web.Common;
using Vhm.Web.Models.RepositoryModels;
using Vhm.Web.Models.ViewModels.Shared;

namespace VirginLifecareWeb.Controllers
{
    public class PartnersController : ControllerBase
    {
        private const int PROGRAMS_MODULE_ID = 8; //programs modules will have an id greater than 8 -> 9, ...
        
        public PartnersController()
        {
        }

        public ActionResult Index()
        {
            var selectedColorTheme = GetSelectedTheme();
            var model = new PartnerOverviewVM { PageTitle = new PageTitle(true, "Programs Overview"), ThemeCode = selectedColorTheme };

            return View(model);
        }

        [ErrorHandle(Action = "Error")]
        public ActionResult PartnersPage(long sponsorModuleId)
        {
            var modules = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.MemberModules) as List<MemberModule> ??
                         MemberBL.GetMemberModules(VhmSession.MemberID, VhmSession.PrimarySponsorID);

            CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.MemberModules, modules);
            #region Check if member has any Programs modules available

            var programModules = (from m in modules
                                 where m.ModuleID > PROGRAMS_MODULE_ID
                                 select m).ToList();
            var hasProgramModulesAvailable = (programModules.Any());
            var partnerModule = new PartnerModule();
            if (hasProgramModulesAvailable)
            {
                var membModule = (from m in programModules
                                 where m.SponsorModuleId == sponsorModuleId
                                 select m).ToList();
                if (membModule.Any())
                {
                    var imgGuid = new Guid(membModule.First().ModuleImage);
                    partnerModule.MaskModuleName = membModule.First().MaskModuleName;
                    partnerModule.PartnerGUID = membModule.First().PartnerGUID;
                    partnerModule.IsPartner = membModule.First().IsPartner;
                    partnerModule.URL = membModule.First().URL;
                    partnerModule.ModuleImageSrc = string.Format( "{0}{1}.png",
                                                                 ApplicationSettings.ModuleImageUrl,
                                                                  imgGuid );
                    partnerModule.PartnerGUID = membModule.First().PartnerGUID;
                    partnerModule.PageTitle = new PageTitle(true, membModule.First().MaskModuleName);
                }
            }
            #endregion

            var selectedColorTheme = GetSelectedTheme();
            var model = new PartnerModuleVM
                            {
                                Module = partnerModule,
                                ThemeCode = selectedColorTheme
                            };

            return View(model);
        }

    }
}
