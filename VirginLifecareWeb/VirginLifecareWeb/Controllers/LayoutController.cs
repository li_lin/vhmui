﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Vhm.Providers;
using Vhm.Web.BL;
using Vhm.Web.Common;
using Vhm.Web.Models.Enums;
using Vhm.Web.Models.RepositoryModels;
using Vhm.Web.Models.ViewModels.Shared;
using VirginLifecareWeb.Controllers.ActionResults;
using VirginLifecare.Utils;
using System.Configuration;

namespace VirginLifecareWeb.Controllers
{
    [Authorize]
    public class LayoutController : ControllerBase
    {
        public bool EnableNewLanding
        {
            get
            {
                if (VhmSession == null || string.IsNullOrEmpty(VhmSession.SessionID))
                {
                    return false;
                }

                #region check if is express test sponsor
                var member = MemberBL.GetMemberById(VhmSession.SessionID, VhmSession.MemberID);
                var game = RewardsBL.GetSponsorCurrentGame(VhmSession.SessionID, member, VhmSession.PrimarySponsorID);
                if (game != null && game.ProgramType == "EXP")
                {
                    return true;
                }
                #endregion
                string[] betaUIForSponsor = ConfigurationManager.AppSettings["BetaUIForSponsor"].Split(',');
                bool useNewLandingPage = false;
                var httpCookie = Request.Cookies["NewLanding" + VhmSession.MemberID.ToString()];

                useNewLandingPage = betaUIForSponsor.Any(s => s == VhmSession.PrimarySponsorID.ToString());

                if (httpCookie != null && !string.IsNullOrEmpty(httpCookie.Value) && useNewLandingPage)
                {
                    if (httpCookie.Value == "yes")
                    {
                        ViewBag["EnableNewLanding"] = true;
                        return true;
                        
                    }
                    ViewBag["EnableNewLanding"] = false;
                    return false;
                }
                ViewBag["EnableNewLanding"] = false;
                return false;
            }
        }
        private Dictionary<HeaderContent, string> GetHeaderContent()
        {

            var headerContent = MemberBL.GetContentLabels<HeaderContent>(PageNames.PageHeader, LanguageId);

            return headerContent;
        }

        public ActionResult Header()
        {
            var viewModel = new HeaderViewModel();

            SponsorProfile = GetSponsorProfile();
            //the default Standard red color theme
            var selectedColorTheme = new ColorTheme { ColorThemeCode = "red", ColorThemeName = "Standard Red", BrandingThemeId = 1, isDefault = true };
            if (VhmSession.PrimarySponsorID > 0)
            {
                selectedColorTheme = ColorThemes.FirstOrDefault(ct => ct.BrandingThemeId == SponsorProfile.BrandingThemeID);
            }
            viewModel.ThemeCode = selectedColorTheme;

            var count = SocialBL.GetMemberNotificationCount(VhmSession.MemberID);
            viewModel.UnreadNotificationCount = count;

            var headerContent = GetHeaderContent();

            // Try to get the main menu from cache. If not there, rebuild it from the database and store it in cache.
            var mainMenu = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.MvcMenu) as MainMenuItem;
            if (mainMenu != null)
            {
                viewModel.MainMenu = mainMenu;
                viewModel.Content = headerContent;
            }
            else
            {
                #region Build main menu

                var modules = GetModules();
                viewModel.Content = headerContent;

                var challenges = MemberBL.GetMemberChallenges(VhmSession.MemberID, VhmSession.PrimarySponsorID, ApplicationSettings.ChallengeEndedDisplayDays, VhmSession.LanguageID);
                var crpChallenges = new List<ChallengeBase>();
                crpChallenges.AddRange(challenges);
                CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.CorporateChallenges, crpChallenges);
                
                bool hasBrowseChallenges = false;
                try
                {
                    hasBrowseChallenges = challenges.Any( c => c.ShowInBrowseOnly );
                    // remove corporate tracking challenges for which the member has not yet signed up
                    challenges.RemoveAll(c => c.ShowInBrowseOnly && !c.IsMemberEnrolled);

                    CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.MemberChallenges, challenges);
                }
                catch (Exception ex)
                {
                    VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
                    challenges = new List<ChallengeBase>();
                }
                
                challenges = challenges.Where(c => c.MenuDisplay).ToList();

                var personChallValue = modules.Count(m => m.ModuleID == -1) > 0;

                var dsMenu = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.MainMenu) as DataSet;
                if (dsMenu == null)
                {
                    dsMenu = InitMenuDataSet(modules, headerContent);
                    using (var dt = dsMenu.Tables[1])
                    {
                        var arrChallengeMenuItems = GetChallengeMenuItems(personChallValue, challenges, headerContent, modules, hasBrowseChallenges);

                        foreach (object t in arrChallengeMenuItems)
                        {
                            var challengeMenuItem = (ChallengeMenuItem)t;
                            var nrq = dt.NewRow();
                            nrq["Name"] = challengeMenuItem.CodeName;
                            nrq["Desc"] = challengeMenuItem.Name;
                            nrq["NavUrl"] = challengeMenuItem.Link;
                            nrq["NavCategory_Id"] = 3;
                            dt.Rows.Add(nrq);
                        }
                    }
                    CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.MainMenu, dsMenu);
                }

                mainMenu = BuildMenuItems(dsMenu, headerContent);
                viewModel.MainMenu = mainMenu;

                

                CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.MvcMenu, mainMenu);
                #endregion
            }
            #region Build the Branding section
            //make sure profile exists
            if (SponsorProfile != null && SponsorProfile.SponsorID == VhmSession.PrimarySponsorID)
            {
                if (SponsorProfile.UseCustomBrand)
                {
                    #region build sponsor logo, if any available from database
                    //populate the Use custom brand properties
                    if (SponsorProfile.LogoType != null && SponsorProfile.SponsorLogo != null && SponsorProfile.SponsorLogo.Length > 1)
                    {
                        //if sponsor has a logo uploaded in database
                        if (SponsorProfile.LogoType == SponsorLogoType.TXT || SponsorProfile.LogoType == SponsorLogoType.TEXT)
                        {
                            viewModel.SponsorImageUrl = string.Empty;
                            viewModel.SponsorLogoType = "text";
                            viewModel.SponsorText = Encoding.ASCII.GetString(SponsorProfile.SponsorLogo);
                        }
                        else //it can only be an image if not text
                        {
                            //pass random parameter to prevent cached data
                            viewModel.SponsorImageUrl = "/v2/Image/LogoDisplay?r=" + DateTime.Now.Ticks;
                            viewModel.SponsorLogoType = "image";
                            viewModel.SponsorText = string.Empty;
                        }
                    }
                    #endregion
                }
                else
                {
                    #region build VHM logo link and check if sponsor has "Brought to you by"
                    #region check if brought to you by and populate properties
                    if (SponsorProfile.LogoType != null && SponsorProfile.SponsorLogo != null && SponsorProfile.SponsorLogo.Length > 1)
                    {
                        viewModel.SponsorDefaultBroughtToYouByText =
                            headerContent[HeaderContent.BroughtToYouBy];

                        //if sponsor has a logo uploaded in database
                        viewModel.ShowBroughtToYouBy = true;
                        if (SponsorProfile.LogoType == SponsorLogoType.TXT || SponsorProfile.LogoType == SponsorLogoType.TEXT)
                        {
                            viewModel.SponsorBroughtToYouByUrl = string.Empty;
                            viewModel.SponsorBroughtToYouByText = Encoding.ASCII.GetString(SponsorProfile.SponsorLogo);
                            viewModel.SponsorLogoType = "text";
                        }
                        else //it can only be an image if not text
                        {
                            //pass random parameter to prevent cached data
                            viewModel.SponsorBroughtToYouByUrl = "/v2/Image/LogoDisplay?r=" + DateTime.Now.Ticks;
                            viewModel.SponsorBroughtToYouByText = string.Empty;
                            viewModel.SponsorLogoType = "image";
                        }
                    }
                    else
                    {
                        viewModel.ShowBroughtToYouBy = false;
                        viewModel.SponsorBroughtToYouByUrl = string.Empty;
                        viewModel.SponsorBroughtToYouByText = string.Empty;
                        viewModel.SponsorBroughtToYouByLogoType = string.Empty;
                    }
                    #endregion
                    //populate the Virgin Brand properties
                    viewModel.VhmLogoUrl = string.Format("{0}", ApplicationSettings.VhmLogoImageURL);
                    #endregion
                }
                viewModel.UseCustomBrand = SponsorProfile.UseCustomBrand;
            }
            #endregion

            var sessionId = VhmSession.SessionID;
            var memberID = VhmSession.MemberID;
            var member = SocialBL.GetMemberProfile(sessionId, memberID, memberID, false);

            viewModel.MemberId = memberID;
            viewModel.MemberFullName = string.Format("{0} {1}", member.Member.FirstName, member.Member.LastName);
            viewModel.Permissions = VhmSession.Permission;
            var imageUrl = member.Member.ImageUrl;
            viewModel.MemberImageUrl = imageUrl.StartsWith("http") ? imageUrl : ApplicationSettings.CDNBaseURL + imageUrl;
            viewModel.ShowSiteNavigationMenu = ShowSiteNavigationDropDown();
            viewModel.Content = headerContent;
            viewModel.ShowMemberDeviceSettingsLink = ShowMemberDeviceSettingsLink();
            

            List<SponsorModule> sponsorModules = SponsorBL.GetSponsorModules(VhmSession.PrimarySponsorID);
            if (sponsorModules.Any(x => x.ModuleTypeCode == "LEA"))
            {
                if (viewModel.MainMenu.ChildItems.Last().Text != "Learning")
                viewModel.MainMenu.ChildItems.Add(new MainMenuItem { Text = "Learning", Url = "../../cms/learning/" });
            }
   
            return PartialView(viewModel);
        }

        public ActionResult HeaderV3()
        {
            var viewModel = new HeaderViewModel();

            SponsorProfile = GetSponsorProfile();
            //the default Standard red color theme
            var selectedColorTheme = new ColorTheme { ColorThemeCode = "red", ColorThemeName = "Standard Red", BrandingThemeId = 1, isDefault = true };
            if (VhmSession.PrimarySponsorID > 0)
            {
                selectedColorTheme = ColorThemes.FirstOrDefault(ct => ct.BrandingThemeId == SponsorProfile.BrandingThemeID);
            }
            viewModel.ThemeCode = selectedColorTheme;

            var count = SocialBL.GetMemberNotificationCount(VhmSession.MemberID);
            viewModel.UnreadNotificationCount = count;

            var headerContent = GetHeaderContent();

            // Try to get the main menu from cache. If not there, rebuild it from the database and store it in cache.
            var mainMenu = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.MvcMenu) as MainMenuItem;
            if (mainMenu != null)
            {
                viewModel.MainMenu = mainMenu;
                viewModel.Content = headerContent;
            }
            else
            {
                #region Build main menu

                var modules = GetModules();
                viewModel.Content = headerContent;

                var challenges = MemberBL.GetMemberChallenges(VhmSession.MemberID, VhmSession.PrimarySponsorID, 7, VhmSession.LanguageID);
                var crpChallenges = new List<ChallengeBase>();
                crpChallenges.AddRange(challenges);
                CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.CorporateChallenges, crpChallenges);
                
                bool hasBrowseChallenges = challenges.Any(c => c.ShowInBrowseOnly);
                
                try
                {
                    // remove corporate tracking challenges for which the member has not yet signed up
                    challenges.RemoveAll(c => c.ShowInBrowseOnly && !c.IsMemberEnrolled);
                        
                    CacheProvider.Update( System.Web.HttpContext.Current, CacheProvider.CacheItems.MemberChallenges,
                                            challenges );
                }
                catch ( Exception ex )
                {
                    VirginLifecare.Exception.ExceptionHandler.ProcessException( ex );
                    challenges = new List<ChallengeBase>();
                }
                
                challenges = challenges.Where(c => c.MenuDisplay).ToList();

                var personChallValue = modules.Count(m => m.ModuleID == -1) > 0;

                var dsMenu = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.MainMenu) as DataSet;
                if (dsMenu == null)
                {
                    dsMenu = InitMenuDataSet(modules, headerContent);
                    using (var dt = dsMenu.Tables[1])
                    {
                        var arrChallengeMenuItems = GetChallengeMenuItems(personChallValue, challenges, headerContent, modules, hasBrowseChallenges);

                        foreach (object t in arrChallengeMenuItems)
                        {
                            var challengeMenuItem = (ChallengeMenuItem)t;
                            var nrq = dt.NewRow();
                            nrq["Name"] = challengeMenuItem.CodeName;
                            nrq["Desc"] = challengeMenuItem.Name;
                            nrq["NavUrl"] = challengeMenuItem.Link;
                            nrq["NavCategory_Id"] = 3;
                            dt.Rows.Add(nrq);
                        }
                    }
                    CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.MainMenu, dsMenu);
                }

                mainMenu = BuildMenuItems(dsMenu, headerContent);
                viewModel.MainMenu = mainMenu;

                CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.MvcMenu, mainMenu);
                #endregion
            }
            #region Build the Branding section
            //make sure profile exists
            if (SponsorProfile != null && SponsorProfile.SponsorID == VhmSession.PrimarySponsorID)
            {
                if (SponsorProfile.UseCustomBrand)
                {
                    #region build sponsor logo, if any available from database
                    //populate the Use custom brand properties
                    if (SponsorProfile.LogoType != null && SponsorProfile.SponsorLogo != null && SponsorProfile.SponsorLogo.Length > 1)
                    {
                        //if sponsor has a logo uploaded in database
                        if (SponsorProfile.LogoType == SponsorLogoType.TXT || SponsorProfile.LogoType == SponsorLogoType.TEXT)
                        {
                            viewModel.SponsorImageUrl = string.Empty;
                            viewModel.SponsorLogoType = "text";
                            viewModel.SponsorText = Encoding.ASCII.GetString(SponsorProfile.SponsorLogo);
                        }
                        else //it can only be an image if not text
                        {
                            //pass random parameter to prevent cached data
                            viewModel.SponsorImageUrl = "/v2/Image/LogoDisplay?r=" + DateTime.Now.Ticks;
                            viewModel.SponsorLogoType = "image";
                            viewModel.SponsorText = string.Empty;
                        }
                    }
                    #endregion
                }
                else
                {
                    #region build VHM logo link and check if sponsor has "Brought to you by"
                    #region check if brought to you by and populate properties
                    if (SponsorProfile.LogoType != null && SponsorProfile.SponsorLogo != null && SponsorProfile.SponsorLogo.Length > 1)
                    {
                        viewModel.SponsorDefaultBroughtToYouByText =
                            headerContent[HeaderContent.BroughtToYouBy];

                        //if sponsor has a logo uploaded in database
                        viewModel.ShowBroughtToYouBy = true;
                        if (SponsorProfile.LogoType == SponsorLogoType.TXT || SponsorProfile.LogoType == SponsorLogoType.TEXT)
                        {
                            viewModel.SponsorBroughtToYouByUrl = string.Empty;
                            viewModel.SponsorBroughtToYouByText = Encoding.ASCII.GetString(SponsorProfile.SponsorLogo);
                            viewModel.SponsorLogoType = "text";
                        }
                        else //it can only be an image if not text
                        {
                            //pass random parameter to prevent cached data
                            viewModel.SponsorBroughtToYouByUrl = "/v2/Image/LogoDisplay?r=" + DateTime.Now.Ticks;
                            viewModel.SponsorBroughtToYouByText = string.Empty;
                            viewModel.SponsorLogoType = "image";
                        }
                    }
                    else
                    {
                        viewModel.ShowBroughtToYouBy = false;
                        viewModel.SponsorBroughtToYouByUrl = string.Empty;
                        viewModel.SponsorBroughtToYouByText = string.Empty;
                        viewModel.SponsorBroughtToYouByLogoType = string.Empty;
                    }
                    #endregion
                    //populate the Virgin Brand properties
                    viewModel.VhmLogoUrl = string.Format("{0}", ApplicationSettings.VhmLogoImageURL);
                    #endregion
                }
                viewModel.UseCustomBrand = SponsorProfile.UseCustomBrand;
            }
            #endregion

            var sessionId = VhmSession.SessionID;
            var memberID = VhmSession.MemberID;
            var member = SocialBL.GetMemberProfile(sessionId, memberID, memberID, false);

            viewModel.MemberId = memberID;
            viewModel.MemberFullName = string.Format("{0} {1}", member.Member.FirstName, member.Member.LastName);
            viewModel.Permissions = VhmSession.Permission;
            var imageUrl = member.Member.ImageUrl;
            viewModel.MemberImageUrl = imageUrl.StartsWith("http") ? imageUrl : ApplicationSettings.CDNBaseURL + imageUrl;
            viewModel.ShowSiteNavigationMenu = ShowSiteNavigationDropDown();
            viewModel.Content = headerContent;
            viewModel.ShowMemberDeviceSettingsLink = ShowMemberDeviceSettingsLink();

            List<SponsorModule> sponsorModules = SponsorBL.GetSponsorModules(VhmSession.PrimarySponsorID);
            if (sponsorModules.Any(x => x.ModuleTypeCode == "LEA"))
            {
                if (viewModel.MainMenu.ChildItems.Last().Text != "Learning")
                    viewModel.MainMenu.ChildItems.Add(new MainMenuItem { Text = "Learning", Url = "../../cms/learning/" });
            }

            return PartialView("LandingPageV3/HeaderV3", viewModel);
            
     
        }

        public ActionResult Footer()
        {

            var footerViewModel = new FooterViewModel();
            var modules = GetModules();

            // TODO: Move this out to the web.config or something else
            footerViewModel.CompanyName = string.Format("©{0} Virgin HealthMiles", DateTime.Now.Year);
            if (VhmSession.PrimarySponsorID > 0)
            {
                var profile = GetSponsorProfile();// SponsorBL.GetSponsorProfile(VhmSession.PrimarySponsorID);
                footerViewModel.ShowPoweredByVhm = profile.IncludePoweredByVHM;
            }
            else
            {
                footerViewModel.ShowPoweredByVhm = false;
            }


            var footerMenu = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.FooterMenuData) as MainMenuItem;

            if (footerMenu == null)
            {

                using (var dsFooterList = InitFooterDataSet(modules))
                {

                    using (var dv = dsFooterList.Tables[0].DefaultView)
                    {

                        footerMenu = new MainMenuItem();

                        foreach (DataRow row in dv.Table.Rows)
                        {
                            footerMenu.ChildItems.Add(new MainMenuItem
                            {
                                Text = row[0].ToString(),
                                Url = row[1].ToString()
                            });
                        }

                        CacheProvider.Add(System.Web.HttpContext.Current, CacheProvider.CacheItems.FooterMenuData, footerMenu);

                    }
                }
            }

            footerViewModel.FooterMenu = footerMenu;
        
            return PartialView(footerViewModel);
        }

        public ActionResult FooterV3()
        {

            var footerViewModel = new FooterViewModel();
            var modules = GetModules();

            // TODO: Move this out to the web.config or something else
            footerViewModel.CompanyName = string.Format("©{0} Virgin HealthMiles", DateTime.Now.Year);
            if (VhmSession.PrimarySponsorID > 0)
            {
                var profile = GetSponsorProfile();// SponsorBL.GetSponsorProfile(VhmSession.PrimarySponsorID);
                footerViewModel.ShowPoweredByVhm = profile.IncludePoweredByVHM;
            }
            else
            {
                footerViewModel.ShowPoweredByVhm = false;
            }


            var footerMenu = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.FooterMenuData) as MainMenuItem;

            if (footerMenu == null)
            {

                using (var dsFooterList = InitFooterDataSet(modules))
                {

                    using (var dv = dsFooterList.Tables[0].DefaultView)
                    {

                        footerMenu = new MainMenuItem();

                        foreach (DataRow row in dv.Table.Rows)
                        {
                            footerMenu.ChildItems.Add(new MainMenuItem
                            {
                                Text = row[0].ToString(),
                                Url = row[1].ToString()
                            });
                        }

                        CacheProvider.Add(System.Web.HttpContext.Current, CacheProvider.CacheItems.FooterMenuData, footerMenu);

                    }
                }
            }

            footerViewModel.FooterMenu = footerMenu;
       
                return PartialView("LandingPageV3/FooterV3", footerViewModel);
            
       
        }

        public ActionResult FooterLandingPage()
        {

            var footerViewModel = new FooterViewModel();
            footerViewModel.Content = MemberBL.GetContentLabels<FooterContent>(PageNames.FooterContent, LanguageId);
            var footerLandingMenu = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.MvcMenu) as MainMenuItem;

            footerViewModel.MainMenu = footerLandingMenu;

            return PartialView(footerViewModel);
        }

        private DataSet InitFooterDataSet(IEnumerable<MemberModule> modules)
        {
            var dataSet = new DataSet();

            var xmlFilePath = string.Format(@"{0}\FooterLinkList.xml", Request.PhysicalApplicationPath);

            dataSet.ReadXml(xmlFilePath);

            var sbEntrollDate = new StringBuilder();
            var sEnrollmentDate = VhmSession.IWPMemberSince;

            for (var i = 0; i < sEnrollmentDate.Length; i++)
            {
                var sDivider = (i != 4 && i != 6) ? "" : "-";
                sbEntrollDate.AppendFormat("{0}{1}", sDivider, sEnrollmentDate.Substring(i, 1));
            }

            var oDaysOfMembership = DateTime.Now - ((sbEntrollDate.Length > 0) ? DateTime.Parse(sbEntrollDate.ToString(), CultureInfo.InvariantCulture) : DateTime.Now);

            if (modules.Count(m => m.ModuleID == 2) == 0)
            {
                var dataRows = dataSet.Tables[0].Select("Url LIKE '%download%'");

                foreach (var dataRow in dataRows)
                {
                    dataSet.Tables[0].Rows.Remove(dataRow);
                }
            }

            if (oDaysOfMembership.TotalDays <= 7)
            {
                var dataRows = dataSet.Tables[0].Select("Url = 'secure/member/survey.aspx'");

                foreach (var dataRow in dataRows)
                {
                    dataSet.Tables[0].Rows.Remove(dataRow);
                }
            }

            return dataSet;
        }

        public ActionResult TitlePageHeader(PageTitle page)
        {
            var pageTitle = new PageTitle(page.ShowPageTitle, page.PageTitleName, page.ShowRightTitleLink,
                                           page.RightLinkText);
            return PartialView("_TitlePageHeader", pageTitle);
        }

        private DataSet InitMenuDataSet(List<MemberModule> modules, Dictionary<HeaderContent, string> content)
        {

            string companyName = ApplicationSettings.CompanyName;
            var xmlFilePath = string.Format(@"{0}\TopNavList_{1}.xml", Request.PhysicalApplicationPath,
                                             companyName.Replace(" ", "_").ToLower());

            var dsMenu = new DataSet();

            dsMenu.ReadXml(xmlFilePath);

            #region Modules

            // init all modules
            var hra = modules.FirstOrDefault(m => m.ModuleTypeCode == "HRA");
            var vac = modules.FirstOrDefault(m => m.ModuleTypeCode == "VAC");
            var vbi = modules.FirstOrDefault(m => m.ModuleTypeCode == "VBI");
            var sea = modules.FirstOrDefault(m => m.ModuleTypeCode == "SEA");
            var seb = modules.FirstOrDefault(m => m.ModuleTypeCode == "SEB");
            var osb = modules.FirstOrDefault(m => m.ModuleTypeCode == "OSB");
            var tfa = modules.FirstOrDefault(m => m.ModuleTypeCode == "TFA");
            var rpt = modules.FirstOrDefault(m => m.ModuleTypeCode == "RPA");
            var ntr = modules.FirstOrDefault(m => m.ModuleTypeCode == "NTR");

            if (ntr != null && ( ntr.IsPartner.HasValue && ntr.IsPartner.Value ))
            {
                ntr.PartnerGUID = Guid.NewGuid();
                ntr.MaskModuleName = ntr.ModuleName;
            }

            var partnerModules = modules.Where(m => m.IsPartner.GetValueOrDefault() && m.PartnerGUID != Guid.Empty && m.PartnerGUID != null).ToList();

            if (hra == null)
            {
                DataRow[] drsfa = dsMenu.Tables[1].Select("NavUrl LIKE '%hrassessment.aspx%'");
                foreach (DataRow dr in drsfa)
                    dsMenu.Tables[1].Rows.Remove(dr);
            }
            else
            {
                DataRow[] drsfa = dsMenu.Tables[1].Select("NavUrl LIKE '%hrassessment.aspx%'");
                foreach (DataRow dr in drsfa)
                    dr["Desc"] = hra.HRAName;
            }
            if (rpt == null)
            {
                DataRow[] drsfa = dsMenu.Tables[1].Select("NavUrl LIKE '%reports.aspx%'");
                foreach (DataRow dr in drsfa)
                    dsMenu.Tables[1].Rows.Remove(dr);
            }

            if (vac == null && sea == null)
            {
                DataRow[] drNsa =
                    dsMenu.Tables[1].Select(
                        "NavUrl LIKE '%activityjournal.aspx%' or NavUrl LIKE '%Devices%' or NavUrl LIKE '%intro.aspx%'");
                foreach (DataRow dr in drNsa)
                    dsMenu.Tables[1].Rows.Remove(dr);
                drNsa = dsMenu.Tables[0].Select("Name LIKE '%Activity%'");
                foreach (DataRow dr in drNsa)
                    dsMenu.Tables[0].Rows.Remove(dr);

            }
            else if (vac == null || vac.OtherDevices == null || !vac.OtherDevices.Value)
            {
                DataRow[] drNsa = dsMenu.Tables[1].Select(" NavUrl LIKE '%Devices%'");
                foreach (DataRow dr in drNsa)
                    dsMenu.Tables[1].Rows.Remove(dr);
            }

            if (osb == null && seb == null && vbi == null)
            {
                DataRow[] res =
                    dsMenu.Tables[1].Select(" NavUrl LIKE '%biomeasurements.aspx%' or  NavUrl LIKE '%hzlocator.aspx%'");
                foreach (DataRow dr in res)
                    dsMenu.Tables[1].Rows.Remove(dr);
            }
            else if (vbi == null || vbi.HZLocator == null || !vbi.HZLocator.Value)
            {
                DataRow[] res = dsMenu.Tables[1].Select("NavUrl LIKE '%hzlocator.aspx%'");
                foreach (DataRow dr in res)
                    dsMenu.Tables[1].Rows.Remove(dr);
            }

            if (rpt == null && vbi == null && seb == null && hra == null && osb == null && tfa == null)
            {
                DataRow[] item = dsMenu.Tables[0].Select("Name LIKE '%Measure%'");
                foreach (DataRow dr in item)
                {
                    dsMenu.Tables[0].Rows.Remove(dr);
                }
            }
            else if (rpt == null && vbi == null && seb == null && hra != null && osb == null && tfa == null)
            {
                DataRow[] res = dsMenu.Tables[0].Select("Name LIKE '%Measure%'");
                foreach (DataRow dr in res)
                {
                    dr["Name"] = hra.HRAName;
                }
            }

            if (tfa == null)
            {
                DataRow[] drNsa = dsMenu.Tables[1].Select("NavUrl LIKE '%NoSmoking%'");
                foreach (DataRow dr in drNsa)
                    dsMenu.Tables[1].Rows.Remove(dr);
            }

            // add the partner overview page if the sponsor has more than one module
            var partnerCount = partnerModules.Count;
            if (partnerCount > 1)
            {
                var nrq = dsMenu.Tables[1].NewRow();
                nrq["Name"] = "Overview";
                nrq["Desc"] = "Partner Pages Overview";
                nrq["NavUrl"] = ApplicationSettings.PartnersPageURL;
                nrq["NavCategory_Id"] = 5;
                dsMenu.Tables[1].Rows.Add(nrq);
            }
            else if (partnerCount == 0)
            {
                var dtPrograms = dsMenu.Tables[0].Select("Name LIKE 'Programs'");
                foreach (var dtProgram in dtPrograms)
                {
                    dsMenu.Tables[0].Rows.Remove(dtProgram);
                }
            }
            // add the individual partner pages for each partner module
            foreach (var module in partnerModules)
            {
                var nrq = dsMenu.Tables[1].NewRow();
                nrq["Name"] = module.MaskModuleName;
                nrq["Desc"] = module.MaskModuleName;

                if (module.ModuleTypeCode != "NTR")
                {
                    nrq["NavUrl"] = string.Format( ApplicationSettings.PartnersPageSponsorModulURL,module.SponsorModuleId );
                }
                else
                {
                    nrq["NavUrl"] = ApplicationSettings.BaseURL + "/v2/Nutrition/LandingPage";
                }

                nrq["NavCategory_Id"] = 5;
                dsMenu.Tables[1].Rows.Add(nrq);
            }

            #endregion Modules

            #region Suppress Rebate center related

            var sponsorPerks = VhmSession.SponsorPerks.FirstOrDefault();
            if (sponsorPerks != null && sponsorPerks.SuppressRebateCtr)
            {
                DataRow[] drRebateCtr = dsMenu.Tables[1].Select("NavUrl LIKE '%MallNetwork.aspx%'");
                foreach (DataRow dr in drRebateCtr)
                    dsMenu.Tables[1].Rows.Remove(dr);
            }

            #endregion

            #region Rename "Spend Your HealthCash" to "Shop for Products"

            //Check if Allow spending of VLC Cash is turned off
            if (VhmSession.SpendVLCCash == 0)
            {
                DataRow[] drRebateCtr = dsMenu.Tables[1].Select("Desc LIKE '%Spend Your HealthCash%'");
                foreach (DataRow dr in drRebateCtr)
                {
                    dr.BeginEdit();
                    dr["Desc"] = "Shop for Products";
                    dr["EktronLabelID"] = (int)HeaderContent.ShopForProducts;
                    dr.EndEdit();
                }
            }

            #endregion

            #region show Rewards Details appropriate page - for tasks or for levels

            var drProgramTable = dsMenu.Tables[1].Select("Name LIKE '%YourProgram%'");
            string programName = ApplicationSettings.ProgramName;
            string desc = string.Format(content[HeaderContent.YourHealthMilesProgram], programName);

            var member = MemberBL.GetMemberById(VhmSession.SessionID, VhmSession.MemberID);
            var game = RewardsBL.GetSponsorCurrentGame(VhmSession.SessionID, member, VhmSession.PrimarySponsorID);
            bool isExpress = game != null && game.ProgramType == "EXP";
            if ( isExpress )
            {
                desc = ApplicationSettings.ExpressProgramMenuDescription;
            }

            foreach (DataRow dr in drProgramTable)
            {
                dr.BeginEdit();
                dr["Desc"] = desc;
                dr["EktronLabelID"] = 0;
                dr.EndEdit();
                dr.AcceptChanges();
            }

            #endregion

            #region Remove Social Pages for members that have the Community turned off

            if (VhmSession.CommunityOn == 0)
            {
                var drSocialPages =
                    dsMenu.Tables[1].Select(
                        "NavUrl LIKE '%MyFriends.aspx%' or NavUrl LIKE '%MyGroups%' or NavUrl LIKE '%MyNewsFeed.aspx%'");

                foreach (DataRow dr in drSocialPages)
                {
                    dsMenu.Tables[1].Rows.Remove(dr);
                }
            }

            #endregion

            #region Remove Badges Page if Sponsor hasn't enabled system badges or personal challenge badges.

            bool hasBadgesEnabled = MemberBL.BadgesEnabledForSponsor(VhmSession.PrimarySponsorID);
            if (!hasBadgesEnabled)
            {
                var drBadgePage = dsMenu.Tables[1].Select("NavUrl LIKE '%MyBadges.aspx%'");

                foreach (DataRow dr in drBadgePage)
                {
                    dsMenu.Tables[1].Rows.Remove(dr);
                }
            }

            #endregion

            HttpContext.Items.Add("MainMenu", dsMenu);

            return dsMenu;
        }

        private ArrayList GetChallengeMenuItems(bool personChallValue, List<ChallengeBase> challenges, Dictionary<HeaderContent, string> content, List<MemberModule> modules, bool hasBrowseChallenges)
        {
            var arRes = new ArrayList();
            #region KAG
            var kags = (from challenge in challenges
                        where challenge.ChallengeType == ChallengeBase.CHALLENGE_TYPE_KAG
                        && challenge.MenuDisplay
                        select challenge).ToList();
            foreach (var kag in kags)
            {
                arRes.Add(new ChallengeMenuItem(
                               string.Format("KAG {0}", kags.IndexOf(kag)),
                               kag.Name,
                               string.Format("secure/challenge/sechallenge.aspx?secId={0}", kag.ChallengeId),
                               kag.ChallengeId,
                               ChallengeItemCategory.PMO,
                               kag.StartDate,
                               kag.EndDate.GetValueOrDefault()
                               ));
            }
            #endregion

            #region quests
            var quests = (from challenge in challenges
                          where challenge.Source == ChallengeBase.CHALLENGE_TYPE_QUEST
                          && challenge.MenuDisplay
                          select challenge).ToList();

            foreach (var quest in quests)
            {
                arRes.Add(new ChallengeMenuItem(
                       string.Format("Quest {0}", quests.IndexOf(quest)),
                       quest.Name,
                       string.Format("secure/quest/questregistration.aspx?quest_id={0}", quest.ChallengeId),
                       quest.ChallengeId,
                       ChallengeItemCategory.QUS,
                       quest.StartDate,
                       quest.EndDate.GetValueOrDefault()
                       ));
            }
            #endregion           

            #region Challenges

            List<ChallengeBase> crpChallenges = null;
            if (challenges.Count > 0)
            {
                crpChallenges = (from challenge in challenges
                                     where (challenge.Source == ChallengeBase.CHALLENGE_TYPE_CORP ||
                                     challenge.Source == ChallengeBase.CHALLENGE_TYPE_CORP_SIGN_UP)
                                     && challenge.MenuDisplay
                                     select challenge).ToList();

                if (crpChallenges.Any())
                {
                    #region tracking challenge links

                    #region Corporate Tracking Challenges

                    var allTrackingChallengeMenuItem = CreateChallengeMenuItems( content, crpChallenges.Where( t=>!(t is Challenge)).ToList() );
                    allTrackingChallengeMenuItem.Sort();

                    for (var i = 0; i < allTrackingChallengeMenuItem.Count; i++)
                    {
                        var chmchi = (ChallengeMenuChallengeItem)allTrackingChallengeMenuItem[i];

                        arRes.Add(new ChallengeMenuItem(
                            string.Format("Challenge {0}", i),
                            chmchi.ChallengeName,
                            chmchi.Link,
                            chmchi.ChallengeId,
                            chmchi.ChallengeType,
                            chmchi.StartDate,
                            chmchi.EndDate
                            ));
                    }
                    #endregion

                    #endregion
                }
            }

            #region Browse Corporate Tracking Challenges

            if (hasBrowseChallenges)
            {
                arRes.Add(new ChallengeMenuItem(
                    "BrowseChallenges",
                    content[HeaderContent.BrowseChallenges],
                    "challenge/join",
                    0,
                    ChallengeItemCategory.XXX,
                    DateTime.MinValue,
                    DateTime.MinValue
                    ));
            }

            #endregion

            #region Corporate Challenges
            if (crpChallenges != null)
            {
                var allChallengeMenuItems = CreateChallengeMenuItems(content, crpChallenges.Where(t => t is Challenge).ToList());
                allChallengeMenuItems.Sort();
                for (var i = 0; i < allChallengeMenuItems.Count; i++)
                {
                    var chmchi = (ChallengeMenuChallengeItem)allChallengeMenuItems[i];

                    arRes.Add(new ChallengeMenuItem(
                                  string.Format("Challenge {0}", i),
                                  chmchi.ChallengeName,
                                  chmchi.Link,
                                  chmchi.ChallengeId,
                                  chmchi.ChallengeType,
                                  chmchi.StartDate,
                                  chmchi.EndDate
                                  ));
                }
            }

            #endregion

            #endregion

            #region Promos
            var promos = (from challenge in challenges
                          where challenge.Source == ChallengeBase.CHALLENGE_TYPE_PROMO &&
                          challenge.ChallengeType != ChallengeBase.CHALLENGE_TYPE_KAG
                          && challenge.MenuDisplay
                          select challenge).ToList();
            foreach (var promo in promos)
            {
                arRes.Add(new ChallengeMenuItem(
                       string.Format("Promo {0}", promos.IndexOf(promo)),
                       promo.Name,
                       string.Format("secure/challenge/promo.aspx?promoid={0}", promo.ChallengeId),
                       promo.ChallengeId,
                       ChallengeItemCategory.PMO,
                       promo.StartDate,
                       promo.EndDate.GetValueOrDefault()
                       ));
            }

            #endregion

            if (personChallValue)
            {
                #region create My Personal Challenges page link no matter if there are OR NOT any available challenges
                arRes.Add(new ChallengeMenuItem(
                    string.Format("Challenge {0}", 0),
                    content[HeaderContent.MyPersonalChallenge],
                    string.Format("secure/challenge/MyPersonalChallenges.aspx"),
                    0,
                    ChallengeItemCategory.XXX,
                    DateTime.MinValue,
                    DateTime.MinValue
                    ));
                #endregion

                #region create challenge link
                // Express sponsors get the new personal challenges                
                bool useTrackingChallenges = modules.Any(m => m.ModuleTypeCode == "TRK");
                var createChallengeLink = useTrackingChallenges ? "challenge/select" : "secure/challenge/create.aspx?challengeid=";

                arRes.Add(new ChallengeMenuItem(
                    string.Format("Challenge {0}", 0),
                    content[HeaderContent.CreatePersonalChallenge],
                    string.Format(createChallengeLink),
                    0,
                    ChallengeItemCategory.XXX,
                    DateTime.MinValue,
                    DateTime.MinValue
                    ));
                #endregion
            }

            arRes.Add(new ChallengeMenuItem(
                "ChallengeHistory",
                content[HeaderContent.ChallengeHistory],
                "secure/challenge/History.aspx",
                0,
                ChallengeItemCategory.XXX,
                DateTime.MinValue,
                DateTime.MinValue
                ));
            return arRes;
        }

        private ArrayList CreateChallengeMenuItems( Dictionary<HeaderContent, string> content, List<ChallengeBase> crpChallenges )
        {
            var alChallengeMenuItem = new ArrayList();
            DateTime dtNowMemberTime = MemberBL.GetMemberNow( VhmSession.MemberID, DateTime.Now );
            // VirginLifecare.Client.TimeZones.FromSystemTimeForMember(DateTime.Now, memberId);

            for ( var i = 0; i < crpChallenges.Count(); i++ )
            {
                var challenge = crpChallenges[i];
                var bSignedUp = challenge.SignedUp;
                var challengeStatus = ChallengeStatus.None;
                var startDate = challenge.StartDate;
                var endDate = challenge.EndDate.GetValueOrDefault();
                const ChallengeItemCategory S_CHALLENGE_TYPE = ChallengeItemCategory.CHG;
                var challengeText = string.Empty;

                if ( challenge.DaysUntilStart > 0 )
                {
                    challengeStatus = ChallengeStatus.Created;
                    challengeText = String.Format( content[HeaderContent.StartingInDay],
                                                   Convert.ToString( challenge.DaysUntilStart ) );
                } else if ( challenge.DaysUntilStart == 0 )
                {
                    challengeText = content[HeaderContent.StartingToday];
                    challengeStatus = ChallengeStatus.Created;
                }

                if ( challenge.EndDate.HasValue && challenge.EndDate < dtNowMemberTime )
                {
                    if ( challenge.UploadDeadline.HasValue )
                    {
                        if ( challenge.UploadDeadline.Value > dtNowMemberTime )
                        {
                            challengeStatus = ChallengeStatus.ResultPending;
                            challengeText = content[HeaderContent.ResultsPending];
                        } else if ( challenge.UploadDeadline.Value.Date.AddDays( 8d ) > dtNowMemberTime.Date )
                        {
                            // display completed challenges only for 7 days after the deadline
                            challengeText = content[HeaderContent.Completed];
                            challengeStatus = ChallengeStatus.Complete;
                        } else
                        {
                            continue;
                        }
                    } else if ( challenge.EndDate.Value.Date.AddDays( 8d ) > dtNowMemberTime.Date )
                    {
                        // display completed challenges only for 7 days after the end date (target challenges)
                        challengeText = content[HeaderContent.Completed];
                        challengeStatus = ChallengeStatus.Complete;
                    } else
                    {
                        continue;
                    }
                } else if ( challenge.DaysUntilStart < 0 )
                {
                    challengeText = content[HeaderContent.InProgress];
                    challengeStatus = ChallengeStatus.InProgress;
                }

                challengeText += " - " + challenge.Name;

                string link;
                var menuCategory = string.Format( "Challenge {0}", i );

                if ( challengeStatus == ChallengeStatus.Draft )
                {
                    link = string.Format( "secure/challenge/create.aspx?challengeid={0}", challenge.ChallengeId );
                } else if ( challengeStatus == ChallengeStatus.Created && !bSignedUp ) //
                {
                    link = string.Format( "secure/challenge/invitation.aspx?challengeid={0}", challenge.ChallengeId );
                } else if ( challenge.ChallengeType == ChallengeType.PersonalKAG )
                {
                    // these are Tracking Challenges
                    if ( challenge.ShowInBrowseOnly )
                    {
                        link = challenge.IsMemberEnrolled
                                   ? string.Format( "challenge/{0}", challenge.ChallengeId )
                                   : string.Format( "secure/challenge/invitation.aspx?challengeid={0}",
                                                    challenge.ChallengeId );
                    } else
                    {
                        // know and go challenges add the member to the challenge when they go to the page
                        // if they're not already in it, so link directly to the page
                        link = string.Format( "challenge/{0}", challenge.ChallengeId );
                    }
                } else
                {
                    link = string.Format( "secure/challenge/rankings.aspx?challengeid={0}", challenge.ChallengeId );
                }

                alChallengeMenuItem.Add( new ChallengeMenuChallengeItem( challengeText, challengeStatus,
                                                                         challenge.ChallengeId, startDate,
                                                                         endDate, S_CHALLENGE_TYPE, link,
                                                                         menuCategory ) );
            }
            return alChallengeMenuItem;
        }

        private MainMenuItem BuildMenuItems(DataSet dsMenu, Dictionary<HeaderContent, string> content)
        {
            var dtMenu = dsMenu.Tables[0];
            var menu = new MainMenuItem { Root = true };

            if (Request.Url != null)
            {
                var uri = Request.Url.AbsoluteUri;
                var baseUri = new Uri(uri);
                var root = baseUri.Scheme + Uri.SchemeDelimiter + baseUri.Host;

                foreach (DataRowView row in dtMenu.DefaultView)
                {
                    var url = "#";
                    var text = row["Name"].ToString();
                    var localizText = LocalizationHelpers.GetLocalizeText(row.Row, content, "Name");
                    var id = int.Parse(row["NavCategory_Id"].ToString());
                    const int WIDTH = 0;

                    menu.ChildItems.Add(new MainMenuItem(url, localizText, WIDTH, id));
                    #region Challenges
                    if (text == "Challenges")
                    {

                        var item = menu.ChildItems.SingleOrDefault(c => c.NavCategoryId == id);
                        if (item != null)
                        {
                            #region all Challenges
                            var challengeView = from rows in dsMenu.Tables[1].AsEnumerable()
                                                where rows.Field<int>("NavCategory_Id") == id
                                                select rows;
                            if (challengeView.Any())
                            {
                                foreach (DataRow subRow in challengeView)
                                {
                                    var navUrl = Convert.ToString(subRow["NavUrl"]);
                                    id = int.Parse(subRow["NavCategory_Id"].ToString());
                                    var corpUrl = (navUrl.IndexOf("http://", StringComparison.Ordinal) == 0 ||
                                                    navUrl.IndexOf("https://", StringComparison.Ordinal) == 0)
                                                      ? navUrl
                                                      : (navUrl != "#")
                                                            ? string.Format("{0}/{1}", root, navUrl)
                                                            : Request.FilePath;

                                    var corpLocalizText = LocalizationHelpers.GetLocalizeText(subRow, content, "Desc");
                                    var corpText = string.Format("{0} ", corpLocalizText);
                                    item.ChildItems.Add(new MainMenuItem(corpUrl, corpText, 350, id));
                                }
                            }

                            #endregion
                        }
                        continue;

                    }
                    #endregion
                    var id1 = id;
                    var view = from rows in dsMenu.Tables[1].AsEnumerable()
                               where rows.Field<int>("NavCategory_Id") == id1
                               select rows;

                    if (view.Any())
                    {
                        foreach (DataRow subRow in view)
                        {
                            var navUrl = Convert.ToString(subRow["NavUrl"]);
                            id = int.Parse(subRow["NavCategory_Id"].ToString());
                            string rootUrl = (Request.ApplicationPath != "/") ? Request.ApplicationPath : "";
                            if (subRow["Name"].ToString() == "TakeHS")
                            {
                                subRow["EktronLabelID"] = 0;
                            }
                            if (subRow["Name"].ToString().ToLower().IndexOf("dummy", StringComparison.Ordinal) == -1)
                            {
                                url = (navUrl.IndexOf("http://", StringComparison.Ordinal) == 0 || navUrl.IndexOf("https://", StringComparison.Ordinal) == 0)
                                          ? navUrl
                                          : (navUrl != "#")
                                                ? string.Format("{0}/{1}", rootUrl, navUrl)
                                                : Request.FilePath;

                                var item = menu.ChildItems.SingleOrDefault(c => c.NavCategoryId == id);
                                if (item != null)
                                {
                                    localizText = LocalizationHelpers.GetLocalizeText(subRow, content, "Desc");
                                    text = string.Format("{0} ", localizText);
                                    item.ChildItems.Add(new MainMenuItem(url, text, 350, id));
                                }
                            }
                        }
                    }
                }
            }

            return menu;
        }

        private bool ShowSiteNavigationDropDown()
        {
            var returnVal = false;
            if (VhmSession != null && VhmSession.Permission != null)
            {
                //we need to check what permisions this member has. If it has more than just the member (access to member site) than show the dropdown
                var count = 0;
                if (VhmSession.Permission.Member)
                {
                    count++;
                }
                if (VhmSession.Permission.OnlineReporting)
                {
                    count++;
                }
                if (VhmSession.Permission.MeasurementAssessor)
                {
                    count++;
                }
                if (VhmSession.Permission.LifeZoneMasterLogin)
                {
                    count++;
                }
                if (VhmSession.Permission.VoucherReceiver)
                {
                    count++;
                }
                if (VhmSession.Permission.ECoach)
                {
                    count++;
                }
                if (VhmSession.Permission.ChallengeManager)
                {
                    count++;
                }
                if (VhmSession.Permission.HealthZoneAdmin)
                {
                    count++;
                }
                if (VhmSession.Permission.CrmAdmin)
                {
                    count++;
                }
                returnVal = count > 1; //we know there must be at least the Member site access.
            }

            return returnVal;
        }

        /// <summary>
        /// Determines if member has agozone device registered on their account and returns true if the version of the device = 3 or false otherwise
        /// </summary>
        /// <returns></returns>
        private bool ShowMemberDeviceSettingsLink()
        {
            try
            {
                bool returnValue = false;
                var membDeviceVersion = ActivityBL.GetMemberGoZoneVersionMajor(VhmSession.MemberID);
                if (!string.IsNullOrEmpty(membDeviceVersion) && membDeviceVersion.Length > 0)
                {
                    int majorVersion = int.Parse(membDeviceVersion);
                    returnValue = majorVersion == 3;
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
                return false;
            }
        }
    }

    public enum SponsorCoBrandType
    {
        None,
        Text,
        Image
    }
}
