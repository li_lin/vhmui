﻿using System.Collections.Generic;
using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using Vhm.Providers;
using Vhm.Web.BL;
using Vhm.Web.Common;
using Vhm.Web.Models.Enums;
using Vhm.Web.Models.InputModels;
using Vhm.Web.Models.RepositoryModels;
using Vhm.Web.Models.ViewModels.Shared;
using Vhm.Web.Models.ViewModels.Social;
using VirginLifecareWeb.Controllers.ActionResults;
using Member = Vhm.Web.Models.RepositoryModels.Member;

namespace VirginLifecareWeb.Controllers
{
    public class SocialController : ControllerBase
    {

        private const string DEFAULT_CULTURE = "en-US";

        #region Localization


        #endregion Localiaztion

        public ActionResult MyGroups()
        {
            var sessionId = VhmSession.SessionID;
            var memberId = VhmSession.MemberID;
            
            var content = MemberBL.GetContentLabels<MyGroupsContent>(PageNames.MyGroups, LanguageId);
            
            var contentSocial = MemberBL.GetContentLabels<SocialChallengePromotionLabelsContent>(PageNames.SocialChallengePromotionLabels, LanguageId);

            var pageTitle = new PageTitle(true, ContentHeader[HeaderContent.MyGroups]);

            #region set up paging info for paged data
            var pagForMyGroups = new PagingIM { Count = 24, MemberId = memberId, PageNumber = 0 };
            //get 4 so we can render "See More..." 
            var pagForSuggestedGrps = new PagingIM { Count = 4, MemberId = memberId, PageNumber = 0 };
            #endregion

            var modelGroup = new MyGroupsModel();


            var getModelGroup = SocialBL.GetMyGroupsObjects(sessionId, VhmSession.MemberID, pagForMyGroups, pagForSuggestedGrps, false);

            if (getModelGroup != null)
            {


                modelGroup = getModelGroup;
                modelGroup.PageT = pageTitle;
                modelGroup.Content = content;
                modelGroup.ContentSocial = contentSocial;
                #region decode groups names and descriptions
                //this is needed because the one used in Vhm.Web.DataServices\Repositories\SocialRepository seems not to be enough. looks like encoded html still manages to pass through.
                if (modelGroup.PagedGroups.Groups != null && modelGroup.PagedGroups.Groups.Count > 0)
                {
                    foreach (var group in modelGroup.PagedGroups.Groups)
                    {
                        group.Group.Name = DecodeHtml(group.Group.Name);
                        group.Group.Description = DecodeHtml(group.Group.Description);
                        group.Group.Category = DecodeHtml(group.Group.Category);
                    }
                }
                if (modelGroup.SuggestedGroups != null && modelGroup.SuggestedGroups.Count > 0)
                {
                    foreach (var suggestedGroup in modelGroup.SuggestedGroups)
                    {
                        suggestedGroup.Group.Name = DecodeHtml(suggestedGroup.Group.Name);
                        suggestedGroup.Group.Description = DecodeHtml(suggestedGroup.Group.Description);
                        suggestedGroup.Group.Category = DecodeHtml(suggestedGroup.Group.Category);
                    }
                }
                #endregion

                #region get and populate group names suggestions for search group by name
                //get all the group names member is part of, and convert to json - used for suggestions when users starts typing data in search boxes, so they can search by group names
                var listOfGroupNames = SocialBL.GetMemberGroupNames(sessionId, memberId);
                if (listOfGroupNames != null && listOfGroupNames.Count > 0)
                {
                    for (int i = 0; i < listOfGroupNames.Count; i++)
                    {
                        listOfGroupNames[i] = DecodeHtml(listOfGroupNames[i]);
                    }
                    modelGroup.JsonMemberGroupsNames = JsonConvert.SerializeObject(listOfGroupNames);
                }
                else
                {
                    modelGroup.JsonMemberGroupsNames = string.Empty;
                }
                #endregion

                #region get and populate Member friend names suggestions for search group by administrator name
                //get all the group names member is part of, and convert to json - used for suggestions when users starts typing data in search boxes, so they can search by group admins
                var listOfFriendNames = new List<string>();

                // For members with too many friends, this may fail
                try
                {
                    var friendCount = SocialBL.GetMemberFriendCount( memberId );
                    if (friendCount < ApplicationSettings.FriendCountThreshold)
                    {
                        listOfFriendNames = SocialBL.GetMemberFriendNames( sessionId, memberId );
                    }
                }
                catch ( Exception ex )
                {
                    VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
                }

                if (listOfFriendNames != null && listOfFriendNames.Count > 0)
                {
                    for (int i = 0; i < listOfFriendNames.Count; i++)
                    {
                        listOfFriendNames[i] = DecodeHtml(listOfFriendNames[i]);
                    }
                    modelGroup.JsonMemberFriendsNames = JsonConvert.SerializeObject(listOfFriendNames);
                }
                else
                {
                    modelGroup.JsonMemberFriendsNames = string.Empty;
                }
                #endregion

                var selectedColorTheme = GetSelectedTheme();
                modelGroup.ThemeCode = selectedColorTheme;
            }
            return View(modelGroup);
        }

        public ActionResult _GetPagedGroups(int count, int pageNumber)
        {
            try
            {
                var pagedGroups = new PagedGroupsVM();
                string sessionId = VhmSession.SessionID;
                long memberId = VhmSession.MemberID;

                var pagForMyGroups = new PagingIM { Count = count, MemberId = memberId, PageNumber = pageNumber };

                var getPagedGroups = SocialBL.GetMyGroupsPaged(sessionId, pagForMyGroups, false);
                if (getPagedGroups != null)
                {
                    pagedGroups = getPagedGroups;
                    if (pagedGroups.Groups.Count > 0)
                    {
                        foreach (var groupVm in pagedGroups.Groups)
                        {
                            groupVm.Group.Name = DecodeHtml(groupVm.Group.Name);
                            groupVm.Group.Description = DecodeHtml(groupVm.Group.Description);
                            groupVm.Group.Category = DecodeHtml(groupVm.Group.Category);
                        }
                    }
                }
                return PartialView("_BuildPagedGroups", pagedGroups);
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
                return new JsonNetResult { Data = "Service Error" };
            }
        }

        public ActionResult _GetMoreSuggestedGroups(int pageNumber, int groupsPerPage)
        {
            try
            {
                var sessionId = VhmSession.SessionID;
                var paging = new PagingIM
                    {
                    Count = groupsPerPage,
                    MemberId = VhmSession.MemberID,
                    PageNumber = pageNumber
                };

                var pagedGroupsVm = new PagedGroupsVM();
                var getPagedGroups = SocialBL.GetListOfSuggestedGroups(sessionId, paging, false);
                if (getPagedGroups != null)
                {
                    pagedGroupsVm = getPagedGroups;
                    if (pagedGroupsVm.Groups.Count > 0)
                    {
                        #region decode groups names and descriptions
                        foreach (var group in pagedGroupsVm.Groups)
                        {
                            group.Group.Name = DecodeHtml(group.Group.Name);
                            group.Group.Description = DecodeHtml(group.Group.Description);
                            group.Group.Category = DecodeHtml(group.Group.Category);
                        }
                        #endregion
                    }
                }
                return new JsonNetResult { Data = pagedGroupsVm };
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
                return new JsonNetResult { Data = "Service Error" };
            }
        }

        public ActionResult _PublicGroupProfile(string group)
        {
            try
            {
                var grpDetails = JsonConvert.DeserializeObject<GroupVM>(group);
                return View(grpDetails);
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
                return new JsonNetResult { Data = "Service Error" };
            }
        }
        //to be used for Suggested groups and pending groups - we know member is not part of the group
        public ActionResult _GetPublicGroupProfileFromID(long groupId)
        {
            try
            {
                if (groupId > 0)
                {
                    const int MAX_GROUP_MEMBERS_TO_DISPLAY = 8;
                    var group = SocialBL.GetGroupVMDetails(VhmSession.SessionID, groupId, VhmSession.MemberID, MAX_GROUP_MEMBERS_TO_DISPLAY, false);
                    return PartialView("_PublicGroupProfile", group);
                }
                return new JsonNetResult { Data = "Service Error" }; //missing group id
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
                return new JsonNetResult { Data = "Service Error" };
            }
        }
        public ActionResult _AddMemberToGroup(long id)
        {
            if (id > 0)
            {
                var group = SocialBL.GetGroupDetails(VhmSession.SessionID, id);

                if (group != null)
                {
                    if (!group.IsPrivate && (group.CommunityID == VhmSession.CommunityID))
                    {
                        SocialBL.AddMemberToGroup(VhmSession.SessionID, group, VhmSession.MemberID);
                        return new JsonNetResult { Data = true };
                    }
                }
            }

            // No permissions
            return new JsonNetResult { Data = false };
        }

        /// <param name="id">The MemberNotificationId.</param>
        /// <returns></returns>
        public ActionResult _AddMemberToGroupByNotificationId(long id)
        {
            var success = false;

            if (id > 0)
            {
                var memberNotifications = SocialBL.GetMemberNotifications(VhmSession.SessionID, VhmSession.MemberID);
                var memberNotification = memberNotifications.FirstOrDefault(mn => mn.MemberNotificationId == id);

                if (memberNotification != null)
                {
                    var group = SocialBL.GetGroupDetails(VhmSession.SessionID, memberNotification.NotificationTypeEntityId);
                    SocialBL.AddMemberToGroup(VhmSession.SessionID, group, VhmSession.MemberID);
                    success = true;
                }
            }

            if (success)
            {
                return new JsonNetResult { Data = true };
            }
            // No permissions
            return new JsonNetResult { Data = false };
        }

        public ActionResult _SearchGroupByName(string srcName, int pageNumber, int groupsPerPage)
        {
            try
            {
                var sessionId = VhmSession.SessionID;
                var paging = new PagingIM
                {
                    Count = groupsPerPage,
                    MemberId = VhmSession.MemberID,
                    PageNumber = pageNumber
                };
                var rsltList = new PagedGroupsVM();
                string searchVal = EncodeHtml(srcName);
                var getPagedGrps = SocialBL.SearchGroupByGroupName(sessionId, paging, searchVal);
                if (getPagedGrps != null)
                {
                    rsltList = getPagedGrps;
                    if (rsltList.Groups.Count > 0)
                    {
                        foreach (var grp in rsltList.Groups)
                        {
                            grp.Group.Name = DecodeHtml(grp.Group.Name);
                            grp.Group.Description = DecodeHtml(grp.Group.Description);
                            grp.Group.Category = DecodeHtml(grp.Group.Category);
                        }
                    }
                }
                return PartialView("_BuildPagedGroups", rsltList);
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
                return new JsonNetResult { Data = "Service Error" };
            }

        }
        public ActionResult _SearchGroupByAdminName(string srcName, int pageNumber, int groupsPerPage)
        {
            try
            {
                var sessionId = VhmSession.SessionID;
                var paging = new PagingIM()
                {
                    Count = groupsPerPage,
                    MemberId = VhmSession.MemberID,
                    PageNumber = pageNumber
                };
                var rsltList = new PagedGroupsVM();
                var getGrpups = SocialBL.SearchGroupByAdminName(sessionId, paging, srcName);
                if (getGrpups != null)
                {
                    rsltList = getGrpups;
                    if (rsltList.Groups.Count > 0)
                    {
                        foreach (var grp in rsltList.Groups)
                        {
                            grp.Group.Name = DecodeHtml(grp.Group.Name);
                            grp.Group.Description = DecodeHtml(grp.Group.Description);
                            grp.Group.Category = DecodeHtml(grp.Group.Category);
                        }
                    }
                }
                return PartialView("_BuildPagedGroups", rsltList);
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
                return new JsonNetResult { Data = "Service Error" };
            }
        }
        public ActionResult _SearchGroupByCategory(string category, int pageNumber, int groupsPerPage)
        {
            try
            {
                var sessionId = VhmSession.SessionID;
                var categ = (GroupCategories)Convert.ToInt32(category);
                var paging = new PagingIM
                    {
                    Count = groupsPerPage,
                    MemberId = VhmSession.MemberID,
                    PageNumber = pageNumber
                };
                var rsltList = new PagedGroupsVM();
                var getSearchResults = SocialBL.SearchGroupByCategory(sessionId, paging, categ);
                if (getSearchResults != null)
                {
                    rsltList = getSearchResults;
                    if (rsltList.Groups.Count > 0)
                    {
                        foreach (var grp in rsltList.Groups)
                        {
                            grp.Group.Name = DecodeHtml(grp.Group.Name);
                            grp.Group.Description = DecodeHtml(grp.Group.Description);
                            grp.Group.Category = DecodeHtml(grp.Group.Category);
                        }
                    }

                }
                return PartialView("_BuildPagedGroups", rsltList);
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
                return new JsonNetResult { Data = "Service Error" };
            }
        }
        public ActionResult _BuildPagedGroups(string jsonGroupList)
        {
            try
            {
                var pdGroups = new PagedGroupsVM();
                if (jsonGroupList != string.Empty)
                {
                    pdGroups = JsonConvert.DeserializeObject<PagedGroupsVM>(jsonGroupList);
                }
                return View(pdGroups);
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
                return new JsonNetResult { Data = "Service Error" };
            }
        }
        public ActionResult _GroupPendingInvitations()
        {
            try
            {
                string sessionId = VhmSession.SessionID;
                var pedingGroups = SocialBL.GetAllGroupInvitations(sessionId, VhmSession.MemberID);
                #region decode group names and descriptions
                foreach (var item in pedingGroups)
                {
                    var grp = item;

                    grp.GroupInfo.Group.Name = DecodeHtml(grp.GroupInfo.Group.Name);
                    grp.GroupInfo.Group.Name = DecodeHtml(grp.GroupInfo.Group.Name);
                    grp.GroupInfo.Group.Category = DecodeHtml(grp.GroupInfo.Group.Category);
                }
                #endregion
                return View(pedingGroups);
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
                return new JsonNetResult { Data = "Service Error" };
            }
        }
        public ActionResult _DeclineNotification(long nMemberNotificId)
        {
            try
            {
                var success = false;
                string sessionId = VhmSession.SessionID;
                if (nMemberNotificId > 0)
                {
                    var memberNotifications = SocialBL.GetMemberNotifications(sessionId, VhmSession.MemberID);
                    var memberNotification = memberNotifications.FirstOrDefault(mn => mn.MemberNotificationId == nMemberNotificId);
                    if (memberNotification != null && memberNotification.MemberNotificationId > 0)//make sure we found the notification we want to decline
                    {
                        //before declinining make sure we set the member id. Looks like there have been some changes in middle tier and for some reason memberID is 0.
                        memberNotification.MemberId = VhmSession.MemberID;
                        success = SocialBL.DeclineNotification(sessionId, memberNotification);
                    }
                }
                return Json(JsonConvert.SerializeObject(success), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
                return new JsonNetResult { Data = "Service Error" };
            }
        }

        public ActionResult CreateEditGroup(long? id, string mode = "")
        {
            // Moved cache to Adaptors.

            //var contentCreateGroup = (Dictionary<CreateGroupContent, string>)CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.CreateGroup);
            //var contentGroupCreated = (Dictionary<CGYourGroupHasBeenCreatedContent, string>)CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.YourGroupCreated);
            //var contentSocial = (Dictionary<SocialChallengePromotionLabelsContent, string>)CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.SocialChallengePromotionLabelsContent);
            //var contentError = (Dictionary<ErrorMessagesContent, string>)CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.ErrorMessages);
            //var contentCommon = (Dictionary<CommonLabelsContent, string>)CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.CommonLabels);

            // if content not exist in cache then get from Ektron
            //if (contentCreateGroup == null)
            //{
            var contentCreateGroup = MemberBL.GetContentLabels<CreateGroupContent>(PageNames.CreateGroup, LanguageId);
            //  CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.CreateGroup, contentCreateGroup);
            //}
            //if (contentGroupCreated == null)
            //{
            var contentGroupCreated = MemberBL.GetContentLabels<CGYourGroupHasBeenCreatedContent>(PageNames.CreateGroupHasBeenCreated, LanguageId);
            //   CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.YourGroupCreated, contentGroupCreated);
            //}
            //if (contentSocial == null)
            //{
            var contentSocial = MemberBL.GetContentLabels<SocialChallengePromotionLabelsContent>(PageNames.SocialChallengePromotionLabels, LanguageId);
            //   CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.SocialChallengePromotionLabelsContent, contentSocial);
            //}
            //if (contentError == null)
            //{
            var contentError = MemberBL.GetContentLabels<ErrorMessagesContent>(PageNames.ErrorMessages, LanguageId);
            //CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.ErrorMessages, contentError);
            //}
            //if (contentCommon == null)
            //{
            var contentCommon = MemberBL.GetContentLabels<CommonLabelsContent>(PageNames.CommonLabels, LanguageId);
            // CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.CommonLabels, contentCommon);
            //}


            var pageTitle = new PageTitle(true, contentCreateGroup[CreateGroupContent.CreateGroup]);

            var paging = new PagingIM
            {
                MemberId = VhmSession.MemberID,
                PageNumber = 0,
                Count = 9
            };

            var createEditGroup = SocialBL.GetCreateEditGroupVM(VhmSession.SessionID, paging, id, mode, VhmSession.MemberID);
            createEditGroup.ContentCreateGroup = contentCreateGroup;
            createEditGroup.ContentGroupCreated = contentGroupCreated;
            createEditGroup.ContentSocial = contentSocial;
            createEditGroup.ContentError = contentError;
            createEditGroup.ContentCommon = contentCommon;

            if (createEditGroup == null)
            {
                return RedirectToAction("MyGroups");
            }
            createEditGroup.PageTitle = pageTitle;

            var selectedColorTheme = GetSelectedTheme();
            createEditGroup.ThemeCode = selectedColorTheme;

            return View(createEditGroup);
        }
        public ActionResult GetMembers()
        {
            return Json(new List<Member>());
        }
        [GroupIMValidate]
        [HttpPost]
        [ErrorHandle(RedirectPreviousPage = true)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateGroup(GroupIM groupIM)
        {
            if (ModelState.IsValid)
            {
                if (groupIM.Image != null)
                    groupIM.ImageStream = groupIM.Image.InputStream;
                SocialBL.UpdateGroup(VhmSession.SessionID, VhmSession.MemberID, groupIM);
            }
            return RedirectToAction("MyGroups");
        }
        [GroupIMValidate]
        [HttpPost]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddGroup(GroupIM groupIM)
        {
            long? newGroupId = null;
            if (ModelState.IsValid)
            {
                if (groupIM.Image != null)
                {
                    groupIM.ImageStream = groupIM.Image.InputStream;
                }
                //make sure we set the date to en_Us format
                newGroupId = SocialBL.AddGroup(VhmSession.SessionID, VhmSession.MemberID, groupIM, VhmSession.CommunityID);
            }
            if (newGroupId == 0)
            {
                return RedirectToAction("MyGroups");
            }
            if (groupIM.PageMode == "Create_1")
            {
                return RedirectToAction("MyGroups");
            }
            return RedirectToAction("CreateEditGroup", "Social", new { id = newGroupId, mode = "Create_1" });
        }


        public ActionResult DeleteGroup(long id)
        {
            var res = false;
            try
            {
                res = SocialBL.DeleteGroup(VhmSession.SessionID, VhmSession.MemberID, id);
                return Json(JsonConvert.SerializeObject(res), JsonRequestBehavior.AllowGet);
                //return RedirectToAction("MyGroups");
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
                return Json(JsonConvert.SerializeObject(res), JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult IsGroupNameAvalibale(string groupName)
        {
            return Json(SocialBL.IsGroupNameAvalibale(VhmSession.SessionID, groupName));
        }
        
        public PartialViewResult UserProfile(long friendId)
        {
            var model = SocialBL.GetMemberProfile(VhmSession.SessionID, friendId, VhmSession.MemberID, true);

            return PartialView("Member/_UserProfile", model);
        }

        public void LikePost(long postId, long postById, long groupId)
        {
            SocialBL.LikePost(VhmSession.SessionID, VhmSession.MemberID, postById, postId, groupId);
        }

        public JsonResult SubmitPost(string messageText, bool isGroupPost, long groupId)
        {
            var bSuccess = false;
            try
            {
                if (!string.IsNullOrWhiteSpace(messageText))
                {
                    var newsFeedType = isGroupPost
                                           ? (int)NewsFeedPostType.MemberPostedInGroup
                                           : (int)NewsFeedPostType.ManualPost;
                    //apply bad word filter
                    var badWordList = MemberBL.GetBadWords(null);
                    if (badWordList != null && badWordList.Count > 0)
                    {
                        //messageText = badWordList.Aggregate( messageText, ( current, badWrd ) => Regex.Replace( current, "\\b" + badWrd.BadWord + "\\b", new String( '*', badWrd.BadWord.Length ), RegexOptions.IgnoreCase ) );
                        foreach (var badWrd in badWordList)
                        {
                            messageText = Regex.Replace(messageText, "\\b" + badWrd.BadWord + "\\b", new String('*', badWrd.BadWord.Length), RegexOptions.IgnoreCase);
                        }

                        messageText = messageText.Replace("<", "&lt;").Replace(">", "&gt;");
                    }
                    if (groupId <= 0) //set group id = CommunityGroupID for manual news feed posts 
                    {
                        groupId = VhmSession.CommunityGroupID;
                    }
                    bSuccess = SocialBL.SumbitMemberPost(VhmSession.SessionID, VhmSession.MemberID, messageText, groupId, VhmSession.CommunityID, newsFeedType);
                }

                return Json(JsonConvert.SerializeObject(new { Success = bSuccess }), JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(JsonConvert.SerializeObject(new { Success = false }), JsonRequestBehavior.AllowGet);
            }
        }

        //public ActionResult MyNewsFeed()
        //{
        //    var model = new NewsFeedVM();
        //    var pageTitle = new PageTitle(true, "My NewsFeed");
        //    model.PageTitle = pageTitle;

        //    var selectedColorTheme = GetSelectedTheme();
        //    model.ThemeCode = selectedColorTheme;

        //    return View(model);
        //}

        public PartialViewResult NewsFeed()
        {
            const int COMMENT_COUNT = 0;
            byte miniNewsFeedCount = ApplicationSettings.MiniNewsfeedCount;
            var newsfeed = new List<NewsfeedPost>();
            string memberImage = string.Empty;
            string nfUrl = ApplicationSettings.NewsfeedUrl;

            var modelErrors = new Dictionary<NewsFeedVM.NewsFeedModelErrors, string>();

            try
            {
                newsfeed = SocialBL.GetMemberNewsfeed(VhmSession.MemberID, VhmSession.CommunityID,
                                                        COMMENT_COUNT).Take(miniNewsFeedCount).ToList();

                memberImage = SocialBL.GetMemberImagePath(VhmSession.MemberID,
                                                           ApplicationSettings.MemberImageUrl,
                                                            ApplicationSettings.DefaultMemberPicGuid);
                memberImage += "?v=" + VhmSession.MemberID.ToString(CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
                modelErrors.Add(NewsFeedVM.NewsFeedModelErrors.NewsFeed, ex.Message);
            }
            var content = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.NewsfeedContent) as Dictionary<NewsFeedContent, string>;

            if (content == null)
            {
                content = MemberBL.GetContentLabels<NewsFeedContent>(PageNames.NewsfeedContent, LanguageId);
                //  CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.NewsfeedContent, content);
            }
            #region determine a culture. If VhmSession.Culture is null, then get member's browser culture

            var memberCulture = new CultureInfo(DEFAULT_CULTURE);
            if (VhmSession.Culture != null && VhmSession.Culture != string.Empty)
            {
                memberCulture = new CultureInfo(VhmSession.Culture);
            }
            else
            {
                //get browser settings culture
                var l = Request.UserLanguages;
                string browserlanguage = (l != null && l[0] != null) ? l[0] : DEFAULT_CULTURE;
                Thread.CurrentThread.CurrentCulture = new CultureInfo(browserlanguage);
                memberCulture = Thread.CurrentThread.CurrentCulture;
            }
            #endregion
            var newsFeedVM = new NewsFeedVM
            {
                NewsFeed = newsfeed,
                NewsfeedMessage = SocialBL.GetWeeklyStatusUpdatePrompt(VhmSession.SessionID,VhmSession.Culture),
                CurrentCulture = memberCulture,
                MemberImage = memberImage,
                NewsfeedUrl = nfUrl,
                Content = content,
                ModelErrors = modelErrors
            };

            return PartialView("_NewsFeed", newsFeedVM);
        }


        public PartialViewResult NewsFeedV3()
        {
            const int COMMENT_COUNT = 0;
            byte miniNewsFeedCount = ApplicationSettings.MiniNewsfeedCount;
            var newsfeed = new List<NewsfeedPost>();
            string memberImage = string.Empty;
            string nfUrl = ApplicationSettings.NewsfeedUrl;

            var modelErrors = new Dictionary<NewsFeedVM.NewsFeedModelErrors, string>();

            try
            {
                newsfeed = SocialBL.GetMemberNewsfeed(VhmSession.MemberID, VhmSession.CommunityID,
                                                        COMMENT_COUNT).Take(3).ToList();

                memberImage = SocialBL.GetMemberImagePath(VhmSession.MemberID,
                                                           ApplicationSettings.MemberImageUrl,
                                                            ApplicationSettings.DefaultMemberPicGuid);
                memberImage += "?v=" + VhmSession.MemberID.ToString(CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
                modelErrors.Add(NewsFeedVM.NewsFeedModelErrors.NewsFeed, ex.Message);
            }
            var content = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.NewsfeedContent) as Dictionary<NewsFeedContent, string>;

            if (content == null)
            {
                content = MemberBL.GetContentLabels<NewsFeedContent>(PageNames.NewsfeedContent, LanguageId);
                //  CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.NewsfeedContent, content);
            }
            #region determine a culture. If VhmSession.Culture is null, then get member's browser culture

            var memberCulture = new CultureInfo(DEFAULT_CULTURE);
            if (VhmSession.Culture != null && VhmSession.Culture != string.Empty)
            {
                memberCulture = new CultureInfo(VhmSession.Culture);
            }
            else
            {
                //get browser settings culture
                var l = Request.UserLanguages;
                string browserlanguage = (l != null && l[0] != null) ? l[0] : DEFAULT_CULTURE;
                Thread.CurrentThread.CurrentCulture = new CultureInfo(browserlanguage);
                memberCulture = Thread.CurrentThread.CurrentCulture;
            }
            #endregion
            var newsFeedVM = new NewsFeedVM
            {
                NewsFeed = newsfeed,
                NewsfeedMessage = SocialBL.GetWeeklyStatusUpdatePrompt(VhmSession.SessionID, VhmSession.Culture),
                CurrentCulture = memberCulture,
                MemberImage = memberImage,
                NewsfeedUrl = nfUrl,
                Content = content,
                ModelErrors = modelErrors
            };

            return PartialView("LandingPageV3/_NewsFeedV3", newsFeedVM);
        }
        //[OutputCache(Duration = 7200, Location = OutputCacheLocation.Client)]
        public MvcHtmlString SponsorContent()
        {
            var html = new MvcHtmlString("&nbsp;");
            try
            {
                var content = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.ContentAreaSponsorContent) as string;
                if (!string.IsNullOrEmpty(content))
                {
                    return new MvcHtmlString(content);
                }

                if (VhmSession != null)
                {
                    content = SocialBL.GetSponsorContentForMember(VhmSession.MemberID);
                    CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.ContentAreaSponsorContent, content);

                    if (!string.IsNullOrEmpty(content))
                    {
                        return new MvcHtmlString(content);
                    }
                }
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
            }

            return html;
        }


        public MvcHtmlString ContentAreaChildAction(string contentAreaId)
        {
            var html = new MvcHtmlString("&nbsp;");
            try
            {
                if (VhmSession != null)
                {
                    string content = SocialBL.GetVhmContent(VhmSession.PrimarySponsorID, VhmSession.MemberID, contentAreaId, LanguageId );
                    if ( !string.IsNullOrEmpty( content ) )
                    {
                        html = new MvcHtmlString(content);
                    }
                }
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
            }
            return html;
        }

        //[OutputCache(Duration = 7200, Location = OutputCacheLocation.Client, VaryByParam = "contentAreaId")]
        public MvcHtmlString ContentArea(string contentAreaId)
        {
            var html = new MvcHtmlString("&nbsp;");
            try
            {

                // There are multiple content areas on the landing page that we will cache. If not one of those,
                // just get it from the database. When we move away from mapping content keys to enumeration
                // values, we can cache all content areas. This just optimizes the most important ones.
                //bool useCache;
                //var cacheKey = CacheProvider.CacheItems.MainMenu;
                //switch (contentAreaId)
                //{
                //    case "0797af26-fd1e-45a1-ac15-d805dc5f803f":
                //        {
                //            // 2X2 content area
                //            useCache = true;
                //            cacheKey = CacheProvider.CacheItems.ContentArea2X2;
                //            break;
                //        }
                //    case "f89fd910-7b85-49f0-b64d-f57ac85df840":
                //        {
                //            // Banner Ad content area
                //            useCache = true;
                //            cacheKey = CacheProvider.CacheItems.ContentAreaBannerAd;
                //            break;
                //        }
                //    case "cd51f4df-02d9-43a6-90b5-a158706d6f0b":
                //        {
                //            // VHM Content content area
                //            useCache = true;
                //            cacheKey = CacheProvider.CacheItems.ContentAreaVHMContent;
                //            break;
                //        }
                //    case "86b52b1c-3456-4cc0-a31b-4afdbbcefd1b":
                //        {
                //            // Sponsor Content content area
                //            useCache = true;
                //            cacheKey = CacheProvider.CacheItems.ContentAreaSponsorContent;
                //            break;
                //        }
                //    default:
                //        {
                //            break;
                //        }
                //}

                // check if this is a content area that is cached
                //if (useCache && cacheKey != CacheProvider.CacheItems.MainMenu)
                //{
                //    // check if the content is actually in the cache. If it is, return it. If not add it.
                //    content = CacheProvider.Get(System.Web.HttpContext.Current, cacheKey) as string;
                //    if (!string.IsNullOrEmpty(content))
                //    {
                //        return new MvcHtmlString(content);
                //    }
                //}

                // If we got this far, the content was not found in cache
                if (VhmSession != null)
                {
                    string content = SocialBL.GetVhmContent(VhmSession.PrimarySponsorID, VhmSession.MemberID, contentAreaId, LanguageId);
                    // content was not found in the cache. Update it.
                    // cache.Update(cacheKey, content);
                    if (!string.IsNullOrEmpty(content))
                    {
                        html = new MvcHtmlString(content);
                    }
                }
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
            }
            return html;
        }

        public string GetImagePreview()
        {

            string imgString = string.Empty;
            if (Request.Files.Count > 0)
            {
                var postedFileBase = Request.Files[0];
                if (postedFileBase != null && postedFileBase.ContentLength < 1572864) // 1.5MB limit
                {

                    var img = Request.Files[0];

                    if (img != null)
                    {
                        var buffer = new byte[img.InputStream.Length];
                        img.InputStream.Read(buffer, 0, (int)img.InputStream.Length);
                        imgString = Convert.ToBase64String(buffer);

                        Session["ContentLength"] = img.InputStream.Length;
                        var httpPostedFileBase = Request.Files[0];
                        if ( httpPostedFileBase != null )
                        {
                            Session["ContentType"] = httpPostedFileBase.ContentType;
                        }
                        byte[] resizedImage = Vhm.Web.Common.Utils.ImageResizer.CreateThumbnail(buffer);
                        Session["ContentStream"] = resizedImage;
                    }
                }
                else
                {
                    imgString = "FILESIZE:The picture excides max size limit of 1.5MB";
                }
            }
            return imgString;
        }

        #region Private methods
        private string EncodeHtml(string whatToEncode)
        {
            string encodedHtml = Server.HtmlEncode(whatToEncode);
            return encodedHtml;
        }

        private string DecodeHtml(string whatToDecode)
        {
            string decodedHtml = System.Net.WebUtility.HtmlDecode(whatToDecode);
            //decode one more time as database seems to encode encoded caracters
            decodedHtml = System.Web.HttpUtility.HtmlDecode(decodedHtml);
            return decodedHtml;
        }
        #endregion
    }
}
