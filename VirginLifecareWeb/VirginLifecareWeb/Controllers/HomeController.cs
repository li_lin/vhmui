﻿using System.Web.Mvc;
using Vhm.Web.BL;
using Vhm.Web.BL.Interfaces;

namespace VirginLifecareWeb.Controllers
{
    public class HomeController : ControllerBase
    {
        public HomeController() : base()
        {
        }
        public ActionResult Index()
        {
            return View();
        }
    }
}
