﻿using System.Web.Mvc;
using Vhm.Web.Models.RepositoryModels;
using Vhm.Web.Models.ViewModels.Shared;

namespace VirginLifecareWeb.Controllers
{
    public class Model
    {
        public ColorTheme ThemeCode { get; set; }
    }

    public class NutritionController : ControllerBase
    {
        public NutritionController()
        {
        }

        [ErrorHandle(Action = "Error")]
        public ActionResult LandingPage()
        {
            var model = new Model()
            {
                ThemeCode = GetSelectedTheme()
            };

            ViewData["pageTitle"] = new PageTitle
            {
                PageTitleName = "Nutrition",
                RightLinkText = string.Empty,
                ShowPageTitle = true,
                ShowRightTitleLink = false
            };
            return View(model);
        }
    }
}