﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace VirginLifecareWeb.Controllers
{
    public class ErrorHandleAttribute : HandleErrorAttribute
    {
        public string WhatToDo { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
        public bool RedirectPreviousPage { get; set; }

        public override void OnException(ExceptionContext filterContext)
        {
            if (RedirectPreviousPage)
            {
                filterContext.Result = new RedirectResult(filterContext.RequestContext.HttpContext.Request.UrlReferrer.ToString());
                filterContext.Controller.TempData.Add("Errors", "Something went wrong");
            }else
            {
                filterContext.Result = new RedirectResult(Action);
                if (!filterContext.Controller.TempData.ContainsKey("LastError"))
                {
                    filterContext.Controller.TempData.Add( "LastError", filterContext.Exception );
                }
            }

            string showExceptionSetting = System.Configuration.ConfigurationManager.AppSettings.Get("ShowException");
            VirginLifecare.Exception.ExceptionHandler.ProcessException(filterContext.Exception, true);
            if (showExceptionSetting != null && Convert.ToBoolean(showExceptionSetting)) {

                //write error message - with machine name and date
               // lbErrorMessage.Text = string.Format("<p class='font13Verdana'><strong>{0}</strong></p><p height='30'>&nbsp;</p><p><strong>Exception Details:</strong> {1}<br /></p><p><strong>Source Error:</strong> {2}</p><p><strong>Source File:</strong> {3}</p><p><strong>Stack Trace:</strong><br />{4}</p><p><strong>Date: </strong>{5:u} {6}</p>", errMessage, errTypeMessage, errSource, filterString(Request.Url.ToString()), errStack, DateTime.Now.ToUniversalTime(), System.Environment.MachineName);
            } 

            filterContext.ExceptionHandled = true;
            
        }

    }
}