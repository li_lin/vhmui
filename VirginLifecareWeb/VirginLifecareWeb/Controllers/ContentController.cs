﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Xml;
using Vhm.Web.BL;
using Vhm.Web.DataServices.Adapters;
using Vhm.Web.Models.RepositoryModels;
using Vhm.Web.Models.ViewModels.CmsContent;

namespace VirginLifecareWeb.Controllers
{
    [Authorize]
    public class ContentController : ControllerBase
    {
        #region New Content areas
        public PartialViewResult RichContent(string contentAreaType, bool sponsorSpecific = false)
        {
            long sponsorId = 0;
            if (sponsorSpecific == true)
            {
                SponsorProfile sponsorProfile = GetSponsorProfile();
                sponsorId = sponsorProfile.SponsorID;
            }

            var viewModel = new RichContent();

            if (!String.IsNullOrEmpty(contentAreaType))
            {
                ContentAdapter _contentAdapter = new ContentAdapter();
                var content = _contentAdapter.GetEktronRichContent(contentAreaType, sponsorId, LanguageId);
                viewModel.Title = content.Title;
                viewModel.Html = content.Html;

                if (!string.IsNullOrEmpty(viewModel.Html) && !string.IsNullOrEmpty(content.Html) && content.Html.Contains("{spouseCode}"))
                {
                    Member member = MemberBL.GetMemberById(VhmSession.SessionID, VhmSession.MemberID);
                    viewModel.Html = member != null && member.SpouseCode != null
                                         ? content.Html.Replace("{spouseCode}", member.SpouseCode)
                                         : content.Html.Replace("{spouseCode}", "");
                }

            }
            return PartialView("_HtmlRichContent", viewModel);
        }

        public PartialViewResult LatestMemberEngagementArticle(string contentAreaType, bool sponsorSpecific = false)
        {
            long sponsorId = 0;
            if (sponsorSpecific == true)
            {
                SponsorProfile sponsorProfile = GetSponsorProfile();
                sponsorId = sponsorProfile.SponsorID;
            }

            var viewModel = new MemberEngagementArticle();


            if (!String.IsNullOrEmpty(contentAreaType))
            {
                ContentAdapter _contentAdapter = new ContentAdapter();
                List<EktronRichContent> contentList = new List<EktronRichContent>();
                contentList = _contentAdapter.GetSponsorLatestBlogPosts(contentAreaType, sponsorId, LanguageId, 1);

                if (contentList.Count > 0)
                {

                    XmlDocument doc = new XmlDocument();
                    try
                    {
                        doc.LoadXml(contentList[0].Html);
                        viewModel.Title = doc.SelectSingleNode("/ArticleBlog/Title").InnerText;
                        viewModel.Summary = doc.SelectSingleNode("/ArticleBlog/IntroductionText").InnerXml.ToString();
                        viewModel.FullText = doc.SelectSingleNode("/ArticleBlog/FullText").InnerXml.ToString();
                        viewModel.PreviewImageUrl = doc.SelectSingleNode("/ArticleBlog/PreviewImageUrl").InnerXml.ToString();
                        viewModel.FullArticleImageUrl = doc.SelectSingleNode("/ArticleBlog/FullArticleImageUrl").InnerXml.ToString();
                        viewModel.GoogleAnalyticsTrackingCode = doc.SelectSingleNode("/ArticleBlog/GoogleAnalyticsTrackingCode").InnerXml.ToString();
                        viewModel.Id = contentList[0].Id;
                        viewModel.ThemeColor = doc.SelectSingleNode("/ArticleBlog/Color").InnerXml.ToString();
                        viewModel.HideReadMoreLink = doc.SelectSingleNode("/ArticleBlog/HideReadMore").InnerXml.ToString();
                        viewModel.CustomLinkText = doc.SelectSingleNode("/ArticleBlog/CustomReadMore/a").InnerXml.ToString();
                        
                        if (String.IsNullOrEmpty(doc.SelectSingleNode("/ArticleBlog/CustomReadMore/a").Attributes["href"].Value))
                            viewModel.CustomLinkUrl = doc.SelectSingleNode("/ArticleBlog/CustomReadMore/a").Attributes["href"].Value;


                        viewModel.DateCreated = contentList[0].DateCreated;
                    }
                    catch (Exception ex)
                    {
                    }

                }
            }
            return PartialView("_MemberEngagementLeftFooterBanner", viewModel);
        }

        public ViewResult Engagement(long id)
        {
            var viewModel = new MemberEngagementArticle();

            ContentAdapter _contentAdapter = new ContentAdapter();
            EktronRichContent content = new EktronRichContent();
            content = _contentAdapter.GetBlogPostById(id, LanguageId);

            if (content != null)
            {
                XmlDocument doc = new XmlDocument();
                try
                {
                    doc.LoadXml(content.Html);
                    viewModel.Title = doc.SelectSingleNode("/ArticleBlog/Title").InnerText;
                    viewModel.Summary = doc.SelectSingleNode("/ArticleBlog/IntroductionText").InnerXml.ToString();
                    viewModel.FullText = doc.SelectSingleNode("/ArticleBlog/FullText").InnerXml.ToString();
                    viewModel.PreviewImageUrl = doc.SelectSingleNode("/ArticleBlog/PreviewImageUrl").InnerXml.ToString();
                    viewModel.FullArticleImageUrl = doc.SelectSingleNode("/ArticleBlog/FullArticleImageUrl").InnerXml.ToString();
                    viewModel.Id = content.Id;
                    viewModel.DateCreated = content.DateCreated;
                }
                catch (Exception ex)
                {
                }

            }


            return View("MemberEngagementArticle", viewModel);
        }

        public ViewResult BlogArticle(long id)
        {
            var viewModel = new BlogArticle();

            ContentAdapter _contentAdapter = new ContentAdapter();
            EktronRichContent content = new EktronRichContent();
            content = _contentAdapter.GetBlogPostById(id, LanguageId);

            if (content != null)
            {

                BlogArticle blogArticleContent = new BlogArticle();
                XmlDocument doc = new XmlDocument();
                try
                {
                    doc.LoadXml(content.Html);
                    blogArticleContent.Title = doc.SelectSingleNode("/ArticleBlog/Title").InnerText;
                    blogArticleContent.Summary = doc.SelectSingleNode("/ArticleBlog/IntroductionText").InnerXml.ToString();
                    blogArticleContent.FullText = doc.SelectSingleNode("/ArticleBlog/FullText").InnerXml.ToString();
                    blogArticleContent.ImgUrl = doc.SelectSingleNode("/ArticleBlog/Image/img/@src").Value.ToString();
                    blogArticleContent.Id = content.Id;
                    blogArticleContent.DateCreated = content.DateCreated;
                    viewModel = (blogArticleContent);
                }
                catch (Exception ex)
                {
                }

            }


            return View("BlogPost", viewModel);
        }

        public ViewResult LatestBlogPosts(bool sponsorSpecific = false, int count = 3)
        {
            long sponsorId = 0;
            if (sponsorSpecific == true)
            {
                SponsorProfile sponsorProfile = GetSponsorProfile();
                sponsorId = sponsorProfile.SponsorID;
            }

            var viewModel = new BlogList();
            viewModel.Articles = new List<BlogArticle>();

            ContentAdapter _contentAdapter = new ContentAdapter();
            List<EktronRichContent> contentList = new List<EktronRichContent>();
            contentList = _contentAdapter.GetSponsorLatestBlogPosts("BlogPost", sponsorId, LanguageId, count);

            if (contentList != null && contentList.Any())
            {
                foreach (var item in contentList)
                {
                    BlogArticle blogArticleContent = new BlogArticle();
                    XmlDocument doc = new XmlDocument();
                    try
                    {
                        doc.LoadXml(item.Html);
                        blogArticleContent.Title = doc.SelectSingleNode("/ArticleBlog/Title").InnerText;
                        blogArticleContent.Summary = doc.SelectSingleNode("/ArticleBlog/IntroductionText").InnerXml.ToString();
                        blogArticleContent.FullText = doc.SelectSingleNode("/ArticleBlog/FullText").InnerXml.ToString();
                        blogArticleContent.ImgUrl = doc.SelectSingleNode("/ArticleBlog/Image/img/@src").Value.ToString();
                        blogArticleContent.Id = item.Id;
                        blogArticleContent.DateCreated = item.DateCreated;
                        viewModel.Articles.Add(blogArticleContent);
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }


            return View("BlogListPage", viewModel);
        }
        #endregion

        #region Old Landing Page Content

        public PartialViewResult OldLandingTwoByTwo(string contentAreaType, bool sponsorSpecific = false)
        {
            long sponsorId = 0;
            if (sponsorSpecific == true)
            {
                SponsorProfile sponsorProfile = GetSponsorProfile();
                sponsorId = sponsorProfile.SponsorID;
            }

            var viewModel = new OldLandingTwoByTwo();


            if (!String.IsNullOrEmpty(contentAreaType))
            {
                ContentAdapter _contentAdapter = new ContentAdapter();
                EktronRichContent content = new EktronRichContent();
                content = _contentAdapter.GetEktronRichContent(contentAreaType, sponsorId, LanguageId);


                if (content != null)
                {

                    XmlDocument doc = new XmlDocument();
                    try
                    {
                        doc.LoadXml(content.Html);
                        viewModel.Title = doc.SelectSingleNode("/ArticleBlog/Title").InnerText;
                        viewModel.Summary = doc.SelectSingleNode("/ArticleBlog/IntroductionText").InnerXml.ToString();
                        viewModel.BackgroundImage = doc.SelectSingleNode("/ArticleBlog/PreviewImageUrl").InnerXml.ToString();
                    }
                    catch (Exception ex)
                    {
                    }

                }
            }
            return PartialView("_OldLandingTwobyTwo", viewModel);
        }

        public PartialViewResult OldLandingBannerHorizontalBanner(string contentAreaType, bool sponsorSpecific = false)
        {
            long sponsorId = 0;
            if (sponsorSpecific == true)
            {
                SponsorProfile sponsorProfile = GetSponsorProfile();
                sponsorId = sponsorProfile.SponsorID;
            }

            var viewModel = new HomepageBanner();


            if (!String.IsNullOrEmpty(contentAreaType))
            {
                ContentAdapter _contentAdapter = new ContentAdapter();
                EktronRichContent content = new EktronRichContent();
                content = _contentAdapter.GetEktronRichContent(contentAreaType, sponsorId, LanguageId);

                if (content != null)
                {
                    viewModel.RichHtml = content.Html;
                }
            }
            return PartialView("_HomepageBanner", viewModel);
        }

        public PartialViewResult OldLandingMemberEngagementContent(string contentAreaType, bool sponsorSpecific = false)
        {
            long sponsorId = 0;
            if (sponsorSpecific == true)
            {
                SponsorProfile sponsorProfile = GetSponsorProfile();
                sponsorId = sponsorProfile.SponsorID;
            }

            var viewModel = new MemberEngagementArticle();


            if (!String.IsNullOrEmpty(contentAreaType))
            {
                ContentAdapter _contentAdapter = new ContentAdapter();
                List<EktronRichContent> contentList = new List<EktronRichContent>();
                contentList = _contentAdapter.GetSponsorLatestBlogPosts(contentAreaType, sponsorId, LanguageId, 1);


                if (contentList.Count > 0)
                {

                    XmlDocument doc = new XmlDocument();
                    try
                    {
                        doc.LoadXml(contentList[0].Html);
                        viewModel.Title = doc.SelectSingleNode("/ArticleBlog/Title").InnerText;
                        viewModel.Summary = doc.SelectSingleNode("/ArticleBlog/IntroductionText").InnerXml.ToString();
                        viewModel.FullText = doc.SelectSingleNode("/ArticleBlog/FullText").InnerXml.ToString();
                        viewModel.PreviewImageUrl = doc.SelectSingleNode("/ArticleBlog/PreviewImageUrl").InnerXml.ToString();
                        viewModel.FullArticleImageUrl = doc.SelectSingleNode("/ArticleBlog/FullArticleImageUrl").InnerXml.ToString();
                        viewModel.GoogleAnalyticsTrackingCode = doc.SelectSingleNode("/ArticleBlog/GoogleAnalyticsTrackingCode").InnerXml.ToString();
                        viewModel.Id = contentList[0].Id;
                        viewModel.ThemeColor = doc.SelectSingleNode("/ArticleBlog/Color").InnerXml.ToString();
                        viewModel.CustomLinkText = doc.SelectSingleNode("/ArticleBlog/CustomReadMore/a").InnerXml.ToString();
                        if (String.IsNullOrEmpty(doc.SelectSingleNode("/ArticleBlog/CustomReadMore/a").Attributes["href"].Value))
                            viewModel.CustomLinkUrl = doc.SelectSingleNode("/ArticleBlog/CustomReadMore/a/@href").Value;
                        viewModel.DateCreated = contentList[0].DateCreated;
                    }
                    catch (Exception ex)
                    {
                    }

                }
            }
            return PartialView("_OldLandingMemberEngagementContent", viewModel);
        }




        #endregion
    }

}