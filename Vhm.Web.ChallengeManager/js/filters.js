'use strict';

/* Filters */

angular.module('ChallengeManager.filters', []).
  filter('interpolate', ['version', function(version) {
      return function(text) {
          return String(text).replace(/\%VERSION\%/mg, version);
      };
  }]).
filter('increment', ['page', function(page) {
    return function (text) {
        page = page + 1;
        return String(text).replace(/\%PAGE_NAME\%/mg, page);
    };
}]);
