'use strict';

var page = 0;


// Declare app level module which depends on filters, and services
angular.module('ChallengeManager', ['ChallengeManager.filters', 'ChallengeManager.services', 'ChallengeManager.directives', 'ngCookies', 'ngSanitize', '$strap.directives']).
    config(['$routeProvider', '$httpProvider', function($routeProvider, $httpProvider, $http) {
        $routeProvider
            .when('/list/:id',  { templateUrl: 'partials/list.html',  controller: ChallengeManagerCtrl })
            .when('/step1/:id', { templateUrl: 'partials/step1.html', controller: Step1Ctrl })
            .when('/step2/:id', { templateUrl: 'partials/step2.html', controller: Step2Ctrl })
            .when('/step3/:id', { templateUrl: 'partials/step3.html', controller: Step3Ctrl })
            .when('/step4/:id', { templateUrl: 'partials/step4.html', controller: Step4Ctrl })
            .when('/step5/:id', { templateUrl: 'partials/step5.html', controller: Step5Ctrl });

        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
        $routeProvider.otherwise({ redirectTo: '/list/:id' });
    }]);
