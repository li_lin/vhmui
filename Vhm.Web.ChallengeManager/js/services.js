'use strict';

var rootSponsorGetUrl = "/rest/v1/challenges/sponsorchallenges";
var rootChallengeUrl = "/rest/v1/challenges/sponsorchallenges/challenge";

var services = angular.module('ChallengeManager.services', ['ngResource'])
    .factory('Summary', function() {
        return { Display: false };
    })
    .factory('SponsorChallenges', function($resource) {
        return $resource(rootSponsorGetUrl, {
            query: { url: rootSponsorGetUrl, method: 'GET', cache: false, isArray: false, responseType: 'json' },
            get: { url: rootSponsorGetUrl, method: 'GET', params: { sponsorId: '@sponsorId', type: '@type' }, cache: false },
            //save: { url: rootUpdateUrl, method: 'POST', cache: false, isArray: false, responseType: 'json' },
            //update: { method: 'PUT' },
        });
    })
    .factory('Challenges', function ($resource) {
        return $resource(rootChallengeUrl + '/', {
            query: { url: rootChallengeUrl, method: 'GET', cache: false, isArray: false, responseType: 'json' },
            get: { url: rootChallengeUrl, method: 'GET', cache: false, isArray: false },
            save: { url: rootChallengeUrl, method: 'POST', cache: false, isArray: false, responseType: 'json' },
            update: { method: 'PUT' },
        });
    })
    .factory('DeleteChallenges', function($resource) {
        return $resource(rootChallengeUrl + '/:id', { id: '@Id' }, {
            remove: { method: 'DELETE', params: { id: '@Id' } }
        });
    });