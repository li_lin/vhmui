'use strict';

function setTheme(rootScope, scope) {
    rootScope.Theme = scope.Theme = {
        Class: 'Pul' 
    };
}

function ChallengeManagerCtrl($scope, $rootScope, $location, $http, $cookies, Challenges, SponsorChallenges, DeleteChallenges, $routeParams) {

    $scope.Model = {};
    $scope.$parent.DisplaySummary = false;
    $scope.SearchText = '';
    $scope.CurrentPage = 0;

    $scope.Model.sponsorId = $routeParams.id;

    if ($scope.Model.sponsorId == null || $scope.Model.sponsorId.length == 0) $scope.Model.sponsorId = 1;

    $scope.Model.Save = function(challenge) {

        if (challenge.Validate()) {
            if (challenge.ChallengeId == 0) {
                
                Challenges.save(challenge,
                    function(newChallenge) {
                        $scope.Model.Challenge.ChallengeId = newChallenge.ChallengeID;
                    },
                    function() {
                        alert('error');
                    });
            } else {
                Challenges.update(challenge,
                    function(newChallenge) {
                        $scope.Model.Challenge.ChallengeId = newChallenge.ChallengeID;
                    },
                    function() {
                        alert('error');
                    });
            }
        } else {
            
        }
    };

    $scope.CancelChallenge = function(challenge) {

        if (confirm('Are you sure you want to cancel ' + challenge.Name + '?')) {
            DeleteChallenges.remove({ id: challenge.ChallengeId },
                function() {
                    challenge.Status = 'Cancelled';
                },
                function() {
                    alert('oops');
                });
        }
    };

    $scope.EditChallenge = function(challenge) {
        $location.path("/step1/" + challenge.ChallengeId);
    };
    $scope.SetPage = function(idx) {
        $scope.CurrentPage = idx;
    };

    if ($location.$$url.indexOf('list') >= 0) {
        SponsorChallenges.get({ sponsorId: $scope.Model.sponsorId, type: 'TRK' },
            function(raw) {

                $scope.Model.Pages = [];

                var pagesize = 20;
                var data = raw.reverse();

                var aChallenges = new Array();
                for (var i = 0; i < data.length; i++) {

                    var challenge = {};

                    switch (data[i].ChallengeType) {
                    case 'TRK':
                    {
                        challenge.Type = data[i].OwnerType == 'CRP' ? 'Corporate Tracking' : 'Personal Tracking';
                        break;
                    }
                    case 'GZS':
                    {
                        challenge.Type = 'Most Steps';
                        break;
                    }
                    }
                    var startDateMs = Date.parse(data[i].StartDate); // date milliseconds
                    var startDate = new Date(startDateMs);

                    var endDateMs = Date.parse(data[i].EndDate);
                    var endDate = new Date(endDateMs);

                    challenge.ChallengeId = data[i].ChallengeID;
                    challenge.StartDate = (startDate.getMonth() + 1) + '/' + startDate.getDate() + '/' + startDate.getFullYear();
                    challenge.EndDate = (endDate.getMonth() + 1) + '/' + endDate.getDate() + '/' + endDate.getFullYear();
                    challenge.Name = data[i].Name;

                    /*
                 Cancelled = cancelled date has a value, 
                 In Progress = today is between start and upload deadline, 
                 Completed = today is after the upload deadline
                */

                    var today = new Date();
                    if (data[i].CancelDate != null) {
                        challenge.Status = 'Cancelled';
                    } else {
                        if (today < startDate) {
                            challenge.Status = 'Pending';
                        }
                        if (today >= startDate && today <= endDate) {
                            challenge.Status = 'In Progress';
                        }
                        if (today > endDate) {
                            challenge.Status = 'Completed';
                        }
                    }
                    aChallenges.push(challenge);
                }

                aChallenges = aChallenges.sort(sortDesc);

                // apply paging
                var ChallengeList = [];
                for (var i = 0; i < aChallenges.length; i++) {
                    var challenge = aChallenges[i];

                    ChallengeList.push(challenge);

                    if ((i + 1) % pagesize == 0) {
                        $scope.Model.Pages.push(ChallengeList);
                        var ChallengeList = [];
                    }
                }
                if (ChallengeList.length > 0) {
                    $scope.Model.Pages.push(ChallengeList); // get the last page
                }

            },
            function() {
                alert('oops');
            });
    }

    function sortDesc(c1, c2) {

        var d1, d2;
        d1 = new Date(c1.StartDate);
        d2 = new Date(c2.StartDate);
        if (d1 < d2)
            return 1;
        if (d1 > d2)
            return -1;
        return 0;
    }

    $scope.Model.Challenge = {
        ChallengeId: 0,
        ChallengeType: '',
        Name: '',
        IsChatEnabled: null,
        CreatedDate: new Date(),
        UpdatedDate: new Date(),
        Leaderboard: null,
        RulesCopy: '',
        StageNumber: 1,
        StartDate: null,
        EndDate: null,
        SignUpStart: null, // new Date("2014-01-05T00:00:00.000Z"),
        SignUpDeadline: null, // new Date("2014-01-12T00:00:00.000Z"),
        SignUpDuration: null,
        UploadDeadline: null,
        ChallengeDuration: null,
        UploadGracePeriod: null,
        RewardType: '',
        RewardSubType: '',
        OwnerId: $scope.Model.sponsorId,
        OwnerType: 'CRP',
        TeamCreationType: 'MANUAL',
        ParticipationType: 'ALL_MEMBERS',
        Details: '',
        Message: '',
        GetDuration: function(date1, date2) {

            var ret = '';
            if (date1 && date2) {
                var d1 = Date.parse(date1);
                var d2 = Date.parse(date2);
                var milliseconds = new Date(d2 - d1);
                var seconds = (milliseconds / 1000) | 0;
                var minutes = (seconds / 60) | 0;
                var hours = (minutes / 60) | 0;
                var days = (hours / 24) | 0;
                var weeks = (days / 7) | 0;
                days -= weeks * 7;

                if (weeks == 1) {
                    ret = weeks + ' week ';
                } else if (weeks > 1) {
                    ret = weeks + ' weeks ';
                }
                if (days == 1) {
                    ret += days + ' day';
                } else if (days > 1) {
                    ret += days + ' days';
                }

                this.SignUpDuration = ret;
            } else {
                this.SignUpDuration = '';
            }
            return ret;
        },
        IsValid: true,
        ShowInBrowseOnly: false,
        ChallengeQuestions: [
            {
                ChallengeQuestionID: 0,
                Question: '',
                AnswerType: '',
                QuestionFrequency: 1,
                QuestionFrequencyDatePart: 'dd',
                DisplayType: 'STR',
                MaxValue: null,
                ScoreType: 'ENT',
                QuestionPromptType: '',
                TargetType: null
            }
        ],
        Validate: function() {
            this.Errors = [];
            if (this.ChallengeType.length == 0) this.Errors.push('Challenge Type');
            if (this.Name.length == 0) this.Errors.push('Challenge Name');
            if (this.IsChatEnabled == null) this.Errors.push('Chat Enabled/Disabled');
            if (this.Leaderboard == null) this.Errors.push('Leaderboard Enabled/Disabled');
            if (this.ChallengeQuestions[0].AnswerType.length == 0) this.Errors.push('Answer Type');
            if (this.ChallengeQuestions[0].AnswerType == 'NUM' && this.ChallengeQuestions[0].MaxValue == null) this.Errors.push('Target');
            if (this.ChallengeQuestions[0].AnswerType == 'NUM' && this.ChallengeQuestions[0].TargetType == null) this.Errors.push('Target Type');
            if (this.RulesCopy.length == 0) this.Errors.push('Description');
            if (this.ChallengeQuestions[0].Question.length == 0) this.Errors.push('Challenge Question');
            if (this.SignUpStart == null) this.Errors.push('SignUp Start Date');
            if (this.SignUpDeadline == null) this.Errors.push('SignUp End Date');
            if (this.StartDate == null) this.Errors.push('Start Date');
            if (this.EndDate == null) this.Errors.push('End Date');
            if (this.UploadDeadline == null) this.Errors.push('Tracking Deadline');
            
            this.IsValid = this.Errors.length == 0;
            return this.IsValid;
        },
        SetPromptType: function() {
            var ansType = this.ChallengeQuestions[0].AnswerType;
            this.ChallengeQuestions[0].QuestionPromptType = ansType == 'YNO' ? 'YNO' : 'TXT';
        },
        Errors: []
    };

    $scope.Model.ChallengeTypes = [
        { Description: 'Select', Type: '' },
        { Description: 'Most Steps', Type: 'ACT' },
        { Description: 'Tracking Challenge', Type: 'TRK' }
    ];
    $scope.Model.RewardTypes = [
        { Description: 'Select', Type: '' },
        { Description: ' Points', Type: 'PTS' }
    ];
    $scope.Model.RewardSubTypes = [
        { Description: 'Select', Type: '' },
        { Description: 'Activity', Type: 'ACT' },
        { Description: 'Measurement', Type: 'MES' },
        { Description: 'Participation', Type: 'PAR' }
    ];
    $scope.Model.AvailableBadges = [
        { Selected: true, BadgeId: 297, Url: 'https://mashauth1.virginhealthmiles.com/images/aac3a1a2-7906-45ed-bc2b-97163c2508c4.png', Title: 'Skip Desert' },
        { Selected: false, BadgeId: 299, Url: 'https://mashauth1.virginhealthmiles.com/images/93c3924e-64c8-4480-af83-ea1d42e69524.png', Title: 'Bring Lunch From Home' },
        { Selected: false, BadgeId: 300, Url: 'https://mashauth1.virginhealthmiles.com/images/d0f3dcb9-d24d-45b2-a8e0-071aea95211e.png', Title: 'Healthy Recipes' },
        { Selected: false, BadgeId: 301, Url: 'https://mashauth1.virginhealthmiles.com/images/c4b6ebc7-60e5-46fe-953c-68a8ee80d3e0.png', Title: 'Veggie' },
        { Selected: false, BadgeId: 320, Url: 'https://mashauth1.virginhealthmiles.com/images/36848101-6a0c-4cbb-b1d9-0ca926eaebd3.png', Title: 'Drink 8 Glasses of Water' }
    ];

}

ChallengeManagerCtrl.$inject = ['$scope', '$rootScope', '$location', '$http', '$cookies', 'Challenges', 'SponsorChallenges', 'DeleteChallenges', '$routeParams'];

function Step1Ctrl($scope, $rootScope, $http, $cookies, $routeParams, Challenges) {

    if (!$cookies.SessionID || $cookies.SessionID == "") {
        window.location = '/login.aspx';
        return;
    }
    $scope.$parent.DisplaySummary = true;
    setTheme($rootScope, $scope);

    var id = $routeParams.id;
    if (id > 0) {

        Challenges.get({ challengeId: id },
            function (challenge) {
                debugger;
                var signUpStartDateMs = Date.parse(challenge.StartDate); // date milliseconds
                var signUpStartDate = new Date(signUpStartDateMs);

                var startDateMs = Date.parse(challenge.StartDate); // date milliseconds
                var startDate = new Date(startDateMs);

                var startDateMs = Date.parse(challenge.StartDate); // date milliseconds
                var startDate = new Date(startDateMs);

                var endDateMs = Date.parse(challenge.EndDate);
                var endDate = new Date(endDateMs);

                challenge.StartDate = (startDate.getMonth() + 1) + '/' + startDate.getDate() + '/' + startDate.getFullYear();
                challenge.EndDate = (endDate.getMonth() + 1) + '/' + endDate.getDate() + '/' + endDate.getFullYear();


                $scope.Model.Challenge = challenge;
            },
            function() {
                alert('error');
            });
    } else {
        $scope.Model.Challenge = $scope.$parent.Model.Challenge;
    }

    $scope.Model.Challenge.Errors = [];

    var sessionId = '';
    $rootScope.SessionId = '';
    
}

Step1Ctrl.$inject = ['$scope', '$rootScope', '$http', '$cookies', '$routeParams', 'Challenges'];

function Step2Ctrl($scope, $rootScope, $http, $cookies) {

    $scope.$parent.DisplaySummary = true;
    setTheme($rootScope, $scope);
}
Step2Ctrl.$inject = ['$scope', '$rootScope', '$http', '$cookies'];

function Step3Ctrl($scope, $rootScope, $http, $cookies) {

    $scope.$parent.DisplaySummary = true;
    setTheme($rootScope, $scope);
}
Step3Ctrl.$inject = ['$scope', '$rootScope', '$http', '$cookies'];

function Step4Ctrl($scope, $rootScope, $http, $cookies) {

    $scope.$parent.DisplaySummary = true;
    setTheme($rootScope, $scope);
}
Step4Ctrl.$inject = ['$scope', '$rootScope', '$http', '$cookies'];

function Step5Ctrl($scope, $rootScope, $http, $cookies) {

    $scope.$parent.DisplaySummary = true;
    setTheme($rootScope, $scope);
}
Step5Ctrl.$inject = ['$scope', '$rootScope', '$http', '$cookies'];

function NavigationCtrl($scope) {

    //$scope.CurrentPage = 'page1';
}

NavigationCtrl.$inject = ['$scope'];








