﻿'use strict';

/* Directives */
angular.module('ChallengeManager.directives', [])
    /*APPLICATION VERSION*/
    .directive('appVersion', ['version', function(version) {
        return function(scope, elm, attrs) {
            elm.text(version);
        };
    }])
    /*NAVIGATION*/
    .directive('nav', function() {
        return {
            restrict: 'E',
            controller: NavigationCtrl,
            replace: true,
            scope: true, // give each instance of the nav link its own scope
            template: '<h2><a class="slideNav {{Class}}" href="#/{{Page}}">{{Text}}</a></h2>',
            link: function(scope, elem, attrs) {
                scope.Page = elem.attr('target');
                scope.Text = elem.attr('text');
                scope.Class = elem.attr('class');
            }
        };
    });
    /*DATE PICKER*/
    //.directive('datepicker', function () {
    //    return {
    //        restrict: 'E',
    //        require: ['ngModel'],
    //        scope: {
    //            ngModel: '='
    //        },
    //        replace: true,
    //        template:
    //            '<div class="input-group">' +
    //                '<input type="text"  class="form-control" ngModel required>' +
    //                '<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>' +
    //                '</div>',
    //        link: function(scope, element, attrs) {
    //            var input = element.find('input');
    //            var nowTemp = new Date();
    //            var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    //            console.log(now);
    //            console.log(nowTemp);

    //            input.datepicker({
    //                format: "yyyy-mm-dd",
    //                onRender: function(date) {
    //                    return date.valueOf() < now.valueOf() ? 'disabled' : '';
    //                }
    //            });

    //            element.bind('blur keyup change', function() {
    //                scope.ngModel = input.val();
    //                console.info('date-picker event', input.val(), scope.ngModel);
    //            });
    //        }
    //    };
    //});
    
