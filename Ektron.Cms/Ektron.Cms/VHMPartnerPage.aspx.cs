﻿using System;
using System.Web;
using Ektron.Cms.Framework.User;
using Ektron.Cms.Framework.Content;

namespace Ektron.Cms {
    public partial class VHMPartnerPage : Ektron.Cms.PageBuilder.PageBuilder {
        protected SponsorSettings SponsorSettings { get; set; }

        protected void Page_Load(object sender, EventArgs e) {
            SponsorSettingsHelper sponsorHelper = new SponsorSettingsHelper();
            SponsorSettings = sponsorHelper.GetSettings();
            if (SponsorSettings == null) {
                SponsorSettings = new SponsorSettings();
            }

            //TODO: Based on settings, set logo and color theme

            //VirginLifecare.Stateful oResult = new Stateful();
            //string strSessionID = string.Empty;
            //if (strSessionID.Length != 0) {
            //    VirginLifecare.Stateful objParameters = new Stateful();

            //    // Populate the parameter list
            //    objParameters.Add("SessionID", strSessionID);
            //    objParameters.Add("Type", "");
            //    objParameters.Add("UpdateLastAction", false);
            //    objParameters.Add("System", "Website");

            //   // oResult = VirginLifecare.Client.SessionManager.RetrieveSessionData(objParameters);
            //}
            UserManager userMngr = new UserManager();
            //Response.Write(userMngr.UserId.ToString());

            var langCookie = Request.Cookies["SelectedLanguage"];
            int selctedLanguage = 1033;// english
            if (langCookie != null && langCookie.Value != null) {
                selctedLanguage = int.Parse(langCookie.Value);
            }
            userMngr.ContentLanguage = selctedLanguage;
            ContentManager contentMngr = new ContentManager();
            contentMngr.ContentLanguage = selctedLanguage;

            HttpCookie ecm = new HttpCookie("ecm");
            if (ecm == null) {
                // cookie was not created
            } else {
                ecm.Value = HttpContext.Current.Request.Cookies["ecm"].Value;
                ecm.Values.Set("DefaultLanguage", selctedLanguage.ToString());
                ecm.Values.Set("NavLanguage", selctedLanguage.ToString());
                ecm.Values.Set("SiteLanguage", selctedLanguage.ToString());
                ecm.Values.Set("LastValidLanguageID", selctedLanguage.ToString());
                ecm.Values.Set("UserCulture", selctedLanguage.ToString());
                HttpContext.Current.Response.SetCookie(ecm);
            };

            this.hdnWp1.Value = Request.Params["SessionID"];

        }

        public override void Error(string message) {
            throw new NotImplementedException();
        }

        public override void Notify(string message) {
            throw new NotImplementedException();
        }
    }
}