﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BioWidget.ascx.cs" Inherits="VirginLifeCare.Secure.Member.BioWidget" %>


<link href="http://localhost/VirginLifecare/styles/style_measurements.css" rel="stylesheet" type="text/css" />
<link href="http://localhost/VirginLifecare/Styles/style_buttons.css" type="text/css" rel="stylesheet" />
<link href="http://localhost/VirginLifecare/Styles/Incentives/style.css" rel="stylesheet" type="text/css" media="screen" />

<script type="text/javascript">
    $(document).ready(function () {
       
       
    });
</script>
 
<script src="http://localhost/VirginLifecare/javascript/lib/core.js" type="text/javascript" ></script>
<script src="http://localhost/VirginLifecare/javascript/lib/global.js" type="text/javascript"></script>

<script src="http://localhost/VirginLifecare/JavaScript/Incentives/CombinedTools.js" type="text/javascript"></script>
<script src="http://localhost/VirginLifecare/JavaScript/jquery/ui/jquery.ui.core.min.js" type="text/javascript"></script>
<script src="http://localhost/VirginLifecare/JavaScript/jquery/ui/jquery.ui.position.min.js" type="text/javascript"></script>
<script src="http://localhost/VirginLifecare/JavaScript/jquery/ui/jquery.ui.widget.min.js" type="text/javascript"></script>
<script src="http://localhost/VirginLifecare/JavaScript/jquery/ui/jquery.ui.mouse.min.js" type="text/javascript"></script>
<script src="http://localhost/VirginLifecare/JavaScript/jquery/ui/jquery.ui.draggable.min.js" type="text/javascript"></script>
<script src="http://localhost/VirginLifecare/JavaScript/jquery/ui/jquery.ui.resizable.min.js" type="text/javascript"></script>
<script src="http://localhost/VirginLifecare/JavaScript/jquery/ui/jquery.ui.dialog.min.js" type="text/javascript"></script>
<script src="http://localhost/VirginLifecare/JavaScript/jquery/ui/jquery.ui.tabs.min.js" type="text/javascript"></script>
<script src="http://localhost/VirginLifecare/JavaScript/ActivityJournalSlider.js" type="text/javascript"></script>
<script src="http://localhost/VirginLifecare/JavaScript/jquery/jquery.vhm.patches.js" type="text/javascript"></script>
<script src="http://localhost/VirginLifecare/JavaScript/lib/FusionCharts.js" type="text/javascript"></script>
<script src="http://localhost/VirginLifecare/JavaScript/biomeasurements/biomeasurements.js" type="text/javascript"></script>
<script src="http://localhost/VirginLifecare/JavaScript/biomeasurements/biomeasurementsslider.js" type="text/javascript"></script>
  
  <script type="text/javascript">
      $(document).ready(function () {
        

          VHMB.Bio.WSUrl = 'http://localhost/VirginLifecare/WebServices/WSVHMMeasurements.asmx/';
          VHMB.Bio.ChartURL = "http://localhost/VirginLifecare/Secure/FusionCharts/MSCombi2D.swf";
      });
</script>

<!--[if IE]>
<style type="text/css">          
     #bmControlArea{ border-top:1px solid gray;}
 </style>
<![endif]-->

<!--[if lt IE 8]>
<style type="text/css">
    #bmControlDropDowns{position:absolute;}
    .AMTarget{position:absolute;}
 </style>
<![endif]-->

<style type="text/css">          
     #divMeasureWithDevice{ width:230px;}
 </style>
 

<div class="BioMTabholder">  
    <div id="bioMtabs" class="bioMtabs">  
	    <ul>
            <li id="tabWeight"><a href="#BioMTab0"><span>Weight</span></a></li> 
            <li id="tabBMI"><a href="#BioMTab1"><span>BMI</span></a></li> 
            <li id="tabBodyFat"><a href="#BioMTab2"><span>Body Fat</span></a></li> 
            <li id="tabBloodPressure"><a href="#BioMTab3"><span>Blood Pressure</span></a></li> 
            <li id="tabCholesterol"><a href="#BioMTab4"><span>Cholesterol</span></a></li>
            <li id="tabGlucose"><a href="#BioMTab5"><span>Glucose</span></a></li>           
            <li id="tabA1C"><a href="#BioMTab6"><span>A1C</span></a></li>
            <li id="tabWaistCircumference"><a href="#BioMTab7"><span>Waist Circumference</span></a></li>
        </ul>            
        <div id="bmControlArea">
            <div id="bmControlDropDowns">
                <div id="viewByTimeLabel">View By:</div>
                <div id="viewByTimeSelect">
                    <select id="ddlBioTimeSapn" onchange="javascript:VHMB.Bio.SelectedTimeSpanChanged();">
                        <option value="0"  selected="selected">Last 12 Measurements</option>
                        <option value="1">Year</option>
                    </select>                
                </div>
                <div id="divCholesterolSelection">
                    <div id="viewCholesterolLabel">View:</div>
                    <div id="viewCholesterolSelect">
                        <select id="ddlBioCholesterol" onchange="javascript:VHMB.Bio.SelectedCholesterolChanged();">
                            <option value="4" selected="selected">Total Cholesterol</option>
                            <option value="5">HDL Cholesterol</option>
                            <option value="6">LDL Cholesterol</option>
                            <option value="7">Triglycerides</option>
                        </select>
                    </div>
                </div>
            </div>  
            <div style="clear:both;"></div>
        </div>
        <div id="BioMTab0"></div>             
        <div id="BioMTab1"></div>
        <div id="BioMTab2"></div> 
        <div id="BioMTab3"></div> 
        <div id="BioMTab4"></div>
        <div id="BioMTab5"></div>
        <div id="BioMTab6"></div>
        <div id="BioMTab7"></div> 
    </div>                   
    <div id="BioMPagerHolder" class="BioMPagerHolderHidden">
        <div id="BioMActivitypgr">
            <div id="BioMActivitypgrNext" class="BioMNoNextValue"></div>
            <div id="BioMActivitypgrPrev" class="BioMPrevValue"></div>
        </div>
    </div>
</div>