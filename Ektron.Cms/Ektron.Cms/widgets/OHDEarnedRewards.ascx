﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OHDEarnedRewards.ascx.cs" Inherits="Vhm.Widgets.POC.OHDEarnedRewards" %>
 
<style type="text/css">
    .rewardItem{ display: inline-block;height: 80px;width: 80px;}	
</style>
 
<div class="widget" style="width: 550px; height: 110px; text-align: center;">
    <div>
        <h3>You can earn rewards by participating in the <%= this.Title %></h3>
    </div>        
    <div>
    
        <% foreach (var reward in this.Rewards)
           {%>
                 <div class="rewardItem">
                     <img src="<%=reward.RewardImage %>"/>
                     <span><%=reward.Amount %></span>
                 </div>
         <%   }%> 
    </div>
</div>