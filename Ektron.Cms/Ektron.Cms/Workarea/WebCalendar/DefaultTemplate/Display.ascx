﻿<%@ Control Language="C#" AutoEventWireup="True" Inherits="Workarea_WebCalendar_DefaultTemplate_Display" Codebehind="Display.ascx.cs" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Label ToolTip="Title" ID="title" runat="server"></asp:Label>
<telerik:RadToolTip ID="description"
                    runat="server" 
                    Animation="None" 
                    HideEvent="Default" 
                    Position="BottomCenter" 
                    ShowEvent="OnMouseOver" 
                    ManualClose="false" 
                    Sticky="true" 
                    VisibleOnPageLoad="false" />
