﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Ektron.ContentDesignerWithValidator" Codebehind="ContentDesignerWithValidator.ascx.cs" %>
<%@ Register TagPrefix="ektron" Assembly="Ektron.ContentDesigner" Namespace="Ektron.ContentDesigner" %>
<ektron:ContentDesigner ID="ContentDesigner" runat="server" />
<asp:RegularExpressionValidator ID="ContentValidator" ControlToValidate="ContentDesigner" EnableClientScript="true" runat="server" />

