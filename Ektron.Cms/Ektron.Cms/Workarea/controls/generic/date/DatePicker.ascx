﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Ektron.Cms.Common.DatePicker" Codebehind="DatePicker.ascx.cs" %>
<span id="DatePickerContainer" class="DatePickerContainer" runat="server">
	<asp:Label ToolTip="Date" ID="lblDatePicker" CssClass="DatePicker_Label" runat="server" EnableViewState="false" />
	<asp:TextBox ToolTip="Date" CssClass="DatePicker_input" ID="tbDate" runat="server" />
</span>
