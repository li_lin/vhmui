The SEO control analyzes the pages of your Web site for W3C compliance, and reveals what information Google has about the page, Alexa rankings, image alt text, keyword density, and metadata. The tool indicates how (and if) you�ve set these values. 

To learn more, read the following Ektron Knowledge Base article: http://dev.ektron.com/kb_article.aspx?id=22196.