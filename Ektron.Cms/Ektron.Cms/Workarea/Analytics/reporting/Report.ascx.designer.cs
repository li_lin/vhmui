﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------



public partial class Analytics_reporting_Report {
    
    /// <summary>
    /// lblNoRecords control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label lblNoRecords;
    
    /// <summary>
    /// ltrlNoRecords control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Literal ltrlNoRecords;
    
    /// <summary>
    /// pnlData control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Panel pnlData;
    
    /// <summary>
    /// UpdatePanel1 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.UpdatePanel UpdatePanel1;
    
    /// <summary>
    /// litCssTweaks control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Literal litCssTweaks;
    
    /// <summary>
    /// ErrorPanel control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Panel ErrorPanel;
    
    /// <summary>
    /// litErrorMessage control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Literal litErrorMessage;
    
    /// <summary>
    /// lnkBreadcrumbReport control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlAnchor lnkBreadcrumbReport;
    
    /// <summary>
    /// htmBreadcrumbSeparatorFor control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlGenericControl htmBreadcrumbSeparatorFor;
    
    /// <summary>
    /// lblBreadcrumbFor control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label lblBreadcrumbFor;
    
    /// <summary>
    /// lnkBreadcrumbFor control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlAnchor lnkBreadcrumbFor;
    
    /// <summary>
    /// htmBreadcrumbSeparatorAnd control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlGenericControl htmBreadcrumbSeparatorAnd;
    
    /// <summary>
    /// lblBreadcrumbAnd control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label lblBreadcrumbAnd;
    
    /// <summary>
    /// lnkBreadcrumbAnd control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlAnchor lnkBreadcrumbAnd;
    
    /// <summary>
    /// htmBreadcrumbSeparatorAlso control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlGenericControl htmBreadcrumbSeparatorAlso;
    
    /// <summary>
    /// lblBreadcrumbAlso control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label lblBreadcrumbAlso;
    
    /// <summary>
    /// htmReportTitle control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlGenericControl htmReportTitle;
    
    /// <summary>
    /// AnalyticsReportDateRangeDisplay control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlGenericControl AnalyticsReportDateRangeDisplay;
    
    /// <summary>
    /// htmReportSubtitle control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlGenericControl htmReportSubtitle;
    
    /// <summary>
    /// htmReportSummary control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlGenericControl htmReportSummary;
    
    /// <summary>
    /// gvDataTable control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.GridView gvDataTable;
    
    /// <summary>
    /// AnalyticsDetail control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Analytics_reporting_Detail AnalyticsDetail;
    
    /// <summary>
    /// AnalyticsPieChart control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::controls_reports_PercentPieChart AnalyticsPieChart;
    
    /// <summary>
    /// dvVisualSelect control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvVisualSelect;
    
    /// <summary>
    /// ltrVisualizationView control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Literal ltrVisualizationView;
    
    /// <summary>
    /// drpVisualization control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.DropDownList drpVisualization;
    
    /// <summary>
    /// GrdDynamic control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.GridView GrdDynamic;
}
