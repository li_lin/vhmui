﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Analytics_controls_ProviderSelector" Codebehind="ProviderSelector.ascx.cs" %>
<span class="ProviderSelectorContainer" ID="ProviderSelectorContainer" runat="server">
    <asp:Label ID="lblProviderSelector" runat="server" EnableViewState="false" />
    <asp:DropDownList ToolTip="Select Analytics Provider from the Drop Down Menu" ID="ProviderSelectorList" CssClass="ProviderSelectorList" runat="server" OnSelectedIndexChanged="ProviderSelectorList_SelectionChanged" />
</span>
