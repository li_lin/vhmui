﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Ektron.Cms.Framework.UI.Controls.EktronUI.Templates.ButtonSet" Codebehind="ButtonSet.ascx.cs" %>
<ektronUI:JavaScriptBlock ID="uxInitializationScript" runat="server" ExecutionMode="OnEktronReady">
    <ScriptTemplate>
        Ektron.Controls.EktronUI.ButtonSet.init('#<%# this.ControlContainer.ClientID %>');
    </ScriptTemplate>
</ektronUI:JavaScriptBlock>