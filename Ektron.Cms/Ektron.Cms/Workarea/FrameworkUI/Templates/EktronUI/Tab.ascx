﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Ektron.Cms.Framework.UI.Controls.EktronUI.Templates.Tab" Codebehind="Tab.ascx.cs" %>
<h3 id="aspAccordionHeader" runat="server"><a href="#<%# this.TabClientID %>" title="<%# this.TabText %>" <%# this.TabOnClick %>><%# this.TabText%></a></h3>
<div>
    <asp:PlaceHolder ID="aspContents" runat="server"></asp:PlaceHolder>
</div>
