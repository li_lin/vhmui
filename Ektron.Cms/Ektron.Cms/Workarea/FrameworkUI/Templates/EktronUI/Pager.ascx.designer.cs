﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ektron.Cms.Framework.UI.Controls.EktronUI.Templates {
    
    
    public partial class Pager {
        
        /// <summary>
        /// uxPrevious control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Ektron.Cms.Framework.UI.Controls.EktronUI.Button uxPrevious;
        
        /// <summary>
        /// aspPages control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder aspPages;
        
        /// <summary>
        /// uxNext control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Ektron.Cms.Framework.UI.Controls.EktronUI.Button uxNext;
        
        /// <summary>
        /// uxIE7Rules control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Ektron.Cms.Framework.UI.Controls.EktronUI.CssBlock uxIE7Rules;
        
        /// <summary>
        /// aspCurrentPage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField aspCurrentPage;
    }
}
