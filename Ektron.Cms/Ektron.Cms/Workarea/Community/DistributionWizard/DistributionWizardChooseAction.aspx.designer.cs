﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------



public partial class Community_DistributionWizard_DistributionWizardChooseAction {
    
    /// <summary>
    /// divDistributionOptions control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlGenericControl divDistributionOptions;
    
    /// <summary>
    /// lbCopy control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.LinkButton lbCopy;
    
    /// <summary>
    /// lbReplace control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.LinkButton lbReplace;
    
    /// <summary>
    /// divRedistributionOptions control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlGenericControl divRedistributionOptions;
    
    /// <summary>
    /// divRedistritbutionDetails control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlGenericControl divRedistritbutionDetails;
    
    /// <summary>
    /// ltrRedistributionDetails control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Literal ltrRedistributionDetails;
    
    /// <summary>
    /// lbRedistribute control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.LinkButton lbRedistribute;
    
    /// <summary>
    /// btnClose control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Button btnClose;
    
    /// <summary>
    /// Master property.
    /// </summary>
    /// <remarks>
    /// Auto-generated property.
    /// </remarks>
    public new Community_DistributionWizard_DistributionWizard Master {
        get {
            return ((Community_DistributionWizard_DistributionWizard)(base.Master));
        }
    }
}
