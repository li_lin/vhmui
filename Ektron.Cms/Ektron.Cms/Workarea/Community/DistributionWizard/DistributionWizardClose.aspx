<%@ Page Language="C#" MasterPageFile="DistributionWizard.master"  AutoEventWireup="true" Inherits="Community_DistributionWizard_DistributionWizardClose" Codebehind="DistributionWizardClose.aspx.cs" %>

<asp:Content ContentPlaceHolderID="cphDistributionWizardContent" runat="server" ID="contentClose">
    <div>
        <asp:Literal ID="ltrPageContent" runat="server"></asp:Literal>
    </div>
</asp:Content>
