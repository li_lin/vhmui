﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SponsorSettingsHelper
/// </summary>
public class SponsorSettingsHelper {
    private Ektron.Cms.Framework.Content.ContentManager _contentManager;

    private string _sponsorName;

    public SponsorSettingsHelper(string sponsorName)
    {
        _sponsorName = sponsorName;
        _contentManager = new Ektron.Cms.Framework.Content.ContentManager(Ektron.Cms.Framework.ApiAccessMode.Admin);
    }

    public SponsorSettings GetSettings() {
        SponsorSettings settings = new SponsorSettings();
        Ektron.Cms.Content.ContentCriteria contentCriteria = new Ektron.Cms.Content.ContentCriteria();
        contentCriteria.AddFilter(Ektron.Cms.Common.ContentProperty.Path, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, GetPath());
        contentCriteria.AddFilter(Ektron.Cms.Common.ContentProperty.Title, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, GetTitle());
        
        List<Ektron.Cms.ContentData> contentList = _contentManager.GetList(contentCriteria);
        if (contentList.Count > 0) {
            settings = Convert(contentList[0]);
        }

        return settings;
    }

    private string GetPath() {
        return String.Format("VHM/Sponsors/{0}/Content/", GetSponsorName());
    }

    private string GetTitle() {
        return "Settings";
    }

    private string GetSponsorName()
    {
        return _sponsorName;// "Lockheed Martin";// "Virgin HealthMiles";
    }

    private SponsorSettings Convert(Ektron.Cms.ContentData content) {
        string color = String.Empty;
        string logo = String.Empty;

        System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
        xmlDoc.LoadXml(content.Html);

        color = GetNodeValue(xmlDoc, "/settings/color");
        logo = GetNodeValue(xmlDoc, "/settings/logo/img/@src");

        return new SponsorSettings() { Color = color, Logo = logo };
    }

    private string GetNodeValue(System.Xml.XmlDocument xmlDoc, string path) {
        string ret = String.Empty;

        if (xmlDoc.SelectSingleNode(path) != null) {
            ret = xmlDoc.SelectSingleNode(path).InnerXml;
        }

        return ret;
    }


}
