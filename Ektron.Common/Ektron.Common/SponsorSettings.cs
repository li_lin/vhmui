﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SponsorSettings
/// </summary>
public class SponsorSettings {
    public string Logo {
        get;
        set;
    }

    public string Color {
        get;
        set;
    }
}
