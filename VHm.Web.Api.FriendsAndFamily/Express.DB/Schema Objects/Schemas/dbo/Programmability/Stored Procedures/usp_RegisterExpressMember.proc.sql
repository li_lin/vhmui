﻿/*****************************************************************
**	Company:	Virgin Lifecare
**	Project:	CRM
**	Procedure:	usp_RegisterExressMember
**	Synopsis:	Registers an Express Member
**
**	Change History:
**	13-Sep-2013	VB	Initial Version
*****************************************************************/
CREATE PROCEDURE [dbo].[usp_RegisterExpressMember]
(   
@Title			VARCHAR(20)		= NULL,
@Forename		VARCHAR(50)		= NULL,
@Surname		VARCHAR(50)		= NULL,
@Gender			CHAR(1)			= NULL,
@DateOfBirth		DATETIME		= NULL,
@EmailAddress		VARCHAR(255)		= NULL,
@HomeTelephone		VARCHAR(20)		= NULL,
@MobileTelephone	VARCHAR(20)		= NULL,
@PreferredPhoneType	CHAR(3)			= NULL,	 /* 'WRK','MOB','FAX','HOM' */
@Pin			BINARY(50)		= NULL,
@SponsorID		BIGINT			= NULL,
@MemberReference	VARCHAR(100)		= NULL
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE		@MemberID		BIGINT,
			@CurrencyCode		CHAR(3),
			@Now			DATETIME,
			@Today			DATETIME,
			@Preferred		BIT,
			@IdentityNumber		VARCHAR(12),
			@EligibilityID		BIGINT,
			@EReasonCode		CHAR(3),
			@SponsorTypeCode	CHAR(3),
			@VerificationType	CHAR(4),
			@SponsorshipID		BIGINT,
			@NewRegistry		BIT,
			@ErrorMessage		VARCHAR(MAX),
			@Demo			BIT,
			@PrimarySponsor		BIGINT,
			@RewardsAnniversary	DATETIME

	SET		@Preferred = 0			

	/* Set CurrencyCode with default for United States Dollar */
	SET		@CurrencyCode = 'USD'
	SET		@Now = GETDATE()
	SET		@Today = CONVERT(DATETIME, CONVERT(VARCHAR(20), @Now, 112))

	/* Get the type of sponsor */
	SET		@SponsorTypeCode = [dbo].[udf_GetSponsorType](@SponsorID)

	/* Determine verification type */
	SELECT		@VerificationType = EnrollmentVerificationType
	FROM		[crm].[SponsorProfile] WITH(NOLOCK)
	WHERE		SponsorID = @SponsorID
	

	/* Create IdentityNumber for new Member */
	SET	@IdentityNumber = [dbo].[udf_CreateIdentity](@Forename, @Surname)
	SET	@NewRegistry = 1
	
	/* Create/Update Person Details */
	EXEC [dbo].[isp_SavePerson]	@MemberID	OUTPUT,
					@IdentityNumber = @IdentityNumber,
					@Title = @Title,
					@Initials = NULL,
					@Forename = @Forename,
					@Surname = @Surname,
					@EmailAddress = @EmailAddress,
					@UpdateExisting	= 0
	
	EXEC [dbo].[isp_SaveMember]	@EntityID = @MemberID,
					@Gender = @Gender,
					@DateOfBirth = @DateOfBirth
	
	/* Create/Update Member Login */
	IF	@Pin IS NOT NULL
	BEGIN
		EXEC [dbo].[usp_SaveLogin]	@MemberID = @MemberID,
						@Pin = @Pin
	END	
	
	/* Create/Update Member Phone */
	IF NOT @HomeTelephone IS NULL
	BEGIN
		/* Begin Home Phone */
		IF @PreferredPhoneType = 'HOM'
		BEGIN
			SET @Preferred = 1
		END

		EXEC [dbo].[usp_SaveTelephone]	@EntityID = @MemberID,
						@TelephoneType = 'HOM',
						@TelephoneNumber = @HomeTelephone,
						@Preferred = @Preferred

		/* Change preferred back to 0 for further processing */
		SET	@Preferred = 0	

	END
	IF NOT @MobileTelephone IS NULL
	BEGIN
		/* Begin Fax Phone */
		IF @PreferredPhoneType = 'MOB'
		BEGIN
			SET	@Preferred = 1
		END

		EXEC [dbo].[usp_SaveTelephone]	@EntityID = @MemberID,
						@TelephoneType = 'MOB',
						@TelephoneNumber = @MobileTelephone,
						@Preferred = @Preferred

		/*  Change preferred back to 0 for further processing*/
		SET	@Preferred = 0
	END
	
	SELECT		@Demo = sprop.[Demo]
	FROM		[dbo].[SponsorProperties] sprop		WITH(NOLOCK)
	WHERE		sprop.[SponsorID] = @SponsorID

	/* Create Sponsorship between the member and the sponsor */
	INSERT INTO	[dbo].[Sponsorship]
			(
			[MemberID],
			[SponsorID],
			[StatusCode],
			[EligibilityID],
			[Demo]
			)
	SELECT		@MemberID,
			@SponsorID,
			'ACTIVATED',
			@EligibilityID,
			@Demo
	WHERE		NOT EXISTS	(	SELECT		1
						FROM		[dbo].[Sponsorship] sp			WITH(NOLOCK)
						WHERE		sp.[MemberID] = @MemberID
						AND		sp.[SponsorID] = @SponsorID
						AND		sp.[StatusCode] IN ('ACTIVATED','PENDCANCEL')
					)	

	SET		@SponsorshipID = SCOPE_IDENTITY()

	/* Register Member for Drawings */
	IF	@SponsorTypeCode = 'EMP'
	BEGIN
		DECLARE		@DrawingID	BIGINT
		
		SELECT TOP 1	@DrawingID = dr.DrawingID 
		FROM		[dbo].[Drawings] dr			WITH(NOLOCK)
		WHERE		dr.[EntityID] = @SponsorID
		AND		dr.[Live] = 1 
		AND		(dr.[StartDate] <= @NOW) 
		AND		(ISNULL(dr.[EndDate], DATEADD(yy,1, dr.[StartDate])) > @NOW)
		
		IF		@DrawingID IS NOT NULL
		AND		NOT EXISTS	(	SELECT		md.MemberID
							FROM		dbo.Member_Drawing md		WITH(NOLOCK)
							WHERE		md.MemberID = @MemberID
							AND		md.DrawingID = @DrawingID
						)
		BEGIN
			EXEC [dbo].[usp_SaveMemberDrawing]	@MemberID = @MemberID,
								@DrawingID = @DrawingID,
								@RegistrationDate = @Now
		END		
	END
				
	/*Get Details to generate specific email. */
	/* Queue email for New Enrollment */
	IF		@NewRegistry =  1
	BEGIN
			EXEC [dbo].[usp_CreateQueuedMail]	@Reason = 'RGP',
								@EntityID = @MemberID,
								@Email = 1
	END
	/* Queue email for Re-Enrollment */
	IF		@NewRegistry = 0
	AND		(	SELECT		COUNT(1)
				FROM		[dbo].[udf_HMSponsors](@MemberID)
			) = 1
	BEGIN
			EXEC [dbo].[usp_CreateQueuedMail]	@Reason = 'WBE',
								@EntityID = @MemberID,
								@Email = 1
	END
	
	--/* Trigger Newbie Badge when registering for HealthMiles program */
	--IF		(	SELECT		b.LIVE
	--			FROM		dbo.Badges b		WITH(NOLOCK)
	--			INNER JOIN	dbo.SponsorBadges sb	WITH(NOLOCK)
	--				ON	b.BadgeID = sb.BadgeID
	--			WHERE		b.BadgeTriggerCode = 'NEWBI'
	--			AND		sb.SponsorID = @SponsorID
	--		) = 1
	--BEGIN
	--		EXEC VLC_REWARDS.dbo.usp_TriggerMemberReward	@MemberID = @MemberID,
	--								@RewardCode = 'NEWBI',
	--								@TriggerDate = @Now,
	--								@SurpressResult = 1
									
	--END
	
	--Get the primary Sponsor
	SELECT		@PrimarySponsor = hms.[SponsorID]
	FROM		[dbo].[udf_HMSponsors](@MemberID) hms
	WHERE		hms.[JoiningOrder] = 1
	
		
	--Set the members AnniversaryDate
	IF @SponsorID = @PrimarySponsor --Only do this for the primary sponsor
	BEGIN
		
		SELECT		@RewardsAnniversary = rs.[RewardsAnniversary]
		FROM		[dbo].[RewardsSettings] rs		WITH(NOLOCK)
		WHERE		rs.[EntityID] = @PrimarySponsor
		AND		rs.[CurrentRewardsProgramType] = 'LVL'
		AND		rs.[RewardsAnniversaryType] = 'SPN'
		
		
		SET		@RewardsAnniversary = dbo.udf_MakeDate(YEAR(@Now), MONTH(@RewardsAnniversary), DAY(@RewardsAnniversary))
		IF @RewardsAnniversary > @Today
		BEGIN
			SET		@RewardsAnniversary = DATEADD(yyyy, -1, @RewardsAnniversary)
		END
		
		UPDATE		[dbo].[Member]
		SET		[RewardsAniversary] = ISNULL(@RewardsAnniversary, @Today)
		WHERE		[Member].[MemberID] = @MemberID
	END	
	
	/* Return Member Details to the Caller. */
	SELECT		@MemberID		AS [MemberID],
			@IdentityNumber		AS [IdentityNumber],
			@SponsorshipID		AS [SponsorshipID],
			@PrimarySponsor		AS [PrimarySponsorID],
			@Forename		AS [Forename],
			@Surname		AS [Surname]
			
	SET NOCOUNT OFF
END