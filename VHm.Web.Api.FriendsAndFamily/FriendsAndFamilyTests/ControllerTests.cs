﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.Hosting;
using System.Web.Script.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using VHm.Web.Api.FriendsAndFamily.Controllers;
using VHm.Web.Api.FriendsAndFamily.Models;

namespace FriendsAndFamilyTests
{
    [TestClass]
    public class ControllerTests
    {
        [TestMethod]
        //[ExpectedException(typeof(UriFormatException))]
        public void Invitation_EmptyTicket_ReturnsNotFound()
        {
            //var stubHttpClient = new Mock<HttpClient>();
            //stubHttpClient.Setup(x => x.BaseAddress).Returns(new Uri("some_uri"));

            var mockDataContext = Mock.Of<FriendsAndFamilyFDataContext>();

            var invitationController = new InvitationsController(mockDataContext);

            var token = string.Empty;

            invitationController.Request = new HttpRequestMessage();
            invitationController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

            var response = invitationController.Get(token);

            Assert.AreEqual(response.StatusCode, HttpStatusCode.NotFound);

        }

        [TestMethod]
        public void Invitation_NonGuidTicket_ReturnsBadRequestWithEmptyMessage()
        {
            var mockDataContext = Mock.Of<FriendsAndFamilyFDataContext>();

            var invitationController = new InvitationsController(mockDataContext);

            var token = "argh"; // this should be a guid, but is just a junk string

            invitationController.Request = new HttpRequestMessage();
            invitationController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

            //var stubHttpClient = new Mock<HttpClient>();
            //stubHttpClient.Setup(x => x.BaseAddress).Returns(new Uri(invitationController));

            var response = invitationController.Get(token);

            Assert.AreEqual(response.StatusCode, HttpStatusCode.BadRequest);
            Assert.IsNull(response.RequestMessage.Content);

        }

        [TestMethod]
        public void Invitation_ValidGuidTicketButNoActiveSession_ReturnsBadRequestWithEmptyMessage()
        {
            var mockDataContext = Mock.Of<FriendsAndFamilyFDataContext>();

            var invitationController = new InvitationsController(mockDataContext);

            Mock.Get(mockDataContext)
                .Setup(ctx => ctx.GetInvitations(0, InviteStatus.All))
                .Returns(new List<FriendsAndFamily>());

            const string TOKEN = "4549EB5B-DB95-4606-BD45-39C25F2CC572";

            invitationController.Request = new HttpRequestMessage();
            invitationController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

            var response = invitationController.Get(TOKEN);

            Assert.AreEqual(response.StatusCode, HttpStatusCode.BadRequest);
            Assert.IsNull(response.RequestMessage.Content);

        }

        [TestMethod]
        public void Invitation_ValidMemberWithNoInvitations_ReturnsBadRequestWithEmptyMessage()
        {
            var mockDataContext = Mock.Of<FriendsAndFamilyFDataContext>();

            var invitationController = new InvitationsController(mockDataContext);

            Mock.Get(mockDataContext)
                .Setup(ctx => ctx.GetInvitations(1, InviteStatus.All))
                .Returns(new List<FriendsAndFamily>());

            const string TOKEN = "4549EB5B-DB95-4606-BD45-39C25F2CC572";

            invitationController.Request = new HttpRequestMessage();
            invitationController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

            var response = invitationController.Get(TOKEN);

            Assert.AreEqual(response.StatusCode, HttpStatusCode.BadRequest);
            Assert.IsNull(response.RequestMessage.Content);

        }

        [TestMethod]
        public void Invitation_ValidMemberWithOneInvitation_ReturnsOkWithListOfFamilyAndFriends()
        {
            var mockDataContext = Mock.Of<IFriendsAndFamilyFDataContext>();

            var testPerson = new FriendsAndFamily
                {
                    CreatedDate = DateTime.Now,
                    DateSent = DateTime.Now,
                    Deleted = false,
                    EmailSent = true,
                    FriendsAndFamilyID = 0,
                    InviteStatus = (int) InviteStatus.Pending,
                    InviteToken = new Guid("4549EB5B-DB95-4606-BD45-39C25F2CC572"),
                    MemberID = 33,
                    RelationID = null,
                    RelationEmail = "test@ttt.com",
                    UpdatedDate = DateTime.Now
                };
            var returnList = new List<FriendsAndFamily> {testPerson};

            Mock.Get(mockDataContext)
                .Setup(ctx => ctx.GetInvitations(It.IsAny<long>(), InviteStatus.All))
                .Returns(returnList);
            Mock.Get(mockDataContext)
                .Setup(ctx => ctx.GetMemberIdBySession(It.IsAny<Guid>()))
                .Returns(1);

            var invitationController = new InvitationsController(mockDataContext);

            const string TOKEN = "4549EB5B-DB95-4606-BD45-39C25F2CC572";

            invitationController.Request = new HttpRequestMessage();
            var config = new HttpConfiguration();
            config.Formatters.Clear();
            config.Formatters.Add(new JsonMediaTypeFormatter());
            invitationController.Configuration = config;
            invitationController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, config);

            var response = invitationController.Get(TOKEN);

            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);

            var serializer = new JavaScriptSerializer();

            var sList = serializer.Serialize(returnList);
            var result = response.Content.ReadAsAsync<string>().Result;
            Assert.AreEqual(result, sList);
        }

        [TestMethod]
        public void Enrollment_FirstNameEmpty_ReturnsBadRequest()
        {
            var mockDataContext = Mock.Of<IFriendsAndFamilyFDataContext>();

            var term = new TermsDTO
                {
                    Accept = true,
                    EntityID = 1,
                    TermsCode = "AAA"
                };

            var terms = new TermsDTO[1];
            terms[0] = term;
            var testDTO = new EnrollmentDTO
                {
                    FirstName = string.Empty,
                    LastName = "Brown",
                    Email = "Test@test.com",
                    ConfirmEmail = "Test@test.com",
                    Phone = "123-456-7890",
                    Password = "a1111111",
                    ConfirmPassword = "a1111111",
                    Terms = terms
                };

            var enrollmentController = new EnrollmentController(mockDataContext);

            enrollmentController.Request = new HttpRequestMessage();
            var config = new HttpConfiguration();
            config.Formatters.Clear();
            config.Formatters.Add(new JsonMediaTypeFormatter());
            enrollmentController.Configuration = config;
            enrollmentController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, config);

            var response = enrollmentController.Post(testDTO);
            Assert.AreEqual(response.StatusCode, HttpStatusCode.BadRequest);

        }

        [TestMethod]
        public void Enrollment_LastNameEmpty_ReturnsBadRequest()
        {
            var mockDataContext = Mock.Of<IFriendsAndFamilyFDataContext>();

            var term = new TermsDTO {
                Accept = true,
                EntityID = 1,
                TermsCode = "AAA"
            };

            var terms = new TermsDTO[1];
            terms[0] = term;
            var testDTO = new EnrollmentDTO {
                FirstName = "Vincent",
                LastName = string.Empty,
                Email = "Test@test.com",
                ConfirmEmail = "Test@test.com",
                Phone = "123-456-7890",
                Password = "a1111111",
                ConfirmPassword = "a1111111",
                Terms = terms
            };

            var enrollmentController = new EnrollmentController(mockDataContext) {Request = new HttpRequestMessage()};

            var config = new HttpConfiguration();
            config.Formatters.Clear();
            config.Formatters.Add(new JsonMediaTypeFormatter());
            enrollmentController.Configuration = config;
            enrollmentController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, config);

            var response = enrollmentController.Post(testDTO);
            Assert.AreEqual(response.StatusCode, HttpStatusCode.BadRequest);

        }

        [TestMethod]
        public void Enrollment_EmailNameEmpty_ReturnsBadRequest()
        {
            var mockDataContext = Mock.Of<IFriendsAndFamilyFDataContext>();

            var term = new TermsDTO {
                Accept = true,
                EntityID = 1,
                TermsCode = "AAA"
            };

            var terms = new TermsDTO[1];
            terms[0] = term;
            var testDTO = new EnrollmentDTO {
                FirstName = "Vincent",
                LastName = "Brown",
                Email = string.Empty,
                ConfirmEmail = "Test@test.com",
                Phone = "123-456-7890",
                Password = "a1111111",
                ConfirmPassword = "a1111111",
                Terms = terms
            };

            var enrollmentController = new EnrollmentController(mockDataContext) {Request = new HttpRequestMessage()};

            var config = new HttpConfiguration();
            config.Formatters.Clear();
            config.Formatters.Add(new JsonMediaTypeFormatter());
            enrollmentController.Configuration = config;
            enrollmentController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, config);

            var response = enrollmentController.Post(testDTO);
            Assert.AreEqual(response.StatusCode, HttpStatusCode.BadRequest);

        }

        [TestMethod]
        public void Enrollment_EmailDoesNotMatchConfirmEmail_ReturnsBadRequest()
        {
            var mockDataContext = Mock.Of<IFriendsAndFamilyFDataContext>();

            var term = new TermsDTO {
                Accept = true,
                EntityID = 1,
                TermsCode = "AAA"
            };

            var terms = new TermsDTO[1];
            terms[0] = term;
            var testDTO = new EnrollmentDTO {
                FirstName = "Vincent",
                LastName = "Brown",
                Email = "MyEMail@test.com",
                ConfirmEmail = "YourEMail@test.com",
                Phone = "123-456-7890",
                Password = "a1111111",
                ConfirmPassword = "a1111111",
                Terms = terms
            };

            var enrollmentController = new EnrollmentController(mockDataContext) {Request = new HttpRequestMessage()};

            var config = new HttpConfiguration();
            config.Formatters.Clear();
            config.Formatters.Add(new JsonMediaTypeFormatter());
            enrollmentController.Configuration = config;
            enrollmentController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, config);

            var response = enrollmentController.Post(testDTO);
            Assert.AreEqual(response.StatusCode, HttpStatusCode.BadRequest);

        }

        [TestMethod]
        public void Enrollment_DTODoesNotHaveAtLeastOnePhoneNumber_ReturnsBadRequest()
        {
            var mockDataContext = Mock.Of<IFriendsAndFamilyFDataContext>();

            var term = new TermsDTO {
                Accept = true,
                EntityID = 1,
                TermsCode = "AAA"
            };

            var terms = new TermsDTO[1];
            terms[0] = term;
            var testDTO = new EnrollmentDTO {
                FirstName = "Vincent",
                LastName = "Brown",
                Email = "MyEMail@test.com",
                ConfirmEmail = "MyEMail@test.com",
                Phone = string.Empty,
                Password = "a1111111",
                ConfirmPassword = "a1111111",
                Terms = terms
            };

            var enrollmentController = new EnrollmentController(mockDataContext) {Request = new HttpRequestMessage()};

            var config = new HttpConfiguration();
            config.Formatters.Clear();
            config.Formatters.Add(new JsonMediaTypeFormatter());
            enrollmentController.Configuration = config;
            enrollmentController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, config);

            var response = enrollmentController.Post(testDTO);
            Assert.AreEqual(response.StatusCode, HttpStatusCode.BadRequest);

        }

        [TestMethod]
        public void Enrollment_PasswordIsEmpty_ReturnsBadRequest()
        {
            var mockDataContext = Mock.Of<IFriendsAndFamilyFDataContext>();

            var term = new TermsDTO {
                Accept = true,
                EntityID = 1,
                TermsCode = "AAA"
            };

            var terms = new TermsDTO[1];
            terms[0] = term;
            var testDTO = new EnrollmentDTO {
                FirstName = "Vincent",
                LastName = "Brown",
                Email = "MyEMail@test.com",
                ConfirmEmail = "MyEMail@test.com",
                Phone = "401-111-2222",
                Password = string.Empty,
                ConfirmPassword = "a1111111",
                Terms = terms
            };

            var enrollmentController = new EnrollmentController(mockDataContext) {Request = new HttpRequestMessage()};

            var config = new HttpConfiguration();
            config.Formatters.Clear();
            config.Formatters.Add(new JsonMediaTypeFormatter());
            enrollmentController.Configuration = config;
            enrollmentController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, config);

            var response = enrollmentController.Post(testDTO);
            Assert.AreEqual(response.StatusCode, HttpStatusCode.BadRequest);

        }

        [TestMethod]
        public void Enrollment_PasswordDoesNotMatchConfirmPassword_ReturnsBadRequest()
        {
            var mockDataContext = Mock.Of<IFriendsAndFamilyFDataContext>();

            var term = new TermsDTO {
                Accept = true,
                EntityID = 1,
                TermsCode = "AAA"
            };

            var terms = new TermsDTO[1];
            terms[0] = term;
            var testDTO = new EnrollmentDTO {
                FirstName = "Vincent",
                LastName = "Brown",
                Email = "MyEMail@test.com",
                ConfirmEmail = "MyEMail@test.com",
                Phone = "401-111-2222",
                Password = "a1111111",
                ConfirmPassword = "b2222222",
                Terms = terms
            };

            var enrollmentController = new EnrollmentController(mockDataContext);

            enrollmentController.Request = new HttpRequestMessage();
            var config = new HttpConfiguration();
            config.Formatters.Clear();
            config.Formatters.Add(new JsonMediaTypeFormatter());
            enrollmentController.Configuration = config;
            enrollmentController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, config);

            var response = enrollmentController.Post(testDTO);
            Assert.AreEqual(response.StatusCode, HttpStatusCode.BadRequest);

        }

        [TestMethod]
        public void Enrollment_TermsNotAccepted_ReturnsBadRequest()
        {
            var mockDataContext = Mock.Of<IFriendsAndFamilyFDataContext>();

            var term = new TermsDTO {
                Accept = false,
                EntityID = 1,
                TermsCode = "AAA"
            };

            var terms = new TermsDTO[1];
            terms[0] = term;
            var testDTO = new EnrollmentDTO {
                FirstName = "Vincent",
                LastName = "Brown",
                Email = "MyEMail@test.com",
                ConfirmEmail = "MyEMail@test.com",
                Phone = "401-111-2222",
                Password = "a1111111",
                ConfirmPassword = "a1111111",
                Terms = terms
            };

            var enrollmentController = new EnrollmentController(mockDataContext);

            enrollmentController.Request = new HttpRequestMessage();
            var config = new HttpConfiguration();
            config.Formatters.Clear();
            config.Formatters.Add(new JsonMediaTypeFormatter());
            enrollmentController.Configuration = config;
            enrollmentController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, config);

            var response = enrollmentController.Post(testDTO);
            Assert.AreEqual(response.StatusCode, HttpStatusCode.BadRequest);

        }
    }
}