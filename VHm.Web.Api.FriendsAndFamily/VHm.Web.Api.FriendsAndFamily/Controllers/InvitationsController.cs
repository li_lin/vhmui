﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Script.Serialization;
using VHm.Web.Api.FriendsAndFamily.Models;
using log4net;

namespace VHm.Web.Api.FriendsAndFamily.Controllers
{
                   
    public class InvitationsController : BaseApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(EmailController));

        public InvitationsController(IFriendsAndFamilyFDataContext context)
            : base(context)
        {
           
        }

        public HttpResponseMessage Get(string ticket, InviteStatus status = InviteStatus.All)
        {
            try
            {
                if (string.IsNullOrEmpty(ticket))
                {
                    Log.Info("Empty sessionId");
                    return Request.CreateResponse(HttpStatusCode.NotFound, string.Empty);
                }

                Guid sessionId;
                if (!Guid.TryParse(ticket, out sessionId))
                {
                    Log.Info("Bad sessionId: " + ticket);
                    return Request.CreateResponse(HttpStatusCode.BadRequest, string.Empty);
                }

                var memberId = GetMemberID(sessionId);
                if (memberId <= 0)
                {
                    Log.Info("MemberId not found for sessionId: " + ticket);
                    return Request.CreateResponse(HttpStatusCode.BadRequest, string.Empty);
                }

                if (context.IsExpressMember(memberId))
                {
                    Log.Info("MemberId: " + memberId + " is an Express member.");
                    return Request.CreateResponse(HttpStatusCode.BadRequest, string.Empty); 
                }

                var invitations = context.GetInvitations(memberId, status);
                if (invitations == null)
                {
                    Log.Info("No invitations found for memberId: " + memberId);
                    return Request.CreateResponse(HttpStatusCode.OK, string.Empty);
                }
                object data = new JavaScriptSerializer().Serialize(invitations);

                var response = Request.CreateResponse(HttpStatusCode.OK, data);
                return response;
            }
            catch (System.Data.EntityException entEx)
            {
                var response = Request.CreateResponse(HttpStatusCode.InternalServerError, string.Empty);
                Log.Error(entEx);

                return response;    
            }
            catch (Exception ex)
            {
                var response = Request.CreateResponse(HttpStatusCode.InternalServerError, string.Empty);
                Log.Error(ex);
                return response;
            }
            
        }

        public HttpResponseMessage Delete(string ticket, long inviteId)
        {
            try
            {
                context.RemoveInvite(inviteId);
                return Request.CreateResponse(HttpStatusCode.OK, string.Empty);
            }
            catch (InvalidOperationException ioEx)
            {
                var response = Request.CreateResponse(HttpStatusCode.InternalServerError, "There was a problem saving data");
                Log.Error(ioEx);
                return response;
            }
            catch (Exception ex)
            {
                var response = Request.CreateResponse(HttpStatusCode.InternalServerError, "Unknown error");
                Log.Error(ex);
                return response;
            }
        }
    }
}
