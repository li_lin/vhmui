﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using VHm.Web.Api.FriendsAndFamily.Models;
using log4net;

namespace VHm.Web.Api.FriendsAndFamily.Controllers
{
    public class BaseApiController : ApiController
    {
        protected readonly IFriendsAndFamilyFDataContext context;

        protected BaseApiController(IFriendsAndFamilyFDataContext ctx)
        {
            context = ctx;
        }
        private static readonly ILog Log = LogManager.GetLogger(typeof(BaseApiController));
        /// <summary>
        /// This method accepts an encrypted auth token which must be encrypted using the same machine key and
        /// encryption algorithm (SHA1) as this application. See the web.config machine key node for current details.
        /// Once decrypted, the session ID is used to look up the currently logged in user's memberId. A valid memberId
        /// is only passed back if we can decrypt the cookie and get a valid session Id. If an error occurs, we log the
        /// error and send back -1 for the memberId and let the calling function handle it from there. 
        /// </summary>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        protected long GetMemberID(Guid sessionId)
        {
            long memberId = 0;
            // ensure we have a non-null session cookie
            if (sessionId != Guid.Empty)
            {
                try
                {
                    memberId = context.GetMemberIdBySession(sessionId);
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                }
                
            }
            return memberId;
        }

        protected List<MemberSponsors> GetMemberSponsorship(Guid sessionId)
        {
            var sponsorship = new List<MemberSponsors>();

            // ensure we have a non-null session cookie
            if (sessionId != Guid.Empty)
            {
                try
                {
                    long memberId = context.GetMemberIdBySession(sessionId);
                    sponsorship = context.GetMemberSponsors(memberId);
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                }

            }
            return sponsorship;
        }
    }
}
