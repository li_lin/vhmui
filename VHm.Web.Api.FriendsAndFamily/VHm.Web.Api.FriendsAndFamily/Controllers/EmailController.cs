﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Net.Mime;
using System.Web.Script.Serialization;
using VHm.Web.Api.FriendsAndFamily.Models;
using log4net;

namespace VHm.Web.Api.FriendsAndFamily.Controllers
{
    public class EmailController : BaseApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(EmailController));

        private readonly string _smtpUser = ConfigurationManager.AppSettings["SMTPUser"];  // Replace with your SMTP username. 
        private readonly string _smtpPassword = ConfigurationManager.AppSettings["SMTPKey"];  // Replace with your SMTP password.
        private readonly string _smtpHost = ConfigurationManager.AppSettings["SMTPHost"];
        private readonly string _smtpFrom = ConfigurationManager.AppSettings["SMTPFromAddress"];
        private readonly string _smtpPort = ConfigurationManager.AppSettings["SMTPPort"];        

        public EmailController(IFriendsAndFamilyFDataContext context)
            : base(context)
        {

        }

        public HttpResponseMessage Post(string ticket, string to)
        {
            int port;
            if (!int.TryParse(_smtpPort, out port))
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Invalid SMTP Port"); 
            }
            
            Log.Info("Using email config: HOST: " + _smtpHost + ", PORT: " + _smtpPort + ", USER: " + _smtpUser + ", PASSWORD: " + _smtpPassword + ", FROM: " + _smtpFrom);
            
            Guid sessionId;
            if (!Guid.TryParse(ticket, out sessionId))
            {
                Log.Info("Bad sessionId: " + ticket);
                return Request.CreateResponse(HttpStatusCode.NotFound, string.Empty);
            }

            var memberId = GetMemberID(sessionId);
            if (memberId <= 0)
            {
                Log.Error("MemberId not found sending email for ticket: " + ticket);                 
                return Request.CreateResponse(HttpStatusCode.Unauthorized, string.Empty);
            }

            var existingMemberId = context.GetActiveMemberIdByEmail(to);
            if (existingMemberId > 0)
            {
                Log.Info("Member: " + existingMemberId + " already exists. Cannot send duplicate invite.");                 
                return Request.CreateResponse(HttpStatusCode.BadRequest, "This member already exists.");
            }

            int maxInvites;
            int.TryParse(ConfigurationManager.AppSettings["MaxInvites"], out maxInvites);

            var invitationsRemaining = context.GetInvitesRemaining(memberId, maxInvites);
            if (invitationsRemaining <= 0)
            {
                Log.Info("Member: " + existingMemberId + " has exceeded the maximum number of allowed invites of" + maxInvites + ".");
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Maximum number of invites exceeded.");
            }

            using (var client = new SmtpClient(_smtpHost, port))
            {
                // Create a network credential with your SMTP user name and password.
                client.Credentials = new NetworkCredential(_smtpUser, _smtpPassword);

                // Use SSL when accessing Amazon SES. The SMTP session will begin on an unencrypted connection, and then 
                // the client will issue a STARTTLS command to upgrade to an encrypted connection using SSL.
                client.EnableSsl = true;
                
                // Send the email. 
                try
                {

                    var person = context.GetPersonById(memberId);

                    var token = Guid.NewGuid();
                   
                    if (person == null)
                    {
                        Log.Error("Person not found. MemberID: " + memberId);
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, "Email not sent");
                    }
                    
                    var template = context.GetExpressEmailTemplate();
                    var eMessage = context.GetExpressEmailBody();

                    if (eMessage == null)
                    {
                        Log.Error("Email body not setup in CRM");
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, "Email not sent");
                    }

                    string bodyText = eMessage.EMsgPlain;
                    bodyText = bodyText.Replace("{{TOKEN}}", token.ToString());
                    bodyText = bodyText.Replace("{{FIRSTNAME}}", person.Forename);
                    bodyText = bodyText.Replace("{{LASTNAME}}", person.Surname);

                    string bodyHtml = eMessage.EMsgHTML;
                    bodyHtml = bodyHtml.Replace("{{TOKEN}}", token.ToString());
                    bodyHtml = bodyHtml.Replace("{{FIRSTNAME}}", person.Forename);
                    bodyHtml = bodyHtml.Replace("{{LASTNAME}}", person.Surname);

                    if (template == null)
                    {
                        Log.Error("Email template is not setup.");
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, "Email not sent");
                    }

                    string subject = eMessage.ESubject;
                    subject = person.Forename + " " + person.Surname + " " + subject;

                    bodyText = template.PlainHeader + bodyText + template.PlainFooter;
                    bodyHtml = template.HTMLHeader + bodyHtml + template.HtMLFooter;

                    var message = new MailMessage
                        {
                            From = new MailAddress(_smtpFrom),
                            Body = bodyText,
                            IsBodyHtml = true,
                            Subject = subject
                        };
                    message.To.Add(new MailAddress(to));

                    // Add the alternate body to the message to allow for Html content
                    var mimeType = new ContentType("text/html");

                    AlternateView alternate = AlternateView.CreateAlternateViewFromString(bodyHtml, mimeType);
                    message.AlternateViews.Add(alternate);

                    client.Send(message);

                    var results = context.SaveFriendsAndFamily(memberId, to, token);

                    if (results > 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "Email Sent!");
                    }
                    Log.Error("Unable to save FriendsAndFamily. MemberId: " + memberId + ", To: " + to + ", Token: " + token);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Email not sent");
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
                }
            }
        }

        public HttpResponseMessage Put(string ticket, long inviteId)
        {            
            if (string.IsNullOrEmpty(ticket))
            {
                Log.Info("Bad ticket: " + ticket);
                return Request.CreateResponse(HttpStatusCode.Unauthorized, string.Empty);
            }
            if (inviteId <= 0)
            {
                Log.Info("invitedId <= 0 " + inviteId);
                return Request.CreateResponse(HttpStatusCode.NotFound, string.Empty);
            }

            int port;
            if (!int.TryParse(_smtpPort, out port))
            {
                Log.Info("Invalid SMTP Port " + port);
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid SMTP Port");
            }

            Guid sessionId;
            if (!Guid.TryParse(ticket, out sessionId))
            {
                Log.Info("Bad sessionId: " + ticket);
                return Request.CreateResponse(HttpStatusCode.BadRequest, string.Empty);
            }

            var memberId = GetMemberID(sessionId);
            if (memberId < 0)
            {
                Log.Info("can't find memberId by sessionId " + sessionId);
                return Request.CreateResponse(HttpStatusCode.Unauthorized, string.Empty);
            }

            if (context.IsExpressMember(memberId))
            {
                Log.Info("MemberId: " + memberId + " is an Express member.");
                return Request.CreateResponse(HttpStatusCode.BadRequest, string.Empty);
            }

            using (var client = new SmtpClient(_smtpHost, port))
            {
                // Create a network credential with your SMTP user name and password.
                client.Credentials = new NetworkCredential(_smtpUser, _smtpPassword);

                // Use SSL when accessing Amazon SES. The SMTP session will begin on an unencrypted connection, and then 
                // the client will issue a STARTTLS command to upgrade to an encrypted connection using SSL.
                client.EnableSsl = true;

                // Send the email. 
                try
                {
                    var invite = context.GetInviteByFriendsAndFamilyId(inviteId);
                    if (invite.InviteToken == Guid.Empty)
                    {
                        return Request.CreateResponse(HttpStatusCode.NotFound, string.Empty);
                    }

                    var person = context.GetPersonById(memberId);

                    if (person == null)
                    {
                        return Request.CreateResponse(HttpStatusCode.NotFound, string.Empty);
                    }

                    var eMessage = context.GetExpressEmailBody();

                    if (eMessage == null)
                    {
                        Log.Error("Email body not setup in CRM");
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, "Email not sent");
                    }

                    string bodyText = eMessage.EMsgPlain;

                    string bodyHtml = eMessage.EMsgHTML;

                    bodyText = bodyText.Replace("{{TOKEN}}", invite.InviteToken.ToString());
                    bodyText = bodyText.Replace("{{FIRSTNAME}}", person.Forename);
                    bodyText = bodyText.Replace("{{LASTNAME}}", person.Forename);

                    bodyHtml = bodyHtml.Replace("{{TOKEN}}", invite.InviteToken.ToString());
                    bodyHtml = bodyHtml.Replace("{{FIRSTNAME}}", person.Forename);
                    bodyHtml = bodyHtml.Replace("{{LASTNAME}}", person.Forename);

                    var template = context.GetExpressEmailTemplate();
                    if (template == null)
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, "Email not sent");
                    }

                    string subject = eMessage.ESubject;
                    subject = person.Forename + " " + person.Surname + subject;

                    bodyText = template.PlainHeader + bodyText + template.PlainFooter;
                    bodyHtml = template.HTMLHeader + bodyHtml + template.HtMLFooter;

                    var message = new MailMessage {
                        From = new MailAddress(_smtpFrom),
                        Body = bodyText,
                        IsBodyHtml = true,
                        Subject = subject
                    };
                    message.To.Add(new MailAddress(invite.RelationEmail));

                    // Add the alternate body to the message to allow for Html content
                    var mimeType = new ContentType("text/html");

                    AlternateView alternate = AlternateView.CreateAlternateViewFromString(bodyHtml, mimeType);
                    message.AlternateViews.Add(alternate);

                    client.Send(message);

                    var results = context.UpdateFriendsAndFamilyInviteSent(invite.InviteToken);

                    if (results > 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "Email Sent!");
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Email not sent");
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
                }
            }
        }
    }
}
