﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using System.Web.Security;
using VHm.Web.Api.FriendsAndFamily.Models;
using log4net;

namespace VHm.Web.Api.FriendsAndFamily.Controllers
{
    public class EnrollmentController : BaseApiController
    {

        private static readonly ILog Log = LogManager.GetLogger(typeof(EmailController));

        public EnrollmentController(IFriendsAndFamilyFDataContext context)
            : base(context)
        {
           
        }

        private long GetExpressSponsorIdFromCookie()
        {
            long sponsorId = 0;
            var cookieName = ConfigurationManager.AppSettings["ExpressEnrollmentSponsorCookieName"];
            if (string.IsNullOrEmpty(cookieName))
            {
                Log.Error("ExpressEnrollmentSponsorCookieName app setting missing from web.config");
                return sponsorId;
            }

            var sponsorCookie = System.Web.HttpContext.Current.Request.Cookies[cookieName];

            if (sponsorCookie == null)
            {
                Log.Warn("Express Sponsor cookie not found");
                return sponsorId;
            }

            var ticket = FormsAuthentication.Decrypt(sponsorCookie.Value);
            if (ticket != null)
            {
                long.TryParse(ticket.UserData, out sponsorId);
            }
            return sponsorId;
        }

        /// <summary>
        /// If the invite token is empty, checks to see if the person has selected an Express
        /// Sponsor based on the value in the ExpressEnrollment cookie
        /// </summary>
        /// <param name="id">The invite token</param>
        /// <returns></returns>
        public HttpResponseMessage Get(string id = "")
        {
            try
            {
                var terms = context.GetTerms();
                if (string.IsNullOrEmpty(id))
                {

                    // This person is coming from the Join page after selecting an Express sponsor. Check the ExpressSponsor cookie.
                    long expressSponsorId = GetExpressSponsorIdFromCookie();
                    if (context.IsExpressSponsor(expressSponsorId))
                    {
                        //CGW 1/9/13 Get Filtervalues for Enrollemnt page
                        var sponsorFilter = context.GetSponsorFilters(expressSponsorId);

                        return Request.CreateResponse(HttpStatusCode.OK, new
                        {
                            Email = string.Empty,
                            IsValid = true,
                            Terms = terms,
                            SponsorFilters = sponsorFilter                         
                        });
                    }
                }
                else
                {
                    // This person was invited by another member, so check that the invite token is valid
                    var person = context.GetInvitationByToken(id);

                    if (terms.Count == 0)
                    {
                        Log.Error("Membership Terms not set up");
                    }
                    if (person != null)
                    {
                        //CGW 1/22/14: Added SponsorFilters for F & F invites which dont use Filter Values
                        return Request.CreateResponse(HttpStatusCode.OK, new
                        {
                            Email = person.RelationEmail,
                            IsValid = person.InviteStatus == (int) InviteStatus.Pending,
                            Terms = terms,
                            SponsorFilters = Enumerable.Empty<string>()
                        });
                    }
                }
                return Request.CreateResponse(HttpStatusCode.Unauthorized, string.Empty);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, false);
            }
        }

        [Serializable]
        private class EmailDomainSetting
        {

            public long SponsorId { get; set; }
            public string EmailDomain { get; set; }
        }

        /// <summary>
        /// Creates a new Enrollmentment
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public HttpResponseMessage Post([FromBody] EnrollmentDTO dto)
        {
            if (dto == null)
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized, false);
            }

            try
            {
                #region Validation

                bool valid = !string.IsNullOrEmpty(dto.FirstName);
                valid = valid && !string.IsNullOrEmpty(dto.LastName);
                valid = valid && !string.IsNullOrEmpty(dto.Email);
                valid = valid && dto.Email.Equals(dto.ConfirmEmail);
                valid = valid && !string.IsNullOrEmpty(dto.Phone);
                valid = valid && !string.IsNullOrEmpty(dto.Password);
                valid = valid && dto.Password.Equals(dto.ConfirmPassword);
                valid = valid && dto.Terms.All(t => t.Accept);

                //CGW 1/15/2014 - Verify that the Filter Value has been selected
                valid = valid && dto.SponsorFilters.All(t => t.FilterValueID !=null && t.FilterValueID > 0);

                if (!valid)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Validation Failed");
                }

                long expressSponsorId = GetExpressSponsorIdFromCookie();

                string configuredDomains = ConfigurationManager.AppSettings["SponsorInviteSettings"];
                var inviteSettings = new JavaScriptSerializer().Deserialize<List<EmailDomainSetting>>(configuredDomains);
                string sendDomain = dto.Email.Substring(dto.Email.IndexOf('@')).Replace("@", string.Empty);

                // check if the email domain from the 'to' address matches any of the configured domains (note, if a sponsor is configured with a '*', 
                // they allow anything)
                bool configured = inviteSettings.Any(i => i.SponsorId == expressSponsorId);
                bool foundDomain = inviteSettings.Any(s =>
                        s.SponsorId == expressSponsorId &&
                        (s.EmailDomain.Equals(sendDomain) || s.EmailDomain.Equals("*")));

                if (configured && !foundDomain)
                {
                    Log.Info("SendTo: " + dto.Email + " Email domain is not in the configured list.");
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Email domain is not in the configured list.");
                }
                #endregion

                // Check for existing email
                if (context.GetActiveMemberIdByEmail(dto.Email) > 0)
                {
                    Log.Info("Duplicate enrollment email: " + dto.Email);
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Duplicate Email: " + dto.Email); 
                }

                // good to go
                var memberId = context.EnrollExpressMember(dto, expressSponsorId);
                if (memberId <= 0)
                {
                    Log.Error("Enrollment failed: " + dto.Email);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Unable to complete registration");
                }

                var cookieName = ConfigurationManager.AppSettings["ExpressEnrollmentSponsorCookieName"];
                if (!string.IsNullOrEmpty(cookieName))
                {
                    var ctx = System.Web.HttpContext.Current;
                    var sponsorCookie = ctx.Request.Cookies[cookieName];
                    if (sponsorCookie != null)
                    {
                        // expire the sponsor cookie now that we're done with it. It should not be used more than once.
                        sponsorCookie.Expires = DateTime.Now.AddDays(-1);
                    }
                }
                var message = Request.CreateResponse(HttpStatusCode.OK, memberId);

                return message;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, false);
            }
        }
    }
}
