//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VHm.Web.Api.FriendsAndFamily.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class EmailTemplate
    {
        public EmailTemplate()
        {
            this.EMessages = new HashSet<EMessage>();
        }
    
        public long EmailTemplateID { get; set; }
        public Nullable<long> SponsorID { get; set; }
        public string TemplateName { get; set; }
        public string HTMLHeader { get; set; }
        public string HtMLFooter { get; set; }
        public string PlainHeader { get; set; }
        public string PlainFooter { get; set; }
        public System.DateTime CreatedDate { get; set; }
    
        public virtual ICollection<EMessage> EMessages { get; set; }
    }
}
