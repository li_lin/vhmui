﻿using System;

namespace VHm.Web.Api.FriendsAndFamily.Models
{
    public class EnrollmentDTO
    {
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public int DOB_Month { get; set; }
        public int DOB_Day { get; set; }
        public int DOB_Year { get; set; }
        public string Email { get; set; }
        public string ConfirmEmail { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public Guid Token { get; set; }
        public TermsDTO[] Terms { get; set; }
        public SponsorFilterDTO[] SponsorFilters { get; set; }
    }
}