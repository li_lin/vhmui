//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VHm.Web.Api.FriendsAndFamily.Models
{
    using System;
    
    public partial class RegisterExpressMember_Result
    {
        public Nullable<long> MemberID { get; set; }
        public string IdentityNumber { get; set; }
        public Nullable<long> SponsorshipID { get; set; }
        public Nullable<long> PrimarySponsorID { get; set; }
        public string Forename { get; set; }
        public string Surname { get; set; }
    }
}
