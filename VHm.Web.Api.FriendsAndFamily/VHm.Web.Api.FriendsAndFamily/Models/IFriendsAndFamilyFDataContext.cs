﻿using System;
using System.Collections.Generic;

namespace VHm.Web.Api.FriendsAndFamily.Models
{
    public interface IFriendsAndFamilyFDataContext
    {
        int GetInvitesRemaining(long memberId, int maxInvites);
        object GetInvitations(long memberId, InviteStatus status);
        int SaveFriendsAndFamily(long memberId, string to, Guid token);
        int UpdateFriendsAndFamilyInviteSent(Guid token);
        FriendsAndFamily GetInviteByFriendsAndFamilyId(long inviteId);
        int RemoveInvite(long inviteId);
        long GetMemberIdBySession(Guid sessionId);
        long GetActiveMemberIdByEmail(string email);
        Person GetPersonById(long memberId);
        FriendsAndFamily GetInvitationByToken(string token);
        long EnrollExpressMember(EnrollmentDTO member, long expressSponsorId = 0);
        List<Terms_Get_Result> GetTerms();
        EmailTemplate GetExpressEmailTemplate();
        EMessage GetExpressEmailBody();
        bool IsExpressMember(long memberId);
        bool IsExpressSponsor(long sponsorId);
        List<MemberSponsors> GetMemberSponsors(long memberId);
        object GetSponsorFilters(long sponsorId);
    }
}
