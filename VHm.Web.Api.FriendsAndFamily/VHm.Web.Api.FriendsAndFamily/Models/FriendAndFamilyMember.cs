﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VHm.Web.Api.FriendsAndFamily.Models
{
    [Serializable]
    public class FriendAndFamilyMember
    {
        public long MemberId { get; set; }
        public long RelationId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int InviteStatus { get; set; }
        public string ProfilePicture { get; set; }

    }
}