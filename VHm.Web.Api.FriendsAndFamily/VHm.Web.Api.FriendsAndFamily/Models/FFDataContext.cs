﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Transactions;
using System.Web.Security;
using log4net;

namespace VHm.Web.Api.FriendsAndFamily.Models
{
    public class FriendsAndFamilyFDataContext : IFriendsAndFamilyFDataContext
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(FriendsAndFamilyFDataContext));

        private static VLC_CRMEntities _crm;
        private static VHM_SOCIALEntities _social;
        private static VLC_RewardsEntities _rewards;

        private static VLC_CRMEntities GetCRMModel()
        {
            return _crm ?? (_crm = new VLC_CRMEntities());
        }

        private static VHM_SOCIALEntities GetSocialModel()
        {
            return _social ?? (_social = new VHM_SOCIALEntities());
        }

        private static VLC_RewardsEntities GetRewardsModel()
        {
            return _rewards ?? (_rewards = new VLC_RewardsEntities());
        }

        public virtual object GetInvitations(long memberId, InviteStatus status)
        {
            try
            {
                var model = GetCRMModel();

                var person = (from p in model.People
                              where p.PersonID == memberId
                              select new { PersonId = p.PersonID, p.EmailAddress }).SingleOrDefault();

                if (person == null)
                {
                    return null;
                }

                var invitations = (from invites in model.FriendsAndFamilies
                                   where invites.MemberID == memberId
                                         && !invites.Deleted
                                         && (
                                            invites.InviteStatus == (int)InviteStatus.Accepted ||
                                            invites.InviteStatus == (int)InviteStatus.Pending ||
                                            invites.InviteStatus == (int)InviteStatus.PendRevoked
                                            )
                                   join people in model.People on invites.MemberID equals people.PersonID
                                   join profile in model.MemberProfiles on invites.RelationID equals profile.MemberID
                                       into relations
                                   from profile in relations.DefaultIfEmpty()
                                   select new
                                       {
                                           InviteId = invites.FriendsAndFamilyID,
                                           Name = profile.DisplayName ?? invites.RelationEmail,
                                           invites.MemberID,
                                           invites.RelationID,
                                           MemberEmail = people.EmailAddress,
                                           invites.RelationEmail,
                                           profile.ProfilePicture,
                                           invites.InviteStatus
                                       }).ToList();

                if (status != InviteStatus.All)
                {
                    invitations = invitations.Where(i => ((InviteStatus)i.InviteStatus) == status).ToList();
                }

                var invitationList = invitations.Select(invite => new
                    {
                        invite.InviteId,
                        invite.Name,
                        invite.RelationID,
                        invite.RelationEmail,
                        ProfilePicture = invite.ProfilePicture != null && invite.ProfilePicture.Length > 0
                                             ? new Guid(invite.ProfilePicture).ToString().ToLower()
                                             : string.Empty,
                        invite.InviteStatus

                    }).ToList();

                return new
                    {
                        MemberId = person.PersonId,
                        MemberEmail = person.EmailAddress,
                        CDNPath = ConfigurationManager.AppSettings["CDNImagePath"],
                        Invitations = invitationList
                    };
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }
        }

        public bool IsExpressMember(long memberId)
        {
            var model = GetCRMModel();

            long expressSponsorID = long.Parse(ConfigurationManager.AppSettings["ExpressSponsorID"]);

            var isExpress = model.Sponsorships.Any(sp => sp.MemberID == memberId
                                                         &&
                                                         (sp.StatusCode == "ACTIVATED" || sp.StatusCode == "PENDCANCEL")
                                                         && sp.SponsorID == expressSponsorID);
            return isExpress;
        }

        public List<MemberSponsors> GetMemberSponsors(long memberId)
        {
            var model = GetCRMModel();
            return model.GetMemberSponsors(memberId, null).ToList();
        }

        public bool IsExpressSponsor(long sponsorId)
        {
            var model = GetRewardsModel();
            return model.RewardsStructures.Any(s => s.SponsorID == sponsorId && s.Current && s.ProgramTypeCode == "EXP" && !s.Deleted);
        }

        public int SaveFriendsAndFamily(long memberId, string to, Guid token)
        {
            var today = DateTime.Now;
            var crm = GetCRMModel();

            crm.FriendsAndFamilies.Add(
                new FriendsAndFamily
                    {
                        MemberID = memberId,
                        RelationID = null,
                        EmailSent = true,
                        RelationEmail = to,
                        InviteStatus = (int)InviteStatus.Pending,
                        CreatedDate = today,
                        UpdatedDate = today,
                        Deleted = false,
                        DateSent = today,
                        InviteToken = token
                    });
            return crm.SaveChanges();
        }

        public int UpdateFriendsAndFamilyInviteSent(Guid token)
        {
            var crm = GetCRMModel();
            var invitee = crm.FriendsAndFamilies.SingleOrDefault(f => f.InviteToken == token);
            if (invitee != null)
            {
                invitee.DateSent = DateTime.Now;
                return crm.SaveChanges();
            }
            return 0;
        }

        public FriendsAndFamily GetInviteByFriendsAndFamilyId(long inviteId)
        {
            var crm = GetCRMModel();
            var invite = (from invites in crm.FriendsAndFamilies
                          where invites.FriendsAndFamilyID == inviteId
                          select invites).SingleOrDefault();
            return invite;
        }

        public int RemoveInvite(long inviteId)
        {
            int updated = 0;

            using (var tscope = new TransactionScope(TransactionScopeOption.RequiresNew))
            {
                try
                {
                    var model = GetCRMModel();
                    var invitation = (from person in model.FriendsAndFamilies
                                      where person.FriendsAndFamilyID == inviteId
                                      select person).SingleOrDefault();
                    if (invitation != null)
                    {

                        var relatedMemberId = invitation.RelationID.GetValueOrDefault();

                        invitation.InviteStatus = relatedMemberId == 0 ? (int)InviteStatus.Revoked : (int)InviteStatus.PendRevoked;    // updates the FriendsAndFamily table

                        if (relatedMemberId > 0)
                        {
                            // send the cancel request
                            const string CANCEL_TYPE_CODE = "SPONSOR";
                            long expressSponsorID = long.Parse(ConfigurationManager.AppSettings["ExpressSponsorID"]);
                            var cancelDate = DateTime.Now;
                            var sponsorCancelSettings = model.SponsorCancelSettings.SingleOrDefault(s => s.SponsorID == expressSponsorID && s.CancelTypeCode == CANCEL_TYPE_CODE);
                            int cancelDays = 30;
                            if (sponsorCancelSettings != null)
                            {
                                cancelDays = sponsorCancelSettings.PendCancelDays.GetValueOrDefault();
                            }
                            var effectiveDate = cancelDate.AddDays(cancelDays + 1);

                            var sponsorshipId = invitation.RelationSponsorshipID.GetValueOrDefault();
                            if (sponsorshipId > 0)
                            {
                                const bool RETURN_RESULTS = false;
                                const bool SEND_CANCEL_EMAIL = true;
                                const bool CANCEL_SOCIAL = true;
                                const bool DELETE_FROM_CRP_CHALLENGE = true;

                                model.SubmitCancelRequest(sponsorshipId, null, CANCEL_TYPE_CODE, cancelDate,
                                                          effectiveDate.Date, null, null, (int)invitation.MemberID, SEND_CANCEL_EMAIL, RETURN_RESULTS,
                                                          CANCEL_SOCIAL, DELETE_FROM_CRP_CHALLENGE);
                            }

                        }

                        updated = model.SaveChanges();
                    }

                    // Complete the transaction
                    tscope.Complete();
                }
                catch (Exception ex)
                {
                    Log.Error("Error removing invited member:" + ex);
                }
            }
            return updated;
        }

        public long GetMemberIdBySession(Guid sessionId)
        {
            long memberId = -1;

            try
            {
                var crm = GetCRMModel();
                var session = (from s in crm.Session_Allocate
                               where s.Session_ID == sessionId
                                     && s.Expired == false
                               select s).SingleOrDefault();

                if (session != null)
                {
                    memberId = session.Member_ID;
                    Log.Info("Found memberId: " + memberId);
                }
                else
                {
                    Log.Info("MemberId not found for sessionId: " + sessionId.ToString());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return memberId;
        }

        public long GetActiveMemberIdByEmail(string email)
        {
            var model = GetCRMModel();

            var member = (from person in model.People
                          join sp in model.Sponsorships on person.PersonID equals sp.MemberID
                          where person.EmailAddress == email
                                && (sp.StatusCode == "ACTIVATED" || sp.StatusCode == "PENDCANCEL")
                          select person).FirstOrDefault();
            return member != null ? member.PersonID : -1;
        }

        public Person GetPersonById(long memberId)
        {
            var crm = GetCRMModel();
            if (memberId > 0)
            {
                return crm.People.SingleOrDefault(p => p.PersonID == memberId);
            }
            return null;
        }

        public int GetInvitesRemaining(long memberId, int maxInvites)
        {
            var crm = GetCRMModel();
            if (memberId > 0)
            {
                var allInvites = crm.FriendsAndFamilies.Where(ff => ff.MemberID == memberId).ToList();
                var activeInvites = allInvites.Count(ff => ff.InviteStatus == (int)InviteStatus.Accepted ||
                                                           ff.InviteStatus == (int)InviteStatus.PendRevoked ||
                                                           ff.InviteStatus == (int)InviteStatus.Pending);
                return maxInvites - activeInvites;
            }
            return 0;
        }

        public FriendsAndFamily GetInvitationByToken(string token)
        {
            var model = GetCRMModel();
            FriendsAndFamily thisPerson = null;
            if (!string.IsNullOrEmpty(token))
            {

                var expirationPeriod = int.Parse(ConfigurationManager.AppSettings["InvitationExpirationPeriodInDays"]);

                var inviteToken = new Guid(token);
                var invitation = (from invitations in model.FriendsAndFamilies
                                  where invitations.InviteToken == inviteToken
                                        && invitations.DateSent.HasValue
                                  select invitations).SingleOrDefault();

                if (invitation != null &&
                    invitation.DateSent.GetValueOrDefault().AddDays(expirationPeriod) >= DateTime.Now && invitation.InviteStatus == (int)InviteStatus.Pending)
                {
                    thisPerson = invitation;
                }
            }

            return thisPerson;
        }

        public EmailTemplate GetExpressEmailTemplate()
        {
            var crm = GetCRMModel();
            string expressTemplate = ConfigurationManager.AppSettings["ExpressTemplateName"];
            return crm.EmailTemplates.SingleOrDefault(t => t.TemplateName == expressTemplate);
        }

        public EMessage GetExpressEmailBody()
        {
            var crm = GetCRMModel();
            const string EREASON_FRIENDS_AND_FAMILY = "FAF";
            var messages = crm.EMessages.Where(m => m.EReasonCode == EREASON_FRIENDS_AND_FAMILY && !m.Deleted &&
                                                     (m.EMsgToDate == null || m.EMsgToDate <= DateTime.Now)).ToList().OrderBy(m => m.EMsgFromDate);

            return messages.FirstOrDefault();
        }

        public long EnrollExpressMember(EnrollmentDTO member, long expressSponsorId = 0)
        {
            var crm = GetCRMModel();

            var dob = new DateTime(member.DOB_Year, member.DOB_Month, member.DOB_Day);
            expressSponsorId = expressSponsorId > 0 ? expressSponsorId : long.Parse(ConfigurationManager.AppSettings["ExpressSponsorID"]);
            var pin = Encrypt(member.Password);
            string memberReference = string.Empty;

            // Ensure we don't have this email already in the system
            var existingId = GetActiveMemberIdByEmail(member.Email);
            if (existingId > 0)
            {
                Log.Error("Duplicate email found during enrollment: " + member.Email);
                return 0;
            }

            // all terms must be accepted
            if (member.Terms.Any(t => !t.Accept))
            {
                Log.Error("Terms not accepted");
                return 0;
            }

            /* 
             * As part of registration, the following operations must complete successfully:
             *      1. Registration: the member information is saved by calling RegisterMember(). 
             *      2. Accept Terms: the member must accept all membership terms to enroll
             *      3. Update the invitation status
             *      4. Add member to the Sponsor community group
             *      5. Trigger the Newbie reward
             */
            long memberId = 0;
            using (var tscope = new TransactionScope(TransactionScopeOption.RequiresNew))
            {
                try
                {
                    var invitee = crm.FriendsAndFamilies.SingleOrDefault(m => m.InviteToken == member.Token);

                    // check if InviteToken is already used by anyone
                    if (invitee != null && invitee.InviteStatus == Convert.ToInt32(InviteStatus.Accepted))
                    {
                        Log.Error(string.Format("InviteToken {0} is already used", invitee.InviteToken));
                        return 0;
                    }

                    #region Register Member

                    var result =
                        crm.RegisterExpressMember(member.Title, member.FirstName, member.LastName, member.Gender, dob,
                                                  member.Email, member.Phone, "HOM", pin, expressSponsorId,
                                                  memberReference)
                           .SingleOrDefault();
                    #endregion

                    #region Acknowlege Terms

                    if (result != null)
                    {
                        const string TERMS_SOURCE = "Website";
                        foreach (var terms in member.Terms)
                        {
                            crm.AcknowledgeMemberTerms(terms.TermsCode, terms.Accept, TERMS_SOURCE, result.MemberID,
                                                       null,
                                                       null,
                                                       terms.EntityID, null);
                        }
                    }

                    #endregion

                    #region Save filter values

                    //CGW 1/10/14 - Save filter values if they are on the enrollment page
                    if (member.SponsorFilters != null)
                    {
                        foreach (var sponsorFilter in member.SponsorFilters)
                            crm.SaveSponsorshipFilterValue(result.SponsorshipID, null, null, null, sponsorFilter.FilterValueID);
                    }

                    #endregion

                    if (result != null && result.MemberID.GetValueOrDefault() > 0)
                    {
                        #region Update Invitation Status

                        bool updated = true;
                        if (member.Token != Guid.Empty)
                        {
                            updated = UpdateInviteeStatus(invitee, result.MemberID.GetValueOrDefault(),
                                result.SponsorshipID.GetValueOrDefault(), InviteStatus.Accepted);
                        }

                        #endregion

                        #region Add member to the Sponsor community group

                        // identify the Community Group, and add the member to the one that is identified as the community group
                        // for the express sponsor.
                        const string GROUP_TYPE_COMMUNITY = "COM";
                        var social = GetSocialModel();
                        var groupTypes = social.GetGroupTypes();

                        var distributors = crm.GetSponsorDistributors(expressSponsorId, true, false, false, true);

                        // identify the community distributor
                        var communityDistributor = distributors.SingleOrDefault(d => d.CommunityDistributor);

                        if (communityDistributor != null)
                        {
                            var groups = social.GetCommunityGroups(communityDistributor.DistributorID);
                            if (groups != null)
                            {
                                var groupType = groupTypes.SingleOrDefault(gt => gt.GroupTypeCode == GROUP_TYPE_COMMUNITY);
                                if (groupType != null)
                                {
                                    var communityGroupTypeId = groupType.GroupTypeID;
                                    var communityGroup = groups.SingleOrDefault(g => g.GroupTypeID == communityGroupTypeId);

                                    // add the member to the community group
                                    if (communityGroup != null)
                                    {
                                        social.AddMemberToGroup(communityGroup.GroupID,
                                                                 result.MemberID.GetValueOrDefault(), false);
                                    }
                                    else
                                    {
                                        Log.Error("Social group not set up for express sponsorID: " + expressSponsorId);
                                    }
                                }
                            }

                        }

                        // get the distributor groupID for the open community. There should be only one record.

                        //var groupId = (from distSponsor in crm.DistributorSponsors
                        //               join dist in crm.Distributors on distSponsor.DistributorID equals
                        //                   dist.DistributorID
                        //               where
                        //                   dist.CommunityDistributorType == "OPN" &&
                        //                   distSponsor.SponsorID == expressSponsorID
                        //               select distSponsor.DistributorID).SingleOrDefault();

                        #endregion

                        #region Trigger Newbie Badge
                        if (updated)
                        {
                            const string NEWBIE = "NEWBIE";

                            var systemBadges = crm.GetSponsorSystemBadges(expressSponsorId).ToList();
                            var badgeId = (from badge in systemBadges
                                           where badge.BadgeName.ToUpper() == NEWBIE
                                           select badge.BadgeID).FirstOrDefault();
                            if (badgeId > 0)
                            {
                                var rewards = GetRewardsModel();
                                rewards.TriggerMemberReward(result.MemberID, NEWBIE, DateTime.Now, 0, false, true, null,
                                                            null,
                                                            null, null);

                            }
                            else
                            {
                                Log.Info("Newbie badge not setup for sponsorId: " + expressSponsorId);
                            }
                        }
                        else
                        {
                            Log.Error("Couldn't update FriendsandFamilies invite status for memberId: " + result.MemberID.GetValueOrDefault());
                        }
                        #endregion

                        memberId = result.MemberID.GetValueOrDefault();
                    }
                    else
                    {
                        Log.Error("RegisterExpressMember failed");
                    }

                    // Complete the transaction
                    tscope.Complete();
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    return 0;
                }
            }
            return memberId;
        }

        public List<Terms_Get_Result> GetTerms()
        {
            long expressSponsorID = long.Parse(ConfigurationManager.AppSettings["ExpressSponsorID"]);
            var model = GetCRMModel();
            return model.Terms_Get(null, null, null, expressSponsorID).ToList();
        }

        private bool UpdateInviteeStatus(FriendsAndFamily invitee, long relationId, long sponsorShipId, InviteStatus status)
        {
            var crm = GetCRMModel();

            if (invitee != null)
            {
                invitee.InviteStatus = (int)status;
                invitee.UpdatedDate = DateTime.Now;
                invitee.RelationID = relationId;
                invitee.RelationSponsorshipID = sponsorShipId;

                crm.SaveChanges();

                AddMemberFriend(invitee, relationId); // should be 2

                return true;
            }
            return false;
        }

        /// <summary>
        /// Adds 2 MemberFriend records (2 way relation)
        /// </summary>
        /// <param name="invitee"></param>
        /// <param name="relationId"></param>
        private void AddMemberFriend(FriendsAndFamily invitee, long relationId)
        {
            try
            {
                var today = DateTime.Now;
                var socialModel = GetSocialModel();
                socialModel.AddMemberFriend(invitee.MemberID, relationId, today);
                socialModel.SaveChanges();
            }
            catch (Exception ex)
            {
                Log.Error("Friends not added for memberId: " + invitee.MemberID + ". " + ex);
            }
        }

        private byte[] Encrypt(string sourceString)
        {
            try
            {
                // the Key
                const string S_KEY = "Sean Gadsby Created this For Virgin Lifecare";

                // Make String into Bytes
                var enc = new System.Text.UTF8Encoding();
                byte[] inputHash = enc.GetBytes(sourceString.ToLower().Trim());
                byte[] bKey = enc.GetBytes(S_KEY);

                // Create the Encrypt the Bytes
                KeyedHashAlgorithm dsaHash = KeyedHashAlgorithm.Create();
                dsaHash.Key = bKey;
                byte[] sourceHash = dsaHash.ComputeHash(inputHash);

                return sourceHash;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return null;
            }
        }

        public object GetSponsorFilters(long sponsorId)
        {
            //CGW 1/9/14 - Get FilterValues to display on enrollment page
            var model = GetCRMModel();

            List<SponsorFilter_Result> Filters = model.GetSponsorFilter(sponsorId.ToString(), null, null, null).Where(x => x.Display == true).ToList();

            List<SponsorFilterValues_Result> FilterValues = model.GetSponsorFilterValues(sponsorId, null, null, null, null, false, 500, false, null).ToList();

            List<object> filterValueList = new List<object>();

            //CGW 1/8/2014, Build list of filters with each having a list of values
            foreach (var userStore in Filters.AsEnumerable())
            {
                var filters = new
                {
                    FilterName = userStore.FilterName,
                    SponsorFilterID = userStore.SponsorFilterID,
                    FilterValueID = 0,
                    FilterValues = FilterValues.Where(x => x.FilterNum == userStore.FilterNum).Select(x => new
                    {
                        FilterValueID = x.SponsorFilterValueID,
                        FilterValueText = x.FilterValue
                    }).ToList()
                };

                filterValueList.Add(filters);
            }
            return filterValueList;
        }
    }
}