﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VHm.Web.Api.FriendsAndFamily.Models
{
    public class SponsorFilterDTO
    {
        public string FilterName { get; set; }
        public long FilterValueID { get; set; }
        public long SponsorFilterID { get; set; }
    }
}
