//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VHm.Web.Api.FriendsAndFamily.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MemberProfile
    {
        public long MemberID { get; set; }
        public string DisplayName { get; set; }
        public string Motto { get; set; }
        public byte[] ProfilePicture { get; set; }
        public short ShowGender { get; set; }
        public short ShowAge { get; set; }
        public bool ShowState { get; set; }
        public short ShowStepsAvg { get; set; }
        public short ShowLastGZUpload { get; set; }
        public bool ShowLevel { get; set; }
        public bool ShowPoints { get; set; }
        public bool ShowAnniversary { get; set; }
        public short ShowChallengeHistory { get; set; }
        public short ShowSponsors { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public short ShowNickname { get; set; }
        public short ShowCity { get; set; }
        public short ShowOccupation { get; set; }
        public short ShowGoals { get; set; }
        public short ShowInterests { get; set; }
        public bool PersonalInfoComplete { get; set; }
        public bool OccupationComplete { get; set; }
        public bool GoalsComplete { get; set; }
        public bool InterestsComplete { get; set; }
        public short ShowTotalSteps { get; set; }
        public int NewsFeedPermissions { get; set; }
        public int ShowFriendNewsFeed { get; set; }
        public bool PrivacySettingsSet { get; set; }
        public short ShowBadges { get; set; }
        public bool Privacy { get; set; }
    
        public virtual Member Member { get; set; }
    }
}
