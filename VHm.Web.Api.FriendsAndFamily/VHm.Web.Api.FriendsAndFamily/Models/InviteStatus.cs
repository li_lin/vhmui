﻿namespace VHm.Web.Api.FriendsAndFamily.Models
{
    public enum InviteStatus
    {
        Accepted = 1,
        Pending = 2,
        Declined = 3,
        Expired = 4,
        Revoked = 5,
        PendRevoked = 6,
        All = 7
    }
}