﻿namespace VHm.Web.Api.FriendsAndFamily.Models
{
    public class TermsDTO
    {
        public string TermsCode { get; set; }
        public bool Accept { get; set; }
        public long EntityID { get; set; }
    }
}