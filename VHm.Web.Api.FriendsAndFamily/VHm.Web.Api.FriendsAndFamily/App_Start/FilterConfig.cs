﻿using System.Web;
using System.Web.Mvc;

namespace VHm.Web.Api.FriendsAndFamily
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}