﻿using System;
using System.Linq;
using NUnit.Framework;
using SimpleBrowser;
using TechTalk.SpecFlow;

namespace SpecFlowRegression
{
    [Binding]
    public class LogOnSteps
    {
        private Browser _browser=new Browser();
        [Given(@"I am on the Login Page")]
        public void GivenIAmOnTheLoginPage()
        {
            _browser.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.224 Safari/534.10";
            _browser.Navigate("https://local.virginhealthmiles.com/login.aspx");
            Assert.IsTrue(_browser.Find("h2", FindBy.Text, "Member Login").Exists);
            ScenarioContext.Current.Add( "Browser", _browser );
        }

        [When(@"I enter my username and password")]
        public void WhenIEnterMyUsernameAndPassword()
        {
            _browser.Find("oUserID").Value = "0525800002";
            _browser.Find("oPwdID").Value = "a1111111";
        }

        [When(@"I click the Log On button")]
        public void WhenIClickTheLogOnButton()
        {
            _browser.Find("oLogon").Click();
        }

        [Then(@"I am on the Home Page Logged In")]
        public void ThenIAmOnTheHomePageLoggedIn()
        {
            Assert.GreaterOrEqual(_browser.Url.AbsolutePath, "/v2/member/landingpage", "it was" + _browser.Url.AbsolutePath);
        }

    }
}
