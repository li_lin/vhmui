﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using NUnit.Framework;
using TechTalk.SpecFlow;
using SimpleBrowser;

namespace SpecFlowRegression
{
    [Binding]
    public class EnrollmentSteps
    {
        private readonly Browser _browser = new Browser();
        private long _sponsorId;

        [Given(@"I have sponsorId")]
        public void GivenIHaveSponsorId()
        {
            _sponsorId = 1;
        }

        [Given(@"I am on product page")]
        public void GivenIAmOnProductPage()
        {
            _browser.Navigate( "https://local.virginhealthmiles.com/join/product.aspx?cid=1" );
        }

        [When(@"I choose package")]
        public void WhenIChoosePackage()
        {
            _browser.Find( "lnkJoinRpt" ).Click();
        }

        [Then(@"I need to go to enrollment page")]
        public void ThenINeedToGoToEnrollmentPage()
        {
            Assert.IsTrue(_browser.Url.AbsolutePath.Contains("enrollment.aspx"));
        }

        [Then(@"I entered all data")]
        public void ThenIEnteredAllData()
        {
            _browser.Find( "txtFName" ).Value = GenerateChar( 5 );
            _browser.Find( "txtLName" ).Value = GenerateChar( 5 );
            _browser.Find( "rdoListGender_0" ).Click();
            _browser.Find( "ddlDOBMonth" ).Value = "1";
            _browser.Find( "ddlDOBDay" ).Value = "1";
            _browser.Find( "ddlDOBYear" ).Value = "1988";
            _browser.Find("txtCity").Value = GenerateChar(5);
            _browser.Find("ddlHomeRegion").Value = "AL";
            _browser.Find("txtPhoneNumber").Value = "5555555555";
            _browser.Find("txtPostalCode").Value = "22222";
            var email = GenerateChar( 5 ) + "@mail.com";
            _browser.Find("txtEmail").Value = email;
            _browser.Find( "txtEmail2" ).Value = email;
            _browser.Find("txtPin").Value = "a1111111";
            _browser.Find("txtPin2").Value = "a1111111";

        }

        [When(@"I press next button")]
        public void WhenIPressNextButton()
        {
            _browser.Find( "lbtnSubmitPersonalInfo" ).Click();

        }

        [Then(@"i should see enrollment step(.*)")]
        public void ThenIShouldSeeEnrollmentStep(int p0)
        {
            Assert.IsTrue(_browser.Url.PathAndQuery.Contains("enrollment.aspx?enrollmentstep=3"));
            Assert.IsTrue(_browser.Find("chbTermsReqAUT").Exists);
        }

        public string GenerateChar()
        {
            Random random = new Random();

            return Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65))).ToString();
        }

        public string GenerateChar(int count)
        {
            string randomString = "";

            for (int i = 0; i < count; i++)
            {
                randomString += GenerateChar();
            }

            return randomString;
        }

    }
}
