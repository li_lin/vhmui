﻿using System;
using TechTalk.SpecFlow;
using SimpleBrowser;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using NUnit.Framework;

namespace SpecFlowRegression
{
    [Binding]
    public class CheckAllPagesSteps:Steps
    {
        private Browser _browser=new Browser();
        [Given(@"I am on the Home Page Logged In")]
        public void GivenIAmOnTheHomePageLoggedIn()
        {
            Given( "I am on the Login Page");
            When( "I enter my username and password" );
            And( "I click the Log On button" );
            _browser = ScenarioContext.Current["Browser"] as Browser;
        }

        
        [When(@"I press program description page link")]
        public void WhenIPressProgramDescriptionPageLink()
        {
            var mainNav = _browser.Find( "ul", FindBy.Id, "mainNav" );
            var pDescription = mainNav.XElement;
            var pD= pDescription.Elements( "li" ).Where( s => s.Elements("a").First().Attribute( "href" ).Value.Contains( "programdescription.aspx" ));
            _browser.Navigate( "https://local.virginhealthmiles.com/secure/rewards/programdescription.aspx" );
            Assert.IsNotNull(pD,"this should not be null");


        }
        
        [Then(@"i should be able to see that page")]
        public void ThenIShouldBeAbleToSeeThatPage()
        {
            var header=_browser.Find( "div", FindBy.Id, "contentHeader" );
            Assert.IsTrue(_browser.Url.AbsolutePath.Contains("programdescription.aspx"),"you are on wrong page");
        }
    }
}
