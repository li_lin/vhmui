﻿using System;
using System.Collections.Generic;

namespace Vhm.Providers.Interfaces
{
    public interface IContentProvider
    {
        Dictionary<T, string> GetContent<T>() where T : struct, IConvertible;
        string GetValue(string applicationName, string key, string culture);
    }
}
