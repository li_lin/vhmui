﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Vhm.Providers.Interfaces
{
    public interface ICookieProvider
    {
        HttpCookie Get(string name);
        void Set(string name, string value);
        void Remove(string name);
    }
}
