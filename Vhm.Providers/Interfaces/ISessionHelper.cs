﻿namespace Vhm.Providers.Interfaces
{
    public interface ISessionHelper
    {
        string GetSessionId();
        object GetValue(string key);
        void SetValue(string key, object value);
    }
}
