﻿using System;
using System.Web;
using System.Web.Caching;

namespace Vhm.Providers
{
    public class ApplicationCacheHelper
    {
        public static T GetData<T>(string dataCacheKey, int cacheTimeInMinutes, Func<T> dataMethod) where T : class
        {
            var data = HttpRuntime.Cache.Get(dataCacheKey) as T;

            if (data == null)
            {
                lock (HttpRuntime.Cache)
                {
                    data = HttpRuntime.Cache.Get(dataCacheKey) as T;

                    if (data == null)
                    {
                        data = dataMethod();

                        HttpRuntime.Cache.Add(dataCacheKey, data, null, DateTime.Now.AddMinutes(cacheTimeInMinutes), Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
                    }
                }
            }

            return data;
        }
    }
}
