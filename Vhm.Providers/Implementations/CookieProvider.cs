﻿using System.Web;
using Vhm.Providers.Interfaces;

namespace Vhm.Providers.Implementations
{
    public class CookieProvider : ICookieProvider
    {
        public HttpCookie Get(string name)
        {
            return HttpContext.Current.Request.Cookies[name];
        }

        public void Set(string name, string value)
        {
            HttpContext.Current.Response.Cookies.Add(new HttpCookie(name, value) { Secure = true });
        }

        public void Remove(string name)
        {
            HttpContext.Current.Response.Cookies.Set(new HttpCookie(name, null) { Secure = true });
        }
    }
}
