﻿using System;
using System.Collections.Generic;
using System.Resources;
using Vhm.Providers.Interfaces;
using System.Globalization;

namespace Vhm.Providers.Implementations
{
    public class ResourceFileContentProvider : IContentProvider
    {
        private readonly ResourceManager _resourceManager;

        public ResourceFileContentProvider(ResourceManager resourceManager)
        {
            _resourceManager = resourceManager;
        }

        public string GetValue(string region, string key, string cultureCode)
        {
            // Since we're pulling from a static resource file, the region can be discarded
            return _resourceManager.GetString(string.Format(key), CultureInfo.GetCultureInfo(cultureCode));
        }

        public Dictionary<T, string> GetContent<T>() where T : struct, IConvertible 
        {
            var content = new Dictionary<T, string>();
            var typeOfT = typeof (T);

            foreach (var id in Enum.GetValues(typeOfT))
             {
                 content.Add((T)id, GetValue("Sponsor", Enum.GetName(typeOfT, id), string.Empty));
             }
            return content;
        }
    }
}
