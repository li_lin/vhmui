﻿using System.Collections;
using System.Web;
using System.Web.Security;
using Vhm.Providers.Exceptions;
using Vhm.Providers.Interfaces;

namespace Vhm.Providers.Implementations
{
    public class SessionHelper : ISessionHelper
    {
        public string GetSessionId()
        {
            var httpContext = HttpContext.Current;

            if (httpContext == null)
            {
                throw new HttpContextNotFoundException();
            }

            var request = httpContext.Request;

            var sessionCookie = request.Cookies["SessionID"];

            if (sessionCookie != null)
            {
                var encryptedSessionId = sessionCookie.Value;

                if (!string.IsNullOrEmpty(encryptedSessionId))
                {
                    var formsAuthenticationTicket = FormsAuthentication.Decrypt(encryptedSessionId);
                    return formsAuthenticationTicket.UserData;
                }                                            
            }

            return string.Empty;
        }

        public object GetValue(string key)
        {
            return HttpContext.Current.Session[key];
        }

        public void SetValue(string key, object value)
        {
            var context = HttpContext.Current;

            if (context.Session != null)
            {
                if (context.Session[key] != null)
                {
                    context.Session[key] = value;
                }
                else
                {
                    context.Session.Add(key, value);
                }
            }
        }
    }
}
