﻿using System;
using System.Runtime.Serialization;

namespace Vhm.Providers.Exceptions
{
    public class HttpContextNotFoundException : Exception, ISerializable
    {
        public HttpContextNotFoundException()
            : base("You must call this code from the context of a web application.")
        {

        }
    }
}
