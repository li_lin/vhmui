﻿using System;
using System.Web;
using Vhm.Web.Common;

namespace Vhm.Providers
{
    public static class CacheProvider
    {
        public enum CacheItems
        {
            MainMenu,
            MvcMenu,
            SponsorText,
            SponsorImageUrl,
            SponsorShowBroughtToYouBy,
            EloquaGuid,
            ProfileUpdated,
            Miles,
            FooterMenuData,
            CreditCardOrder,
            OrderHistoryUpdated,
            CheckSubmission,
            CartItems,
            CartOrder,
            ACHDeposit,
            CheckMember,
            ShadowSession,
            FinishedWelcomeJourney,
            MemberNo,
            SessionVariables,
            HSnapshotDoNotRemind,
            VHMSessionObject,  // used in the webforms site
            VHMWebSessionObject, // used in the MVC site
            SessionCheckTimestamp,
            AdminMenu,
            AdminSubmenu,
            BadgesSetting,
            MemberModules,
            LandingPageContent,
            KagContent,
            IsConnectedToRunKeeper,
            IsRunKeeperDataSync,
            IsConnectedToFitbit,
            IsConnectedToEndomondo,
            IsConnectedToMisfit,
            ConnectToVendorStatusResponse,
            IsFitbitDataSync,
            NewsfeedContent,
            MemberVoucher,
            MeasurementSummaryContent,
            MaxRecentlyEarnedRewardsToShow,
            MaxRecentlyEarnedRewardsSummaryToShow,
            ChallengeEndedDisplayDays,
            ActivityGraphHeight,
            ActivityDaysToShow,
            StepsTargetDefault,
            ModuleImageUrl,
            SiteImageUrl,
            BadgeImageUrl,
            RewardTypeImageUrl,
            MemberChallenges,
            SponsorThemes,
            WelcomeJourney,
            ContentArea2X2,
            ContentAreaBannerAd,
            ContentAreaVHMContent,
            ContentAreaSponsorContent,
            MyGroupsContent,
            SocialChallengePromotionLabelsContent,
            CreateGroup,
            YourGroupCreated,
            ErrorMessages,
            CommonLabels,
            Member,
            HeaderContent,
            SponsorProfile, 
            UpForGrabsItem,
            WelcomeJourneyLabels,
            MemberProfile,
            SocMemberProfile,
            MemberGoZoneDeviceMajorVersion,
            CorporateChallenges,
            OAuthSecret,
            OAuthFitbitSecret
        }
        //private static readonly HttpContext _context;
        private const string ERROR_NO_CONTEXT = "Cannot innitialize context";
        private const string ERROR_UNKNOWN = "Unknown error trying to get item {0} from cache";

        
        //public CacheProvider(HttpContext context)
        //{
        //    _context = context;

        //    if (_context == null)
        //    {
        //        throw new Exception(ERROR_NO_CONTEXT);
        //    }
        //}
        private static string GetLanguageId(HttpContext ctx)
        {
            var languageCookie = ctx.Request.Cookies[ApplicationCookies.VHMLanguageId.ToString()];
            var languageId = languageCookie != null ? languageCookie.Value.ToString() : ApplicationSettings.DefaultLanguage.ToString();
            return languageId;
        }
        public static object Get(HttpContext ctx, CacheItems key)
        {
            try
            {

                return ctx.Session != null ? ctx.Session[GetLanguageId(ctx) + key.ToString()] : null;
            }
            catch(Exception ex)
            {
                string message = string.Format(ERROR_UNKNOWN, key);
                throw new Exception(message + ": " + ex.Message);
            }
        }

        public static object Add(HttpContext ctx, CacheItems key, object item)
        {
            if(ctx.Session != null)
            {
                if(ctx.Session[GetLanguageId(ctx) + key.ToString()] != null)
                {
                    Update(ctx, key, item);
                }
                else
                {
                    ctx.Session.Add(GetLanguageId(ctx) + key.ToString(), item);
                }

                return item;
            }
            return null;
        }

        public static void Remove(HttpContext ctx, CacheItems key)
        {
            if(ctx.Session != null)
            {
                ctx.Session.Remove(GetLanguageId(ctx) + key.ToString());
            }
        }

        public static object Update(HttpContext ctx, CacheItems key, object item)
        {
            if(ctx.Session != null)
            {
                if(ctx.Session[GetLanguageId(ctx) + key.ToString()] == null)
                {
                    Add(ctx, key, item);
                }
                else
                {
                    ctx.Session[GetLanguageId(ctx) + key.ToString()] = item;
                }
                return item;
            }
            return null;
        }

        public static void Clear(HttpContext context)
        {
            if(context.Session != null)
            {
                context.Session.RemoveAll();
            }
        }
    }
}
