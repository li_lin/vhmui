﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MemberBioMetricStats.ascx.cs" Inherits="Vhm.Widgets.POC.MemberBioMetricStats" %>
                         
<style type="text/css">
    table#bioStats{ border-collapse: collapse;}
    table#bioStats th{ text-align: left;}
    table#bioStats td{ line-height: 22px;padding: 3px;}
    tr.even{ background: #ffffff;}
    tr.odd{ background: #8DB6CD;}	
</style>

<div style="width: 530px; margin-top: 35px;">
    <table id="bioStats">
        <colgroup>
            <col width="45%"/>
            <col width="20%"/>
            <col width="20%"/>
            <col width="15%"/>
        </colgroup>
        <tr class="even">
            <th>Test</th>
            <th>Result</th>
            <th>Ideal Range</th>
            <th></th>
        </tr>                   
        <tr class="odd">
            <td>Push Ups</td>
            <td>10</td>
            <td>20&nbsp;-&nbsp;40</td>
            <td></td>
        </tr>
        <tr class="even">
            <td>Curl Ups</td>
            <td>20</td>
            <td>20&nbsp;-&nbsp;50</td>
            <td></td>
        </tr>
        <tr class="odd">
            <td>Flexibility</td>
            <td>12 in</td>
            <td></td>
            <td></td>
        </tr>
        <tr class="even">
            <td>Resting Pulse</td>
            <td>43 bpm</td>
            <td>40-73 bpm</td>
            <td></td>
        </tr>
        <tr class="odd">
            <td>Cardiovascular Fitness</td>
            <td>40.9</td>
            <td>&gt;30</td>
            <td></td>
        </tr>
    </table>
</div>