﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MyRewards.ascx.cs" Inherits="Vhm.Widgets.POC.MyRewards" %>
     
<style type="text/css">                                                                                                  
    div.rewardsItem{ float: left;margin: 8px;}
	div.rewardAmt{ position: absolute;font-weight: bold;color: #CB0100;font-size: 13px; text-align: center;width: 50px;}
	table#friendRwdTbl{ width: 100%; }
	table#friendRwdTbl td{ padding: 4px;}
	span.name{ color: #A4D3EE;font-weight: bold;}
	span.bold{ font-weight: bold;}
</style>

<div style="width:240px; background: #FFFFE0; padding:7px; padding-bottom: 70px; margin-top: 35px;">
    <h2>Rewards:</h2>                       
    <p><span class="bold">You have rewards!</span> You have received <span class="bold">$250 HealthCash, 1000 HealthMiles</span> and the 
    <span class="bold">Healthy Heart badge</span> for completing your screening.</p>
    <div class="rewardsItem">
        <div class="rewardAmt" style="margin-top: 10px;">$250</div>                                                                     
        <img src="/VirginLifeCare/images/rewards/RewardTypes/badge_on.gif" />
    </div>
    <div class="rewardsItem">
        <div class="rewardAmt"style="margin-top: 3px;">1000</div>                                                                     
        <img src="/VirginLifeCare/images/rewards/RewardTypes/badge_hm_on.gif" />
    </div>
    <div class="rewardsItem">
        <div class="rewardAmt" style="margin-top: 10px;">$250</div>                                                                     
        <img src="/VirginLifeCare/images/rewards/RewardTypes/badge_on.gif" />
    </div>
</div>