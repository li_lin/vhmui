﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Widgets.POC {
    public partial class OHDEarnedRewards : System.Web.UI.UserControl {

        protected string Title { get; set; }
        protected string BadgeImage { get; set; }
        protected List<RewardItem> Rewards;
        protected void Page_Load(object sender, EventArgs e)
        {

            this.Title = "Vital Numbers Screening";
            const string TEST_SESSION = "6017BD471FB288ECCF73498FDA41570863A78DA45C5AC46F18ACEEDA26345D8CEBF5AC084F37AD1792430C88611D38A7C749B9D40954FE2355541BF1A479808766A4ED86A98FB3858D17E89B1E513CBDE46543186E0208A10DAF184C25F7683E3117E7BEFE6D27EA4BAFFF471194E8FEE1699C4D37352B9AE50E8C6592E9E50829946D4DB020202F98CB7CAE31C1B547E04662880D9DFFF94E687C98A8E31A05ADC096B2CF2CABDC904809A36A064449858EEB49DB35F4C2415CD3746C5422AB6D645C27";
            string sessionId = TEST_SESSION;

            if (Context.Session["SessionID"] != null) {
                sessionId = Context.Session["SessionID"].ToString();
            }
            

            this.Rewards = new List<RewardItem>();
            this.Rewards.Add(new RewardItem("", 250, "badge_on.gif", true));
            this.Rewards.Add(new RewardItem("", 1000, "badge_hm_on.gif", false));
            this.Rewards.Add(new RewardItem("", 0, "hh.png", false));
        }

        
    }
}