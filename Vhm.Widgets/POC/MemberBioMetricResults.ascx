﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MemberBioMetricResults.ascx.cs" Inherits="Vhm.Widgets.POC.MemberBioMetricResults" %>

<script type="text/javascript">
    $(document).ready(function () {
        var newTop = parseInt($(".graphFrame").height());
        newTop -= $(".bar").height() -9;
        $(".bar").css("margin-top", newTop + "px");
    });

</script>
<style type="text/css">
    div.graph
    {
    	display: inline-block;
    	margin-left: 25px;
        margin-right: 10px;
    }
    .graphFrame
    {
    	border-bottom: solid 1pt silver;
    	border-collapse: collapse;
    }
    .graphFrame td
    {
     border-top: solid 1pt silver;
     height: 25px;
     width: 80px;
    }
    .graphFrame td.label
    {
    	vertical-align: bottom;
    	width: 10px;
    }
    .graphFrame td.label label
    {
        margin-left: -23px;
    	margin-top: -37px;
    	position: absolute;
    	text-align: center;
    	width: 20px;
    }
    label.label
    {
    margin-left: -23px;
    	margin-top: -12px;
    	position: absolute;
    	text-align: center;
    	width: 20px;
    }
    div.bar
    {
    	background: #104E8B;
        color: #fff;
        height: 75px;
        margin-left: 35px;
        margin-top: -80px;
        padding: 3px;
    	position: absolute;
        text-align: center;
    	width: 20px;
    }
    h3.graphTitle
    {
    	margin-bottom: 15px;
    	font-size: 12px;
    }
</style>                                                 
<div class="widget" style=" margin-top: 35px; margin-bottom: 6px;">
<% foreach (MeasurementData data in this.Data){ %>
<div class="graph">
    <h3 class="graphTitle"><%=data.Title %></h3>
    <div class="bar">
        <label><%=data.Value1 %></label>
    </div>
    <% if (data.Value2.HasValue) { %>
    <div class="bar">
        <label><%=data.Value2%></label>
    </div>
    <%} %>
    <table class="graphFrame">
        <tr>
            <td class="label"><label class="label">60</label></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="label"><label class="label">50</label></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="label"><label class="label">40</label></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="label"><label class="label">30</label></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="label"><label class="label">20</label></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="label"><label class="label">10</label></td>
            <td>&nbsp;</td>
        </tr>
    </table>
    <label class="label">0</label>
</div>
<%} %>    
</div>