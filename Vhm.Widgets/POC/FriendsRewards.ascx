﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FriendsRewards.ascx.cs" Inherits="Vhm.Widgets.POC.FriendsRewards" %>

<style type="text/css">
	div.rewardAmt{ position: absolute;font-weight: bold;color: #CB0100;font-size: 13px;text-align: center;width: 50px;}
	table#friendRwdTbl{ width: 100%; }
	table#friendRwdTbl td{ padding: 4px;}
	span.name{ color: #A4D3EE;font-weight: bold;}
	span.bold{ font-weight: bold;}
</style>

<div style="width:270px; margin-top: 35px;">
    <h2>What are my friends earning?</h2>           
    <table id="friendRwdTbl" style="width: 100%;">
        <tr>
            <td>
                <div class="rewardAmt" style="margin-top: 10px;">$250</div>                                                                     
                <img src="/VirginLifeCare/images/rewards/RewardTypes/badge_on.gif" />
            </td>                                                          
            <td>
                <span class="name">Linn Keife</span> and <span class="name">Cayte Burdick</span> have earned <span class="bold">$250</span> and the <span class="bold">Healthy Heart</span> badge for completing the screening.    
            </td>
        </tr>
        <tr>
            <td>
                <div class="rewardAmt">1,000</div>                                                                     
                <img src="/VirginLifeCare/images/rewards/RewardTypes/badge_hm_on.gif" />
            </td>                                                          
            <td>
                <span class="name">Richard Boylan, Linn Keife</span> and <span class="name">3 others</span> have earned <span class="bold">1,000 HealthMiles</span> for signing up for the screening.
            </td>
        </tr>
    </table>    

</div>
