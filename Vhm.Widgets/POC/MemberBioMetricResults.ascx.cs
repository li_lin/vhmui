﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Vhm.Widgets.POC {
    public partial class MemberBioMetricResults : System.Web.UI.UserControl {

        protected List<MeasurementData> Data;

        protected void Page_Load(object sender, EventArgs e) {

            this.Data = new List<MeasurementData>();
            this.Data.Add(
                new MeasurementData {
                    Value1 = 120,
                    Value2 = 80,
                    Title = "Blood Pressure",
                    IdealMin1 = 100,
                    IdealMax1 = 120,
                    IdealMin2 = 60,
                    IdealMax2 = 80,
                    Message = "Way to to!",
                    X_Axis_Label = "MM/YY",
                    Y_Axis_Label = "mm/Hg"
                }
                );
            this.Data.Add(
                new MeasurementData {
                    Value1 = 175,
                    Value2 = null,
                    Title = "Weight",
                    IdealMin1 = 160,
                    IdealMax1 = 190,
                    IdealMin2 = null,
                    IdealMax2 = null,
                    Message = "Ooo looking good!",
                    X_Axis_Label = "MM/YY",
                    Y_Axis_Label = "pounds"
                }
                );
            this.Data.Add(
                new MeasurementData {
                    Value1 = 18,
                    Value2 = null,
                    Title = "Body Mass Index (BMI)",
                    IdealMin1 = 17,
                    IdealMax1 = 26,
                    IdealMin2 = null,
                    IdealMax2 = null,
                    Message = "Ooo looking good!",
                    X_Axis_Label = "MM/YY",
                    Y_Axis_Label = "pounds"
                }
                );
            this.Data.Add(
                new MeasurementData {
                    Value1 = 18,
                    Value2 = null,
                    Title = "Body Fat",
                    IdealMin1 = 16,
                    IdealMax1 = 26,
                    IdealMin2 = null,
                    IdealMax2 = null,
                    Message = "Ooo looking good!",
                    X_Axis_Label = "MM/YY",
                    Y_Axis_Label = "percent"
                }
                );


        }

        protected class MeasurementData {
            public int Value1 { get; set; }
            public int? Value2 { get; set; }
            public string Title { get; set; }
            public int IdealMin1 { get; set; }
            public int IdealMax1 { get; set; }
            public int? IdealMin2 { get; set; }
            public int? IdealMax2 { get; set; }
            public string Message { get; set; }
            public string X_Axis_Label { get; set; }
            public string Y_Axis_Label { get; set; }

            public MeasurementData(){
            }
            public MeasurementData(int v1, int v2, string t, int min1, int max1, int? min2, int? max2, string message, string xLabel, string yLabel) {
                this.Value1 = v1;
                this.Value2 = v2;
                this.Title = t;
                this.IdealMin1 = min1;
                this.IdealMax1 = max1;
                this.IdealMin2 = min2;
                this.IdealMax2 = max2;
                this.Message = message;
                this.X_Axis_Label = xLabel;
                this.Y_Axis_Label = yLabel;
            }
        }
    }

    
}