﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OHDScheduler.ascx.cs" Inherits="VirginLifecareWeb.Views.PartnerWidget.OHDScheduler" %>


<script type="text/javascript">
    $(document).ready(function () {

        $("#btnOpenOHDWindow").click(function () {
            openOHDWindow();

            $.ajax({
                url: '',
                type: 'POST'
            });
        });
    });    

    function openOHDWindow() {

        window.open("http://google.com", "ohdWindow", "target=_blank");
    }
</script>

<div class="widget">
    <h1>Call to Action</h1>
    <p>Ready to get started? Visit Onsite Health Diagnostics' site to arrange your biometric screening.</p>                                                                                      
    <div>
        <input id="btnOpenOHDWindow" type="button" class="SubmitButton" value="Register"/>                              
    </div>       
    <div>
        <h2>Questions?</h2>
        <span>Call OHD at 877-366-7843</span>
    </div>
</div>