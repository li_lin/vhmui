﻿
describe("SelectVM", function () {

    var SelectVM;

    beforeEach( function() {
        SelectVM = new SelectChallengeModel();
        SelectVM.ShowErrors = false;
        SelectVM.GetModel();
    });

    it( "should have a content dictionary", function() {
        expect(SelectVM.Content).toNotBe(null);
    });
    
    it("should have 3 arrays of challenge types", function () {

        expect( SelectVM.ActivityBadges).toNotBe( null );
        expect( SelectVM.NutritionBadges).toNotBe( null );
        expect( SelectVM.WellBeingBadges).toNotBe( null );
    });
    
} );

