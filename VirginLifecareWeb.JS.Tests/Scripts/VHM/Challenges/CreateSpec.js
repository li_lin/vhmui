﻿
describe( "CreateVM", function() {

    var CreateVM;
    var TARGET = '10';
    var QUESTION = 'Do you like testing?';
    var DETAILS = 'These are the challenge details';
    beforeEach( function() {
        CreateVM = new CreateChallengeModel();
        CreateVM.ShowErrors = false;
        CreateVM.Target(TARGET);
        CreateVM.Question(QUESTION);
        CreateVM.AnswerType(['YNO', 'NUM']);
        CreateVM.MoreLess(['more', 'less']);
        CreateVM.Duration(['oneDay', 'oneWeek', 'twoWeek', 'oneMonth']);
        CreateVM.Details(DETAILS);

        var badges = new Array();
        badges.push( { BadgeId: 1 } );
        CreateVM.Badges(badges);
        
        CreateVM.BadgeId( badges[0].BadgeId );
    });
    
    it("should have a target", function () {
        expect(CreateVM.Target()).toEqual(TARGET);
    });
    it("should have a question", function () {
        expect(CreateVM.Question().length).toEqual(QUESTION.length);
    });
    it("should have two answer types", function () {
        expect(CreateVM.AnswerType().length).toEqual(2);
    });
    it( "should have two 'more/less' values", function() {
        expect( CreateVM.MoreLess().length ).toEqual( 2 );
    });
    it("should have four 'duration' values", function () {
        expect(CreateVM.Duration().length).toEqual(4);
    });
    it("should have details", function () {
        expect(CreateVM.Details().length).toEqual(DETAILS.length);
    });
    it("should have a collection of badges", function () {
        expect(CreateVM.Badges().length).toEqual(1);
    });
    it( "should pass validation when all fields are suupplied", function() {
        expect( CreateVM.IsValid() ).toEqual( true );
    });
    it("should fail validation when question is missing and should have one validation warning", function () {
        CreateVM.Question('');
        CreateVM.CheckIsValid();
        expect(CreateVM.IsValid()).toEqual(false);
        expect(CreateVM.ValidationErrors().length).toEqual(1);
    });
    it("should fail validation when duration is missing and should have one validation warning", function () {
        CreateVM.Duration('');
        CreateVM.CheckIsValid();
        expect(CreateVM.IsValid()).toEqual(false);
        expect(CreateVM.ValidationErrors().length).toEqual(1);
    });
    it("should fail validation when answer type is missing and should have one validation warning", function () {
        CreateVM.AnswerType([]);
        CreateVM.CheckIsValid();
        expect(CreateVM.IsValid()).toEqual(false);
        expect(CreateVM.ValidationErrors().length).toEqual(1);
    });
    it("should fail validation when badge Id is missing and should have one validation warning", function () {
        CreateVM.BadgeId(-1);
        CreateVM.CheckIsValid();
        expect(CreateVM.IsValid()).toEqual(false);
        expect(CreateVM.ValidationErrors().length).toEqual(1);
    });
    it("should fail validation when details are missing and should have one validation warning", function () {
        CreateVM.Details('');
        CreateVM.CheckIsValid();
        expect(CreateVM.IsValid()).toEqual(false);
        expect(CreateVM.ValidationErrors().length).toEqual(1);
    });
} );