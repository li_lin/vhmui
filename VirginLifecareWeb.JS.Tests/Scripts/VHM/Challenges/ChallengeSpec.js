﻿describe("ChallengeVM", function () {

    var ChallengeVM;
    var ALLOWED_CHARS = 340;
    var testPostMessage = "This is a new post";

    String.prototype.repeat = function (num) {
        return new Array(num + 1).join(this);
    };
    
    beforeEach(function () {
        
        var idx = window.location.href.lastIndexOf('/') + 1;
        var cId = window.location.href.substring(idx, idx + 7 );
        ChallengeVM = new ChallengeModel(parseInt(cId));
        ChallengeVM.ShowErrors = false;
        var days = [];
        var answers = ['1', '0', '1', '1', '1', '1', '0'];
        
        // assumes this signatore for TrackedDay:
        // self.TrackedDay = function( answerType, answer, target, dayDisplay, day, challengeScoreId, challengeId, isMemberAnswer, useGreaterThan, min, max )
        var answerType = 'YNO';
        var target = 1;
        var dayDisplay = i + 1 + '/05/2013';
        var day = new Date(dayDisplay);
        var challengeScoreId = 1;
        var isMemberAnswer = true;
        var useGreaterThan = false;
        var min = 0;
        var max = 1;
        for (var i = 0; i < answers.length; i++) {
            var answer = answers[i];
            var newTrackedDay = new ChallengeVM.TrackedDay(answerType, answer, target, dayDisplay, day, challengeScoreId, cId, isMemberAnswer, useGreaterThan, min, max);
            days.push(newTrackedDay);
        }
       
        ChallengeVM.TrackedDays(days);
    });

    it("should be a thing", function () {
        expect(ChallengeVM).toNotBe(null);
    });
    it("should have 7 tracked days if the challenge is set up for 7", function () {
        expect(ChallengeVM.TrackedDays().length).toEqual(7);
    });
    it("should have 5 Yes days if 5 are entered", function () {
        
        var yesCount = 0;
        for (var i = 0; i < 7; i++) {
            if (ChallengeVM.TrackedDays()[i].Answer() == '1') {
                yesCount += 1;
            }
        }
        expect(yesCount).toEqual(5);
    });

    it("should not allow a post message to be entered if the number of characters is > " + ALLOWED_CHARS, function () {
        ChallengeVM.Post("a".repeat(ALLOWED_CHARS));
        expect(ChallengeVM.AllowTyping()).toEqual(false);
    });
    it("should allow a post message to be entered if the number of characters is < " + ALLOWED_CHARS, function () {
        ChallengeVM.Post("a".repeat(ALLOWED_CHARS - 5));
        expect(ChallengeVM.AllowTyping()).toEqual(true);
    });

    it("should increase the Post count by 1 after adding a message, and the text should say " + testPostMessage, function () {
        ChallengeVM.Post(testPostMessage);
        ChallengeVM.addMessage();
        expect(ChallengeVM.Messages().length).toEqual(1);
        expect(ChallengeVM.Messages()[0].FilteredMessageText).toEqual(testPostMessage);
    });
    it( "should show an inspirational message", function() {
        expect( ChallengeVM.InspriationalMessage().length ).toBeGreaterThan( 0 );
    } );
});