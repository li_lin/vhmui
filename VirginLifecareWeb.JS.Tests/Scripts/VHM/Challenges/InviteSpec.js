﻿
describe( "InviteVM", function() {

    var InviteVM;
    beforeEach(function () {
        
        var idx = window.location.href.lastIndexOf('/') + 1;
        var cId = window.location.href.substring(idx, idx + 4);
        
        InviteVM = new InviteModel(parseInt(cId));
        InviteVM.ShowErrors = false;
        InviteVM.Content({});

        var data = { MemberId: 1, EmailAddress: 'temp@tmp.com', Selected: true, DisplayName: '', ImageUrl: '' };
        var member = new Member(data);
        
        var invitees = new Array();
        invitees.push(member);
        InviteVM.Invitees(invitees);
        
        var friends = new Array();
        friends.push(member);
        InviteVM.Friends(friends);
        
        //var groups = new Array();
        //groups.push({ MemberId: 1, EmailAddress: 'temp@tmp.com' });
        //InviteVM.Groups(groups);
        
        var writeIns = new Array();
        writeIns.push(member);
        InviteVM.WriteIns(writeIns);
        
        var newList = new Array();
        newList.push(member);
        InviteVM.NewList(newList);
        
        var existingList = new Array();
        existingList.push(member);
        InviteVM.ExistingList(existingList);
        
        InviteVM.SearchVal( 'rich' );
        InviteVM.NotFound( false );
        InviteVM.ModelError( '' );
    });

    it( "should have a search value", function() {
        expect( InviteVM.SearchVal().length ).toBeGreaterThan( 0 );
    });
    it("should have a list of invitees with a memberId and email address", function () {

        var person = InviteVM.Invitees()[0];

        expect(person).toNotBe(null);
        expect(person.EmailAddress).toNotBe(null);
        expect(person.MemberId).toBeGreaterThan(0);
    });
    it("should accept a list of Friends", function () {

        var person = InviteVM.Friends()[0];

        expect(person).toNotBe(null);
        expect(person.EmailAddress).toNotBe(null);
        expect(person.MemberId).toBeGreaterThan(0);
    });
    it("should accept a list of WriteIns", function () {

        var person = InviteVM.WriteIns()[0];

        expect(person).toNotBe(null);
        expect(person.EmailAddress).toNotBe(null);
        expect(person.MemberId).toBeGreaterThan(0);
    });
    it("should accept a list of members imported from a new list", function () {

        var person = InviteVM.NewList()[0];

        expect(person).toNotBe(null);
        expect(person.EmailAddress).toNotBe(null);
        expect(person.MemberId).toBeGreaterThan(0);
    });
    it("should accept a list of members imported from an existing list", function () {

        var person = InviteVM.ExistingList()[0];

        expect(person).toNotBe(null);
        expect(person.EmailAddress).toNotBe(null);
        expect(person.MemberId).toBeGreaterThan(0);
    });
    it("should have a content dictionary", function () {
        expect(InviteVM.Content).toNotBe(null);
    });
    
    it("should not allow duplicate invitees", function () {
        
        var people = new Array();
        var data = { MemberId: 1, EmailAddress: 'temp@tmp.com', Selected: true, DisplayName: '', ImageUrl: '' };
        var member = new Member(data);
        people.push(member);
        people.push(member);

        InviteVM.Invitees([]);
        InviteVM.Friends(people);
        InviteVM.AddToInviteList();
        
        expect(InviteVM.Invitees().length).toEqual(1);
    });
    
    it("should not contain any invitees after removing them from the list", function () {

        var people = new Array();
        var data = { MemberId: 1, EmailAddress: 'temp@tmp.com', Selected: true, DisplayName: '', ImageUrl: '' };
        var member = new Member(data);
        people.push(member);
        people.push(member);

        InviteVM.Friends(people);
        
        InviteVM.AddToInviteList();

        InviteVM.RemoveAllInvitees();
        
        expect(InviteVM.Invitees().length).toEqual(0);
    });

    it("should only contain the AllFriends item when adding All Friends and the display name of that item should be 'Group: All Friends'", function () {
        var people = new Array();
        var data1 = { MemberId: 1, EmailAddress: 'temp1@tmp.com', Selected: true, DisplayName: '', ImageUrl: '' };
        var data2 = { MemberId: 1, EmailAddress: 'temp2@tmp.com', Selected: true, DisplayName: '', ImageUrl: '' };
        var data3 = { MemberId: 1, EmailAddress: 'temp3@tmp.com', Selected: true, DisplayName: '', ImageUrl: '' };
        
        people.push(new Member(data1));
        people.push(new Member(data2));
        people.push(new Member(data3));

        InviteVM.Friends(people);

        InviteVM.AddToInviteList();
        
        InviteVM.AddAllFriends();// should remove the other items and add the 1 'all friends' item

        expect(InviteVM.Invitees().length).toEqual(1);
        expect(InviteVM.Invitees()[0].DisplayName).toEqual("Group: All Friends");
    });
} );