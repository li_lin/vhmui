﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Providers.Entities;
using System.Web.Security;
using SSOEasy.EasyConnect.Integration;
using Vhm.Web.BL;
using Vhm.Web.Common;
using Virginlifecare.Aera.Configuration;

namespace VieLifeSSO.Controllers
{
    public class VieLifeSSOController : Controller
    {

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            const string SESSION_ID_COOKIE = "SessionID";

            base.Initialize(requestContext);

            try
            {
                if (Request != null && Request.Cookies != null && Request.Cookies[SESSION_ID_COOKIE] != null)
                {
                    var httpCookie = Request.Cookies[SESSION_ID_COOKIE];
                    if (httpCookie != null)
                    {
                        string sessionId = httpCookie.Value;
                        var decryptedSessionId = Decrypt(sessionId);

                        if (!string.IsNullOrEmpty(sessionId))
                        {
                            _session = AccountBL.GetSession(decryptedSessionId);

                        }
                        if (_session == null || string.IsNullOrEmpty(_session.SessionID))
                        {
                            Response.Redirect(FormsAuthentication.LoginUrl, true);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw VirginLifecare.Exception.ExceptionHandler.ProcessException( ex );
            }
        }


        public void Index()
        {
            try
            {
                var serviceProvider = ServiceProvider();

                if ( serviceProvider == "vlife" &&
                     ConfigurationFileReader.VlifeAeraSponsorIDs.Split( ',' )
                                            .Contains( _session.PrimarySponsorID.ToString( CultureInfo.InvariantCulture ) ) )
                {
                    LoadVlifeSSOAsertionValue( _session.PrimarySponsorID.ToString() );

                    var attributes = new Dictionary<string, IList<string>>();

                    foreach ( VlifeSSOAsertion ssoAsert in Enum.GetValues( typeof ( VlifeSSOAsertion ) ) )
                    {
                        attributes[ssoAsert.ToString()] = new List<string> { GetValue( ssoAsert ) };
                    }

                    IntegrationConfiguration.FileName = Server.MapPath( "App_Data\\integration.config" );

                    //Call InitiateSSO to asert sso and begin
                    IdentityProvider.InitiateSSO( System.Web.HttpContext.Current.Response,
                                                  GetValue( VlifeSSOAsertion.serviceProviderID ),
                                                  "urn:oasis:names:tc:SAML:2.0:ac:classes:Password",
                                                  GetValue( VlifeSSOAsertion.SSOname ),
                                                  attributes,
                                                  GetValue( VlifeSSOAsertion.serviceURL )
                        );

                }
            }
            catch ( Exception ex )
            {
                throw VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
            }
        }

        protected Vhm.Web.Models.Session _session;
        private static Dictionary<VlifeSSOAsertion, string> VlifeSSOAsertionValues = null;

        enum VlifeSSOAsertion
        {
            serviceProviderID,
            SSOname,
            serviceURL,
            orgunitid,
            clientid,
            email,
            firstname,
            lastname,
            dateOfbirth,
            gender,
            keyword,
            sponsorid,
            externalcustomerid,
            languagepreference
        }
        private string Decrypt(string dataToDecrypt)
        {
            var data = string.Empty;
            try
            {
                var formsAuthenticationTicket = FormsAuthentication.Decrypt(dataToDecrypt);

                data = formsAuthenticationTicket != null
                           ? formsAuthenticationTicket.UserData
                           : string.Empty;
            }
            catch (Exception ex)
            {
               
            }

            return data;
        }

        private string GetValue(VlifeSSOAsertion asertion)
        {
            string value;
            return (VlifeSSOAsertionValues.TryGetValue(asertion, out value)) ? value : asertion.ToString();
        }

        private string ServiceProvider()
        {
            string serviceProvider = Request.Url.Query;
            if (!string.IsNullOrEmpty(serviceProvider) && serviceProvider.IndexOf("sp", StringComparison.Ordinal) >= 0)
            {
                NameValueCollection querryCollection = HttpUtility.ParseQueryString(serviceProvider);

                serviceProvider = Convert.ToString(querryCollection["sp"]);
            }
            return serviceProvider;
        }

        /// <summary>
        /// Returns true if the memberID has a value but the PrimarySponsorID is zero. 
        /// </summary>
        /// <returns></returns>
        private bool IsMemberCancelled()
        {
            return _session.PrimarySponsorID == 0 && _session.MemberID > 0;
        }

        /// <summary>
        /// Updates the value for the VhmSession object in session state.
        /// </summary>
        private void LoadVlifeSSOAsertionValue(string sponsorId)
        {
            //var member = VirginLifecare.Client.CRM.Member.GetMember(base.Session.SessionID, MemberId);
            var member = MemberBL.GetMemberById(_session.SessionID, _session.MemberID);
            string VHMclientid = Vhm.Web.Common.ApplicationSettings.VlifeServiceVHMclientid;

            if (ConfigurationFileReader.VlifeAeraSponsorIDs.Split(',').Contains(sponsorId) || ConfigurationFileReader.VlifeSponsorIDs.Split(',').Contains(sponsorId))
            {
                string sponsorOrgunitid = ConfigurationFileReader.VlifeServiceAeraGunitId;
                string sponsorKeyword = ConfigurationFileReader.VlifeServiceAeraKeyword;

                // Populate sso asertion with values from web config
                VlifeSSOAsertionValues = new Dictionary<VlifeSSOAsertion, string>
                    {
                        //config
                        {VlifeSSOAsertion.serviceProviderID, ConfigurationFileReader.VlifeServiceProviderID},
                        {VlifeSSOAsertion.SSOname, member.MemberId.ToString()},
                        {VlifeSSOAsertion.serviceURL, ConfigurationFileReader.VlifeServiceURL},
                        //sponsor specific 

                        {VlifeSSOAsertion.orgunitid, sponsorOrgunitid},
                        {VlifeSSOAsertion.clientid, VHMclientid},
                        {VlifeSSOAsertion.keyword, sponsorKeyword},

                        {VlifeSSOAsertion.externalcustomerid, member.MemberId.ToString()},
                        {VlifeSSOAsertion.sponsorid, _session.PrimarySponsorID.ToString()},
                        {VlifeSSOAsertion.firstname, member.FirstName},
                        {VlifeSSOAsertion.lastname, member.LastName},
                        {VlifeSSOAsertion.gender, member.Gender},
                        {VlifeSSOAsertion.dateOfbirth, new DateTime(1980, 1, 1).ToString()},
                        {VlifeSSOAsertion.email, member.EmailAddress},
                        {VlifeSSOAsertion.languagepreference, "en-us"},

                    };
            }
        }
        

    }
}
