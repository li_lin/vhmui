'use strict';

function setTheme(rootScope, scope) {
    rootScope.Theme = scope.Theme = {
        Class: 'Pul'
    };
}

function HomeCtrl($scope, $rootScope, $http, $cookies, Invitations, Email) {

    if (!$cookies.SessionID || $cookies.SessionID == "") {
        window.location = '/login.aspx';
        return;
    }

    $scope.Messages = {
        Accepted: 'Invitation Accepted',
        Pending: 'Invitation Pending',
        CancelledPending: 'Invitation Cancellation Pending',
        Cancelled: 'Invitation Cancelled'
    };
    
    setTheme($rootScope, $scope);
    
    var sessionId = '';
    $rootScope.SessionId = '';
    
    $.ajax({
        url: '/v2/Member/GetSessionId',
        cache: false,
        success: function(data) {
            
            sessionId = data.replace(/"/g, '');
            sessionId = sessionId.replace(/"/g, '');

            $rootScope.SessionId = sessionId;

            $scope.$apply(function() {

                Invitations.get({ ticket: $rootScope.SessionId, status: 'All' },
                    function(dto) {

                        setUpModel(dto);
                        $scope.Waiting = false;
                        
                    }, function(response) {

                        if (response.status == 400) { // express members can't be here
                            window.location.href = '/v2/member/landingpage';
                            return;
                        }
                        $scope.ErrorMessage = 'There was a problem getting invitations';
                        $scope.Waiting = false;
                        $scope.Error = true;
                    });
            });
            return true;
        },
        error: function (error) {
            $scope.$apply(function() {
                $scope.ErrorMessage = 'There was a problem getting invitations';
                $scope.Waiting = false;
                $scope.Error = true;
            });
        }
    });
    
    $scope.Waiting = true;
    $scope.Error = false;

    $scope.DismissError = function() {
        $scope.Error = false;
    };

    $scope.SendInvite = function() {

        if ($scope.model.InvitesRemaining <= 0 || $scope.model.EmailToSend.length == 0) {
            return false;
        }

        if ($scope.model.EmailToSend.indexOf('@') < 0 || $scope.model.EmailToSend.indexOf('.') < 0) { // not too picky about this
            $scope.ErrorMessage = 'Please enter a valid email address.';
            $scope.Error = true;
            return false;
        }
        
        $scope.Waiting = true;
        $scope.Error = false;

        Email.post({ ticket: $rootScope.SessionId, to: $scope.model.EmailToSend }, function() {
            
            var newInvitee = {
                InviteId: 0,
                Img: $scope.model.UnknownUser,
                Name: $scope.model.EmailToSend,
                InvitationMessage: $scope.Messages.Pending,
                Status: 'Pending',
                ShowResend: true,
                MemberId: $scope.model.Member.MemberID,
                RelationId: null
            };
            $scope.model.Invitees.push(newInvitee);
            $scope.model.InvitesRemaining = $scope.model.MaxInvites - $scope.model.Invitees.length;
            $scope.model.EmailToSend = '';
            $scope.ErrorMessage = '';
            $scope.Waiting = false;
        }, function(response) {
            
            $scope.Waiting = false;
            if (response.status == 400) {                
                $scope.ErrorMessage = $scope.model.EmailToSend + ' cannot be invited. Either the maximum number of invites has been sent or this person has already been invited.';                
                $scope.model.EmailToSend = '';
            } else {
                $scope.ErrorMessage = 'There was a problem sending this invitation.';
            }
            $scope.Error = true;
        });
        return true;
    };

    $scope.RemoveInvitee = function (memberId, relationId, inviteId) {
        
        $scope.Waiting = true;
        $scope.Error = false;
       
        Invitations.remove({ ticket: $cookies.SessionID, inviteId: inviteId }, function () {
            $scope.Waiting = false;
            for (var i = 0; i < $scope.model.Invitees.length; i++) {
                if ($scope.model.Invitees[i].InviteId == inviteId) {
                    
                    if ($scope.model.Invitees[i].RelationId == null) {
                        $scope.model.Invitees.splice(i, 1);
                    } else {
                        $scope.model.Invitees[i].InvitationMessage = $scope.Messages.CancelledPending;
                    }
                    
                    $scope.model.InvitesRemaining = $scope.model.InvitesRemaining + 1;
                    break;
                }
            }
            
        }, function () {
            $scope.Waiting = false;
            $scope.ErrorMessage = 'There was a problem removing this person';
            $scope.Error = true;
        });
    };

    $scope.ResendInvitation = function (invteeId) {
        
        $scope.Waiting = true;
        $scope.Error = false;
        
        Email.update({ ticket: $rootScope.SessionId, inviteId: invteeId }, function () {
            $scope.Waiting = false;
            $scope.ErrorMessage = '';
            $scope.Error = false;
        }, function () {
            $scope.Waiting = false;
            $scope.ErrorMessage = 'There was a problem resending this invitation.';
            $scope.Error = true;
        });
    };
    var setUpModel = function (data) {
        
        var _model = JSON.parse(data);
        if (_model.length > 0) {
            _model = JSON.parse(_model);
        }
        var model = {};
        
        model.Under21 = null;
        model.EmailToSend = '';
        model.UnknownUser = _model.CDNPath + 'ce64b453-f1a1-4f91-b450-20b7c11de5c0.gif';
        model.MaxInvites = 3;

        model.Member = {
            MemberId: _model.MemberId,
            Email: _model.MemberEmail
        };
        model.Invitees = [];

        if (_model.Invitations) {

            for (var i = 0; i < _model.Invitations.length; i++) {

                var messageDisplay = getInvitationStatusMessage(_model.Invitations[i].InviteStatus);
                
                model.Invitees.push({
                    InviteId: _model.Invitations[i].InviteId,
                    Img: _model.Invitations[i].ProfilePicture.length > 0 ? _model.CDNPath + _model.Invitations[i].ProfilePicture + '.jpg' : model.UnknownUser,
                    Name: _model.Invitations[i].Name,
                    InvitationMessage: messageDisplay,
                    Status: 'Active',
                    ShowResend: _model.Invitations[i].InviteStatus == 2,
                    MemberId: model.Member.MemberId,
                    RelationId: _model.Invitations[i].RelationId
                });
            }
        }
        
        model.InvitesRemaining = model.MaxInvites - model.Invitees.length;

        $scope.model = model;
        
        
    };
    function getInvitationStatusMessage(status) {
        
        var message = '';
        switch (status) {
            case 1:
                {
                    message = $scope.Messages.Accepted; break;
                }
            case 2:
                {
                    message = $scope.Messages.Pending; break;
                }
            case 5:
                {
                    message = $scope.Messages.Cancelled; break;
                }
            case 6:
                {
                    message = $scope.Messages.CancelledPending; break;
                }
        }
        return message;
    }
}
HomeCtrl.$inject = ['$scope', '$rootScope', '$http', '$cookies', 'Invitations', 'Email'];




