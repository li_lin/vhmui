﻿'use strict';

/* Directives */


angular.module('FaF.directives', []).
    directive('appVersion', ['version', function(version) {
        return function(scope, elm, attrs) {
            elm.text(version);
        };
    }]).
    directive('nav', function () {
        return {
            restrict: 'E',
            controller: NavigationCtrl,
            replace: true,
            scope: true, // give each instance of the nav link its own scope
            template: '<h2><a class="slideNav {{Class}}" href="#/{{Page}}">{{Text}}</a></h2>',
            link: function (scope, elem, attrs) {
                scope.Page = elem.attr('target');
                scope.Text = elem.attr('text');
                scope.Class = elem.attr('class');
            }
        };
    });
    
