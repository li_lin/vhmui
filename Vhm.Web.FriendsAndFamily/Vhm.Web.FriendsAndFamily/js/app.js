'use strict';

var page = 0;


// Declare app level module which depends on filters, and services
angular.module('FaF', ['FaF.filters', 'FaF.services', 'FaF.directives', 'ngCookies', 'ngSanitize']).
    config(['$routeProvider', '$httpProvider', function ($routeProvider, $httpProvider, $http) {
        $routeProvider.when('/home', { templateUrl: 'partials/home.html', controller: HomeCtrl });
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
        $routeProvider.otherwise({ redirectTo: '/home' });
    }]);
