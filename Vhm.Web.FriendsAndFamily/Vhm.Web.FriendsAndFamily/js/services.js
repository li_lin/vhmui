'use strict';

var services = angular.module('FaF.services', ['ngResource']).
    factory('Invitations', function ($resource) {
        return $resource('/FFApi/api/Invitations/', {ticket: '', status: ''}, {
            query: { method: 'GET', params: { ticket: '', status: '' }, cache: false, isArray: true, responseType: 'json' },
            get: { method: 'GET', params: { ticket: '', status: '' }, cache: false },
            post: { method: 'POST' },
            update: { method: 'PUT' },
            remove: { method: 'DELETE' }
        });
    }).
    factory('Email', function ($resource) {
        return $resource('/FFApi/api/Email', { ticket: '@ticket', to: '@to', inviteId: '' }, {
            get: { method: 'GET', cache: false },
            post: { method: 'POST', params: { ticket: '@ticket', to: '@to' } },
            update: { method: 'PUT', params: { ticket: '@ticket', inviteId: '@inviteId' } },
            remove: { method: 'DELETE' }
        });
    });