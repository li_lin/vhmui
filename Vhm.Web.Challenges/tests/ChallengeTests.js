﻿module.exports = {
    'I can log in': function( test ) {
        test
            .open( 'https://local.virginhealthmiles.com/login.aspx' )
            .assert.url().is( 'https://local.virginhealthmiles.com/login.aspx', 'At the login page' )
            .type( '#oUserID', '0203790001' )
            .assert.val( '#oUserID', '0203790001', 'text has been set' )
            .type( '#txtPlainPassword', 'a1111111', 'hidden password name is' )
            .assert.val( '#txtPlainPassword', 'a1111111', 'password has been set' )
            .click( '#oLogon' )
            .open( 'https://local.virginhealthmiles.com/v2/member/landingpage' )
            .assert.url().is( 'https://local.virginhealthmiles.com/v2/member/landingpage', 'at the landing page' )
            .done();
    },
    'I can select a personal tracking challenge from the select page and create a challenge': function (test) {
        test
            .open('https://local.virginhealthmiles.com/challenge/select')
                .assert.url().is('https://local.virginhealthmiles.com/challenge/select', 'At the challenge select page')
            .waitForElement('ul.iconList:last-child li:nth-child(5)', 8000)
                .assert.text('ul.iconList:last-child li:nth-child(5) a', 'Take an exercise class', 'Options are loaded')
            .click('ul.iconList:last-child li:nth-child(5) a')
            .assert.url().is('https://local.virginhealthmiles.com/challenge/create/6c5b9fac-5c68-4c8c-b711-ca0c7e45ca7a', 'at the challenge page')
            .waitForElement('.challengeBadge', 10000)
                .assert.exists('#btnOnlyForMe', 'submit button exists')
            .assert.text('#btnOnlyForMe', 'This is just for me', 'submit button text is correct')
            .click('#btnOnlyForMe')
                .assert.exists('.checkbox', 'days to track exist...')
                .assert.numberOfElements('.checkbox', 7, '...and we have 7 days to track')
        //.assert.url().is('https://local.virginhealthmiles.com/challenge', 'at the challenge page')
            .done();
    }
};