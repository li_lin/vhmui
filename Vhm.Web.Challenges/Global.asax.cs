﻿using System;
using System.Globalization;
using System.IO;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Xml;
using Vhm.Web.Challenges.App_Start;
using Vhm.Web.Models.ViewModels.Challenges;

namespace Vhm.Web.Challenges
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            
            //AuthConfig.RegisterAuth();
            BL.AutoMapperConfig.Intialize();

            string configFilePath = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "App_Data", "ChallengeSetup.xml");
            var configDoc = new XmlDocument();
            configDoc.Load(configFilePath);
            ChallengeSetup.Load(configDoc.InnerXml);

        }

        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            const int TWO_MINUTES = 120;
            string refreshInterval =
                (FormsAuthentication.Timeout.TotalSeconds + TWO_MINUTES).ToString(CultureInfo.InvariantCulture);
            Response.AddHeader("Refresh", refreshInterval);

        }
    }
}