﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Newtonsoft.Json;
using Vhm.Providers;
using Vhm.Web.BL;
using Vhm.Web.Models;
using Vhm.Web.Common;
using Vhm.Web.Models.ContentEnums;
using Vhm.Web.Models.RepositoryModels;
using Vhm.Web.Models.ViewModels.Challenges;

namespace Vhm.Web.Challenges.api
{
    
    public class Challenge : ApiController
    {

        private readonly Session _session = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.VHMWebSessionObject) as Session;

        // GET api/<controller>
        public IEnumerable<string> GetPersonalTrackingChallenge()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            var challengeMember = ChallengeBL.GetChallengeMember(_session.MemberID, id);
            if (challengeMember == null)
            {
                return JsonConvert.SerializeObject("");
            }

            #region Challenge Details

            var challenge = new PersonalTrackingChallenge();
            try
            {
                challenge = ChallengeBL.GetChallenge(id);

            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(new Exception("Error getting challenge details", ex));
            }
            #endregion

            #region Chat
            var chat = new List<ChallengeMessage>();
            try
            {
                chat = ChallengeBL.GetChallengeMessages(id);
            }
            catch (Exception ex)
            {

                VirginLifecare.Exception.ExceptionHandler.ProcessException(new Exception("Error getting chat", ex));
            }
            #endregion

            #region LeaderBoard

            var leaders = new List<ChallengeMemberScore>();
            try
            {
                leaders = ChallengeBL.GetChallengeLeaders(id, 0,
                                                           ApplicationSettings.ChallengeLeaderBoardPageSize);
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(new Exception("Error getting leader board", ex));
            }
            #endregion

            #region Viewer Name
            var viewerName = string.Empty;
            try
            {
                var member = MemberBL.GetMemberById(_session.SessionID, _session.MemberID);
                if (member != null)
                {
                    viewerName = member.DisplayName;
                }
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(new Exception("Error getting viewer name", ex));
            }
            #endregion

            #region Content

            Dictionary<string, string> stringContentDictionary;
            try
            {
                var content = MemberBL.GetContentLabels<RankingsContent>(PageNames.RankingsPage, _session.LanguageID);
                stringContentDictionary = content.ToDictionary(item => item.Key.ToString(), item => item.Value);
            }
            catch (Exception ex)
            {
                stringContentDictionary = new Dictionary<string, string>();
                VirginLifecare.Exception.ExceptionHandler.ProcessException(new Exception("Error getting challenge content", ex));
            }
            #endregion

            #region Tracked Days

            var days = new ChallengeScoreInfo();
            // initialize an anonomous structure
            var trackedDays = from cs in new List<ChallengeScore>()
                              select new {
                                  Day = string.Empty,
                                  Answer = bool.FalseString,
                                  MemberId = new long(),
                                  ChallengeId = new long(),
                                  ChallengeScoreId = new long(),
                                  ScoreDate = DateTime.Now,
                                  Value = new decimal?(),
                                  Score = new decimal(),
                                  IsMemberAnswer = false
                              };
            try
            {
                days = ChallengeBL.GetTrackedDays(challenge.StartDate, challenge.EndDate.GetValueOrDefault(), id, _session.MemberID);
                trackedDays = from cs in days.ChallengeScores
                              select new {
                                  Day = cs.ScoreDate.ToShortDateString(),
                                  Answer = cs.Value.ToString(),
                                  cs.MemberId,
                                  cs.ChallengeId,
                                  ChallengeScoreId = cs.ChallengeScoreID,
                                  cs.ScoreDate,
                                  cs.Value,
                                  cs.Score,
                                  IsMemberAnswer = cs.Value != null
                              };
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(new Exception("Error getting challenge tracked days", ex));
            }

            var trackedDaysWithYesCount = new {
                TrackedDays = trackedDays,
                DaysOfYes = days.ScoreCount
            };
            #endregion

            #region Member Profile Image

            var profileImage = string.Empty;
            try
            {
                var profile = SocialBL.GetMemberProfile(_session.SessionID, _session.MemberID, _session.MemberID, false);
                if (profile != null)
                {
                    profileImage = profile.Member.ImageUrl;
                }
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
            }
            #endregion

            var badge =
                    RewardsBL.GetBadge(
                        challenge.ChallengeQuestions[0].ChallengeTasks[0].ChallengeTaskRewards[0].BadgeID.GetValueOrDefault());

            var model = new PersonalKnowAndGoVM {
                Challenge = challenge,
                LeaderBoard = leaders,
                Chat = chat.Take(ApplicationSettings.ChallengeChatMessagesToDisplay).ToList(),
                ContentRankings = stringContentDictionary,
                TrackedDaysInfo = trackedDaysWithYesCount,
                Rank = challengeMember.Rank,
                ViewerName = viewerName,
                MemberImage = profileImage,
                BadgeFileName = ApplicationSettings.BadgeImageUrl + new Guid(badge.BadgeImage).ToString(),
                DaysToDisplay = ApplicationSettings.PersonalTrackingDaysToDisplay
            };

            return JsonConvert.SerializeObject(model);
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}