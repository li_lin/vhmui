﻿using System.Web.Optimization;

namespace Vhm.Web.Challenges.App_Start
{
    public static class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region Script Bundles
            
            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                        "~/Scripts/jquery-{version}.js",
                        //"~/Scripts/modernizr-*",
                        "~/Scripts/jquery-migrate-1.2.1.js",
                        "~/Scripts/jquery-ui-{version}.js",
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/knockout-{version}.js",
                        "~/Scripts/bootstrap.js",
                        "~/Scripts/challenges.js"));

           
            bundles.Add(new ScriptBundle("~/bundles/create").Include(
                "~/Scripts/create.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/invite").Include(                
                "~/Scripts/invite.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/select").Include(
                "~/Scripts/select.js"
                ));
			bundles.Add(new ScriptBundle("~/bundles/join").Include(
				"~/Scripts/join.js"
				));
            #endregion

            #region Style bundles
            
            bundles.Add(new StyleBundle("~/Content/site").Include(
                "~/Content/bootstrap.css", 
                "~/Content/bootstrap-responsive.css",
                "~/Content/styles/challenge.css"                
                ));

            #endregion

        }
    }
}