﻿using System.Web.Mvc;
using StackExchange.Profiling.MVCHelpers;

namespace Vhm.Web.Challenges.App_Start
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new ProfilingActionFilter());
        }
    }
}