﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Vhm.Web.Challenges.App_Start
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "select",
                url: "select",
                defaults: new { controller = "Home", action = "Select" }
            );
            routes.MapRoute(
                name: "create",
                url: "create/{id}",
                defaults: new { controller = "Home", action = "Create", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "invite",
                url: "invite/{id}",
                defaults: new { controller = "Home", action = "Invite", id = UrlParameter.Optional }
            );
			routes.MapRoute(
				name: "join",
				url: "join/{id}",
				defaults: new { controller = "Home", action = "Join", id = UrlParameter.Optional }
			);
			routes.MapRoute(
                name: "pkag",
                url: "{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            
        }
    }
}