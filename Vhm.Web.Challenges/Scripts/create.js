﻿
var ModelError = function(error) {
    var self = this;
    self.Description = error;
};

var CreateChallengeModel = function() {
    
    var self = this;

    self.ShowWarnings = true; // set to false when unit testing
    
    self.Content = ko.observable({});
       
    self.Target = ko.observable('');
    self.Question = ko.observable('');
    self.AnswerType = ko.observableArray([]);
    self.MoreLess = ko.observableArray([]); //'more', 'less'
    self.Duration = ko.observableArray([]); //'oneDay', 'oneWeek', 'twoWeek', 'oneMonth'
    self.Details = ko.observable('');
    self.Badges = ko.observableArray([]);
    self.BadgeId = ko.observable(-1);
    self.ChallengeId = ko.observable(-1);
    self.CDNPath = ko.observable('');
    
    self.ValidationErrors = ko.observableArray([]);

    self.Ready = ko.observable(false);
    self.LoadError = ko.observable(false);
    
    self.ClearTarget = function() {
        self.Target('');
        self.MoreLess( [] );
    };


    var getCategoryKeyFromUrl = function () {
        var href = window.location.href;
        var keyIdx = href.lastIndexOf('/') + 1;
        var catKey = href.substring(keyIdx);
        var GUID_LENGTH = 36;

        return catKey.length == GUID_LENGTH ? catKey : '';
    };
    
    self.CategoryKey = getCategoryKeyFromUrl();
    
    self.IsValid = ko.observable( true );
    self.CheckIsValid = function() {
        var warnings = new Array();
        
        if ( self.Question().length == 0 ) {
            warnings.push( new ModelError( 'Question is required' ) );
        }
        if ( self.AnswerType().length == 0 ) {
            warnings.push( new ModelError( 'Answer Type is required' ) );
        }
        if (self.AnswerType() == 'NUM' && (self.MoreLess().length == 0 || self.Target() <= 0)) {
            warnings.push(new ModelError('Target is required'));
        }
        if ( self.Duration().length == 0 ) {
            warnings.push( new ModelError( 'Duration is required' ) );
        }
        if (self.BadgeId() < 0) {
            warnings.push( new ModelError( 'Badge is required' ) );
        }
        if (self.Details().length == 0) {
            warnings.push( new ModelError( 'Details are required' ) );
        }
        
        self.ValidationErrors( warnings ); // update the new list of warnings

        self.IsValid( warnings.length == 0 );
        return self.IsValid();
    };
    
    self.HasAnswerType = ko.computed(function () {
        return self.AnswerType().length > 0;
    } );
    self.HasDuration = ko.computed(function () {
        return self.Duration().length > 0;
    });
    self.HasMoreLess = ko.computed(function () {
        return self.MoreLess().length > 0;
    });
    self.SetSelectedBadge = function (el) {
        self.BadgeId( el.BadgeID );
    };
    self.SaveChallenge = function () {
        
        if ( self.CheckIsValid() ) {
            save( navToMyChallenges );
        } else {
            self.IsValid(false);
        }
    };
    
    var save = function (callback) {

        var promptType = getPromptType();
        var scoreType = getScoreType();
        var data = {
            name: self.Question(), description: self.Details(), question: self.Question(), displayType: 'STR', 
            frequency: 1, frequencyDatePart: self.Duration(), target: self.Target(), targetType: self.MoreLess(), answType: self.AnswerType(),
            scoreType: scoreType, badgeId: self.BadgeId(), promptType: promptType, category: 'NUT', type: 'TRK', createType: 'NONE',
            ownerType: 'PER', partType: 'PERSONAL_INVITATION'
        };

        $.ajax({
            url: '/Challenge/PersonalTracking/Save',
            data: data,
            cache: false,
            success: function (id) {
                self.ChallengeId(id);
                callback();
            },
            error: function () {
                console.log($("#hdnErrorSavingChallenge").val());
            }
        });
    };

    var getPromptType = function() {
        return (self.Target() != null && self.Target() > 0) ? 'TXT' : 'YNO';
    };
    var getScoreType = function () {
        return (self.Target() != null && self.Target() > 0) ? 'CAK' : 'ENT';
    };
    var navToMyChallenges = function() {
        window.location.href = '/challenge/' + self.ChallengeId();
    };
    
    var navToInvite = function() {
        var id = self.ChallengeId();
        if (id > 0) {
            window.location.href = '/challenge/invite/' + id;
        }
    };
    self.SaveAndSendInvites = function () {

        if (self.CheckIsValid()) {
            save( navToInvite );
        } else {
            self.IsValid( false );
        }

    };

    self.OnModelReady = function( ) {
        return true;
    };
    
    self.GetModel = function() {
        
        $.ajax( {
            url: '/challenge/PersonalTracking/GetCreateChallengeModel',
            data: { contentLabel: 'PersonalKnowAndGo', categoryKey: self.CategoryKey },
            cache: true,
            success: function (c) {
                
                self.Content(c.Content);
                
                for (var i = 0; i < c.Badges.length; i++) {
                    c.Badges[i].BadgeImageString = c.CDNPath + c.Badges[i].BadgeImageString;
                }
                
                self.Badges(c.Badges);
                self.BadgeId(-1);
                self.CDNPath(c.CDNPath);
                
                self.ValidationErrors([]);

                if ( c.ChallengeConfig != null ) {
                    self.AnswerType( c.ChallengeConfig.AnswerType );
                    self.Duration( c.ChallengeConfig.Duration );
                    self.MoreLess( c.ChallengeConfig.MoreLess );
                    self.Target( c.ChallengeConfig.Target );
                    self.Question( c.ChallengeConfig.Question );
                    self.BadgeId( c.ChallengeConfig.BadgeId );
                    self.Details( c.ChallengeConfig.Description );
                }
                self.Ready( true );
                self.OnModelReady();
                
            },
            error: function () {
                self.LoadError( true );
            }
        } );
            
    };
    
    
    
    self.GetModel();
    
};

function onKeyPressOnlyNum(e) {
    e = e || window.event;
    var keyunicode = e.charCode || e.keyCode;
    return keyunicode >= 48 && keyunicode <= 57 || keyunicode == 8;
};