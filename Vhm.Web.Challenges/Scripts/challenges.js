﻿
var CURRENT_YES_NO_ANSWER = '';

var ModelError = function (error) {
    var self = this;
    self.Description = error;
};
String.prototype.padLeft = function( desiredLength, padCharacter ) {

    var _char = '';
    for (var i = 0; i < desiredLength; i++) {
        _char += padCharacter;
    }

    return (_char + this).slice(desiredLength * -1);
};
Number.prototype.toOrdinal = function() {
    var n = this % 100;
    var suffix = ['th', 'st', 'nd', 'rd', 'th'];
    var ord = n < 21 ? ( n < 4 ? suffix[n] : suffix[0] ) : ( n % 10 > 4 ? suffix[0] : suffix[n % 10] );

    if ( this == 0 ) {
        return '';
    } else {
        return this + ord;
    }
};

var ChallengeModel = function (id) {
    var self = this;
    var chats = [];
    var trackedDays = [];

    var challengeMembers = [];

    var trackedDaysCount = 0;
    var startDate = '';
    var endDate = '';
    var viewerName = '';

    self.ALLOWED_CHARS = 340;
    self.ShowErrors = true; //this is mainly for testing
    self.Post = ko.observable('');
    self.Messages = ko.observableArray(chats);
    self.TrackedDays = ko.observableArray(trackedDays);
    self.DisplayMin = ko.observable(0);
    self.DisplayMax = ko.observable(7);
    self.FrequencyType = ko.observable('Daily');
    self.ChallengeMembers = ko.observableArray(challengeMembers);
    self.ViewerName = ko.observable(viewerName);
    self.BadgeDescription = ko.observable('');
    self.BadgeID = ko.observable(-1);
    self.AnswerType = ko.observable('');
    self.Target = ko.observable('');
    self.UploadDeadline = ko.observable('');
    self.MoreLess = ko.observable('');
    self.DaysToDisplay = ko.observable(0);
    self.UploadDeadlinePassed = ko.observable('');
    self.LeaderBoardEnabled = ko.observable();
    self.ChatEnabled = ko.observable();

    self.Ready = ko.observable(false);
    self.LoadError = ko.observable(false);

    // get challengeId from url
    self.ChallengeId = id;
    self.IsChallengeCreator = ko.observable(false);
    self.IsChallengeRunning = ko.observable(true);

    self.Today = ko.computed(function () {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        today = mm + '/' + dd + '/' + yyyy;
        return today;
    }, this);

    self.Challenge = ko.observable({});

    self.MemberImage = ko.observable('');
    self.Content = ko.observable({});
    self.ChallengeQuestion = ko.observable('');
    self.TrackedDaysCount = ko.observable(trackedDaysCount);
    self.Rank = ko.observable(0);
    // leader board page size attributes
    self.PageSizes = ko.observableArray([5, 15, 30, 50, 100]);
    self.SelectedPageSize = ko.observable(self.PageSizes()[0]);

    self.StartDate = ko.observable(startDate);
    self.EndDate = ko.observable(endDate);
    self.ValidationErrors = ko.observableArray([]);
    self.BadgeFileName = ko.observable();
    self.OwnerType = ko.observable( '' );
    self.PostLength = ko.computed(function () {
        var len = self.ALLOWED_CHARS - self.Post().length;
        return len;
    }, this);

    self.AllowTyping = function () {
        return self.PostLength() > 0;
    };
    self.InspriationalMessage = function () {

        try {

            var inspirationalMessages = ['Way to go!', 'Good job!', 'Keep it up!', 'Woo hoo!', 'Nice one!', 'Hooray!', 'High 5!', 'Awesome!'];
            var random = Math.floor((Math.random() * inspirationalMessages.length - 1) + 1);
            return inspirationalMessages[random];
        } catch (e) {
            return '&nbsp;';
        }
    };

    self.ShowChat = ko.computed( function() {
        return self.ChatEnabled();
    });

    self.ShowLeaderBoard = ko.computed(function () {
        return self.LeaderBoardEnabled() && self.ChallengeMembers().length > 1;
    });

    /*ADD CHAT MESSAGE*/
    self.addMessage = function () {

        var message = self.Post();
        if (message.length > 0) {
            self.Messages.unshift({ NickName: self.ViewerName(), ImgSrc: self.MemberImage, FilteredMessageText: message, CreatedDateString: self.Today() });

            $.ajax({
                url: '/challenge/PersonalTracking/PostChatMessage',
                data: { challengeId: self.ChallengeId, nickName: self.ViewerName(), message: message },
                cache: false,
                success: function () {
                    self.Post('');
                },
                error: function () {
                    if (self.ShowErrors) {
                        console.log("uh oh, couldn't post your message");
                    }
                }
            });
        }
    };

    self.LoadLeaderBoard = function () {
        loadLeaderBoard();
    };

    self.GetModel = function () {
        loadChallenge();
    };

    self.UseGreaterThan = ko.observable(true);

    self.ClearErrors = function () {
        self.ValidationErrors([]);
    };

    self.ScrollLeft = function () {
        var min = self.DisplayMin();

        var max = self.DisplayMax();
        if (min > 0) {
            min--;
            max--;
            self.DisplayMin(min);
            self.DisplayMax(max);
        }
    };
    self.ScrollRight = function () {
        var min = self.DisplayMin();
        var max = self.DisplayMax();
        if (min < self.TrackedDays().length) {
            min++;
            max++;
            self.DisplayMin(min);
            self.DisplayMax(max);
        }
    };
    var parseDate = function (jsonDate, pattern) {
        var d = jsonDate;
        var dtString = '';
        var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];


        if (d.substring(0, 6) == "/Date(") {
            var dt = new Date(parseInt(d.substring(6, d.length - 2)));
            switch (pattern) {
                case 'DDD MM/DD':
                    {
                        dtString = dt.getDay() + ' ' + dt.getMonth() + "/" + dt.getDate();
                        break;
                    }
                case 'MMM/dd':
                    {
                        dtString = months[dt.getMonth()] + " " + dt.getDate();
                        break;
                    }
                case 'MMM/dd/yyyy':
                    {
                        dtString = months[dt.getMonth()] + " " + dt.getDate() + ", " + dt.getFullYear();
                        break;
                    }
                default:
                    {
                        dtString = (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear();
                        break;
                    }
            }
        }
        return dtString;
    };

    /*LOAD CHALLENGE*/
    var loadChallenge = function () {

        $.ajax({
            url: '/challenge/PersonalTracking/GetChallenge',
            data: { challengeId: self.ChallengeId },
            cache: false,
            success: function (c) {

                self.Challenge(c.Challenge);

                self.ChallengeQuestion(c.Challenge.ChallengeQuestions[0].Question);

                self.LeaderBoardEnabled(c.Challenge.ChallengeQuestions[0].LeaderBoardEnabled);
                self.ChatEnabled(c.Challenge.ChallengeQuestions[0].ChatEnabled);

                startDate = parseDate(c.Challenge.StartDate, 'MM/dd/yyyy');
                self.StartDate(startDate);

                endDate = parseDate(c.Challenge.EndDate, 'MM/dd/yyyy');                
                self.EndDate(endDate);

                self.AnswerType(c.Challenge.ChallengeQuestions[0].AnswerType);
                self.Messages(c.Chat);

                for (var i = 0; i < c.LeaderBoard.length; i++) {
                    c.LeaderBoard[i].Rank = c.LeaderBoard[i].Rank.toOrdinal();
                }

                self.ChallengeMembers(c.LeaderBoard);
                self.Content(c.ContentRankings);
                self.ViewerName(c.ViewerName);
                self.BadgeFileName(c.BadgeFileName + '.png');
                self.DaysToDisplay(c.DaysToDisplay);

                self.FrequencyType(c.FrequencyType);
                self.OwnerType(c.Challenge.OwnerType);
                
                var min = 0;
                var max = 0;
                if (c.Challenge.ChallengeQuestions[0].ChallengeAnswerKeys != null && c.Challenge.ChallengeQuestions[0].ChallengeAnswerKeys.length > 0) {

                    min = c.Challenge.ChallengeQuestions[0].ChallengeAnswerKeys[0].EntryMin;
                    max = c.Challenge.ChallengeQuestions[0].ChallengeAnswerKeys[0].EntryMax;

                    var target = min > 0 ? min : max;

                    self.UseGreaterThan(min > 0);

                    self.Target(target);
                }

                self.UploadDeadline(parseDate(c.Challenge.UploadDeadline));

                var days = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
                if (c.FrequencyType == 'Once') { // just get the first day

                    var firstDay = c.TrackedDaysInfo.TrackedDays[0];
                    var answer = parseInt(firstDay.Answer);
                    answer = (answer.toString() == 'NaN') ? '' : answer;

                    var day = new Date(firstDay.Day);
                    var dayDisplay = days[day.getDay()] + ' ' + (day.getMonth() + 1) + '/' + day.getDate();

                    var d = new self.TrackedDay(self.AnswerType, answer, self.Target(), dayDisplay, day, firstDay.ChallengeScoreId,
                        firstDay.ChallengeId, firstDay.IsMemberAnswer, self.UseGreaterThan(), min, max);
                    d.Index = 0; // index to keep track of what's in the visible display
                    trackedDays.push(d);
                } else {

                    var cnt = 0;
                    for (var i = 0; i < c.TrackedDaysInfo.TrackedDays.length; i++) {

                        var thisDay = c.TrackedDaysInfo.TrackedDays[i];
                        var answer = parseInt(thisDay.Answer);
                        answer = (answer.toString() == 'NaN') ? '' : answer;

                        var day = new Date(thisDay.Day);
                        var dayDisplay = '';
                        if (c.FrequencyType == 'Weekly') {
                            dayDisplay = 'Week of ' + (day.getMonth() + 1) + '/' + day.getDate();
                        } else {
                            dayDisplay = days[day.getDay()] + ' ' + (day.getMonth() + 1) + '/' + day.getDate();
                        }

                        var d = new self.TrackedDay(self.AnswerType, answer, self.Target(), dayDisplay, day, thisDay.ChallengeScoreId,
                            thisDay.ChallengeId, thisDay.IsMemberAnswer, self.UseGreaterThan(), min, max);
                        d.Index = cnt; // index to keep track of what's in the visible display
                        trackedDays.push(d);

                        if (c.FrequencyType == 'Weekly') {
                            i += 6;
                        }
                        cnt++;
                    }
                }
                self.TrackedDays(trackedDays);

                trackedDaysCount = c.TrackedDaysInfo.DaysOfYes;
                self.TrackedDaysCount(trackedDaysCount);
                self.Rank(c.Rank.toOrdinal());

                self.MemberImage(c.MemberImage);
                self.BadgeDescription();
                var badgeId = c.Challenge.ChallengeQuestions[0].ChallengeTasks.length > 0
                    ? c.Challenge.ChallengeQuestions[0].ChallengeTasks[0].ChallengeTaskRewards[0].BadgeID
                    : null;
                self.BadgeID(badgeId);

                self.IsChallengeCreator(c.IsChallengeCreator);

                var endDateCheck = new Date(self.EndDate());
                endDateCheck.setHours(23);
                endDateCheck.setMinutes(59);
                endDateCheck.setSeconds( 59 );
                self.IsChallengeRunning(new Date() <= endDateCheck);

                if (trackedDays.length > c.DaysToDisplay) {
                    scrollToToday(trackedDays);
                }

                var uploadDeadlineDateObject = new Date( self.UploadDeadline() );
                var todayDateObject = new Date(self.Today());
                
                self.UploadDeadlinePassed( uploadDeadlineDateObject < todayDateObject );
                
                self.Ready(true);
            }, error: function (xhr, status, error) {
                if (xhr.status == 404) {
                    window.location.href = '/secure/challenge/mypersonalchallenges.aspx';
                    return;
                }
                self.LoadError(true);
            }
        });
    };

    function scrollToToday(dayArray) {

        // Scroll the days display such that the field for 'today' is always in view.
        // We're assuming that the display always shows up to (and no more than) 7 days.        
        var today = new Date().toLocaleDateString();
        var lastDay = dayArray[dayArray.length - 1].DayValue.toLocaleDateString();
        var firstDayOfLastWeek = dayArray.length > 7 ? dayArray[dayArray.length - 1 - 7].DayValue.toLocaleDateString() : lastDay;
        var dtfirstDayOfLastWeek = new Date(firstDayOfLastWeek);

        for (var idx = 0; idx < dayArray.length; idx++) {

            var thisDay = dayArray[idx].DayValue.toLocaleDateString();
            // stop scrolling if we're viewing the last week. need a real date to do comparison.
            if (today == thisDay || dayArray[idx].DayValue > dtfirstDayOfLastWeek) {
                break;
            }

            self.ScrollRight();
        }
    }

    /*LOAD LEADER BOARD*/
    var loadLeaderBoard = function () {

        $.ajax({
            url: '/challenge/PersonalTracking/GetLeaderBoard',
            data: { challengeId: self.ChallengeId, pageNumber: 0, pageSize: self.SelectedPageSize() },
            cache: false,
            success: function (members) {
                for (var i = 0; i < members.length; i++) {
                    members[i].Rank = members[i].Rank.toOrdinal();
                }
                self.ChallengeMembers(members);
            }
        });
    };

    /* Custom biding for selecting current Date, or last if challenge finished*/
    ko.bindingHandlers.selectCurrentDate = {
        init: function (element, valueAccessor, allBindingsAccessor, data) {
            $(element.parentNode).hover(
            function () {
                $(element).parent().find('.icon-pencil').addClass('show-pencil-icon');
             }, function () {
                $(element).parent().find('.icon-pencil').removeClass('show-pencil-icon');
            }
            );
            
            var IsToday = valueAccessor();
            
            if (IsToday || self.FrequencyType() == 'Once') {
                var currentDateText = $( element ).parent().find( 'label.date-label' ).html();
                var rangeDiv = $(element).find('.rangeNumber');
                var answerDiv = $(element).find('.answer-display');
                var selectedDiv = $(element).find('.checkbox');
                $('#answer-NUM-input .date-of-answer-boxes').html(currentDateText);
                $('#answer-YNO-boxes .date-of-answer-boxes').html(currentDateText);

                 var pencilIcon = $(element).parent().find('.icon-pencil');
                pencilIcon.addClass("show-pencil-icon");

                var dateLabel = $(element).parent().find('.date-label');
                dateLabel.addClass("clicked");
                
                if (data.Answer() === 0) {
                    $(document).find('#NO-box').addClass('NO-box');
                    $(document).find('#YES-box').removeClass('YES-box');
                    $(document).find('#YES-box').addClass('YES-box-selected');

                } else if (data.Answer() === 1) {
                    $(document).find('#YES-box').addClass('YES-box');
                    $(document).find('#NO-box').removeClass('NO-box');
                    $(document).find('#NO-box').addClass('NO-box-selected');
                } else {

                }

                if (self.FrequencyType() == 'Once') {
                    $( element ).parent().addClass("selected-date-background");
                }

                if (selectedDiv[0] !== undefined)
                    $(selectedDiv[0]).addClass('selected-checkbox');
                if ( rangeDiv[0] !== undefined )
                    $(rangeDiv[0]).addClass('selected-input');
                if ( answerDiv[0] !== undefined )
                    $(answerDiv[0]).addClass('selected-answer');
            }

        }
    };

    /*TRACKED DAY MODEL*/
    self.TrackedDay = function (answerType, answer, target, dayDisplay, day, challengeScoreId, challengeId, isMemberAnswer, useGreaterThan, min, max) {

        var me = this;

        me.Answer = ko.observable(answer);
        me.Day = dayDisplay;
        me.DayValue = day;
        me.ChallengeScoreId = ko.observable(challengeScoreId);
        me.IsMemberAnswer = ko.observable(isMemberAnswer);
        me.ChallengeId = challengeId;
        me.AnswerType = answerType;
        me.Target = target;

        me.HitTarget = ko.computed(function () {
            var nAnswer = parseInt(me.Answer());

            if (me.AnswerType == 'YNO') {
                return me.Answer() == '1';
            } else {
                return (nAnswer >= min && nAnswer <= max);
            }

        }, this);

        var start = new Date(startDate);
        var today = new Date(self.Today());
        var clickedDay = new Date(me.DayValue);

        me.IsTodayFunc = function () {
            return (today.getDate() == day.getDate()) && (today.getMonth() == day.getMonth()) && (today.getFullYear() == day.getFullYear());
        };

        me.IsToday = me.IsTodayFunc();

     
        me.Validate = function (item, evt) {

            evt = evt || window.event;
            var charCode = evt.which || evt.keyCode;
            var charStr = String.fromCharCode(charCode);
            var CHAR_DELETE = 46;
            var CHAR_BACKSPACE = 8;
            var CHAR_TAB = 9;
            var CHAR_ENTER = 13;

            if (/\d/.test(charStr) == false && charCode != CHAR_DELETE && charCode != CHAR_BACKSPACE && charCode != CHAR_TAB && charCode != CHAR_ENTER) {
                var warnings = new Array();
                warnings.push(new ModelError("Please enter a valid answer"));
                self.ValidationErrors(warnings);

                return false;
            }
            return true;
        };

        function getEndOfWeek(d) {
            var endOfWeek = new Date(d.getTime() + (6 - d.getDay()) * 24 * 60 * 60 * 1000);
            endOfWeek.setHours(23);
            endOfWeek.setMinutes(59);
            endOfWeek.setSeconds(59);
            return endOfWeek;
        }

        me.IsEditable = function () {
            var uploadDeadline = new Date(self.UploadDeadline());
            var endOfWeek = getEndOfWeek(today);
            if (self.FrequencyType() == 'Weekly') {

                if (today < start || clickedDay <= uploadDeadline) {
                    return clickedDay <= endOfWeek;
                }
                return false;
            }

            return today >= start && clickedDay <= today && today <= uploadDeadline;
        };


        me.SaveAnswer = function () {
           
           switch (CURRENT_YES_NO_ANSWER) {
                    case 'YES':
                        me.Answer(0);
                        break;
                    case 'NO':
                        me.Answer(1);
                        break;
                }

                me.HitTarget = ko.computed(function () {
                    var nAnswer = parseInt(me.Answer());
                    if (me.AnswerType == 'YNO') {
                        return me.Answer() == '0';
                    } else {
                        return (nAnswer >= min && nAnswer <= max);
                    }

                }, this);
            
            var uploadDeadline = new Date(self.UploadDeadline());
            var myAnswer = me.Answer();

            if (self.FrequencyType() == 'Weekly') {
                var endOfWeek = getEndOfWeek(today);

                if (today < start || clickedDay > endOfWeek) {
                    me.Answer('');
                    if (self.ShowErrors) {
                        alert("Sorry, you can't enter a value for this day.");
                    }
                    return false;
                }

            } else {
                if (today < start || clickedDay > today) {
                    me.Answer('');
                  if (self.ShowErrors) {
                        alert("Sorry, you can't enter a value for this day.");
                    }
                    return false;
                }
            }

            if (today > uploadDeadline) {
                me.Answer('');
               if (self.ShowErrors) {
                    alert("Sorry, tracking for this challenge has ended.");
                }
                return false;
            }

        
            if (me.AnswerType() == 'YNO') {

                if (CURRENT_YES_NO_ANSWER == "YES") {
                    if (myAnswer.length == 0) {
                        me.Answer('0');
                    } else {
                        me.Answer('0');
                    }
                } else if (CURRENT_YES_NO_ANSWER == "NO") {
                    if (myAnswer.length == 0) {
                        me.Answer('1');
                    } else {
                        me.Answer('1');
                    }
                }

            }

            if (myAnswer.length == 0) {
                me.Answer('0');
            }

            var data = {
                challengeScoreId: me.ChallengeScoreId,
                challengeId: me.ChallengeId,
                scoreDate: (me.DayValue.getMonth() + 1) + '/' + me.DayValue.getDate() + '/' + me.DayValue.getFullYear(),
                answer: me.Answer(),
                score: me.HitTarget() ? 1 : 0,
                isMemberAnswer: me.IsMemberAnswer,
                frequencyType: self.FrequencyType()
            };

            saveAnswer(data);
            return true;
        };

        me.prepareSaveAnswerPost = function (data , event) {

            var answer = me.Answer();
            setSelectedDate(answer, event);
           
            var data = {
                challengeScoreId: me.ChallengeScoreId,
                challengeId: me.ChallengeId,
                scoreDate: (me.DayValue.getMonth() + 1) + '/' + me.DayValue.getDate() + '/' + me.DayValue.getFullYear(),
                answer: me.Answer(),
                score: me.HitTarget() ? 1 : 0,
                isMemberAnswer: me.IsMemberAnswer,
                frequencyType: self.FrequencyType()
            };
        };

        var saveAnswer = function (data) {

            $.ajax({
                url: '/challenge/PersonalTracking/SaveChallengeScore',
                data: data,
                cache: false,
                success: function (score) {

                    me.ChallengeScoreId(score);
                    me.IsMemberAnswer(true);
                    getTrackedDaysCount();

                },
                error: function () {
                    if (self.ShowErrors) {
                        console.log("uh oh, couldn't save your score");
                    }
                }
            });
        };

        function getTrackedDaysCount() {
            $.ajax({
                url: '/challenge/PersonalTracking/GetTrackedDaysCount',
                data: { challengeId: me.ChallengeId },
                cache: false,
                success: function (days) {

                    self.TrackedDaysCount(days);

                },
                error: function () {
                    if (self.ShowErrors) {
                        console.log("error getting tracked days count");
                    }
                }
            });
        }
    };


    // get all the things...
    self.GetModel();

};

function setSelectedDate(answer,e) {
   
    var target = (e.target) ? e.target : e.srcElement;
    
    if ( $(target).hasClass('disabled') == true) {
        return false;
    }
    
    $(".date-label.clicked").removeClass("clicked");
    $(".selected-checkbox").removeClass('selected-checkbox');
    $(target).toggleClass('clicked');

    var selectedDateText = $(target).html();

    $('#answer-NUM-input .date-of-answer-boxes').html(selectedDateText);
    $('#answer-YNO-boxes .date-of-answer-boxes').html(selectedDateText);
    
    var selectedDiv = $(target).parent().parent().find(".checkbox");
    $( selectedDiv[0] ).addClass( 'selected-checkbox' );

    $( ".selected-date-background" ).removeClass( "selected-date-background" );
    var e = target.parentNode.parentNode;
    e.className = " selected-date-background ";

    $(".show-pencil-icon").removeClass("show-pencil-icon");
    var pencilIcon =  $( target ).parent().parent().find( '.icon-pencil' );
    pencilIcon.addClass("show-pencil-icon");

    $(".selected-input").removeClass('selected-input');
    $('.selected-answer').removeClass('selected-answer');
    var rangeDiv = $( target ).parent().parent().find( '.rangeNumber' );
    var answerDiv = $(target).parent().parent().find('.answer-display'); 
    
    if (rangeDiv[0] !== undefined)
        $(rangeDiv[0]).addClass('selected-input');
    if (answerDiv[0] !== undefined)
        $(answerDiv[0]).addClass('selected-answer');
    $(".rangeNumber-big-input").val($('.selected-input').val());
    
    if (answer === 0) {
        $(document).find('#NO-box').addClass('NO-box');
        $(document).find('#YES-box').removeClass('YES-box');
        $(document).find('#YES-box').addClass('YES-box-selected');
    } else if (answer === 1) {
        $(document).find('#YES-box').addClass('YES-box');
        $(document).find('#NO-box').removeClass('NO-box');
        $(document).find('#NO-box').addClass('NO-box-selected');
    } else {
        $(document).find('#NO-box').removeClass('NO-box-selected');
        $(document).find('#NO-box').addClass('NO-box');
        $(document).find('#YES-box').removeClass('YES-box-selected');
        $(document).find('#YES-box').addClass('YES-box');
    }

}

function saveAnswerYesNo(answer) {


 CURRENT_YES_NO_ANSWER = answer;
        if (answer == 'YES') {
            $(document).find('#NO-box').addClass('NO-box');
            $(document).find('#YES-box').removeClass('YES-box');
            $(document).find('#YES-box').addClass('YES-box-selected');

        } else {
            $(document).find('#YES-box').addClass('YES-box');
            $(document).find('#NO-box').removeClass('NO-box');
            $(document).find('#NO-box').addClass('NO-box-selected');
        }
  
    
    $('.selected-checkbox').click();
  
    
};


 function saveInputAnswer() {

    CURRENT_YES_NO_ANSWER = '';
     
    var inputNumber = $(".rangeNumber-big-input").val();
  $('.selected-answer').html(inputNumber);
    $('.selected-input').val(inputNumber);
    $('.selected-input').change();
    // saveAnswer( data );
};

function Validate(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    if (key === 8) { return; }
    key = String.fromCharCode(key);
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault(); 
    }
};
 
function onKeyPressOnlyNum(e) {
    e = e || window.event;
    var keyunicode = e.charCode || e.keyCode;
    return keyunicode >= 48 && keyunicode <= 57 || keyunicode == 8 || keyunicode == 46 || keyunicode == 37 || keyunicode == 39;
};   