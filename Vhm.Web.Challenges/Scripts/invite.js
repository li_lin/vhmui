﻿
var InviteModel = function(cId) {

    var self = this;


    self.Ready = ko.observable(false);

    self.Content = ko.observable({});

    self.OwnerType = ko.observable('');
    
    self.Invitees = ko.observableArray([]); // list of those to be invited

    self.Friends = ko.observableArray([]);
    self.Groups = ko.observableArray([]);
    self.WriteIns = ko.observableArray([]);
    self.NewList = ko.observableArray([]);
    self.ExistingList = ko.observableArray([]);

    self.SearchVal = ko.observable('');
    self.NotFound = ko.observable(false);
    self.ModelError = ko.observable('');
    self.LoadError = ko.observable(false);
    
    self.ChallengeId = cId;

    self.FriendsPageNumber = ko.observable(0);

    self.PostText = ko.observable('');
    
    self.FilterVal = ko.observable('');
    self.Filter = function() {

        var filterVal = self.FilterVal();

        var friends = self.Friends();
        for (var i = 0; i < friends.length; i++) {
            var name = friends[i].DisplayName.toLowerCase();
            var match = new RegExp(filterVal.toLowerCase(), "g");
            friends[i].Visible(match.test(name));
        }
    };

    self.SelectAll = ko.observable(false);
    self.ToggleSelectAllFriends = function() {
        
        var selected = !self.SelectAll(); // property gets updated after the change, so use !selected
        setSelectAll(selected);
        
        return true;
    };

    function setSelectAll(selected) {
        
        var friends = self.Friends();
        for (var i = 0; i < friends.length; i++) {
            if (friends[i].Visible()) {
                friends[i].Selected(selected);
            }
        }
        self.SelectAll(selected);
    }
    
    self.SelectAllFriends = ko.observable(false);

    self.GetNextFriendsPage = function() {

        var nextPage = self.FriendsPageNumber() + 1;

        $.ajax({
            url: '/challenge/personaltracking/GetMoreFriendsToInvite',
            data: { pageNumber: nextPage },
            cache: true,
            success: function(f) {
                self.FriendsPageNumber(nextPage);
                var friends = self.Friends();

                for (var i = 0; i < f.length; i++) {
                    friends.push(new Member(f[i]));
                }
                self.Friends(friends);
                self.OnGetNextFriendPage();
            },
            error: function() {
                var message = $("#hdnErrorGettingContent").val();
                self.ModelError((message != null && message.length > 0) ? message : 'There was a problem loading this content');
            }
        });
    };

    self.OnGetNextFriendPage = function() {
        return true;
    };

    self.RemoveAllInvitees = function() {
        self.Invitees([]);
        setSelectAll(false);
    };

    self.RemoveInvitee = function(item) {

        var existingInvitees = self.Invitees();
        try {
            for (var i = 0; i < existingInvitees.length; i++) {
                if (item.MemberId != null && item.MemberId != '') {
                    if (existingInvitees[i] && existingInvitees[i].MemberId == item.MemberId) {
                        existingInvitees.splice(i, 1);
                        break;
                    }
                } else {
                    if (existingInvitees[i] && existingInvitees[i].EmailAddress == item.EmailAddress) {
                        existingInvitees.splice(i, 1);
                        break;
                    }
                }
            }
            self.Invitees(existingInvitees);
        } catch(ex) {

            self.ModelError("There was a problem removing this item from the list");
        }
    };

    self.RemoveWriteIn = function(item) {

        self.ModelError('');
        var existingWriteIns = self.WriteIns();
        try {
            for (var i = 0; i < existingWriteIns.length; i++) {
                if (item.MemberId != null && item.MemberId != '') {
                    if (existingWriteIns[i] && existingWriteIns[i].MemberId == item.MemberId) {
                        existingWriteIns.splice(i, 1);
                        break;
                    }
                } else {
                    if (existingWriteIns[i] && existingWriteIns[i].EmailAddress == item.EmailAddress) {
                        existingWriteIns.splice(i, 1);
                        break;
                    }
                }
            }
            self.WriteIns(existingWriteIns);
        } catch(ex) {

            self.ModelError(ex.message);
        }
    };

    self.RemoveNewList = function(item) {
        self.ModelError('');
        var existingNewList = self.NewList();
        try {
            for (var i = 0; i < existingNewList.length; i++) {
                if (item.MemberId != null && item.MemberId != '') {
                    if (existingNewList[i] && existingNewList[i].MemberId == item.MemberId) {
                        existingNewList.splice(i, 1);
                        break;
                    }
                } else {
                    if (existingNewList[i] && existingNewList[i].EmailAddress == item.EmailAddress) {
                        existingNewList.splice(i, 1);
                        break;
                    }
                }
            }
            self.NewList(existingNewList);
        } catch (ex) {

            self.ModelError(ex.message);
        }
    };
    
    self.UploadProgress = ko.observable(0);
    self.GetImportList = function() {

    };

    self.FileUploadOptions = {
        type: "POST",
        url: '/challenge/personaltracking/GetUploadInviteList',
        data: null,
        contentType: "text/plain",
        dataType: "json",
        beforeSend: function() {
            self.UploadProgress(0);
        },
        uploadProgress: function(event, position, total, percentComplete) {
            self.UploadProgress(percentComplete);
        },
        success: function(people) {
            self.UploadProgress(100);

            if (people.length == 0) {
                self.NotFound(true);
            } else {
                self.NotFound(false);
                addNewImportList(people);
            }
        },
        error: function() {
            var message = $("#hdnErrorGettingContent").val();
            self.ModelError((message != null && message.length > 0) ? message : 'There was a problem getting your file');
        }
    };

    self.AddToInviteList = function() {
        // Go through all the list collections and add them to the final invite list.
        var lists = new Array();
        var invitees = new Array();

        lists.push(self.Friends());
        lists.push(self.Groups());
        lists.push(self.WriteIns());
        lists.push(self.NewList());
        lists.push(self.ExistingList());

        var allInvitees = self.Invitees();

        for (var m = 0; m < lists.length; m++) { // iterate over all the collections

            var thisList = lists[m];

            for (var i = 0; i < thisList.length; i++) { // iterate over all the individuals in the current collection
                var added = false; // don't add the same person twice
                for (var j = 0; j < allInvitees.length; j++) {
                    var id1, id2;
                    if (parseInt(thisList[i].MemberId) > 0) {
                        id1 = thisList[i].MemberId;
                        id2 = allInvitees[j].MemberId;
                    } else {
                        id1 = thisList[i].EmailAddress;
                        id2 = allInvitees[j].EmailAddress;
                    }
                    if (isDuplicate(id1, id2)) {
                        added = true;
                        break;
                    }
                }

                if (!added && thisList[i].Selected() == true) {

                    allInvitees.push(thisList[i]);
                }
                thisList[i].Selected(false);
            }

            // clear out the select lists (but not friends or groups)
            self.WriteIns([]);
            self.NewList([]);
            self.ExistingList([]);
        }
        // add to the main invite list
        invitees.sort(compare);
        self.Invitees(allInvitees);

        setSelectAll(false);
    };

    function compare(a, b) {
        if (a.DisplayName.toLowerCase() < b.DisplayName.toLowerCase())
            return -1;
        if (a.DisplayName.toLowerCase() > b.DisplayName.toLowerCase())
            return 1;
        return 0;
    }

    self.Search = function() {

        self.ModelError('');
        var searchVal = self.SearchVal();
        searchVal = removeDuplicatesFromString(searchVal); /*if this is a comma-separated list, remove the duplicates*/

        if (searchVal.length < 2) {
            return;
        }

        $.ajax({
            url: '/challenge/personaltracking/SearchForMember',
            data: { emailList: searchVal },
            cache: false,
            success: function(people) {

                if (people.length == 0) {
                    self.NotFound(true);
                } else {
                    self.NotFound(false);
                    addToWriteInList(people);
                }
            },
            error: function() {
                var message = $("#hdnErrorGettingContent").val();
                self.ModelError((message != null && message.length > 0) ? message : 'There was a problem loading this content');
            }
        });

    };

    function removeDuplicatesFromString(val) {
        var items = val.split(',');
        var uniqueItems = new Array();
        $.each(items, function(i, item) {
            if ($.inArray(item.trim(), uniqueItems) === -1) uniqueItems.push(item.trim());
        });
        return uniqueItems.join(',');
    }

    function addNewImportList(people) {
        var newList = self.NewList();
        for (var i = 0; i < people.length; i++) {
            var added = false; // don't add the same person twice
            for (var j = 0; j < newList.length; j++) {
                // if no memberId is available, check email
                var id1, id2;
                if (parseInt(newList[j].MemberId) > 0) {
                    id1 = newList[j].MemberId;
                    id2 = people[i].MemberId;
                } else {
                    id1 = newList[j].EmailAddress;
                    id2 = people[i].EmailAddress;
                }
                if (isDuplicate(id1, id2)) {
                    added = true;
                    break;
                }
            }
            if (!added) {
                newList.push(new Member(people[i]));
            }
        }
        self.NewList(newList);
    }

    function addToWriteInList(people) {

        var writeIns = self.WriteIns();
        for (var i = 0; i < people.length; i++) {
            var added = false; // don't add the same person twice
            for (var j = 0; j < writeIns.length; j++) {
                // if no memberId is available, check email
                var id1, id2;
                if (parseInt(writeIns[j].MemberId) > 0) {
                    id1 = writeIns[j].MemberId;
                    id2 = people[i].MemberId;
                } else {
                    id1 = writeIns[j].EmailAddress;
                    id2 = people[i].EmailAddress;
                }
                if (isDuplicate(id1, id2)) {
                    added = true;
                    break;
                }

            }
            if (!added) {
                writeIns.push(new Member(people[i]));
            }
        }

        self.WriteIns(writeIns);
        self.SearchVal('');
    }

    function isDuplicate(val1, val2) {
        return val1 == val2;
    }

    self.GetModel = function() {

        $.ajax({
            url: '/challenge/personaltracking/GetInviteModel',
            data: { challengeId: self.ChallengeId, contentLabel: 'PersonalKnowAndGo' },
            cache: false,
            success: function(c) {

                self.Content(c.Model.Content);
                self.OwnerType(c.Model.OwnerType);
                
                var friends = new Array();
                for (var i = 0; i < c.Friends.length; i++) {
                    friends.push(new Member(c.Friends[i]));
                }
                self.Friends(friends);
                self.PostText('')
                self.Ready(true);
                
            },
            error: function() {
                self.LoadError(true);
            }
        });
    };

    self.GetModel();
};

var Member = function (m) {

    var me = this;
    me.DisplayName = m.DisplayName;
    me.ImageUrl = m.ImageUrl;
    me.Selected = ko.observable(m.Selected);
    me.MemberId = m.MemberId;
    me.EmailAddress = m.EmailAddress;
    me.Visible = ko.observable(true);

    me.GetDisplayName = function () {
        return me.DisplayName.trim().length == 0 ? me.EmailAddress : me.DisplayName;
    };
};