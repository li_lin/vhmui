﻿$(document).ready(function () {
    ko.applyBindings(new SelectChallengeModel());
});
var SelectChallengeModel = function() {
    
    var self = this;
    self.Content = ko.observable({});
    self.ActivityBadges = ko.observableArray([]);
    self.NutritionBadges = ko.observableArray([]);
    self.WellBeingBadges = ko.observableArray([]);
    self.ImageURLBase = '';
    self.ChallengeTypeId = 1;
    self.Ready = ko.observable(false);
    self.LoadError = ko.observable(false);
    
    self.GetModel = function () {

        $.ajax({
            url: '/challenge/PersonalTracking/GetSelectChallengeModel',
            cache: true,            
            success: function (c) {

                self.Content(c.Content);
                self.ImageURLBase = c.ImageURLBase;
                
                var pac = new Array();
                var nut = new Array();
                var wel = new Array();
                
                for ( var i = 0; i < c.Badges.length; i++ ) {
                    var badge = c.Badges[i];

                    badge.Link = '/challenge/create/' + badge.BadgeImageString;
                    
                    // temp solution for links that aren't ready yet
                    if ( badge.BadgeImageString.toUpperCase() == 'D95EF806-8C56-4872-9CB1-B6B1E5BC41D7' ||
                        badge.BadgeImageString.toUpperCase() == '3958CDCB-4212-47B9-A353-951D8F3392A8' ||
                        badge.BadgeImageString.toUpperCase() == '26DA77F4-F02A-4EC7-B929-29DB7CC8C467') {
                        
                        badge.Link = '/secure/challenge/create.aspx';
                        
                    }
                    badge.BadgeImageString = 'icon-' + badge.BadgeImageString + '.png';
                    
                    switch ( badge.BadgeCategoryCode ) {
                    case 'PAC':
                        {
                            pac.push( badge );
                            break;
                        }
                    case 'NUT':
                        {
                            nut.push( badge );
                            break;
                        }
                    case 'WEL':
                        {
                            wel.push( badge );
                            break;
                        }
                    }
                }
                
                self.ActivityBadges(pac);
                self.NutritionBadges( nut );
                self.WellBeingBadges(wel);

                self.Ready( true );
            },
            error: function () {
                self.LoadError( true );
            }
        });

    };

    self.GetModel();
};