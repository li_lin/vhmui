﻿// Model used by the Join a Challenge page

( function() {
    "use strict";

    $( document ).ready( function() {
        // Support pluralizing labels as appropriate
        // TODO: Move this logic to a global space so it can be shared by multiple views
        ko.bindingHandlers.pluralize = {
            update: function( element, valueAccessor ) {
                var settings = valueAccessor();
                var text = settings.data + ' ' + ( ( settings.data === 1 ) ? settings.singular : settings.plural );
                $( element ).text( ko.utils.unwrapObservable( text ) );
            }
        };

        var model = new JoinModel();
        model.AfterOnBind = function() {

            // Handle "Select all"
            //$('.select-all a').click(function () {
            //    $('input[type=checkbox]:visible').each(function () {
            //        if (!this.checked) {
            //            this.checked = true;
            //            $(this).trigger('change');
            //        }
            //    });
            //    return false;
            //});

            // Select first tab
            $( '#join-challenge .nav-tabs li:first, #join-challenge .carousel .item:first' ).addClass( 'active' );

            $( ".challengequestion" ).each( function() {
                $( this ).popover( { content: $( this ).attr( 'questiontext' ) } );
            } );

            // Configure carousel, which groups challenges by category
            $( '#join-carousel' ).carousel( { interval: false } ).on( 'slide', function( e ) {
                var tabs = $( '#join-challenge .nav-tabs li' );
                tabs.removeClass( 'active' );
                $( tabs[$( e.relatedTarget ).index()] ).addClass( 'active' );
            } );
        }
        ko.applyBindings( model );
    } );
}
)();

var JoinModel = function () {
	var self = this;

	self.categories = ko.observableArray();

	self.Ready = ko.observable(false);
    
	self.ClearSelected = function (challenge) {
	
	   for (var i = 0; i < self.categories().length; i++) {
            var category = self.categories()[i];
            var challenges = category.challenges;
            for (j = 0; j < challenges.length; j++) {
                if (challenge.ChallengeId != challenges[j].ChallengeId) {
                    challenges[j].Selected(false);
                } else {
                    challenges[j].Selected(true);
                }
            }
        }
	    return true;
	};
    
	self.GetModel = function () {
	 
	    $.ajax({
	        url: '/challenge/personaltracking/GetBrowseList',
	        success: function (response) {

	            for (var i = 0; i < response.Challenges.length; i++) {
	                response.Challenges[i].img = response.CDNPath + response.Challenges[i].img + '.png';
	                response.Challenges[i].numPlayers = response.Challenges[i].ParticipantCount;
	                response.Challenges[i].question=response.Challenges[i].Question;
	            }
	            
	            var tmpCategories = [];

	            for (i = 0; i < response.BrowseCategories.length; i++) {
	                var newCategory = {
	                    title: response.BrowseCategories[i].BrowseCategory,
	                    code: response.BrowseCategories[i].Code,
	                    challenges: filterChallenges(response.Challenges, response.BrowseCategories[i].BrowseCategory)
	                };
	                for (j = 0; j < newCategory.challenges.length; j++) {
	                    newCategory.challenges[j].Selected = ko.observable(false);	                    
	                }
	                tmpCategories.push(newCategory);
	            }
	             
	            self.categories(tmpCategories);
	            
	            self.AfterOnBind();

	            self.Ready( true );
	        },
	        error: function () {

	        }

	    });

	};

    self.AfterOnBind = function() {
        return true;
    };

    self.JoinChallenges = function () {
        
      var challengeIds = [];
        for (var i = 0; i < self.categories().length; i++) {
            var category = self.categories()[i];
            var challenges = category.challenges;
            for (j = 0; j < challenges.length; j++) {
                if (challenges[j].Selected()) {
                    challengeIds.push(challenges[j].ChallengeId);
                }
            }
        }

        $.ajax({
            url: '/challenge/personaltracking/AddMemberToChallenge',
            data: { challengeIdList: challengeIds.join(',') },
            success: function (response) {
                window.location.href = '/challenge/' + challengeIds[0];

            },
            error: function() {
                alert('error!');
            }
        });
    };
    
    self.GetModel();

	function fetchCategorizedChallenges() {
		// TODO:  This is dummy data - replace with real data from backend

		var challenges = [{
			title: 'Skip soda for a week',
			numPlayers: 432,
			img: '/challenge/content/badges/nutrition/badge-soda.png'
		}, {
			title: 'Get 8 hours of sleep a night for a week',
			numPlayers: 341,
			img: '/challenge/content/badges/well_being/D5075344-48AD-44EA-BB1E-2CF9FD5707CD.png'
		}, {
			title: 'Break the caffeine habit!  No caffeine for a week',
			numPlayers: 320,
			img: '/challenge/content/badges/well_being/47CFD497-A282-4654-8925-E390BB8E69EE.png'
		}, {
			title: 'Try a new fruit or veggie',
			numPlayers: 201,
			img: '/challenge/content/badges/nutrition/badge-veggie.png'
		}, {
			title: 'Take 107 stairs this week',
			numPlayers: 194,
			img: '/challenge/content/badges/activity/badge-stairs.png'
		}, {
			title: 'Skip dessert!',
			numPlayers: 190,
			img: '/challenge/content/badges/nutrition/badge-dessert.png'
		}, {
			title: 'Try a new class at the gym',
			numPlayers: 125,
			img: '/challenge/content/badges/activity/badge-zumba.png'
		}, {
			title: 'Have a walking meeting',
			numPlayers: 111,
			img: '/challenge/content/badges/activity/badge-active-minutes.png'
		}, {
			title: 'Hug your kids or loved ones',
			numPlayers: 103,
			img: '/challenge/content/badges/well_being/badge-hug.png'
		}, {
			title: 'Keep an activity journal',
			numPlayers: 95,
			img: '/challenge/content/badges/activity/badge-activity-journal.png'
		}];

		return [{
			title: 'Most popular',
			challenges: filterChallenges( challenges, 100 )
		}, {
			title: 'Activity',
			challenges: filterChallenges( challenges, 'activity' )
		}, {
			title: 'Nutrition',
			challenges: filterChallenges( challenges, 'nutrition' )
		}, {
			title: 'Well-Being',
			challenges: filterChallenges( challenges, 'well_being' )
		}, {
		    title: 'Other',
		    challenges: filterChallenges( challenges, 'other' )
    }];
	}

	// This a helper function to group challenges by category or popularity
	function filterChallenges(challenges, filter) {
		var dest = [];
		for (var i = 0; i < challenges.length; i++) {
			var c = challenges[i];
			if (typeof (filter) == 'number') {
				if (c.numPlayers >= filter) {
					dest.push(c);
				}
			 } else if (c.ChallengeCategory.BrowseCategory.indexOf(filter) >= 0) {
			//} else if (c.img.indexOf(filter) >= 0) {
				dest.push(c);
			}
		}

		return dest;
	}
}
