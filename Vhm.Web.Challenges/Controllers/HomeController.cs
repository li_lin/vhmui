﻿using System.Web.Mvc;

namespace Vhm.Web.Challenges.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        #region View Action Methods

        public ActionResult Index( int? id = 0 )
        {
            if ( id == 0 )
            {
                return RedirectToAction( "Select" );
            }
            return View();
        }

        public ActionResult Select()
        {

            return View();
        }
        
        public ActionResult Create(string id = "")
        {
            return View();
        }

        public ActionResult Invite(long id = -1)
        {
            return View();
        }

		public ActionResult Join(long id = -1)
		{
			return View();
		}

        #endregion
    }                            
}