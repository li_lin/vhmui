﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Vhm.Providers;
using Vhm.Web.BL;
using Vhm.Web.Common;
using Vhm.Web.Models.ContentEnums;
using Vhm.Web.Models.Enums;
using Vhm.Web.Models.InputModels;
using Vhm.Web.Models.RepositoryModels;
using Vhm.Web.Models.ViewModels.Challenges;
using Vhm.Web.Models.ViewModels.Social;

namespace Vhm.Web.Challenges.Controllers
{
    [Authorize]
    public class PersonalTrackingController : BaseController
    {
        private const string OBSOLETE_BADGE = "skip caffeine";
        public JsonResult GetChallenge(long challengeId)
        {

            var challengeMember = ChallengeBL.GetChallengeMember(_session.MemberID, challengeId); // needed for member rank

            #region Challenge Details

            var challenge = new PersonalTrackingChallenge();
            var frequencyType = TrackingChallengeFrequencyType.Daily;
            try
            {
                challenge = ChallengeBL.GetChallenge( challengeId );
                var availableChallenges = ChallengeBL.GetMemberAvailableChallengesByType( _session.MemberID,
                    ChallengeType.PersonalKAG );

                int trackingChallengeDisplayDaysAfterEndDate =
                    int.Parse(ConfigurationManager.AppSettings["TrackingChallengeDisplayDaysAfterEndDate"]);
                // If the member is not in the challenge, check the owner type of the challenge. If it is a personal challenge,
                // they should not be here at all. If it is a corporate challenge, add the member to the challenge. 
                if (availableChallenges.All(c => c.ChallengeId != challenge.ChallengeId) && challengeMember == null &&
                     challenge.OwnerType == ChallengeOwnerType.PersonalChallenge )
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "User is not in this challenge (" + challengeId + ")";
                    return Json( string.Empty, JsonRequestBehavior.AllowGet );
                }

                if ( DateTime.UtcNow >
                     challenge.EndDate.GetValueOrDefault().AddDays( trackingChallengeDisplayDaysAfterEndDate ) )
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "Challenge has ended and is no longer available for display (" + challengeId + ")";
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }

                var challengeMenuItems = CacheProvider.Get( System.Web.HttpContext.Current, CacheProvider.CacheItems.CorporateChallenges ) as
                        List<ChallengeBase> ?? MemberBL.GetMemberChallenges( _session.MemberID,
                            _session.PrimarySponsorID, ApplicationSettings.ChallengeEndedDisplayDays,
                            _session.LanguageID );

                // if the member doesn't have this challenge in their menu, return 404
                if (challengeMenuItems.Any(c => c.ChallengeId == challengeId) && challenge.OwnerType == ChallengeOwnerType.CorporateChallenge)
                {
                    // If they're not already in the challenge, add them in here.
                    if ( challengeMember == null )
                    {
                        ChallengeBL.AddPersonalTrackingChallengeMember( _session.MemberID, challengeId );
                        challengeMember = ChallengeBL.GetChallengeMember( _session.MemberID, challengeId );
                    }
                } else
                {
                    if ( challengeMember == null )
                    {
                        Response.StatusCode = 404;
                        Response.StatusDescription = "User is not in this challenge (" + challengeId + ")";
                        return Json( string.Empty, JsonRequestBehavior.AllowGet );
                    }
                }


                var question = challenge.ChallengeQuestions.FirstOrDefault();
                if ( question != null )
                {
                    if ( question.QuestionFrequency == 255 && question.QuestionFrequencyDatePart == "yy" )
                    {
                        frequencyType = TrackingChallengeFrequencyType.Once;
                    }
                    if ( question.QuestionFrequency == 1 && question.QuestionFrequencyDatePart == "dd" )
                    {
                        frequencyType = TrackingChallengeFrequencyType.Daily;
                    }
                    if ( question.QuestionFrequency == 1 && question.QuestionFrequencyDatePart == "wk" )
                    {
                        frequencyType = TrackingChallengeFrequencyType.Weekly;
                    }
                }
            }
            catch ( Exception ex )
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(
                    new Exception( "Error getting challenge details", ex ) );
            }

            #endregion

            #region Chat
            var chat = new List<ChallengeMessage>();
            try
            {
                chat = ChallengeBL.GetChallengeMessages(challengeId);
            }
            catch (Exception ex)
            {

                VirginLifecare.Exception.ExceptionHandler.ProcessException(new Exception("Error getting chat", ex));
            }
            #endregion

            #region LeaderBoard

            var leaders = new List<ChallengeMemberScore>();
            try
            {
                leaders = ChallengeBL.GetChallengeLeaders(challengeId, 0,
                                                           ApplicationSettings.ChallengeLeaderBoardPageSize);
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(new Exception("Error getting leader board", ex));
            }
            #endregion

            #region Viewer Name
            var viewerName = string.Empty;
            try
            {
                var member = MemberBL.GetMemberById(_session.SessionID, _session.MemberID);
                if (member != null)
                {
                    viewerName = member.DisplayName;
                }
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(new Exception("Error getting viewer name", ex));
            }
            #endregion

            #region Content

            Dictionary<string, string> stringContentDictionary;
            try
            {
                var content = MemberBL.GetContentLabels<RankingsContent>(PageNames.RankingsPage, _session.LanguageID);
                stringContentDictionary = content.ToDictionary(item => item.Key.ToString(), item => item.Value);
            }
            catch (Exception ex)
            {
                stringContentDictionary = new Dictionary<string, string>();
                VirginLifecare.Exception.ExceptionHandler.ProcessException(new Exception("Error getting challenge content", ex));
            }
            #endregion

            #region Tracked Days

            var days = new ChallengeScoreInfo();
            // initialize an anonomous structure
            var trackedDays = from cs in new List<ChallengeScore>()
                              select new {
                                  Day = string.Empty,
                                  Answer = bool.FalseString,
                                  MemberId = new long(),
                                  ChallengeId = new long(),
                                  ChallengeScoreId = new long(),
                                  ScoreDate = DateTime.Now,
                                  Value = new decimal?(),
                                  Score = new decimal(),
                                  IsMemberAnswer = false
                              };
            try
            {
                days = ChallengeBL.GetTrackedDays(challenge.StartDate, challenge.EndDate.GetValueOrDefault(), challengeId, _session.MemberID);
                trackedDays = from cs in days.ChallengeScores
                              select new {
                                  Day = cs.ScoreDate.ToShortDateString(),
                                  Answer = cs.Value.ToString(),
                                  cs.MemberId,
                                  cs.ChallengeId,
                                  ChallengeScoreId = cs.ChallengeScoreID,
                                  cs.ScoreDate,
                                  cs.Value,
                                  cs.Score,
                                  IsMemberAnswer = cs.Value != null
                              };
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(new Exception("Error getting challenge tracked days", ex));
            }

            var trackedDaysWithYesCount = new {
                TrackedDays = trackedDays,
                DaysOfYes = days.ScoreCount
            };
            #endregion

            #region Member Profile Image

            var profileImage = string.Empty;
            try
            {
                var profile = SocialBL.GetMemberProfile( _session.SessionID, _session.MemberID, _session.MemberID, false );
                if ( profile != null )
                {
                    profileImage = profile.Member.ImageUrl;
                }
            }
            catch ( Exception ex )
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException( ex );
            }
            #endregion

            // get the badge from the AllDone or AllDoneAtTarget task
            var badge = new BadgeItem();
            var task =
                challenge.ChallengeQuestions[0].ChallengeTasks.FirstOrDefault(
                    t => t.TriggerCode == "ALLDN" || t.TriggerCode == "ALLTG" );
            if ( task != null && task.ChallengeTaskRewards.Count > 0 )
            {
                var reward = task.ChallengeTaskRewards.SingleOrDefault( t => t.BadgeID.HasValue );
                if ( reward != null )
                {
                    badge = RewardsBL.GetBadge( reward.BadgeID.Value );
                }
            }
            
            var model = new PersonalKnowAndGoVM {
                Challenge = challenge,
                LeaderBoard = leaders,
                Chat = chat.Take(ApplicationSettings.ChallengeChatMessagesToDisplay).ToList(),
                ContentRankings = stringContentDictionary,
                TrackedDaysInfo = trackedDaysWithYesCount,
                Rank = challengeMember.Rank,
                ViewerName = viewerName,
                MemberImage = profileImage,
                BadgeFileName = badge.BadgeImage != null ? ApplicationSettings.BadgeImageUrl + new Guid(badge.BadgeImage) : string.Empty,
                DaysToDisplay = ApplicationSettings.PersonalTrackingDaysToDisplay,
                IsChallengeCreator = challenge.OwnerID == _session.MemberID,
                FrequencyType = frequencyType.ToString()
            };

            return Json(model, JsonRequestBehavior.AllowGet);
        }
        
        [HttpGet]
        public JsonResult GetTrackedDaysCount(long challengeId)
        {
            var challenge = ChallengeBL.GetChallenge( challengeId );
            var days = ChallengeBL.GetTrackedDays(challenge.StartDate, challenge.EndDate.GetValueOrDefault(), challengeId, _session.MemberID);
            return Json( days.ScoreCount, JsonRequestBehavior.AllowGet );
        }

        public JsonResult GetCreateChallengeModel(string contentLabel, string categoryKey = "")
        {
            try
            {
                var model = new CreateChallengeVM();

                var label = (PageNames)Enum.Parse(typeof(PageNames), contentLabel);
                var content = MemberBL.GetContentLabels<PersonalKnowAndGoContent>(label, _session.LanguageID);
                var stringContentDictionary = content.ToDictionary(item => item.Key.ToString(), item => item.Value);
                model.Content = stringContentDictionary;
                categoryKey = categoryKey.ToLower();

                model.CDNPath = ApplicationSettings.BadgeImageUrl;

                // get all the Personal KAG badges
                var badges = ChallengeBL.GetBadgesByType(BadgeType.TRK).Where(b => b.BadgeName.ToLower() != OBSOLETE_BADGE).ToList();
                var selectedBadge = badges.SingleOrDefault(b => new Guid(b.BadgeImage).ToString().ToLower() == categoryKey);
                if (selectedBadge != null)
                {
                    var categoryCode = selectedBadge.BadgeCategoryCode;
                    // Get all the badges with the same category code as the selected one
                    if (categoryCode != null)
                    {
                        model.Badges = badges.Where(b => b.BadgeCategoryCode == categoryCode).ToList();
                    }

                    // get the rest of the challenge configuration information from the config file
                    var setup = ChallengeSetup.Configurations;
                    if (setup != null)
                    {
                        model.ChallengeConfig = setup.FirstOrDefault(config => config.CategoryKey.ToLower() == categoryKey);
                        // assign the selected badgeID to the configuration (that's not in the config file)
                        if (model.ChallengeConfig != null)
                        {
                            model.ChallengeConfig.BadgeId = selectedBadge.BadgeID;
                        }
                    }
                                        
                } else
                {
                    model.Badges = badges;
                }

                return Json(model, JsonRequestBehavior.AllowGet); 
            }
            catch (Exception ex)
            {
                throw VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
            }
        }

        public JsonResult GetInviteModel(long challengeId, string contentLabel)
        {            
            try
            {
                var model = new InviteChallengeVM();

                #region challenge
                var challenge = ChallengeBL.GetChallenge(challengeId);
                #endregion

                #region content
                var challengeLabel = (PageNames)Enum.Parse(typeof(PageNames), contentLabel);
                var challengeContent = MemberBL.GetContentLabels<PersonalKnowAndGoContent>(challengeLabel, _session.LanguageID);


                var socialLabel = (PageNames)Enum.Parse(typeof(PageNames), PageNames.SocialChallengePromotionLabels.ToString());
                var socialContent = MemberBL.GetContentLabels<SocialChallengePromotionLabelsContent>(socialLabel, _session.LanguageID);

                var stringContentDictionary = challengeContent.ToDictionary(item => item.Key.ToString(),
                                                                            item => item.Value);
                foreach (var item in socialContent)
                {
                    if (!stringContentDictionary.ContainsKey(item.Key.ToString()))
                    {
                        stringContentDictionary.Add(item.Key.ToString(), item.Value);
                    }
                }                                                         
                                               

                model.Content = stringContentDictionary;
                model.OwnerType = challenge.OwnerType;
                model.ChallengeName = challenge.Name;

                #endregion

                #region Friends

                var paging = new PagingIM
                    {
                        Count = ApplicationSettings.PersonalTrackingFriendsPerPage,
                        MemberId = _session.MemberID,
                        PageNumber = 0
                    };
                var friends = ChallengeBL.GetFriendSelectListForChallengeInvite(_session.SessionID, paging);
                #endregion

                var items = (from f in friends
                            select
                                new
                                    {
                                        f.DisplayName,
                                        f.ImageUrl,
                                        f.MemberId,
                                        //f.EmailAddress,
                                        string.Empty,
                                        Selected = false
                                    }).ToList();


                return Json(new {Model = model, Friends = items}, JsonRequestBehavior.AllowGet); 
            }
            catch (Exception ex)
            {
                throw VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
            }
        }

        public JsonResult GetMoreFriendsToInvite(int pageNumber)
        {            
            var paging = new PagingIM {
                Count = ApplicationSettings.PersonalTrackingFriendsPerPage,
                MemberId = _session.MemberID,
                PageNumber = pageNumber
            };
            var friends = ChallengeBL.GetFriendSelectListForChallengeInvite(_session.SessionID, paging);

            var items = (from f in friends
                         select
                             new {
                                 f.DisplayName,
                                 f.ImageUrl,
                                 f.MemberId,
                                 f.EmailAddress,
                                 Selected = false
                             }).ToList();
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSelectChallengeModel()
        {            
            try
            {
                var model = new SelectChallengeVM();

                model.Badges = new List<RewardBadge>();

                model.Badges.AddRange(ChallengeBL.GetBadgesByType(BadgeType.TRK).Where(b => b.BadgeName.ToLower() != OBSOLETE_BADGE));
                
                model.ImageURLBase = string.Concat( ConfigurationManager.AppSettings["CDNBaseURL"],
                                                    ConfigurationManager.AppSettings["BadgeImageUrl"] );

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
            }
        }

        public ActionResult SendInvites()
        {
            long challengeId;
            try
            {
                challengeId = long.Parse( Request.Form["ChallengeId"] );

                var challenge = ChallengeBL.GetChallenge( challengeId );

                var thisMember = _session.MemberID.ToString( CultureInfo.InvariantCulture );

                var memberIds = Request.Form["MemberID"];
                var emails = Request.Form["EmailAddress"];

                var aMemberIds =
                    memberIds.Split( ',' )
                        .Where( mId => !string.IsNullOrEmpty( mId ) && mId != thisMember && mId != "0" )
                        .ToList();
                var aEmails = emails.Split( ',' ).Where( email => !string.IsNullOrEmpty( email ) );

                // ensure we have lists with no duplicates or null values. if we're inviting all friends, allow write-ins,
                // but don't send any more memberIds since their already included in 'all friends'.
                var memberIdListWithoutNulls = string.Join( ",", aMemberIds.Distinct().ToList() );
                var emailListWithoutNulls = string.Join( ",", aEmails.Distinct().ToList() );

                // For corporate tracking challenges, we just send a notification and possibly trigger a reward (if it's configured)
                // personal tracking challenges send invites, which is a different process.
                if ( challenge.OwnerType == ChallengeOwnerType.CorporateChallenge )
                {
                    // Submit the newsfeed post
                    //string postText = Request.Form["PostText"];
                    //bool posted = SocialBL.SumbitMemberPost(_session.SessionID, _session.MemberID, postText, _session.CommunityGroupID,
                    //    _session.CommunityGroupID, NewsFeedPostType.TellFriend );
                    ChallengeBL.SendTrackingNotification( _session.SessionID, memberIds, challengeId, _session.MemberID );

                    // trigger any rewards associated with the challenge trigger codes
                    char[] charSeparators = { ',' };
                    int rewardCount = memberIdListWithoutNulls.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries).ToArray().Count() +
                                      emailListWithoutNulls.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries).ToArray().Count();

                    TriggerReward( challenge, rewardCount );

                } else
                {
                    ChallengeBL.AddChallengeInvites( _session.SessionID, challengeId, _session.MemberID,
                        memberIdListWithoutNulls, emailListWithoutNulls );

                    ChallengeBL.SendChallengeInvite( challengeId, string.Empty );
                }
            }
            catch ( Exception ex )
            {
                throw VirginLifecare.Exception.ExceptionHandler.ProcessException( ex );
            }
            return RedirectToAction( "Index", "Home", new { id = challengeId } );
        }

        private void TriggerReward( PersonalTrackingChallenge challenge, int rewardCount )
        {
            // we're using only the first challenge question, as right now we only ever have 1 question. Extra logic
            // would be needed to support multiple questions.
            var question = challenge.ChallengeQuestions.FirstOrDefault();
            if (question != null)
            {
                // reward for 'Tell A Friend'
                var task =
                    question.ChallengeTasks.FirstOrDefault(t => t.TriggerCode == TriggerCode.TELL_A_FRIEND);
                if (task != null)
                {
                    // trigger one reward per memberId notified up to the reward limit
                    var maxToRewardThisTime = Math.Min( rewardCount, task.RewardLimit );
                    for (int i = 0; i < maxToRewardThisTime; i++)
                    {
                        RewardsBL.TriggerReward( _session.MemberID, TriggerCode.TELL_A_FRIEND, false, true,
                            DateTime.UtcNow,
                            task.ChallengeTaskID );
                    }
                }
            }
        }

        public JsonResult Save(string name, string description, string question, string displayType,
                                byte frequency, string frequencyDatePart, int? target, string targetType, string answType,
                                string scoreType, long badgeId, string promptType, string category = "NUT", string type = "TRK",
                                string createType = "NONE", string ownerType = ChallengeOwnerType.PersonalChallenge, string partType = "PERSONAL_INVITATION")
        {

            try
            {
                string cleanName = Server.HtmlEncode(name);
                string cleanDescription = Server.HtmlEncode(description);
                string cleanQuestion = Server.HtmlEncode(question);

                var challengeId = ChallengeBL.SaveChallenge(_session.SessionID, _session.MemberID, cleanName, cleanDescription,
                                                             cleanQuestion, displayType, frequency, frequencyDatePart,
                                                             answType, scoreType, promptType, target, targetType, badgeId,
                                                             category, type, createType, ownerType, partType);


                return Json(challengeId, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
            }

        }

        public void PostChatMessage(long challengeId, string nickName, string message)
        {
            try
            {
                ChallengeBL.SaveChallengeMessage(challengeId, null, _session.MemberID, nickName, message);
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
            }
        }

        public JsonResult GetChat(long challengeId)
        {
            try
            {
                List<ChallengeMessage> results = ChallengeBL.GetChallengeMessages(challengeId);

                // get the distinct list of memberIds and their profile images into a dictionary.
                var memberImages = results.Select(m => new { MemberId = m.MemberID })
                                          .Distinct() // need to do distinct because we need a dictionary. no duplicates allowed.                                    
                                          .ToDictionary(m => m.MemberId,
                                                         m => SocialBL.GetMemberImagePath(m.MemberId,
                                                                                           ApplicationSettings.MemberImageUrl,
                                                                                           ApplicationSettings.DefaultMemberPicGuid));

                results.ForEach(m => m.ImgSrc = memberImages[m.MemberID]);
                return Json(results.Take(ApplicationSettings.ChallengeChatMessagesToDisplay), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
            }
            return Json(string.Empty, JsonRequestBehavior.AllowGet);
        }
        
        public long SaveChallengeScore(long challengeScoreId, long challengeId, DateTime scoreDate, int answer, int score, bool isMemberAnswer, string frequencyType)
        {
            long returnScoreId = challengeScoreId;
            try
            {
                if (frequencyType.ToLowerInvariant() == "weekly")
                {
                    
                    var today = DateTime.Now;
                    int daysToAdd = 6 - (int) today.DayOfWeek;
                    var endOfWeek = today.AddDays(daysToAdd);
                    if (scoreDate > endOfWeek)
                    {
                        return returnScoreId;
                    }
                }
                else if (scoreDate > DateTime.Now)
                {
                    return returnScoreId;
                }

                var challengeScore = new ChallengeScore {
                    ChallengeScoreID = challengeScoreId,  
                    ChallengeId = challengeId,
                    MemberId = _session.MemberID,
                    ScoreDate = scoreDate,
                    Score = score, // from answer key
                    Value = answer  // what the person typed in
                };

                if (isMemberAnswer)
                {    
                    // they are updating their score for this day
                    ChallengeBL.UpdateChallengeScore(challengeScore);
                }
                else
                {   
                    // this is the first time they are entering a score for this day
                    returnScoreId = ChallengeBL.AddChallengeScore(challengeScore);
                }
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
            }

            return returnScoreId;
        }        

        public JsonResult GetLeaderBoard(long challengeId, int pageNumber = 0, int pageSize = 0)
        {
            try
            {
                var leaders = ChallengeBL.GetChallengeLeaders(challengeId, pageNumber, pageSize);
                return Json(leaders, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
            }
            return Json(string.Empty, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SearchForMember(string emailList)
        {
            try
            {
                var inputList = emailList.Split( ',' ).ToList();

                List<MemberEmail> memberEmails = MemberBL.GetMembersByEmailList(inputList);
                if (memberEmails != null)
                {
                    var data = memberEmails.Select(m => new
                    {
                        MemberId = m.MemberID,
                        EmailAddress = m.MemberID.GetValueOrDefault() > 0 ? string.Empty : m.EmailAddress,
                        DisplayName =
                            m.MemberID.GetValueOrDefault() > 0 ? m.FirstName + " " + m.LastName : m.EmailAddress,
                        Selected = true
                    }).ToList();
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                return Json(new{}, JsonRequestBehavior.AllowGet);
                
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
            }
            return Json(string.Empty, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetUploadInviteList()
        {
            HttpPostedFileBase fileBase = Request.Files[0];
            if (fileBase != null)
            {
                var csvStreamReader = new StreamReader(fileBase.InputStream);
                var mIds = PartialViewsBL.GetMemeberIdFromCSV(_session.SessionID, csvStreamReader);
            
                var list = SocialBL.GetMembersByIds(_session.SessionID, mIds);
            
                var data = list.Select(m => new {
                    m.Member.MemberId,
                    string.Empty,
                    DisplayName = m.Member.FirstName + " " + m.Member.LastName,
                    Selected = true
                }).ToList();


                return new JsonResult
                {
                    ContentEncoding = new UTF8Encoding( false ),
                    ContentType = "text/plain",
                    Data = data,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            return new JsonResult
            {
                ContentEncoding = new UTF8Encoding(false),
                ContentType = "text/plain",
                Data = new {MemberId = 0, string.Empty, DisplayName = string.Empty, Selected = false},
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult AddMemberToChallenge(string challengeIdList)
        {
            try
            {
                if (string.IsNullOrEmpty(challengeIdList))
                {
                    return Json(new { Success = false });
                }

                var challengeIds = challengeIdList.Split(',');

                foreach (var challengeIdStr in challengeIds)
                {
                    long challengeId;
                    long.TryParse(challengeIdStr, out challengeId);
                    if (challengeId > 0)
                    {
                        ChallengeBL.AddPersonalTrackingChallengeMember(_session.MemberID, challengeId);
                        ChallengeBL.RemoveCacheByKey( string.Format( "Challenge:MemberChallengeMenu: {0}: {1}",
                            _session.MemberID, _session.PrimarySponsorID ) );
                        ChallengeBL.RemoveCacheByKey(string.Format("Challenge:Challenge: {0}", challengeId));

                    }
                }

                CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.MemberChallenges, null);
                CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.MvcMenu, null);
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
                return Json(new { Success = false }, JsonRequestBehavior.AllowGet);
            }
            return Json(new {Success = true}, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBrowseList()
        {
            try
            {
                
                var challengeMenu = CacheProvider.Get( System.Web.HttpContext.Current, CacheProvider.CacheItems.CorporateChallenges ) as List<ChallengeBase>;
                if ( challengeMenu == null )
                {
                    var challengeDisplayDays = byte.Parse(ConfigurationManager.AppSettings["ChallengeEndedDisplayDays"]);
                    challengeMenu = MemberBL.GetCorporateTrackingChallenges( _session.MemberID,
                        _session.PrimarySponsorID, challengeDisplayDays, _session.LanguageID );
                }

                var challengeIds = (
                    from c in challengeMenu
                    where c.ShowInBrowseOnly
                    select c.ChallengeId ).ToArray();

                var challenges = ChallengeBL.GetChallenges( challengeIds );

                var badges = ChallengeBL.GetBadgesByType(BadgeType.TRK).Where(b => b.BadgeName.ToLower() != OBSOLETE_BADGE).ToList();
                var lookups = MemberBL.GetLookup( "CGC" ).ToList();

                /*
                 * NOTE - This next query assumes the following:
                 * 1. that there will always be a badge reward associated with the tracking challenge
                 * 2. we want to display the badge associated with the first reward of the first task of the first challenge question
                 * 
                 * If any of these preconditions change, this code will need to be adjusted to reflect that change.
                 */
                var corporateChallenges = (from c in challenges
                                          join cm in challengeMenu on c.ChallengeId equals  cm.ChallengeId
                                          where cm.ShowInBrowseOnly &&
                                          !cm.IsMemberEnrolled &&
                                          c.ChallengeQuestions.Any() && // assure we have at least one challenge question
                                          c.ChallengeQuestions[0].ChallengeTasks.Any() // assure we have at least one challenge task
                                          && c.ChallengeQuestions[0].ChallengeTasks[0].ChallengeTaskRewards.Any(r=>r.BadgeID.HasValue) // assure we have at least one badge reward
                                          join l in lookups on c.ChallengeCategory equals l.Code
                                          join b in badges on c.ChallengeQuestions[0].ChallengeTasks[0].ChallengeTaskRewards.Single(t=>t.BadgeID.HasValue).BadgeID equals  b.BadgeID
                                          select new
                                              {
                                                  title = c.Name,
                                                  img = new Guid(b.BadgeImage).ToString().ToLower(),
                                                  c.ChallengeId,
                                                  c.ChallengeQuestions[0].Question,
                                                  c.ParticipantCount,
                                                  ChallengeCategory = new
                                                      {
                                                          Name = l.Description,                                                          
                                                          Code = c.ChallengeCategory,
                                                          BrowseCategory = ((c.ChallengeCategory == "NUT" || c.ChallengeCategory == "ACT" || c.ChallengeCategory == "SLP") ? "ENERGY" :
                                                                                (c.ChallengeCategory == "RES" || c.ChallengeCategory == "PFM" || c.ChallengeCategory == "SKL" || c.ChallengeCategory == "PTZ") ? "FOCUS" :
                                                                                (c.ChallengeCategory == "COM" || c.ChallengeCategory == "FIN" || c.ChallengeCategory == "REL") ? "DRIVE" : "")
                                                      }
                                              }).ToList();


                var categories = (from c in corporateChallenges 
                                  select c.ChallengeCategory).Distinct().ToList();

                var browseCategories = (from c in corporateChallenges
                                  select c.ChallengeCategory).GroupBy(x => x.BrowseCategory).Select(g => g.First()).ToList();

                return Json(new
                    {
                        BrowseCategories = browseCategories,
                        Categories = categories,
                        CDNPath = ApplicationSettings.BadgeImageUrl,
                        Challenges = corporateChallenges.ToList()
                    }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
                return null;
            }
            
        }
    }
}
