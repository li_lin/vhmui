﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using Vhm.Providers;
using Vhm.Web.BL;
using Vhm.Web.Common;
using Vhm.Web.DataServices.Adapters;
using Vhm.Web.Models;
using Vhm.Web.Models.RepositoryModels;

namespace Vhm.Web.Challenges.Controllers
{
    public class BaseController : Controller
    {
        protected Session _session;
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            const string SESSION_ID_COOKIE = "SessionID";

            base.Initialize(requestContext);
            
            var ctx = System.Web.HttpContext.Current;
            try
            {
                if (Request != null && Request.Cookies != null && Request.Cookies[SESSION_ID_COOKIE] != null)
                {
                    var httpCookie = Request.Cookies[SESSION_ID_COOKIE];
                    if (httpCookie != null)
                    {
                        string sessionId = httpCookie.Value;
                        // If we have a session cookie, check how long it's been since getting a fresh copy of the VhmSession object
                        // and update it along with the cache if it's stale.
                        if (!string.IsNullOrEmpty(sessionId))
                        {
                            // get the configured time in minutes for how long to keep the session object in cache
                            int timeoutPeriod = ApplicationSettings.DBSessionTimeCheckInMinutes;

                            // Check the timestamp on the session. Need to keep the DB session alive
                            var timestampCheck =
                                CacheProvider.Get(ctx, CacheProvider.CacheItems.SessionCheckTimestamp) as DateTime?;
                            var decryptedSessionId = Decrypt(sessionId);

                            if (timestampCheck == null)
                            {
                                // If we don't have a timestamp value in cache, this is the first time through. Go get a fresh session and update the cache.
                                _session = AccountBL.GetSession(decryptedSessionId);
                                UpdateSessionCache();
                            }
                            else
                            {
                                // Check how long it's been since the last refresh of the session object. If it's stale, go get a fresh session object
                                // and update the cache.
                                if (timestampCheck.GetValueOrDefault().AddMinutes(timeoutPeriod) < DateTime.UtcNow)
                                {
                                    _session = AccountBL.GetSession(decryptedSessionId);
                                    UpdateSessionCache();

                                }
                                else
                                {
                                    // The session object that's in cache is still good, so use that...
                                    _session =
                                        CacheProvider.Get(ctx, CacheProvider.CacheItems.VHMWebSessionObject) as Session;

                                    //...unless, of course, if it's null. If it is null, then get a fresh copy and update the cache.
                                    if (_session == null)
                                    {
                                        _session = AccountBL.GetSession(decryptedSessionId);
                                        UpdateSessionCache();
                                    }
                                }
                            }
                        }

                        // if the session object is still null at this point, the session has expired in the databae
                        // and we should redirect back to the login page
                        if (_session == null || string.IsNullOrEmpty(_session.SessionID))
                        {
                            Response.Redirect(FormsAuthentication.LoginUrl, true);
                        }
                        RedirectIfMemberIsCancelled();
                    }
                }

                if (_session != null)
                {
                    if (_session.ThemeCode == null || string.IsNullOrEmpty(_session.ThemeCode.ColorThemeCode))
                    {
                        SetThemeCode();
                    }
                    if (_session != null && _session.ThemeCode != null)
                    {
                        ViewBag.Theme = _session.ThemeCode.ColorThemeCode;
                    }
                    else
                    {
                        VirginLifecare.Exception.ExceptionHandler.ProcessException(
                            new NoNullAllowedException("ThemeCode not set for sponsorID: " + _session.PrimarySponsorID));
                    }
                    
                    //redirect to HRA page if required before welcome journey
                    var modules = GetModules();
                    bool redirectToHRA = false;
                    var hraModule = modules.FirstOrDefault(m => m.ModuleTypeCode == "HRA");
                    short? hraSett = 0;
                    if (hraModule != null)
                    { //check the hra setting
                        // -1 = "Off", 0 = "Optional, not prompted", 1 = "Optional, but prompted", 
                        // 2 = "Mandatory, after Welcome Screens", 3 = "Mandatory, before Welcome Screens"; 
                        hraSett = hraModule.HRASetting;
                    }
                    if (hraSett == 3 || (_session.Membership != 1 && hraSett == 2))
                    // According to Product Manager: if HRA is set to 2 or 3, User has to complete the HRA
                    // For all members (new and old) if HRA is 3, then redirect to HRA page. New user, who hasn't completed the welcome journey and HRA is set to 2,
                    // user has to complete the welcome Journey first then comlete the HRA.
                    {
                        redirectToHRA = _session.NeedHealthSnapShot;
                    }
                    if (redirectToHRA)
                    {
                        Response.Redirect(string.Format("{0}/secure/healthsnapshot/hrassessment.aspx", ApplicationSettings.BaseURL), false);
                    }

                }
                SetGoogleAnalyticsTrackerKey();
                
                
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
            }
        }

        protected List<MemberModule> GetModules()
        {

            var modules = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.MemberModules) as List<MemberModule>;
            if (_session != null && modules == null)
            {
                modules = MemberBL.GetMemberModules(_session.MemberID, _session.PrimarySponsorID);
                CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.MemberModules, modules);
            }
            return modules;
        }

        #region Private methods
        /// <summary>
        /// Redirects to the Credit Card upgrade page if the member is cancelled
        /// </summary>
        private void RedirectIfMemberIsCancelled()
        {
            if (IsMemberCancelled())
            {
                //this is a cancelled member
                if (!Response.IsRequestBeingRedirected)
                {
                    Response.Redirect(string.Format("{0}/secure/member/upgrade.aspx", ApplicationSettings.BaseURL), true);
                }
            }
        }

        /// <summary>
        /// Returns true if the memberID has a value but the PrimarySponsorID is zero. 
        /// </summary>
        /// <returns></returns>
        private bool IsMemberCancelled()
        {
            return _session.PrimarySponsorID == 0 && _session.MemberID > 0;
        }

        /// <summary>
        /// Updates the value for the VhmSession object in session state.
        /// </summary>
        private void UpdateSessionCache()
        {
            CacheProvider.Update(System.Web.HttpContext.Current,
                                                         CacheProvider.CacheItems.VHMWebSessionObject, _session);
            CacheProvider.Update(System.Web.HttpContext.Current,
                                  CacheProvider.CacheItems.SessionCheckTimestamp,
                                  DateTime.UtcNow);
        }

        /// <summary>
        /// Updates the ViewBag TrackerKey value with the tracker key from the registry.
        /// </summary>
        private void SetGoogleAnalyticsTrackerKey()
        {
            var subKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"Software\VirginLifecare", false);
            if (subKey != null && !string.IsNullOrEmpty(Convert.ToString(subKey.GetValue("GoogleAnalyticsId", ""))))
            {
                ViewBag.TrackerKey = Convert.ToString(subKey.GetValue("GoogleAnalyticsId", ""));
            }
            else
            {
                ViewBag.TrackerKey = "UA-5511099-1";
            }
        }

        /// <summary>
        /// Updates the session object with the sponsor's theme code
        /// </summary>
        private void SetThemeCode()
        {
            var sponsorProfile = CacheProvider.Get(System.Web.HttpContext.Current, CacheProvider.CacheItems.SponsorProfile) as SponsorProfile;
            if (sponsorProfile == null && _session.PrimarySponsorID > 0)
            {
                sponsorProfile = SponsorBL.GetSponsorProfile(_session.PrimarySponsorID);
                CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.SponsorProfile, sponsorProfile);
            }
            var colorThemes = SponsorBL.GetAvailableSponsorColorThemes(_session.PrimarySponsorID);
            var colorTheme = colorThemes.FirstOrDefault(ct => sponsorProfile != null && ct.BrandingThemeId == sponsorProfile.BrandingThemeID);
            _session.ThemeCode = colorTheme;
            CacheProvider.Update(System.Web.HttpContext.Current, CacheProvider.CacheItems.VHMWebSessionObject, _session);
        }

        /// <summary>
        /// Decrypts an encrypted string using FormsAuthentication
        /// </summary>
        /// <param name="dataToDecrypt"></param>
        /// <returns></returns>
        private string Decrypt(string dataToDecrypt)
        {
            var data = string.Empty;
            try
            {
                var formsAuthenticationTicket = FormsAuthentication.Decrypt(dataToDecrypt);

                data = formsAuthenticationTicket != null
                           ? formsAuthenticationTicket.UserData
                           : string.Empty;
            }
            catch (Exception ex)
            {
                VirginLifecare.Exception.ExceptionHandler.ProcessException(ex);
            }

            return data;
        }
        #endregion

    }
}
