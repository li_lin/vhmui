﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using StackExchange.Profiling;

namespace Vhm.Web.Challenges.Controllers.Attributes
{
    public class ProfileAttribute : ActionFilterAttribute
    {

        const string STACK_KEY = "ProfilingActionFilterStack";

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            MiniProfiler.Start();

            var mp = MiniProfiler.Current;
            if (mp != null)
            {
                var stack = HttpContext.Current.Items[STACK_KEY] as Stack<IDisposable>;
                if (stack == null)
                {
                    stack = new Stack<IDisposable>();
                    HttpContext.Current.Items[STACK_KEY] = stack;
                }
                MiniProfiler.Current.Level = ProfileLevel.Verbose;

                var prof = MiniProfiler.Current.Step("Controller: " + filterContext.Controller + "." + filterContext.ActionDescriptor.ActionName);
                
                stack.Push(prof);

            }

            
            base.OnActionExecuting(filterContext);
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
            var stack = HttpContext.Current.Items[STACK_KEY] as Stack<IDisposable>;
            if (stack != null && stack.Count > 0)
            {
                stack.Pop().Dispose();
            }
            
            MiniProfiler.Stop();
        }

    }
}